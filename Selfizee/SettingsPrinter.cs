﻿
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;

namespace Selfizee
{
    public class SettingsPrinter
    {
        [DllImport("winspool.Drv",
          EntryPoint = "DocumentPropertiesW",
          SetLastError = true,
          ExactSpelling = true,
          CallingConvention = CallingConvention.StdCall)]
        static extern int DocumentProperties
            (IntPtr hwnd, IntPtr hPrinter,
            [MarshalAs(UnmanagedType.LPWStr)]
            string pDeviceName,
            IntPtr pDevModeOutput,
            IntPtr pDevModeInput,
            int fMode);
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalFree(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalLock(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalUnlock(IntPtr handle);


        public static bool GetPrinterSettings(System.Drawing.Printing.PrinterSettings prnSettings, string fileSettingsName)
        {
            IntPtr hDevMode;
            IntPtr pDevMode;

            IntPtr hPrinter = new IntPtr(0);
            // Open the printer.
            if (OpenPrinter(prnSettings.PrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                try
                {
                    // Get the default printer settings
                    hDevMode = prnSettings.GetHdevmode(prnSettings.DefaultPageSettings);

                    // Obtain a lock on the handle
                    pDevMode = GlobalLock(hDevMode);                   

                    
                    var directorySettings = "C:/PRINTERSETTINGS";
                    var pathFileSettings = $"{directorySettings}/{fileSettingsName}";
                    if (!Directory.Exists(directorySettings))
                    {
                        Directory.CreateDirectory(directorySettings);
                    }
                    int sizeNeeded = DocumentProperties(IntPtr.Zero, IntPtr.Zero, prnSettings.PrinterName, IntPtr.Zero, pDevMode, 0);
                    // file stream
                    using (FileStream fs = new FileStream(pathFileSettings, FileMode.Create, FileAccess.Write))
                    {
                        for (int i = 0; i < sizeNeeded; ++i)
                        {
                            fs.WriteByte(Marshal.ReadByte(pDevMode, i));
                        }
                    }

                    // Unlock the handle                
                    GlobalUnlock(hDevMode);
                    GlobalFree(hDevMode);
                    hDevMode = IntPtr.Zero;
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"ERROR Writing settings to file : {ex}");
                }           
            }

            return false;                       
        }

        public static bool UploadPrinterSettings(System.Drawing.Printing.PrinterSettings prnSettings, string fileSettingsName)
        {
            IntPtr hDevMode;
            IntPtr pDevMode;
            byte[] savedDevMode;
            Stream savedDevStream;

            IntPtr hPrinter = new IntPtr(0);

            var pathFileSettings = $"C:/PRINTERSETTINGS/{fileSettingsName}";
            if (File.Exists(pathFileSettings))
            {           
                // Open the printer.
                if (OpenPrinter(prnSettings.PrinterName.Normalize(), out hPrinter, IntPtr.Zero))
                {
                    // get the current DEVMODE position in memory
                    hDevMode = prnSettings.GetHdevmode(prnSettings.DefaultPageSettings);

                    // Obtain a lock
                    pDevMode = GlobalLock(hDevMode);

                    // Load the saved DEVMODE
                    savedDevStream = new FileStream(pathFileSettings, FileMode.Open, FileAccess.Read);
                    savedDevMode = new byte[savedDevStream.Length];
                    savedDevStream.Read(savedDevMode, 0, savedDevMode.Length);
                    savedDevStream.Close();
                    savedDevStream.Dispose();

                    // Overwrite the current DEVMODE
                    for (int i = 0; i < savedDevMode.Length; ++i)
                    {
                        Marshal.WriteByte(pDevMode, i, savedDevMode[i]);
                    }

                    // It is good to go. All done
                    GlobalUnlock(hDevMode);

                    // upload our printer settings to use the one we just overwrote
                    prnSettings.SetHdevmode(hDevMode);

                    GlobalFree(hDevMode);
                }
                else
                {
                    return false;
                }
            }else { return false; }
            return true;
        }

        public static PrinterSettings OpenPrinterPropertiesDialog(PrinterSettings printerSettings)
        //Shows the printer settings dialogue that comes with the printer driver
        {
            IntPtr hDevMode = IntPtr.Zero;
            IntPtr devModeData = IntPtr.Zero;
            IntPtr hPrinter = IntPtr.Zero;
            String pName = printerSettings.PrinterName;
            try
            {
                hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
                IntPtr pDevMode = GlobalLock(hDevMode);
                int sizeNeeded = DocumentProperties(IntPtr.Zero, IntPtr.Zero, pName, IntPtr.Zero, pDevMode, 0);//get needed size and allocate memory 
                if (sizeNeeded < 0)
                {
                    MessageBox.Show("Bummer, Cant get size of devmode structure");
                    Marshal.FreeHGlobal(devModeData);
                    Marshal.FreeHGlobal(hDevMode);
                    devModeData = IntPtr.Zero;
                    hDevMode = IntPtr.Zero;
                    return printerSettings;
                }
                devModeData = Marshal.AllocHGlobal(sizeNeeded);
                //show the native dialog 
                int returncode = DocumentProperties(IntPtr.Zero, IntPtr.Zero, pName, devModeData, pDevMode, 14);
                if (returncode < 0) //Failure to display native dialogue
                {
                    MessageBox.Show("Dialogue Bummer, Got me a devmode, but the dialogue got stuck");
                    Marshal.FreeHGlobal(devModeData);
                    Marshal.FreeHGlobal(hDevMode);
                    devModeData = IntPtr.Zero;
                    hDevMode = IntPtr.Zero;
                    return printerSettings;
                }
                if (returncode == 2) //User clicked "Cancel"
                {
                    GlobalUnlock(hDevMode);//unlocks the memory
                    if (hDevMode != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(hDevMode); //Frees the memory
                        hDevMode = IntPtr.Zero;
                    }
                    if (devModeData != IntPtr.Zero)
                    {
                        GlobalFree(devModeData);
                        devModeData = IntPtr.Zero;
                    }
                }
                GlobalUnlock(hDevMode);//unlocks the memory
                if (hDevMode != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(hDevMode); //Frees the memory
                    hDevMode = IntPtr.Zero;
                }
                if (devModeData != IntPtr.Zero)
                {
                    printerSettings.SetHdevmode(devModeData);
                    printerSettings.DefaultPageSettings.SetHdevmode(devModeData);
                    GlobalFree(devModeData);
                    devModeData = IntPtr.Zero;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred, caught and chucked back\n" + ex.Message);
            }
            finally
            {
                if (hDevMode != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(hDevMode);
                }
                if (devModeData != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(devModeData);
                }
            }
            return printerSettings;
        }
    }
}