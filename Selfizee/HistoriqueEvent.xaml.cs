﻿using Selfizee.Managers;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee
{
    /// <summary>
    /// Logique d'interaction pour HistoriqueEvent.xaml
    /// </summary>
    public partial class HistoriqueEvent : UserControl
    {
        public string code { get; set; }
        private static Wifi wifi;
        string push = "";

        public HistoriqueEvent()
        {
            wifi = new Wifi();
            InitializeComponent();

            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            var brushTB = new ImageBrush();
            brushTB.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnTB, UriKind.Relative));
            //btnTB.Background = brushTB;

            //var brushRetour = new ImageBrush();
            //brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAnim, UriKind.Relative));
            //btnRetour.Background = brushRetour;

            code = Globals.codeEvent_toEdit;
            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));


            Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);

            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);
            imgCercle.Visibility = Visibility.Hidden;

            string iniPath = Globals._assetsFolder + "\\" + Globals.codeEvent_toEdit + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(iniPath);
            string Name = iniFile.GetSetting("EVENT", "Name");

            setTimeLine();
        }

        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (Globals.ScreenType == "SPHERIK")
            {
                if (viewModel.GoToAdminSumarySpherik.CanExecute(null))
                    viewModel.GoToAdminSumarySpherik.Execute(null);
            }
            else if (Globals.ScreenType == "DEFAULT")
            {
                if (viewModel.GoToAdminSumary.CanExecute(null))
                    viewModel.GoToAdminSumary.Execute(null);
            }
        }

        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }

        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(HistoriqueEvent));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter l'application, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void Configuration_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToConfiguration.CanExecute(null))
                viewModel.GoToConfiguration.Execute(null);
        }

        private void BackToEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (HistoriqueEventViewModel)DataContext;
            if (viewModel.GoToAdminSumary.CanExecute(null))
                viewModel.GoToAdminSumary.Execute(null);
        }
        

        private void setTimeLine()
        {
            string path = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";
            DateTime histoDate = new DateTime();
            string curTime = "";
            if (File.Exists(path))
            {
                DateTime? lastDate = null;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                //for (int i = 1; i < rows.Length; i++)
                for (int i = rows.Length - 1; i > 0; i--)
                {
                    string[] tabText = rows[i].Split(',');

                    if (tabText == null || tabText.Count() < 8)
                        tabText = rows[i].Split(';');

                    if (tabText != null && tabText.Count() >= 8)
                    {
                        string dtPhoto = tabText[7];
                        string dtPrint = tabText[8];
                        string current = "";
                        string currentLib = "";
                        if (String.IsNullOrEmpty(dtPrint))
                        {
                            current = dtPhoto;
                            currentLib = "photo";
                            if (!string.IsNullOrEmpty(current))
                            {
                                Regex regex = new Regex(@"\s");
                                string[] splitted = regex.Split(current.ToLower());
                                curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                string curDate = splitted[0];

                                //DateTime histoDate = Convert.ToDateTime(curDate);
                                //DateTime histoDate = DateTime.Parse(curDate);
                                if (curDate.Contains('/'))
                                {
                                    try
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null);
                                    }
                                    catch (Exception e)
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "dd/MM/yyyy", null);
                                    }
                                    
                                }
                                else
                                {
                                    histoDate = DateTime.ParseExact(curDate, "yyyy-MM-dd", null);
                                }

                            }
                        }
                        else
                        {
                            current = dtPrint;
                            currentLib = "photo + impression";
                            if (!string.IsNullOrEmpty(current))
                            {
                                Regex regex = new Regex(@"\s");
                                string[] splitted = regex.Split(current.ToLower());
                                curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                string curDate = splitted[0];

                                //DateTime histoDate = Convert.ToDateTime(curDate);
                                //DateTime histoDate = DateTime.Parse(curDate);
                                if (curDate.Contains('/'))
                                {
                                    try
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null);
                                    }
                                    catch (Exception e)
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "dd/MM/yyyy", null);
                                    }
                                }
                                else
                                {
                                    histoDate = DateTime.ParseExact(curDate, "yyyy-MM-dd", null);
                                }
                                

                            }
                        }

                        if (!lastDate.HasValue || lastDate != histoDate)
                        {
                            TextBlock dateHisto = new TextBlock();
                            dateHisto.Text = histoDate.ToLongDateString().UppercaseFirstEach();
                            dateHisto.Height = 15;
                            dateHisto.FontSize = 12;
                            dateHisto.Margin = new Thickness(50, 30, 0, 0);
                            dateHisto.FontWeight = FontWeights.Bold;
                            dateHisto.Foreground = System.Windows.Media.Brushes.White;
                            timelineStack.Children.Add(dateHisto);

                            lastDate = histoDate;
                        }
                        TextBlock infoHisto = new TextBlock();
                        infoHisto.Text = curTime + " : " + currentLib;
                        infoHisto.Height = 17;
                        infoHisto.FontSize = 14;

                        infoHisto.Margin = new Thickness(50, 10, 0, 0);
                        infoHisto.FontWeight = FontWeights.Bold;
                        infoHisto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#B9B9B9"));
                        //rowHisto.Width = 250;
                        timelineStack.Children.Add(infoHisto);
                        Separator sep1 = new Separator();
                        sep1.Margin = new Thickness(50, 10, 0, 0);
                        sep1.Width = 400;
                        sep1.HorizontalAlignment = HorizontalAlignment.Left;
                        sep1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#B9B9B9"));
                        timelineStack.Children.Add(sep1);
                    }
                }
            }
        }
    }
}
