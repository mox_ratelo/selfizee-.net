﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Media;

using System.Windows.Threading;
using EOSDigital.API;
using EOSDigital.SDK;

using System.Windows.Interop;
using System.Windows.Forms;
using Selfizee.Managers;
using Selfizee.Models.Enum;
using Selfizee.Models.Form;
using log4net;
using System.Threading;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public delegate void ReadyToShowDelegate(object sender, EventArgs args);

        public event ReadyToShowDelegate ReadyToShow;

        private DispatcherTimer timer;

        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private static string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";
        private static string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private INIFileManager _inimanager1 = new INIFileManager(Globals._appConfigFile);
        List<Fenetre> fenetres = new List<Fenetre>();
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        //private INIFileManager _inimanager = new INIFileManager($"{Globals.ExeDir()}\\Config.ini");

        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
            System.Windows.Application curApp = System.Windows.Application.Current;
            //curApp.Shutdown();
            e.Handled = true;
            Environment.Exit(0);
        }

        public MainWindow()
        {
            InitializeComponent();
            this.Activate();
            this.Focus();

            // Set the current process to run at 'High' Priority
            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            process.PriorityClass = System.Diagnostics.ProcessPriorityClass.High;

            // Set the current thread to run at 'Highest' Priority
            Thread thread = System.Threading.Thread.CurrentThread;
            thread.Priority = ThreadPriority.Highest;

            setTimerToShowMain();
            HideCursor();

            Globals.portName = _inimanager.GetSetting("SERIAL", "name");
            string port = _inimanager.GetSetting("SERIAL", "port");
            if (!string.IsNullOrEmpty(port))
            {
                Globals.portNumber = Convert.ToInt32(port);
            }
           
           
            //ImageBrush imgBrush = new ImageBrush();
            //imgBrush.ImageSource = new BitmapImage(new Uri(Globals.LoadBG(TypeFond.BgGolbal), UriKind.Relative));
            //imgBrush.Stretch = Stretch.Fill;
            //mainWindow.Background = imgBrush;

            fenetres = _inimanager.GetFenetre();
            var questions = fenetres.Select(s => s.Questions.Select(q => q.Title)).Distinct().ToList();
            string firstline = "startdatetime;event;fondvert;photo;impression;";
            //foreach (var items in questions)
            //    foreach (var item in items)
            //        firstline += $"{item};";
            //if (!File.Exists(Result))
            //{
            //    File.Create(Result).Close();
            //    StreamWriter sw = new StreamWriter(Result);
            //    sw.WriteLine(firstline);
            //    sw.Dispose();
            //    sw.Close();
            //}



            //if (File.Exists(TmpCsv))
            //{
            //    File.Delete(TmpCsv);
            //}

            //File.Create(TmpCsv).Close();

            //using (StreamWriter sw2 = new StreamWriter(TmpCsv))
            //{
            //    sw2.WriteLine($"startdatetime={DateTime.Now}");
            //}
            

    }

       
        void timer_Tick(object sender, EventArgs e)
        {

            timer.Stop();

            if (ReadyToShow != null)
            {
                ReadyToShow(this, null);
            }


        }

        void setTimerToShowMain()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private void MainWindow_Closed(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //System.Windows.Application.Current.Shutdown(); //or Environment.Exit(0);
            if (!string.IsNullOrEmpty(Globals.heureConnexion))
            { 
                string exitTime = DateTime.Now.ToString("HH:mm");
                DateTime connexion = DateTime.Parse(Globals.heureConnexion);
                DateTime deconnexion = DateTime.Parse(exitTime);
                var diff = deconnexion.Subtract(connexion);
                var res = String.Format("[{0} Heure(s)][{1} minutes][{2} seconde(s)]", diff.Hours, diff.Minutes, diff.Seconds);
                Log.Info(res);
            }
            Environment.Exit(0);
        }

        private System.Windows.Size GetHostingScreenSize()
        {
            var interopHelper = new WindowInteropHelper(System.Windows.Application.Current.MainWindow);
            var activeScreen = Screen.FromHandle(interopHelper.Handle);

            var hdc = Managers.ScreenSizeManager.CreateDC(activeScreen.DeviceName, "", "", IntPtr.Zero);

            // Height
            int DESKTOPVERTRES = Managers.ScreenSizeManager.GetDeviceCaps(hdc, (int)Managers.DeviceCap.DESKTOPVERTRES);

            //width
            int DESKTOPHORZRES = Managers.ScreenSizeManager.GetDeviceCaps(hdc, (int)Managers.DeviceCap.DESKTOPHORZRES);

            return new System.Windows.Size(DESKTOPHORZRES, DESKTOPVERTRES);
        }

        private void HideCursor()
        {
            #region btnretakepictureconfig
            var enableCursor = _inimanager1.GetSetting("CURSOR", "EnableCursor");
            if (enableCursor == "0")
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.None;
            }
            #endregion
        }




    }
}
