﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections;
using System.Drawing;
using Selfizee.Models.Enum;
using Selfizee.Models;
using Selfizee.Managers;
using log4net;
using System.Threading.Tasks;
using System.Windows.Threading;
using Selfizee.View;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for FiltreManagement.xaml
    /// </summary>
    public partial class FiltreManagement : UserControl
    {
        private string _imageFolder = @"View"; //Globals.directory_Target; 
        public static string csvHoraires = Globals.EventAssetFolder() + "\\InstantGagnant\\horaires.csv";
        private string _imageFinalFolder = @"Final";
        private ArrayList _imageFiles;
        private string _filtered;
        private string path_ToSave = "";
        private string _imageFrame = @"Frames";
        private string _finalFolder = @"Final";
        private string currDir = "";
        private bool win = false;
        DateTime start;
        DateTime end;
        List<DateTime> lstHeure;
        private bool _instantWinner = false;

        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        CollectionViewSource view = new CollectionViewSource();

        int currentPageIndex = 0;
        int itemPerPage = 6;
        int totalPage = 0;

        private ImageCollection _photos;
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        private bool isClickInPage = true;
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        public void setButtonHome()
        {
            setTimeout();
            LaunchTimerAfter2();

            if (File.Exists(Globals._btnVisualisationHome))
            {
                BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._btnVisualisationHome);
            }
            else
            {
                BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._defaultbtnVisualisationHome);
            }
            var btnGoToHomePosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualisationBackToHome");
            if (btnGoToHomePosition[0] != "")
                Canvas.SetLeft((Button)vbtnGoToHome, Convert.ToInt32(btnGoToHomePosition[0]));

            var VisuBtnHomeX = _inimanager.GetSetting("BUTTONPOSITION", "VisuBtnHomeX");
            var VisuBtnHomeY = _inimanager.GetSetting("BUTTONPOSITION", "VisuBtnHomeY");
            VisuBtnHomeX = !string.IsNullOrEmpty(VisuBtnHomeX) ? VisuBtnHomeX : "0";
            VisuBtnHomeY = !string.IsNullOrEmpty(VisuBtnHomeY) ? VisuBtnHomeY : "0";

            TranslateTransform initHomeTransform = new TranslateTransform();
            initHomeTransform.X = (-1) * double.Parse(($"{VisuBtnHomeX}").ToString());
            initHomeTransform.Y = double.Parse(($"{VisuBtnHomeY}").ToString());
            this.StkpHome.RenderTransform = initHomeTransform;
        }

        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }

        private void BtnContent(Button btn, string imageName, string contentString, string imagePath)
        {
            if (File.Exists(imagePath))
            {
                var icon = new System.Windows.Controls.Image();
                icon.Name = imageName;
                icon.MaxHeight = 280;
                icon.MaxWidth = 600;
                BitmapImage bmImage = new BitmapImage();
                bmImage.BeginInit();
                bmImage.UriSource = new Uri(imagePath);
                bmImage.EndInit();
                icon.Source = bmImage;
                btn.Content = icon;
                btn.Width = icon.Width;
                btn.Height = icon.Height;
                btn.Background = new SolidColorBrush(Colors.Transparent);
                btn.Foreground = new SolidColorBrush(Colors.Transparent);
                btn.BorderBrush = new SolidColorBrush(Colors.Transparent);
            }

            if (!File.Exists(imagePath))
            {
                btn.Content = contentString;
                btn.Width = 180;
                btn.Height = 30;
            }
        }

        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            StkpHome.Visibility = Visibility.Visible;
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();

        }

        public void Page_Click(object sender, MouseEventArgs e)
        {
            if (isClickInPage)
            {
                StkpHome.Visibility = Visibility.Hidden;
                lbl_CountDown.Content = "";
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                LaunchTimerAfter2();
            }

        }

        public void Touch_Click(object sender, TouchEventArgs e)
        {
            if (isClickInPage)
            {
                StkpHome.Visibility = Visibility.Hidden;
                lbl_CountDown.Content = "";
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                LaunchTimerAfter2();
            }
        }

        private void GoToThanks(object sender, RoutedEventArgs e)
        {
            if(timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            
            var viewModel = (FiltreManagementViewModel)DataContext;
            if (viewModel.GoToThanks.CanExecute(null))
                viewModel.GoToThanks.Execute(null);
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0 )
            //{
            //    setTimeout();
            //}
            Globals.timeOut--;
            if (Globals.timeOut == 0)
            {
                lbl_CountDown.Content = "";
            }
            else
            {
                lbl_CountDown.Content = Globals.timeOut;
            }

            if (Globals.timeOut == -1)
            {
                lbl_CountDown.Content = "";
                GoToThank();


            }
        }

        private void GoToThank()
        {
            // SaveCSV(); 
            //await Task.Delay(TimeSpan.FromSeconds(12));
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var viewModel = (FiltreManagementViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToThanks.CanExecute(null))
                    viewModel.GoToThanks.Execute(null);
            }

        }

        private List<string> ImageUri(string imageFolder)
        {
            List<string> images = new List<string>();
            DirectoryInfo directory = new DirectoryInfo(imageFolder);
            if (directory.GetFiles().Any())
                foreach (var item in directory.GetFiles())
                    images.Add(item.Name);

            images = filterListContent(images);
            return images;
        }

        private List<string> filterListContent(List<string> images)
        {
            List<string> finalList = new List<string>();
            foreach (string str in images)
            {
                if (str.ToLower().Contains("_color"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (string str in images)
            {
                if (str.ToLower().Contains("_blackwhite"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (string str in images)
            {
                if (str.ToLower().Contains("_sepia"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (string str in images)
            {
                if (str.ToLower().Contains("_blackwhite") == false && str.ToLower().Contains("_color") == false && str.ToLower().Contains("_sepia") == false)
                {
                    finalList.Add(str);
                }
            }
            return finalList;
        }

        private ArrayList filterArrayContent(ArrayList images)
        {
            ArrayList finalList = new ArrayList();
            foreach (FileInfo str in images)
            {
                string imageName = System.IO.Path.GetFileNameWithoutExtension(str.FullName);
                if (imageName.ToLower().Contains("_color"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (FileInfo str in images)
            {
                string imageName = System.IO.Path.GetFileNameWithoutExtension(str.FullName);
                if (imageName.ToLower().Contains("_blackwhite"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (FileInfo str in images)
            {
                string imageName = System.IO.Path.GetFileNameWithoutExtension(str.FullName);
                if (imageName.ToLower().Contains("_sepia"))
                {
                    finalList.Add(str);
                    break;
                }
            }
            foreach (FileInfo str in images)
            {
                string imageName = System.IO.Path.GetFileNameWithoutExtension(str.FullName);
                if (imageName.ToLower().Contains("_blackwhite") == false && imageName.ToLower().Contains("_color") == false && imageName.ToLower().Contains("_sepia") == false)
                {
                    finalList.Add(str);
                }
            }
            return finalList;
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {

                                GC.SuppressFinalize(this);
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                                var viewModel = (FiltreManagementViewModel)DataContext;
                               /* if (viewModel.GoToConnexionClient.CanExecute(null))
                                    viewModel.GoToConnexionClient.Execute(null);*/
                                Globals._FlagConfig = "client";
                                if (Globals.ScreenType == "SPHERIK")
                                {
                                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                        viewModel.GoToClientSumarySpherik.Execute(null);
                                }
                                else if (Globals.ScreenType == "DEFAULT")
                                {
                                    if (viewModel.GoToClientSumary.CanExecute(null))
                                        viewModel.GoToClientSumary.Execute(null);
                                }
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                        break;
                    case Key.F2:

                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                GC.SuppressFinalize(this);
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                                Globals._FlagConfig = "admin";
                                var viewModel2 = (FiltreManagementViewModel)DataContext;
                                if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                    viewModel2.GoToConnexionConfig.Execute(null);
                                
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                            
                        });
                        
                        
                        break;


                }
            }
            catch (Exception ex)
            {

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            setButtonHome();

            this.Focusable = true;
            Keyboard.Focus(this);
            var inimanager = _inimanager;//new Managers.INIFileManager($"{Globals.ExeDir()}\\Config.ini");

            var activeFiltre = inimanager.GetNumberOfActiveFilter();

            var btngototakepicture = inimanager.GetBtnPosition("BUTTONPOSITION", "GoToTakePicture");
            take_photo_again.Visibility = inimanager.GetSetting("BUTTONCONFIG", "enableButtonRetakePicture").Trim().Equals("1") ? Visibility.Visible : Visibility.Hidden;

            /*  if (btngototakepicture[0] != "")
                  Canvas.SetLeft((Button)BtnGoToTakePicture, Convert.ToInt32(btngototakepicture[0]));
              if (btngototakepicture[1] != "")
                  Canvas.SetBottom((Button)BtnGoToTakePicture, Convert.ToInt32(btngototakepicture[1]));*/

            currDir = Globals.currDir;
            path_ToSave = currDir + $"\\{_imageFolder}";
            _finalFolder = currDir + $"\\{_finalFolder}";
            _imageFiles = GetImageFileInfo();
            //image1.DataContext = _imageFiles[0];
            //image2.DataContext = _imageFiles[1];
            //image3.DataContext = _imageFiles[2];
            if (_imageFiles.Count == 1)//&& activeFiltre == 1)
            {
                GC.SuppressFinalize(this);
                GC.WaitForPendingFinalizers();
                GC.Collect();
                if (timerCountdown != null) timerCountdown.Stop();
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                var vm = (FiltreManagementViewModel)DataContext;
                Globals.imageChosen = _imageFiles[0].ToString();
                vm.selectedFilter = _imageFiles[0].ToString();
                if (vm.GoToVisualisation.CanExecute(null))
                    vm.GoToVisualisation.Execute(null);
            }
            ImageCollection collect = new ImageCollection();
            //_photos = new ImageCollection(_finalFolder);
            _photos = collect.Update(_imageFiles);

            ((CollectionViewSource)this.Resources["MyPhotos"]).Source = _photos;
            ((CollectionViewSource)this.Resources["MyPhotos"]).View.MoveCurrentToPosition(-1);
            ((CollectionViewSource)this.Resources["MyPhotos"]).Filter += new FilterEventHandler(view_Filter);

            int itemcount = _photos.Count;

            // Calculate the total pages
            totalPage = itemcount / itemPerPage;
            if (itemcount % itemPerPage != 0)
            {
                totalPage += 1;
            }

            string btnleft = "";
            if (File.Exists(Globals._arrowLeft))
            {
                btnleft = Globals._arrowLeft;
            }

            string btnright = Globals._arrowRight;
            /*
            btnpreview.DataContext = btnleft;
            btnNext.DataContext = btnright;

            if (totalPage <= 1)
            {
                btnpreview.IsEnabled = false;
                btnNext.IsEnabled = false;
                btnNext.Visibility = Visibility.Hidden;
                btnpreview.Visibility = Visibility.Hidden; 
            }*/
            //_photos = (ImageCollection)(Resources["MyPhotos"] as ObjectDataProvider).Data;
            //_photos.Path = _finalFolder;

            /*Test for dina*/
            /*
            ImageFilterManager imageManager = new ImageFilterManager();
            imageManager.Applifilter(_imageFiles[0].ToString());
            */
            /*********/

            #region btn
            string imagePath = "";
            if (File.Exists(Globals._btnFiltreGoToTakePicture))
            {
                imagePath = Globals._btnFiltreGoToTakePicture;
            }
            else
            {
                imagePath = Globals._defaultbtnFiltreGoToTakePicture;
            }
            if (File.Exists(imagePath))
            {
                var btnBackToTakePicture = new System.Windows.Controls.Image();
                btnBackToTakePicture.Name = "ImageBtnBackToTakePicture";
                btnBackToTakePicture.MaxHeight = 100;
                btnBackToTakePicture.MaxWidth = 100;
                BitmapImage bmImage = new BitmapImage();
                bmImage.BeginInit();
                bmImage.UriSource = new Uri(imagePath);
                bmImage.EndInit();
                btnBackToTakePicture.Source = bmImage;
                /* BtnGoToTakePicture.Content = btnBackToTakePicture;
                 BtnGoToTakePicture.Width = btnBackToTakePicture.Width;
                 BtnGoToTakePicture.Height = btnBackToTakePicture.Height;
                 BtnGoToTakePicture.Background = new SolidColorBrush(Colors.Transparent);
                 BtnGoToTakePicture.Foreground = new SolidColorBrush(Colors.Transparent);
                 BtnGoToTakePicture.BorderBrush = new SolidColorBrush(Colors.Transparent); */
            }
            /*
            if (!File.Exists(imagePath))
            {
                BtnGoToTakePicture.Content = "Revenir à la prise de photo ";
                BtnGoToTakePicture.Width = 180;
                BtnGoToTakePicture.Height = 30; 
            }
            #endregion

            #region btnretakepictureconfig
            var enableBtnRetakePicture = inimanager.GetSetting("BUTTONCONFIG", "EnableButtonRetakePicture");
            if(enableBtnRetakePicture=="0")
            {
                BtnGoToTakePicture.Visibility = Visibility.Hidden; 
            }*/
            #endregion
        }

        private void view_Filter(object sender, FilterEventArgs e)
        {
            int index = _photos.IndexOf((Images)e.Item);

            if (index >= itemPerPage * currentPageIndex && index < itemPerPage * (currentPageIndex + 1))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }

        }

        private void btnPreview_click(object sender, RoutedEventArgs e)
        {
            // Display previous page
            if (currentPageIndex > 0)
            {
                currentPageIndex--;
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }

        private void btnNext_click(object sender, RoutedEventArgs e)
        {
            // Display next page
            if (currentPageIndex < totalPage - 1)
            {
                currentPageIndex++;
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }


        public FiltreManagement()
        {
            InitializeComponent();
            Globals.point_relais = "";
            Globals._originalString = "";
            lstHeure = _inimanager.getAllHours("INSTANTGAGNANT");
            start = lstHeure[0];
            end = lstHeure[1];
            Globals.gift = "";
            Globals.gift_index = -1;
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            this.MouseLeftButtonUp += Page_Click;
            this.TouchUp += Touch_Click;
            var vm = (FiltreManagementViewModel)DataContext;
            if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
            {
                ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                comMgr.writeData("filter");
            }
            ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("ChoiceFilter");
            if (backgroundAttributes.IsImage)
            {
                ImageBrush imgBrush = new ImageBrush();
                var fileName = Globals.LoadBG(TypeFond.ForFiltre);
                if (!File.Exists(fileName))
                {
                    fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                }
                Bitmap bmp = null;
                using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
                {
                    bmp = new Bitmap(img, new System.Drawing.Size(1920, 1080));
                }
                imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp);
                imgBrush.Stretch = Stretch.Fill;
                filtrePage.Background = imgBrush;
            }

            if (!backgroundAttributes.EnableTitle)
                viewTitle.Visibility = Visibility.Collapsed;

            if (!backgroundAttributes.IsImage)
            {
                if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                {
                    var bgcolor = backgroundAttributes.BackgroundColor;
                    var bc = new BrushConverter();
                    this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                }


                if (backgroundAttributes.EnableTitle)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                        viewTitle.Text = backgroundAttributes.Title;
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                        viewTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                            Canvas.SetLeft((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                            Canvas.SetTop((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                    }
                }
            }

            CheckGift();

        }

        private void ShowImage(object sender, SelectionChangedEventArgs args)
        {
            var list = ((ListBox)sender);
            var index = list?.SelectedIndex; //Save the selected index 
            if (index >= 0)
            {
                var selection = list.SelectedItem.ToString();
                if (!string.IsNullOrEmpty(selection))
                {
                    //Set currentImage to selected Image
                    var selLoc = new Uri(selection);
                    var id = new BitmapImage(selLoc);
                    var currFileInfo = new FileInfo(selection);
                    /*
                    currentImage.Source = id;

                    //Setup Info Text
                    imageSize.Text = id.PixelWidth + " x " + id.PixelHeight;
                    imageFormat.Text = id.Format.ToString();
                    fileSize.Text = ((currFileInfo.Length + 512) / 1024) + "k";
                    */
                }
            }
        }

        private ArrayList GetImageFileInfo()
        {
            var vm = (FiltreManagementViewModel)DataContext;
            var imageFiles = new ArrayList();

            //var currDir = Directory.GetCurrentDirectory();
            var currDir = Globals.currDir;
            //var temp = currDir + $"\\..\\..\\{_imageFolder}";
            var temp = currDir + $"\\{_imageFinalFolder}";
            var manager = new Manager.ImageFilterManager();

            //if(vm.cmdOrigin != "FromVisualisation")
            //_filtered = manager.Applifilter(Directory.GetFiles(temp, "*.jpeg")[0]);
            var files = Directory.GetFiles(temp, "*.jpg");

            foreach (var image in files)
            {
                var info = new FileInfo(image);
                imageFiles.Add(info);
            }

            //imageFiles.Sort();

            #region chromakeytest
            /*
            var _manager = new Manager.ImageFilterManager();
            //_manager.ChromaKeyManager(files[0]);
            //_manager.ChromaKey2(files[0]);
            _manager.Chromakey3(files[0]);
            Bitmap img = _manager.RemoveBackground(new Bitmap(files[0]), vm.selectedBG);
            string finalPath = $"{Globals.ExeDir()}\\Final";
            img.Save(finalPath + "\\_bitmap_method.png"); 
            */
            #endregion
            imageFiles = filterArrayContent(imageFiles);
            return imageFiles;
        }

        private void SelectedFilter(object sender, RoutedEventArgs e)
        {
            isClickInPage = false;
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;

                }));

            }).ContinueWith(task =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    GC.SuppressFinalize(this);
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    if (timerCountdown != null) timerCountdown.Stop();
                    if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                    var fe = (FrameworkElement)sender;
                    var vm = (FiltreManagementViewModel)DataContext;
                    if (vm.GoToVisualisation.CanExecute(null))
                        vm.GoToVisualisation.Execute(null);
                }));

            });
            

            

        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            isClickInPage = false;
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();

                }));

            }).ContinueWith(task =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    try
                    {
                        var selectedImage = (sender as ListBox).SelectedItem as Images;
                        //string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";
                        var vm = (FiltreManagementViewModel)DataContext;
                        string directory = Path.GetDirectoryName(selectedImage.Source.ToString());
                        string selected = "";
                        Globals.filterChosen = getFilterName(Path.GetFileNameWithoutExtension(selectedImage.Source.ToString()));
                        switch (Globals.EventChosen)
                        {
                            case 1:
                                selected = directory + "\\Original\\" + Path.GetFileName(selectedImage.Source.ToString());
                                break;
                            default:
                                selected = selectedImage.Source.ToString();
                                break;
                        }
                        Globals.imageChosen = selected;
                        if (timerCountdown != null) timerCountdown.Stop();
                        if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                        //vm.selectedFilter = selected;
                        if (vm.GoToVisualisation.CanExecute(null))
                        {
                            vm.GoToVisualisation.Execute(null);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Log.Error($"FiltreManagement.xaml.cs - LbSelectionChanged ERROR : {ex}");
                    }
                }));

            });          


        }

        private void CheckGift()
        {
            if (File.Exists(csvHoraires))
            {
                FileInfo info = new FileInfo(Globals.csvHoraires);
                if (!IsFileinUse(info))
                {
                    Globals.lstHoraires = new System.Collections.Generic.List<InstantGagnant>();
                    Globals.getHoraires();
                    DateTime now = DateTime.Now;
                    var check = Globals.lstHoraires.Where(s => s.startDate.Month == now.Month && s.startDate.Year == now.Year && s.startDate.Day == now.Day && s.win == false && s.startDate.Hour <= now.Hour && now.Hour <= end.Hour && start.Hour <= now.Hour).DefaultIfEmpty().First();
                    if (check != null)
                    {
                        if (check.startDate.Hour == now.Hour && check.startDate.Minute <= now.Minute)
                        {
                            win = true;
                        }
                        if (check.startDate.Hour < now.Hour)
                        {
                            win = true;
                        }
                        if (win == true)
                        {
                            switch (check.TypeCadeau)
                            {
                                case 0:
                                    Globals.gift = "Bonnet Alpes";
                                    Globals.point_relais = "Equipe #purealpes";
                                    Globals.gift_index = 0;
                                    break;
                                case 1:
                                    Globals.gift = "Bonnet le Sauze";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 1;
                                    break;
                                case 2:
                                    Globals.gift = "Bonnet les Orres";
                                    Globals.point_relais = "Stations Les Orres";
                                    Globals.gift_index = 2;
                                    break;
                                case 3:
                                    Globals.gift = "Bonnet Pra Loup";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 3;
                                    break;
                                case 4:
                                    Globals.gift = "Place de Cinéma Pra Loup";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 4;
                                    break;
                                case 5:
                                    Globals.gift = "Flasque Alpes";
                                    Globals.point_relais = "Equipe #purealpes";
                                    Globals.gift_index = 5;
                                    break;
                                case 6:
                                    Globals.gift = "Forfait Champsaur 3 Gliss";
                                    Globals.point_relais = "Station du Champsaur et Valgaudemar";
                                    Globals.gift_index = 6;
                                    break;
                                case 7:
                                    Globals.gift = "Forfait Alpin journée à Dévoluy";
                                    Globals.point_relais = "Station du Dévoluy";
                                    Globals.gift_index = 7;
                                    break;
                                case 8:
                                    Globals.gift = "Forfait Journée du Sauze";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 8;
                                    break;
                                case 9:
                                    Globals.gift = "Forfait journée Pra Loup";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 9;
                                    break;
                                case 10:
                                    Globals.gift = "Mugs Alpes";
                                    Globals.point_relais = "Equipe #purealpes";
                                    Globals.gift_index = 10;
                                    break;
                                case 11:
                                    Globals.gift = "Sacoche Alpes";
                                    Globals.point_relais = "Equipe #purealpes";
                                    Globals.gift_index = 11;
                                    break;
                                case 12:
                                    Globals.gift = "Skipass journée à Serre-Chevalier";
                                    Globals.point_relais = "Station de Serre-Chevalier";
                                    Globals.gift_index = 12;
                                    break;
                                case 13:
                                    Globals.gift = "Station du Dévoluy";
                                    Globals.point_relais = "Stations Les Orres";
                                    Globals.gift_index = 13;
                                    break;
                                case 14:
                                    Globals.gift = "Tour de Coup Ubaye";
                                    Globals.point_relais = "Ubaye Tourisme";
                                    Globals.gift_index = 14;
                                    break;
                                case 15:
                                    Globals.gift = "Tote bag Dévoluy";
                                    Globals.point_relais = "Station du Dévoluy";
                                    Globals.gift_index = 15;
                                    break;
                            }
                            Globals._originalString = check.originalString;
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Vous devez fermer le fichier horaire.csv avant de continuer");
                }


            }
        }



        public void GetGameRule()
        {
            List<DateTime> lstHeure = _inimanager.getAllHours("INSTANTGAGNANT");
            DateTime now = DateTime.Now;
            bool exist = Globals.lstHoraires.Any(d => d.startDate.Month == now.Month && d.startDate.Day == now.Day && d.startDate.Year == now.Year);
            DateTime start = lstHeure[0];
            DateTime end = lstHeure[1];
            if (exist)
            {
                if (start.Hour <= now.Hour && end.Hour >= now.Hour)
                {
                    if (start.Minute <= now.Minute && end.Minute >= now.Minute)
                    {
                        _instantWinner = true;
                    }
                    else if (end.Hour == now.Hour)
                    {
                        if (end.Minute <= now.Minute)
                        {
                            _instantWinner = false;
                        }
                    }
                    else
                    {
                        _instantWinner = true;
                    }
                }

            }
        }

        private string getFilterName(string fileName)
        {
            if (fileName.ToLower().Contains("sepia"))
                return "SEPIA";
            else if (fileName.ToLower().Contains("onecolor"))
                return "ONECOLOR";
            else if (fileName.ToLower().Contains("blackwhite"))
                return "BLACKWHITE";
            else if (fileName.ToLower().Contains("_color"))
                return "COLOR";

            return "";
        }

        protected virtual bool IsFileinUse(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            Globals._FlagConfig = "client";
            var viewModel = (FiltreManagementViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToConnexionClient.CanExecute(null))
                    viewModel.GoToConnexionClient.Execute(null);
                /*if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }*/
            }
        }

        public void GoToChoiceEvent_Click(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var viewModel3 = (FiltreManagementViewModel)DataContext;
            if (viewModel3.GoToChoiceEvent.CanExecute(null))
                viewModel3.GoToChoiceEvent.Execute(null);
        }

        private void GoToTakePictureButton(Models.Enum.BouttonPosition type)
        {
            /*
            switch (type)
            {
                case Models.Enum.BouttonPosition.hautDroite:
                    DockPanel.SetDock(this.btnPanel, Dock.Top);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                case Models.Enum.BouttonPosition.hautCentre:
                    DockPanel.SetDock(this.btnPanel, Dock.Top);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    break;
                case Models.Enum.BouttonPosition.hautGauche:
                    DockPanel.SetDock(this.btnPanel, Dock.Top);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case Models.Enum.BouttonPosition.basGauche:
                    DockPanel.SetDock(this.btnPanel, Dock.Bottom);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case Models.Enum.BouttonPosition.basCentre:
                    DockPanel.SetDock(this.btnPanel, Dock.Bottom);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    break;
                case Models.Enum.BouttonPosition.basDroite:
                    DockPanel.SetDock(this.btnPanel, Dock.Bottom);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                default:
                    DockPanel.SetDock(this.btnPanel, Dock.Top);
                    this.btnPanel.Orientation = Orientation.Horizontal;
                    this.btnPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
            }
            */
        }
        

    }
}
