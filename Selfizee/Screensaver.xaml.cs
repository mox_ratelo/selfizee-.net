﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for Screensaver.xaml
    /// </summary>
    public partial class Screensaver : UserControl
    {
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdownRefresh;
        private string[] imageFiles = null;
        private int countdownRefresh = 120;
        Managers.INIFileManager _inimanager;
        private int countdown = 0;
        private int _interval = 0;
        private int _refresh = 0;
        private string interval = "";
        private string directory = "";


        public Screensaver()
        {
            InitializeComponent();
             _inimanager = new Managers.INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
            _refresh = Convert.ToInt32(_inimanager.GetSetting("SCREENSAVER", "refreshcoutdown").ToLower());
            interval = _inimanager.GetSetting("SCREENSAVER", "interval").ToLower();
            directory = _inimanager.GetSetting("SCREENSAVER", "Directory").ToLower();
            _interval = Convert.ToInt32(interval);
            if (directory.Equals("default")){
                if(GetFiles(Globals._screesaverAssetFolder,
                  "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif").Length > 0)
                {
                    this.imageFiles = GetFiles(Globals._screesaverAssetFolder,
                  "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif");
                }else
                {
                    this.imageFiles = new string[1];
                    this.imageFiles[0] = Globals.exerDir + "\\Images\\background.jpg";
                }
                   
                
            }
            else
            {
                if (GetFiles(Globals.EventMediaFolder() + "\\Photos",
                  "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif").Length>0){
                    this.imageFiles = GetFiles(Globals.EventMediaFolder() + "\\Photos",
                  "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif");
                }
                else
                {
                    this.imageFiles = new string[1];
                    this.imageFiles[0] = Globals.exerDir + "\\Images\\background.jpg";
                }
                
            }
            
            countdown = (this.imageFiles.Length) * _interval;
            countdownRefresh = _refresh;
            ShowImage(imageFiles[0]);
            LaunchTimerCountdown();
            LaunchTimerCountdownRefresh();
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }
        void LaunchTimerCountdownRefresh()
        {
            timerCountdownRefresh = new DispatcherTimer();
            timerCountdownRefresh.Interval = TimeSpan.FromSeconds(1);
            timerCountdownRefresh.Tick += new EventHandler(timerCountdownRefresh_Tick);
            timerCountdownRefresh.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            
            countdown--;
            int modulo = countdown % _interval;
            if (countdown >= 0 && modulo == 0)
            {
                int finalIndex = countdown / _interval;
                ShowNextImage(finalIndex);
            }
            else if(countdown<0)
            {
                countdown = (this.imageFiles.Length) * _interval;
                ShowNextImage(0);
            }
            
        }

        void timerCountdownRefresh_Tick(object sender, EventArgs e)
        {

            countdownRefresh--;
           
            if (countdownRefresh <=0)
            {
                timerCountdownRefresh.Stop();
                timerCountdown.Stop();
                if (directory.Equals("default"))
                {
                    if (GetFiles(Globals._screesaverAssetFolder,
                      "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif").Length > 0)
                    {
                        this.imageFiles = GetFiles(Globals._screesaverAssetFolder,
                      "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif");
                    }
                    else
                    {
                        this.imageFiles = new string[1];
                        this.imageFiles[0] = Globals.exerDir + "\\Images\\background.jpg";
                    }


                }
                else
                {
                    if (GetFiles(Globals.EventMediaFolder() + "\\Photos",
                      "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif").Length > 0)
                    {
                        this.imageFiles = GetFiles(Globals.EventMediaFolder() + "\\Photos",
                      "*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff;*.gif");
                    }
                    else
                    {
                        this.imageFiles = new string[1];
                        this.imageFiles[0] = Globals.exerDir + "\\Images\\background.jpg";
                    }

                }
                countdown = (this.imageFiles.Length) * _interval;
                ShowImage(imageFiles[0]);
                LaunchTimerCountdown();
                LaunchTimerCountdownRefresh();
            }

        }

        public static string[] GetFiles(string path, string searchPattern)
        {
            string[] patterns = searchPattern.Split(';');
            List<string> files = new List<string>();
            foreach (string filter in patterns)
            {
                Stack<string> dirs = new Stack<string>(20);

                if (!Directory.Exists(path))
                {
                    throw new ArgumentException();
                }
                dirs.Push(path);

                while (dirs.Count > 0)
                {
                    string currentDir = dirs.Pop();
                    string[] subDirs;
                    try
                    {
                        subDirs = Directory.GetDirectories(currentDir);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }

                    try
                    {
                        files.AddRange(Directory.GetFiles(currentDir, filter));
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }
                    foreach (string str in subDirs)
                    {
                        dirs.Push(str);
                    }
                }
            }

            return files.ToArray();
        }

        public void ShowImage(string path)
        {

            ImageBrush imgBrushPlay = new ImageBrush();

            imgBrushPlay.ImageSource = new BitmapImage(new Uri(path, UriKind.Absolute));
            imgBrushPlay.Stretch = Stretch.Uniform;
            var image1 = new BitmapImage();
            image1.BeginInit();
            image1.UriSource = new Uri(path);
            image1.EndInit();
            bg_screen.Background = imgBrushPlay;
        }

        private void ShowNextImage(int index)
        {
            ShowImage(this.imageFiles[index]);
        }


    }

   
}
