﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models
{
    public class PassedData
    {
        public Origin Origin { get; set; }
        public Dictionary<Object, Object> Datas {get;set;}
    }

    public enum Origin
    {
        Accueil,
        BGSelection,
        TakePhoto,
        FiltreSelection,
        Visualisation,
        Thanks
    }
}
