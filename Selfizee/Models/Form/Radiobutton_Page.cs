﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class Radiobutton_Page
    {
        public string Name { get; set; }
        public bool mendatory { get; set; }
        public List<RadiobuttonItem> Radiobuttons_items { get; set; }
        public string Title { get; set; }
    }
}
