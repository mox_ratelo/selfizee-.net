﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class Optin
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string identifiant { get; set; }
        public bool mendatory { get; set; }
        public string Label { get; set; }
    }
}
