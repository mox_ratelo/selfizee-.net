﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Form
{
    public class IniTmpObject
    {
        public string Fenetre { get; set; }
        public string Question { get; set; }
        public string Attribute { get; set; }
        public string AttribueValue { get; set; }
    }
}
