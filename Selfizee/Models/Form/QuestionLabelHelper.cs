﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Selfizee.Models.Form
{
    public class QuestionLabelHelper : DependencyObject
    {
        public static readonly DependencyProperty QuestionLabelProprety = DependencyProperty.RegisterAttached("QuestionLabel", typeof(string), typeof(QuestionLabelHelper), new PropertyMetadata(String.Empty));

        public static void SetQuestionLabel(DependencyObject target, string value)
        {
            target.SetValue(QuestionLabelProprety, value);
        }

        public static string GetQuestionLabel(DependencyObject target)
        {
            return (string)target.GetValue(QuestionLabelProprety);
        }
    }
}
