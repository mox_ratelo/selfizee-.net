﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Form
{
    public class Question
    {
        public string WindowName { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public int Rank { get; set; }
        public bool? IsMandatory { get; set; }
        public List<string>  Items { get; set; }
    }
}
