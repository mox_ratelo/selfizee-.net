﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class Combobox_page
    {
        public string Name { get; set; }
        public List<String> Combobox_items { get; set; }
        public bool mendatory { get; set; }
        public string Title { get; set; }
    }
}
