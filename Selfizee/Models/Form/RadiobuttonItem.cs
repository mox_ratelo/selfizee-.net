﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class RadiobuttonItem
    {
        public string item { get; set; }
        public string value { get; set; }
        public string temp { get; set; }
    }
}
