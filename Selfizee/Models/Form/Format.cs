﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Form
{
    public class Format
    {
        public int posX { get; set; }
        public int posY { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string format { get; set; }
        public int nbShoot { get; set; }
        public int countDown { get; set; }
    }
}
