﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class Page
    {
        public int index { get; set; }
        public bool first_page { get; set; }
        public Title title { get; set; }
        public List<element> lstElements { get; set; }
        public NavigationButtoncs navigation {get;set;}
    }
}
