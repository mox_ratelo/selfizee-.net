﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Documents; 

namespace Selfizee.Models.Form
{
    public class MandatoryQuestionHelper: DependencyObject
    {
        public static readonly DependencyProperty IsMandatoryProprety = DependencyProperty.RegisterAttached("IsMandatory", typeof(bool), typeof(MandatoryQuestionHelper), new PropertyMetadata(false)); 

        public static void SetIsMandatory(DependencyObject target, Boolean value)
        {
            target.SetValue(IsMandatoryProprety, value); 
        }

        public static bool GetIsMandatory(DependencyObject target)
        {
            return (bool)target.GetValue(IsMandatoryProprety); 
        }
    }
}
