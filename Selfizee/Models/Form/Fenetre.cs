﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Form
{
    public class Fenetre
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int Rank { get; set;  }
        public List<Question> Questions { get; set; }
    }
}
