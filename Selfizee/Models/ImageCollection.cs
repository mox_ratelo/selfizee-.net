﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Data;
using System.Collections;
using System.Drawing;

namespace Selfizee.Models
{
    public class ImageCollection : ObservableCollection<Images>
    {
        public ImageCollection() { }

        public ImageCollection(string path) : this(new DirectoryInfo(path)) { }

        public ImageCollection(DirectoryInfo directory)
        {
            _directory = directory;
            Update();
        }

        public string Path
        {
            set
            {
                _directory = new DirectoryInfo(value);
                Update();
            }
            get { return _directory.FullName; }
        }

        public DirectoryInfo Directory
        {
            set
            {
                _directory = value;
                Update();
            }
            get { return _directory; }
        }
        private void Update()
        {
            this.Clear();
            try
            {
                var listjpeg = _directory.GetFiles("*.jepg");
                Array.Sort(listjpeg, new AlphanumComparatorFast());
                foreach (FileInfo f in listjpeg)
                    Add(new Images(f.FullName));

                var listpng = _directory.GetFiles("*.png");
                Array.Sort(listpng, new AlphanumComparatorFast());
                foreach (FileInfo f in listpng)
                    Add(new Images(f.FullName));

                var listjpg = _directory.GetFiles("*.jpg");
                     Array.Sort(listjpg, new AlphanumComparatorFast());
                foreach (FileInfo f in listjpg)
                    Add(new Images(f.FullName));
            }
            catch (DirectoryNotFoundException ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

       

        public ImageCollection Update(ArrayList imagesList)
        {
            ImageCollection toReturn = new ImageCollection();
            this.Clear();
            try
            {
                foreach (FileInfo f in imagesList)
                    toReturn.Add(new Images(f.FullName));
                //foreach (FileInfo f in _directory.GetFiles("*.png"))
                //    Add(new Images(f.FullName));
                //foreach (FileInfo f in _directory.GetFiles("*.jpg"))
                //    Add(new Images(f.FullName));

            }
            catch (DirectoryNotFoundException ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            return toReturn;
        }

        DirectoryInfo _directory;

        internal void Add(Bitmap bitmap)
        {
            throw new NotImplementedException();
        }
    }
}
