﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models
{
    public class WifiModel
    {
        public string WifiName { get; set; }
        public string SignalStrength { get; set; }
        public string IsConnected { get; set; }
        public bool ToConnect { get; set; }
        public bool ToDisconnect { get; set; }

    }
}
