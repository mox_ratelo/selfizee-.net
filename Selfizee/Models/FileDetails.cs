﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models
{
    public class FileDetails
    {
        public DateTime LastModified { get; set; }
        public string   Filename { get; set; }
    }
}
