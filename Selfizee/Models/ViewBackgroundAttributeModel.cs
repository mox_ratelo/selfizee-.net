﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models
{
    public class ViewBackgroundAttributeModel
    {
        public bool IsImage { get; set; }
        public string BackgroundColor { get; set; }
        public bool EnableTitle { get; set; }
        public string Title { get; set; }
        public string TitlePosition { get; set; }
        public string TitleFontSize { get; set; }
        public string TitleFontFamily { get; set; }
        public string TitleFontColor { get; set; }

        public string TitlePositionX {
            get { return GetTitleXPosition(); }
            }

        public string TitlePositionY
        {
            get { return GetTitleYPosition(); }
        }
        
        private string GetTitleXPosition()
        {
            string[] pos; 
            if(!string.IsNullOrEmpty(TitlePosition))
            {
                pos = TitlePosition.Split(',');
                return pos[0]; 
            }
            return ""; 
        }

        private string GetTitleYPosition()
        {
            string[] pos;
            if (!string.IsNullOrEmpty(TitlePosition))
            {
                pos = TitlePosition.Split(',');
                return pos[1];
            }
            return "";
        }

    }
}
