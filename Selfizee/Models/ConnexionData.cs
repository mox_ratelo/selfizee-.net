﻿using System;

namespace Selfizee.Models
{
    public struct GetToken
    {
        public string token;
    }

    class ConnexionData
    {

        public Boolean success { get; set; }
        public GetToken data { get; set; }
    }
}
