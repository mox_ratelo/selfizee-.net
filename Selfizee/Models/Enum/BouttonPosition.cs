﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Enum
{
    public enum BouttonPosition
    {
        basDroite,
        basCentre,
        basGauche,
        hautDroite,
        hautCentre,
        hautGauche
    }
}
