﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models.Enum
{
    public enum TypeFond
    {
        BgGolbal, 
        ForAccueil,
        ForEvent,
        ForChromaKeyChoice,
        ForCadre,
        ForTakephoto,
        ForFiltre,
        ForVisualisation,
        ForNoCamera,
        ForThanks    
    }
}
