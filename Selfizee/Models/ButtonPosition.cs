﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Selfizee.Models
{
    public class ButtonPosition
    {
        public Button Button { get; set; }
        public double Position { get; set; }
    }
}
