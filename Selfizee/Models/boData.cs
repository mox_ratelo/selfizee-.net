﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Models
{
    public struct champ
    {
        public string id;
        public string value;
    }
    public class boData
    {
        public string name_photo { get; set; }
        public string code_logiciel { get; set; }
        public string date_photo { get; set; }
        public string count_print { get; set; }
        public string print_date { get; set; }
        public string filter { get; set; }
        public string type_format { get; set; }
        public string green_screen { get; set; }
        public List<champ> champs {get;set; }
    }
}
