﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using Selfizee.Models.Enum;
using Selfizee.Managers;
using Selfizee.Models;
using EOSDigital.API;
using log4net;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Threading;
using System.Reflection;
using Selfizee.View;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading.Tasks;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for Accueil.xaml
    /// </summary>
    public partial class Accueil : UserControl
    {
        public DispatcherTimer timerCountdown;
        
        private int countdown = 10;
        private string EventIni ="";
        private INIFileManager _inimanager;
        private bool activated = false;
        private bool autoupdate = false;
        private static readonly ILog Log =
              LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        CanonAPI APIHandler;

        public Accueil()
        {
            InitializeComponent();
            countdown = 10;
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            Globals.timerHomeLaunched = false;
            Globals.gswithoutframe = false;
            EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
            Globals.nextcloudPath = Globals.EventMediaFolder() + "\\Photos\\"; //DEBUGGAGE SANS CONNEXION (bug : Photos non enregistrés)
            if (File.Exists(EventIni))
            {
                _inimanager = new INIFileManager(EventIni);
                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("Home");
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("startscreen");
                    comMgr.writeData(Globals.GetEventId());
                }
                if (backgroundAttributes.IsImage)
                {
                    ImageBrush imgBrush = new ImageBrush();
                    var fileName = Globals.LoadBG(TypeFond.ForAccueil);
                    if (!File.Exists(fileName))
                    {
                        fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                    }
                    Bitmap bmp = null;
                    using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
                    {
                        bmp = new Bitmap(img, new System.Drawing.Size(1920, 1080));
                    }
                    imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp);
                    imgBrush.Stretch = Stretch.Fill;
                    Globals._currentChromakeyBG = "";
                    accueilBG.Background = imgBrush;
                }
                Globals.leaveHome = true;
                //Managers.INIFileManager _inimanager = new Managers.INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                var timeout = _inimanager.GetSetting("SCREENSAVER", "timeout").ToLower();
                countdown = Convert.ToInt32(timeout);
                //if (!backgroundAttributes.EnableTitle)
                //    accueilTitle.Visibility = Visibility.Collapsed;

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }
                }
                if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                {
                    if (!Globals.connected)
                    {
                        //getCityName();
                        getNextcloudPath();
                    }
                }
               

                Globals.connected = true;

                var Activated = _inimanager.GetSetting("SCREENSAVER", "activated").ToLower();
                if (!string.IsNullOrEmpty(Activated))
                {
                    activated = Convert.ToInt32(Activated) > 0;
                }
                if (activated)
                {
                    if(!Directory.Exists(Globals._screesaverAssetFolder))
                    {
                        Directory.CreateDirectory(Globals._screesaverAssetFolder);
                    }
                }
               
                
            }
            else
            {
                ImageBrush imgBrush = new ImageBrush();
                var fileName = Globals.exerDir + "\\Images\\background.jpg"; 
                if (!File.Exists(fileName))
                {

                    fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                }
                imgBrush.ImageSource = new BitmapImage(new Uri(fileName, UriKind.Absolute));
                imgBrush.Stretch = Stretch.Fill;
                Globals._currentChromakeyBG = "";
                accueilBG.Background = imgBrush;
            }
        }

        

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();


            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                LoadingPanel.ShowPanel();
                            }));

                        }).ContinueWith(task =>
                        {
                            

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                var viewModel = (AccueilViewModel)DataContext;
                                /*if (viewModel.GoToConnexionClient.CanExecute(null))
                                    viewModel.GoToConnexionClient.Execute(null);*/
                                Globals._FlagConfig = "client";
                                if (Globals.ScreenType == "SPHERIK")
                                {
                                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                        viewModel.GoToClientSumarySpherik.Execute(null);
                                }
                                else if (Globals.ScreenType == "DEFAULT")
                                {
                                    if (viewModel.GoToClientSumary.CanExecute(null))
                                        viewModel.GoToClientSumary.Execute(null);
                                }
                                
                            }));
                        });
                        
                        break;
                    case Key.F2:
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                LoadingPanel.ShowPanel();

                            }));

                        }).ContinueWith(task =>
                        {
                           
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                var viewModel2 = (AccueilViewModel)DataContext;
                                if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                    viewModel2.GoToConnexionConfig.Execute(null);
                                Globals._FlagConfig = "admin";
                                // LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                        //AutoUpdate();
                        break;

                }
            }
            catch (Exception ex)
            {

            }
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            countdown -= 1;
            if (countdown > 0)
            {

            }
            else
            {
                
                timerCountdown.Stop();
                if (Globals.leaveHome && Globals.timerHomeLaunched == false)
                {
                    var viewModel = (AccueilViewModel)DataContext;
                    if (viewModel != null)
                    {
                        if (viewModel.GoToScreensaver.CanExecute(null))
                            viewModel.GoToScreensaver.Execute(null);
                    }
                   
                }
               
            }

        }

        //public static bool CheckUpdate(FtpManager ftpMgr)
        //{
        //    if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
        //    {
        //        string remoteVersion = ftpMgr.GetFtpFileVersion();
        //        var thisApp = Assembly.GetExecutingAssembly();
        //        AssemblyName name = new AssemblyName(thisApp.FullName);
        //        string localVersion = name.Version.ToString();
        //        if (localVersion == remoteVersion)
        //        {
        //            return true;
        //        }
        //        else { return false; }
        //    }
        //    return false;
        //}


        //public void AutoUpdate()
        //{

        //    FtpManager ftpMgr = new FtpManager();
        //    if (!CheckUpdate(ftpMgr))
        //    {
        //        var UpdateW = new UpdateWindow();
        //        UpdateW.ShowDialog();
        //        bool updateResult = UpdateW.FlagDownload;
        //        long filesize = ftpMgr.GetFileSize();
        //        if (!updateResult)
        //        {
        //            this.IsEnabled = true;
        //        }
        //        else
        //        {
        //            this.IsEnabled = false;
        //            autoupdate = true;
        //            Task.Factory.StartNew(() =>
        //            {
        //                this.Dispatcher.BeginInvoke((Action)(() =>
        //                {
        //                    lbl_CurrentDownload.Visibility = Visibility.Visible;
        //                }));
        //            }).ContinueWith(task =>
        //            {
        //                ftpMgr.DownloadFile();
        //                AssemblyManager assmManager = new AssemblyManager();
        //                long remoteFileSize = assmManager.GetLocalFileSize();
        //                this.Dispatcher.BeginInvoke((Action)(() =>
        //                {
        //                    lbl_CurrentDownload.Visibility = Visibility.Hidden;
        //                }));
        //                if (remoteFileSize == filesize)
        //                {
        //                    assmManager.InstallUpdate();
        //                    //System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
        //                    System.Windows.Forms.Application.Restart();
        //                    Environment.Exit(0);
        //                }
        //            });
        //        }
        //    }

        //}


        public static bool CheckUpdate(string remoteVersion)
        {
            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                //string remoteVersion = ftpMgr.GetFtpFileVersion();
                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string localVersion = name.Version.ToString();
                if (localVersion == remoteVersion)
                {
                    return true;
                }
                else { return false; }
            }
            return false;
        }

        //public void AutoUpdate()
        //{
        //    //FtpManager ftpMgr = new FtpManager();
        //    var UpdateW = new UpdateWindow();
        //    if (!CheckUpdate(UpdateW.RemoteVersion))
        //    {
        //        UpdateW.ShowDialog();
        //        bool updateResult = UpdateW.FlagDownload;
                
        //        string urlExe = UpdateW.ExeLink;

        //        if (!updateResult)
        //        {
        //            this.IsEnabled = true;
        //            //vue erreur connexion
        //        }
        //        else
        //        {
        //            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
        //            {
        //                this.IsEnabled = false;
        //                autoupdate = true;
        //                //var proUpdateW = new ProgressUpdateWindow(filesize, ftpFileNamePath, username, password);
        //                var proUpdateW = new ProgressUpdateWindow(urlExe, Globals._defaultUpdateDirectory);
        //                proUpdateW.ShowDialog();
        //            }
        //            else
        //            {
        //                var noConexW = new NoConnexionWindow();
        //                noConexW.ShowDialog();
        //            }
        //        }
        //    }

        //}


        public static string[] GetFiles(string path, string searchPattern)
        {
            string[] patterns = searchPattern.Split(';');
            List<string> files = new List<string>();
            foreach (string filter in patterns)
            {
                Stack<string> dirs = new Stack<string>(20);

                if (!Directory.Exists(path))
                {
                    throw new ArgumentException();
                }
                dirs.Push(path);

                while (dirs.Count > 0)
                {
                    string currentDir = dirs.Pop();
                    string[] subDirs;
                    try
                    {
                        subDirs = Directory.GetDirectories(currentDir);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }

                    try
                    {
                        files.AddRange(Directory.GetFiles(currentDir, filter));
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }
                    foreach (string str in subDirs)
                    {
                        dirs.Push(str);
                    }
                }
            }

            return files.ToArray();
        }

        public void getNextcloudPath()
        {
            string waitingPath = Globals.currDir + "\\Waiting\\";
            string nextCloudPath = Globals.EventMediaFolder() + "\\Photos\\";
            bool result = InternetConnectionManager.checkConnection("upload.selfizee.fr");
            if (result)
            {
                Globals.nextcloudPath = nextCloudPath;
                string[] fileEntries = Directory.GetFiles(waitingPath);
                if(fileEntries.Length > 0)
                {
                    foreach(string file in fileEntries)
                    {
                        string fileName = Path.GetFileName(file);

                        File.Move(file, nextCloudPath + fileName);
                    }
                   
                }
            }
            else
            {
                Globals.nextcloudPath = waitingPath;
            }


            
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }
        public void getCityName()
        {
           
                String address = "";
                WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                using (WebResponse response = request.GetResponse())
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    address = stream.ReadToEnd();
                }

                int pFrom = address.IndexOf("Current IP Address: ") + "Current IP Address: ".Length;
                int pTo = address.LastIndexOf("</body>");

                String result = address.Substring(pFrom, pTo - pFrom);
                GetGeoLocation geoLocate = new GetGeoLocation();
                geoLocate.GetIPGeoLocation(result);
            
           
        }
        public void PlayFlashFile()
        {
            string swfFileName = Directory.GetFiles(Globals._flashFile).First();
            SWFPlayer.FlashPlayer player = new SWFPlayer.FlashPlayer(swfFileName);
            //player.Click += new EventHandler(Player_Click);
            //player.Anchor = System.Windows.Forms.AnchorStyles.Top;
            //Host.Child = player;
           
        }

        public void ShowEndUpdate()
        {
            string dir = "C:\\EVENTS\\Events\\AppConfig.ini";
            INIFileManager _inimanager = new INIFileManager(dir);
            string isUpdate = _inimanager.GetSetting("UPDATE","IsUpdate");
            if (isUpdate.ToLower() == "true")
            {
                var endUpdateW = new EndUpdateWindow();
                endUpdateW.ShowDialog();
                IniUtility _iniUtility = new IniUtility(dir);
                _iniUtility.Write("IsUpdate", "false", "UPDATE");
            }
        }

        public string SendVersionData(VersionData version)
        {
            string responseInString="";
            try
            {
                string url = "https://booth.selfizee.fr/api/setVersionLogiciel";
                using (var wb = new WebClient())
                {
                   var data = new NameValueCollection();
                   data["numero_borne"] = version.numero_borne;
                   data["numero_version"] = version.numero_version;
                   data["date_heure_installation"] = version.date_heure_installation;
                   var response = wb.UploadValues(url, "POST", data);
                                
                   responseInString = Encoding.UTF8.GetString(response);
                }
            }
            catch(Exception e)
            {

            }
            return responseInString;

        }

        public void CheckAndSendVersion()
        {
            try
            {
                string path = "C:\\EVENTS\\Events\\AppConfig.ini";
                            INIFileManager _inimanager = new INIFileManager(path);
                            string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");

                            var thisApp = Assembly.GetExecutingAssembly();
                            AssemblyName name = new AssemblyName(thisApp.FullName);
                            string localVersion = name.Version.ToString();

                            DateTime datenow = DateTime.Now;
                            string sdatenow = datenow.ToString("yyyy-MM-dd HH:mm:ss");

                            VersionData version = new VersionData();
                            version.numero_borne = idborne;
                            version.numero_version = localVersion;
                            version.date_heure_installation = sdatenow;

                            //string dir = "C:\\Events\\My ini\\Config.ini";
                            //Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);

                            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                            {
                                string isSent = _inimanager.GetSetting("SENDVERSION","IsSent");
                                if (isSent.ToLower() == "false")
                                {
                                    string response = SendVersionData(version);
                                    string[] lresponnse = response.Split(',');
                                    string successMsg = lresponnse[0].Split(':')[1];
                                    string Msgsuccess = Regex.Replace(successMsg, "}", "");
                                    if (Msgsuccess == "true")
                                    {
                                        //isSent = "true";
                                        IniUtility _iniUtility = new IniUtility(path);
                                        _iniUtility.Write("IsSent", "true", "SENDVERSION");
                                    }
                                }
                
                            }
            }
            catch(Exception e)
            {

            }
            
        }
        private void WindowLoaded(object sender, EventArgs e)
        {
            this.Focusable = true;
            Keyboard.Focus(this);

            GC.Collect();
            GC.WaitForPendingFinalizers(); 
            var path1 = $"{Globals._tmpDir}\\1.png";
            var path2 = $"{Globals._tmpDir}\\2.png";
            if (File.Exists(path1))File.Delete(path1);               
            if (File.Exists(path2)) File.Delete(path2);
            if(File.Exists(Globals.EventAssetFolder() + "\\Config.ini"))
            {
                INIFileManager _inimanager = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                var flashActivated = _inimanager.GetSetting("HOME", "Animation").ToLower();
                if (flashActivated == "flash")
                {
                    PlayFlashFile();
                }
                else if (flashActivated == "rebound")
                {
                    string rebond = "C:\\EVENTS\\Assets\\" + Globals.GetEventId() + "\\Buttons\\rebound.png";
                    if (File.Exists(rebond))
                    {
                        image.DataContext = rebond;
                    }
                    else
                    {
                        image.DataContext = Globals._defaultbutonAccueil;
                        Log.Error("Fichier rebond introuvable");
                    }
                    

                }
                else
                {
                    string rebond = "C:\\EVENTS\\Assets\\" + Globals.GetEventId() + "\\Buttons\\rebound.png";
                    if (File.Exists(rebond))
                    {
                        imageStatique.DataContext = rebond;
                    }
                    else
                    {
                        imageStatique.DataContext = Globals._defaultbutonAccueil;
                        Log.Error("Fichier rebond introuvable");
                    }
                    
                }
                string dir = "C:\\EVENTS\\Events\\AppConfig.ini";
                INIFileManager _inimanagers = new INIFileManager(dir);
                Globals.ScreenType = _inimanagers.GetSetting("EVENTSCONFIG", "Type");
                //CheckAndSendVersion();
                ShowEndUpdate();
                //AutoUpdate();
                //if (activated && !autoupdate)
                if (activated)
                {
                    LaunchTimerCountdown();
                }
            }
            else
            {
                string rebond = Globals.exerDir + "\\Images\\rebound.png";
                if (File.Exists(rebond))
                {
                    image.DataContext = rebond;
                }
                else
                {
                    image.DataContext = Globals._defaultbutonAccueil;
                    Log.Error("Fichier rebond introuvable");
                }
            }
            

        }

        void UserControl1_KeyDown(object sender, KeyEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Key down");
        }


        private void ShowButton(object sender, RoutedEventArgs e)
        {
          
        }

        private string LoadBG()
        {
            var currDir = Globals.EventFolder();
            var temp = currDir + "\\Assets\\Backgrounds";
            var fileName = currDir + "\\Assets\\Backgrounds\\background_home.jpg";
            if (File.Exists(fileName)){
                return Directory.GetFiles(temp, "background_home.jpg")[0];
            }
            else if(File.Exists(currDir + "\\Assets\\Background\\background.jpg"))
            {
                return currDir + "\\Assets\\Background\\background.jpg";
            }
            else
            {
                return Globals.exerDir + "\\Images\\background.jpg";
            }
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            Globals._FlagConfig = "client";
            var viewModel = (AccueilViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToConnexionClient.CanExecute(null))
                    viewModel.GoToConnexionClient.Execute(null);
                /*if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }*/
            }
        }
    }

    public class GetGeoLocation
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void GetIPGeoLocation(string IP)
        {
            WebClient client = new WebClient();
            // Make an api call and get response.
            try
            {
                string response = client.DownloadString("http://ip-api.com/json/" + IP);
                //Deserialize response JSON
                IPData ipdata = JsonConvert.DeserializeObject<IPData>(response);
                if (ipdata.status == "fail")
                {
                    Log.Error("Invalide adresse IP");
                }
                else
                {
                    Globals.heureConnexion = DateTime.Now.ToString("HH:mm");
                    Log.Info("Connexion à Selfizee " + "Pays: " + ipdata.countryCode + " " + ipdata.country + ", Nom de la ville: " + ipdata.city + " Adresse IP: " + IP);
                }
            }
            catch (Exception)
            {
                throw;
            }
         }
            //return null;
     }
    
    public class IPData
        {
            public string status { get; set; }
            public string country { get; set; }
            public string countryCode { get; set; }
            public string region { get; set; }
            public string regionName { get; set; }
            public string city { get; set; }
            public string zip { get; set; }
            public string lat { get; set; }
            public string lon { get; set; }
            public string timezone { get; set; }
            public string isp { get; set; }
            public string org { get; set; }
            public string @as { get; set; }
            public string query { get; set; }
        }
    public class VersionData
{
    public string numero_borne { get; set; }
    public string numero_version { get; set; }
    public string date_heure_installation { get; set; }
}
}

