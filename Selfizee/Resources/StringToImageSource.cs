﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.IO; 

namespace Selfizee.Resources
{
    [ValueConversion(typeof(String), typeof(ImageSource))]
    public class StringToImageSource : IValueConverter
    {

        private double _decodePixelWidth;
        private double _decodePixelHeight;

        public double DecodePixelWidth
        {
            get
            {
                return _decodePixelWidth;
            }
            set
            {
                _decodePixelWidth = value;
            }
        }

        public double DecodePixelHeight
        {
            get
            {
                return _decodePixelHeight;
            }
            set
            {
                _decodePixelHeight = value;
            }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            object result = null;
            string uri = value as string; 
            if(uri != null)
            {

                //BitmapImage image = new BitmapImage();
                //image.BeginInit();
                //image.CacheOption = BitmapCacheOption.OnLoad;
                //image.UriSource = new Uri(uri);
                //image.EndInit();

                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = new FileStream(uri, FileMode.Open, FileAccess.Read);
                bitmapImage.DecodePixelWidth = (int)_decodePixelWidth;
                bitmapImage.DecodePixelHeight = (int)_decodePixelHeight;
                //load the image now so we can immediately dispose of the stream
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                //clean up the stream to avoid file access exceptions when attempting to delete images
                bitmapImage.StreamSource.Dispose();

                result = bitmapImage;
                return result; 
            }
            return result;  
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException(); 
        }
    }
}
