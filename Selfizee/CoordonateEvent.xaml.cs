﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for CoordonateEvent.xaml
    /// </summary>
    public partial class CoordonateEvent : UserControl
    {
        private List<ModifyEvent> lstCoordonate = new List<ModifyEvent>();
        private string test { get; set; }
        public string code { get; set; }
        public CoordonateEvent()
        {
            InitializeComponent();
            code = Globals.codeEvent_toEdit;
            for (int i = 0; i < 8; i++)
            {
                ModifyEvent mdfEvent = new ModifyEvent();
                mdfEvent.Image = Globals._defaultNoneImage;
                mdfEvent.Date = "12/10/2017 à 14h11";
                mdfEvent.Email = "johndoe@gmail.com";
                mdfEvent.Optin = "OUI";
                mdfEvent.Tel = "06 12 13 14 15";
                lstCoordonate.Add(mdfEvent);
            }
            List_event.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
            Add_event.Foreground = System.Windows.Media.Brushes.White;
            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
            btn_lanchAnimation.Background = btnLaunchAnimBrush;

            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConfigBack, UriKind.Absolute));
            btn_Back.Background = btnback;

            ImageBrush btnbackToEvent = new ImageBrush();
            btnbackToEvent.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnBackToEvent, UriKind.Absolute));
            btn_BackToEvent.Background = btnbackToEvent;

            ImageBrush btnexportData = new ImageBrush();
            btnexportData.ImageSource = new BitmapImage(new Uri(Globals._defaultexportdataconfig, UriKind.Absolute));
            btn_export.Background = btnexportData;

            ImageBrush btnedeleteData = new ImageBrush();
            btnedeleteData.ImageSource = new BitmapImage(new Uri(Globals._defaultdeletedataconfig, UriKind.Absolute));
            btn_delete_data.Background = btnedeleteData;

            string iniPath = Globals._assetsFolder + "\\" + Globals.codeEvent_toEdit + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(iniPath);
            string Name = iniFile.GetSetting("EVENT", "Name");
            eventName.Text = Name;
            codeEvent.Text = $"({code})";

            setCoordonate();
        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void Configuration_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToConfiguration.CanExecute(null))
                viewModel.GoToConfiguration.Execute(null);
        }

        private void BackToEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void setCoordonate()
        {
            Style buttonStyle = this.FindResource("noHighlight") as Style;
            foreach (var coordonate in lstCoordonate)
            {
                Grid detailsLineStack = new Grid();
                detailsLineStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#fafafa"));
                StackPanel mainContainer = new StackPanel();
                mainContainer.Orientation = Orientation.Horizontal;
                mainContainer.HorizontalAlignment = HorizontalAlignment.Left;
                //detailsLineStack.Orientation = Orientation.Horizontal;
                StackPanel imgStack = new StackPanel();
                imgStack.Height = 75;
                System.Windows.Controls.Image imgView = new System.Windows.Controls.Image();
                imgView.Height = 75;
                imgView.Width = 120;
                //imgView.Margin = new Thickness(20);
                Bitmap imageBitmap = new Bitmap(coordonate.Image);

                imgView.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                //imgView = coordonate.Image;
                imgStack.Children.Add(imgView);
                mainContainer.Children.Add(imgStack);
                //detailsLineStack.Children.Add(imgStack);

                StackPanel mailStack = new StackPanel();
                mailStack.Height = 15;
                TextBlock mailTxt = new TextBlock();
                mailTxt.Text = coordonate.Email;
                mailTxt.FontSize = 12;
                mailTxt.FontWeight = FontWeights.Bold;
                //mailTxt.Margin = new Thickness(20);
                mailTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                mailTxt.Height = 15;
                mailTxt.Width = 255;
                mailStack.Children.Add(mailTxt);
                mainContainer.Children.Add(mailStack);
                //detailsLineStack.Children.Add(mailStack);

                StackPanel telStack = new StackPanel();
                telStack.Height = 15;
                TextBlock telTxt = new TextBlock();
                telTxt.Text = coordonate.Tel;
                telTxt.Height = 15;
                telTxt.FontSize = 12;
                //telTxt.Margin = new Thickness(20);
                telTxt.FontWeight = FontWeights.Bold;
                telTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                telTxt.Width = 120;
                telStack.Children.Add(telTxt);
                mainContainer.Children.Add(telStack);
                //detailsLineStack.Children.Add(telStack);

                StackPanel optinStack = new StackPanel();
                optinStack.Height = 15;
                TextBlock optinTxt = new TextBlock();
                optinTxt.Text = coordonate.Optin;
                optinTxt.Height = 15;
                optinTxt.FontSize = 12;
                //optinTxt.Margin = new Thickness(20);
                optinTxt.FontWeight = FontWeights.Bold;
                optinTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                optinTxt.Width = 50;
                optinStack.Children.Add(optinTxt);
                mainContainer.Children.Add(optinStack);
                //detailsLineStack.Children.Add(optinStack);

                StackPanel dateStack = new StackPanel();
                dateStack.Height = 15;
                TextBlock dateTxt = new TextBlock();
                dateTxt.Text = coordonate.Date;
                dateTxt.Height = 15;
                dateTxt.FontSize = 12;
                //dateTxt.Margin = new Thickness(20);
                dateTxt.FontWeight = FontWeights.Bold;
                dateTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                dateTxt.Width = 500;
                dateTxt.HorizontalAlignment = HorizontalAlignment.Left;
                dateStack.Children.Add(dateTxt);
                mainContainer.Children.Add(dateStack);
                detailsLineStack.Children.Add(mainContainer);

                StackPanel btnStack = new StackPanel();
                btnStack.Orientation = Orientation.Horizontal;
                btnStack.Margin = new Thickness(0, 0, 30, 0);
                btnStack.HorizontalAlignment = HorizontalAlignment.Right;
                Button btn_edit = new Button();
                ImageBrush btneditcoordonateBrush = new ImageBrush();
                btneditcoordonateBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtneditcoordonate, UriKind.Absolute));
                btn_edit.Background = btneditcoordonateBrush;
                btn_edit.Height = 15;
                btn_edit.Margin = new Thickness(10);
                btn_edit.Width = 15;
                btn_edit.Style = buttonStyle;
                btnStack.Children.Add(btn_edit);

                Button btn_delete = new Button();
                ImageBrush btndeletecoordonateBrush = new ImageBrush();
                btndeletecoordonateBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtndeletecoordonate, UriKind.Absolute));
                btn_delete.Background = btndeletecoordonateBrush;
                btn_delete.Height = 15;
                btn_delete.Width = 15;
                btn_delete.Margin = new Thickness(10);
                btn_delete.Style = buttonStyle;
                btnStack.Children.Add(btn_delete);
                detailsLineStack.Children.Add(btnStack);

                coordonateStack.Children.Add(detailsLineStack);
                Separator sep = new Separator();
                sep.Height = 3;
                sep.FontSize = 3;
                sep.FontWeight = FontWeights.Bold;
                sep.Background = System.Windows.Media.Brushes.Transparent;
                sep.Foreground = System.Windows.Media.Brushes.Transparent;

                sep.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Transparent); ;
                coordonateStack.Children.Add(sep);
            }
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CoordonateEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
    }

    public class ModifyEvent
    {
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Image { get; set; }
        public string Optin { get; set; }
        public string Date { get; set; }
    }
}
