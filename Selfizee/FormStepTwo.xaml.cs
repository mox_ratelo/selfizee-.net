﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for FormStepTwo.xaml
    /// </summary>
    public partial class FormStepTwo : UserControl
    {
        public FormStepTwo()
        {
            InitializeComponent();
            var bc = new BrushConverter();
            var color = "#ffffff";
            this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(color);
        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtSelectTeam.Visibility = comboBox1.SelectedItem == null ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
