﻿using AForge.Video;
using AForge.Video.DirectShow;
using EOSDigital.API;
using EOSDigital.SDK;
using Selfizee.Manager;
using Selfizee.Managers;
using Selfizee.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for GreenSettings.xaml
    /// </summary>
    public partial class GreenSettings : UserControl
    {
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }
        private FilterInfo _currentDevice;
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        Action<BitmapImage> SetImageAction;
        private ImageCollection _photos;
        ImageBrush bgbrush = new ImageBrush();
        Camera MainCamera;
        private int width = 0;
        private int height = 0;
        private VideoCaptureDevice _videoSource;
        bool IsInit = false;
        Camera currentCamera;
        bool background_changed = false;
        int _strength = 0;
        int _transparency = 0;
        private Bitmap currentGSBackgroound = null;
        private string currentBackground;
        CanonAPI APIHandler;
        public GreenSettings()
        {
            InitializeComponent();
            int defaultStrength = 3;
            background_changed = true;
            int defaulttransparency = 2000;

            string code = Globals.codeEvent_toEdit;
            strength.GotKeyboardFocus += TextBoxGetFocused;
            strength.LostFocus += LostTextBoxKeyboardFocus;
            transparency.GotKeyboardFocus += TextBoxGetFocused;
            transparency.LostFocus += LostTextBoxKeyboardFocus;
            string greenpath = "c:\\Events\\Assets\\" + code + "\\GreenScreen";
            string iniPath = "c:\\Events\\Assets\\" + code + "\\Config.ini";
            INIFileManager _ini = new INIFileManager(iniPath);
            IniUtility __ini = new IniUtility(iniPath);
            int _strengthini = 0;
            int _transparencyini = 0;
            _transparency = 2000;
            try
            {
                _strengthini = Convert.ToInt32(_ini.GetSetting("EVENT", "strength"));
                //_transparencyini = Convert.ToInt32(_ini.GetSetting("EVENT", "transparency"));

                if (_strengthini == 0)
                {
                    __ini.Write("strength", defaultStrength.ToString(), "EVENT");
                    __ini.Write("transparency", defaulttransparency.ToString(), "EVENT");
                    _strength = defaultStrength;
                    //_transparency = defaulttransparency;
                }
                else
                {
                    _strength = _strengthini;
                    //_transparency = _transparencyini;
                }
                strength.Text = _strength.ToString();
                //transparency.Text = _transparency.ToString();
            }
            catch (Exception e)
            {
                __ini.Write("strength", _strength.ToString(), "EVENT");
                __ini.Write("transparency", _transparency.ToString(), "EVENT");
                strength.Text = _strength.ToString();
                //transparency.Text = _transparency.ToString();
            }
            if (Globals.ScreenType == "SPHERIK")
            {
                LVCanvas.Width = 605;
                LVCanvas.Height = 400;
                numeriqueKeyboard.Width = 180;
                numeriqueKeyboard.Height = 180;
            }
            else
            {
                LVCanvas.Width = 905;
                LVCanvas.Height = 600;
                numeriqueKeyboard.Width = 180;
                numeriqueKeyboard.Height = 180;
            }
            _strength = Convert.ToInt32(strength.Text);
            //_transparency = Convert.ToInt32(transparency.Text);
            _transparency = 2000;
            if (Directory.Exists(greenpath))
            {
                currentBackground = Directory.GetFiles(greenpath).FirstOrDefault();
            }
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            if (camera_usb_activated == 1)
            {
                LaunchWebcamCamera();
            }
            else
            {
                LaunchCameraReflex();
            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            var imageFiles = new ArrayList();
            width = Convert.ToInt32(LVCanvas.Width);
            height = Convert.ToInt32(LVCanvas.Height);
            string path = "C:\\Events\\Assets\\" + Globals.codeEvent_toEdit + "\\GreenScreen";
            var files = Directory.GetFiles(path, "*.jpg");
            foreach (var image in files)
            {
                var info = new FileInfo(image);
                imageFiles.Add(info);
            }
            ImageCollection collect = new ImageCollection();
            //_photos = new ImageCollection(_finalFolder);
            _photos = collect.Update(imageFiles);

            ((CollectionViewSource)this.Resources["MyPhotos"]).Source = _photos;
            ((CollectionViewSource)this.Resources["MyPhotos"]).View.MoveCurrentToPosition(-1);
            ((CollectionViewSource)this.Resources["MyPhotos"]).Filter += new FilterEventHandler(view_Filter);

        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _strength = Convert.ToInt32(strength.Text);
                        background_changed = true;
                        _transparency = Convert.ToInt32(transparency.Text);
                        lblInfo.Visibility = Visibility.Visible;

                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        var selectedImage = (sender as ListBox).SelectedItem as Images;
                        currentBackground = selectedImage.Source;
                        StopCamera();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        var selectedImage = (sender as ListBox).SelectedItem as Images;
                        currentBackground = selectedImage.Source;
                        CloseSession();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }



        }

        private void view_Filter(object sender, FilterEventArgs e)
        {


        }

        public void TextBoxGetFocused(object sender, EventArgs e)
        {
            TextBox tt = (TextBox)sender;
            tt.Foreground = System.Windows.Media.Brushes.Black;

            //numeriqueKeyboard.Visibility = Visibility.Visible;
            numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
            numeriqueKeyboard.ActiveContainer = tt;
        }

        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            TextBox tt = (TextBox)sender;
            if (tt.Name == "strength")
            {
                int _strength = Convert.ToInt32(tt.Text);
                if (_strength < 3 || _strength > 30)
                {
                    tt.Text = "18";
                }
            }
            if (tt.Name == "transparency")
            {
                int _strength = Convert.ToInt32(tt.Text);
                if (_strength < 400 || _strength > 2000)
                {
                    tt.Text = "1000";
                }
            }
            numeriqueKeyboard.Visibility = Visibility.Collapsed;

        }

        private void save(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            if (camera_usb_activated == 1)
            {
                StopCamera();
            }
            else
            {
                MainCamera.StopLiveView();
                CloseSession();
            }

            string code = Globals.GetEventId();
            string iniPath = "c:\\Events\\Assets\\" + code + "\\Config.ini";
            IniUtility __ini = new IniUtility(iniPath);
            __ini.Write("strength", strength.Text.ToString(), "EVENT");
            __ini.Write("transparency", "2000", "EVENT");
            var vm = (GreenSettingsViewModel)DataContext;
            if (vm.GoToClientConfigs.CanExecute(null))
                vm.GoToClientConfigs.Execute(null);
        }

        private void refresh(object sender, RoutedEventArgs e)
        {
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //play.Text = "Mettre à jour";
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        StopCamera();

                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        CloseSession();
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }


            //}
            //else
            //{
            //    play.Text = "Arreter";
            //    _strength = Convert.ToInt32(strength.Text);
            //    _transparency = Convert.ToInt32(transparency.Text);

            //    if (camera_usb_activated == 1)
            //    {
            //        _refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //        LaunchWebcamCamera();
            //    }
            //    else
            //    {
            //        LaunchCameraReflex();
            //    }
            //}

            //int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //    play.Text = "Relancer";
            //    if (camera_usb_activated == 1)
            //    {
            //        _refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //        LaunchWebcamCamera();
            //    }
            //    else
            //    {
            //        MainCamera.StopLiveView();
            //        CloseSession();
            //    }


            //}
            //else
            //{
            //    play.Text = "Arreter";
            //    _strength = Convert.ToInt32(strength.Text);
            //    _transparency = Convert.ToInt32(transparency.Text);

            //    if (camera_usb_activated == 1)
            //    {
            //        _refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //        LaunchWebcamCamera();
            //    }
            //    else
            //    {
            //        LaunchCameraReflex();
            //    }
            //}

        }

        private void StopCamera()
        {
            bgbrush.ImageSource = null;
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
                _videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame);
            }
        }
        public void LaunchCameraReflex()
        {
            ErrorHandler.NonSevereErrorHappened += ErrorHandler_NonSevereErrorHappened;
            SetImageAction = (BitmapImage img) => { bgbrush.ImageSource = img; };
            APIHandler = new CanonAPI();
            APIHandler.CameraAdded += APIHandler_CameraAdded;
            IsInit = true;
            RefreshCamera();
            //CloseSession();
            OpenSession();
            startLiveView();
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
        private void StartCamera()
        {
            if (CurrentDevice != null)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    _videoSource = new VideoCaptureDevice(CurrentDevice.MonikerString);
                    _videoSource.VideoResolution = selectResolution(_videoSource);
                    _videoSource.NewFrame += video_NewFrame;
                    _videoSource.Start();
                }));

            }
        }

        private SizeF getSizePaysage(int width, int height, int referenceWidth, int referenceHeight)
        {
            for (float i = 100; i > 0; i--)
            {

                float index = i / 100f;

                SizeF theSize = new SizeF(width * index, height * index);
                if (referenceWidth > theSize.Width && referenceHeight > theSize.Height)
                {
                    return theSize;
                }

            }
            return new SizeF();
        }

        private Bitmap resizeBackground(Bitmap param, int widthresized, int heightresized)
        {
            System.Drawing.Bitmap imageBitmap = param;
            imageBitmap = ImageUtility.ResizeImage(imageBitmap, widthresized, heightresized);
            Aurigma.GraphicsMill.Bitmap _background = new Aurigma.GraphicsMill.Bitmap(imageBitmap);
            return (System.Drawing.Bitmap)_background;
        }

        private void video_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
            using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
            {
                Bitmap b = bitmap;
                int xpos = 0;
                int ypos = 0;
                int currwidth = width;
                int currheight = height;
                int referenceWidth = width;
                int referenceHeight = height;
                if (referenceHeight > b.Height)
                {
                    referenceHeight = b.Height;
                }
                if (referenceWidth > b.Width)
                {
                    referenceWidth = b.Width;
                }
                if (b.Width > 930)
                {
                    SizeF _ref = getSizePaysage(currwidth, currheight, 930, referenceHeight);
                    currheight = Convert.ToInt32(_ref.Height);
                    currwidth = Convert.ToInt32(_ref.Width);
                }
                else
                {
                    SizeF _ref = getSizePaysage(currwidth, currheight, referenceWidth, referenceHeight);
                    currheight = Convert.ToInt32(_ref.Height);
                    currwidth = Convert.ToInt32(_ref.Width);
                }
                xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                ypos = Convert.ToInt32(b.Height - currheight) / 2;
                if (ypos < 0)
                {
                    ypos = 0;
                }
                if (xpos < 0)
                {
                    xpos = 0;
                }
                if (background_changed)
                {
                    background_changed = false;
                    Bitmap bg = new Bitmap(currentBackground);
                    currentGSBackgroound = resizeBackground(bg, currwidth, currheight);
                    bg.Dispose();
                }
                b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                ImageFilterManager imageFilter = new ImageFilterManager();
                if (!Globals.gswithoutframe)
                {
                    b = imageFilter.removeGraphicsMillGreenAsync(b, currentBackground, 0, 0, currentGSBackgroound, width, height);
                    //b = imageFilter.removeGraphicsMillGreen(b, currentBackground, _strength, _transparency);
                }


                b.SetResolution(640, 480);
                var ms = new MemoryStream();
                b.Save(ms, ImageFormat.Bmp);
                b.Dispose();
                using (WrapStream s = new WrapStream(ms))
                {

                    BitmapImage EvfImage = new BitmapImage();
                    EvfImage.BeginInit();
                    EvfImage.StreamSource = s;
                    EvfImage.CacheOption = BitmapCacheOption.None;
                    EvfImage.EndInit();
                    EvfImage.Freeze();

                    Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));

                    //System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();
                }
            }
        }

        private static VideoCapabilities selectResolution(VideoCaptureDevice device)
        {
            VideoCapabilities capTemp = device.VideoCapabilities[0];
            foreach (var cap in device.VideoCapabilities)
            {
                if (cap.FrameSize.Width > capTemp.FrameSize.Width)
                {
                    capTemp = cap;
                }
            }
            return capTemp;
        }

        private void APIHandler_CameraAdded(CanonAPI sender)
        {

            try { Dispatcher.BeginInvoke((Action)(() => { RefreshCamera(); })); }
            catch (Exception ex) { }
        }

        private void ErrorHandler_NonSevereErrorHappened(object sender, ErrorCode ex)
        {

        }
        private void RefreshCamera()
        {
            List<Camera> cameraList = APIHandler.GetCameraList();
            if (cameraList.Count <= 0)
            {
                var vm = (GreenSettingsViewModel)DataContext;
                if (vm.GoToNoCamera.CanExecute(null))
                    vm.GoToNoCamera.Execute(null);
            }
            else
            {
                currentCamera = cameraList[0];
            }

        }

        public void LaunchWebcamCamera()
        {
            background_changed = true;
            GetVideoDevices();
            SetImageAction = (BitmapImage img) => { bgbrush.ImageSource = img; };
            IsInit = true;
            StartCamera();
        }

        private void GetVideoDevices()
        {
            bool found = false;
            string camera_usb = iniAppFile.GetSetting("WEBCAM", "Name");
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (!string.IsNullOrEmpty(camera_usb))
            {
                foreach (var _videoDevice in VideoDevices)
                {
                    if (_videoDevice.Name.ToLower().Equals(camera_usb.ToLower()))
                    {
                        found = true;
                        CurrentDevice = _videoDevice;
                    }
                }
            }
            else
            {

                if (VideoDevices.Any())
                {
                    if (VideoDevices.Count == 1)
                    {
                        found = true;
                        CurrentDevice = VideoDevices[0];
                    }
                    else
                    {
                        foreach (var _videoDevice in VideoDevices)
                        {
                            if (_videoDevice.Name.ToLower().Equals("lenovo easycamera") || _videoDevice.Name.ToLower().Equals("usb camera") || _videoDevice.Name.ToLower().Equals("microsoft camera front") || _videoDevice.Name.ToLower().Equals("logitech brio"))
                            //if (_videoDevice.Name == "HP HD Camera")
                            {
                                found = true;
                                CurrentDevice = _videoDevice;
                            }
                        }
                    }
                }
            }

            if (!found)
            {
                var viewModel = (TakeWebcamCameraViewModel)DataContext;
                if (viewModel.GoToNoCamera.CanExecute(null))
                    viewModel.GoToNoCamera.Execute(null);
                //scrollNoCamera.Visibility = Visibility.Visible;
            }
        }

        private async void MainCamera_LiveViewUpdated(Camera sender, Stream img)
        {
            Bitmap b = new Bitmap(img);
            ImageFilterManager imageFilter = new ImageFilterManager();
            b = imageFilter.removeGraphicsMillGreen(b, currentBackground, _strength, _transparency);
            b.SetResolution(640, 480);
            var ms = new MemoryStream();
            b.Save(ms, ImageFormat.Bmp);
            b.Dispose();
            using (WrapStream s = new WrapStream(ms))
            {
                img.Position = 0;
                BitmapImage EvfImage = new BitmapImage();
                EvfImage.BeginInit();
                EvfImage.StreamSource = s;
                EvfImage.CacheOption = BitmapCacheOption.None;
                EvfImage.EndInit();
                EvfImage.Freeze();
                await Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);

            }
        }

        private void MainCamera_StateChanged(Camera sender, StateEventID eventID, int parameter)
        {
            try { if (eventID == StateEventID.Shutdown && IsInit) { Dispatcher.Invoke((Action)delegate { CloseSession(); }); } }
            catch (Exception ex) { }
        }

        private void startLiveView()
        {
            try
            {
                setCameraSettings();
                if (!MainCamera.IsLiveViewOn)
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        LVCanvas.Background = bgbrush;
                        MainCamera.StartLiveView();
                    }));

                }
                else
                {
                    //MainCamera.StopLiveView();
                    //LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                }
            }
            catch (Exception ex) { } //ex.Message
        }

        private void setCameraSettings()
        {
            MainCamera.SetSetting(PropertyID.Evf_Mode, (int)EvfModeChoice.enable);
            MainCamera.SetSetting(PropertyID.AFMode, (int)AFMode.AIServo);
            MainCamera.SetSetting(PropertyID.SaveTo, (int)SaveTo.Host);
            MainCamera.SetCapacity(20420, int.MaxValue);
        }

        private void OpenSession()
        {
            if (currentCamera != null)
            {
                MainCamera = currentCamera;
                MainCamera.OpenSession();
                MainCamera.LiveViewUpdated += MainCamera_LiveViewUpdated;
                MainCamera.StateChanged += MainCamera_StateChanged;
            }
        }

        private void CloseSession()
        {
            IsInit = false;
            MainCamera?.Dispose();
            APIHandler?.Dispose();
        }

        private void DefaultStrength(object sender, RoutedEventArgs e)
        {
            strength.Text = "3";
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //play.Text = "Mettre à jour";
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _strength = Convert.ToInt32(strength.Text);
                        _transparency = Convert.ToInt32(transparency.Text);
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        StopCamera();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        CloseSession();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }
        }

        private void DefaultTransparency(object sender, RoutedEventArgs e)
        {
            transparency.Text = "1000";
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //play.Text = "Mettre à jour";
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _strength = Convert.ToInt32(strength.Text);
                        _transparency = Convert.ToInt32(transparency.Text);
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        StopCamera();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        CloseSession();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }
        }

        private void _Fermer(object sender, MouseButtonEventArgs e)
        {
            GC.SuppressFinalize(this);
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            if (camera_usb_activated == 1)
            {
                StopCamera();
            }
            else
            {
                MainCamera.StopLiveView();
                CloseSession();
            }
            var vm = (GreenSettingsViewModel)DataContext;
            if (vm.GoToClientConfigs.CanExecute(null))
                vm.GoToClientConfigs.Execute(null);
        }

        private void Transparency_DragStarted(object sender, DragStartedEventArgs e)
        {

        }


        private void Transparency_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //play.Text = "Mettre à jour";
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _strength = Convert.ToInt32(strength.Text);
                        _transparency = Convert.ToInt32(transparency.Text);
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        StopCamera();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        CloseSession();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }

        }

        private void Strength_DragStarted(object sender, DragStartedEventArgs e)
        {

        }


        private void Strength_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            int camera_usb_activated = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            //if (play.Text.ToLower() == "arreter")
            //{
            //play.Text = "Mettre à jour";
            if (camera_usb_activated == 1)
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _strength = Convert.ToInt32(strength.Text);
                        _transparency = Convert.ToInt32(transparency.Text);
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        StopCamera();
                        Thread.Sleep(200);
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchWebcamCamera();
                    }));

                });
                //_refresh.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lblInfo.Visibility = Visibility.Visible;
                        Thread.Sleep(2000);
                    }));
                }).ContinueWith(task =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MainCamera.StopLiveView();
                        CloseSession();
                        lblInfo.Visibility = Visibility.Hidden;
                        LaunchCameraReflex();
                    }));

                });
            }
        }


    }
}
