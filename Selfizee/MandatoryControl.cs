﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Selfizee
{
    public class MandatoryControl
    {
        public string title { get; set; }
        public List<Control> lstControl { get; set; }
        public bool isMandatory { get; set; }
        public string response { get; set; }
        public bool errorAddedOnView { get; set; }
    }
}
