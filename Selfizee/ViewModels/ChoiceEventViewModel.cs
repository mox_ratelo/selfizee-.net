﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Selfizee.Models;

namespace Selfizee
{
    public class ChoiceEventViewModel : BaseViewModel, IPageViewModel
    {
        private PassedData passedData;
        private bool isGreenScreen; 
        private ICommand _goToTakePhoto;
        private ICommand _goToBackgroundOffer;
        private ICommand _goToBackgroundWindow;
        private ICommand _goToWebcamTakePhoto;
        private ICommand _goToNoCamera;
        private ICommand _goToConnexionConfig;
        private ICommand _goToNoEvent;
        public string selectedEvent
        {
            set { SetValue(selectedEventProprety, value); }
            get { return (string)GetValue(selectedEventProprety); }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        public ICommand GoToNoEvent
        {
            get
            {
                return _goToNoEvent ?? (_goToNoEvent = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToNoEvent", "");
                }));
            }
        }

        public ICommand GoToTakePhoto
        {
            get
            {
                return _goToTakePhoto ?? (_goToTakePhoto = new RelayCommand(x =>
                {
                    isGreenScreen = CheckIfGreenScreen(); 
                    if(isGreenScreen)
                        Mediator.Notify("GoToBGChromaKey", selectedEvent);
                    if(!isGreenScreen)
                    {
                        Mediator.Notify("GoToTakePhoto", selectedEvent);
                    }

                }));
            }
        }

        public ICommand GoToTakePhotoWebcam
        {
            get
            {
                return _goToWebcamTakePhoto ?? (_goToWebcamTakePhoto = new RelayCommand(x =>
                {
                    isGreenScreen = CheckIfGreenScreen();
                    if (isGreenScreen)
                        Mediator.Notify("GoToBGChromaKey", selectedEvent);
                    if (!isGreenScreen)
                    {
                        Mediator.Notify("GoToTakeWebCam", selectedEvent);
                    }

                }));
            }
        }

        

        //public ICommand GoToBackgroundWindow
        //{
        //    get
        //    {
        //        return _goToBackgroundWindow ?? (_goToBackgroundWindow = new RelayCommand(x =>
        //        {
        //            isGreenScreen = CheckIfGreenScreen();
        //            if (isGreenScreen)
        //                Mediator.Notify("GoToBGChromaKey", selectedEvent);
        //            if (!isGreenScreen)
        //            {
        //                Mediator.Notify("GoToTakePhoto", selectedEvent);
        //            }

        //        }));
        //    }
        //}

        public ICommand GoToBackgroundWindow
        {
            get
            {
                return _goToBackgroundWindow ?? (_goToBackgroundWindow = new RelayCommand(x =>
                        Mediator.Notify("GoToBackgroundWindow", selectedEvent)
                ));
            }
        }
        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        public ICommand GoToBackgroundOffer
        {
            get
            {
                return _goToBackgroundOffer ?? (_goToBackgroundOffer = new RelayCommand(x =>
                        Mediator.Notify("GoToBackgroundOffer", selectedEvent)
                ));
            }
        }

        public ICommand GoToNoCamera
        {
            get
            {
                return _goToNoCamera ?? (_goToNoCamera = new RelayCommand(x =>
                        Mediator.Notify("GoToNoCamera", "")
                ));
            }
        }

        private static DependencyProperty selectedEventProprety;

        static ChoiceEventViewModel()
        {
            FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata();
            metadata.Journal = true;
            selectedEventProprety = DependencyProperty.Register(
            "selectedEventProprety", typeof(string),
            typeof(ChoiceEventViewModel), metadata, null);
        }

        private bool CheckIfGreenScreen()
        {
            var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            //var value = inimanager.GetSetting("EVENT", "GreenScreen");
            //int _value = 0;
            //if (int.TryParse(value, out _value))
            //    _value = Convert.ToInt32(value);
            //return _value != 0; 
            return inimanager.CheckIfGreenScreen(); 
        }
    }
}
