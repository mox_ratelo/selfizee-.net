﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class AppareilManquantViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToChoiceEvent;
        private ICommand _goToConnexionConfig;

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        public ICommand GoToChoiceEvent
        {
            get
            {
                return _goToChoiceEvent ?? (_goToChoiceEvent = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToChoiceEvent", "");
                }));
            }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }


    }
}
