﻿using System;
using System.Collections.Generic;
using Selfizee.Models;
using System.Windows.Input;
using System.Windows;

namespace Selfizee
{
    public class BackgroundWindowViewModel : BaseViewModel, IPageViewModel
    {
        private PassedData passedData;
        private bool isGreenScreen;
        private ICommand _goToTakePhoto;
        private ICommand _goToTakeWebcam;
        private ICommand _goToConnexionConfig;

        public string selectedBackground
        {
            set { SetValue(selectedBGProprety, value); }
            get { return (string)GetValue(selectedBGProprety); }
        }

        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }
        public ICommand GoToTakePhoto
        {
            get
            {
                return _goToTakePhoto ?? (_goToTakePhoto = new RelayCommand(x =>
                {
                    isGreenScreen = CheckIfGreenScreen();
                    if (isGreenScreen)
                        if (Globals.gswithoutframe)
                        {
                            Mediator.Notify("GoToTakePhoto", selectedBackground);
                        }
                        else
                        {
                            Mediator.Notify("GoToBGChromaKey", selectedBackground);
                        }
                       
                    if (!isGreenScreen)
                    {
                        Globals.greenScreen = "";
                        Mediator.Notify("GoToTakePhoto", selectedBackground);
                    }
                }));
            }
        }

        public ICommand GoToTakeWecam
        {
            get
            {
                return _goToTakeWebcam ?? (_goToTakeWebcam = new RelayCommand(x =>
                {
                    isGreenScreen = CheckIfGreenScreen();
                    if (isGreenScreen)
                        if (Globals.gswithoutframe)
                        {
                            Mediator.Notify("GoToTakeWebCam", selectedBackground);
                        }
                        else
                        {
                            Mediator.Notify("GoToBGChromaKey", selectedBackground);
                        }

                    if (!isGreenScreen)
                    {
                        Globals.greenScreen = "";
                        Mediator.Notify("GoToTakeWebCam", selectedBackground);
                    }
                }));
            }
        }


        private static DependencyProperty selectedBGProprety;

        static BackgroundWindowViewModel()
        {
            FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata();
            metadata.Journal = true;
            selectedBGProprety = DependencyProperty.Register(
            "selectedBGProprety", typeof(string),
            typeof(BackgroundWindowViewModel), metadata, null);
        }

        private bool CheckIfGreenScreen()
        {
            var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            Globals.gswithoutframe = inimanager.CheckIfGSNoFrame();
            return inimanager.CheckIfGreenScreen();
        }
    }
}
