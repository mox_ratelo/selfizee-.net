﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class ConfigPhotosDetailsViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToEditEvent;
        private ICommand _goToAccueil;
        private ICommand _goToPhotos;
        private ICommand _goToParametre;
        private ICommand _goToConfiguration;
        private ICommand _goToWifiList;
        private ICommand _goToAdminSumarySpherik;
        public ICommand GoToAdminSumarySpherik
        {
            get
            {
                return _goToAdminSumarySpherik ?? (_goToAdminSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToConfiguration
        {
            get
            {
                return _goToConfiguration ?? (_goToConfiguration = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConfiguration", "");
                }));
            }
        }

        private ICommand _goToAdminSumary;
        public ICommand GoToAdminSumary
        {
            get
            {
                return _goToAdminSumary ?? (_goToAdminSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumary", "");
                }));
            }
        }
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        public ICommand GoToPhotos
        {
            get
            {
                return _goToPhotos ?? (_goToPhotos = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToPhotos", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToEditEvent
        {
            get
            {
                return _goToEditEvent ?? (_goToEditEvent = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToEditEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}
