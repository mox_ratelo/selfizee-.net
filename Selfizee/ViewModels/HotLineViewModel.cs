﻿using System;
using System.Windows.Input;

namespace Selfizee
{
    public class HotLineViewModel :  BaseViewModel, IPageViewModel
    {
        private ICommand _goToVisualization;
        private ICommand _goToConnexionConfig;

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        public ICommand GoToVisualisation
        {
            get
            {
                return _goToVisualization ?? (_goToVisualization = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToVisualisation", "");
                }));
            }
        }
    }
}
