﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class WifiViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToDownload;
        private ICommand _goToConfiguration;
        private ICommand _goToListEvent;
        private ICommand _goToParametre;
        private ICommand _goToWifiList;
        private ICommand _goToClientSumary;
        private ICommand _goToAccueil;
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToConfiguration
        {
            get
            {
                return _goToConfiguration ?? (_goToConfiguration = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConfiguration", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }

        private ICommand _goToAdminSumary;
        public ICommand GoToAdminSumary
        {
            get
            {
                return _goToAdminSumary ?? (_goToAdminSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumary", "");
                }));
            }
        }
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
    }
}
