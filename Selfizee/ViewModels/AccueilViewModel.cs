﻿using System.Windows.Input; 

namespace Selfizee
{
    public class AccueilViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToScreensaver;
        private ICommand _goToBGChromaKey; 
        private ICommand _goToChoiceEvent;
        private ICommand _goToConnexionConfig;

        public ICommand GoToChoiceEvent
        {
            get
            {
                return _goToChoiceEvent ?? (_goToChoiceEvent = new RelayCommand(x =>
                {
                  //Mediator.Notify("GoToTakePhoto", "");
                   Mediator.Notify("GoToChoiceEvent", "");
                }));
            }
        }
        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }


        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        public ICommand GoToScreensaver
        {
            get
            {
                return _goToScreensaver ?? (_goToScreensaver = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToScreensaver", "");
                }));
            }
        }

        public ICommand GoToBGChromaKey
        {
            get
            {
                return _goToBGChromaKey ?? (_goToBGChromaKey = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToBGChromaKey", "");

                    // Mediator.Notify("GoToFiltre", ""); 
                }));
            }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }
    }
}
