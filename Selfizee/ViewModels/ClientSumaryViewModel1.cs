﻿using System.Windows.Input;

namespace Selfizee
{
    public class ClientSumaryViewModel1: BaseViewModel, IPageViewModel
    {
        string bidon;
        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }

        private ICommand _goToClientConfigs;
        public ICommand GoToClientConfig
        {
            get
            {
                return _goToClientConfigs ?? (_goToClientConfigs = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientConfigs", "");
                }));
            }
        }

        private ICommand _goToWifiList;
        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }

        private ICommand _goToClientPhotosList;
        public ICommand GoToClientPhotosList
        {
            get
            {
                return _goToClientPhotosList ?? (_goToClientPhotosList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientPhotosList", "");
                }));
            }
        }

        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        private ICommand _goToConnexionConfig;
        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

    }
}
