﻿using System;
using System.Windows.Input;

namespace Selfizee
{
    public class ScreensaverViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _gotoAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _gotoAccueil ?? (_gotoAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
    }
}
