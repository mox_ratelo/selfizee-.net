﻿using System.Windows.Input; 

namespace Selfizee
{
    public class TakePhotoViewModel : BaseViewModel , IPageViewModel
    {
        private ICommand _goToFiltre;
        private ICommand _goToConnexionConfig;

        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }
        public ICommand GoToFiltre
        {
            get
            {
                return _goToFiltre ?? (_goToFiltre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToFiltre", "");
                }));
            }
        }
        public string selectedEvent { get; set; }
        public string selectedBGCromaKey { get; set; }
        public string selectedBackground { get; set; }
    }
}
