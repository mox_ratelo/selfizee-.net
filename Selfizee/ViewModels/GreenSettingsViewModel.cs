﻿using System.Windows.Input;

namespace Selfizee
{
    public class GreenSettingsViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToBGChromakey;
        public ICommand GoToBGChromakey
        {
            get
            {
                return _goToBGChromakey ?? (_goToBGChromakey = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToBGChromaKey", "");
                }));
            }
        }
        private ICommand _goToNoCamera;
        public ICommand GoToNoCamera
        {
            get
            {
                return _goToNoCamera ?? (_goToNoCamera = new RelayCommand(x =>
                        Mediator.Notify("GoToNoCamera", "")
                ));
            }
        }

        private ICommand _goClientConfig;
        public ICommand GoToClientConfigs
        {
            get
            {
                return _goClientConfig ?? (_goClientConfig = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientConfigs", "");
                }));
            }
        }
    }
}
