﻿using System.Windows.Input;

namespace Selfizee
{
    public class ClientConfigsViewModel: BaseViewModel, IPageViewModel
    {
        string bidon;
        private ICommand _goToClientConfigs;
        public ICommand GoToClientConfigs
        {
            get
            {
                return _goToClientConfigs ?? (_goToClientConfigs = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientConfigs", "");
                }));
            }
        }
        private ICommand _goToSettingGreen;
        public ICommand GoToSettingGreen
        {
            get
            {
                return _goToSettingGreen ?? (_goToSettingGreen = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToGreenSettings", "");
                }));
            }
        }
        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        private ICommand _goToAdminSumary;
        public ICommand GoToAdminSumary
        {
            get
            {
                return _goToAdminSumary ?? (_goToAdminSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumary", "");
                }));
            }
        }
        private ICommand _goToAdminSumarySpherik;
        public ICommand GoToAdminSumarySpherik
        {
            get
            {
                return _goToAdminSumarySpherik ?? (_goToAdminSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumarySpherik", "");
                }));
            }
        }
    }
}
