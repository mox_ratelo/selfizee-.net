﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Selfizee
{
    public class ImageItemViewModel : BaseViewModel
    {
        private bool _ischecked;
        private bool _IsEnabled;
        public BitmapImage ImageItem { get; set; }
        public string ImagePath { get; set; }

        public int ImgWidth { get; set; }
        public int ImgHeight { get; set; }

        public bool IsChecked
        {
            get { return _ischecked; }
            set
            {
                _ischecked = value;
                OnPropertyChanged("IsChecked");
            }
        }
        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set
            {
                _IsEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }
    }
}
