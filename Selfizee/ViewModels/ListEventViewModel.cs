﻿using System.Windows.Input;

namespace Selfizee
{
    public class ListEventViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToEditEvent;
        private ICommand _goToDownload;
        private ICommand _goToAccueil;
        private ICommand _goToParametre;
        private ICommand _goToListEvent;
        private ICommand _goToWifiList;

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        private ICommand _goToAdminSumary;
        public ICommand GoToAdminSumary
        {
            get
            {
                return _goToAdminSumary ?? (_goToAdminSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumary", "");
                }));
            }
        }
        private ICommand _goToAdminSumarySpherik;
        public ICommand GoToAdminSumarySpherik
        {
            get
            {
                return _goToAdminSumarySpherik ?? (_goToAdminSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        public ICommand GoToEditEvent
        {
            get
            {
                return _goToEditEvent ?? (_goToEditEvent = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToEditEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}
