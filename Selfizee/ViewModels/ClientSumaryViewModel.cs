﻿using System.Windows.Input;

namespace Selfizee.ViewModels
{
    public class ClientSumaryViewModel: BaseViewModel, IPageViewModel
    {
        string bidon;
        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
    }
}
