﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class ChoiceDownloadViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToDownloadDetail;
        private ICommand _goToRunDownload;
        private ICommand _goToListEvent;

        public ICommand GoToDownloadDetail
        {
            get
            {
                return _goToDownloadDetail ?? (_goToDownloadDetail = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToDownloadDetail", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToRunDownload ?? (_goToRunDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }
    }
}
