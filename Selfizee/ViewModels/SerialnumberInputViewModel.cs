﻿using System.Windows.Input;

namespace Selfizee 
{
    public class SerialnumberInputViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _gotoAccueil;

        public ICommand GoToAcceuil
        {
            get
            {
                return _gotoAccueil ?? (_gotoAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAcceuil", "");
                }));
            }
        }
    }
}
