﻿using System;
using System.Collections.Generic;
using System.Linq;
using Selfizee.Models;
using Selfizee.ViewModels;

namespace Selfizee
{
    public class MainWindowViewModel : BaseViewModel
    {
        private IPageViewModel _currentPageViewModel;
        private List<IPageViewModel> _pageViewModels;

        private string _selectedPath;
        private string selectedEvent;
        private string selectedChromaKeyBG;
        private string filtredImage; 

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (_pageViewModels == null)
                    _pageViewModels = new List<IPageViewModel>();

                return _pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return _currentPageViewModel;
            }
            set
            {
                _currentPageViewModel = value;
                OnPropertyChanged("CurrentPageViewModel");
            }
        }

        private void ChangeViewModel(IPageViewModel viewModel)
        {
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);
            try
            {
                CurrentPageViewModel = PageViewModels
                    .FirstOrDefault(vm => vm == viewModel);
            }
            catch(Exception e)
            {

            }
        }

        private void OnGoToChoiceEvent(object obj)
        {
            //ChangeViewModel(PageViewModels[8]);
            ChangeViewModel(PageViewModels[1]);
            if (_currentPageViewModel.GetType() == typeof(ChoiceEventViewModel))
            {
                var vm = (ChoiceEventViewModel)_currentPageViewModel;
                vm.selectedEvent = obj.ToString();
            }
        }

        private void OnGoToBackgroundWindow(object obj)
        {
            ChangeViewModel(PageViewModels[3]);
            if (_currentPageViewModel.GetType() == typeof(BackgroundWindowViewModel))
            {
                var vm = (BackgroundWindowViewModel)_currentPageViewModel;
                //vm.selectedBackground = obj.ToString();
            }
            //selectedEvent = obj.ToString();
        }

        private void OnGoToTakePhoto(object obj)
        {
            var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            bool isGreenScreen = inimanager.CheckIfGreenScreen();

            ChangeViewModel(PageViewModels[5]);
            var vm = (TakePhotoViewModel)_currentPageViewModel;
            //if (!Globals.gswithoutframe)
            //{
            //    selectedChromaKeyBG = isGreenScreen ? obj.ToString() : "";
            //    vm.selectedBGCromaKey = selectedChromaKeyBG;
            //}
           
            
            vm.selectedEvent = selectedEvent;
            
        }

        private void OnGoToNoCamera(object obj)
        {
            ChangeViewModel(PageViewModels[2]);
        }

        private void OnGoToBackground(object obj)
        {
            ChangeViewModel(PageViewModels[0]);
        }

        private void OnGoToFiltre(object obj)
        {         
            ChangeViewModel(PageViewModels[6]);
            var vm = (FiltreManagementViewModel)_currentPageViewModel;
            if (obj is string && obj.ToString() != "")
            {
                if (_currentPageViewModel.GetType() == typeof(FiltreManagementViewModel))
                {                 
                    vm.cmdOrigin = obj.ToString();
                }
            }

            if(obj is PassedData)
            {
                var passedData = (PassedData)obj;
                if (passedData.Origin == Origin.BGSelection)
                {
                    var data = passedData.Datas;
                    if (data.ContainsKey("selectedBG"))
                    {
                        vm.selectedBG = data.FirstOrDefault(s => s.Key.ToString() == "selectedBG").Value.ToString();
                    }
                        
                }
            }
        }

        private void OnGoToVisualisation(object obj)
        {
            ChangeViewModel(PageViewModels[7]);
            if(_currentPageViewModel.GetType() == typeof(VisualisationViewModel))
            {
                var vm = (VisualisationViewModel)_currentPageViewModel;
                vm.selectedFilter = obj.ToString(); 
            }
            filtredImage = obj.ToString(); 
        }

        private void OnGoToThanks(object obj)
        {
            ChangeViewModel(PageViewModels[8]);
            
        }      

        private void OnGoToAccueil(object obj)
        {
            ChangeViewModel(PageViewModels[0]);         
        }

        private void OnGoToBGChromaKey(object obj)
        {
            ChangeViewModel(PageViewModels[4]);
            //selectedEvent = obj.ToString(); 
        }

       
        private void OnGoToFormStepOne(object obj)
        {
            ChangeViewModel(PageViewModels[9]); 
        }
        

        private void OnBackToVisualisation(object obj)
        {
            ChangeViewModel(PageViewModels[7]);
            if (_currentPageViewModel.GetType() == typeof(VisualisationViewModel))
            {
                var vm = (VisualisationViewModel)_currentPageViewModel;
                vm.selectedFilter =filtredImage;
            }
        }

        private void OnBackToStepOne(object obj)
        {
            ChangeViewModel(PageViewModels[0]);
        }

        private void OnGoToStepTwo(object obj)
        {
            ChangeViewModel(PageViewModels[10]);
        }

        private void OnGoToHotline(object obj)
        {
            ChangeViewModel(PageViewModels[11]);
        }

        private void OnGoToScreensaver(object obj)
        {
            ChangeViewModel(PageViewModels[12]);
        }

        private void OnGoToSerialNumber(object obj)
        {
            ChangeViewModel(PageViewModels[13]);
        }

        private void OnGoToConnexionConfig(object obj)
        {
            ChangeViewModel(PageViewModels[14]);
        }

        private void OnGoToListEvent(object obj)
        {
            ChangeViewModel(PageViewModels[15]);
        }

        private void OnGoToEditEvent(object obj)
        {
            ChangeViewModel(PageViewModels[16]);
        }

        private void OnGoToCoordonate(object obj)
        {
            ChangeViewModel(PageViewModels[17]);
        }

        private void OnGoToPhotos(object obj)
        {
            ChangeViewModel(PageViewModels[18]);
        }

        private void OnGoToDownload(object obj)
        {
            ChangeViewModel(PageViewModels[19]);
        }

        private void OnGoToDownloadDetails(object obj)
        {
            ChangeViewModel(PageViewModels[20]);
        }

        private void OnGoToTakeWebcam(object obj)
        {
            ChangeViewModel(PageViewModels[21]);
        }

        private void OnGoToChoiceDownload(object obj)
        {
            ChangeViewModel(PageViewModels[22]);
        }

        private void OnGoToNoUSBCamera(object obj)
        {
            ChangeViewModel(PageViewModels[23]);
        }

        private void OnGoToNoEvent(object obj)
        {
            ChangeViewModel(PageViewModels[24]);
        }

        private void OnGoToConfiguration(object obj)
        {
            ChangeViewModel(PageViewModels[25]);
        }

        private void OnGoToParametre(object obj)
        {
            ChangeViewModel(PageViewModels[26]);
        }

        private void OnGoToHistorique(object obj)
        {
            ChangeViewModel(PageViewModels[27]);
        }

        private void OnGoToWinnerPage(object obj)
        {
            ChangeViewModel(PageViewModels[28]);
        }

        private void OnGoToWifiList(object obj)
        {
            ChangeViewModel(PageViewModels[29]);
        }

        private void OnGoToClientSumary(object obj)
        {
            ChangeViewModel(PageViewModels[30]);
        }
        private void OnGoToClientConfigs(object obj)
        {
            ChangeViewModel(PageViewModels[31]);
        }
        private void OnGoToClientPhotosList(object obj)
        {
            ChangeViewModel(PageViewModels[32]);
        }

        private void OnGoToAdminSumary(object obj)
        {
            ChangeViewModel(PageViewModels[33]);
        }
        private void OnGoToPostInstall(object obj)
        {
            ChangeViewModel(PageViewModels[34]);
        }
        private void OnGoToClientSumarySpherik(object obj)
        {
            ChangeViewModel(PageViewModels[35]);
        }
        private void OnGoToAdminSumarySpherik(object obj)
        {
            ChangeViewModel(PageViewModels[36]);
        }
        private void OnGoToSettingGreen(object obj)
        {
            ChangeViewModel(PageViewModels[37]);
        }

        private void OnGoToBackgroundOffer(object obj)
        {
            ChangeViewModel(PageViewModels[38]);
        }

        private void OnGoToConnexionClient(object obj)
        {
            ChangeViewModel(PageViewModels[39]);
        }
        public MainWindowViewModel()
        {
            // Add available pages and set page
            //PageViewModels.Add(new SplashScreenWindowViewModel()); 
            //PageViewModels.Add(new BackgroundManagerViewModel());
            //PageViewModels.Add(new SnapTchatViewModel());
            PageViewModels.Add(new AccueilViewModel()); //0
            PageViewModels.Add(new ChoiceEventViewModel()); //1
            PageViewModels.Add(new AppareilManquantViewModel()); //2
            PageViewModels.Add(new BackgroundWindowViewModel()); //3
            PageViewModels.Add(new BGChromaKeyViewmodel()); //4         
            PageViewModels.Add(new TakePhotoViewModel()); //5
            PageViewModels.Add(new FiltreManagementViewModel()); //6
            PageViewModels.Add(new VisualisationViewModel()); //7
            PageViewModels.Add(new ThanksViewModel()); //8
            PageViewModels.Add(new FormStepOneViewmodel()); //9
            PageViewModels.Add(new FormStepTwoViewModel()); //10
            PageViewModels.Add(new HotLineViewModel()); //11
            PageViewModels.Add(new ScreensaverViewModel()); //12
            PageViewModels.Add(new SerialnumberInputViewModel()); //13
            PageViewModels.Add(new ConnexionConfigViewModel()); //14
            PageViewModels.Add(new ListEventViewModel()); //15
            PageViewModels.Add(new EditEventViewModel()); //16
            PageViewModels.Add(new CoordonateEventViewModel()); //17
            PageViewModels.Add(new ConfigPhotosDetailsViewModel()); //18
            PageViewModels.Add(new RunDownloadViewModel()); //19
            PageViewModels.Add(new DownloadDetailsViewModel()); //20
            PageViewModels.Add(new TakeWebcamCameraViewModel()); //21
            PageViewModels.Add(new ChoiceDownloadViewModel()); //22
            PageViewModels.Add(new USBCameraManquantViewModel()); //23
            PageViewModels.Add(new NoEventViewModel()); //24
            PageViewModels.Add(new ConfigurationViewModel()); //25
            PageViewModels.Add(new ParametreViewModel()); //26
            PageViewModels.Add(new HistoriqueEventViewModel()); //27
            PageViewModels.Add(new WinnerPageViewModel()); //28
            PageViewModels.Add(new WifiViewModel()); //29
            PageViewModels.Add(new ClientSumaryViewModel1()); //30
            PageViewModels.Add(new ClientConfigsViewModel()); //31
            PageViewModels.Add(new ClientPhotoListViewModel()); //32
            PageViewModels.Add(new AdminSumaryViewModel()); //33
            PageViewModels.Add(new PostInstallViewModel()); //34
            PageViewModels.Add(new ClientSumarySpherikViewModel()); //35
            PageViewModels.Add(new AdminSumarySpherikViewModel()); //36
            PageViewModels.Add(new GreenSettingsViewModel());//37
            PageViewModels.Add(new BackgroundOfferViewModel());//38
            PageViewModels.Add(new ConnexionClientViewModel());//39


            CurrentPageViewModel = PageViewModels[0];

            Mediator.Subscribe("GoToChoiceEvent", OnGoToChoiceEvent);
            Mediator.Subscribe("GoToNoCamera", OnGoToNoCamera);
            Mediator.Subscribe("GoToBackgroundWindow", OnGoToBackgroundWindow);
            Mediator.Subscribe("GoToTakePhoto", OnGoToTakePhoto); 
            Mediator.Subscribe("GoToFiltre", OnGoToFiltre);
            Mediator.Subscribe("GoToThanks", OnGoToThanks);
            Mediator.Subscribe("GoToAccueil", OnGoToAccueil);
            Mediator.Subscribe("GoToVisualisation", OnGoToVisualisation);
            Mediator.Subscribe("GoToBGChromaKey", OnGoToBGChromaKey);
            Mediator.Subscribe("BackToVisualisation", OnBackToVisualisation);
            Mediator.Subscribe("BackToStepOne", OnBackToStepOne);
            Mediator.Subscribe("GoToStepTwo", OnGoToStepTwo);
            Mediator.Subscribe("GoToFormStepOne", OnGoToFormStepOne);
            Mediator.Subscribe("GoToHotline", OnGoToHotline);
            Mediator.Subscribe("GoToScreensaver", OnGoToScreensaver);
            Mediator.Subscribe("GoToSerialnumber", OnGoToSerialNumber);
            Mediator.Subscribe("GoToConnexionConfig", OnGoToConnexionConfig);
            Mediator.Subscribe("GoToListEvent", OnGoToListEvent);
            Mediator.Subscribe("GoToEditEvent", OnGoToEditEvent);
            Mediator.Subscribe("GoToCoordonate", OnGoToCoordonate);
            Mediator.Subscribe("GoToPhotos", OnGoToPhotos);
            Mediator.Subscribe("GoToDownload", OnGoToDownload);
            Mediator.Subscribe("GoToDownloadDetail", OnGoToDownloadDetails);
            Mediator.Subscribe("GoToTakeWebCam", OnGoToTakeWebcam);
            Mediator.Subscribe("GoToChoiceDownload", OnGoToChoiceDownload);
            Mediator.Subscribe("GoToNoUSBCamera", OnGoToNoUSBCamera);
            Mediator.Subscribe("GoToNoEvent", OnGoToNoEvent);
            Mediator.Subscribe("GoToConfiguration", OnGoToConfiguration);
            Mediator.Subscribe("GoToParametre", OnGoToParametre);
            Mediator.Subscribe("GoToHistorique", OnGoToHistorique);
            Mediator.Subscribe("GoToWinnerPage", OnGoToWinnerPage);
            Mediator.Subscribe("GoToWifiList", OnGoToWifiList);
            Mediator.Subscribe("GoToClientSumary", OnGoToClientSumary);
            Mediator.Subscribe("GoToClientConfigs", OnGoToClientConfigs);
            Mediator.Subscribe("GoToClientPhotosList", OnGoToClientPhotosList);
            Mediator.Subscribe("GoToAdminSumary", OnGoToAdminSumary);
            Mediator.Subscribe("GoToPostInstall", OnGoToPostInstall);
            Mediator.Subscribe("GoToClientSumarySpherik", OnGoToClientSumarySpherik);
            Mediator.Subscribe("GoToAdminSumarySpherik", OnGoToAdminSumarySpherik);
            Mediator.Subscribe("GoToGreenSettings", OnGoToSettingGreen);
            Mediator.Subscribe("GoToBackgroundOffer", OnGoToBackgroundOffer);
            Mediator.Subscribe("GoToConnexionClient", OnGoToConnexionClient);
        }
    }
}
