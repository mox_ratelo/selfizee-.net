﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input; 

namespace Selfizee
{
    public class SnapTchatViewModel : BaseViewModel , IPageViewModel
    {
        private ICommand _goToFiltre;

        public ICommand GoToFiltre
        {
            get
            {
                return _goToFiltre ?? (_goToFiltre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToFiltre", "");
                }));
            }
        }
    }
}
