﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{ 
    public class WinnerPageViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToThanks;
        private ICommand _gotoAccueil;

        public ICommand GoToThanks
        {
            get
            {
                return _goToThanks ?? (_goToThanks = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToThanks", "");
                }));
            }
        }

        public ICommand GoToAcceuil
        {
            get
            {
                return _gotoAccueil ?? (_gotoAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAcceuil", "");
                }));
            }
        }

        public string selectedFilter { get; set; }
    }
}
