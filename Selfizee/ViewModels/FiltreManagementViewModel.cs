﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows; 

namespace Selfizee
{
    public class FiltreManagementViewModel:BaseViewModel, IPageViewModel
    {
        private ICommand _goToVisualisation;
        private ICommand _goToTakePhoto;
        private ICommand _goToConnexionConfig;
        private ICommand _goToWinnerPage;
        private ICommand _gotoAccueil;

        public string cmdOrigin { get; set; }

        public string selectedFilter
        {
            set { SetValue(selectedFilterProprety, value); }
            get { return (string)GetValue(selectedFilterProprety); }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        public string selectedBG { get; set; }

        public ICommand GoToVisualisation
        {
            get
            {
                return _goToVisualisation ?? (_goToVisualisation = new RelayCommand(x =>
                {                            
                    Mediator.Notify("GoToVisualisation", selectedFilter);
                }));
            }
        }

        public ICommand GoToWinnerPage
        {
            get
            {
                return _goToWinnerPage ?? (_goToWinnerPage = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWinnerPage", selectedFilter);
                }));
            }
        }

        public ICommand GoToAcceuil
        {
            get
            {
                return _gotoAccueil ?? (_gotoAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAcceuil", "");
                }));
            }
        }
        public ICommand GoToTakePhoto
        {
            get
            {
                return _goToTakePhoto ?? (_goToTakePhoto = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToTakePhoto", ""); 
                    //Mediator.Notify("GoToAccueil", "FromFiltre");
                }));
            }
        }

        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }

        private ICommand _goToChoiceEvent;
        public ICommand GoToChoiceEvent
        {
            get
            {
                return _goToChoiceEvent ?? (_goToChoiceEvent = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToChoiceEvent", "");
                }));
            }
        }

        private ICommand _goToTanks;
        public ICommand GoToThanks
        {
            get
            {
                return _goToTanks ?? (_goToTanks = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToThanks", "");
                }));
            }
        }
        private static DependencyProperty selectedFilterProprety;

        static FiltreManagementViewModel()
        {
            FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata();
            metadata.Journal = true;
            selectedFilterProprety = DependencyProperty.Register(
            "selectedFilterProprety", typeof(string),
            typeof(FiltreManagementViewModel), metadata, null);
        }

        
    }
}
