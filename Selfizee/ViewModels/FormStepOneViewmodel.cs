﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class FormStepOneViewmodel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToStepTwo;
        private ICommand _goToVisualisation;
        private ICommand _goToThanks;

        public string UserEmail {get;set;}
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }

        public string textcolor { get; set; }
        public string buttonBackground { get; set; }
        public string buttonTextColor { get; set; }
        public string buttonNavigationBackground { get; set; }
        public string buttonNavigationTextColor { get; set; }
        public string fenetreTitleTextColor { get; set; }
        public string fenetreTitle { get; set; }
        public bool CanMyPropertyBeChanged { get; set; }

        public ICommand GoToVisualisation
        {
            get
            {
                return _goToVisualisation ?? (_goToVisualisation = new RelayCommand(x =>
                {
                    Mediator.Notify("BackToVisualisation", "");
                }));
            }
        }

        public ICommand GoToThank
        {
            get
            {
                return _goToThanks ?? (_goToThanks = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToThanks", "");
                }));
            }
        }

        public ICommand GoToStepTwo
        {
            get
            {
                return _goToStepTwo ?? (_goToStepTwo = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToStepTwo", "");
                }));
            }
        }
    }
}
