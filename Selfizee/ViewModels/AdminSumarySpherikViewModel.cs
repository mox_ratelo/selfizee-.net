﻿using System.Windows.Input;

namespace Selfizee.ViewModels
{
    class AdminSumarySpherikViewModel : BaseViewModel, IPageViewModel
    {

        private ICommand _goToPhotos;

        public ICommand GoToPhotos
        {
            get
            {
                return _goToPhotos ?? (_goToPhotos = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToPhotos", "");
                }));
            }
        }

        private ICommand _goToClientConfigs;
        public ICommand GoToClientConfig
        {
            get
            {
                return _goToClientConfigs ?? (_goToClientConfigs = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientConfigs", "");
                }));
            }
        }

        private ICommand _goToClientPhotosList;
        public ICommand GoToClientPhotosList
        {
            get
            {
                return _goToClientPhotosList ?? (_goToClientPhotosList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientPhotosList", "");
                }));
            }
        }

        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        private ICommand _goTotimeLine;
        public ICommand GoTotimeLine
        {
            get
            {
                return _goTotimeLine ?? (_goTotimeLine = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToHistorique", "");
                }));
            }
        }


        private ICommand _goToListEvent;
        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }
    }
}
