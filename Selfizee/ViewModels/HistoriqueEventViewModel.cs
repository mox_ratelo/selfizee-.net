﻿using System.Windows.Input;

namespace Selfizee
{
    public class HistoriqueEventViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToEditEvent;
        private ICommand _goToAccueil;
        private ICommand _goToParametre;
        private ICommand _goToConfiguration;
        private ICommand _goToWifiList;
        string push = "";

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToConfiguration
        {
            get
            {
                return _goToConfiguration ?? (_goToConfiguration = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConfiguration", "");
                }));
            }
        }
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToEditEvent
        {
            get
            {
                return _goToEditEvent ?? (_goToEditEvent = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToEditEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }



        private ICommand _goToAdminSumary;
        public ICommand GoToAdminSumary
        {
            get
            {
                return _goToAdminSumary ?? (_goToAdminSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumary", "");
                }));
            }
        }

        private ICommand _goToAdminSumarySpherik;
        public ICommand GoToAdminSumarySpherik
        {
            get
            {
                return _goToAdminSumarySpherik ?? (_goToAdminSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAdminSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}