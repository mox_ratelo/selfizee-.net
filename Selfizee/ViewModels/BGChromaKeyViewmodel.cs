﻿using System.Windows.Input;
using System.Windows;
using Selfizee.Models;

namespace Selfizee
{
    public class BGChromaKeyViewmodel : BaseViewModel, IPageViewModel
    {
        private PassedData passedData; 

        private ICommand _goToTakePhoto;
        private ICommand _goToTakeWebcam;
        private ICommand _goToConnexionConfig;

        public string selectedBG
        {
            set { SetValue(selectedBGProprety, value); }
            get { return (string)GetValue(selectedBGProprety); }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        private ICommand _goToSettingGreen;
        public ICommand GoToSettingGreen
        {
            get
            {
                return _goToSettingGreen ?? (_goToSettingGreen = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToGreenSettings", "");
                }));
            }
        }

        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }
        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }
        public ICommand GoToTakePhoto
        {
            get
            {
                return _goToTakePhoto ?? (_goToTakePhoto = new RelayCommand(x =>
                {
                    //if (String.IsNullOrEmpty(selectedBG))
                    //    selectedBG = "";
                    Mediator.Notify("GoToTakePhoto", selectedBG);

                    //#region forTest_Dina
                    //var passedData = new PassedData();
                    //passedData.Origin = Origin.BGSelection;
                    //var data = new Dictionary<Object, Object>();
                    //data.Add("selectedBG", selectedBG);
                    //passedData.Datas = data;
                    //Mediator.Notify("GoToFiltre", passedData);
                    //#endregion
                }));
            }
        }

        public ICommand GoToTakeWebcam
        {
            get
            {
                return _goToTakeWebcam ?? (_goToTakeWebcam = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToTakeWebCam", selectedBG);

                }));
            }
        }

        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        private static DependencyProperty selectedBGProprety;

        static BGChromaKeyViewmodel()
        {
            FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata();
            metadata.Journal = true;
            selectedBGProprety = DependencyProperty.Register(
            "selectedBGProprety", typeof(string),
            typeof(BGChromaKeyViewmodel), metadata, null);
        }
    }
}
