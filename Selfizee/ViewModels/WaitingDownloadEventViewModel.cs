﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee.ViewModels
{
    public class WaitingDownloadEventViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToChoiceEvent;

        public ICommand GoToChoiceEvent
        {
            get
            {
                return _goToChoiceEvent ?? (_goToChoiceEvent = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToChoiceEvent", "");
                }));
            }
        }
    }
}
