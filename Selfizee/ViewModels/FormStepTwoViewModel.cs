﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input; 

namespace Selfizee
{
    class FormStepTwoViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _backToVisualisation;

        public ICommand BackToVisualisation
        {
            get
            {
                return _backToVisualisation ?? (_backToVisualisation = new RelayCommand(x =>
                {
                    Mediator.Notify("BackToVisualisation", "");
                }));
            }
        }

        private ICommand _backToStepOne;

        public ICommand BackToStepOne
        {
            get
            {
                return _backToStepOne ?? (_backToStepOne = new RelayCommand(x =>
                {
                    Mediator.Notify("BackToStepOne", "");
                }));
            }
        }
    }
}
