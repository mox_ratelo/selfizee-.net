﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class RunDownloadViewModel : BaseViewModel, IPageViewModel
    {

        private ICommand _goToDownloadDetail;
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToChoiceDownload;
        private ICommand _goToParametre;
        private ICommand _goToWifiList;

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToDownloadDetail
        {
            get
            {
                return _goToDownloadDetail ?? (_goToDownloadDetail = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToDownloadDetail", "");
                }));
            }
        }

        public ICommand GoToChoiceDownload
        {
            get
            {
                return _goToChoiceDownload ?? (_goToChoiceDownload = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToChoiceDownload", "");
                }));
            }
        }

        

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}
