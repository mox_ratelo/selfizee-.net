﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class EditEventViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToCoordonate;
        private ICommand _goToHistorique;
        private ICommand _goToPhotos;
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToAccueil;
        private ICommand _goToParametre;
        private ICommand _goToConfiguration;
        private ICommand _goToWifiList;

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToConfiguration
        {
            get
            {
                return _goToConfiguration ?? (_goToConfiguration = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConfiguration", "");
                }));
            }
        }
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        public ICommand GoToCoordonate
        {
            get
            {
                return _goToCoordonate ?? (_goToCoordonate = new RelayCommand(x =>
                {
                    
                    Mediator.Notify("GoToCoordonate", "");
                }));
            }
        }

        public ICommand GoToHistorique
        {
            get
            {
                return _goToHistorique ?? (_goToHistorique = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToHistorique", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToPhotos
        {
            get
            {
                return _goToPhotos ?? (_goToPhotos = new RelayCommand(x =>
                {
                   
                    Mediator.Notify("GoToPhotos", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}
