﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class BackgroundManagerViewModel :BaseViewModel, IPageViewModel
    {
        private ICommand _goToSnapTchat;

        public ICommand GoToSnapTchat
        {
            get
            {
                return _goToSnapTchat ?? (_goToSnapTchat = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToSnapTchat", "");
                }));
            }
        }
    }
}
