﻿using System.Windows.Input;

namespace Selfizee
{
    public class TakeWebcamCameraViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToFiltre;
        private ICommand _goToConnexionConfig;
        private ICommand _goToAccueil;
        private ICommand _goToNoCamera;

        public ICommand GoToFiltre
        {
            get
            {
                return _goToFiltre ?? (_goToFiltre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToFiltre", "");
                }));
            }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        public ICommand GoToNoCamera
        {
            get
            {
                return _goToNoCamera ?? (_goToNoCamera = new RelayCommand(x =>
                        Mediator.Notify("GoToNoUSBCamera", "")
                ));
            }
        }

        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }
        public string selectedEvent { get; set; }
        public string selectedBGCromaKey { get; set; }
        public string selectedBackground { get; set; }
    }
}
