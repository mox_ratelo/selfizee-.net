﻿
using System.Windows.Input;

namespace Selfizee
{
    public class ConfigurationViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToParametre;
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToEditEvent;
        private ICommand _goToAccueil;
        private ICommand _goToWifiList;

        public ICommand GoToParametre
        {
            get
            {
                return _goToParametre ?? (_goToParametre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToParametre", "");
                }));
            }
        }

        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToEditEvent
        {
            get
            {
                return _goToEditEvent ?? (_goToEditEvent = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToEditEvent", "");
                }));
            }
        }

        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }

        public ICommand GoToWifiList
        {
            get
            {
                return _goToWifiList ?? (_goToWifiList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToWifiList", "");
                }));
            }
        }
    }
}
