﻿using System.Windows.Input;

namespace Selfizee
{
    public class ClientPhotoListViewModel: BaseViewModel, IPageViewModel
    {
        string bidon;
        private ICommand _goToClientPhotoList;
        public ICommand GoToClientConfigs
        {
            get
            {
                return _goToClientPhotoList ?? (_goToClientPhotoList = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientPhotoList", "");
                }));
            }
        }
        private ICommand _goToAccueil;
        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }
        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }
    }
}
