﻿
using System.Windows.Input;

namespace Selfizee
{
    public class DownloadDetailsViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToListEvent;
        private ICommand _goToDownload;
        private ICommand _goToAccueil;
        private ICommand _goToPostInstall;

        public ICommand GoToAccueil
        {
            get
            {
                return _goToAccueil ?? (_goToAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAccueil", "");
                }));
            }
        }

        public ICommand GoToPostInstall
        {
            get
            {
                return _goToPostInstall ?? (_goToPostInstall = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToPostInstall", "");
                }));
            }
        }
        public ICommand GoToListEvent
        {
            get
            {
                return _goToListEvent ?? (_goToListEvent = new RelayCommand(x =>
                {

                    Mediator.Notify("GoToListEvent", "");
                }));
            }
        }

        public ICommand GoToDownload
        {
            get
            {
                return _goToDownload ?? (_goToDownload = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToDownload", "");
                }));
            }
        }
    }
}
