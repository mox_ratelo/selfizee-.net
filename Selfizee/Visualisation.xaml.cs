﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Management;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using Selfizee.Models.Enum;
using System.Printing;

using System.Runtime.InteropServices;
using System.Windows.Threading;
using System.Net.Mail;
using Selfizee.Managers;
using Selfizee.Models;
using System.Drawing.Imaging;
using Selfizee.Manager;
using System.Text.RegularExpressions; 
using log4net;
using System.IO.Ports;
using System.Windows.Input;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Xml;
using System.Globalization;
using System.Net;
using System.Collections.Specialized;
using System.Reflection;
using Newtonsoft.Json;
using Xceed.Wpf.Toolkit;
using System.Threading;
using Selfizee.View;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for Visualisation.xaml
    /// </summary>
    public partial class Visualisation : UserControl
    {
        //private int countdown = 8;
        private bool enter_TextboxFocus = false;
        private string _copieValue = "0";
        private int current_Form_Index = 0;
        List<Page> lstPages = new List<Page>();
        //private INIFileManager _inimanager = new INIFileManager($"{Globals.ExeDir()}\\Config.ini");
        private bool _isMultiImpression = false;
        private string typeFormulaire = "";
        private PrintDocument _pd;
        Dictionary<string, string> hash = new Dictionary<string, string>();
        Dictionary<string, string> hashResult = new Dictionary<string, string>();
        Dictionary<string, string> hashCarmila = new Dictionary<string, string>();
        Dictionary<string, string> hashMendatory = new Dictionary<string, string>();
        Dictionary<string, string> hashTextBox = new Dictionary<string, string>();
        List<ToggleButton> _lst_Optin = new List<ToggleButton>();
        //private string OriginalFinalDirectory = "";
        private SerialPort port;
        private int nbPrinted = 0;
        private int globalPrinted = 0;
        private bool connected = true;
        private string sigle = "";
        int current_page = 1;
        private string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager ;
        private string mediaDataPath = Globals.EventMediaFolder() + "\\Data\\print.txt";
        PrintDocument pd = new PrintDocument();
        private int nbmultiimpression = 0;

        private int screenWidth = (int)SystemParameters.PrimaryScreenWidth;
        private int screenHeight = (int)SystemParameters.PrimaryScreenHeight;
        private int nbPrintedForPage = 0;
        private int last_page = 0;
        private TextBox currentTextBox { get; set; }
        private int nbmaxprinting = 0;
        private string typeForm { get; set; }

        private string gender_carmellia { get; set; }
        private string birth_carmellia { get; set; }
        private string name_carmellia { get; set; }
        private string firstname_carmellia { get; set; }
        private string phone_number_carmellia { get; set; }
        private string email_carmellia { get; set; }
        private string receive_mail { get; set; }
        private string receive_sms { get; set; }


        private string current_form { get; set; }
        private string form_email { get; set; }
        private string form_profil { get; set; }
        private string form_position { get; set; }
        private string form_job { get; set; }
        private string form_activity_area { get; set; }
        private string form_autoriseEmailPhoto { get; set; }
        private string form_diffuseEmailPhoto { get; set; }
        private string form_SendActuality { get; set; }

        private string form_receive_newsletter { get; set; }

        private string form_send_coordonates { get; set; }

        private string form_play_game { get; set; }
        private List<TextBlock> listOptinTextCheck = new List<TextBlock>();
        private List<CheckBox> listOptinCheckBox = new List<CheckBox>();

        private string form_rgpd_Name { get; set; }
        private string form_rgpd_Country { get; set; }
        private string form_rgpd_lastname { get; set; }
        private string form_rgpd_gender { get; set; }
        private string form_rgpd_email { get; set; }
        private string form_rgpd_postalcode { get; set; }
        private string form_rgpd_accept { get; set; }
        List<string> headText = new List<string>();

        private string form_typeuser { get; set; }
        private string form_typeconstructor { get; set; }
        private string form_activitysector { get; set; }
        private string form_usermission { get; set; }
        private bool activated = false;
        private int timer_Form = 300;

        ImageBrush brushCroix = new ImageBrush();

        //PrinterSettings printerSettings;
        #region P/Invoke
        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalFree(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalLock(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalUnlock(IntPtr handle);
        #endregion
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_From;

        private DispatcherTimer timerCountdown_print;
        INIFileManager appIni = new INIFileManager(Globals._appConfigFile);
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public Visualisation()
        {
            try
            {
                //Globals.timeOut = 8;
                InitializeComponent();
                _inimanager = new INIFileManager(EventIni);
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ClosePanel();
                }));
                getJsonData();
                brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix, UriKind.Relative));
                //GetCountryList();
                typeFormulaire = "";
                receive_mail = "0";
                receive_sms = "0";
                if (Globals.ScreenType.ToUpper() == "SPHERIK")
                {
                    int x = (int)SystemParameters.PrimaryScreenWidth;
                    alphanumeriquekeyboardForm1.Width = (x * 90) / 100;
                    alphanumeriquekeyboardForm1.Height = 350;
                    lbl_CountDown.Margin = new Thickness(0, -30, 0, 0);
                    lbl_CountDownPrint.Margin = new Thickness(0, -30, 0, 0);
                    mainStack.Margin = new Thickness(0, -175, 0, 0);
                }
                createPage();
                CloseAllPageForm();
                setTimeout();
                hash.Add(_Name.Text, _Name.Tag.ToString());
                hash.Add(_lastname.Text, _lastname.Tag.ToString());
                hash.Add(_emailRgpd.Text, _emailRgpd.Tag.ToString());
                hash.Add(_postalCode.Text, _postalCode.Tag.ToString());
                hashTextBox.Add("key_" + _Name.Name, _Name.Text);
                hashTextBox.Add("key_" + _lastname.Name, _lastname.Text);
                hashTextBox.Add("key_" + _emailRgpd.Name, _emailRgpd.Text);
                hashTextBox.Add("key_" + _postalCode.Name, _postalCode.Text);
                hashTextBox.Add("key_" + _country.Name, _country.Text);

                hashTextBox.Add("key_" + _lastnameCarmellia.Name, _lastnameCarmellia.Text);
                hashTextBox.Add("key_" + _NameCarmellia.Name, _NameCarmellia.Text);
                hashTextBox.Add("key_" + _portable.Name, _portable.Text);
                hashTextBox.Add("key_" + _emailCarmellia.Name, _emailCarmellia.Text);
                hashTextBox.Add("key_" + _portableConnect.Name, _portableConnect.Text);

                hashTextBox.Add("key_" + _lastnameCarmelliaKnown.Name, _lastnameCarmelliaKnown.Text);
                hashTextBox.Add("key_" + _NameCarmelliaKnown.Name, _NameCarmelliaKnown.Text);
                hashTextBox.Add("key_" + _emailCarmelliaKnown.Name, _emailCarmelliaKnown.Text);
                hashTextBox.Add("key_" + _lastnameCarmelliaUnKnown.Name, _lastnameCarmelliaUnKnown.Text);
                hashTextBox.Add("key_" + _NameCarmelliaUnKnown.Name, _NameCarmelliaUnKnown.Text);
                hashTextBox.Add("key_" + _emailCarmelliaUnKnown.Name, _emailCarmelliaUnKnown.Text);

                _birthConnect.GotKeyboardFocus += TextBoxGetFocused;
                _birthConnect.LostFocus += LostTextBoxKeyboardFocus;
                _birthConnect.GotFocus += OnGotFocus;
                //_birthConnect.SelectionChanged += new SelectionChangedEventArgs(OnSelectionChanged);
                _birth.GotKeyboardFocus += TextBoxGetFocusedDisconnect;
                _birth.LostFocus += LostTextBoxKeyboardFocusDisconnect;
                _birth.GotFocus += OnGotFocus;
                //_birth.GotFocus += GetFieldFocu;


                hashMendatory.Add(_lastnameCarmellia.Name, "");
                hashMendatory.Add(_NameCarmellia.Name, "");
                hashMendatory.Add(_portable.Name, "");
                hashMendatory.Add(_emailCarmellia.Name, "");
                hashMendatory.Add(_portableConnect.Name, "A");

                hashMendatory.Add(_lastnameCarmelliaKnown.Name, "B");
                hashMendatory.Add(_NameCarmelliaKnown.Name, "B");
                hashMendatory.Add(_emailCarmelliaKnown.Name, "B");
                hashMendatory.Add(_lastnameCarmelliaUnKnown.Name, "C");
                hashMendatory.Add(_NameCarmelliaUnKnown.Name, "C");
                hashMendatory.Add(_emailCarmelliaUnKnown.Name, "C");

                string code = appIni.GetSetting("EVENTSCONFIG", "id");
                string pathForm = "C:\\Events\\Assets\\" + code + "\\Form\\content.xml";
                string formName = _inimanager.GetSetting("FORM", "Name");

                if (formName.ToLower() == "carmila" && lstPages != null)
                {
                    hashResult.Add(rd_male.Name, "");
                    hashCarmila.Add(rd_male.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(rd_maleKnown.Name, "");
                    hashCarmila.Add(rd_maleKnown.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(rd_maleUnKnown.Name, "");
                    hashCarmila.Add(rd_maleUnKnown.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(rd_female.Name, "");
                    hashCarmila.Add(rd_female.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(rd_femaleKnown.Name, "");
                    hashCarmila.Add(rd_femaleKnown.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(rd_femaleUnKnown.Name, "");
                    hashCarmila.Add(rd_femaleUnKnown.Name, lstPages[0].lstElements[2].lst_textBox.Name.Split('_').Last());

                    hashResult.Add(_birth.Name, "");
                    hashCarmila.Add(_birth.Name, lstPages[0].lstElements[0].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_birthConnect.Name, "");
                    hashCarmila.Add(_birthConnect.Name, lstPages[0].lstElements[0].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_lastnameCarmellia.Name, "");
                    hashCarmila.Add(_lastnameCarmellia.Name, lstPages[0].lstElements[4].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_NameCarmellia.Name, "");
                    hashCarmila.Add(_NameCarmellia.Name, lstPages[0].lstElements[3].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_portable.Name, "");
                    hashCarmila.Add(_portable.Name, lstPages[0].lstElements[1].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_emailCarmellia.Name, "");
                    hashCarmila.Add(_emailCarmellia.Name, lstPages[0].lstElements[5].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_portableConnect.Name, "");
                    hashCarmila.Add(_portableConnect.Name, lstPages[0].lstElements[1].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_lastnameCarmelliaKnown.Name, "");
                    hashCarmila.Add(_lastnameCarmelliaKnown.Name, lstPages[0].lstElements[4].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_NameCarmelliaKnown.Name, "");
                    hashCarmila.Add(_NameCarmelliaKnown.Name, lstPages[0].lstElements[3].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_emailCarmelliaKnown.Name, "");
                    hashCarmila.Add(_emailCarmelliaKnown.Name, lstPages[0].lstElements[5].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_lastnameCarmelliaUnKnown.Name, "");
                    hashCarmila.Add(_lastnameCarmelliaUnKnown.Name, lstPages[0].lstElements[4].lst_textBox.Name.Split('_').Last());
                    hashResult.Add(_NameCarmelliaUnKnown.Name, "");
                    hashCarmila.Add(_NameCarmelliaUnKnown.Name, lstPages[0].lstElements[3].lst_textBox.Name.Split('_').Last());
                    hashResult.Add("key_" + _emailCarmelliaUnKnown.Name, "");
                    hashCarmila.Add( _emailCarmelliaUnKnown.Name, lstPages[0].lstElements[5].lst_textBox.Name.Split('_').Last());

                    hashCarmila.Add(NonReceivemail_Carmellia_Disconnect.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(YesReceivemail_Carmellia_Disconnect.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(NonReceiveSms_Carmellia.Name, lstPages[1].lstElements[1].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(YesReceiveSms_Carmellia.Name, lstPages[1].lstElements[1].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(NonReceiveMail_Carmellia_known.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(YesReceiveMail_Carmellia_known.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(NonReceiveMail_Carmellia_Unknown.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                    hashCarmila.Add(YesReceiveMail_Carmellia_Unknown.Name, lstPages[1].lstElements[0].lst_Optin.Name.Split('_').Last());
                }



                //hashTextBox.Add("key_" + _emailCarmelliaUnKnown.Name, _emailCarmelliaUnKnown.Text);


                ImageToPrint.MaxWidth = SystemParameters.MaximizedPrimaryScreenWidth;
                ImageToPrint.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
                Globals.dtPrint = DateTime.MinValue;
                /*
                alphanumeriquekeyboardForm1.KeyUp += Page_Click;
                alphanumeriquekeyboardForm2.KeyUp += Page_Click;
                alphanumeriquekeyboardForm3.KeyUp += Page_Click;
                alphanumeriquekeyboardForm4.KeyUp += Page_Click;
                alphanumeriquekeyboardForm5.KeyUp += Page_Click;
                alphanumeriquekeyboardForm6.KeyUp += Page_Click;
                alphanumeriquekeyboardForm7.KeyUp += Page_Click;
                alphanumeriquekeyboardForm8.KeyUp += Page_Click;
                alphanumeriquekeyboardForm9.KeyUp += Page_Click;

                alphanumeriquekeyboardForm10.KeyUp += Page_Click;
                alphanumeriquekeyboardForm11.KeyUp += Page_Click;
                alphanumeriquekeyboardCarmellia.KeyUp += Page_Click;
                alphanumeriquekeyboard.KeyUp += Page_Click;
                alphanumeriquekeyboardBis.KeyUp += Page_Click;
                alphanumeriquekeyboardCarmelliaKnown.KeyUp += Page_Click;
                alphanumeriquekeyboardCarmelliaUnKnown.KeyUp += Page_Click;
                alphanumeriquekeyboardRgpd.KeyUp += Page_Click;
                numeriqueKeyboardCarmellia.KeyUp += Page_Click;
                numeriqueKeyboardCarmelliaKnown.KeyUp += Page_Click;
                numeriqueKeyboardCarmelliaUnKnown.KeyUp += Page_Click;
                numeriqueKeyboardConnection.KeyUp += Page_Click;
                numeriqueKeyboardForm1.KeyUp += Page_Click;
                numeriqueKeyboardForm2.KeyUp += Page_Click;
                numeriqueKeyboardForm3.KeyUp += Page_Click;
                numeriqueKeyboardForm4.KeyUp += Page_Click;
                numeriqueKeyboardForm5.KeyUp += Page_Click;
                numeriqueKeyboardForm6.KeyUp += Page_Click;
                numeriqueKeyboardForm7.KeyUp += Page_Click;
                numeriqueKeyboardForm8.KeyUp += Page_Click;
                numeriqueKeyboardForm9.KeyUp += Page_Click;
                numeriqueKeyboardForm10.KeyUp += Page_Click;
                numeriqueKeyboardForm11.KeyUp += Page_Click;
                numeriqueKeyboardRgpd.KeyUp += Page_Click;*/


                typeForm = appIni.GetSetting("FORM", "type");
                alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                numeriqueKeyboardForm1.Visibility = Visibility.Collapsed;
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                Globals.obligatoryField = new List<MandatoryControl>();
                nbmultiimpression = Convert.ToInt32(_inimanager.GetSetting("PRINTING", "eventauthorizedmultiprinting"));
                nbmaxprinting = Convert.ToInt32(_inimanager.GetSetting("PRINTING", "eventauthorizedprint"));
                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("Visualization");
                if (backgroundAttributes.IsImage)
                {
                    ImageBrush imgBrush = new ImageBrush();
                    var fileName = Globals.LoadBG(TypeFond.ForVisualisation);
                    if (!File.Exists(fileName))
                    {
                        fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                    }
                    Bitmap bmp = null;
                    using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
                    {
                        bmp = new Bitmap(img, new System.Drawing.Size(1920, 1080));
                    }
                    imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp);
                    imgBrush.Stretch = Stretch.Fill;
                    visualisationPage.Background = imgBrush;
                }

                if (!backgroundAttributes.EnableTitle)
                    viewTitle.Visibility = Visibility.Collapsed;

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }



                    if (backgroundAttributes.EnableTitle)
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                            viewTitle.Text = backgroundAttributes.Title;
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                            viewTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                        {
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                                Canvas.SetLeft((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                                Canvas.SetTop((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                        }
                    }
                }

                if (DefaultCopie() > 0)
                    _copieValue = DefaultCopie().ToString();
                setTimeout();
                INIFileManager _ini = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
                if (_print <= 0)
                {
                    vbtnImprimer.Visibility = Visibility.Hidden;
                    if (File.Exists(Globals._btnVisualisationEmail))
                    {
                        if (activated)
                        {
                            vbtnImprimer.Visibility = Visibility.Visible;
                            BtnContent((Button)vbtnImprimer, "BtnImprimer", "Imprimer", Globals._btnVisualisationEmail);
                        }
                        else
                        {
                            vbtnImprimer.Visibility = Visibility.Hidden;
                        }
                       
                    }
                }
                else
                {
                    if (File.Exists(Globals._btnVisualisationPrinting))
                    {
                        BtnContent((Button)vbtnImprimer, "BtnImprimer", "Imprimer", Globals._btnVisualisationPrinting);
                    }
                    else
                    {
                        BtnContent((Button)vbtnImprimer, "BtnImprimer", "Imprimer", Globals._defaultbtnVisualisationPrinting);
                    }
                }
                _email.GotFocus += CoordonneeTextBoxGetFocused;
                //email_Mandatory.GotFocus += CoordonneeMandatoryTextBoxGetFocused;
                _email.LostFocus += CoordonneeTextBoxLostFocused;
                _emailBis.GotFocus += CoordonneeTextBoxGetFocused;
                //email_Mandatory.GotFocus += CoordonneeMandatoryTextBoxGetFocused;
                _emailBis.LostFocus += CoordonneeTextBoxLostFocused;
                //email_Mandatory.LostFocus += CoordonneeMandatoryTextBoxLostFocused;
                _Name.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                _Name.LostFocus += CoordonneeTextBoxRgpdLostFocused;
                _lastname.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                _lastname.LostFocus += CoordonneeTextBoxRgpdLostFocused;
                _emailRgpd.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                _emailRgpd.LostFocus += CoordonneeTextBoxRgpdLostFocused;
                //_email.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                //_email.LostFocus += CoordonneeTextBoxRgpdLostFocused;
                _postalCode.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                _postalCode.LostFocus += CoordonneeTextBoxRgpdLostFocused;

                _lastnameCarmellia.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _lastnameCarmellia.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _NameCarmellia.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _NameCarmellia.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _portable.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _portable.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _emailCarmellia.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _emailCarmellia.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _portableConnect.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _portableConnect.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _lastnameCarmelliaKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _lastnameCarmelliaKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _NameCarmelliaKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _NameCarmelliaKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _emailCarmelliaKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _emailCarmelliaKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _lastnameCarmelliaUnKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _lastnameCarmelliaUnKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _NameCarmelliaUnKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _NameCarmelliaUnKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;
                _emailCarmelliaUnKnown.GotFocus += CoordonneeTextBoxGetFocusedConnection;
                _emailCarmelliaUnKnown.LostFocus += CoordonneeTextBoxLostFocusedConnection;

                _country.GotFocus += CoordonneeTextBoxRgpdGetFocused;
                _country.LostFocus += CoordonneeTextBoxRgpdLostFocused;
                _postalCode.TextChanged += textBox_TextChanged;
                YesAllowSendNews.PreviewMouseDown += ChangeToggleEvent;
                NoAllowSendNews.PreviewMouseDown += ChangeToggleEvent;
                YesReceiveMail.PreviewMouseDown += ChangeToggleEvent;
                NoReceiveMail.PreviewMouseDown += ChangeToggleEvent;
                YesUsePhotos.PreviewMouseDown += ChangeToggleEvent;
                NoUsePhotos.PreviewMouseDown += ChangeToggleEvent;
                var VisuBtnHelpX = _ini.GetSetting("BUTTONPOSITION", "VisuBtnHelpX");
                var VisuBtnHelpY = _ini.GetSetting("BUTTONPOSITION", "VisuBtnHelpY");
                var VisuBtnHomeX = _ini.GetSetting("BUTTONPOSITION", "VisuBtnHomeX");
                var VisuBtnHomeY = _ini.GetSetting("BUTTONPOSITION", "VisuBtnHomeY");
                var VisuBtnPrintX = _ini.GetSetting("BUTTONPOSITION", "VisuBtnPrintX");
                var VisuBtnPrintY = _ini.GetSetting("BUTTONPOSITION", "VisuBtnPrintY");
                VisuBtnHelpX = !string.IsNullOrEmpty(VisuBtnHelpX) ? VisuBtnHelpX : "0";
                VisuBtnHelpY = !string.IsNullOrEmpty(VisuBtnHelpY) ? VisuBtnHelpY : "0";
                VisuBtnHomeX = !string.IsNullOrEmpty(VisuBtnHomeX) ? VisuBtnHomeX : "0";
                VisuBtnHomeY = !string.IsNullOrEmpty(VisuBtnHomeY) ? VisuBtnHomeY : "0";
                VisuBtnPrintX = !string.IsNullOrEmpty(VisuBtnPrintX) ? VisuBtnPrintX : "0";
                VisuBtnPrintY = !string.IsNullOrEmpty(VisuBtnPrintY) ? VisuBtnPrintY : "0";

                //////////////////////////////////////////////////////////////////////////
                ImageBrush btnprevious = new ImageBrush();
                btnprevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormPrevious, UriKind.Absolute));
                ImageBrush btncancelform = new ImageBrush();
                btncancelform.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnCancelForm, UriKind.Absolute));

                ImageBrush btnCalendar = new ImageBrush();
                btnCalendar.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnCalendar, UriKind.Absolute));
                Button button = (Button)_birth.Template.FindName("PART_Button", _birth);
                //button.Background = btnCalendar;

                Button button1 = (Button)_birthConnect.Template.FindName("PART_Button", _birthConnect);
                //button1.Background = btnCalendar;
                ImageBrush btnYesSms = new ImageBrush();
                btnYesSms.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesSms, UriKind.Absolute));

                ImageBrush btnNoSms = new ImageBrush();
                btnNoSms.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoSms, UriKind.Absolute));

                ImageBrush btnYesEmail = new ImageBrush();
                btnYesEmail.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesEmail, UriKind.Absolute));

                ImageBrush btnNoEmail = new ImageBrush();
                btnNoEmail.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoEmail, UriKind.Absolute));

                YesReceivemail_Carmellia_Disconnect.Background = btnYesEmail;
                NonReceivemail_Carmellia_Disconnect.Background = btnNoEmail;
                YesReceiveSms_Carmellia.Background = btnYesSms;
                NonReceiveSms_Carmellia.Background = btnNoSms;
                YesReceiveMail_Carmellia_known.Background = btnYesEmail;
                NonReceiveMail_Carmellia_known.Background = btnNoEmail;
                YesReceiveMail_Carmellia_Unknown.Background = btnYesEmail;
                NonReceiveMail_Carmellia_Unknown.Background = btnNoEmail;

                ImageBrush btnnext = new ImageBrush();
                btnnext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormNext, UriKind.Absolute));
                backType.Background = btnprevious;
                backJob.Background = btnprevious;
                backManufaturer.Background = btnprevious;

                //var brushCroix = new ImageBrush();
                brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix2, UriKind.Relative));
                btnCloseKeyboard1.Background = brushCroix;

                backOptin.Background = btnprevious;
                backSector.Background = btnprevious;
                backToCoordonateRgpd.Background = btnprevious;
                backTypeConstructor.Background = btnprevious;
                backEmail.Background = btnprevious;
                backUser.Background = btnprevious;
                backToCoordonate.Background = btnprevious;
                backToActivitySector.Background = btnprevious;

                hideForm.Background = btncancelform;
                hideForm1.Background = btncancelform;
                hideForm2.Background = btncancelform;
                hideForm3.Background = btncancelform;
                hideForm4.Background = btncancelform;
                hideForm5.Background = btncancelform;
                hideFormRgpd.Background = btncancelform;
                hideFormRgpd1.Background = btncancelform;
                hideFormCheckBox.Background = btncancelform;
                hideForm3Bis.Background = btncancelform;
                hideForm3ConstructorBis.Background = btncancelform;
                hideFormActivitySectorBis.Background = btncancelform;
                hideFormBis.Background = btncancelform;
                hideFormMissionUserBis.Background = btncancelform;
                hideFormCarmellia.Background = brushCroix;
                hideFormCarmelliaKnown.Background = brushCroix;
                hideFormCarmelliaUnKnown.Background = brushCroix;
                hideFormConectionCarmellia.Background = brushCroix;


                //ImageBrush btnnext = new ImageBrush();
                //btnnext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormNext, UriKind.Absolute));
                continueJob.Background = btnnext;

                continueManufacturer.Background = btnnext;
                continueOptin.Background = btnnext;
                continueSector.Background = btnnext;
                continueType.Background = btnnext;
                continueToPrint.Background = btnnext;
                continueFormOneMandatory.Background = btnnext;
                continueFormOne.Background = btnnext;
                continueFormOneRgpd.Background = btnnext;
                continueToPrintexitRgpd.Background = btnnext;
                continueFormOneBis.Background = btnnext;
                continueFormOneBis.Background = btnnext;
                continueActivitysector.Background = btnnext;
                continueTypeConstructorBis.Background = btnnext;
                continueUserMission.Background = btnnext;
                continueToVisualisation.Background = btnnext;

                acceChoice1_txt.MouseLeftButtonDown += new MouseButtonEventHandler(AccessChoice1_MouseLeftButtonDown);
                acceChoice2_txt.MouseLeftButtonDown += new MouseButtonEventHandler(AccessChoice2_MouseLeftButtonDown);
                acceChoice3_txt.MouseLeftButtonDown += new MouseButtonEventHandler(AccessChoice3_MouseLeftButtonDown);
                acceChoice4_txt.MouseLeftButtonDown += new MouseButtonEventHandler(AccessChoice4_MouseLeftButtonDown);

                TranslateTransform initHelpTransform = new TranslateTransform();
                initHelpTransform.X = (-1) * double.Parse(($"{VisuBtnHelpX}").ToString());
                initHelpTransform.Y = (-1) * double.Parse(($"{VisuBtnHelpY}").ToString());
                this.StkpHelp.RenderTransform = initHelpTransform;

                TranslateTransform initHomeTransform = new TranslateTransform();
                initHomeTransform.X = (-1) * double.Parse(($"{VisuBtnHomeX}").ToString());
                initHomeTransform.Y = double.Parse(($"{VisuBtnHomeY}").ToString());
                this.StkpHome.RenderTransform = initHomeTransform;
                this.StkpHome_Print.RenderTransform = initHelpTransform;

                TranslateTransform initImprimTransform = new TranslateTransform();
                initImprimTransform.X = double.Parse(($"{VisuBtnPrintX}").ToString());
                initImprimTransform.Y = double.Parse(($"{VisuBtnPrintY}").ToString());
                this.vbtnImprimer.RenderTransform = initImprimTransform;
                scrollCoordonate.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                stepOneCoordonateContainer.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                stepOneCoordonateContainer.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonate.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateMandatory.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateMandatory.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrolluser.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrolluser.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollManufacturer.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollManufacturer.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollTypeManufacturer.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollTypeManufacturer.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollSector.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollSector.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollOptin.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollOptin.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollRgpd.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateBis.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateBis.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollMissionUserBis.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollMissionUserBis.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollActivitySectorBis.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollActivitySectorBis.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollTypeConstructorBis.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollTypeConstructorBis.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollTypeUserBis.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollTypeUserBis.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;

                scrollOptinCheckBox.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollOptinCheckBox.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;

                scrollRgpd.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateRGPD.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateRGPD.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateCarmellia.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateCarmellia.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateConectionCarmellia.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateConectionCarmellia.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateCarmelliaKnown.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateCarmelliaKnown.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollCoordonateCarmelliaUnKnown.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                scrollCoordonateCarmelliaUnKnown.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;



                //if (Globals.ScreenType == "SPHERIK")
                //{
                scrollCoordonateMandatory.Width = screenWidth;
                scrollCoordonateMandatory.Height = screenHeight;
                scrollCopie.Width = screenWidth;
                scrollCopie.Height = screenHeight;
                //}
                
                

            }
            catch (Exception e)
            {

            }

        }

        public void LoadXmlContent(string path)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);

        }

        public async void OnGotFocus(object sender, EventArgs e)
        {
            Page_Click();
            MaskedTextBox txtBox = (MaskedTextBox)sender;
            await Task.Delay(10);
            if (txtBox.Text == "__/__/____")
            {
                txtBox.SelectionStart = 0;
            }
        }



        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            Page_Click();
            TextBox txtBox = (TextBox)sender;
            KeyboardManager.closeOnscreenKeyboard();
            //alphanumeriquekeyboardForm1.Visibility = Visibility.Visible;

            string keyName = "key_" + txtBox.Name;
            if (txtBox.Tag.ToString().ToLower() == "string" || txtBox.Tag.ToString().ToLower() == "email")
            {
                showKeyboard(txtBox, 0);
            }
            else if (txtBox.Tag.ToString().ToLower() == "numerique" || txtBox.Tag.ToString().ToLower().Contains("phone"))
            {
                showKeyboard(txtBox, 1);
            }
            else
            {
                alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                numeriqueKeyboardForm1.Visibility = Visibility.Collapsed;
            }

            if (hashTextBox.ContainsKey(keyName))
            {
                string valueFilter = hashTextBox.FirstOrDefault(x => x.Key == keyName).Value;
                if (txtBox.Text == valueFilter && !string.IsNullOrEmpty(valueFilter))
                {
                    txtBox.Text = "";
                }

            }
            this.GotTouchCapture += TouchEnabledTextBox_GotTouchCapture;


        }

        public void SurveyTextBoxGetFocused(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            KeyboardManager.closeOnscreenKeyboard();
            showKeyboard(txtBox, 0);

            string keyName = "key_" + txtBox.Name;


            if (hashTextBox.ContainsKey(keyName))
            {
                string valueFilter = hashTextBox.FirstOrDefault(x => x.Key == keyName).Value;
                if (txtBox.Text == valueFilter && !string.IsNullOrEmpty(valueFilter))
                {
                    txtBox.Text = "";
                }

            }
            this.GotTouchCapture += TouchEnabledTextBox_GotTouchCapture;


        }



        private void TouchEnabledTextBox_GotTouchCapture(object sender, TouchEventArgs e)
        {
            Process[] oskProcessArray = Process.GetProcessesByName("TabTip");
            foreach (Process onscreenProcess in oskProcessArray)
            {
                onscreenProcess.Kill();
                onscreenProcess.Dispose();
                onscreenProcess.Close();
            }
        }

        public void CoordonneeTextBoxLostFocusedConnection(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            if (current_Form_Index == 1)
            {
                //alphanumeriquekeyboardConnection.Visibility = Visibility.Collapsed;
                numeriqueKeyboardConnection.Visibility = Visibility.Collapsed;
            }
            else if (current_Form_Index == 2)
            {
                alphanumeriquekeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
            }
            else if (current_Form_Index == 3)
            {
                alphanumeriquekeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
            }
            else if (current_Form_Index == 4)
            {
                alphanumeriquekeyboardCarmellia.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmellia.Visibility = Visibility.Collapsed;
            }

            if (txtBox.Tag.ToString() == "EMAIL" && !checkMailAdress(txtBox.Text))
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                closeCurrentForm();
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
            }
            else
            {
                string keyName = "key_" + txtBox.Name;
                string valueToshow = hashTextBox.FirstOrDefault(x => x.Key == keyName).Value;
                if (txtBox.Text == "")
                {
                    hashResult[txtBox.Name] = "";
                    txtBox.Text = valueToshow;
                }
                else
                {
                    hashResult[txtBox.Name] = txtBox.Text;
                }
            }


        }

        public void CoordonneeTextBoxGetFocusedConnection(object sender, EventArgs e)
        {
            Page_Click();
            TextBox txtBox = (TextBox)sender;
            //alphanumeriquekeyboardConnection.Visibility = Visibility.Visible;
            string keyName = "key_" + txtBox.Name;
            if (txtBox.Tag.ToString() == "STRING" || txtBox.Tag.ToString() == "EMAIL")
            {
                if (current_Form_Index == 1)
                {
                    //alphanumeriquekeyboardConnection.Visibility = Visibility.Visible;
                    //alphanumeriquekeyboardConnection.ActiveContainer = txtBox;
                    //alphanumeriquekeyboardConnection.HorizontalAlignment = HorizontalAlignment.Center;
                    numeriqueKeyboardConnection.Visibility = Visibility.Collapsed;
                }
                else if (current_Form_Index == 2)
                {
                    alphanumeriquekeyboardCarmelliaKnown.Visibility = Visibility.Visible;
                    alphanumeriquekeyboardCarmelliaKnown.ActiveContainer = txtBox;
                    alphanumeriquekeyboardCarmelliaKnown.HorizontalAlignment = HorizontalAlignment.Center;
                    numeriqueKeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
                }
                else if (current_Form_Index == 3)
                {
                    alphanumeriquekeyboardCarmelliaUnKnown.Visibility = Visibility.Visible;
                    alphanumeriquekeyboardCarmelliaUnKnown.ActiveContainer = txtBox;
                    alphanumeriquekeyboardCarmelliaUnKnown.HorizontalAlignment = HorizontalAlignment.Center;
                    numeriqueKeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                }
                else if (current_Form_Index == 4)
                {
                    alphanumeriquekeyboardCarmellia.Visibility = Visibility.Visible;
                    alphanumeriquekeyboardCarmellia.ActiveContainer = txtBox;
                    alphanumeriquekeyboardCarmellia.HorizontalAlignment = HorizontalAlignment.Center;
                    numeriqueKeyboardCarmellia.Visibility = Visibility.Collapsed;
                }

            }
            else if (txtBox.Tag.ToString() == "NUMERIQUE")
            {
                if (current_Form_Index == 1)
                {
                    //alphanumeriquekeyboardConnection.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardConnection.Visibility = Visibility.Visible;
                    numeriqueKeyboardConnection.ActiveContainer = txtBox;
                    numeriqueKeyboardConnection.HorizontalAlignment = HorizontalAlignment.Center;
                }
                else if (current_Form_Index == 2)
                {
                    alphanumeriquekeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardCarmelliaKnown.Visibility = Visibility.Visible;
                    numeriqueKeyboardCarmelliaKnown.ActiveContainer = txtBox;
                    numeriqueKeyboardCarmelliaKnown.HorizontalAlignment = HorizontalAlignment.Center;
                }
                else if (current_Form_Index == 3)
                {

                    alphanumeriquekeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardCarmelliaUnKnown.Visibility = Visibility.Visible;
                    numeriqueKeyboardCarmelliaUnKnown.ActiveContainer = txtBox;
                    numeriqueKeyboardCarmelliaUnKnown.HorizontalAlignment = HorizontalAlignment.Center;
                }
                else if (current_Form_Index == 4)
                {

                    alphanumeriquekeyboardCarmellia.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardCarmellia.Visibility = Visibility.Visible;
                    numeriqueKeyboardCarmellia.ActiveContainer = txtBox;
                    numeriqueKeyboardCarmellia.HorizontalAlignment = HorizontalAlignment.Center;
                }
            }
            else
            {
                //alphanumeriquekeyboardConnection.Visibility = Visibility.Collapsed;
                numeriqueKeyboardConnection.Visibility = Visibility.Collapsed;
                alphanumeriquekeyboardCarmellia.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmellia.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                alphanumeriquekeyboardCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                numeriqueKeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
                alphanumeriquekeyboardCarmelliaKnown.Visibility = Visibility.Collapsed;
            }

            if (hashTextBox.ContainsKey(keyName))
            {
                string valueFilter = hashTextBox.FirstOrDefault(x => x.Key == keyName).Value;
                if (txtBox.Text == valueFilter && !string.IsNullOrEmpty(valueFilter))
                {
                    txtBox.Text = "";
                }

            }

        }

        private void datePicker1_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker _datePicker1 = (DatePicker)sender;
            if (_datePicker1.SelectedDate != null) return;

            FieldInfo fiTextBox = typeof(DatePicker)
              .GetField("_textBox", BindingFlags.Instance | BindingFlags.NonPublic);

            if (fiTextBox != null)
            {
                DatePickerTextBox dateTextBox =
                 (DatePickerTextBox)fiTextBox.GetValue(_datePicker1);

                if (dateTextBox != null)
                {
                    PropertyInfo piWatermark = dateTextBox.GetType()
                     .GetProperty("Watermark", BindingFlags.Instance | BindingFlags.NonPublic);

                    if (piWatermark != null)
                    {
                        piWatermark.SetValue(dateTextBox, "Date naissance *", null);
                    }
                }
            }
        }

        private void RadioButton_Checked(object sender, EventArgs e)
        {
            var button = sender as RadioButton;
            string name = button.Name.Split('_').FirstOrDefault();

            string Value = button.Content.ToString();
            hashResult[name] = Value;
        }

        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }

        public void CoordonneeTextBoxLostFocused(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            CloseKeyboard();
            if (txtBox.Tag.ToString().ToLower().Contains("phone") && !CheckPhoneNumber(txtBox.Text) && !string.IsNullOrEmpty(txtBox.Text))
            {
                currentTextBox = txtBox;
                coordonneeMandatoryTitle.Text = "Veuillez saisir un numéro de téléphone valide.";
                closeCurrentForm();
                txtBox.Focus();
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
            }
            else if (txtBox.Tag.ToString().ToUpper() == "EMAIL" && !checkMailAdress(txtBox.Text) && !string.IsNullOrEmpty(txtBox.Text))
            {
                currentTextBox = txtBox;
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                closeCurrentForm();
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
            }
            else
            {
                string keyName = "key_" + txtBox.Name;
                string valueToshow = hashTextBox.FirstOrDefault(x => x.Key == keyName).Value;
                if (txtBox.Text == "")
                {
                    hashResult[txtBox.Name] = "";
                    txtBox.Text = valueToshow;
                }
                else
                {
                    hashResult[txtBox.Name] = txtBox.Text;
                }
            }


        }

        public void SurveyTextBoxLostFocused(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            CloseKeyboard();
        }

        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            numeriqueKeyboardConnection.Visibility = Visibility.Collapsed;
            if (_birthConnect.Text == "__/__/____")
            {
                _birthConnect.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));

                watermark.Visibility = Visibility.Visible;
            }
            else
            {
                if (!IsDateTime(_birthConnect.Text))
                {
                    coordonneeMandatoryTitle.Text = "Veuillez saisir une date de naissance valide.";
                    scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                    _birthConnect.Focus();
                    sigle = "A";
                    scrollCoordonateMandatory.Visibility = Visibility.Visible;
                }
                else
                {
                    _birthConnect.Foreground = System.Windows.Media.Brushes.Black;
                    watermark.Visibility = Visibility.Collapsed;
                }

            }



        }

        public void TextBoxGetFocused(object sender, EventArgs e)
        {
            Page_Click();
            MaskedTextBox tt = (MaskedTextBox)sender;
            tt.Foreground = System.Windows.Media.Brushes.Black;

            numeriqueKeyboardConnection.Visibility = Visibility.Visible;
            numeriqueKeyboardConnection.HorizontalAlignment = HorizontalAlignment.Center;
            numeriqueKeyboardConnection.ActiveContainer = tt;
            watermark.Visibility = Visibility.Collapsed;
            //if (tt.Text == "__/__/____")
            //{
            //    tt.CaretIndex = 0;
            //}
        }

        public void LostTextBoxKeyboardFocusDisconnect(object sender, EventArgs e)
        {
            numeriqueKeyboardCarmellia.Visibility = Visibility.Collapsed;
            if (_birth.Text == "__/__/____")
            {
                _birth.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));

                watermark_deconnect.Visibility = Visibility.Visible;
            }
            else
            {
                if (!IsDateTime(_birth.Text))
                {
                    coordonneeMandatoryTitle.Text = "Veuillez saisir une date de naissance valide.";
                    scrollCoordonateCarmellia.Visibility = Visibility.Collapsed;
                    _birth.Focus();
                    sigle = "A";
                    scrollCoordonateMandatory.Visibility = Visibility.Visible;
                }
                else
                {
                    _birth.Foreground = System.Windows.Media.Brushes.Black;
                    watermark_deconnect.Visibility = Visibility.Collapsed;
                }

            }



        }

        public void TextBoxGetFocusedDisconnect(object sender, EventArgs e)
        {
            Page_Click();
            MaskedTextBox tt = (MaskedTextBox)sender;
            alphanumeriquekeyboardCarmellia.Visibility = Visibility.Collapsed;
            tt.Foreground = System.Windows.Media.Brushes.Black;
            numeriqueKeyboardCarmellia.Visibility = Visibility.Visible;
            numeriqueKeyboardCarmellia.HorizontalAlignment = HorizontalAlignment.Center;
            numeriqueKeyboardCarmellia.ActiveContainer = tt;
            watermark_deconnect.Visibility = Visibility.Collapsed;
        }

        public void CoordonneeTextBoxRgpdGetFocused(object sender, EventArgs e)
        {
            Page_Click();
            var textbox = sender as TextBox;
            //textbox.Text = ""; 
            if (string.IsNullOrEmpty(textbox.Text) || textbox.Text.Trim() == "Nom *" || textbox.Text.Trim() == "Prénom *" || textbox.Text.Trim() == "E-mail *" || textbox.Text.Trim() == "Code postal *" || textbox.Text.Trim() == "Pays *")
            {
                textbox.Text = "";
            }
            string tag = textbox.Tag.ToString();
            //string tag = hash[type];

            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboardRgpd.Visibility = Visibility.Collapsed;
                numeriqueKeyboardRgpd.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboardRgpd.Visibility = Visibility.Visible;
                numeriqueKeyboardRgpd.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboardRgpd.Visibility = Visibility.Visible;
                alphanumeriquekeyboardRgpd.ActiveContainer = textbox;
                alphanumeriquekeyboardRgpd.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboardRgpd.Visibility = Visibility.Collapsed;
            }

        }

        private double getMarginYForm(int yGrid, int yField, int heightGrid)
        {
            int bottomGrid = yGrid + heightGrid;
            if (bottomGrid > yField)
            {
                return bottomGrid - yField;
            }
            else
            {
                return 0;
            }
        }

        public void CoordonneeTextBoxRgpdLostFocused(object sender, EventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            string keyName = "key_" + textbox.Name;
            if (string.IsNullOrEmpty(textbox.Text) || textbox.Text == "Nom *" || textbox.Text == "Prénom *" || textbox.Text == "E-mail *" || textbox.Text == "Code postal *" || textbox.Text.Trim() == "Pays *")
            {
                string temp = hashTextBox[keyName];
                textbox.Text = temp;
            }
            alphanumeriquekeyboardRgpd.Visibility = Visibility.Collapsed;
            numeriqueKeyboardRgpd.Visibility = Visibility.Collapsed;
        }

        public void CoordonneeMandatoryTextBoxGetFocused(object sender, EventArgs e)
        {
            //TextBox txtBox = (TextBox)sender;
            ////alphanumeriquekeyboard1.Visibility = Visibility.Visible;
            //alphanumeriquekeyboard1.ActiveContainer = txtBox;
            //alphanumeriquekeyboard1.HorizontalAlignment = HorizontalAlignment.Center;
            //if ( txtBox.Text == "E-mail")
            //{
            //    txtBox.Text = "";
            //}

        }

        public bool CheckdateValid(string dateToCheck)
        {
            string[] splitted = dateToCheck.Split('/');
            if(IsNumeric(splitted[0])&& IsNumeric(splitted[1]) && IsNumeric(splitted[2]))
            {
                int year = Convert.ToInt32(splitted[2]);
                int currentyear = Convert.ToInt32(DateTime.Now.Year);
                if (year > 1900 && year < currentyear)
                {
                    string[] formats = { "dd/MM/yyyy" };
                    DateTime parsedDateTime;
                    return DateTime.TryParseExact(dateToCheck, formats, new CultureInfo("en-US"),
                                                   DateTimeStyles.None, out parsedDateTime);
                }
                else return false;
            }
            else return false;
            
        }

        public static T GetChildOfType<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }

        void DatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var dp = sender as DatePicker;
            if (dp == null) return;

            var tb = GetChildOfType<DatePickerTextBox>(dp);
            if (tb == null) return;

            var wm = tb.Template.FindName("PART_Watermark", tb) as ContentControl;
            if (wm == null) return;
            wm.Content = "Date naissance *";

            ImageBrush btnCalendar = new ImageBrush();
            btnCalendar.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnCalendar, UriKind.Absolute));

            Button button = dp.Template.FindName("PART_Button", dp) as Button;
            button.Background = btnCalendar;
            button.Width = 30;
            button.Height = 30;
        }

        private bool CheckPhoneNumber(string phoneNumber)
        {
            if (IsNumeric(phoneNumber))
            {
                string index = phoneNumber.ToString().Substring(0, 2);
                int phoneNumberLength = phoneNumber.ToString().Length;
                bool ok = false;
                bool phonenumberOk = false;
                if (index == "06" || index == "07")
                {
                    ok = true;
                }
                if (ok && phoneNumberLength == 10)
                {
                    phonenumberOk = true;
                }
                return phonenumberOk;
            }
            return false;
        }

        private void gotoCarmelliaCheckConnexion(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;

                }));
                Thread.Sleep(100);
            }
            ).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    bool param_valid = true;
                    if (!CheckdateValid(_birthConnect.Text))
                    {
                        coordonneeMandatoryTitle.Text = "Veuillez sélectionner une date de naissance";
                        scrollCoordonateMandatory.Visibility = Visibility.Visible;
                        scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                        param_valid = false;
                        LoadingPanel.ClosePanel();
                    }
                    if (!CheckPhoneNumber(_portableConnect.Text))
                    {
                        coordonneeMandatoryTitle.Text = "Veuillez saisir un numéro de téléphone valide";
                        scrollCoordonateMandatory.Visibility = Visibility.Visible;
                        scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                        param_valid = false;
                        LoadingPanel.ClosePanel();
                    }
                    if(param_valid)
                    {
                        if (verifyMendatory(_portableConnect.Text, 1, connected))
                        {
                            ToggleButton tg = (ToggleButton)sender;
                            if (tg.Name.ToLower().Contains("yes") && tg.Name.ToLower().Contains("sms"))
                            {
                                receive_sms = "1";
                            }
                            if (tg.Name.ToLower().Contains("no") && tg.Name.ToLower().Contains("sms"))
                            {
                                receive_sms = "0";
                            }
                            if (tg.Name.ToLower().Contains("yes") && tg.Name.ToLower().Contains("mail"))
                            {
                                receive_mail = "1";
                            }
                            if (tg.Name.ToLower().Contains("no") && tg.Name.ToLower().Contains("mail"))
                            {
                                receive_mail = "0";
                            }
                            if (receive_sms == "1")
                            {
                                hashResult[YesReceiveSms_Carmellia.Name] = receive_sms;
                            }
                            else
                            {
                                hashResult[NonReceiveSms_Carmellia.Name] = receive_sms;
                            }
                            scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                            birth_carmellia = _birthConnect.Text;
                            string[] splittedDate = birth_carmellia.Split('/');
                            DateTime dt = new DateTime(Convert.ToInt32(splittedDate[2]), Convert.ToInt32(splittedDate[1]), Convert.ToInt32(splittedDate[0]));
                            phone_number_carmellia = _portableConnect.Text;
                            Globals._PortableConnected = _portableConnect.Text;
                            Globals._BirthConnected = dt.ToString("yyyy-MM-dd HH:mm:ss");
                            hashResult[_birthConnect.Name] = Globals._BirthConnected;
                            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                            {
                                string strBirthday = _birthConnect.Text;
                                String[] spearator = { "/", " " };
                                String[] strlist = strBirthday.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                                strBirthday = strlist[2] + "-" + strlist[1] + "-" + strlist[0];
                                string result = getInfoCarmellia(_portableConnect.Text, strBirthday);

                                CarmelliaObject crmObj = new CarmelliaObject();
                                crmObj.success = false;
                                crmObj = JsonConvert.DeserializeObject<CarmelliaObject>(result);
                                if (crmObj.success)
                                {
                                    sigle = "B";
                                    current_Form_Index = 2;
                                    if (crmObj.data.civilite.ToLower() == "m.")
                                    {
                                        rd_maleKnown.IsChecked = true;
                                    }
                                    else
                                    {
                                        rd_femaleKnown.IsChecked = true;
                                    }
                                    _lastnameCarmelliaKnown.Text = crmObj.data.prenom;
                                    hashResult[_lastnameCarmelliaKnown.Name] = crmObj.data.prenom;
                                    _NameCarmelliaKnown.Text = crmObj.data.nom;
                                    hashResult[_NameCarmelliaKnown.Name] = crmObj.data.prenom;
                                    _emailCarmelliaKnown.Text = crmObj.data.email;
                                    hashResult[_emailCarmelliaKnown.Name] = crmObj.data.prenom;
                                    scrollCoordonateCarmelliaKnown.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    sigle = "C";
                                    current_Form_Index = 3;
                                    scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Visible;

                                }
                            }
                        }
                        LoadingPanel.ClosePanel();
                    }
                    
                    //lbl_loading.Visibility = Visibility.Hidden;

                }));
            }
            );          

        }



        private void gotoCarmelliaPrint(object sender, RoutedEventArgs e)
        {

            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;

                }));
                Thread.Sleep(100);
            }
            ).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if(timerCountdown_From != null)timerCountdown_From.Stop();
                    string currentTextBoxValue = "";
                    string currentdateValid = "";
                    birth_carmellia = "";
                    phone_number_carmellia = "";
                    gender_carmellia = "";
                    name_carmellia = "";
                    firstname_carmellia = "";
                    email_carmellia = "";
                    ToggleButton tg = (ToggleButton)sender;
                    if (current_Form_Index == 2)
                    {
                        currentTextBoxValue = _emailCarmelliaKnown.Text;
                    }
                    else if (current_Form_Index == 3)
                    {
                        currentTextBoxValue = _emailCarmelliaUnKnown.Text;
                    }
                    else if (current_Form_Index == 4)
                    {
                        currentTextBoxValue = _emailCarmellia.Text;
                        currentdateValid = _birth.Text;
                    }
                    //if (!CheckdateValid(currentdateValid))
                    //{
                    //    coordonneeMandatoryTitle.Text = "Veuillez sélectionner une date de naissance";
                    //    scrollCoordonateMandatory.Visibility = Visibility.Visible;
                    //    scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                    //}
                    //else
                    //{
                    if (verifyMendatory(currentTextBoxValue, 2, connected))
                    {
                        if (tg.Name.ToLower().Contains("yes") && tg.Name.ToLower().Contains("sms"))
                        {
                            receive_sms = "1";
                        }
                        if (tg.Name.ToLower().Contains("non") && tg.Name.ToLower().Contains("sms"))
                        {
                            receive_sms = "0";
                        }
                        if (tg.Name.ToLower().Contains("yes") && tg.Name.ToLower().Contains("mail"))
                        {
                            receive_mail = "1";
                        }
                        if (tg.Name.ToLower().Contains("non") && tg.Name.ToLower().Contains("mail"))
                        {
                            receive_mail = "0";
                        }
                        bool resultEmail = true;
                        bool resultPortable = true;
                        if (current_Form_Index == 2)
                        {
                            //resultEmail = CheckEmailValid(_emailCarmelliaKnown.Text);
                            //if (resultEmail)
                            //{
                            if (rd_maleKnown.IsChecked == true)
                            {
                                gender_carmellia = "M.";
                                hashResult[rd_maleKnown.Name] = gender_carmellia;
                            }
                            else
                            {
                                gender_carmellia = "Mme";
                                hashResult[rd_femaleKnown.Name] = gender_carmellia;
                            }
                            if(receive_mail == "1")
                            {
                                hashResult[YesReceiveMail_Carmellia_known.Name] = receive_mail;
                            }
                           else
                            {
                                hashResult[NonReceiveMail_Carmellia_known.Name] = receive_mail;
                            }
                           
                           

                            name_carmellia = _NameCarmelliaKnown.Text;
                            hashResult[_NameCarmelliaKnown.Name] = name_carmellia;
                            firstname_carmellia = _lastnameCarmelliaKnown.Text;
                            hashResult[_lastnameCarmelliaKnown.Name] = firstname_carmellia;
                            email_carmellia = _emailCarmelliaKnown.Text;
                            birth_carmellia = Globals._BirthConnected;
                            phone_number_carmellia = Globals._PortableConnected;
                            hashResult[_emailCarmelliaKnown.Name] = email_carmellia;
                            //}
                        }
                        else if (current_Form_Index == 3)
                        {
                            //resultEmail = CheckEmailValid(_emailCarmelliaUnKnown.Text);
                            //if (resultEmail)
                            //{
                            if (rd_maleUnKnown.IsChecked == true)
                            {
                                gender_carmellia = "M.";
                                hashResult[rd_maleUnKnown.Name] = gender_carmellia;
                            }
                            else
                            {
                                gender_carmellia = "Mme";
                                hashResult[rd_femaleUnKnown.Name] = gender_carmellia;
                            }
                            name_carmellia = _NameCarmelliaUnKnown.Text;
                            hashResult[_NameCarmelliaUnKnown.Name] = name_carmellia;
                            firstname_carmellia = _lastnameCarmelliaUnKnown.Text;
                            hashResult[_lastnameCarmelliaUnKnown.Name] = firstname_carmellia;
                            email_carmellia = _emailCarmelliaUnKnown.Text;
                            hashResult[_emailCarmelliaUnKnown.Name] = email_carmellia;
                            birth_carmellia = Globals._BirthConnected;
                            phone_number_carmellia = Globals._PortableConnected;
                            //}
                            if (receive_mail == "1")
                            {
                                hashResult[YesReceiveMail_Carmellia_Unknown.Name] = receive_mail;
                            }
                            else
                            {
                                hashResult[NonReceiveMail_Carmellia_Unknown.Name] = receive_mail;
                            }
                        }
                        else if (current_Form_Index == 4)
                        {
                            //resultEmail = CheckEmailValid(_emailCarmellia.Text);
                            //resultPortable = CheckPortableValid(_portable.Text);
                            //if (resultEmail && resultPortable)
                            //{

                            birth_carmellia = _birth.Text;
                            String[] spearator = { "/", " " };
                            String[] strlist = birth_carmellia.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                            DateTime dt = new DateTime(Convert.ToInt32(strlist[2]), Convert.ToInt32(strlist[1]), Convert.ToInt32(strlist[0]));
                            birth_carmellia = dt.ToString("yyyy-MM-dd HH:mm:ss");
                            hashResult[_birth.Name] = birth_carmellia;
                            phone_number_carmellia = _portable.Text;
                            hashResult[_portable.Name] = phone_number_carmellia;
                            if (rd_male.IsChecked == true)
                            {
                                gender_carmellia = "M.";
                                hashResult[rd_male.Name] = gender_carmellia;
                            }
                            else
                            {
                                gender_carmellia = "Mme";
                                hashResult[rd_female.Name] = gender_carmellia;
                            }

                            name_carmellia = _NameCarmellia.Text;
                            hashResult[_NameCarmellia.Name] = name_carmellia;
                            firstname_carmellia = _lastnameCarmellia.Text;
                            hashResult[_lastnameCarmellia.Name] = firstname_carmellia;
                            email_carmellia = _emailCarmellia.Text;
                            hashResult[_emailCarmellia.Name] = email_carmellia;
                            if (receive_mail == "1")
                            {
                                hashResult[YesReceivemail_Carmellia_Disconnect.Name] = receive_mail;
                            }
                            else
                            {
                                hashResult[NonReceivemail_Carmellia_Disconnect.Name] = receive_mail;
                            }
                            //}
                        }
                        if (resultEmail && resultPortable)
                        {
                            scrollCoordonateCarmellia.Visibility = Visibility.Collapsed;
                            scrollCoordonateCarmelliaKnown.Visibility = Visibility.Collapsed;
                            scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                            INIFileManager _ini = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                            int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
                            if(_print > 0)
                            {
                                launchPrinting();
                            }
                            else
                            {
                                if(timerCountdown != null)
                                {
                                    timerCountdown.Start();
                                }
                            }
                            
                        }

                    }
                    //lbl_loading.Visibility = Visibility.Hidden;
                    LoadingPanel.ClosePanel();

                }));
            }
            );
            

        }

        private void gotoRgpdText(object sender, RoutedEventArgs e)
        {
            scrollCoordonateRGPD.Visibility = Visibility.Collapsed;
            if (_Name.Text == "" || _Name.Text == "Nom")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_lastname.Text == "" || _lastname.Text == "Prénom")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_emailRgpd.Text == "" || _emailRgpd.Text == "E-mail *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_postalCode.Text == "" || _postalCode.Text == "Code postal")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (!checkMailAdress(_emailRgpd.Text))
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_country.Text == "" || _emailRgpd.Text == "Pays *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            //else if(Homme.IsChecked == false && Femme.IsChecked == false)
            //{
            //    coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
            //    scrollCoordonateMandatory.Visibility = Visibility.Visible;
            //    current_form = "rgpdcoordonate";
            //}
            else
            {

                form_rgpd_Country = _country.Text;
                form_rgpd_Name = _Name.Text;
                form_rgpd_lastname = _lastname.Text;
                form_rgpd_email = _emailRgpd.Text;
                form_rgpd_postalcode = _postalCode.Text;
                //if(Homme.IsChecked == true)
                //{
                //    form_rgpd_gender = "Homme";
                //}
                //else
                //{
                //    form_rgpd_gender = "Femme";
                //}
                scrollRgpd.Visibility = Visibility.Visible;
            }


        }

        private void backtcoordonateRgpd(object sender, RoutedEventArgs e)
        {
            form_rgpd_Name = "";
            form_rgpd_lastname = "";
            form_rgpd_email = "";
            form_rgpd_postalcode = "";
            form_rgpd_gender = "";
            scrollCoordonateRGPD.Visibility = Visibility.Visible;
            scrollRgpd.Visibility = Visibility.Collapsed;
        }

        private void backtcoordonate(object sender, RoutedEventArgs e)
        {
            form_email = "";
            scrollCoordonate.Visibility = Visibility.Visible;
            scrollOptinCheckBox.Visibility = Visibility.Collapsed;
        }

        private void goToPrintRgpd(object sender, RoutedEventArgs e)
        {
            scrollRgpd.Visibility = Visibility.Collapsed;
            if (accept.IsChecked == false)
            {
                coordonneeMandatoryTitle.Text = "Veuillez cocher la case \"Accepter\" pour continuer";
                current_form = "textrgpd";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
            }
            else
            {
                form_rgpd_accept = "OUI";
                var nbToPrint = Convert.ToInt32(scrollcopieValue.Text);
                var nbPrintFinal = getNbPrinted() + nbToPrint;

                if ((nbmaxprinting == 0) || (nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
                {
                    System.Windows.MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    Globals.dtPrint = DateTime.Now;
                    writeformData();
                    visualisationImage.Opacity = 0.7;
                    var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
                    _isMultiImpression = isMultiImpression == "1";
                    lbl_CountDown.IsEnabled = false;
                    BinaryFileManager mgrBin = new BinaryFileManager();
                    string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                    if (_isMultiImpression)
                    {

                        if (timerCountdown != null) timerCountdown.Stop();
                        setTimeout();
                        //if (Globals.ScreenType == "SPHERIK")
                        //{
                        //    scrollCopie.Width = 1200;
                        //    scrollCopie.Height = 800;
                        //}
                        scrollCopie.Visibility = Visibility.Visible;

                        //copieValue.Text = _copieValue;
                    }

                    if (!_isMultiImpression)
                    {
                        if (timerCountdown != null) timerCountdown.Stop();
                        setTimeout();
                        //lbl_CurrentPrinting.Content = "Impression en cours....";

                        //nbPrinted = 1;
                        nbPrintFinal = getNbPrinted() + 1;
                        vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                        _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                        int toWrite = mgrBin.readIntData(binFileName);
                        toWrite += Convert.ToInt32(_copieValue);
                        //writeTxtFileData(mediaDataPath,"");
                        writeTxtFileData(mediaDataPath, toWrite.ToString());
                        mgrBin.writeData(toWrite, binFileName);
                        //this.Dispatcher.BeginInvoke((Action)(() =>
                        //{
                        //    lbl_CurrentPrinting.Content = "Impression en cours....";
                        //}));
                        Printing(false);
                    }

                    if (Globals.mailsent)
                    {
                        ReadReplace("OUI");
                    }
                    else
                    {

                    }
                }
            }
        }

        private void goToPrintFromOptinCheckbox(object sender, RoutedEventArgs e)
        {
            scrollOptinCheckBox.Visibility = Visibility.Collapsed;

            if (acceptChoice1.IsChecked == true)
            {
                form_autoriseEmailPhoto = "OUI";
            }
            else
            {
                form_autoriseEmailPhoto = "NON";
            }
            if (acceptChoice2.IsChecked == true)
            {
                form_receive_newsletter = "OUI";
            }
            else
            {
                form_receive_newsletter = "NON";
            }
            if (acceptChoice3.IsChecked == true)
            {
                form_send_coordonates = "OUI";
            }
            else
            {
                form_send_coordonates = "NON";
            }
            if (acceptChoice4.IsChecked == true)
            {
                form_play_game = "OUI";
            }
            else
            {
                form_play_game = "NON";
            }


            var nbToPrint = Convert.ToInt32(scrollcopieValue.Text);
            var nbPrintFinal = getNbPrinted() + nbToPrint;

            if ((nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
            {
                System.Windows.MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Globals.dtPrint = DateTime.Now;
                writeformData();
                visualisationImage.Opacity = 0.7;
                var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
                _isMultiImpression = isMultiImpression == "1";
                lbl_CountDown.IsEnabled = false;
                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                if (_isMultiImpression)
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    //if (Globals.ScreenType == "SPHERIK")
                    //{
                    //    scrollCopie.Width = 1200;
                    //    scrollCopie.Height = 800;
                    //}
                    scrollCopie.Visibility = Visibility.Visible;

                    //copieValue.Text = _copieValue;
                }

                if (!_isMultiImpression)
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    //lbl_CurrentPrinting.Content = "Impression en cours....";

                    //nbPrinted = 1;
                    nbPrintFinal = getNbPrinted() + 1;
                    vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                    _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                    int toWrite = mgrBin.readIntData(binFileName);
                    toWrite += Convert.ToInt32(_copieValue);
                    //writeTxtFileData(mediaDataPath,"");
                    writeTxtFileData(mediaDataPath, toWrite.ToString());
                    mgrBin.writeData(toWrite, binFileName);
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lbl_CurrentPrinting.Content = "Impression en cours....";
                    }));
                    Printing(false);
                }

                if (Globals.mailsent)
                {
                    ReadReplace("OUI");
                }
                else
                {

                }
            }

        }

        public Formulaire getPages()
        {
            Globals.nbField = 0;
            string code = appIni.GetSetting("EVENTSCONFIG", "id");
            string pathForm = "C:\\Events\\Assets\\" + code + "\\Form\\content.xml";
            string formName = _inimanager.GetSetting("FORM", "Name");
            string stractivated = _inimanager.GetSetting("FORM", "activated");
            if (stractivated == "1")
            {
                activated = true;
            }
            else
            {
                activated = false;
            }
            if (formName.ToLower() == "dynamic" && !File.Exists(pathForm))
            {
                activated = false;
            }

            XmlManager _xmlManager = new XmlManager();

            List<Formulaire> lstFormulaire = _xmlManager.loadDataForm(pathForm);
            Formulaire currentForm = new Formulaire();
            if (lstFormulaire != null)
            {
                foreach (var form in lstFormulaire)
                {
                    if (form.type.ToLower() == formName.ToLower() && activated)
                    {
                        currentForm = form;
                        typeFormulaire = currentForm.type;
                    }

                }

            }
            else
            {
                if (!string.IsNullOrEmpty(formName) && activated)
                {
                    typeFormulaire = formName;
                }
                //currentForm.type = formName;
                //activated = false;
            }
            if (formName.ToLower() == "carmila" && typeFormulaire.ToLower() == formName.ToLower() && activated)
            {
                typeFormulaire = formName;
            }
            else if (formName.ToLower() == "dynamic" && typeFormulaire.ToLower() == formName.ToLower() && activated)
            {
                typeFormulaire = formName;
            }
            else
            {
                typeFormulaire = "";
                activated = false;
                currentForm = null;
            }

            if (File.Exists(pathForm) && lstFormulaire == null)
            {
                System.Windows.MessageBox.Show("Erreur survenu lors du chargement du fichier Xml contenant le formulaire");
                return null;
            }
            else
            {
                return currentForm;
            }

        }

        public void CoordonneeMandatoryTextBoxLostFocused(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            //alphanumeriquekeyboard1.Visibility = Visibility.Collapsed;

            if (txtBox.Text == "")
            {
                txtBox.Text = "E-mail";
            }
        }

        private void gotoUser(object sender, RoutedEventArgs e)
        {

            if (_email.Text == "" || _email.Text == "e-mail *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "coordonate";
            }
            else if (!checkMailAdress(_email.Text))
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                //alphanumeriquekeyboard1.Visibility = Visibility.Visible;
            }
            else
            {
                scrollCoordonate.Visibility = Visibility.Collapsed;
                form_email = _email.Text;
                if (Particulier.IsChecked == false && Professionnel.IsChecked == false)
                {
                    Particulier.IsChecked = false;
                    Professionnel.IsChecked = false;
                }

                scrolluser.Visibility = Visibility.Visible;
            }
        }

        private void gotoOptinBisCheckBox(object sender, RoutedEventArgs e)
        {

            if (_email.Text == "" || _email.Text == "e-mail *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "coordonate";
            }
            else if (!checkMailAdress(_email.Text))
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                //alphanumeriquekeyboard1.Visibility = Visibility.Visible;
            }
            else
            {
                scrollCoordonate.Visibility = Visibility.Collapsed;
                form_email = _email.Text;


                scrollOptinCheckBox.Visibility = Visibility.Visible;
            }
        }

        private void gotoUserBis(object sender, RoutedEventArgs e)
        {

            if (_email.Text == "" || _emailBis.Text == "E-mail *")
            {
                coordonneeTitleBis.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "coordonate";
            }
            else if (!checkMailAdress(_emailBis.Text))
            {
                coordonneeTitleBis.Text = "Veuillez saisir une adresse e-mail valide.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                //alphanumeriquekeyboard1.Visibility = Visibility.Visible;
            }
            else
            {
                scrollCoordonateBis.Visibility = Visibility.Collapsed;
                form_email = _emailBis.Text;


                scrollTypeUserBis.Visibility = Visibility.Visible;
            }
        }

        private void backtoEmail(object sender, RoutedEventArgs e)
        {
            form_email = "";
            scrolluser.Visibility = Visibility.Collapsed;
            scrollCoordonate.Visibility = Visibility.Visible;
        }

        private void backtoEmailBis(object sender, RoutedEventArgs e)
        {
            form_email = "";
            scrollTypeUserBis.Visibility = Visibility.Collapsed;
            scrollCoordonateBis.Visibility = Visibility.Visible;
        }

        private void gotoManufacturer(object sender, RoutedEventArgs e)
        {
            scrolluser.Visibility = Visibility.Collapsed;
            if (Particulier.IsChecked == false && Professionnel.IsChecked == false)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "user";
            }
            else if (Particulier.IsChecked == true)
            {
                form_profil = "Particulier";
                YesUsePhotos.IsChecked = true;
                NoUsePhotos.IsChecked = false;
                YesReceiveMail.IsChecked = true;
                NoReceiveMail.IsChecked = false;
                YesAllowSendNews.IsChecked = true;
                NoAllowSendNews.IsChecked = false;


                ImageBrush btnyeschecked = new ImageBrush();
                btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                ImageBrush btnnounchecked = new ImageBrush();
                btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                YesUsePhotos.Background = btnyeschecked;
                YesReceiveMail.Background = btnyeschecked;
                YesAllowSendNews.Background = btnyeschecked;

                NoAllowSendNews.Background = btnnounchecked;
                NoReceiveMail.Background = btnnounchecked;
                NoUsePhotos.Background = btnnounchecked;


                scrollOptin.Visibility = Visibility.Visible;
            }
            else
            {
                form_profil = "Professionnel";
                if (Fabricant.IsChecked == false && Utilisateur.IsChecked == false)
                {
                    Fabricant.IsChecked = false;
                    Utilisateur.IsChecked = false;
                }

                scrollManufacturer.Visibility = Visibility.Visible;
            }


        }



        private void gotoJob(object sender, RoutedEventArgs e)
        {
            scrollManufacturer.Visibility = Visibility.Collapsed;
            if (Fabricant.IsChecked == false && Utilisateur.IsChecked == false)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "profil";
            }
            else if (Fabricant.IsChecked == true)
            {
                form_position = "Fabricant";
                YesUsePhotos.IsChecked = true;
                NoUsePhotos.IsChecked = false;
                YesReceiveMail.IsChecked = true;
                NoReceiveMail.IsChecked = false;
                YesAllowSendNews.IsChecked = true;
                NoAllowSendNews.IsChecked = false;
                ImageBrush btnyeschecked = new ImageBrush();
                btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                ImageBrush btnnounchecked = new ImageBrush();
                btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                YesUsePhotos.Background = btnyeschecked;
                YesReceiveMail.Background = btnyeschecked;
                YesAllowSendNews.Background = btnyeschecked;

                NoAllowSendNews.Background = btnnounchecked;
                NoReceiveMail.Background = btnnounchecked;
                NoUsePhotos.Background = btnnounchecked;
                scrollOptin.Visibility = Visibility.Visible;
            }
            else
            {
                form_position = "Utilisateur";
                scrollTypeManufacturer.Visibility = Visibility.Visible;
            }

        }

        private void backtoUser(object sender, RoutedEventArgs e)
        {
            form_profil = "";
            scrollManufacturer.Visibility = Visibility.Collapsed;
            scrolluser.Visibility = Visibility.Visible;
        }

        private void backtoActivitySector(object sender, RoutedEventArgs e)
        {
            form_activitysector = "";
            scrollMissionUserBis.Visibility = Visibility.Collapsed;
            scrollTypeConstructorBis.Visibility = Visibility.Visible;
        }

        private void gotoSector(object sender, RoutedEventArgs e)
        {
            scrollTypeManufacturer.Visibility = Visibility.Collapsed;
            if (cbManufaturer.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "job";
            }
            else
            {
                form_job = (cbManufaturer.SelectedItem as ComboBoxItem).Content.ToString();

                if (form_job.ToLower() == "artisan")
                {
                    scrollSector.Visibility = Visibility.Visible;
                }
                else
                {
                    YesUsePhotos.IsChecked = true;
                    NoUsePhotos.IsChecked = false;
                    YesReceiveMail.IsChecked = true;
                    NoReceiveMail.IsChecked = false;
                    YesAllowSendNews.IsChecked = true;
                    NoAllowSendNews.IsChecked = false;
                    ImageBrush btnyeschecked = new ImageBrush();
                    btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                    ImageBrush btnnounchecked = new ImageBrush();
                    btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                    YesUsePhotos.Background = btnyeschecked;
                    YesReceiveMail.Background = btnyeschecked;
                    YesAllowSendNews.Background = btnyeschecked;

                    NoAllowSendNews.Background = btnnounchecked;
                    NoReceiveMail.Background = btnnounchecked;
                    NoUsePhotos.Background = btnnounchecked;
                    scrollOptin.Visibility = Visibility.Visible;
                    scrollOptin.Visibility = Visibility.Visible;
                }


            }

        }

        private void combo_SelectionChanged(object sender, EventArgs e)
        {
            var combo = (ComboBox)sender;
            int selected = combo.SelectedIndex;
            string value = combo.Items[selected].ToString();
            hashResult[combo.Name] = value;

        }

        private void gotoTypeConstructor(object sender, RoutedEventArgs e)
        {
            scrollTypeUserBis.Visibility = Visibility.Collapsed;
            if (cbTypeUserBis.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "typeuser";
            }
            else
            {
                form_typeuser = (cbTypeUserBis.SelectedItem as ComboBoxItem).Content.ToString();

                if (form_typeuser.ToLower() == "professionnel")
                {
                    scrollTypeConstructorBis.Visibility = Visibility.Visible;
                }
                else if (form_typeuser.ToLower() == "collectivité")
                {
                    scrollActivitySectorBis.Visibility = Visibility.Visible;
                }
                else
                {
                    YesUsePhotos.IsChecked = true;
                    NoUsePhotos.IsChecked = false;
                    YesReceiveMail.IsChecked = true;
                    NoReceiveMail.IsChecked = false;
                    YesAllowSendNews.IsChecked = true;
                    NoAllowSendNews.IsChecked = false;
                    ImageBrush btnyeschecked = new ImageBrush();
                    btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                    ImageBrush btnnounchecked = new ImageBrush();
                    btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                    YesUsePhotos.Background = btnyeschecked;
                    YesReceiveMail.Background = btnyeschecked;
                    YesAllowSendNews.Background = btnyeschecked;

                    NoAllowSendNews.Background = btnnounchecked;
                    NoReceiveMail.Background = btnnounchecked;
                    NoUsePhotos.Background = btnnounchecked;
                    scrollOptin.Visibility = Visibility.Visible;
                    scrollOptin.Visibility = Visibility.Visible;
                }


            }

        }

        private void gotoActivitySectorBis(object sender, RoutedEventArgs e)
        {
            scrollTypeConstructorBis.Visibility = Visibility.Collapsed;
            if (cbTypeConstructorBis.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "typeconstructor";
            }
            else
            {
                form_typeconstructor = (cbTypeConstructorBis.SelectedItem as ComboBoxItem).Content.ToString();

                //if (form_typeconstructor.ToLower() == "distributeur")
                //{
                scrollMissionUserBis.Visibility = Visibility.Visible;
                //}
                //else
                //{
                //    YesUsePhotos.IsChecked = true;
                //    NoUsePhotos.IsChecked = false;
                //    YesReceiveMail.IsChecked = true;
                //    NoReceiveMail.IsChecked = false;
                //    YesAllowSendNews.IsChecked = true;
                //    NoAllowSendNews.IsChecked = false;
                //    ImageBrush btnyeschecked = new ImageBrush();
                //    btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                //    ImageBrush btnnounchecked = new ImageBrush();
                //    btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                //    YesUsePhotos.Background = btnyeschecked;
                //    YesReceiveMail.Background = btnyeschecked;
                //    YesAllowSendNews.Background = btnyeschecked;

                //    NoAllowSendNews.Background = btnnounchecked;
                //    NoReceiveMail.Background = btnnounchecked;
                //    NoUsePhotos.Background = btnnounchecked;
                //    scrollOptin.Visibility = Visibility.Visible;
                //    scrollOptin.Visibility = Visibility.Visible;
                //}


            }

        }

        private void gotoMissionUserBis(object sender, RoutedEventArgs e)
        {
            scrollActivitySectorBis.Visibility = Visibility.Collapsed;
            if (cbActivitySectorBis.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "activitysector";
            }
            else
            {
                form_activitysector = (cbActivitySectorBis.SelectedItem as ComboBoxItem).Content.ToString();

                if (form_activitysector.ToLower() == "aménagement urbain")
                {
                    scrollMissionUserBis.Visibility = Visibility.Visible;
                }
                else
                {
                    YesUsePhotos.IsChecked = true;
                    NoUsePhotos.IsChecked = false;
                    YesReceiveMail.IsChecked = true;
                    NoReceiveMail.IsChecked = false;
                    YesAllowSendNews.IsChecked = true;
                    NoAllowSendNews.IsChecked = false;
                    ImageBrush btnyeschecked = new ImageBrush();
                    btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                    ImageBrush btnnounchecked = new ImageBrush();
                    btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                    YesUsePhotos.Background = btnyeschecked;
                    YesReceiveMail.Background = btnyeschecked;
                    YesAllowSendNews.Background = btnyeschecked;

                    NoAllowSendNews.Background = btnnounchecked;
                    NoReceiveMail.Background = btnnounchecked;
                    NoUsePhotos.Background = btnnounchecked;
                    scrollOptin.Visibility = Visibility.Visible;
                    scrollOptin.Visibility = Visibility.Visible;
                }


            }

        }

        private void gotoOptinFromActivitySector(object sender, RoutedEventArgs e)
        {
            scrollActivitySectorBis.Visibility = Visibility.Collapsed;
            if (cbActivitySectorBis.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "activitysector";
            }
            else
            {
                form_activitysector = (cbActivitySectorBis.SelectedItem as ComboBoxItem).Content.ToString();

                //if (form_activitysector.ToLower() == "aménagement urbain")
                //{
                //    scrollMissionUserBis.Visibility = Visibility.Visible;
                //}
                //else
                //{
                YesUsePhotos.IsChecked = true;
                NoUsePhotos.IsChecked = false;
                YesReceiveMail.IsChecked = true;
                NoReceiveMail.IsChecked = false;
                YesAllowSendNews.IsChecked = true;
                NoAllowSendNews.IsChecked = false;
                ImageBrush btnyeschecked = new ImageBrush();
                btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                ImageBrush btnnounchecked = new ImageBrush();
                btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                YesUsePhotos.Background = btnyeschecked;
                YesReceiveMail.Background = btnyeschecked;
                YesAllowSendNews.Background = btnyeschecked;

                NoAllowSendNews.Background = btnnounchecked;
                NoReceiveMail.Background = btnnounchecked;
                NoUsePhotos.Background = btnnounchecked;
                scrollOptin.Visibility = Visibility.Visible;
                scrollOptin.Visibility = Visibility.Visible;
                //}


            }

        }

        private void backtoManufacturer(object sender, RoutedEventArgs e)
        {
            form_position = "";
            scrollTypeManufacturer.Visibility = Visibility.Collapsed;
            scrollManufacturer.Visibility = Visibility.Visible;
        }

        private void backtoTypeUserBis(object sender, RoutedEventArgs e)
        {
            form_typeuser = "";
            scrollTypeConstructorBis.Visibility = Visibility.Collapsed;
            scrollTypeUserBis.Visibility = Visibility.Visible;
        }

        private void backtoTypeConstructorBis(object sender, RoutedEventArgs e)
        {
            form_typeconstructor = "";
            scrollActivitySectorBis.Visibility = Visibility.Collapsed;
            scrollTypeUserBis.Visibility = Visibility.Visible;
        }

        private void backtoJob(object sender, RoutedEventArgs e)
        {
            form_job = "";
            scrollSector.Visibility = Visibility.Collapsed;
            scrollTypeManufacturer.Visibility = Visibility.Visible;
        }

        private void gotoOptin(object sender, RoutedEventArgs e)
        {
            scrollSector.Visibility = Visibility.Collapsed;
            if (ActivityArea.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "sector";
            }
            else
            {
                form_activity_area = (ActivityArea.SelectedItem as ComboBoxItem).Content.ToString();
                YesUsePhotos.IsChecked = true;
                NoUsePhotos.IsChecked = false;
                YesReceiveMail.IsChecked = true;
                NoReceiveMail.IsChecked = false;
                YesAllowSendNews.IsChecked = true;
                NoAllowSendNews.IsChecked = false;
                ImageBrush btnyeschecked = new ImageBrush();
                btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                ImageBrush btnnounchecked = new ImageBrush();
                btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                YesUsePhotos.Background = btnyeschecked;
                YesReceiveMail.Background = btnyeschecked;
                YesAllowSendNews.Background = btnyeschecked;

                NoAllowSendNews.Background = btnnounchecked;
                NoReceiveMail.Background = btnnounchecked;
                NoUsePhotos.Background = btnnounchecked;

                scrollOptin.Visibility = Visibility.Visible;
            }

        }

        private void gotoOptinBis(object sender, RoutedEventArgs e)
        {
            scrollMissionUserBis.Visibility = Visibility.Collapsed;
            if (cbAMissionUserBis.SelectedIndex == -1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le champ obligatoire.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "missionuser";
            }
            else
            {
                form_usermission = (cbAMissionUserBis.SelectedItem as ComboBoxItem).Content.ToString();
                YesUsePhotos.IsChecked = true;
                NoUsePhotos.IsChecked = false;
                YesReceiveMail.IsChecked = true;
                NoReceiveMail.IsChecked = false;
                YesAllowSendNews.IsChecked = true;
                NoAllowSendNews.IsChecked = false;
                ImageBrush btnyeschecked = new ImageBrush();
                btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                ImageBrush btnnounchecked = new ImageBrush();
                btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));
                YesUsePhotos.Background = btnyeschecked;
                YesReceiveMail.Background = btnyeschecked;
                YesAllowSendNews.Background = btnyeschecked;

                NoAllowSendNews.Background = btnnounchecked;
                NoReceiveMail.Background = btnnounchecked;
                NoUsePhotos.Background = btnnounchecked;

                scrollOptin.Visibility = Visibility.Visible;
            }

        }

        private void backtoSector(object sender, RoutedEventArgs e)
        {

            scrollOptin.Visibility = Visibility.Collapsed;
            if (!string.IsNullOrEmpty(form_usermission))
            {
                form_usermission = "";
                scrollMissionUserBis.Visibility = Visibility.Visible;
                //if (!string.IsNullOrEmpty(form_activitysector))
                //{
                //    if (string.IsNullOrEmpty(form_typeconstructor))
                //    {
                //        form_typeuser = "";
                //        scrollTypeUserBis.Visibility = Visibility.Visible;
                //    }
                //    else
                //    {
                //        form_typeconstructor = "";
                //        scrollTypeConstructorBis.Visibility = Visibility.Visible;
                //    }

                //}else
                //{
                //    form_activitysector = "";
                //    scrollActivitySectorBis.Visibility = Visibility.Visible;
                //}
            }
            else if (!string.IsNullOrEmpty(form_activitysector))
            {
                form_activitysector = "";
                scrollActivitySectorBis.Visibility = Visibility.Visible;
                //if(form_usermission == "")
                //{
                //    form_activitysector = "";
                //    scrollActivitySectorBis.Visibility = Visibility.Visible;
                //}
                //else
                //{
                //    form_usermission = "";
                //    scrollMissionUserBis.Visibility = Visibility.Visible;
                //}

            }
            else
            {
                form_typeuser = "";
                scrollTypeUserBis.Visibility = Visibility.Visible;
            }


        }

        private void gotoPrint(object sender, RoutedEventArgs e)
        {
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (YesReceiveMail.IsChecked == true)
            {
                form_autoriseEmailPhoto = "OUI";
            }
            else
            {
                form_autoriseEmailPhoto = "NON";
            }
            if (YesUsePhotos.IsChecked == true)
            {
                form_diffuseEmailPhoto = "OUI";
            }
            else
            {
                form_diffuseEmailPhoto = "NON";
            }
            if (YesAllowSendNews.IsChecked == true)
            {
                form_SendActuality = "OUI";
            }
            else
            {
                form_SendActuality = "NON";
            }
            writeformData();
            scrollCoordonate.Visibility = Visibility.Collapsed;
            scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            scrolluser.Visibility = Visibility.Collapsed;
            scrollManufacturer.Visibility = Visibility.Collapsed;
            scrollTypeManufacturer.Visibility = Visibility.Collapsed;
            scrollSector.Visibility = Visibility.Collapsed;
            scrollOptin.Visibility = Visibility.Collapsed;


            var nbToPrint = Convert.ToInt32(scrollcopieValue.Text);
            var nbPrintFinal = getNbPrinted() + nbToPrint;

            if ((nbmaxprinting == 0) || (nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
            {
                System.Windows.MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Globals.dtPrint = DateTime.Now;
                visualisationImage.Opacity = 0.7;
                var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
                _isMultiImpression = isMultiImpression == "1";
                lbl_CountDown.IsEnabled = false;
                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                if (_isMultiImpression)
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    scrollCopie.Visibility = Visibility.Visible;

                    //copieValue.Text = _copieValue;
                }

                if (!_isMultiImpression)
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    //lbl_CurrentPrinting.Content = "Impression en cours....";

                    //nbPrinted = 1;
                    nbPrintFinal = getNbPrinted() + 1;
                    vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                    _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                    int toWrite = mgrBin.readIntData(binFileName);
                    toWrite += Convert.ToInt32(_copieValue);
                    //writeTxtFileData(mediaDataPath,"");
                    writeTxtFileData(mediaDataPath, toWrite.ToString());
                    mgrBin.writeData(toWrite, binFileName);
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        lbl_CurrentPrinting.Content = "Impression en cours....";
                    }));
                    Printing(false);
                }

                if (Globals.mailsent)
                {
                    ReadReplace("OUI");
                }
                else
                {

                }


            }
            //vbtnImprimer.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private string removeChar(string txt)
        {
            var charsToRemove = new string[] { "\t", "  " };
            foreach (var c in charsToRemove)
            {
                txt = txt.Replace(c, string.Empty);
            }
            return txt;
        }

        private void ChangeToggleEvent(object sender, EventArgs e)
        {
            Page_Click();
            ImageBrush btnnochecked = new ImageBrush();
            btnnochecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoChecked, UriKind.Absolute));

            ImageBrush btnyeschecked = new ImageBrush();
            btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));


            ImageBrush btnnounchecked = new ImageBrush();
            btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));


            ImageBrush btnyesunchecked = new ImageBrush();
            btnyesunchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesUnChecked, UriKind.Absolute));


            ToggleButton control = (ToggleButton)sender;
            string _splittedString = "";
            string yesno = "";
            foreach (ToggleButton tgTemp in _lst_Optin)
            {
                if (control.Name == tgTemp.Name)
                {
                    char[] separator = new char[] { '_' };
                    string[] splitted = tgTemp.Name.Split(separator);
                    _splittedString = splitted[0] + "_" + splitted[1];
                    if (tgTemp.Name.Split('_').Last() == "yes")
                    {
                        yesno = "yes";
                    }
                    else { yesno = "no"; }

                }
            }
            string _nameToEdit = "";
            if (yesno == "yes")
            {
                _nameToEdit = _splittedString + "_no";
            }
            else
            {
                _nameToEdit = _splittedString + "_yes";
            }
            if (control.IsChecked == true)
            {
                foreach (ToggleButton tg in _lst_Optin)
                {
                    if (tg.Name == _nameToEdit)
                    {
                        if (yesno == "yes")
                        {
                            tg.IsChecked = true;
                            tg.Background = btnnounchecked;
                            control.Background = btnyeschecked;
                            hashResult[_splittedString] = "OUI";
                        }
                        else
                        {
                            tg.IsChecked = true;
                            tg.Background = btnyesunchecked;
                            control.Background = btnnochecked;
                            hashResult[_splittedString] = "NON";
                        }

                    }
                }
            }
            else
            {
                foreach (ToggleButton tg in _lst_Optin)
                {
                    if (tg.Name == _nameToEdit)
                    {
                        if (tg.Name == _nameToEdit)
                        {
                            if (yesno == "yes")
                            {
                                if (tg.IsChecked == false)
                                {
                                    tg.IsChecked = false;
                                    tg.Background = btnnounchecked;
                                    control.Background = btnyeschecked;
                                    hashResult[_splittedString] = "OUI";
                                }
                                else
                                {
                                    tg.IsChecked = true;
                                    tg.Background = btnyeschecked;
                                    control.Background = btnnounchecked;
                                    hashResult[_splittedString] = "OUI";
                                }

                            }
                            else
                            {
                                tg.IsChecked = true;
                                tg.Background = btnyesunchecked;
                                control.Background = btnnochecked;
                                hashResult[_splittedString] = "NON";
                            }

                        }
                    }
                }
            }
        }



        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            string Name = chk.Name;
            if (chk.IsChecked == true)
            {
                hashResult[Name] = "OUI";
            }
            else
            {
                hashResult[Name] = "NON";
            }
        }

        private void hideFormulaire(object sender, RoutedEventArgs e)
        {
            form_email = "";
            form_profil = "";
            form_position = "";
            form_job = "";
            form_activity_area = "";
            form_autoriseEmailPhoto = "";
            form_diffuseEmailPhoto = "";
            form_SendActuality = "";
            _email.Text = "";
            _emailBis.Text = "";
            form_rgpd_accept = "";
            form_rgpd_email = "";
            form_rgpd_gender = "";
            form_rgpd_lastname = "";
            form_play_game = "";
            form_send_coordonates = "";
            form_receive_newsletter = "";
            form_rgpd_email = "";
            form_rgpd_postalcode = "";
            form_typeuser = "";
            form_typeconstructor = "";
            form_activitysector = "";
            form_usermission = "";
            //email_Mandatory.Text = "";
            acceptChoice1.IsChecked = false;
            acceptChoice2.IsChecked = false;
            acceptChoice3.IsChecked = false;
            acceptChoice4.IsChecked = false;
            cbManufaturer.SelectedIndex = -1;
            ActivityArea.SelectedIndex = -1;
            cbTypeUserBis.SelectedIndex = -1;
            cbTypeConstructorBis.SelectedIndex = -1;
            cbActivitySectorBis.SelectedIndex = -1;
            cbAMissionUserBis.SelectedIndex = -1;
            Fabricant.IsChecked = false;
            Professionnel.IsChecked = false;
            scrollCoordonate.Visibility = Visibility.Collapsed;
            scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            scrolluser.Visibility = Visibility.Collapsed;
            scrollManufacturer.Visibility = Visibility.Collapsed;
            scrollTypeManufacturer.Visibility = Visibility.Collapsed;
            scrollSector.Visibility = Visibility.Collapsed;
            scrollOptin.Visibility = Visibility.Collapsed;
            scrollRgpd.Visibility = Visibility.Collapsed;
            scrollCoordonateRGPD.Visibility = Visibility.Collapsed;
            scrollCoordonateBis.Visibility = Visibility.Collapsed;
            scrollTypeUserBis.Visibility = Visibility.Collapsed;
            scrollOptinCheckBox.Visibility = Visibility.Collapsed;
            scrollTypeConstructorBis.Visibility = Visibility.Collapsed;
            scrollActivitySectorBis.Visibility = Visibility.Collapsed;
            scrollMissionUserBis.Visibility = Visibility.Collapsed;
            scrollCoordonateCarmellia.Visibility = Visibility.Collapsed;
            scrollCoordonateCarmelliaKnown.Visibility = Visibility.Collapsed;
            scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Collapsed;
            scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
            setTimeout();
            LaunchTimerCountdown();
        }


        private void gotoUserFromMendatory(object sender, RoutedEventArgs e)
        {
            scrollCoordonateMandatory.Visibility = Visibility.Collapsed;

            if (!string.IsNullOrEmpty(sigle))
            {
                switch (sigle)
                {
                    case "A":
                        scrollCoordonateConectionCarmellia.Visibility = Visibility.Visible;
                        break;
                    case "B":
                        scrollCoordonateCarmelliaKnown.Visibility = Visibility.Visible;
                        break;
                    case "C":
                        scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Visible;
                        break;
                    case "D":
                        scrollCoordonateCarmellia.Visibility = Visibility.Visible;
                        break;
                }
            }
            else
            {
                switch (current_page)
                {
                    case 1:
                        Page_1.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        Page_2.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        Page_3.Visibility = Visibility.Visible;
                        break;
                    case 4:
                        Page_4.Visibility = Visibility.Visible;
                        current_page = 5;
                        break;
                    case 5:
                        Page_5.Visibility = Visibility.Visible;
                        break;
                    case 6:
                        Page_6.Visibility = Visibility.Visible;
                        break;
                    case 7:
                        Page_7.Visibility = Visibility.Visible;
                        break;
                    case 8:
                        Page_8.Visibility = Visibility.Visible;
                        break;
                    case 9:
                        Page_9.Visibility = Visibility.Visible;
                        break;
                    case 10:
                        Page_10.Visibility = Visibility.Visible;
                        break;
                }
            }
            if (currentTextBox != null)
            {
                currentTextBox.Focus();
            }
            //    if (current_form == "coordonate")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollCoordonate.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "user")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrolluser.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "profil")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollManufacturer.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "job")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollTypeManufacturer.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "sector")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollSector.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "rgpdcoordonate")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollCoordonateRGPD.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "textrgpd")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollRgpd.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "typesuser")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollTypeUserBis.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "typeconstructor")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollTypeConstructorBis.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "activitysector")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollActivitySectorBis.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "missionuser")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollMissionUserBis.Visibility = Visibility.Visible;
            //    }
            //    else if (current_form == "optinCheckBox")
            //    {
            //        scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            //        scrollOptinCheckBox.Visibility = Visibility.Visible;
            //    }
        }

        private void GetPrinterInfo(object sender, RoutedEventArgs e)
        {
            try
            {
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                string printer = LocalPrintServer.GetDefaultPrintQueue().FullName;
                showPrinterInfo.Visibility = Visibility.Visible;
                getPrinterInfo(printer);

            }
            catch (Exception ex)
            {

            }
        }

        public void openPort()
        {
            try
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    if (port == null)
                    {
                        port = new SerialPort(Globals.portName, Globals.portNumber, Parity.None, 8, StopBits.One);
                        port.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                        port.Open();
                    }
                }

            }
            catch (UnauthorizedAccessException) { }
            catch (System.IO.IOException) { }
            catch (ArgumentException) { }
        }

        public void setTimeout()
        {
            lbl_CountDown.Content = "";
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }

        private int DefaultCopie()
        {
            //var inimanager = new INIFileManager($"{Globals.ExeDir()}\\Config.ini");
            var _copies = _inimanager.GetSetting("PRINTING", "copyNumber");
            int copies = 1;
            if (int.TryParse(_copies, out copies))
                copies = Convert.ToInt32(_copies);
            return copies;
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0 )
            //{
            //    setTimeout();
            //}
            Globals.timeOut--;
            if (Globals.timeOut <= 0)
            {
                lbl_CountDown.Content = "";
            }
            else
            {
                lbl_CountDown.Content = Globals.timeOut;
            }

            if (Globals.timeOut < 0)
            {
                lbl_CountDown.Content = "";
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                if (Globals.printed == true)
                {
                    GoToThank();
                }
                else
                {
                    CallTakePicturePage();
                }


            }
        }

        void timerCountdown_Form_Tick(object sender, EventArgs e)
        {
            Globals.timeOut--;
            if (Globals.timeOut <= 0)
            {
                lbl_CountDownPrint.Content = "";
            }
            else
            {
                lbl_CountDownPrint.Content = Globals.timeOut;
            }

            if (Globals.timeOut < 0)
            {
                lbl_CountDown.Content = "";
                if (timerCountdown != null) timerCountdown_From.Stop();
                setTimeout();
                if (Globals.printed == true)
                {
                    GoToThank();
                }
                else
                {
                    CallTakePicturePage();
                }


            }
        }

        public void Page_Click(object sender, RoutedEventArgs e)
        {
            
            StkpHome_Print.Visibility = Visibility.Hidden;
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            setTimeout();
            LaunchTimerCountdown_Print();
        }

        public void Page_Click()
        {

            StkpHome_Print.Visibility = Visibility.Hidden;
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            setTimeout();
            LaunchTimerCountdown_Print();
        }


        private void GoToAcceuil()
        {
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAcceuil.CanExecute(null))
                    viewModel.GoToAcceuil.Execute(null);
            }
        }

        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        public void LaunchTimerCountdown()
        {
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            StkpHome.Visibility = Visibility.Visible;
            StkpHome_Print.Visibility = Visibility.Hidden;
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        public void LaunchTimerCountdown_Print()
        {
            timerCountdown_print = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_print.Interval = TimeSpan.FromMinutes(2);
            timerCountdown_print.Tick += new EventHandler(LaunchTimerCountdown_Form);
            timerCountdown_print.Start();
        }

        public void LaunchTimerCountdown_Form(object sender, EventArgs e)
        {
            StkpHome_Print.Visibility = Visibility.Visible;
            timerCountdown_From = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_From.Interval = TimeSpan.FromSeconds(1);
            timerCountdown_From.Tick += new EventHandler(timerCountdown_Form_Tick);
            timerCountdown_From.Start();
            timerCountdown_print.Stop();
        }

        private void CallTakePicturePage()
        {
            //SaveCSV();
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToTakePhoto.CanExecute(null))
                    viewModel.GoToTakePhoto.Execute(null);
            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            
            try
            {
                
                LaunchTimerCountdown();
                this.Focusable = true;
                Keyboard.Focus(this);
                //Keyboard.ClearFocus();
                string directoryImage = "";
                FileInfo image = GetFiltred();
                var sizeInBytes = image.Length;

                Bitmap imge = new Bitmap(image.FullName);

                var imageHeight = imge.Height;
                var imageWidth = imge.Width;
                double width = LVCanvas.MaxWidth;
                double heught = LVCanvas.MaxHeight;
                int x = (int)SystemParameters.PrimaryScreenWidth;
                int y = (int)SystemParameters.PrimaryScreenHeight;
                if (imageWidth > imageHeight)
                {
                    if (Globals.ScreenType == "SPHERIK")
                    {


                        LVCanvas.Width = imageWidth / 2;//imageWidth - (imageWidth / 5);
                        LVCanvas.Height = imageHeight / 2;//imageHeight - (imageHeight /6); //2.5
                        LVCanvas.Width = LVCanvas.Width - (LVCanvas.Width / 6);
                        LVCanvas.Height = LVCanvas.Height - (LVCanvas.Height / 4);

                        imageBorder.Width = LVCanvas.Width + 40;
                        imageBorder.Height = LVCanvas.Height + 40;


                        int cc = (int)VisualisationGrid.Height;
                        int z = (x - (int)VisualisationGrid.Width) / 2;
                        int marginWidth = (x - (int)imageBorder.Width) / 2;
                        int marginHeight = (cc - (int)imageBorder.Height) / 2;
                        marginHeight = marginHeight - ((y - cc) / 2);

                        imageBorder.Margin = new Thickness(0, marginHeight, 0, 0);
                        vbtnImprimer.Margin = new Thickness(z, marginHeight, 0, 0);



                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        LVCanvas.Width = imageWidth - (imageWidth / 2.5);
                        LVCanvas.Height = imageHeight - (imageHeight / 3); //2.5
                        LVCanvas.Width = LVCanvas.Width - (LVCanvas.Width / 6);
                        LVCanvas.Height = LVCanvas.Height - (LVCanvas.Height / 4);

                        imageBorder.Width = LVCanvas.Width + 40;
                        imageBorder.Height = LVCanvas.Height + 40;
                        int cc = (int)VisualisationGrid.Height;
                        int z = (x - (int)VisualisationGrid.Width) / 2;
                        int marginWidth = (x - (int)imageBorder.Width) / 2;
                        int marginHeight = (cc - (int)imageBorder.Height) / 2;
                        marginHeight = marginHeight - ((y - cc) / 2);

                        imageBorder.Margin = new Thickness(0, marginHeight, 0, 0);
                        vbtnImprimer.Margin = new Thickness(z, marginHeight, 0, 0);
                    }

                }
                else
                {
                    double diffWidth = 0;
                    double diffHeight = 0;
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        if (Globals.EventChosen == 1)
                        {
                            diffWidth = (imageWidth / 3);
                            diffHeight = (imageHeight / 2.5);
                        }
                        else
                        {
                            diffWidth = (imageWidth / 3);
                            diffHeight = (imageHeight / 2.5);
                        }

                        LVCanvas.Width = diffWidth;//imageWidth - diffWidth;
                        LVCanvas.Height = diffHeight;//imageHeight - diffHeight;

                        LVCanvas.Width = LVCanvas.Width - (LVCanvas.Width / 6);
                        LVCanvas.Height = LVCanvas.Height - (LVCanvas.Height / 3.5);

                        imageBorder.Width = LVCanvas.Width + 40;
                        imageBorder.Height = LVCanvas.Height + 40;

                        int cc = (int)VisualisationGrid.Height;
                        int z = (x - (int)VisualisationGrid.Width) / 2;
                        int marginWidth = (x - (int)imageBorder.Width) / 2;
                        int marginHeight = (cc - (int)imageBorder.Height) / 2;
                        marginHeight = marginHeight - ((y - cc) / 2);

                        imageBorder.Margin = new Thickness(0, marginHeight, 0, 0);
                        vbtnImprimer.Margin = new Thickness(z, marginHeight, 0, 0);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        if (Globals.EventChosen == 1)
                        {
                            diffWidth = (imageWidth / 2.2);
                            diffHeight = (imageHeight / 2.2);
                        }
                        else
                        {
                            diffWidth = (imageWidth / 1.7);
                            diffHeight = (imageHeight / 2);
                        }

                        LVCanvas.Width = imageWidth - diffWidth;
                        LVCanvas.Height = imageHeight - diffHeight;

                        LVCanvas.Width = LVCanvas.Width - (LVCanvas.Width / 6);
                        LVCanvas.Height = LVCanvas.Height - (LVCanvas.Height / 3.5);

                        imageBorder.Width = LVCanvas.Width + 40;
                        imageBorder.Height = LVCanvas.Height + 40;
                        int cc = (int)VisualisationGrid.Height;
                        int z = (x - (int)VisualisationGrid.Width) / 2;
                        int marginWidth = (x - (int)imageBorder.Width) / 2;
                        int marginHeight = (cc - (int)imageBorder.Height) / 2;
                        marginHeight = marginHeight - ((y - cc) / 2);

                        imageBorder.Margin = new Thickness(0, marginHeight, 0, 0);
                        vbtnImprimer.Margin = new Thickness(z, marginHeight, 0, 0);
                    }

                }



                directoryImage = System.IO.Path.GetDirectoryName(image.FullName);
                string imgName = System.IO.Path.GetFileName(image.FullName);
                if (Globals.EventChosen == 1)
                {
                    imgName = Globals.EventMediaFolder() + "\\Photos\\Original\\" + imgName;
                }
                else
                {
                    imgName = Globals.EventMediaFolder() + "\\Photos\\" + imgName;
                }
                LVCanvas.HorizontalAlignment = HorizontalAlignment.Center;
                if (Globals.EventChosen == 1)
                {
                    string imagePath = Globals._finalFolder + "\\" + System.IO.Path.GetFileName(imgName);
                    Bitmap img = new Bitmap(imagePath);
                    string fileName = createStripImage(imagePath, 30);
                    FileInfo imageName = new FileInfo(fileName);
                    //LVCanvas.HorizontalAlignment = HorizontalAlignment.Center;
                    imageBorder.BorderThickness = new Thickness(0, 0, 0, 0);
                    visualisationImage.DataContext = imageName;
                }
                else
                {
                    visualisationImage.DataContext = image;
                }


                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                globalPrinted = getNbPrinted();

                string shortName = System.IO.Path.GetFileNameWithoutExtension(image.FullName);
                DateTime date = DateTime.Now;
                int year = date.Year;
                int month = date.Month;
                int day = date.Day;
                int hour = date.Hour;
                int minute = date.Minute;
                int second = date.Second;

                savingData();
                getBasicInfo();

                //if (File.Exists(Globals._btnVisualisationPrinting))
                //{
                //    BtnContent((Button)vbtnImprimer, "BtnImprimer", "Imprimer", Globals._btnVisualisationPrinting);
                //}
                //else
                //{
                //    BtnContent((Button)vbtnImprimer, "BtnImprimer", "Imprimer", Globals._defaultbtnVisualisationPrinting);
                //}

                //if (File.Exists(Globals._btnVisualisationEmail))
                //{
                //    BtnContent((Button)vbtnSendToMail, "ImgBtnMail", "Envoyer par mail", Globals._btnVisualisationEmail);
                //}
                //else
                //{
                //    BtnContent((Button)vbtnSendToMail, "ImgBtnMail", "Envoyer par mail", Globals._defaultbtnVisualisationEmail);
                //}
                //if (File.Exists(Globals._btnVisualisationPrintEmail))
                //{
                //    BtnContent((Button)vbtnImprimerSendMail, "ImgBtnPrintAndMail", "Imprimer et envoyer par mail", Globals._btnVisualisationPrintEmail);
                //}
                //else
                //{
                //    BtnContent((Button)vbtnImprimerSendMail, "ImgBtnPrintAndMail", "Imprimer et envoyer par mail", Globals._defaultbtnVisualisationPrintEmail);
                //}
                if (File.Exists(Globals._btnVisualisationHelp))
                {
                    BtnContent((Button)vbtnHelp, "ImgBtnHelp", "Aide", Globals._btnVisualisationHelp);
                }
                else
                {
                    BtnContent((Button)vbtnHelp, "ImgBtnHelp", "Aide", Globals._defaultbtnVisualisationHelp);
                }

                if (File.Exists(Globals._btnVisualisationPrintEmail))
                {
                    BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._btnVisualisationHome);
                         BtnContent((Button)vbtnGoToHomePrint, "ImgBtnHome", "Revenir à l'accueil", Globals._btnVisualisationHome);
                    
                }
                else
                {
                    BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._defaultbtnVisualisationHome);
                    BtnContent((Button)vbtnGoToHomePrint, "ImgBtnHome", "Revenir à l'accueil", Globals._defaultbtnVisualisationHome);
                }


                var btnimprimerPosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "PrintButton");
                //var btnterminerPosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "EndButton");
                var btnGoToTakePicturePosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualizationGoToTakePicture");
                var btnGoToFiltrePosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "GoToFiltre");
                var btnSendEmailPosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualizationSendEmail");
                var btnPrintSendEmailPosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualisationSendEmailAndPrint");
                var btnGoToHomePosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualisationBackToHome");

                if (btnimprimerPosition[0] != "")
                    Canvas.SetLeft((Button)vbtnImprimer, Convert.ToInt32(btnimprimerPosition[0]));
                if (btnimprimerPosition[1] != "")
                    Canvas.SetTop((Button)vbtnImprimer, Convert.ToInt32(btnimprimerPosition[1]));
                //if (btnPrintSendEmailPosition[0] != "")
                //    Canvas.SetLeft((Button)vbtnImprimerSendMail, Convert.ToInt32(btnPrintSendEmailPosition[0]));

                if (btnGoToHomePosition[0] != "")
                {
                    Canvas.SetLeft((Button)vbtnGoToHome, Convert.ToInt32(btnGoToHomePosition[0]));
                    Canvas.SetLeft((Button)vbtnGoToHomePrint, Convert.ToInt32(btnGoToHomePosition[0]));
                }
                    

                //if (btnSendEmailPosition[0] != "")
                //    Canvas.SetLeft((Button)vbtnSendToMail, Convert.ToInt32(btnSendEmailPosition[0]));
                //if (btnSendEmailPosition[1] != "")
                //    Canvas.SetTop((Button)vbtnSendToMail, Convert.ToInt32(btnSendEmailPosition[1]));

                #region btnretakepictureconfig
                var enableBtnRetakePicture = _inimanager.GetSetting("BUTTONCONFIG", "EnableButtonRetakePicture");
                //if (enableBtnRetakePicture == "0")
                //{
                //    vbtnGoToTakePicture.Visibility = Visibility.Hidden;
                //}
                var enablePrint = _inimanager.GetSetting("BUTTONCONFIG", "EnableButtonPrint");
                if (enablePrint != "1")
                {
                    vbtnImprimer.Visibility = Visibility.Collapsed;
                }
                var enableSendMail = _inimanager.GetSetting("BUTTONCONFIG", "EnableButtonSendMail");
                //if (enableSendMail != "1")
                //{
                //    vbtnSendToMail.Visibility = Visibility.Collapsed;
                //}
                var enablePrintAndSend = _inimanager.GetSetting("BUTTONCONFIG", "EnableButtonSendAndPrint");
                //if (enablePrintAndSend != "1")
                //{
                //    vbtnImprimerSendMail.Visibility = Visibility.Collapsed;
                //}
                var brush = new ImageBrush();
                brush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationPrinting, UriKind.Relative));
                validateScrollCopie.Background = brush;

                cancelCopie.Background = brushCroix;
                //closeInfos.Background = brushCroix;
                if (Globals.ScreenType == "SPHERIK")
                {
                    //scrollCopie.Width = 1200;
                    //scrollCopie.Height = 800;
                    mainStack.Height = 1000;
                    mainStack.VerticalAlignment = VerticalAlignment.Center;
                }
                else
                {

                    scrollCopie.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                    scrollCopie.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                    mainStack.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - 200;
                    mainStack.VerticalAlignment = VerticalAlignment.Center;
                }
                int diff = 0;
                if (Globals.EventChosen == 1 || Globals.EventChosen == 2)
                {
                    diff = 40;
                }
                else
                {
                    diff = 20;
                }
                int top = Convert.ToInt32(visualisationImage.Margin.Bottom) + diff;
                // buttonContainer.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                Canvas.SetBottom(vbtnImprimer, top);
                vbtnImprimer.HorizontalAlignment = HorizontalAlignment.Center;
                //buttonContainer.HorizontalAlignment = HorizontalAlignment.Center;
                #endregion

                
            }
            catch (Exception ex)
            {
                Log.Error($"Exception : {ex}");
            }
            
        }
        public int getNbPrinted()
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            return mgrBin.readIntData(binFileName);
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            timer_Form = 300;
           
            try
            {
                switch (e.Key)
                {
                    case Key.F1:

                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                //timerCountdown_From.Stop();
                                GC.SuppressFinalize(this);
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_From != null) timerCountdown_From.Stop();
                                if (timerCountdown_print != null) timerCountdown_print.Stop();
                                setTimeout();
                                var viewModel = (VisualisationViewModel)DataContext;
                                Globals._FlagConfig = "client";
                                if (Globals.ScreenType == "SPHERIK")
                                {
                                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                        viewModel.GoToClientSumarySpherik.Execute(null);
                                }
                                else if (Globals.ScreenType == "DEFAULT")
                                {
                                    if (viewModel.GoToClientSumary.CanExecute(null))
                                        viewModel.GoToClientSumary.Execute(null);
                                }
                                /*if (viewModel.GoToConnexionClient.CanExecute(null))
                                    viewModel.GoToConnexionClient.Execute(null);*/
                                // LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                       
                        
                        break;
                    case Key.F2:

                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                GC.SuppressFinalize(this);
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_From != null) timerCountdown_From.Stop();
                                if (timerCountdown_print != null) timerCountdown_print.Stop();
                                setTimeout();
                                Globals._FlagConfig = "admin";
                                var viewModel2 = (VisualisationViewModel)DataContext;
                                if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                    viewModel2.GoToConnexionConfig.Execute(null);
                                // LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void writeTxtFileData(string filename, string toWrite)
        {
            int i = 0;

            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }

        private static void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            Console.WriteLine("Data Received:");
            Console.Write(indata);
        }

        private void ReinitPrinted(object sender, RoutedEventArgs e)
        {
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            setTimeout();
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            mgrBin.writeData(0, binFileName);
            writeTxtFileData(mediaDataPath, "0");
            //lblNbPrinted.Content = "";
            timerCountdown.Start();
        }

        private void ShowButton(object sender, RoutedEventArgs e)
        {
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            setTimeout();
            //buttonUtil.Visibility = Visibility.Visible;
        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {
            var nbToPrint = Convert.ToInt32(scrollcopieValue.Text);
            var nbPrintFinal = getNbPrinted() + nbToPrint;

            if ((nbmaxprinting == 0) || (nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
            {
                System.Windows.MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Globals.dtPrint = DateTime.Now;
                visualisationImage.Opacity = 0.7;
                var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
                _isMultiImpression = isMultiImpression == "1";
                lbl_CountDown.IsEnabled = false;
                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                if (_isMultiImpression)
                {
                    if (timerCountdown_From != null) timerCountdown_From.Stop();
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    scrollCopie.Visibility = Visibility.Visible;
                }

                if (!_isMultiImpression)
                {
                    if (timerCountdown_From != null) timerCountdown_From.Stop();
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    lbl_CurrentPrinting.Content = "Impression en cours....";

                    //nbPrinted = 1;
                    nbPrintFinal = getNbPrinted() + 1;
                    vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                    _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                    int toWrite = mgrBin.readIntData(binFileName);
                    toWrite += Convert.ToInt32(_copieValue);
                    //writeTxtFileData(mediaDataPath,"");
                    writeTxtFileData(mediaDataPath, toWrite.ToString());
                    mgrBin.writeData(toWrite, binFileName);
                    Printing(false);
                }

                if (Globals.mailsent)
                {
                    ReadReplace("OUI");
                }
                else
                {

                }


            }

        }

        private void showHelp(object sender, RoutedEventArgs e)
        {
            GoToHotline();
        }



        private void ClosePrinterInfo(object sender, RoutedEventArgs e)
        {
            showPrinterInfo.Visibility = Visibility.Collapsed;
            timerCountdown.Start();
        }

        private void Printing(bool sendMail)
        {
            var isPrinted = true;
            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;
                    TypeEvent chosenEvent;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    chosenEvent = (TypeEvent)Globals.EventChosen;

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    PrinterSettings printerSettings = pd.PrinterSettings;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    short copies = 1;
                    short.TryParse(_copieValue, out copies);
                    //copies = Convert.ToInt32(_copieValue);

                    printerSettings.Copies = copies;
                    
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                    //pd.PrinterSettings.Copies = copies;
                    var filedoc = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (Globals.EventChosen == 1)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrinterSettings.Copies = copies;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                 //   LocalPrintServer.GetDefaultPrintQueue().AddJob();
                                 //   LocalPrintServer.GetDefaultPrintQueue().Commit();
                                   // isPrinted = false;
                                    System.Windows.MessageBox.Show("PrinterSettings is not updated!");
                                    //LocalPrintServer.GetDefaultPrintQueue().AddJob();
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrinterSettings.Copies = copies;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                   // LocalPrintServer.GetDefaultPrintQueue().AddJob();
                                   // LocalPrintServer.GetDefaultPrintQueue().Commit();
                                   // isPrinted = false;
                                    System.Windows.MessageBox.Show("PrinterSettings is not updated!");
                                    

                                }
                            }

                            lbl_CurrentPrinting.Content = "";
                        }));

                        if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                        {
                            ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                            comMgr.writeData("printed");
                        }
                        string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";

                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    BinaryFileManager mgrBin = new BinaryFileManager();
                    string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                    //nbPrinted = mgrBin.readIntData(binFileName);
                    nbPrinted = copies;
                    ReadReplace("OUI");
                    if (sendMail)
                    {
                        Globals.printed = isPrinted;
                        //var viewModel = (VisualisationViewModel)DataContext;
                        //if (viewModel.GoToFormStepOne.CanExecute(null))
                        //    viewModel.GoToFormStepOne.Execute(null);

                    }
                    else
                    {
                        try
                        {
                            timerCountdown.Start();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message + "\n" + "stacktrace:" + ex.StackTrace.ToString());
                        }
                    }


                }));

            });

            Globals.printed = isPrinted;
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel.GoToThanks.CanExecute(null))
                viewModel.GoToThanks.Execute(null);



        }

        private bool IsPrinterOk(string name, int checkTimeInMillisec)
        {
            System.Collections.IList value = null;
            do
            {
                //checkTimeInMillisec should be between 2000 and 5000
                System.Threading.Thread.Sleep(checkTimeInMillisec);
                // or use Timer with Threading.Monitor instead of thread sleep

                using (System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_PrintJob WHERE Name like '%" + name + "%'"))
                {
                    value = null;

                    if (searcher.Get().Count == 0) // Number of pending document.
                        return true; // return because we haven't got any pending document.
                    else
                    {
                        foreach (System.Management.ManagementObject printer in searcher.Get())
                        {
                            value = printer.Properties.Cast<System.Management.PropertyData>().Where(p => p.Name.Equals("Status")).Select(p => p.Value).ToList();
                            break;
                        }
                    }
                }
            }
            while (value.Contains("Printing") || value.Contains("UNKNOWN") || value.Contains("OK"));

            return value.Contains("Error") ? false : true;
        }

        private List<String> IsPrinterStatusNull(string name, int checkTimeInMillisec)
        {
            System.Collections.IList value = null;
            do
            {
                //checkTimeInMillisec should be between 2000 and 5000
                System.Threading.Thread.Sleep(checkTimeInMillisec);
                // or use Timer with Threading.Monitor instead of thread sleep

                using (System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_PrintJob WHERE Name like '%" + name + "%'"))
                {
                    value = null;

                    if (searcher.Get().Count == 0) // Number of pending document.
                        return null; // return because we haven't got any pending document.
                    else
                    {
                        foreach (System.Management.ManagementObject printer in searcher.Get())
                        {
                            value = printer.Properties.Cast<System.Management.PropertyData>().Where(p => p.Name.Equals("Status")).Select(p => p.Value).ToList();
                            break;
                        }
                    }
                }
            }
            while (value.Contains("Printing") || value.Contains("UNKNOWN") || value.Contains("OK"));

            return value == null ? null : (List<String>)value;
        }

        private void OnQueryPageSettings(object sender, QueryPageSettingsEventArgs e)
        {
            if (Globals.EventChosen == 0)
            {
                if (Globals.portrait)
                {
                    //printerSettings.DefaultPageSettings.Landscape = false;
                    //pd.PrinterSettings.DefaultPageSettings.Landscape = false;
                    e.PageSettings.Landscape = false;
                }
                else
                {
                    //printerSettings.DefaultPageSettings.Landscape = true;
                    pd.PrinterSettings.DefaultPageSettings.Landscape = true;
                    e.PageSettings.Landscape = true;
                }
            }
            else if (Globals.EventChosen == 1)
            {
                //printerSettings.DefaultPageSettings.Landscape = false;
                //pd.PrinterSettings.DefaultPageSettings.Landscape = false;
                e.PageSettings.Landscape = false;
            }
            else if (Globals.EventChosen == 2)
            {
                //printerSettings.DefaultPageSettings.Landscape = false;
                //pd.PrinterSettings.DefaultPageSettings.Landscape = false;
                e.PageSettings.Landscape = false;
            }
            else if (Globals.EventChosen == 3)
            {

                //printerSettings.DefaultPageSettings.Landscape = true;
                //pd.PrinterSettings.DefaultPageSettings.Landscape = true;
                e.PageSettings.Landscape = true;
            }
        }

        private void getPrinterInfo(string printerName)
        {
            // Lookup arrays.
            string[] PrinterStatuses =
            {
        "Other", "Unknown", "Idle", "Printing", "WarmUp",
        "Stopped Printing", "Offline"
    };
            string[] PrinterStates =
            {
        "Paused", "Error", "Pending Deletion", "Paper Jam",
        "Paper Out", "Manual Feed", "Paper Problem",
        "Offline", "IO Active", "Busy", "Printing",
        "Output Bin Full", "Not Available", "Waiting",
        "Processing", "Initialization", "Warming Up",
        "Toner Low", "No Toner", "Page Punt",
        "User Intervention Required", "Out of Memory",
        "Door Open", "Server_Unknown", "Power Save"};

            // Get a ManagementObjectSearcher for the printer.
            string query = "SELECT * FROM Win32_Printer WHERE Name='" +
                printerName + "'";
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher(query);

            // Get the ManagementObjectCollection representing
            // the result of the WMI query. Loop through its
            // single item. Display some of that item's properties.
            foreach (ManagementObject service in searcher.Get())
            {
                txtPrinter.Text = service.Properties["Name"].Value.ToString();

                UInt32 state =
                    (UInt32)service.Properties["PrinterState"].Value;
                txtEtat.Text =
                    PrinterStates[state];

                UInt16 status =
                    (UInt16)service.Properties["PrinterStatus"].Value;
                txtStatus.Text = PrinterStatuses[status];

                //UInt16 paperNumber = (UInt16)service.Properties["PrinterPaperNumber"].Value;

                txtDescription.Text =
                    GetPropertyValue(service.Properties["Description"]);
                txtHorRes.Text =
                    GetPropertyValue(service.Properties["HorizontalResolution"]);
                txtVerRes.Text =
                    GetPropertyValue(service.Properties["VerticalResolution"]);
                txtPort.Text =
                    GetPropertyValue(service.Properties["PortName"]);

                lstPaper.Items.Clear();
                string[] paper_sizes =
                    (string[])service.Properties["PrinterPaperNames"].Value;
                foreach (string paper_size in paper_sizes)
                {
                    lstPaper.Items.Add(paper_size);
                }

                // List the available properties.
                foreach (PropertyData data in service.Properties)
                {
                    string txt = data.Name;
                    if (data.Value != null)
                        txt += ": " + data.Value.ToString();
                    Console.WriteLine(txt);
                }

            }
        }

        private string GetPropertyValue(PropertyData data)
        {
            if ((data == null) || (data.Value == null)) return "";
            return data.Value.ToString();
        }

        private void savingData()
        {
            var image = GetFiltred();
            string format = "";
            switch (Globals.EventChosen)
            {
                case 0:
                    format = "CADRE";
                    break;
                case 1:
                    format = "STRIP";
                    break;
                case 2:
                    format = "POLAROID";
                    break;
                case 3:
                    format = "MULTISHOOT";
                    break;
            }


            string imgName = System.IO.Path.GetFileName(image.FullName);
            string shortName = System.IO.Path.GetFileNameWithoutExtension(image.FullName);
            DateTime date = DateTime.Now;
            int year = date.Year;
            int month = date.Month;
            int day = date.Day;
            int hour = date.Hour;
            int minute = date.Minute;
            string second = date.Second.ToString();
            if (second.Length == 1)
            {
                second = "0" + second;
            }
            string OriginalFinalDirectory = Globals._finalFolder + "\\Original\\" + System.IO.Path.GetFileName(image.FullName);
            string dividedFinalDirectory = Globals._finalFolder + "\\" + System.IO.Path.GetFileName(image.FullName);
            //string longName = "EVENT_" + Globals.GetEventId() + "__DTM_" + year + "-" + month + "-" + day + "-" + hour + minute + second + ".SELFIZEE";
            string longName = "EVENT_" + Globals.GetEventId() + "__DTM_" + year + "-" + month + "-" + day + "-" + hour + minute + second + "_" + format + ".jpg";
            //Globals.binFileName = Globals.nextcloudPath + longName;
            //BinaryFileManager binMgr = new BinaryFileManager();
            //binMgr.createBinaryFile(Globals.nextcloudPath + longName);
            Globals.printedImage = longName;
            if (Globals.gswithoutframe)
            {
                if (!Directory.Exists(Globals.EventMediaFolder() + "\\Photos\\GSNoFrame"))
                {
                    Directory.CreateDirectory(Globals.EventMediaFolder() + "\\Photos\\GSNoFrame");
                }
            }

            if (Globals.EventChosen != 1)
            {
                if (File.Exists(image.FullName) && Directory.Exists(Globals.nextcloudPath))
                {
                    File.Copy(image.FullName, Globals.EventMediaFolder() + "\\Photos\\" + longName);
                    if (Globals.gswithoutframe)
                    {
                        File.Copy(image.FullName, Globals.EventMediaFolder() + "\\Photos\\GSNoFrame\\" + longName);
                    }
                }
            }
            else
            {
                File.Copy(OriginalFinalDirectory, Globals.EventMediaFolder() + "\\Photos\\Original\\" + longName);
                if (Globals.gswithoutframe)
                {
                    File.Copy(OriginalFinalDirectory, Globals.EventMediaFolder() + "\\Photos\\GSNoFrame" + longName);
                }
                File.Copy(dividedFinalDirectory, Globals.EventMediaFolder() + "\\Photos\\" + longName);
            }
        }

        private void createdataBinFile()
        {
            string fileName = Globals.GetEventId() + ".bin";
            fileName = Globals._dataFolder + "\\" + fileName;
            if (!File.Exists(fileName))
            {
                BinaryFileManager mgrBin = new BinaryFileManager();
                mgrBin.createBinaryFile(fileName);
            }
        }
        private void getBasicInfo()
        {
            bool newFile = false;
            string code = Globals.GetEventId();
            string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
            if (!File.Exists(Result))
            {
                var file = File.Create(Result);
                file.Close();
                newFile = true;
            }
            else
            {
                string contentFile = File.ReadAllText(Result);
                if (string.IsNullOrEmpty(contentFile))
                {
                    newFile = true;
                }
            }
            string[] headerTab = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time" };
            string[] headerCarmelliaTab = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time", "birth", "Phone number", "gender", "Last Name", "FirstName", "email", "receive sms", "receive mail" };
            //string[] headerTab = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time", "email", "J'accepte de recevoir ma photo par email", "Je souhaite recevoir la newsletter «Les Rendez-Vous» de Hyundai Motor France", "J’accepte que mes coordonnées (par exemple: nom, prénom, adresse postale, email, téléphone) soient transmises à des partenaires commerciaux de Hyundai Motor France", "J’accepte de participer au jeu concours du Club e-changiste BlueDrive" };
            ////string[] headerTabRgpd = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time", "Gender", "Name", "First name", "E-mail", "Postal code", "Accept" };
            //string[] headerTabRgpd = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time", "Name", "First name", "E-mail", "Postal code", "Country", "Accept", "Gift", "Participaton Time", "relay point" };
            string finalHeader = "";
            if (typeFormulaire.ToUpper() == "CARMILA" && activated)
            {
                for (int i = 0; i < headerCarmelliaTab.Length; i++)
                {
                    finalHeader += headerCarmelliaTab[i] + ",";
                }
            }
            else
            {
                for (int i = 0; i < headerTab.Length; i++)
                {
                    finalHeader += headerTab[i] + ",";
                }
                foreach (var item in headText)
                {
                    finalHeader += item + ",";
                }
            }
            //for (int i = 0; i < headerTab.Length; i++)
            //{
            //    finalHeader += headerTab[i] + ",";
            //}


            //finalHeader += "\r\n";
            string dateHeure = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string image = System.IO.Path.GetFileName(Globals.printedImage);
            string eventName = "";
            switch (Globals.EventChosen)
            {
                case 0:
                    eventName = "CADRE";
                    break;
                case 1:
                    eventName = "STRIP";
                    break;
                case 2:
                    eventName = "POLAROID";
                    break;
                case 3:
                    eventName = "MULTISHOOT";
                    break;
            }
            string bgChromaKey = Globals.greenScreen;
            if (String.IsNullOrEmpty(bgChromaKey) && Globals.gswithoutframe)
            {
                bgChromaKey = "Without frame";
            }
            else
            {
                bgChromaKey = "";
            }
            DateTime photoTime = DateTime.MinValue;
            if (Globals.dtTakenPhoto != DateTime.MinValue)
            {
                photoTime = Globals.dtTakenPhoto;
            }
            DateTime printTime = DateTime.MinValue;
            if (Globals.dtPrint != DateTime.MinValue)
            {
                printTime = Globals.dtPrint;
            }
            string dateHeurePrint = "";
            string dateHeurePhoto = "";
            if (printTime != DateTime.MinValue)
            {
                dateHeurePrint = printTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            if (photoTime != DateTime.MinValue)
            {
                dateHeurePhoto = photoTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            string printed = "NON";
            Globals.dataToSave = new boData();
            Globals.dataToSave.date_photo = dateHeure;
            Globals.dataToSave.code_logiciel = code;
            Globals.dataToSave.filter = Globals.filterChosen;
            Globals.dataToSave.green_screen = Globals.greenScreen;
            Globals.dataToSave.name_photo = image;
            Globals.dataToSave.type_format = eventName;
            saveJsonData();

            using (StreamWriter sw = File.AppendText(Result))
            {

                string toWrite = dateHeure + "," + image + "," + Globals.filterChosen + "," + eventName + "," + printed + "," + nbPrinted + "," + bgChromaKey + "," + dateHeurePhoto + "," + dateHeurePrint + ",";
                if (newFile)
                {
                    sw.WriteLine(finalHeader);
                }

                sw.WriteLine(toWrite);
                sw.Close();
                sw.Dispose();
            }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_postalCode.Text))
            {
                if (_postalCode.Text != "Code postal *")
                {
                    string textBoxText = _postalCode.Text;
                    string lastCharacter = textBoxText.Substring(textBoxText.Length - 1);
                    if (!IsNumeric(lastCharacter))
                    {
                        _postalCode.Text = _postalCode.Text.Substring(0, textBoxText.Length - 1);
                        _postalCode.SelectionStart = _postalCode.Text.Length;
                    }
                }

            }

        }

        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        private bool checkMailAdress(string adress)
        {
            string strEMail = adress;
            Regex regx = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regx.Match(strEMail);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private void writeformData()
        {

            if (typeFormulaire.ToUpper() == "CARMILA" && activated)
            {

                string form_data = birth_carmellia + "," + phone_number_carmellia + "," + gender_carmellia + "," + name_carmellia + "," + firstname_carmellia + "," + email_carmellia + "," + receive_sms + "," + receive_mail + ",";

                //foreach(var item in hashResult)
                //{
                //    form_data += item.Value + ",";
                //}
                Globals.dataToSave.champs = new List<champ>();
                foreach (var item in hashResult)
                {
                    if(item.Value != "")
                    {
                        champ temp = new champ();
                        char[] tab = new char[] { '_' };
                        //string id = item.Key.Split(tab).Last();
                        string id = "";
                        bool id_exists = hashCarmila.TryGetValue(item.Key, out id);
                        temp.id = id;
                        temp.value = item.Value;
                        Globals.dataToSave.champs.Add(temp);
                        if (!string.IsNullOrEmpty(item.Value))
                        {
                            form_data += item.Value + ",";
                        }
                    }
                   

                }
                Globals.dataToSave.champs = Globals.dataToSave.champs.OrderBy(o => o.id).ToList();
                string path = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
                string allText = File.ReadAllText(path);
                string text = File.ReadLines(path).Last();
                string finalText = text + form_data;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8);
                for (int i = 0; i < rows.Length; i++)
                {
                    if (rows[i].Contains(text))
                    {
                        rows[i] = rows[i].Replace(text, finalText);
                    }
                    if (rows[i] != "")
                        sw.WriteLine(rows[i]);
                }
                sw.Close();
            }
            else
            {
                Globals.dataToSave.champs = new List<champ>();
                string form_data = "";
                hashResult = hashResult.OrderBy(obj => obj.Key).ToDictionary(obj => obj.Key, obj => obj.Value);
                foreach (var item in hashResult)
                {
                    champ temp = new champ();
                    char[] tab = new char[] { '_' };
                    string id = item.Key.Split(tab).Last();
                    temp.id = id;
                    temp.value = item.Value;
                    Globals.dataToSave.champs.Add(temp);
                    if (!string.IsNullOrEmpty(item.Value))
                    {
                        form_data += item.Value + ",";
                    }

                }
                Globals.dataToSave.champs = Globals.dataToSave.champs.OrderBy(o => o.id).ToList();
                //string form_data = form_rgpd_gender + "," + form_rgpd_Name + "," + form_rgpd_lastname + "," + form_rgpd_email + "," + form_rgpd_postalcode + "," + form_rgpd_accept + ",";
                //string form_data = form_rgpd_Name + "," + form_rgpd_lastname + "," + form_rgpd_email + "," + form_rgpd_postalcode + "," + form_rgpd_Country + "," + form_rgpd_accept + ",,,,";
                string path = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
                string allText = File.ReadAllText(path);
                string text = File.ReadLines(path).Last();
                string finalText = text + form_data;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8);
                for (int i = 0; i < rows.Length; i++)
                {
                    if (rows[i].Contains(text))
                    {
                        rows[i] = rows[i].Replace(text, finalText);
                    }
                    if (rows[i] != "")
                        sw.WriteLine(rows[i]);
                }
                sw.Close();
            }

        }


        private void PrintPage(object o, PrintPageEventArgs e)
        {
            string image = GetFiltred().FullName;
            var bmp = Bitmap.FromFile(image);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private FileInfo GetFiltred()
        {
            var vm = (VisualisationViewModel)DataContext;
            //var uri = vm.selectedFilter;
            //if (string.IsNullOrEmpty(uri))
            //{
            var uri = Globals.imageChosen;
            //}
            if (uri.IndexOf("file:") >= 0)
            {
                int strindex = "file:///".Length;
                int len = uri.Length - strindex;
                uri = uri.Substring(strindex, len);
            }
            var image = new FileInfo(uri);
            return image;
        }

        private string createStripImage(String imageFile, int angle)
        {
            List<Bitmap> images = new List<Bitmap>();
            Bitmap secondImage;
            Bitmap firstImage = new Bitmap(imageFile);
            Bitmap finalImage = null;
            //First image
            ImageFilterManager filtre = new ImageFilterManager();
            ImageManipulator imgManipulatorFirst = new ImageManipulator(firstImage);
            firstImage = imgManipulatorFirst.RotateImg(15);
            firstImage.MakeTransparent(System.Drawing.Color.Black);
            System.Drawing.Image imgFirstImage = (System.Drawing.Image)firstImage;
            imgFirstImage.Save(Globals._finalFolder + "\\Temporaire\\tempfirst.png", ImageFormat.Png);
            //Second Image
            secondImage = new Bitmap(firstImage);
            ImageManipulator imgManipulatorSecond = new ImageManipulator(secondImage);
            secondImage = imgManipulatorFirst.RotateImg(330);
            secondImage.MakeTransparent(System.Drawing.Color.Black);
            System.Drawing.Image imgSecondImage = (System.Drawing.Image)secondImage;
            imgSecondImage.Save(Globals._finalFolder + "\\Temporaire\\tempsecond.png", ImageFormat.Png);
            string fileName = ImageUtility.CreateOverlayedStrip(imageFile);
            firstImage.Dispose();
            secondImage.Dispose();
            //string fileName = System.IO.Path.GetFileName(imageFile);
            return fileName;// Globals._finalFolder + "\\Affichage\\" + fileName;
        }

        private void GoToThank()
        {
            // SaveCSV(); 
            //await Task.Delay(TimeSpan.FromSeconds(12));
            GC.SuppressFinalize(this);
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToThanks.CanExecute(null))
                    viewModel.GoToThanks.Execute(null);
            }

        }

        private void GoToHotline()
        {
            // SaveCSV(); 
            //await Task.Delay(TimeSpan.FromSeconds(12));
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            setTimeout();
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel.GoToHotline.CanExecute(null))
                viewModel.GoToHotline.Execute(null);
        }


        private string GetBinFile(string printerName, string choicedEvent)
        {
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            var exeDir = System.IO.Path.GetDirectoryName(exePath);
            var printerConfig = exeDir + "\\Config\\Printer";
            var foldersPrinterConfig = Directory.GetDirectories(printerConfig);
            if (foldersPrinterConfig.Length == 0) return "";
            for (int index = 0; index < foldersPrinterConfig.Length; index++)
            {
                var path = foldersPrinterConfig[index];
                if (string.IsNullOrEmpty(path)) continue;
                if (path.ToLower().IndexOf(printerName) > 0)
                {
                    var files = Directory.GetFiles(path);
                    if (files.Length == 0) return "";
                    for (int i = 0; i < files.Length; i++)
                    {
                        var binFile = files[i];
                        if (binFile.IndexOf(choicedEvent) > 0) return binFile;
                    }
                }
            }
            return "";
        }

        private void AddButton(string buttontype, BouttonPosition positiontype)
        {
            switch (buttontype)
            {
                case "PrintButton":
                    Button imprimer = new Button();
                    imprimer.Name = "vbtnImprimer";
                    //imprimer.Content = "Imprimer";
                    //imprimer.Width = 120;
                    //imprimer.Height = 30;
                    BtnContent(imprimer, "BtnImprimer", "Imprimer", Globals._btnVisualisationPrinting);
                    imprimer.Margin = new Thickness(3, 3, 0, 0);

                    imprimer.Click += Imprimer;
                    BtnPanelPosition(positiontype, imprimer);
                    break;
                case "VisualisationSendEmailAndPrint":
                    Button PrintAndSendEmail = new Button();
                    PrintAndSendEmail.Name = "vbtnImprimer";
                    //imprimer.Content = "Imprimer";
                    //imprimer.Width = 120;
                    //imprimer.Height = 30;
                    BtnContent(PrintAndSendEmail, "BtnImprimer", "PrintAndSend", Globals._btnVisualisationPrinting);
                    PrintAndSendEmail.Margin = new Thickness(3, 3, 0, 0);

                    PrintAndSendEmail.Click += Imprimer;
                    BtnPanelPosition(positiontype, PrintAndSendEmail);
                    break;
                case "VisualisationBackToHome":
                    Button goToHome = new Button();
                    goToHome.Name = "vbtnGoToHome";
                    BtnContent(goToHome, "BtnGoToHome", "Revenir à l'accueil", Globals._btnVisualisationHome);
                    goToHome.Margin = new Thickness(3, 3, 0, 0);

                    goToHome.SetBinding(Button.CommandProperty, CreateBinding("GoToHome"));

                    BtnPanelPosition(positiontype, goToHome);
                    break;
                case "VisualisationTerminer":
                    Button terminer = new Button();
                    terminer.Name = "vbtnTerminer";
                    //terminer.Content = "Terminer";
                    //terminer.Width = 120;
                    //terminer.Height = 30;
                    BtnContent(terminer, "BtnTerminer", "Terminer", Globals._btnVisualisationEnd);
                    terminer.Margin = new Thickness(3, 3, 0, 0);
                    terminer.SetBinding(Button.CommandProperty, CreateBinding("GoToThanks"));

                    BtnPanelPosition(positiontype, terminer);
                    break;
                case "GoToFiltre":
                    Button goToFiltre = new Button();
                    goToFiltre.Name = "vbtnGoToFiltre";
                    //goToFiltre.Content = "Revenir à la séléction de filtre";
                    //goToFiltre.Width = 180;
                    //goToFiltre.Height = 30;
                    BtnContent(goToFiltre, "ImgBtnBackToFiltre", "Revenir à la séléction de filtre", Globals._btnVisualisationBackToFiltreChoice);
                    goToFiltre.Margin = new Thickness(3, 3, 0, 0);
                    goToFiltre.SetBinding(Button.CommandProperty, CreateBinding("GoToFiltre"));

                    BtnPanelPosition(positiontype, goToFiltre);
                    break;
                case "VisualizationGoToTakePicture":
                    Button goToTakePicture = new Button();
                    goToTakePicture.Name = "vbtnGoToTakePicture";
                    //goToTakePicture.Content = "Revenir à la prise de photo";
                    //goToTakePicture.Width = 180;
                    //goToTakePicture.Height = 30;
                    BtnContent(goToTakePicture, "ImgBtnBackToTakePicture", "Revenir à la prise de photo", Globals._btnVisualisationBackToTakePicture);
                    goToTakePicture.Margin = new Thickness(3, 3, 0, 0);
                    goToTakePicture.SetBinding(Button.CommandProperty, CreateBinding("GoToTakePhoto"));

                    BtnPanelPosition(positiontype, goToTakePicture);
                    break;
            }
        }

        private void TextBoxNormaltKeyboardFocus(object sender, EventArgs e)
        {
            TextBox source = sender as TextBox;

            if (source != null)
            {
                //_popup.IsOpen = true;
                alphanumeriquekeyboardForm1.Visibility = Visibility.Visible;
                alphanumeriquekeyboardForm1.ActiveContainer = source;

            }
        }

        private Binding CreateBinding(string path)
        {
            Binding binding = new Binding();
            binding.Path = new PropertyPath(path);
            return binding;
        }

        private void BtnContent(Button btn, string imageName, string contentString, string imagePath)
        {
            if (File.Exists(imagePath))
            {
                var icon = new System.Windows.Controls.Image();
                icon.Name = imageName;
                icon.MaxHeight = 280;
                icon.MaxWidth = 600;
                BitmapImage bmImage = new BitmapImage();
                bmImage.BeginInit();
                bmImage.UriSource = new Uri(imagePath);
                bmImage.EndInit();
                icon.Source = bmImage;
                btn.Content = icon;
                btn.Width = icon.Width;
                btn.Height = icon.Height;
                btn.Background = new SolidColorBrush(Colors.Transparent);
                btn.Foreground = new SolidColorBrush(Colors.Transparent);
                btn.BorderBrush = new SolidColorBrush(Colors.Transparent);
            }

            if (!File.Exists(imagePath))
            {
                btn.Content = contentString;
                btn.Width = 180;
                btn.Height = 30;
            }
        }

        private void BtnPanelPosition(Models.Enum.BouttonPosition type, Button _button)
        {

        }

        private double GetPanelWidth(UIElementCollection elements)
        {
            double width = 0.0;
            foreach (var el in elements)
            {
                Button _el = (Button)el;
                width += _el.Width;
            }
            return width;
        }

        private void RemoveNbCopie(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(scrollcopieValue.Text, out value))
            {
                //nombre de copie ne devra pas être en dessous de 1
                if (Convert.ToInt32(scrollcopieValue.Text) > 1)
                {
                    value = Convert.ToInt32(scrollcopieValue.Text) - 1;
                    _copieValue = value.ToString();
                    scrollcopieValue.Text = _copieValue;
                }
            }
        }

        private void AddNbCopie(object sender, RoutedEventArgs e)
        {
            int value = 0;
            int nbRealPrinted = 0;
            if (int.TryParse(scrollcopieValue.Text, out value))
            {
                nbRealPrinted = getNbPrinted() + Convert.ToInt32(scrollcopieValue.Text);
                //nombre de copie ne dépasse pas 10
                if (Convert.ToInt32(scrollcopieValue.Text) < nbmultiimpression)
                {
                    value = Convert.ToInt32(scrollcopieValue.Text) + 1;
                    _copieValue = value.ToString();
                    if (nbmaxprinting == 0)
                    {
                        scrollcopieValue.Text = _copieValue;
                    }
                    else
                    {
                        if (nbRealPrinted > nbmaxprinting)
                        {
                            //scrollcopieValue.Text = nbmaxprinting.ToString();
                        }
                        else
                        {
                            scrollcopieValue.Text = _copieValue;
                        }
                    }


                }
            }
        }

        private void copieCancel(object sender, EventArgs e)
        {
            getJsonData();
            int count = Globals.lstDataToSave.Count;
            Globals.lstDataToSave[count - 1] = Globals.dataToSave;
            string pathJson = Globals.EventMediaFolder() + "//Data//data.json";
            //List<boData> crmObj = new List<boData>();
            File.WriteAllText(pathJson, JsonConvert.SerializeObject(Globals.lstDataToSave));
            visualisationImage.Opacity = 1;
            lbl_CurrentPrinting.Content = "";
            timerCountdown.Start();
            scrollCopie.Visibility = Visibility.Collapsed;
            vbtnImprimer.Visibility = Visibility.Visible;
            vbtnImprimer.IsEnabled = true;
            setTimeout();
            LaunchTimerCountdown();
            //copieValue.Text = DefaultCopie().ToString();
        }

        private void copieValidate(object sender, RoutedEventArgs e)
        {
            var nbToPrint = Convert.ToInt32(scrollcopieValue.Text);
            var nbPrintFinal = getNbPrinted() + nbToPrint;
            vbtnImprimer.Visibility = Visibility.Visible;
            vbtnImprimer.IsEnabled = false;
            nbPrintedForPage += nbToPrint;

            if ((nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)) || (nbPrintedForPage > nbmultiimpression))
            {
                System.Windows.MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                nbPrintedForPage -= nbToPrint;
            }
            else
            {
                scrollCopie.Visibility = Visibility.Collapsed;
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                }));

                vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal <= nbmaxprinting)) || (nbPrintedForPage == nbmultiimpression);

                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                int toWrite = mgrBin.readIntData(binFileName);
                toWrite += Convert.ToInt32(_copieValue);
                writeTxtFileData(mediaDataPath, toWrite.ToString());
                mgrBin.writeData(toWrite, binFileName);
                Printing(false);
            }
        }



        #region formulaire
        private void OpenForm(object sender, EventArgs e)
        {           
            Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        if (timerCountdown != null) timerCountdown.Stop();
                        StkpHome.Visibility = Visibility.Hidden;
                        setTimeout();
                        //lbl_loading.Visibility = Visibility.Visible;
                        LoadingPanel.ShowPanel();

                    }));
                    Thread.Sleep(100);
                }
            ).ContinueWith(task=>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        INIFileManager _ini = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                        int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
                        LaunchTimerCountdown_Print();
                        lbl_CurrentPrinting.Visibility = Visibility.Hidden;
                        //string typeForm = appIni.GetSetting("FORM", "type");
                        if (typeFormulaire.ToUpper() == "CARMILA" && activated)
                        {
                            if (InternetConnectionManager.checkConnection("www.google.com"))
                            {
                                sigle = "A";
                                current_Form_Index = 1;
                                scrollCoordonateConectionCarmellia.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                connected = false;
                                sigle = "D";
                                current_Form_Index = 4;
                                scrollCoordonateCarmellia.Visibility = Visibility.Visible;
                            }
                        }
                        else
                        {
                            if (last_page == 0 && !activated)
                            {
                                if(_print> 0)
                                {
                                    launchPrinting();
                                }
                                
                            }
                            else if (last_page == 0 && activated)
                            {
                                if (_print > 0)
                                {
                                    launchPrinting();
                                }
                            }
                            else
                            {
                                if (Globals.nbField == 0)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                }
                                else
                                {
                                    current_page = 1;
                                    Page_1.Visibility = Visibility.Visible;
                                }

                            }
                        }
                        // lbl_loading.Visibility = Visibility.Hidden;
                        LoadingPanel.ClosePanel();
                    }));
                }
            );          


        }

        private void CloseCurrentForm(object sender, EventArgs e)
        {
            switch (current_page)
            {
                case 1:
                    Page_1.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    Page_2.Visibility = Visibility.Collapsed;
                    break;
                case 3:
                    Page_3.Visibility = Visibility.Collapsed;
                    break;
                case 4:
                    Page_4.Visibility = Visibility.Collapsed;
                    break;
                case 5:
                    Page_5.Visibility = Visibility.Collapsed;
                    break;
                case 6:
                    Page_6.Visibility = Visibility.Collapsed;
                    break;
                case 7:
                    Page_7.Visibility = Visibility.Collapsed;
                    break;
                case 8:
                    Page_8.Visibility = Visibility.Collapsed;
                    break;
                case 9:
                    Page_9.Visibility = Visibility.Collapsed;
                    break;
                case 10:
                    Page_10.Visibility = Visibility.Collapsed;
                    break;

            }
            //timerCountdown.Start();
            setTimeout();
            LaunchTimerCountdown();
        }

        private void CloseAllPageForm()
        {

            Page_1.Visibility = Visibility.Collapsed;

            Page_2.Visibility = Visibility.Collapsed;

            Page_3.Visibility = Visibility.Collapsed;

            Page_4.Visibility = Visibility.Collapsed;

            Page_5.Visibility = Visibility.Collapsed;

            Page_6.Visibility = Visibility.Collapsed;

            Page_7.Visibility = Visibility.Collapsed;

            Page_8.Visibility = Visibility.Collapsed;

            Page_9.Visibility = Visibility.Collapsed;

            Page_10.Visibility = Visibility.Collapsed;



        }

        private void backToPreviousForm(object sender, EventArgs e)
        {
            switch (current_page)
            {
                case 1:
                    Page_1.Visibility = Visibility.Visible;
                    current_page = 1;
                    break;
                case 2:
                    Page_1.Visibility = Visibility.Visible;
                    Page_2.Visibility = Visibility.Collapsed;
                    current_page = 1;
                    break;
                case 3:
                    Page_2.Visibility = Visibility.Visible;
                    Page_3.Visibility = Visibility.Collapsed;
                    current_page = 2;
                    break;
                case 4:
                    Page_3.Visibility = Visibility.Visible;
                    Page_4.Visibility = Visibility.Collapsed;
                    current_page = 3;
                    break;
                case 5:
                    Page_4.Visibility = Visibility.Visible;
                    Page_5.Visibility = Visibility.Collapsed;
                    current_page = 4;
                    break;
                case 6:
                    Page_5.Visibility = Visibility.Visible;
                    Page_6.Visibility = Visibility.Collapsed;
                    current_page = 5;
                    break;
                case 7:
                    Page_6.Visibility = Visibility.Visible;
                    Page_7.Visibility = Visibility.Collapsed;
                    current_page = 6;
                    break;
                case 8:
                    Page_7.Visibility = Visibility.Visible;
                    Page_8.Visibility = Visibility.Collapsed;
                    current_page = 7;
                    break;
                case 9:
                    Page_8.Visibility = Visibility.Visible;
                    Page_9.Visibility = Visibility.Collapsed;
                    current_page = 8;
                    break;
                case 10:
                    Page_9.Visibility = Visibility.Visible;
                    Page_10.Visibility = Visibility.Collapsed;
                    current_page = 9;
                    break;

            }
        }

        public bool verifyMendatory(string toVerify, int type, bool connected)
        {
            bool result = false;
            bool checkrealMail = false;
            bool checkrealportable = false;
            if (type >= 0)
            {
                if (type == 1)
                {
                    try
                    {
                        bool formatPhone = CheckPhoneNumber(toVerify);
                        if (formatPhone)
                        {
                            result = true;
                            if (connected)
                            {
                                checkrealportable = true;
                                result = CheckPortableValid(toVerify);
                            }

                        }
                        else
                        {
                            result = false;
                        }


                    }
                    catch (Exception ex)
                    {
                        result = false;
                    }
                }
                else if (type == 2)
                {
                    try
                    {
                        bool formatMail = checkMailAdress(toVerify);
                        if (formatMail)
                        {
                            result = true;
                            if (connected)
                            {
                                checkrealMail = true;
                                result = CheckEmailValid(toVerify);
                            }

                        }
                        else
                        {
                            result = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                    }
                }
            }
            if (result == false && type == 1)
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir un numéro de téléphone valide";
                scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                //return false;
            }
            else if (result == false && type == 2)
            {
                if (checkrealMail)
                {
                    coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse email réelle";
                }
                else
                {
                    coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse email valide";
                }

                scrollCoordonateCarmelliaKnown.Visibility = Visibility.Collapsed;
                scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                scrollCoordonateCarmellia.Visibility = Visibility.Collapsed;
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                //return false;
            }
            else
            {
                if (checkMendatoryCarmellia())
                {
                    coordonneeMandatoryTitle.Text = "Veuillez remplir les champs obligatoires";

                    switch (current_page)
                    {
                        case 1:
                            scrollCoordonateConectionCarmellia.Visibility = Visibility.Collapsed;
                            break;
                        case 2:
                            scrollCoordonateCarmelliaKnown.Visibility = Visibility.Collapsed;
                            break;
                        case 3:
                            scrollCoordonateCarmelliaUnKnown.Visibility = Visibility.Collapsed;
                            break;
                        case 4:
                            scrollCoordonateCarmellia.Visibility = Visibility.Collapsed;
                            break;
                    }
                    scrollCoordonateMandatory.Visibility = Visibility.Visible;
                    return true;
                }

            }
            return result;
        }


        private void continueToNextForm(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    setTimeout();
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;

                }));
                Thread.Sleep(100);
            }
            ).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (checkMendatory())
                    {
                        coordonneeMandatoryTitle.Text = "Veuillez renseigner les champs obligatoires";
                        scrollCoordonateMandatory.Visibility = Visibility.Visible;
                        switch (current_page)
                        {
                            case 1:
                                Page_1.Visibility = Visibility.Collapsed;
                                break;
                            case 2:
                                Page_2.Visibility = Visibility.Collapsed;
                                break;
                            case 3:
                                Page_3.Visibility = Visibility.Collapsed;
                                break;
                            case 4:
                                Page_4.Visibility = Visibility.Collapsed;
                                break;
                            case 5:
                                Page_5.Visibility = Visibility.Collapsed;
                                break;
                            case 6:
                                Page_6.Visibility = Visibility.Collapsed;
                                break;
                            case 7:
                                Page_7.Visibility = Visibility.Collapsed;
                                break;
                            case 8:
                                Page_8.Visibility = Visibility.Collapsed;
                                break;
                            case 9:
                                Page_9.Visibility = Visibility.Collapsed;
                                break;
                            case 10:
                                Page_10.Visibility = Visibility.Collapsed;
                                break;
                        }
                        LoadingPanel.ClosePanel();
                    }
                    else
                    {
                        INIFileManager _ini = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                        int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
                        switch (current_page)
                        {
                            case 1:

                                Page_1.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if(_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    Page_2.Visibility = Visibility.Visible;
                                    current_page = 2;
                                }
                                break;
                            case 2:

                                Page_2.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_3.Visibility = Visibility.Visible;
                                    current_page = 3;
                                }
                                break;
                            case 3:

                                Page_3.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_4.Visibility = Visibility.Visible;
                                    current_page = 4;
                                }
                                break;
                            case 4:

                                Page_4.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_5.Visibility = Visibility.Visible;
                                    current_page = 5;
                                }
                                break;
                            case 5:

                                Page_5.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_6.Visibility = Visibility.Visible;
                                    current_page = 6;
                                }
                                break;
                            case 6:

                                Page_6.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_7.Visibility = Visibility.Visible;
                                    current_page = 7;
                                }
                                break;
                            case 7:

                                Page_7.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_8.Visibility = Visibility.Visible;
                                    current_page = 8;
                                }
                                break;
                            case 8:

                                Page_8.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_9.Visibility = Visibility.Visible;
                                    current_page = 9;
                                }
                                break;
                            case 9:

                                Page_9.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_10.Visibility = Visibility.Visible;
                                    current_page = 10;
                                }
                                break;
                            case 10:

                                Page_10.Visibility = Visibility.Collapsed;

                                if (current_page == last_page)
                                {
                                    if (_print > 0)
                                    {
                                        launchPrinting();
                                    }
                                    else
                                    {
                                        if (timerCountdown != null)
                                        {
                                            timerCountdown.Start();
                                        }
                                    }
                                }
                                else
                                {
                                    Page_10.Visibility = Visibility.Visible;
                                    current_page = 10;
                                }
                                break;

                        }
                        LoadingPanel.ClosePanel();
                    }
                    
                   // lbl_loading.Visibility = Visibility.Hidden;

                }));
            }
            );
            

        }

        private void showKeyboard(TextBox txtBox, int type)
        {
            switch (current_page)
            {
                case 1:
                    if (type == 0)
                    {
                        int top = Convert.ToInt32(main_Page1.Margin.Top);
                        int txttop = Convert.ToInt32(txtBox.Margin.Top);
                        int gridheight = Convert.ToInt32(main_Page1.ActualHeight);
                        double yMargin = getMarginYForm(top, txttop, gridheight);
                        if(yMargin > 0)
                        {
                            double left = main_Page1.Margin.Left;
                            //main_Page1.Margin = new Thickness(left, 0, 0, yMargin);
                        }
                        numeriqueKeyboardForm1.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm1.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm1.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm1.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        int top = Convert.ToInt32(main_Page1.Margin.Top);
                        int txttop = Convert.ToInt32(txtBox.Margin.Top);
                        int gridheight = Convert.ToInt32(main_Page1.ActualHeight);
                        double yMargin = getMarginYForm(top, txttop, gridheight);
                        if (yMargin > 0)
                        {
                            double left = main_Page1.Margin.Left;
                            //main_Page1.Margin = new Thickness(left,0 , 0, yMargin);
                        }
                        alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm1.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm1.ActiveContainer = txtBox;
                        numeriqueKeyboardForm1.HorizontalAlignment = HorizontalAlignment.Center;
                    }


                    break;
                case 2:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm2.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm2.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm2.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm2.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm2.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm2.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm2.ActiveContainer = txtBox;
                        numeriqueKeyboardForm2.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    break;
                case 3:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm3.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm3.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm3.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm3.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm3.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm3.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm3.ActiveContainer = txtBox;
                        numeriqueKeyboardForm3.HorizontalAlignment = HorizontalAlignment.Center;
                    }

                    break;
                case 4:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm4.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm4.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm4.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm4.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm4.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm4.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm4.ActiveContainer = txtBox;
                        numeriqueKeyboardForm4.HorizontalAlignment = HorizontalAlignment.Center;
                    }

                    break;
                case 5:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm5.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm5.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm5.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm5.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm5.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm5.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm5.ActiveContainer = txtBox;
                        numeriqueKeyboardForm5.HorizontalAlignment = HorizontalAlignment.Center;
                    }

                    break;
                case 6:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm6.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm6.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm6.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm6.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm6.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm6.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm6.ActiveContainer = txtBox;
                        numeriqueKeyboardForm6.HorizontalAlignment = HorizontalAlignment.Center;
                    }

                    break;
                case 7:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm7.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm7.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm7.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm7.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm7.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm7.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm7.ActiveContainer = txtBox;
                        numeriqueKeyboardForm7.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    break;
                case 8:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm8.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm8.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm8.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm8.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm8.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm8.ActiveContainer = txtBox;
                        numeriqueKeyboardForm8.HorizontalAlignment = HorizontalAlignment.Center;
                    }

                    break;
                case 9:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm9.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm9.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm9.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm9.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm9.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm9.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm9.ActiveContainer = txtBox;
                        numeriqueKeyboardForm9.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    break;
                case 10:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm10.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm10.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm10.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm10.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm10.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm10.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm10.ActiveContainer = txtBox;
                        numeriqueKeyboardForm10.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    break;
                case 11:
                    if (type == 0)
                    {
                        numeriqueKeyboardForm11.Visibility = Visibility.Collapsed;
                        alphanumeriquekeyboardForm11.Visibility = Visibility.Visible;
                        alphanumeriquekeyboardForm11.ActiveContainer = txtBox;
                        alphanumeriquekeyboardForm11.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    else
                    {
                        alphanumeriquekeyboardForm11.Visibility = Visibility.Collapsed;
                        numeriqueKeyboardForm11.Visibility = Visibility.Visible;
                        numeriqueKeyboardForm11.ActiveContainer = txtBox;
                        numeriqueKeyboardForm11.HorizontalAlignment = HorizontalAlignment.Center;
                    }
                    break;

            }
        }

        private void CloseKeyboard()
        {
            switch (current_page)
            {
                case 1:


                    alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm1.Visibility = Visibility.Collapsed;
                    break;
                case 2:


                    alphanumeriquekeyboardForm2.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm2.Visibility = Visibility.Collapsed;

                    break;
                case 3:


                    alphanumeriquekeyboardForm3.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm3.Visibility = Visibility.Collapsed;


                    break;
                case 4:


                    alphanumeriquekeyboardForm4.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm4.Visibility = Visibility.Collapsed;


                    break;
                case 5:


                    alphanumeriquekeyboardForm5.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm5.Visibility = Visibility.Collapsed;


                    break;
                case 6:


                    alphanumeriquekeyboardForm6.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm6.Visibility = Visibility.Collapsed;


                    break;
                case 7:


                    alphanumeriquekeyboardForm7.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm7.Visibility = Visibility.Collapsed;

                    break;
                case 8:

                    alphanumeriquekeyboardForm8.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm8.Visibility = Visibility.Collapsed;


                    break;
                case 9:


                    alphanumeriquekeyboardForm9.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm9.Visibility = Visibility.Collapsed;

                    break;
                case 10:


                    alphanumeriquekeyboardForm10.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm10.Visibility = Visibility.Collapsed;

                    break;
                case 11:


                    alphanumeriquekeyboardForm11.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm11.Visibility = Visibility.Collapsed;

                    break;

            }
        }

        private void CloseKeyboard(object sender, RoutedEventArgs e)
        {
            switch (current_page)
            {
                case 1:


                    alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm1.Visibility = Visibility.Collapsed;
                    break;
                case 2:


                    alphanumeriquekeyboardForm2.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm2.Visibility = Visibility.Collapsed;

                    break;
                case 3:


                    alphanumeriquekeyboardForm3.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm3.Visibility = Visibility.Collapsed;


                    break;
                case 4:


                    alphanumeriquekeyboardForm4.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm4.Visibility = Visibility.Collapsed;


                    break;
                case 5:


                    alphanumeriquekeyboardForm5.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm5.Visibility = Visibility.Collapsed;


                    break;
                case 6:


                    alphanumeriquekeyboardForm6.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm6.Visibility = Visibility.Collapsed;


                    break;
                case 7:


                    alphanumeriquekeyboardForm7.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm7.Visibility = Visibility.Collapsed;

                    break;
                case 8:

                    alphanumeriquekeyboardForm8.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm8.Visibility = Visibility.Collapsed;


                    break;
                case 9:


                    alphanumeriquekeyboardForm9.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm9.Visibility = Visibility.Collapsed;

                    break;
                case 10:


                    alphanumeriquekeyboardForm10.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm10.Visibility = Visibility.Collapsed;

                    break;
                case 11:


                    alphanumeriquekeyboardForm11.Visibility = Visibility.Collapsed;
                    numeriqueKeyboardForm11.Visibility = Visibility.Collapsed;

                    break;

            }
        }

        private void launchPrinting()
        {

            Globals.dtPrint = DateTime.Now;
            writeformData();
            visualisationImage.Opacity = 0.7;
            var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
            _isMultiImpression = isMultiImpression == "1";
            lbl_CountDown.IsEnabled = false;
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            if (_isMultiImpression && nbmultiimpression > 1)
            {
                lbl_CurrentPrinting.Content = "Impression en cours....";
                if (timerCountdown_From != null) timerCountdown_From.Stop();
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                //if (Globals.ScreenType == "SPHERIK")
                //{
                //    scrollCopie.Width = 1200;
                //    scrollCopie.Height = 800;
                //}
                scrollCopie.Visibility = Visibility.Visible;
                vbtnImprimer.Visibility = Visibility.Collapsed;
                //copieValue.Text = _copieValue;
            }
            else //(!_isMultiImpression)
            {
                if (timerCountdown_From != null) timerCountdown_From.Stop();
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();


                //nbPrinted = 1;
                int nbPrintFinal = 0;
                nbPrintFinal = getNbPrinted() + 1;
                vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                int toWrite = mgrBin.readIntData(binFileName);
                toWrite += Convert.ToInt32(_copieValue);
                //writeTxtFileData(mediaDataPath,"");
                writeTxtFileData(mediaDataPath, toWrite.ToString());
                mgrBin.writeData(toWrite, binFileName);
                //this.Dispatcher.BeginInvoke((Action)(() =>
                //{
                //    lbl_CurrentPrinting.Content = "Impression en cours....";
                //}));
                lbl_CurrentPrinting.Content = "Impression en cours....";
                Printing(false);
            }


        }

        private void closeCurrentForm()
        {
            switch (current_page)
            {
                case 1:
                    Page_1.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    Page_2.Visibility = Visibility.Collapsed;
                    break;
                case 3:
                    Page_3.Visibility = Visibility.Collapsed;
                    break;
                case 4:
                    Page_4.Visibility = Visibility.Collapsed;
                    current_page = 5;
                    break;
                case 5:
                    Page_5.Visibility = Visibility.Collapsed;
                    break;
                case 6:
                    Page_6.Visibility = Visibility.Collapsed;
                    break;
                case 7:
                    Page_7.Visibility = Visibility.Collapsed;
                    break;
                case 8:
                    Page_8.Visibility = Visibility.Collapsed;
                    break;
                case 9:
                    Page_9.Visibility = Visibility.Collapsed;
                    break;
                case 10:
                    Page_10.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void ModifyScrollShape(int current_page)
        {
            switch (current_page)
            {
                case 1:

                    Page_1.Width = screenWidth;
                    Page_1.Height = screenHeight;

                    break;
                case 2:

                    Page_2.Width = screenWidth;
                    Page_2.Height = screenHeight;

                    break;
                case 3:

                    Page_3.Width = screenWidth;
                    Page_3.Height = screenHeight;

                    break;
                case 4:

                    Page_4.Width = screenWidth;
                    Page_4.Height = screenHeight;

                    break;
                case 5:

                    Page_5.Width = screenWidth;
                    Page_5.Height = screenHeight;

                    break;
                case 6:

                    Page_6.Width = screenWidth;
                    Page_6.Height = screenHeight;

                    break;
                case 7:

                    Page_7.Width = screenWidth;
                    Page_7.Height = screenHeight;

                    break;
                case 8:

                    Page_8.Width = screenWidth;
                    Page_8.Height = screenHeight;

                    break;
                case 9:

                    Page_9.Width = screenWidth;
                    Page_9.Height = screenHeight;

                    break;
                case 10:

                    Page_10.Width = screenWidth;
                    Page_10.Height = screenHeight;

                    break;

            }
        }

        public static T FindChild<T>(DependencyObject parent, string childName)
   where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        public bool CheckEmailValid(string email)
        {
            try
            {
                string url = "https://booth.selfizee.fr/clients/checkEmailValidity";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["email"] = email;
                    var response = wb.UploadValues(url, "POST", data);
                    string responseInString = Encoding.UTF8.GetString(response);
                    CarmelliaObject crmObj = new CarmelliaObject();
                    crmObj = JsonConvert.DeserializeObject<CarmelliaObject>(responseInString);
                    return crmObj.success;
                }
            }
            catch (Exception e)
            {
                return true;
            }

        }

        public void getJsonData()
        {
            string code = Globals.GetEventId();
            string path = "C://Events//Media//" + code + "//Data//data.json";
            //List<boData> crmObj = new List<boData>();
            if (File.Exists(path))
            {
                Globals.lstDataToSave = new List<boData>();
                Globals.lstDataToSave = JsonConvert.DeserializeObject<List<boData>>(File.ReadAllText(path));
                //Globals.lstDataToSave = JsonConvert.DeserializeObject<List<boData>>(path);
            }
            else
            {
                Globals.lstDataToSave = new List<boData>();
            }

        }

        public void saveJsonData()
        {
            if(Globals.dataToSave != null && Globals.lstDataToSave != null)
            {
                Globals.lstDataToSave.Add(Globals.dataToSave);
            }
            
            string pathJson = Globals.EventMediaFolder() + "//Data//data.json";
            //List<boData> crmObj = new List<boData>();
            File.WriteAllText(pathJson, JsonConvert.SerializeObject(Globals.lstDataToSave));
        }

        public bool CheckPortableValid(string portable)
        {
            try
            {
                string url = "https://booth.selfizee.fr/clients/checkPortableValidity";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["portable"] = portable;
                    var response = wb.UploadValues(url, "POST", data);
                    string responseInString = Encoding.UTF8.GetString(response);
                    CarmelliaObject crmObj = new CarmelliaObject();
                    crmObj = JsonConvert.DeserializeObject<CarmelliaObject>(responseInString);
                    return crmObj.success;
                }
            }
            catch (Exception e)
            {
                return true;
            }

        }

        private string getInfoCarmellia(string mobile, string date_naissance)
        {
            try
            {
                string url = "https://booth.selfizee.fr/clients/getCoordonnes";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["mobile"] = mobile;
                    data["date_naissance"] = date_naissance;


                    var response = wb.UploadValues(url, "POST", data);
                    var responseInString = Encoding.UTF8.GetString(response);
                    return responseInString;
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }

        }

        private void PrintAndSendEmail(object sender, EventArgs e)
        {
            bool sendMail = true;
            lbl_CurrentPrinting.Visibility = Visibility.Visible;
            var isMultiImpression = _inimanager.GetSetting("PRINTING", "MultiPrinting");
            _isMultiImpression = isMultiImpression == "1";
            lbl_CountDown.IsEnabled = false;
            if (_isMultiImpression)
            {
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                //popCopie.IsOpen = true;
                //copieValue.Text = _copieValue;
            }

            if (!_isMultiImpression)
            {
                if (timerCountdown != null) timerCountdown.Stop();
                setTimeout();
                _copieValue = _inimanager.GetSetting("PRINTING", "copyNumber");
                Printing(sendMail);
            }

            if (timerCountdown != null) timerCountdown.Stop();
            setTimeout();


            //SendEmail.IsOpen = true;


        }

        private void NextPopUp(object sender, EventArgs e)
        {
            //SendEmail.IsOpen = false;
            //ConfirmText.Text = $"Envoyer l'image vers l'adresse mail : {email.Text} ?";
            //ConfirmSendToMail.IsOpen = true;
        }

        private void CancelSendEmail(object sender, EventArgs e)
        {
            //ConfirmSendToMail.IsOpen = false;
            timerCountdown.Start();
        }

        private void ValidateSendEmail(object sender, EventArgs e)
        {
            //ConfirmSendToMail.IsOpen = false;
            //sendmail
            var _email = "";
            if (_email.IndexOf('@') > 0 && _email.IndexOf('.') > 0)
            {
                using (var mail = new MailMessage())
                {
                    var filePath = GetFiltred();
                    var _from = "selfizee2019@gmail.com";
                    var _to = _email;
                    mail.From = new MailAddress(_from);
                    mail.To.Add(new MailAddress(_to));
                    mail.Attachments.Add(new Attachment(filePath.FullName));
                    using (var smtp = new SmtpClient())
                    {
                        smtp.Port = 587;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.UseDefaultCredentials = false;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Credentials = new System.Net.NetworkCredential("selfizee2019@gmail.com", "selfizee11;");
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                        timerCountdown.Start();
                    }
                }
            }
        }

        public void ReadReplace(string value)
        {
            string path = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
            string allText = File.ReadAllText(path);
            string text = File.ReadLines(path).Last();
            string[] tabText = text.Split(',');
            tabText[4] = value;
            tabText[5] = nbPrinted.ToString();
            Globals.dataToSave.print_date = Globals.dtPrint.ToString("yyyy-MM-dd HH:mm:ss"); 
            Globals.dataToSave.count_print = nbPrinted.ToString();
            getJsonData();
            int count = Globals.lstDataToSave.Count;
            Globals.lstDataToSave[count - 1] = Globals.dataToSave;
            string pathJson = Globals.EventMediaFolder() + "//Data//data.json";
            //List<boData> crmObj = new List<boData>();
            File.WriteAllText(pathJson, JsonConvert.SerializeObject(Globals.lstDataToSave));
            tabText[8] = Globals.dtPrint.ToString("yyyy-MM-dd HH:mm:ss"); 
            string finalText = "";
            for (int i = 0; i < tabText.Length; i++)
            {
                finalText += tabText[i] + ",";
            }
            StreamReader sr = new StreamReader(path);
            String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            StreamWriter sw = new StreamWriter(path);
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i].Contains(text))
                {
                    rows[i] = rows[i].Replace(text, finalText);
                }
                if (rows[i] != "")
                    sw.WriteLine(rows[i]);
            }
            sw.Close();
        }

        //public void GetCountryList()
        //{
        //    List<string> cultureList = new List<string>();

        //    CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        //    foreach (CultureInfo culture in cultures)
        //    {
        //        RegionInfo region = new RegionInfo(culture.LCID);

        //        if (!(cultureList.Contains(region.EnglishName)))
        //        {
        //            cultureList.Add(region.EnglishName);
        //        }
        //    }
        //    cultureList = cultureList.OrderBy(q => q).ToList();
        //    foreach (string str in cultureList)
        //    {
        //        cbCountry.Items.Add(str);
        //    }
        //    //return cultureList;
        //}
        #endregion

        private void SaveCSV()
        {
            if (Globals.obligatoryField != null) { int iy = 1; }
            string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";
            string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";

            Hashtable tmpvalue = new Hashtable();
            if (File.Exists(TmpCsv))
            {
                try
                {
                    string strLine;

                    string[] arrLine = File.ReadAllLines(TmpCsv);
                    for (int j = 0; j < arrLine.Length; j++)
                    {
                        strLine = arrLine[j].Trim().ToUpper();
                        if (strLine != "")
                        {
                            var keyPair = strLine.Split(new char[] { '=' }, 2);
                            tmpvalue.Add(keyPair[0], keyPair[1]);
                        }
                    }

                }
                catch (Exception) { }
            }


        }

        private void AccessChoice1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (acceptChoice1.IsChecked == true)
            {
                acceptChoice1.IsChecked = false;
            }
            else
            {
                acceptChoice1.IsChecked = true;
            }
        }

        private void AccessChoice2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (acceptChoice2.IsChecked == true)
            {
                acceptChoice2.IsChecked = false;
            }
            else
            {
                acceptChoice2.IsChecked = true;
            }
        }

        private void AccessChoice3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (acceptChoice3.IsChecked == true)
            {
                acceptChoice3.IsChecked = false;
            }
            else
            {
                acceptChoice3.IsChecked = true;
            }
        }

        private void AccessChoice4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (acceptChoice4.IsChecked == true)
            {
                acceptChoice4.IsChecked = false;
            }
            else
            {
                acceptChoice4.IsChecked = true;
            }
        }
        private void GoToAcceuil(object sender, RoutedEventArgs e)
        {
            //SaveCSV();
            if (timerCountdown != null) timerCountdown.Stop();
            setTimeout();
            CallTakePicturePage();
        }

        private void GoToThanks(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            setTimeout();
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel.GoToThanks.CanExecute(null))
                viewModel.GoToThanks.Execute(null);
        }

        private void closeInformation(object sender, RoutedEventArgs e)
        {
            timerCountdown.Start();
            //buttonUtil.Visibility = Visibility.Collapsed;
        }

        private void navigate(object sender, RoutedEventArgs e)
        {

        }

        private bool checkMendatory()
        {
            bool notFilled = false;
            var valueToshow = hashMendatory.Where(x => x.Value == current_page.ToString()).ToList();
            foreach (var item in valueToshow)
            {
                foreach (var _item in hashResult)
                {
                    if (item.Key == _item.Key && string.IsNullOrEmpty(_item.Value))
                    {
                        notFilled = true;
                    }
                }
            }
            if (notFilled)
            {
                return true;
            }
            return false;
        }

        private bool checkMendatoryCarmellia()
        {
            bool notFilled = false;
            var valueToshow = hashMendatory.Where(x => x.Value == sigle).ToList();
            foreach (var item in valueToshow)
            {
                foreach (var _item in hashResult)
                {
                    if (item.Key == _item.Key && string.IsNullOrEmpty(_item.Value))
                    {
                        notFilled = true;
                    }
                }
            }
            if (notFilled)
            {
                return true;
            }
            return false;
        }

        private void ClickTextBlock(object sender, RoutedEventArgs e)
        {
            TextBlock txt = (TextBlock)sender;
            foreach (TextBlock _txt in listOptinTextCheck)
            {
                if (_txt.Name == txt.Name)
                {
                    string[] spearator = { "_txt" };
                    string shortString = _txt.Name.Split(spearator, 2, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                    foreach (CheckBox chk in listOptinCheckBox)
                    {
                        if (shortString == chk.Name)
                        {
                            if (chk.IsChecked == true)
                            {
                                hashResult[chk.Name] = "NON";
                                chk.IsChecked = false;
                            }
                            else
                            {
                                chk.IsChecked = true;
                                hashResult[chk.Name] = "OUI";
                            }

                        }

                    }
                }
            }
        }

        //private TextBlock setMultiLine (TextBlock txt, string toParse, string separator)
        //{
        //    string[] tabLines = toParse.Split('\n');
        //    for(int i = 0; i < tabLines.Length; i++)
        //    {
        //        txt.Inlines.Add(tabLines[i]);
        //        if(i<tabLines.Length - 1)
        //        {
        //            txt.Inlines.Add(new LineBreak());
        //        }

        //    }

        //}

        private void createPage()
        {
            try
            {
                Formulaire currentForm = getPages();
                if(currentForm != null)
                {
                    if (currentForm.lst_Page != null && currentForm.type != null)
                    {
                        typeFormulaire = currentForm.type;
                        lstPages = currentForm.lst_Page;
                    }
                    else if (currentForm.lst_Page == null && currentForm.type != null)
                    {
                        lstPages = null;
                        typeFormulaire = currentForm.type;
                    }
                }
                else
                {
                    lstPages = null;
                }
                
                if (lstPages!=null)
                {
                    List<Page> SortedList = lstPages.OrderBy(o => o.index).ToList();
                    ImageBrush btncancelform = new ImageBrush();
                    btncancelform.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnCancelForm, UriKind.Absolute));
                    ImageBrush btnprevious = new ImageBrush();
                    btnprevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormPrevious, UriKind.Absolute));

                    ImageBrush btnyeschecked = new ImageBrush();
                    btnyeschecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesChecked, UriKind.Absolute));

                    ImageBrush btnnounchecked = new ImageBrush();
                    btnnounchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnNoUnChecked, UriKind.Absolute));

                    ImageBrush btnnext = new ImageBrush();
                    btnnext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormNext, UriKind.Absolute));

                    foreach (Page _page in SortedList)
                    {
                        last_page += 1;
                        if (last_page == 1)
                        {
                            string pageName = "Page_" + last_page;
                            ModifyScrollShape(last_page);
                            StackPanel mainContainer = new StackPanel();
                            mainContainer.VerticalAlignment = VerticalAlignment.Center;

                            mainContainer.Orientation = Orientation.Vertical;
                            //mainContainer.Height = 600;
                            StackPanel closeContainer = new StackPanel();
                            closeContainer.Height = 75;
                            closeContainer.VerticalAlignment = VerticalAlignment.Top;

                            Button closeButton = new Button();
                            closeButton.Name = "HideFormulaire_" + last_page;
                            closeButton.HorizontalAlignment = HorizontalAlignment.Right;
                            closeButton.Style = this.FindResource("noHighlight") as Style;
                            closeButton.Margin = new Thickness(0, 30, 30, 0);
                            closeButton.Width = 195;
                            closeButton.Height = 58;
                            closeButton.Background = btncancelform;
                            closeButton.Click += CloseCurrentForm;
                            closeContainer.Children.Add(closeButton);
                            closeButton.Background = btncancelform;
                            mainContainer.Height = Double.NaN;// Page_1.Height - 100;
                            mainContainer.Children.Add(closeContainer);
                            StackPanel formContainer = new StackPanel();
                            formContainer.Orientation = Orientation.Vertical;
                            formContainer.Margin = new Thickness(0, 40, 0, 0);
                            //formContainer.Height = Double.NaN;
                            formContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            formContainer.VerticalAlignment = VerticalAlignment.Center;
                            TextBlock titreBlock = new TextBlock();
                            titreBlock.TextAlignment = TextAlignment.Center;
                            titreBlock.FontWeight = FontWeights.Bold;
                            titreBlock.FontSize = 35;
                            titreBlock.Text = removeChar(_page.title._Title);
                            titreBlock.Margin = new Thickness(20, 0, 18, 50);
                            titreBlock.Width = Double.NaN;
                            titreBlock.Height = Double.NaN;
                            titreBlock.Style = this.FindResource("textblockStyle") as Style;
                            formContainer.Children.Add(titreBlock);
                            var lstElements = _page.lstElements;
                            var lstTextBox = lstElements.Where(x => x.lst_textBox != null).ToList();
                            var lstTextBoxData = lstTextBox.Where(x => x.lst_textBox.type.ToLower() != "autre" && x.lst_textBox.type.ToLower() != "other").ToList();
                            var lstTextBoxSurvey = lstTextBox.Where(x => x.lst_textBox.type.ToLower() == "autre" || x.lst_textBox.type.ToLower() == "other").ToList();
                            var lstOtherControl = lstElements.Where(x => x.lst_comboBoxItem != null || x.lst_Optin != null || x.lst_radioButton != null).ToList();
                            StackPanel firstLineCoordonateContainer = new StackPanel();
                            firstLineCoordonateContainer.Orientation = Orientation.Horizontal;
                            firstLineCoordonateContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            firstLineCoordonateContainer.Width = Double.NaN;
                            firstLineCoordonateContainer.Height = Double.NaN;
                            StackPanel secondLineCoordonateContainer = new StackPanel();
                            secondLineCoordonateContainer.Height = Double.NaN;
                            secondLineCoordonateContainer.Orientation = Orientation.Horizontal;
                            secondLineCoordonateContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            if (lstTextBoxData.Count > 0)
                            {
                                if (lstTextBoxData.Count > 3)
                                {
                                    StackPanel coordonateContainer = new StackPanel();
                                    coordonateContainer.Orientation = Orientation.Vertical;

                                    int increment = 0;
                                    foreach (element _element in lstTextBoxData)
                                    {
                                        increment++;
                                        StackPanel stackTemp = new StackPanel();
                                        stackTemp.Orientation = Orientation.Vertical;
                                        stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                        stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                        if (_element.lst_textBox != null)
                                        {
                                            TextBox textBoxTemp = new TextBox();
                                            textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                            if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                            {

                                                textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                            }
                                            else
                                            {
                                                textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                            }

                                            textBoxTemp.Tag = _element.lst_textBox.type;
                                            headText.Add(_element.lst_textBox.text);
                                            textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                            hashResult.Add(textBoxTemp.Name, "");
                                            if (_element.lst_textBox.mendatory)
                                            {
                                                hashMendatory.Add(textBoxTemp.Name, last_page.ToString());
                                            }

                                            textBoxTemp.Text = _element.lst_textBox.text;
                                            textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                            textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                            hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                            hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                            textBoxTemp.LostFocus += CoordonneeTextBoxLostFocused;
                                            textBoxTemp.GotFocus += CoordonneeTextBoxGetFocused;
                                            stackTemp.Children.Add(textBoxTemp);
                                        }
                                        else if (_element.lst_radioButton != null)
                                        {
                                            StackPanel radioContent = new StackPanel();
                                            TextBlock radioTitle = new TextBlock();
                                            radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            radioTitle.Text = _element.lst_radioButton.Title;
                                            radioTitle.Margin = new Thickness(0, 20, 0, 40);
                                            headText.Add(_element.lst_radioButton.Title);
                                            StackPanel radioContainer = new StackPanel();

                                            radioContainer.Orientation = Orientation.Horizontal;
                                            radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                            hashResult.Add(_element.lst_radioButton.Name, "");
                                            foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                            {
                                                RadioButton radio = new RadioButton();
                                                string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                                radio.Content = "  " + item.item;
                                                radio.Checked += RadioButton_Checked;
                                                radio.Name = item.value;
                                                if (_element.lst_radioButton.mendatory)
                                                {
                                                    hashMendatory.Add(item.value, last_page.ToString());
                                                }

                                                radio.Tag = item.value + "_RADIO";

                                                radioContainer.Children.Add(radio);
                                            }
                                            radioContent.Children.Add(radioTitle);
                                            radioContent.Children.Add(radioContainer);
                                        }

                                        if (increment <= 3)
                                        {
                                            firstLineCoordonateContainer.Children.Add(stackTemp);
                                        }
                                        else
                                        {
                                            secondLineCoordonateContainer.Children.Add(stackTemp);
                                        }

                                    }
                                }
                                else
                                {
                                    StackPanel coordonateContainer = new StackPanel();
                                    coordonateContainer.Width = Double.NaN;
                                    coordonateContainer.Orientation = Orientation.Vertical;

                                    int increment = 0;
                                    foreach (element _element in lstTextBoxData)
                                    {
                                        increment++;
                                        StackPanel radioContent = new StackPanel();
                                        radioContent.Orientation = Orientation.Horizontal;
                                        StackPanel stackTemp = new StackPanel();
                                        stackTemp.Orientation = Orientation.Vertical;
                                        stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                        stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                        if (_element.lst_textBox != null)
                                        {
                                            TextBox textBoxTemp = new TextBox();
                                            textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                            if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                            {

                                                textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                            }
                                            else
                                            {
                                                textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                            }
                                            textBoxTemp.TextWrapping = TextWrapping.Wrap;
                                            textBoxTemp.Tag = _element.lst_textBox.type;
                                            headText.Add(_element.lst_textBox.text);
                                            textBoxTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                            textBoxTemp.VerticalAlignment = VerticalAlignment.Center;
                                            textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                            hashResult.Add(_element.lst_textBox.Name, "");
                                            if (_element.lst_textBox.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_textBox.Name, last_page.ToString());
                                            }

                                            textBoxTemp.Text = _element.lst_textBox.text;
                                            textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                            //textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                            hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                            hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                            textBoxTemp.LostFocus += CoordonneeTextBoxLostFocused;
                                            textBoxTemp.GotFocus += CoordonneeTextBoxGetFocused;
                                            textBoxTemp.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
                                            stackTemp.Children.Add(textBoxTemp);
                                        }
                                        else if (_element.lst_radioButton != null)
                                        {

                                            TextBlock radioTitle = new TextBlock();
                                            radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            radioTitle.Margin = new Thickness(0, 20, 0, 0);
                                            radioTitle.Text = "   " + _element.lst_radioButton.Title;
                                            radioTitle.VerticalAlignment = VerticalAlignment.Center;
                                            headText.Add(_element.lst_radioButton.Title);
                                            //radioTitle.Margin = new Thickness(20, 0, 0, 40);
                                            StackPanel radioContainer = new StackPanel();
                                            radioContainer.Orientation = Orientation.Horizontal;
                                            radioContainer.Margin = new Thickness(20, 0, 0, 0);
                                            radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                            if (_element.lst_radioButton.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_radioButton.Name, last_page.ToString());
                                            }
                                            hashResult.Add(_element.lst_radioButton.Name, "");
                                            foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                            {
                                                RadioButton radio = new RadioButton();
                                                string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                                radio.Checked += RadioButton_Checked;
                                                radio.Margin = new Thickness(20, 0, 0, 0);
                                                radio.Content = "         " + item.item;
                                                radio.Name = item.value;



                                                radio.Tag = item.value + "_RADIO";
                                                radioContainer.Children.Add(radio);
                                            }
                                            radioContent.Children.Add(radioTitle);
                                            radioContent.Children.Add(radioContainer);
                                        }

                                        firstLineCoordonateContainer.Children.Add(stackTemp);
                                        firstLineCoordonateContainer.Children.Add(radioContent);
                                    }

                                }
                                formContainer.Children.Add(firstLineCoordonateContainer);
                                formContainer.Children.Add(secondLineCoordonateContainer);
                            }

                            StackPanel fieldsContainer = new StackPanel();
                            fieldsContainer.Name = "fieldsContainer_" + last_page; ;
                            fieldsContainer.Orientation = Orientation.Vertical;
                            fieldsContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            if (lstTextBoxSurvey.Count > 0)
                            {
                                foreach (element _element in lstTextBoxSurvey)
                                {

                                    TextBlock textBoxSurveyTitle = new TextBlock();
                                    textBoxSurveyTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                    textBoxSurveyTitle.Margin = new Thickness(0, 20, 0, 0);
                                    textBoxSurveyTitle.Text = "   " + _element.lst_textBox.text;
                                    textBoxSurveyTitle.VerticalAlignment = VerticalAlignment.Center;
                                    headText.Add(_element.lst_textBox.text);

                                    TextBox textBoxTemp = new TextBox();
                                    textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                    if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                    {

                                        textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                    }
                                    else
                                    {
                                        textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                    }
                                    textBoxTemp.TextWrapping = TextWrapping.Wrap;
                                    textBoxTemp.Tag = _element.lst_textBox.type;
                                    headText.Add(_element.lst_textBox.text);
                                    textBoxTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                    textBoxTemp.VerticalAlignment = VerticalAlignment.Center;
                                    textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                    hashResult.Add(_element.lst_textBox.Name, "");
                                    if (_element.lst_textBox.mendatory)
                                    {
                                        hashMendatory.Add(_element.lst_textBox.Name, last_page.ToString());
                                    }

                                    //textBoxTemp.Text = _element.lst_textBox.text;
                                    textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                    //textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                    hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                    hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                    textBoxTemp.LostFocus += SurveyTextBoxLostFocused;
                                    textBoxTemp.GotFocus += SurveyTextBoxGetFocused;
                                    textBoxTemp.GotKeyboardFocus += SurveyTextBoxGetFocused;
                                    fieldsContainer.Children.Add(textBoxSurveyTitle);
                                    fieldsContainer.Children.Add(textBoxTemp);
                                }
                            }

                            if (lstOtherControl.Count > 0)
                            {
                                foreach (element _element in lstOtherControl)
                                {
                                    StackPanel stackTemp = new StackPanel();
                                    stackTemp.Orientation = Orientation.Vertical;
                                    stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                    stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                    if (_element.lst_comboBoxItem != null)
                                    {
                                        TextBlock comboTitle = new TextBlock();
                                        comboTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                        comboTitle.Text = _element.lst_comboBoxItem.Title;
                                        comboTitle.FontSize = 25;
                                        headText.Add(_element.lst_comboBoxItem.Title);
                                        comboTitle.Margin = new Thickness(0, 20, 0, 40);
                                        ComboBox cmTemp = new ComboBox();
                                        cmTemp.Width = 500;
                                        cmTemp.SelectionChanged += combo_SelectionChanged;
                                        cmTemp.Name = _element.lst_comboBoxItem.Name;
                                        hashResult.Add(_element.lst_comboBoxItem.Name, "");
                                        if (_element.lst_comboBoxItem.mendatory)
                                        {
                                            hashMendatory.Add(_element.lst_comboBoxItem.Name, last_page.ToString());
                                        }

                                        cmTemp.Height = 35;
                                        cmTemp.FontSize = 20;
                                        //cmTemp.Style = this.FindResource("watermarkCombo") as Style;
                                        foreach (string item in _element.lst_comboBoxItem.Combobox_items)
                                        {
                                            cmTemp.Items.Add(item);
                                        }
                                        fieldsContainer.Children.Add(comboTitle);
                                        fieldsContainer.Children.Add(cmTemp);
                                    }
                                    if (_element.lst_radioButton != null)
                                    {
                                        TextBlock radioTitle = new TextBlock();
                                        radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                        radioTitle.Text = _element.lst_radioButton.Title;
                                        headText.Add(_element.lst_radioButton.Title);
                                        radioTitle.FontSize = 25;
                                        radioTitle.HorizontalAlignment = HorizontalAlignment.Center;
                                        radioTitle.Margin = new Thickness(0, 20, 0, 20);
                                        StackPanel radioContainer = new StackPanel();
                                        radioContainer.Orientation = Orientation.Horizontal;
                                        //radioContainer.Margin = new Thickness(0, 0, 0, 0);
                                        radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                        radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                        hashResult.Add(_element.lst_radioButton.Name, "");
                                        if (_element.lst_radioButton.mendatory)
                                        {
                                            hashMendatory.Add(_element.lst_radioButton.Name, last_page.ToString());
                                        }
                                        foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                        {
                                            RadioButton radio = new RadioButton();
                                            string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                            radio.Content = "  " + item.item;
                                            radio.Margin = new Thickness(10, 0, 0, 0);
                                            radio.Checked += RadioButton_Checked;
                                            radio.Name = _element.lst_radioButton.Name + "_" + item.value;



                                            radio.Tag = item.value + "_RADIO";
                                            radioContainer.Children.Add(radio);
                                        }
                                        fieldsContainer.Children.Add(radioTitle);
                                        fieldsContainer.Children.Add(radioContainer);
                                    }
                                    if (_element.lst_Optin != null)
                                    {
                                        if (_element.lst_Optin.Type == "CheckBox")
                                        {
                                            StackPanel optinCheckBoxContainer = new StackPanel();
                                            optinCheckBoxContainer.Height = Double.NaN;
                                            optinCheckBoxContainer.Orientation = Orientation.Horizontal;
                                            optinCheckBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            optinCheckBoxContainer.Margin = new Thickness(0, 0, 0, 10);

                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockTitleyStyle") as Style;

                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(10, 0, 0, 5);
                                            txtTemp.MouseLeftButtonDown += ClickTextBlock;
                                            txtTemp.VerticalAlignment = VerticalAlignment.Center;
                                            txtTemp.FontSize = 15;
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Name = _element.lst_Optin.identifiant + "_txt";
                                            txtTemp.Width = 800;
                                            txtTemp.Height = Double.NaN;

                                            string checkBoxText = removeChar(_element.lst_Optin.Label);
                                            txtTemp.Text = checkBoxText.Trim('\n', '\r');
                                            listOptinTextCheck.Add(txtTemp);
                                            CheckBox optinCheckBox = new CheckBox();
                                            optinCheckBox.VerticalAlignment = VerticalAlignment.Top;
                                            optinCheckBox.Name = _element.lst_Optin.identifiant;

                                            optinCheckBox.Click += CheckBoxChanged;
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }

                                            //optinCheckBox.Margin = new Thickness(15, 0, 0, 0);
                                            optinCheckBoxContainer.Children.Add(optinCheckBox);
                                            optinCheckBoxContainer.Children.Add(txtTemp);
                                            listOptinCheckBox.Add(optinCheckBox);
                                            fieldsContainer.Children.Add(optinCheckBoxContainer);
                                        }
                                        else if (_element.lst_Optin.Type == "Accept")
                                        {
                                            StackPanel optinCheckBoxContainer = new StackPanel();
                                            optinCheckBoxContainer.Height = Double.NaN;
                                            optinCheckBoxContainer.Orientation = Orientation.Vertical;
                                            optinCheckBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            optinCheckBoxContainer.Margin = new Thickness(0, 0, 0, 10);
                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockTitleyStyle") as Style;

                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(-10, 20, 0, 10);
                                            txtTemp.MouseLeftButtonDown += ClickTextBlock;
                                            txtTemp.VerticalAlignment = VerticalAlignment.Center;
                                            txtTemp.FontSize = 15;
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Name = _element.lst_Optin.identifiant + "_txt";
                                            txtTemp.Width = 800;
                                            txtTemp.Height = Double.NaN;
                                            string checkBoxText = removeChar(_element.lst_Optin.Label);
                                            txtTemp.Text = checkBoxText.Trim('\n', '\r');
                                            listOptinTextCheck.Add(txtTemp);
                                            CheckBox optinCheckBox = new CheckBox();
                                            optinCheckBox.VerticalAlignment = VerticalAlignment.Top;
                                            optinCheckBox.Name = _element.lst_Optin.identifiant;
                                            optinCheckBox.Content = "Accept";

                                            optinCheckBox.Click += CheckBoxChanged;
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }

                                            //optinCheckBox.Margin = new Thickness(15, 0, 0, 0);
                                            optinCheckBoxContainer.Children.Add(txtTemp);
                                            optinCheckBoxContainer.Children.Add(optinCheckBox);

                                            listOptinCheckBox.Add(optinCheckBox);
                                            fieldsContainer.Children.Add(optinCheckBoxContainer);
                                        }
                                        else
                                        {
                                            StackPanel optinToggleButtonBoxContainer = new StackPanel();
                                            optinToggleButtonBoxContainer.Orientation = Orientation.Vertical;
                                            optinToggleButtonBoxContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            optinToggleButtonBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            txtTemp.Text = _element.lst_Optin.Label;
                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(0, 20, 0, 40);
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Width = Double.NaN;
                                            optinToggleButtonBoxContainer.Children.Add(txtTemp);
                                            StackPanel toggleContainer = new StackPanel();
                                            toggleContainer.Orientation = Orientation.Horizontal;
                                            toggleContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            toggleContainer.Margin = new Thickness(0, 0, 0, 20);

                                            ToggleButton yesButton = new ToggleButton();
                                            yesButton.Name = _element.lst_Optin.identifiant + "_yes";
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }
                                            ImageBrush btnyesunchecked = new ImageBrush();
                                            btnyesunchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesUnChecked, UriKind.Absolute));
                                            yesButton.Width = 172;
                                            yesButton.Height = 60;
                                            yesButton.FontSize = 20;
                                            yesButton.PreviewMouseDown += ChangeToggleEvent;
                                            yesButton.Background = btnyesunchecked;
                                            yesButton.Style = this.FindResource("whitewhithborder") as Style;
                                            _lst_Optin.Add(yesButton);
                                            ToggleButton noButton = new ToggleButton();
                                            noButton.Name = _element.lst_Optin.identifiant + "_no";
                                            noButton.Width = 172;
                                            noButton.Height = 60;
                                            noButton.PreviewMouseDown += ChangeToggleEvent;
                                            noButton.FontSize = 20;
                                            noButton.Background = btnnounchecked;
                                            noButton.Style = this.FindResource("whitewhithborder") as Style;
                                            _lst_Optin.Add(noButton);
                                            toggleContainer.Children.Add(yesButton);
                                            toggleContainer.Children.Add(noButton);
                                            optinToggleButtonBoxContainer.Children.Add(toggleContainer);
                                            fieldsContainer.Children.Add(optinToggleButtonBoxContainer);
                                        }

                                    }

                                }

                            }

                            formContainer.Children.Add(fieldsContainer);
                            mainContainer.Children.Add(formContainer);
                            StackPanel buttonContainer = new StackPanel();
                            buttonContainer.Orientation = Orientation.Horizontal;
                            buttonContainer.HorizontalAlignment = HorizontalAlignment.Center;


                            Button next = new Button();
                            next.Style = this.FindResource("noHighlight") as Style;
                            next.HorizontalAlignment = HorizontalAlignment.Center;
                            next.Width = 320;
                            next.Height = 60;
                            next.Click += continueToNextForm;
                            next.Opacity = 1;
                            next.Margin = new Thickness(43, 50, 43, 20);
                            next.Name = "Next" + last_page;
                            next.Background = btnnext;
                            buttonContainer.Children.Add(next);
                            //}
                            mainContainer.Children.Add(buttonContainer);

                            main_Page1.Children.Add(mainContainer);

                        }
                        else
                        {
                            string pageName = "Page_" + last_page;
                            //Add main scrollviewer

                            ModifyScrollShape(last_page);
                            StackPanel mainContainer = new StackPanel();
                            mainContainer.Orientation = Orientation.Vertical;
                            mainContainer.VerticalAlignment = VerticalAlignment.Center;
                            StackPanel closeContainer = new StackPanel();
                            closeContainer.Height = 100;
                            Button closeButton = new Button();
                            closeButton.Name = "HideFormulaire_" + last_page;
                            closeButton.HorizontalAlignment = HorizontalAlignment.Right;
                            closeButton.Style = this.FindResource("noHighlight") as Style;
                            closeButton.Margin = new Thickness(0, 30, 30, 0);
                            closeButton.Width = 195;
                            closeButton.Click += CloseCurrentForm;
                            closeButton.Height = 58;
                            closeButton.Background = btncancelform;
                            closeContainer.Children.Add(closeButton);
                            mainContainer.Children.Add(closeContainer);
                            TextBlock titreBlock = new TextBlock();
                            titreBlock.TextAlignment = TextAlignment.Center;
                            titreBlock.FontWeight = FontWeights.Bold;
                            titreBlock.FontSize = 35;
                            titreBlock.Text = _page.title._Title;
                            titreBlock.Margin = new Thickness(20, 0, 18, 50);
                            titreBlock.Width = Double.NaN;
                            titreBlock.Height = Double.NaN;
                            titreBlock.Style = this.FindResource("textblockStyle") as Style;
                            StackPanel formContainer = new StackPanel();
                            formContainer.Orientation = Orientation.Vertical;
                            formContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            formContainer.VerticalAlignment = VerticalAlignment.Center;
                            formContainer.Children.Add(titreBlock);
                            var lstElements = _page.lstElements;
                            var lstTextBox = lstElements.Where(x => x.lst_textBox != null).ToList();
                            var lstTextBoxData = lstTextBox.Where(x => x.lst_textBox.type.ToLower() != "autre" && x.lst_textBox.type.ToLower() != "other").ToList();
                            var lstTextBoxSurvey = lstTextBox.Where(x => x.lst_textBox.type.ToLower() == "autre" || x.lst_textBox.type.ToLower() == "other").ToList();
                            var lstOtherControl = lstElements.Where(x => x.lst_comboBoxItem != null || x.lst_Optin != null || x.lst_radioButton != null).ToList();
                            StackPanel firstLineCoordonateContainer = new StackPanel();
                            firstLineCoordonateContainer.Orientation = Orientation.Horizontal;
                            firstLineCoordonateContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            firstLineCoordonateContainer.Width = Double.NaN;
                            firstLineCoordonateContainer.Height = Double.NaN;
                            StackPanel secondLineCoordonateContainer = new StackPanel();
                            secondLineCoordonateContainer.Height = Double.NaN;
                            secondLineCoordonateContainer.Orientation = Orientation.Horizontal;
                            secondLineCoordonateContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            if (lstTextBoxData.Count > 0)
                            {
                                if (lstTextBoxData.Count > 3)
                                {
                                    StackPanel coordonateContainer = new StackPanel();
                                    coordonateContainer.Orientation = Orientation.Vertical;

                                    int increment = 0;
                                    foreach (element _element in lstTextBoxData)
                                    {
                                        increment++;
                                        StackPanel stackTemp = new StackPanel();
                                        stackTemp.Orientation = Orientation.Vertical;
                                        stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                        stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                        if (_element.lst_textBox != null)
                                        {
                                            TextBox textBoxTemp = new TextBox();
                                            textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                            if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                            {

                                                textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                            }
                                            else
                                            {
                                                textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                            }

                                            textBoxTemp.Tag = _element.lst_textBox.type;
                                            headText.Add(_element.lst_textBox.text);
                                            textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                            hashResult.Add(textBoxTemp.Name, "");
                                            if (_element.lst_textBox.mendatory)
                                            {
                                                hashMendatory.Add(textBoxTemp.Name, last_page.ToString());
                                            }

                                            textBoxTemp.Text = _element.lst_textBox.text;
                                            textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                            textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                            hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                            hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                            textBoxTemp.LostFocus += CoordonneeTextBoxLostFocused;
                                            textBoxTemp.GotFocus += CoordonneeTextBoxGetFocused;
                                            stackTemp.Children.Add(textBoxTemp);
                                        }
                                        else if (_element.lst_radioButton != null)
                                        {
                                            StackPanel radioContent = new StackPanel();
                                            TextBlock radioTitle = new TextBlock();
                                            radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            radioTitle.Text = _element.lst_radioButton.Title;
                                            radioTitle.Margin = new Thickness(0, 20, 0, 40);
                                            headText.Add(_element.lst_radioButton.Title);
                                            StackPanel radioContainer = new StackPanel();

                                            radioContainer.Orientation = Orientation.Horizontal;
                                            radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                            hashResult.Add(_element.lst_radioButton.Name, "");
                                            foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                            {
                                                RadioButton radio = new RadioButton();
                                                string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                                radio.Content = "  " + item.item;
                                                radio.Checked += RadioButton_Checked;
                                                radio.Name = item.value;
                                                if (_element.lst_radioButton.mendatory)
                                                {
                                                    hashMendatory.Add(item.value, last_page.ToString());
                                                }

                                                radio.Tag = item.value + "_RADIO";

                                                radioContainer.Children.Add(radio);
                                            }
                                            radioContent.Children.Add(radioTitle);
                                            radioContent.Children.Add(radioContainer);
                                        }

                                        if (increment <= 3)
                                        {
                                            firstLineCoordonateContainer.Children.Add(stackTemp);
                                        }
                                        else
                                        {
                                            secondLineCoordonateContainer.Children.Add(stackTemp);
                                        }

                                    }
                                }
                                else
                                {
                                    StackPanel coordonateContainer = new StackPanel();
                                    coordonateContainer.Width = Double.NaN;
                                    coordonateContainer.Orientation = Orientation.Vertical;

                                    int increment = 0;
                                    StackPanel radioContent = new StackPanel();
                                    radioContent.Orientation = Orientation.Horizontal;
                                    StackPanel stackTemp = new StackPanel();
                                    stackTemp.Orientation = Orientation.Horizontal;
                                    stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                    stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                    foreach (element _element in lstTextBoxData)
                                    {
                                        increment++;

                                        if (_element.lst_textBox != null)
                                        {
                                            TextBox textBoxTemp = new TextBox();
                                            textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                            if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                            {

                                                textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                            }
                                            else
                                            {
                                                textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                            }
                                            textBoxTemp.TextWrapping = TextWrapping.Wrap;
                                            textBoxTemp.Tag = _element.lst_textBox.type;
                                            headText.Add(_element.lst_textBox.text);
                                            textBoxTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                            textBoxTemp.VerticalAlignment = VerticalAlignment.Center;
                                            textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                            hashResult.Add(_element.lst_textBox.Name, "");
                                            if (_element.lst_textBox.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_textBox.Name, last_page.ToString());
                                            }

                                            textBoxTemp.Text = _element.lst_textBox.text;
                                            textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                            //textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                            hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                            hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                            textBoxTemp.LostFocus += CoordonneeTextBoxLostFocused;
                                            textBoxTemp.GotFocus += CoordonneeTextBoxGetFocused;
                                            textBoxTemp.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
                                            stackTemp.Children.Add(textBoxTemp);
                                        }
                                        else if (_element.lst_radioButton != null)
                                        {

                                            TextBlock radioTitle = new TextBlock();
                                            radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            radioTitle.Margin = new Thickness(0, 20, 0, 0);
                                            radioTitle.Text = "   " + _element.lst_radioButton.Title;
                                            radioTitle.VerticalAlignment = VerticalAlignment.Center;
                                            headText.Add(_element.lst_radioButton.Title);
                                            //radioTitle.Margin = new Thickness(20, 0, 0, 40);
                                            StackPanel radioContainer = new StackPanel();
                                            radioContainer.Orientation = Orientation.Horizontal;
                                            radioContainer.Margin = new Thickness(20, 0, 0, 0);
                                            radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                            if (_element.lst_radioButton.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_radioButton.Name, last_page.ToString());
                                            }
                                            hashResult.Add(_element.lst_radioButton.Name, "");
                                            foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                            {
                                                RadioButton radio = new RadioButton();
                                                string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                                radio.Checked += RadioButton_Checked;
                                                radio.Margin = new Thickness(20, 0, 0, 0);
                                                radio.Content = "         " + item.item;
                                                radio.Name = item.value;



                                                radio.Tag = item.value + "_RADIO";
                                                radioContainer.Children.Add(radio);
                                            }
                                            radioContent.Children.Add(radioTitle);
                                            radioContent.Children.Add(radioContainer);
                                        }


                                    }
                                    firstLineCoordonateContainer.Children.Add(stackTemp);
                                    firstLineCoordonateContainer.Children.Add(radioContent);
                                }
                            }
                            formContainer.Children.Add(firstLineCoordonateContainer);
                            formContainer.Children.Add(secondLineCoordonateContainer);
                            StackPanel fieldsContainer = new StackPanel();
                            fieldsContainer.Name = "fieldsContainer_" + last_page; ;
                            fieldsContainer.Orientation = Orientation.Vertical;
                            fieldsContainer.HorizontalAlignment = HorizontalAlignment.Center;
                            if (lstTextBoxSurvey.Count > 0)
                            {
                                foreach (element _element in lstTextBoxData)
                                {

                                    TextBlock textBoxSurveyTitle = new TextBlock();
                                    textBoxSurveyTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                    textBoxSurveyTitle.Margin = new Thickness(0, 20, 0, 0);
                                    textBoxSurveyTitle.Text = "   " + _element.lst_textBox.text;
                                    textBoxSurveyTitle.VerticalAlignment = VerticalAlignment.Center;
                                    headText.Add(_element.lst_textBox.text);

                                    TextBox textBoxTemp = new TextBox();
                                    textBoxTemp.CharacterCasing = CharacterCasing.Lower;
                                    if (Globals.ScreenType.ToUpper() == "SPHERIK")
                                    {

                                        textBoxTemp.Style = this.FindResource("textboxcoordonneesSpherik") as Style;
                                    }
                                    else
                                    {
                                        textBoxTemp.Style = this.FindResource("textboxcoordonnees") as Style;
                                    }
                                    textBoxTemp.TextWrapping = TextWrapping.Wrap;
                                    textBoxTemp.Tag = _element.lst_textBox.type;
                                    headText.Add(_element.lst_textBox.text);
                                    textBoxTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                    textBoxTemp.VerticalAlignment = VerticalAlignment.Center;
                                    textBoxTemp.Name = _element.lst_textBox.Name.Trim();
                                    hashResult.Add(_element.lst_textBox.Name, "");
                                    if (_element.lst_textBox.mendatory)
                                    {
                                        hashMendatory.Add(_element.lst_textBox.Name, last_page.ToString());
                                    }

                                    //textBoxTemp.Text = _element.lst_textBox.text;
                                    textBoxTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ebebeb"));
                                    //textBoxTemp.Margin = new Thickness(20, 20, 0, 0);
                                    hash.Add(_element.lst_textBox.text, _element.lst_textBox.type);
                                    hashTextBox.Add("key_" + _element.lst_textBox.Name, _element.lst_textBox.text);
                                    textBoxTemp.LostFocus += SurveyTextBoxLostFocused;
                                    textBoxTemp.GotFocus += SurveyTextBoxGetFocused;
                                    textBoxTemp.GotKeyboardFocus += SurveyTextBoxGetFocused;
                                    fieldsContainer.Children.Add(textBoxSurveyTitle);
                                    fieldsContainer.Children.Add(textBoxTemp);
                                }
                            }

                            if (lstOtherControl.Count > 0)
                            {
                                foreach (element _element in lstElements)
                                {
                                    StackPanel stackTemp = new StackPanel();
                                    stackTemp.Orientation = Orientation.Vertical;
                                    stackTemp.HorizontalAlignment = HorizontalAlignment.Center;
                                    stackTemp.VerticalAlignment = VerticalAlignment.Center;
                                    if (_element.lst_comboBoxItem != null)
                                    {
                                        TextBlock comboTitle = new TextBlock();
                                        comboTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                        comboTitle.Text = _element.lst_comboBoxItem.Title;
                                        comboTitle.FontSize = 25;
                                        headText.Add(_element.lst_comboBoxItem.Title);
                                        comboTitle.Margin = new Thickness(0, 20, 0, 40);
                                        ComboBox cmTemp = new ComboBox();
                                        cmTemp.Width = 500;
                                        cmTemp.SelectionChanged += combo_SelectionChanged;
                                        cmTemp.Name = _element.lst_comboBoxItem.Name;
                                        hashResult.Add(_element.lst_comboBoxItem.Name, "");
                                        if (_element.lst_comboBoxItem.mendatory)
                                        {
                                            hashMendatory.Add(_element.lst_comboBoxItem.Name, last_page.ToString());
                                        }

                                        cmTemp.Height = 35;
                                        cmTemp.FontSize = 20;
                                        //cmTemp.Style = this.FindResource("watermarkCombo") as Style;
                                        foreach (string item in _element.lst_comboBoxItem.Combobox_items)
                                        {
                                            cmTemp.Items.Add(item);
                                        }
                                        fieldsContainer.Children.Add(comboTitle);
                                        fieldsContainer.Children.Add(cmTemp);
                                    }
                                    if (_element.lst_radioButton != null)
                                    {
                                        TextBlock radioTitle = new TextBlock();
                                        radioTitle.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                        radioTitle.Text = _element.lst_radioButton.Title;
                                        headText.Add(_element.lst_radioButton.Title);
                                        radioTitle.FontSize = 25;
                                        radioTitle.HorizontalAlignment = HorizontalAlignment.Center;
                                        radioTitle.Margin = new Thickness(0, 20, 0, 20);
                                        StackPanel radioContainer = new StackPanel();
                                        radioContainer.Orientation = Orientation.Horizontal;
                                        //radioContainer.Margin = new Thickness(0, 0, 0, 0);
                                        radioContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                        radioContainer.VerticalAlignment = VerticalAlignment.Center;
                                        hashResult.Add(_element.lst_radioButton.Name, "");
                                        if (_element.lst_radioButton.mendatory)
                                        {
                                            hashMendatory.Add(_element.lst_radioButton.Name, last_page.ToString());
                                        }
                                        foreach (var item in _element.lst_radioButton.Radiobuttons_items)
                                        {
                                            RadioButton radio = new RadioButton();
                                            string radioName = _element.lst_radioButton.Name + "_" + item.value;
                                            radio.Content = "  " + item.item;
                                            radio.Margin = new Thickness(10, 0, 0, 0);
                                            radio.Checked += RadioButton_Checked;
                                            radio.Name = _element.lst_radioButton.Name + "_" + item.value;



                                            radio.Tag = item.value + "_RADIO";
                                            radioContainer.Children.Add(radio);
                                        }
                                        fieldsContainer.Children.Add(radioTitle);
                                        fieldsContainer.Children.Add(radioContainer);
                                    }
                                    if (_element.lst_Optin != null)
                                    {
                                        if (_element.lst_Optin.Type == "CheckBox")
                                        {
                                            StackPanel optinCheckBoxContainer = new StackPanel();
                                            optinCheckBoxContainer.Height = Double.NaN;
                                            optinCheckBoxContainer.Orientation = Orientation.Horizontal;
                                            optinCheckBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            optinCheckBoxContainer.Margin = new Thickness(0, 0, 0, 10);

                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockTitleyStyle") as Style;

                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(10, 0, 0, 5);
                                            txtTemp.MouseLeftButtonDown += ClickTextBlock;
                                            txtTemp.VerticalAlignment = VerticalAlignment.Center;
                                            txtTemp.FontSize = 15;
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Name = _element.lst_Optin.identifiant + "_txt";
                                            txtTemp.Width = 800;
                                            txtTemp.Height = Double.NaN;

                                            string checkBoxText = removeChar(_element.lst_Optin.Label);
                                            txtTemp.Text = checkBoxText.Trim('\n', '\r');
                                            listOptinTextCheck.Add(txtTemp);
                                            CheckBox optinCheckBox = new CheckBox();
                                            optinCheckBox.VerticalAlignment = VerticalAlignment.Top;
                                            optinCheckBox.Name = _element.lst_Optin.identifiant;

                                            optinCheckBox.Click += CheckBoxChanged;
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }

                                            //optinCheckBox.Margin = new Thickness(15, 0, 0, 0);
                                            optinCheckBoxContainer.Children.Add(optinCheckBox);
                                            optinCheckBoxContainer.Children.Add(txtTemp);
                                            listOptinCheckBox.Add(optinCheckBox);
                                            fieldsContainer.Children.Add(optinCheckBoxContainer);
                                        }
                                        else if (_element.lst_Optin.Type == "Accept")
                                        {
                                            StackPanel optinCheckBoxContainer = new StackPanel();
                                            optinCheckBoxContainer.Height = Double.NaN;
                                            optinCheckBoxContainer.Orientation = Orientation.Vertical;
                                            optinCheckBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            optinCheckBoxContainer.Margin = new Thickness(0, 0, 0, 10);
                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockTitleyStyle") as Style;

                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(-10, 20, 0, 10);
                                            txtTemp.MouseLeftButtonDown += ClickTextBlock;
                                            txtTemp.VerticalAlignment = VerticalAlignment.Center;
                                            txtTemp.FontSize = 15;
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Name = _element.lst_Optin.identifiant + "_txt";
                                            txtTemp.Width = 800;
                                            txtTemp.Height = Double.NaN;
                                            string checkBoxText = removeChar(_element.lst_Optin.Label);
                                            txtTemp.Text = checkBoxText.Trim('\n', '\r');
                                            listOptinTextCheck.Add(txtTemp);
                                            CheckBox optinCheckBox = new CheckBox();
                                            optinCheckBox.VerticalAlignment = VerticalAlignment.Top;
                                            optinCheckBox.Name = _element.lst_Optin.identifiant;
                                            optinCheckBox.Content = "Accept";

                                            optinCheckBox.Click += CheckBoxChanged;
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }

                                            //optinCheckBox.Margin = new Thickness(15, 0, 0, 0);
                                            optinCheckBoxContainer.Children.Add(txtTemp);
                                            optinCheckBoxContainer.Children.Add(optinCheckBox);

                                            listOptinCheckBox.Add(optinCheckBox);
                                            fieldsContainer.Children.Add(optinCheckBoxContainer);
                                        }
                                        else
                                        {
                                            StackPanel optinToggleButtonBoxContainer = new StackPanel();
                                            optinToggleButtonBoxContainer.Orientation = Orientation.Vertical;
                                            optinToggleButtonBoxContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            optinToggleButtonBoxContainer.VerticalAlignment = VerticalAlignment.Center;
                                            TextBlock txtTemp = new TextBlock();
                                            txtTemp.Style = this.FindResource("textblockMandatoryStyle") as Style;
                                            txtTemp.Text = _element.lst_Optin.Label;
                                            headText.Add(_element.lst_Optin.Label);
                                            txtTemp.Margin = new Thickness(0, 20, 0, 40);
                                            txtTemp.FontWeight = FontWeights.Bold;
                                            txtTemp.Width = Double.NaN;
                                            optinToggleButtonBoxContainer.Children.Add(txtTemp);
                                            StackPanel toggleContainer = new StackPanel();
                                            toggleContainer.Orientation = Orientation.Horizontal;
                                            toggleContainer.HorizontalAlignment = HorizontalAlignment.Center;
                                            toggleContainer.Margin = new Thickness(0, 0, 0, 20);

                                            ToggleButton yesButton = new ToggleButton();
                                            yesButton.Name = _element.lst_Optin.identifiant + "_yes";
                                            hashResult.Add(_element.lst_Optin.identifiant, "");
                                            if (_element.lst_Optin.mendatory)
                                            {
                                                hashMendatory.Add(_element.lst_Optin.identifiant, last_page.ToString());
                                            }
                                            ImageBrush btnyesunchecked = new ImageBrush();
                                            btnyesunchecked.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnYesUnChecked, UriKind.Absolute));
                                            yesButton.Width = 172;
                                            yesButton.Height = 60;
                                            yesButton.FontSize = 20;
                                            yesButton.PreviewMouseDown += ChangeToggleEvent;
                                            yesButton.Background = btnyesunchecked;
                                            yesButton.Style = this.FindResource("whitewhithborder") as Style;
                                            _lst_Optin.Add(yesButton);
                                            ToggleButton noButton = new ToggleButton();
                                            noButton.Name = _element.lst_Optin.identifiant + "_no";
                                            noButton.Width = 172;
                                            noButton.Height = 60;
                                            noButton.PreviewMouseDown += ChangeToggleEvent;
                                            noButton.FontSize = 20;
                                            noButton.Background = btnnounchecked;
                                            noButton.Style = this.FindResource("whitewhithborder") as Style;
                                            _lst_Optin.Add(noButton);
                                            toggleContainer.Children.Add(yesButton);
                                            toggleContainer.Children.Add(noButton);
                                            optinToggleButtonBoxContainer.Children.Add(toggleContainer);
                                            fieldsContainer.Children.Add(optinToggleButtonBoxContainer);
                                        }

                                    }

                                }

                            }

                            StackPanel buttonsContainer = new StackPanel();
                            buttonsContainer.Orientation = Orientation.Horizontal;
                            buttonsContainer.HorizontalAlignment = HorizontalAlignment.Center;

                            //if (_page.navigation.previous == true)
                            //{
                            Button previous = new Button();
                            previous.Style = this.FindResource("noHighlight") as Style;
                            previous.HorizontalAlignment = HorizontalAlignment.Center;
                            previous.Width = 320;
                            previous.Height = 60;
                            previous.Opacity = 1;
                            previous.Click += backToPreviousForm;
                            previous.Margin = new Thickness(43, 50, 43, 80);
                            previous.Name = "Back_" + last_page;
                            previous.Background = btnprevious;
                            buttonsContainer.Children.Add(previous);
                            //}
                            //if (_page.navigation.next == true)
                            //{
                            Button next = new Button();
                            next.Style = this.FindResource("noHighlight") as Style;
                            next.HorizontalAlignment = HorizontalAlignment.Center;
                            next.Width = 320;
                            next.Click += continueToNextForm;
                            next.Height = 60;
                            next.Opacity = 1;
                            next.Margin = new Thickness(43, 50, 43, 80);
                            next.Name = "Next" + last_page;
                            next.Background = btnnext;
                            buttonsContainer.Children.Add(next);
                            //}
                            formContainer.Children.Add(fieldsContainer);
                            mainContainer.Children.Add(formContainer);
                            mainContainer.Children.Add(buttonsContainer);
                            if (last_page == 2)
                            {
                                main_Page2.Children.Add(mainContainer);
                            }
                            else if (last_page == 3)
                            {
                                main_Page3.Children.Add(mainContainer);
                            }
                            else if (last_page == 4)
                            {
                                main_Page4.Children.Add(mainContainer);
                            }
                            else if (last_page == 5)
                            {
                                main_Page5.Children.Add(mainContainer);
                            }
                            else if (last_page == 6)
                            {
                                main_Page6.Children.Add(mainContainer);
                            }
                            else if (last_page == 7)
                            {
                                main_Page7.Children.Add(mainContainer);
                            }
                            else if (last_page == 8)
                            {
                                main_Page8.Children.Add(mainContainer);
                            }
                            else if (last_page == 9)
                            {
                                main_Page9.Children.Add(mainContainer);
                            }
                            else if (last_page == 10)
                            {
                                main_Page10.Children.Add(mainContainer);
                            }
                            //parent.Children.Add(mainContainer);
                            //mainContainer.Children.Add(keyboardContainer);
                            //newestPage.Content = mainContainer;
                            //parent.Children.Add(newestPage);

                        }

                    }
                }
            }
            catch (Exception e)
            {
                Log.Fatal("Erreur lors du chargement du formulaire: " + e.Message);
                activated = false;
            }


        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            Globals._FlagConfig = "client";
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_From != null) timerCountdown_From.Stop();
            if (timerCountdown_print != null) timerCountdown_print.Stop();
            setTimeout();
            var viewModel = (VisualisationViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToConnexionClient.CanExecute(null))
                    viewModel.GoToConnexionClient.Execute(null);
                /*if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }*/
            }
        }

        public void Page_Click(object sender, EventArgs e)
        {
            timer_Form = 300;
        }

        
    }
}
