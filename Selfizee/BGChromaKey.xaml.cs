﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections;
using Selfizee.Models.Enum;
using Selfizee.Models;
using Selfizee.Managers;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Threading;
using Selfizee.View;
using System.Drawing;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for BGChromaKey.xaml
    /// </summary>
    public partial class BGChromaKey : UserControl
    {

        private string _imageFolder = @"View"; //Globals.directory_Target; 
        private string _imageFinalFolder = @"Final";
        private string _imageBGChromakey = @"Greenscreen";
        private ArrayList _imageFiles;
        private string _filtered;
        private string path_ToSave = "";
        private string _imageFrame = @"Frames";
        private string _finalFolder = @"Final";
        String currDir = "";

        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);

        CollectionViewSource view = new CollectionViewSource();

        int currentPageIndex = 0;
        int itemPerPage = 6;
        int totalPage = 0;

        ImageCollection _photos;
        private int timeout { get; set; }
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;

        public BGChromaKey()
        {
            InitializeComponent();
            lbl_loading.Visibility = Visibility.Hidden;
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            setTimeout();
            var vm = (BGChromaKeyViewmodel)DataContext;

            ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("GreenScreen");
            if(backgroundAttributes.IsImage)
            {
                ImageBrush imgBrush = new ImageBrush();
                var fileName = Globals.LoadBG(TypeFond.ForChromaKeyChoice);
                if (!File.Exists(fileName))
                {
                    fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                }
                imgBrush.ImageSource = new BitmapImage(new Uri(fileName, UriKind.Relative));
                imgBrush.Stretch = Stretch.Fill;
                BGChromaKeyPage.Background = imgBrush;
            }

            if (!backgroundAttributes.EnableTitle)
                viewTitle.Visibility = Visibility.Collapsed;

            int width = (int)SystemParameters.PrimaryScreenWidth;
            listBox1.Width = width;
            if (!backgroundAttributes.IsImage)
            {
                if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                {
                    var bgcolor = backgroundAttributes.BackgroundColor;
                    var bc = new BrushConverter();
                    this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                }

               

                if (backgroundAttributes.EnableTitle)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                        viewTitle.Text = backgroundAttributes.Title;
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                        viewTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                            Canvas.SetLeft((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                            Canvas.SetTop((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                    }
                }
            }
         //   LaunchTimerAfter2();
            LoadingPanel.ClosePanel();
        }

        private void BtnContent(Button btn, string imageName, string contentString, string imagePath)
        {
            if (File.Exists(imagePath))
            {
                var icon = new System.Windows.Controls.Image();
                icon.Name = imageName;
                icon.MaxHeight = 280;
                icon.MaxWidth = 600;
                BitmapImage bmImage = new BitmapImage();
                bmImage.BeginInit();
                bmImage.UriSource = new Uri(imagePath);
                bmImage.EndInit();
                icon.Source = bmImage;
                btn.Content = icon;
                btn.Width = icon.Width;
                btn.Height = icon.Height;
                btn.Background = new SolidColorBrush(Colors.Transparent);
                btn.Foreground = new SolidColorBrush(Colors.Transparent);
                btn.BorderBrush = new SolidColorBrush(Colors.Transparent);
            }

            if (!File.Exists(imagePath))
            {
                btn.Content = contentString;
                btn.Width = 180;
                btn.Height = 30;
            }
        }

        public void setButtonHome()
        {
            setTimeout();
            LaunchTimerAfter2();

            if (File.Exists(Globals._btnVisualisationHome))
            {
                BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._btnVisualisationHome);
            }
            else
            {
                BtnContent((Button)vbtnGoToHome, "ImgBtnHome", "Revenir à l'accueil", Globals._defaultbtnVisualisationHome);
            }
            var btnGoToHomePosition = _inimanager.GetBtnPosition("BUTTONPOSITION", "VisualisationBackToHome");
            if (btnGoToHomePosition[0] != "")
                Canvas.SetLeft((Button)vbtnGoToHome, Convert.ToInt32(btnGoToHomePosition[0]));

            var VisuBtnHomeX = _inimanager.GetSetting("BUTTONPOSITION", "VisuBtnHomeX");
            var VisuBtnHomeY = _inimanager.GetSetting("BUTTONPOSITION", "VisuBtnHomeY");
            VisuBtnHomeX = !string.IsNullOrEmpty(VisuBtnHomeX) ? VisuBtnHomeX : "0";
            VisuBtnHomeY = !string.IsNullOrEmpty(VisuBtnHomeY) ? VisuBtnHomeY : "0";

            TranslateTransform initHomeTransform = new TranslateTransform();
            initHomeTransform.X = (-1) * double.Parse(($"{VisuBtnHomeX}").ToString());
            initHomeTransform.Y = double.Parse(($"{VisuBtnHomeY}").ToString());
            this.StkpHome.RenderTransform = initHomeTransform;
        }

        public void Page_Click(object sender, RoutedEventArgs e)
        {
            StkpHome.Visibility = Visibility.Hidden;
            lbl_CountDown.Content = "";
            if(timerCountdown != null)timerCountdown.Stop();
            if(timerCountdown_minute != null)timerCountdown_minute.Stop();
            setTimeout();
            LaunchTimerAfter2();
        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();

        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    StkpHome.Visibility = Visibility.Hidden;
                    lbl_CountDown.Content = "";
                }
                else
                {
                    lbl_CountDown.Content = timeout.ToString();
                    StkpHome.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    StkpHome.Visibility = Visibility.Hidden;
                    lbl_CountDown.Content = "";
                    if (timerCountdown != null) timerCountdown.Stop();
                    if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void GotoAccueil()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var viewModel = (BGChromaKeyViewmodel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void GotoAccueil_Click(object sender, MouseButtonEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var viewModel = (BGChromaKeyViewmodel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        private void GoToThanks(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var viewModel = (BGChromaKeyViewmodel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                timeout = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                timeout = 10;
            }


            if (timeout < 0)
            {
                timeout *= -1;
            }
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                                LoadingPanel.ShowPanel();
                               // lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                var viewModel = (BGChromaKeyViewmodel)DataContext;
                                /*if (viewModel.GoToConnexionClient.CanExecute(null))
                                    viewModel.GoToConnexionClient.Execute(null);*/
                                Globals._FlagConfig = "client";
                                if (Globals.ScreenType == "SPHERIK")
                                {
                                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                        viewModel.GoToClientSumarySpherik.Execute(null);
                                }
                                else if (Globals.ScreenType == "DEFAULT")
                                {
                                    if (viewModel.GoToClientSumary.CanExecute(null))
                                        viewModel.GoToClientSumary.Execute(null);
                                }
                              //  LoadingPanel.ClosePanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                        break;
                    case Key.F2:
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                if (timerCountdown != null) timerCountdown.Stop();
                                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                Globals._FlagConfig = "admin";
                                var viewModel2 = (BGChromaKeyViewmodel)DataContext;
                                if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                    viewModel2.GoToConnexionConfig.Execute(null);
                               // LoadingPanel.ClosePanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                        break;

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            setButtonHome();
            this.Focusable = true;
            Keyboard.Focus(this);
            //var inimanager = new Managers.INIFileManager($"{Globals.ExeDir()}\\Config.ini");
            var inimanager = _inimanager; 
            var btngototakepicture = inimanager.GetSetting("BUTTONPOSITION", "BGCKGoToTakePhoto");
            int position;
            /*
            if (int.TryParse(btngototakepicture, out position))
                GoToTakePictureButton((Models.Enum.BouttonPosition)Convert.ToInt32(btngototakepicture));
            else
                GoToTakePictureButton(Models.Enum.BouttonPosition.hautGauche); 
*/

            currDir = Globals.EventAssetFolder(); //Directory.GetCurrentDirectory();
            path_ToSave = currDir + $"\\{_imageBGChromakey}";
            var _bgfolders = currDir + $"\\{_imageBGChromakey}";
            _imageFiles = GetImageFileInfo();
            INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
            int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
            var vm = (BGChromaKeyViewmodel)DataContext;
            if (_imageFiles.Count == 0)
            {
                if (timerCountdown != null) timerCountdown.Stop();
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                vm.selectedBG = "";
                Globals.greenScreen = "";
                if (webcam == 1)
                {

                    if (vm.GoToTakeWebcam.CanExecute(null))
                        vm.GoToTakeWebcam.Execute(null);
                }
                else
                {
                    if (vm.GoToTakePhoto.CanExecute(null))
                        vm.GoToTakePhoto.Execute(null);
                }
            }
            else if (_imageFiles.Count == 1)
            {


                if (timerCountdown != null) timerCountdown.Stop();
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                if ((Directory.GetFiles(_bgfolders).ToList().Count) <= 0)
                {
                    vm.selectedBG = "";
                    Globals.greenScreen = "";
                }
                else
                {
                    vm.selectedBG = _imageFiles[0].ToString();
                    Globals.greenScreen = _imageFiles[0].ToString();
                }
                
                
                if (webcam == 1)
                {
                    if (vm.GoToTakeWebcam.CanExecute(null))
                        vm.GoToTakeWebcam.Execute(null);
                }
                else
                {
                    if (vm.GoToTakePhoto.CanExecute(null))
                        vm.GoToTakePhoto.Execute(null);
                }
            }
           if(_imageFiles.Count > 0)
            {
                _photos = new ImageCollection(_bgfolders);

                ((CollectionViewSource)this.Resources["MyPhotos"]).Source = _photos;
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.MoveCurrentToPosition(-1);
                ((CollectionViewSource)this.Resources["MyPhotos"]).Filter += new FilterEventHandler(view_Filter);

                int itemcount = _photos.Count;
                // Calculate the total pages
                totalPage = itemcount / itemPerPage;
                if (itemcount % itemPerPage != 0)
                {
                    totalPage += 1;
                }

                string btnleft = "";
                if (File.Exists(Globals._arrowLeft))
                {
                    btnleft = Globals._arrowLeft;
                }
                else
                {
                    btnleft = Globals._defaultprevious;
                }

                string btnright = "";
                if (File.Exists(Globals._arrowRight))
                {
                    btnright = Globals._arrowRight;
                }
                else
                {
                    btnright = Globals._defaultnext;
                }

                btnpreview.DataContext = btnleft;
                btnNext.DataContext = btnright;

                if (totalPage <= 1)
                {
                    btnpreview.IsEnabled = false;
                    btnNext.IsEnabled = false;
                    btnpreview.Visibility = Visibility.Hidden;
                    stackPrevious.Visibility = Visibility.Hidden;
                    btnNext.Visibility = Visibility.Hidden;
                    stackNext.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnpreview.IsEnabled = false;
                    btnpreview.Visibility = Visibility.Hidden;
                    stackPrevious.Visibility = Visibility.Hidden;
                    btnNext.IsEnabled = true;
                    btnNext.Visibility = Visibility.Visible;
                    stackNext.Visibility = Visibility.Visible;

                }

               
            }

        }

        /*
        private void GoToTakePictureButton(Models.Enum.BouttonPosition type)
        {
            switch (type)
            {
                case Models.Enum.BouttonPosition.hautDroite:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Top);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                case Models.Enum.BouttonPosition.hautCentre:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Top);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    break;
                case Models.Enum.BouttonPosition.hautGauche:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Top);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case Models.Enum.BouttonPosition.basGauche:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Bottom);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case Models.Enum.BouttonPosition.basCentre:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Bottom);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    break;
                case Models.Enum.BouttonPosition.basDroite:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Bottom);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                default:
                    DockPanel.SetDock(this.BGCKButtonPanel, Dock.Top);
                    this.BGCKButtonPanel.Orientation = Orientation.Horizontal;
                    this.BGCKButtonPanel.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
            }          
        }
        */
        private void view_Filter(object sender, FilterEventArgs e)
        {
            int index = _photos.IndexOf((Images)e.Item);

            if (index >= itemPerPage * currentPageIndex && index < itemPerPage * (currentPageIndex + 1))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }

        }

        private void btnPreview_click(object sender, RoutedEventArgs e)
        {
            // Display previous page
            if (currentPageIndex > 0)
            {
                currentPageIndex--;
                btnNext.IsEnabled = true;
                btnNext.Visibility = Visibility.Visible;
                stackNext.Visibility = Visibility.Visible;
                if(currentPageIndex == 0)
                {
                    btnpreview.IsEnabled = false;
                    btnpreview.Visibility = Visibility.Hidden;
                    stackPrevious.Visibility = Visibility.Hidden;
                }
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }

        private void GreenSettings(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            var vm = (BGChromaKeyViewmodel)DataContext;
           
            if (vm.GoToSettingGreen.CanExecute(null))
                vm.GoToSettingGreen.Execute(null);
        }

        private void btnNext_click(object sender, RoutedEventArgs e)
        {
            // Display next page
            if (currentPageIndex < totalPage - 1)
            {
                currentPageIndex++;
                btnpreview.IsEnabled = true;
                btnpreview.Visibility = Visibility.Visible;
                stackPrevious.Visibility = Visibility.Visible;
                if (currentPageIndex == (totalPage - 1))
                {
                    btnNext.IsEnabled = false;
                    btnNext.Visibility = Visibility.Hidden;
                    stackNext.Visibility = Visibility.Hidden;
                }
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }

        private ArrayList GetImageFileInfo()
        {
            var vm = (BGChromaKeyViewmodel)DataContext;
            var imageFiles = new ArrayList();

            //var currDir = Directory.GetCurrentDirectory();
            var currDir = Globals.EventAssetFolder(); //Globals.CurrentDirectory();
            //var temp = currDir + $"\\..\\..\\{_imageFolder}";
            var temp = currDir + $"\\{_imageBGChromakey}";
            var manager = new Manager.ImageFilterManager();

            //if(vm.cmdOrigin != "FromVisualisation")
            //_filtered = manager.Applifilter(Directory.GetFiles(temp, "*.jpeg")[0]);
            if (Directory.Exists(temp))
            {
                var files = Directory.GetFiles(temp, "*.jpeg");

                foreach (var image in files)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
            }
           
            if(imageFiles.Count <= 0 && Directory.Exists(temp))
            {
                var filesjpg = Directory.GetFiles(temp, "*.jpg");

                foreach (var image in filesjpg)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
            }
            if (imageFiles.Count <= 0 && Directory.Exists(temp))
            {
                var filespng = Directory.GetFiles(temp, "*.png");

                foreach (var image in filespng)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
            }

            //imageFiles.Sort();

            return imageFiles;
        }

        private void LbBGSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (timerCountdown != null) timerCountdown.Stop();
                    if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;

                }));
                Thread.Sleep(100);

            }).ContinueWith(task =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    var selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                    string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";

                    var vm = (BGChromaKeyViewmodel)DataContext;
                    vm.selectedBG = selectedImage.Source.ToString();
                    Globals.greenScreen = selectedImage.Source.ToString();
                    if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                    {
                        ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                        comMgr.writeData(selectedImage.Source.ToString());
                    }
                    INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
                    int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
                    if (webcam == 1)
                    {
                        if (vm.GoToTakeWebcam.CanExecute(null))
                            vm.GoToTakeWebcam.Execute(null);
                    }
                    else
                    {
                        if (vm.GoToTakePhoto.CanExecute(null))
                            vm.GoToTakePhoto.Execute(null);
                    }
                    args.Handled = true;
                  //  LoadingPanel.ClosePanel();
                }));
            });
            
        }

        private void SelectedFilter(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var vm = (FiltreManagementViewModel)DataContext;
            //switch (fe.Name)
            //{
            //    case "image1":
            //        vm.selectedFilter = image1.Source.ToString();
            //        break;
            //    case "image2":
            //        vm.selectedFilter = image2.Source.ToString();
            //        break;
            //    case "image3":
            //        vm.selectedFilter = image3.Source.ToString();
            //        break;
            //}
            if (vm.GoToVisualisation.CanExecute(null))
                vm.GoToVisualisation.Execute(null);
        }

        private void ShowImage(object sender, SelectionChangedEventArgs args)
        {
            var list = ((ListBox)sender);
            var index = list?.SelectedIndex; //Save the selected index 
            if (index >= 0)
            {
                var selection = list.SelectedItem.ToString();
                if (!string.IsNullOrEmpty(selection))
                {
                    //Set currentImage to selected Image
                    var selLoc = new Uri(selection);
                    var id = new BitmapImage(selLoc);
                    var currFileInfo = new FileInfo(selection);
                    /*
                    currentImage.Source = id;

                    //Setup Info Text
                    imageSize.Text = id.PixelWidth + " x " + id.PixelHeight;
                    imageFormat.Text = id.Format.ToString();
                    fileSize.Text = ((currFileInfo.Length + 512) / 1024) + "k";
                    */
                }
            }
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            Globals._FlagConfig = "client";
            var viewModel = (BGChromaKeyViewmodel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToConnexionClient.CanExecute(null))
                    viewModel.GoToConnexionClient.Execute(null);
               /* if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }*/
            }
        }

       
    }
}
