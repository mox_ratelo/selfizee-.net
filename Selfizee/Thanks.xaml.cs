﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using System.Windows.Threading;
using Selfizee.Models.Enum;
using Selfizee.Models;
using Selfizee.Managers;
using System.Drawing;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for Thanks.xaml
    /// </summary>
    public partial class Thanks : UserControl
    {
        private Selfizee.MainWindow m_mainWindow;
        int nbSeconde = 0;
        private DispatcherTimer splashAnimationTimer;
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + Globals.GetEventId() + "\\Config.ini");
        private static string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";
        private static string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";

        public Thanks()
        {
            InitializeComponent();

           
                string temp = _ini.GetSetting("GENERAL", "timeoutThanks");
                int nbSeconde_temp = 0;
                if (!string.IsNullOrEmpty(temp))
                {
                    nbSeconde_temp = Convert.ToInt32(temp) * 1000;
                }
            else
            {
                nbSeconde_temp = 5 * 1000;
            }
                if (nbSeconde_temp > 0)
                {
                    nbSeconde = nbSeconde_temp;
                }
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("thanks");
                }
                /*        
                try
                {
                    var temp = Globals.tempDir;
                    var files = Directory.GetFiles(temp, "*.png");
                    if (files.Any())
                        foreach (var file in files)
                        {
                            var rnd = new Random();
                            var newname = $"{Globals.archiveDir}\\{rnd.Next(0, 100)}.png";
                            if (!File.Exists(newname))
                                File.Create(newname).Close();
                            File.Move(file, newname);
                        }
                }catch(Exception ex)
                {

                }
                */

                //m_mainWindow.Closed += new EventHandler(m_mainWindow_Closed);

                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("Thanks");
                if (backgroundAttributes.IsImage)
                {
                    ImageBrush imgBrush = new ImageBrush();
                    var fileName = Globals.LoadBG(TypeFond.ForThanks);
                    if (!File.Exists(fileName))
                    {
                        fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                    }
                    Bitmap bmp = null;
                    using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
                    {
                        bmp = new Bitmap(img, new System.Drawing.Size(1920, 1080));
                     }
                    imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp);
                    imgBrush.Stretch = Stretch.Fill;
                    thanksPage.Background = imgBrush;
                }

                if (!backgroundAttributes.EnableTitle)
                    viewTitle.Visibility = Visibility.Collapsed;

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }



                    if (backgroundAttributes.EnableTitle)
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                            viewTitle.Text = backgroundAttributes.Title;
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                            viewTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                        {
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                                Canvas.SetLeft((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                                Canvas.SetTop((TextBlock)viewTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                        }
                    }
                }

                m_mainWindow = new Selfizee.MainWindow();

                m_mainWindow.ReadyToShow += new MainWindow.ReadyToShowDelegate(m_mainWindow_ReadyToShow);
            
            
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //Window objWindow = sender as Window;
            //objWindow.Close();
            var viewModel = (ThanksViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
            Environment.Exit(0);
            //System.Windows.Application.Current.Shutdown();
        }

        void m_mainWindow_ReadyToShow(object sender, EventArgs args)
        {
            GoToAccueiil();
            #region Animate the splash screen fading
            /*
            Storyboard sb = new Storyboard();
            //
            DoubleAnimation da = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = new Duration(TimeSpan.FromSeconds(20))
            };
            //
            Storyboard.SetTarget(da, this);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Children.Add(da);
            //
            sb.Completed += new EventHandler(sb_Completed);
            //
            sb.Begin();
            */
            #endregion //  splash screen fading With Animation.
        }

        private async void GoToAccueiil()
        {
            await Task.Delay(nbSeconde);
            var viewModel = (ThanksViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
           
        }

        void sb_Completed(object sender, EventArgs e)
        {
            var viewModel = (ThanksViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            Globals._FlagConfig = "client";
            GC.SuppressFinalize(this);
            
            var viewModel = (ThanksViewModel)DataContext;
            if (viewModel != null)
            {
                if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }
            }
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            var viewModel = (ThanksViewModel)DataContext;
            try
            {
                switch (e.Key)
                {
                    case Key.F1:

                        if (Globals.ScreenType == "SPHERIK")
                        {
                            if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                viewModel.GoToClientSumarySpherik.Execute(null);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            if (viewModel.GoToClientSumary.CanExecute(null))
                                viewModel.GoToClientSumary.Execute(null);
                        }
                        break;
                    case Key.F2:
                        
                        if (viewModel.GoToConnexionConfig.CanExecute(null))
                            viewModel.GoToConnexionConfig.Execute(null);
                        break;


                }
            }
            catch (Exception ex)
            {

            }
        }
        private void WindowLoaded(object sender, EventArgs e)
        {

            if (nbSeconde > 0)
            {
                this.Focusable = true;
                Keyboard.Focus(this);
                var savedFolder = Globals.imageSavedFolder;
                var workingDir = Globals._tmpDir;
                var files = Directory.GetFiles(workingDir);
               
            }
            else
            {
                var viewModel = (ThanksViewModel)DataContext;
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
           

        }
      
    }
}
