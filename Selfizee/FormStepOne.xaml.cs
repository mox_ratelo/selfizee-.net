﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Selfizee.Models.Form;
using Selfizee.Managers;
using System.Windows.Controls.Primitives;
using System.IO;
using System.Globalization;
using log4net;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for FormStepOne.xaml
    /// </summary>
    public partial class FormStepOne : UserControl
    {
        List<Fenetre> fenetres = new List<Fenetre>();
        int currentIndex = 0;
        static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        INIFileManager _inimanager = new INIFileManager(EventIni);
        FormManager _formManager = new FormManager();
        Dictionary<string, string> hash = new Dictionary<string, string>();
        Dictionary<string, string> hashTextBox = new Dictionary<string, string>();
        private static readonly ILog Log =
             LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public delegate void EventForTextBox(object sender, KeyboardFocusChangedEventArgs e);

        public FormStepOne()
        {
            InitializeComponent();
            var bc = new BrushConverter();
            var color = string.IsNullOrEmpty(_inimanager.GetSetting("FORM", "BackgroundColor")) ? "#d89f74" : _inimanager.GetSetting("FORM", "BackgroundColor");
            this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(color);
            fenetres = _inimanager.GetFenetre();
            var title = fenetres[0].Title;
            coordonneeTitle.Text = !string.IsNullOrEmpty(title) ? title : "VEUILLEZ SAISIR VOS COORDONNEES \n" + "POUR RECEVOIR VOTRE PHOTO.";
            coordonneeTitle.FontSize = 25;
            coordonneeTitle.FontWeight = FontWeights.Bold;
            var foregroundcolor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "Title")) ? _inimanager.GetSetting("form", "WindowTitleTextColor") : "#000000";
            coordonneeTitle.Foreground = (System.Windows.Media.Brush)bc.ConvertFrom(foregroundcolor);
            //titre.Margin =  new Thickness(20, 50, 20, 30);
            coordonneeTitle.TextAlignment = TextAlignment.Center;
            
        }

        private void FormStepOneOnload(object sender, RoutedEventArgs e)
        {
            var vm = (FormStepOneViewmodel)DataContext;
            vm.buttonBackground = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "ButtonBackground")) ? _inimanager.GetSetting("form", "ButtonBackground") : "#ffffff";
            vm.buttonNavigationBackground = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "ButtonNavigationBackground")) ? _inimanager.GetSetting("form", "ButtonNavigationBackground") : "#262626";
            vm.buttonTextColor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "ButtonTextColor")) ? _inimanager.GetSetting("form", "ButtonTextColor") : "#000000";
            vm.buttonNavigationTextColor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "ButtonNavigationTextColor")) ? _inimanager.GetSetting("form", "ButtonNavigationTextColor") : "#000000";
            vm.textcolor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "QuestionTextColor")) ? _inimanager.GetSetting("form", "QuestionTextColor") : "#000000";
            vm.fenetreTitleTextColor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "WindowTitleTextColor")) ? _inimanager.GetSetting("form", "WindowTitleTextColor") : "#000000";
            vm.fenetreTitle = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "Title")) ? _inimanager.GetSetting("form", "Title") : "VEUILLEZ SAISIR VOS COORDONNEES POUR RECEVOIR VOTRE PHOTO.";
            string formattedText = RemoveDiacritics(vm.fenetreTitle,true);
            coordonneeTitle.Text = formattedText;
            AddCoordonnee(fenetres[0]);
        }

        public static IEnumerable<char> RemoveDiacriticsEnum(string src, bool compatNorm, Func<char, char> customFolding)
        {
            foreach (char c in src.Normalize(compatNorm ? NormalizationForm.FormKD : NormalizationForm.FormD))
                switch (CharUnicodeInfo.GetUnicodeCategory(c))
                {
                    case UnicodeCategory.NonSpacingMark:
                    case UnicodeCategory.SpacingCombiningMark:
                    case UnicodeCategory.EnclosingMark:
                        //do nothing
                        break;
                    default:
                        yield return customFolding(c);
                        break;
                }
        }
        public static IEnumerable<char> RemoveDiacriticsEnum(string src, bool compatNorm)
        {
            return RemoveDiacritics(src, compatNorm, c => c);
        }
        public static string RemoveDiacritics(string src, bool compatNorm, Func<char, char> customFolding)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in RemoveDiacriticsEnum(src, compatNorm, customFolding))
                sb.Append(c);
            return sb.ToString();
        }
        public static string RemoveDiacritics(string src, bool compatNorm)
        {
            return RemoveDiacritics(src, compatNorm, c => c);
        }

        #region Coordonnee
        private void AddCoordonnee(Fenetre fenetre)
        {
            if (fenetre.Questions.Count > 0)
            {
                foreach (var question in fenetre.Questions)
                {
                    StackPanel container = new StackPanel();
                    container.Orientation = Orientation.Vertical;
                    container.HorizontalAlignment = HorizontalAlignment.Center;
                    container.VerticalAlignment = VerticalAlignment.Center;
                    container.Tag = "formcontainer";

                    //var style = (this).FindResource("textboxcoordonnees") as Style;
                    var style = (Style)FindResource("textboxcoordonnees");
                    //Create custom style for textBox

                    var r = this.FindResource("styleRed");
                    foreach (Setter s in style.Setters)
                    {
                        if (s.Property == StackPanel.BackgroundProperty)
                        {
                            s.Value = r;
                        }
                    }
                    TextBox textbox = new TextBox();
                    hash.Add(question.Title, question.Type);
                    textbox.Name = question.Title;
                    textbox.Text = question.Title;
                    hashTextBox.Add(question.Title, question.Title);
                    textbox.LostFocus += textBox_LostFocus;
                    textbox.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
                    textbox.TextChanged += textBox_TextChanged;
                    textbox.Tag = question.Title;
                    textbox.Style = style; 
                    MandatoryQuestionHelper.SetIsMandatory(textbox, question.IsMandatory ?? false);
                    QuestionLabelHelper.SetQuestionLabel(textbox, question.Title);

                    MandatoryControl obligatory = new MandatoryControl();
                    obligatory.title = question.Title;
                    obligatory.isMandatory = false;
                    obligatory.lstControl = new List<Control>();
                    obligatory.lstControl.Add(textbox);
                   
                    Globals.obligatoryField.Add(obligatory);

                    container.Children.Add(textbox);
                    CoordonneeContainer.Children.Add(container);
                    Style btnstyle = this.FindResource("navigationButton") as Style;
                    continueFormOne.Style = btnstyle;
                }
            }
        }

        public string RemoveDiacritics(string text)
        {

            return string.Concat(
        text.Normalize(NormalizationForm.FormD)
         .Where(c => System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c) !=
                     System.Globalization.UnicodeCategory.NonSpacingMark));

        }
        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            //textbox.Text = ""; 
            if (string.IsNullOrEmpty(textbox.Text) || textbox.Text.Trim() == "EMAIL" || textbox.Text.Trim() == "TELEPHONE" || textbox.Text.Trim() == "CODEPOSTAL")
            {
                textbox.Text = "";
            }
                string type = textbox.Tag.ToString();
            string tag = hash[type];
           
            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
                numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Visible;
                numeriqueKeyboard.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Visible;
                alphanumeriquekeyboard.ActiveContainer = textbox;
                alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Collapsed;
            }

        }

        public void AddTextBoxToModal(StackPanel receiver, string title, TextBox item, Style textBlockStyle, Style questionStyle)
        {
            
            TextBox newItem = new TextBox();
            string nameControl = item.Name.ToString() + "_mandatory";
            newItem.Name = nameControl;
            newItem.Tag = nameControl;
            newItem.GotKeyboardFocus += TextBoxNormaltKeyboardFocus;
            var style = (Style)FindResource("textboxcoordonnees");
            newItem.Style = style;
            newItem.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            mandatorykeyboard.ActiveContainer = newItem;
            StackPanel closedQuestionpanel = new StackPanel();
            closedQuestionpanel.Orientation = Orientation.Vertical;
            closedQuestionpanel.VerticalAlignment = VerticalAlignment.Center;
            closedQuestionpanel.HorizontalAlignment = HorizontalAlignment.Center;
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            closedQuestionpanel.Children.Add(textbloc);
            closedQuestionpanel.Children.Add(newItem);
            receiver.Children.Add(closedQuestionpanel);

        }


        private void TextBoxNormaltKeyboardFocus(object sender, EventArgs e)
        {
            TextBox source = sender as TextBox;

            if (source != null)
            {
                //_popup.IsOpen = true;
                mandatorykeyboardcontainer.Visibility = Visibility.Visible;
                mandatorykeyboard.ActiveContainer = source;

            }
        }
        #endregion

        private void GoToQuestions(object sender, RoutedEventArgs e)
        {
            if (mandatoryinnerquestionContainer.Children.Count > 0) mandatoryinnerquestionContainer.Children.Clear();
            bool cangotonext = true;
            List<TextBlock> listToAdd = new List<TextBlock>();
            List<TextBlock> listToRemove = new List<TextBlock>();
            TextBlock mandatoryText = new TextBlock();
            mandatoryText.TextAlignment = TextAlignment.Center;
            mandatoryText.FontWeight = FontWeights.Bold;
            mandatoryText.FontSize = 25;
            mandatoryText.Text = "VEUILLEZ REMPLIR LES CHAMPS OBLIGATOIRES SUIVANTS";
            mandatoryText.Foreground = Brushes.Black;
            mandatoryText.Margin = new Thickness(20, 0, 20, 50);
            mandatoryinnerquestionContainer.Children.Add(mandatoryText);
            if (CoordonneeContainer.Children.Count > 0)
                foreach (var child in CoordonneeContainer.Children)
                {
                    if (child is StackPanel)
                    {
                        var stack = child as StackPanel;
                        
                        
                        foreach (var item in stack.Children)
                        {
                            if (item is TextBox)
                            {
                                
                                bool ismandatory = false;
                                var itm = (TextBox)item;
                                if (string.IsNullOrEmpty(itm.Text) || itm.Text.Trim() == "EMAIL" || itm.Text.Trim() == "TELEPHONE" || itm.Text.Trim() == "CODEPOSTAL")
                                {
                                    ismandatory = MandatoryQuestionHelper.GetIsMandatory(itm);
                                    if (ismandatory)
                                    {
                                        foreach(MandatoryControl control in Globals.obligatoryField)
                                        {
                         


                                            string currentControl = control.lstControl.Where(t => t.Name.Trim() == itm.Name).Select(t => t.Name).SingleOrDefault();
                                            if (!String.IsNullOrEmpty(currentControl))
                                            {
                                                if (!control.errorAddedOnView)
                                                {
                                                    StackPanel _parent = FindParent<StackPanel>(itm);

                                                    TextBlock errobox = new TextBlock();
                                                     listToAdd.Add(errobox);
                                                    Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                                    var style = (Style)FindResource("textboxcoordonnees");
                                                    AddTextBoxToModal(mandatoryinnerquestionContainer, itm.Tag.ToString(), itm, style, questionStyle);
                                                    cangotonext = false;
                                                    control.errorAddedOnView = false;
                                                }
                                            }
                                        }
                                        
                                        break;
                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {
                                        StackPanel _parent = FindParent<StackPanel>(itm);
                                        TextBlock toRemove = new TextBlock();
                                        foreach (var itemError in _parent.Children)
                                        {
                                       
                                            if (itemError is TextBlock)
                                            {
                                            TextBlock tempBlock = (TextBlock)itemError;
                                            if (tempBlock.Tag.ToString() == "error")
                                                {
                                                    listToRemove.Add(tempBlock);
                                                }
                                            }
                                        }
                                }
                                
                                
                            }

                            
                        }
                    }
                }
            foreach(TextBlock blockToRemoev in listToRemove)
            {
                StackPanel _parent = FindParent<StackPanel>(blockToRemoev);
                _parent.Children.Remove(blockToRemoev);
            }
            if (listToAdd.Count > 0)
            {
                TextBlock emptyZone = new TextBlock();
                emptyZone.Height = 10;
                mandatoryinnerquestionContainer.Children.Add(emptyZone);
                StackPanel validationContainer = new StackPanel();
                validationContainer.Orientation = Orientation.Horizontal;
                validationContainer.VerticalAlignment = VerticalAlignment.Center;
                validationContainer.HorizontalAlignment = HorizontalAlignment.Center;
                Button validatemandatory = new Button();
                validatemandatory.Content = "Valider";
                validatemandatory.Click += validateMandatory;
                Style navigationStyle = this.FindResource("navigationButton") as Style;
                validatemandatory.Style = navigationStyle;
                validationContainer.Children.Add(validatemandatory);
                mandatoryinnerquestionContainer.Children.Add(validationContainer);
                questionContainer.Visibility = Visibility.Collapsed;
                stepOneContainer.Visibility = Visibility.Collapsed;
                mandatoryContainer.Visibility = Visibility.Visible;
            }
            if (cangotonext)
            {
                mandatoryContainer.Visibility = Visibility.Collapsed;
                stepOneContainer.Visibility = Visibility.Collapsed;
                GoToNext(sender, e);
            }

        }

        private void textBox_LostFocus(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            string keyName = txtBox.Name;
            if (string.IsNullOrEmpty(txtBox.Text) || txtBox.Text.Trim() == "EMAIL" || txtBox.Text.Trim() == "TELEPHONE" || txtBox.Text.Trim() == "CODEPOSTAL")
            {
                txtBox.Text = hashTextBox[keyName];
            }
        }


        private void textBox_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == txt.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var txtBox = (TextBox)currentControl;
                    //if (comboBox.IsChecked == true)
                    //{
                    control.response = txtBox.Text.ToString();
                    //}
                }
            }
        }

        private void GoToNext(object sender, RoutedEventArgs e)
        {

            var canGoForward = true;

            currentIndex = currentIndex + 1;

            //string csvresult = Globals._formResultPath;
            //if (!File.Exists(csvresult))
            //    File.Create(csvresult);
            if (innerquestionContainer.Children.Count > 0)
            {
                //StreamWriter sw = new StreamWriter(csvresult, true, System.Text.Encoding.UTF8);
                //if (currentIndex == 0)
                //    sw.WriteLine(" ");
                bool blocked = false;
                if (mandatoryinnerquestionContainer.Children.Count > 0) mandatoryinnerquestionContainer.Children.Clear();
                TextBlock mandatoryText = new TextBlock();
                mandatoryText.TextAlignment = TextAlignment.Center;
                mandatoryText.FontWeight = FontWeights.Bold;
                mandatoryText.FontSize = 25;
                mandatoryText.Text = "VEUILLEZ REMPLIR LES CHAMPS OBLIGATOIRES SUIVANTS";
                mandatoryText.Foreground = Brushes.Black;
                mandatoryText.Margin = new Thickness(20, 0, 20, 50);
                mandatoryinnerquestionContainer.Children.Add(mandatoryText);
                foreach (var child in innerquestionContainer.Children)
                {
                    if (child is StackPanel)
                    {
                        if (!CheckMandatoryFieldPanel(child as StackPanel, e))
                        {
                            blocked = true;
                        }
                           
                    }
                }
                if (blocked)
                {
                    TextBlock emptyZone = new TextBlock();
                    emptyZone.Height = 10;
                    mandatoryinnerquestionContainer.Children.Add(emptyZone);
                    StackPanel validationContainer = new StackPanel();
                    validationContainer.Orientation = Orientation.Horizontal;
                    validationContainer.VerticalAlignment = VerticalAlignment.Center;
                    validationContainer.HorizontalAlignment = HorizontalAlignment.Center;
                    Button validatemandatory = new Button();
                    validatemandatory.Margin = new Thickness(0, 0, 3, 0);
                    validatemandatory.Content = "Valider";
                    Style navigationStyle = this.FindResource("navigationButton") as Style;
                    validatemandatory.Style = navigationStyle;
                    validatemandatory.Click += validateMandatory;
                    validationContainer.Children.Add(validatemandatory);
                    mandatoryinnerquestionContainer.Children.Add(validationContainer);
                    questionContainer.Visibility = Visibility.Collapsed;
                    stepOneContainer.Visibility = Visibility.Collapsed;
                    mandatoryContainer.Visibility = Visibility.Visible;
                    canGoForward = false;
                    currentIndex -= 1;
                   
                }
                else
                {
                    canGoForward = true;
                    //currentIndex -= 1;
                }
            }

            if (canGoForward)
            {
                if (innerquestionContainer.Children.Count > 0) innerquestionContainer.Children.Clear();
                if (currentIndex >= fenetres.Count)
                {
                    saveFormResult();
                    if (Globals.printed)
                   {
                        Globals.printed = false;
                        GoToThank();
                    }
                    else
                    {
                        var vm = (FormStepOneViewmodel)DataContext;
                        if (vm.GoToVisualisation.CanExecute(null))
                            vm.GoToVisualisation.Execute(null);
                    }
                  
                    return;
                }

                if (questionContainer.Visibility == Visibility.Collapsed)
                    questionContainer.Visibility = Visibility.Visible;
                AddFormTitre();
                Fenetre currentFenetre = fenetres[currentIndex];
                AddQuestionControl(currentFenetre.Questions);
                AddNavigationButton();

            }


        }

        private void GoToThank()
        {
            // SaveCSV(); 
            var viewModel = (FormStepOneViewmodel)DataContext;
            if (viewModel.GoToThank.CanExecute(null))
                viewModel.GoToThank.Execute(null);
        }

        private string getBasicInfo()
        {
            string dateHeure = DateTime.Now.ToString("dd/mm/yy HH:mm");
            string image = System.IO.Path.GetFileName(Globals.printedImage);
            string eventName = "";
            switch (Globals.EventChosen)
            {
                case 0:
                    eventName = "CADRE";
                    break;
                case 1:
                    eventName = "STRIP";
                    break;
                case 2:
                    eventName = "POLAROID";
                    break;
                case 3:
                    eventName = "MULTISHOOT";
                    break;
            }
            string bgChromaKey = Globals.greenScreen;
            string printed = "NON";
            return dateHeure + ";" + eventName + ";" + image + ";" + bgChromaKey + ";" + printed + ";";
        }

        private void saveFormResult()
        {
            string[] headerTab = new string[] { "Date et heure", "Evénement", "Photo", "Fond vert", "imprimé" };
            string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
            bool newFile = false;
            if (!File.Exists(Result))
            {
                var file = File.Create(Result);
                file.Close();
                newFile = true;
            }
            else
            {
                string contentFile = File.ReadAllText(Result);
                if (string.IsNullOrEmpty(contentFile))
                {
                    newFile = true;
                }
            }
                


                //Create header
                string finalHeader = "";
                for (int i = 0; i < headerTab.Length; i++)
                {
                    finalHeader += headerTab[i] + ";";
                }
                foreach (MandatoryControl control in Globals.obligatoryField)
                {
                    finalHeader += control.title.ToString() + ";";
                
                }

                using (StreamWriter sw = File.AppendText(Result))
                {
                    string toWrite = getBasicInfo();
                    
                    foreach (MandatoryControl control in Globals.obligatoryField)
                    {
                        toWrite += control.response.ToString() + ";";
                    }
                    if (newFile)
                    {
                        sw.WriteLine(finalHeader);
                        Globals.mailsent = true;
                    }
                //BinaryFileManager binMgr = new BinaryFileManager();
                //if (!string.IsNullOrEmpty(Globals.binFileName))
                //{
                //    binMgr.createBinaryFile(Globals.binFileName);
                //    binMgr.writeString(finalHeader + "\r\n" + toWrite);
                //}

                    sw.WriteLine(toWrite);
                    sw.Close();
                    sw.Dispose();
                }
            
        }

        private bool HandleChildOfPanel(StackPanel parent, RoutedEventArgs e)
        {
            var result = true;
            if (parent.Children.Count > 0)
            {
                foreach (var child in parent.Children)
                {
                    if (child is TextBox)
                    {
                        bool ismandatory = false;
                        var item = (TextBox)child;

                        if (string.IsNullOrEmpty(item.Text) || item.Text.Trim() == "EMAIL" || item.Text.Trim() == "TELEPHONE" || item.Text.Trim() == "CODEPOSTAL")
                        {
                            ismandatory = MandatoryQuestionHelper.GetIsMandatory(item);
                            if (ismandatory)
                            {
                                StackPanel _parent = FindParent<StackPanel>(item);
                                _parent.Width = 600; 
                                TextBlock errobox = new TextBlock();
                                errobox.Text = "Champ obligatoire";
                                errobox.Foreground = Brushes.Red;
                                errobox.FontSize = 12;
                                _parent.Children.Add(errobox);
                                return false;

                            }
                        }

                        //string TmpCsv = $"{Globals.EventConfigFolder()}\\Tmp.csv";
                        //using (StreamWriter sw = File.AppendText(TmpCsv))
                        //{
                        //    sw.WriteLine($"{item.Tag.ToString()}={item.Text}");
                        //}
                           
                    }

                    if (child is RadioButton)
                    {
                        var item = (RadioButton)child;
                        if (item.IsChecked.Value)
                        {
                            //string TmpCsv = $"{Globals.EventConfigFolder()}\\Tmp.csv";
                            //using (StreamWriter sw = File.AppendText(TmpCsv))
                            //{
                            //    sw.WriteLine($"{item.Tag.ToString()}={item.Content.ToString()}");
                            //}
                                
                        }
                    }

                    if (child is CheckBox)
                    {
                        var item = (CheckBox)child;
                        //sw.WriteLine(String.Format("{0};{1};{2}", item.Tag.ToString(), item.Content.ToString(), item.IsChecked.Value));
                        if(item.IsChecked??false)
                        {
                            //string TmpCsv = $"{Globals.EventConfigFolder()}\\Tmp.csv";
                            //using (StreamWriter sw = File.AppendText(TmpCsv))
                            //{
                            //    sw.WriteLine($"{item.Tag.ToString()}={item.Content.ToString()}");
                            //}
                        }
                        
                    }

                    if(child is ToggleButton)
                    {
                        var togglebutton = (ToggleButton)child; 
                        if(togglebutton.IsPressed)
                        {
                            //string TmpCsv = $"{Globals.EventConfigFolder()}\\Tmp.csv";
                            //using (StreamWriter sw = File.AppendText(TmpCsv))
                            //{
                            //    sw.WriteLine($"{togglebutton.Tag.ToString()}={togglebutton.Content.ToString()}");
                            //}
                        }
                    }
                    if (child is ComboBox)
                    {
                        var item = (ComboBox)child;
                        var selecteditem = (ComboBoxItem)item.SelectedItem;
                        if (selecteditem != null)
                        {
                            //string TmpCsv = $"{Globals.EventConfigFolder()}\\Tmp.csv";
                            //using (StreamWriter sw = File.AppendText(TmpCsv))
                            //{
                            //    sw.WriteLine($"{item.Tag.ToString()}={selecteditem.Content.ToString()}");
                            //}
                                
                        }
                    }
                }
            }

            return result;
        }

        private bool CheckMandatoryFieldPanel(StackPanel parent, RoutedEventArgs e)
        {
            string title = "";
            foreach (var child in parent.Children)
            {
                if (child is TextBlock)
                {
                    var itemTitle = (TextBlock)child;
                    if (!string.IsNullOrEmpty(itemTitle.Text))
                    {
                        title = itemTitle.Text;
                    }
                    
                }
            }
            var result = true;
            List<StackPanel> lstControlToAdd = new List<StackPanel>();
            List<TextBlock> lstControlToRemove = new List<TextBlock>();
            if (parent.Children.Count > 0)
            {
                foreach (var child in parent.Children)
                {
                    if (child is TextBox)
                    {
                        var item = (TextBox)child;
                        foreach (MandatoryControl control in Globals.obligatoryField)
                        {
                            Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == item.Tag.ToString()).SingleOrDefault();
                            if (currentControl != null)
                            {
                                if (control.isMandatory && String.IsNullOrEmpty(control.response))
                                {
                                    if (!control.errorAddedOnView)
                                    {
                                        Style btnstyle = this.FindResource("whitewhithborder") as Style;
                                        Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                        _formManager.AddQuestionOuvertToModal(parent, mandatoryinnerquestionContainer, item.Tag.ToString(), questionStyle, keyboardContainer, mandatorykeyboard, mandatorykeyboardcontainer);
                                        result = false;
                                        control.errorAddedOnView = true;
                                    }
                                       
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }

                    }

                    if(child is StackPanel)
                    {
                        var innerPanel = (StackPanel)child;
                        foreach(var inner in innerPanel.Children)
                        {
                            if (inner is RadioButton)
                            {
                                var item_ = (RadioButton)inner;
                                foreach (MandatoryControl control in Globals.obligatoryField)
                                {
                                    foreach (Control ctrl in control.lstControl)
                                    {
                                        if (ctrl is RadioButton)
                                        {
                                            RadioButton btnTG = (RadioButton)ctrl;
                                            if (btnTG.Name.ToString() == item_.Name.ToString())
                                            {
                                                if (control.isMandatory && String.IsNullOrEmpty(control.response))
                                                {
                                                    if (!control.errorAddedOnView)
                                                    {
                                                        Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                                        _formManager.AddRadioButtonToModal(parent, mandatoryinnerquestionContainer, title, innerPanel, questionStyle);
                                                        control.errorAddedOnView = true;
                                                        result = false;
                                                    }
                                                }
                                                else
                                                {
                                                    result = true;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                    //Control currentControl = control.lstControl.Where(t => t.Name.ToString() == item.Name.ToString()).SingleOrDefault();
                                    //if(currentControl != null)
                                    //{

                                    //}
                                }


                            }
                        }
                        
                    }

                    

                    if (child is CheckBox)
                    {
                        var item = (CheckBox)child;
                        foreach (MandatoryControl control in Globals.obligatoryField)
                        {

                            foreach (Control ctrl in control.lstControl)
                            {
                                if (ctrl is CheckBox)
                                {
                                    CheckBox btnTG = (CheckBox)ctrl;
                                    if (btnTG.Tag.ToString() == item.Tag.ToString())
                                    {
                                        if (control.isMandatory && String.IsNullOrEmpty(control.response))
                                        {
                                            if (!control.errorAddedOnView)
                                            {

                                                Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                                _formManager.AddCheckBoxToModal(parent, mandatoryinnerquestionContainer, control.title, item, questionStyle);
                                                result = false;
                                                control.errorAddedOnView = true;
                                            }
                                        }
                                        else
                                        {
                                            result = true;
                                        }
                                    }
                                }
                            //    Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == item.Tag.ToString()).SingleOrDefault();
                            //if (currentControl != null)
                            //{
                               

                            }
                        }

                    }

                    if (child is ToggleButton)
                    {
                        var togglebutton = (ToggleButton)child;
                        foreach (MandatoryControl control in Globals.obligatoryField)
                        {
                            //Control currentControl = control.lstControl.Where(t => t.Name.ToString() == togglebutton.Name.ToString()).First();
                            foreach (Control ctrl in control.lstControl){
                                if (ctrl is ToggleButton)
                                {
                                    ToggleButton btnTG = (ToggleButton)ctrl;
                                    if(btnTG.Name.ToString() == togglebutton.Name.ToString())
                                    {
                                        if (control.isMandatory && String.IsNullOrEmpty(control.response))
                                        {
                                            if (!control.errorAddedOnView)
                                            {
                                                Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                                _formManager.AddQuestionFermerToModal(parent, mandatoryinnerquestionContainer, control.title, btnTG, questionStyle);
                                                control.errorAddedOnView = true;
                                                result = false;
                                            }

                                        }
                                        else
                                        {
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (child is ComboBox)
                    {
                        var iteme = (ComboBox)child;
                        foreach (MandatoryControl control in Globals.obligatoryField)
                        {
                            foreach (Control ctrl in control.lstControl)
                            {
                                if (ctrl is ComboBox)
                                {
                                    ComboBox btnTG = (ComboBox)ctrl;
                                    if (btnTG.Tag.ToString() == iteme.Tag.ToString())
                                    {
                                        if (control.isMandatory && String.IsNullOrEmpty(control.response))
                                        {
                                            if (!control.errorAddedOnView)
                                            {
                                                Style questionStyle = this.FindResource("textblockMandatoryStyle") as Style;
                                                _formManager.AddComboBoxToModal(parent, mandatoryinnerquestionContainer, control.title, iteme, questionStyle);
                                                control.errorAddedOnView = true;

                                                result = false;
                                            }
                                        }
                                        else
                                        {
                                            result = true;
                                        }
                                    }
                            }
                            //Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == item.Tag.ToString()).SingleOrDefault();
                            //if (currentControl != null)
                            //{
                                
                            }
                        }
                    }
                }
            }
            //if(lstControlToAdd.Count > 0)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            return result;
        }

        private void GoBack(object sender, EventArgs e)
        {

            currentIndex = currentIndex - 1;
            if (innerquestionContainer.Children.Count > 0) innerquestionContainer.Children.Clear();
            if (currentIndex > 0)
            {
                AddFormTitre();
                Fenetre currentFenetre = fenetres[currentIndex];
                AddQuestionControl(currentFenetre.Questions);
                AddNavigationButton();
            }

            if (currentIndex < 1)
            {
                questionContainer.Visibility = Visibility.Collapsed;
                mandatoryContainer.Visibility = Visibility.Collapsed;
                stepOneContainer.Visibility = Visibility.Visible;
            }

        }

        private void AddQuestionControl(List<Question> questions)
        {
            questions = questions.OrderBy(q => q.Rank).ToList();
            foreach (var question in questions)
            {
                if (question.Type == "QUESTIONFERMER")
                {
                    Style btnstyle = this.FindResource("whitewhithborder") as Style;
                    Style questionStyle = this.FindResource("textblockStyle") as Style;
                    _formManager.AddQuestionFermer(innerquestionContainer, question.Title, btnstyle, questionStyle);
                }

                if (question.Type == "TEXTAREA")
                {
                    Style questionStyle = this.FindResource("textAreablockStyle") as Style;
                    Style keyboardStyle = this.FindResource("DefaultTouchToggleButtonStyle") as Style;
                    _formManager.AddQuestionOuvert(innerquestionContainer, question.Title, questionStyle, keyboardContainer, questionkeyboard, keyboardcontainer, question.IsMandatory);
                }

                if (question.Type == "RADIO")
                {
                    Style questionStyle = this.FindResource("textblockStyle") as Style;
                    _formManager.AddRadioButtonForm(innerquestionContainer, question.Title, question.Items, questionStyle);
                }

                if (question.Type == "CHECKBOX")
                {
                    Style questionStyle = this.FindResource("textblockStyle") as Style;
                    _formManager.AddCheckboxForm(innerquestionContainer, question.Title, question.Items, questionStyle);
                }

                if (question.Type == "COMBOBOX")
                {
                    Style questionStyle = this.FindResource("textblockStyle") as Style;
                    ControlTemplate template = this.FindResource("combo_Template") as ControlTemplate;
                    _formManager.AddComboBoxForm(innerquestionContainer, question.Title, question.Items, questionStyle, template);
                }
            }
        }

        private void AddNavigationButton()
        {
            Style btnstyle = this.FindResource("navigationButton") as Style;
            StackPanel btnpanel = new StackPanel();
            btnpanel.HorizontalAlignment = HorizontalAlignment.Center;
            btnpanel.VerticalAlignment = VerticalAlignment.Center;
            btnpanel.Orientation = Orientation.Horizontal;
            Button btnnon = new Button();
            btnnon.Content = "PRECEDENT";
            btnnon.Style = btnstyle;
            btnnon.Click += GoBack;
            btnpanel.Children.Add(btnnon);

            Button btnoui = new Button();
            btnoui.Content = "SUIVANT";
            btnoui.Style = btnstyle;
            btnoui.Click += GoToNext;
            btnpanel.Children.Add(btnoui);
            innerquestionContainer.Children.Add(btnpanel);
        }

        private void AddFormTitre()
        {
            var title = fenetres[currentIndex].Title;
            TextBlock titre = new TextBlock();
            string titleForm= !string.IsNullOrEmpty(title) ? title : "VEUILLEZ SAISIR VOS COORDONNEES \n" + "POUR RECEVOIR VOTRE PHOTO.";
            byte[] bytes = Encoding.UTF8.GetBytes(titleForm);
            titre.Text = Encoding.Default.GetString(bytes);
            titre.FontSize = 25;
            titre.FontWeight = FontWeights.Bold;
            var bc = new BrushConverter();
            var foregroundcolor = !string.IsNullOrEmpty(_inimanager.GetSetting("form", "WindowTitleTextColor")) ? _inimanager.GetSetting("form", "WindowTitleTextColor") : "#000000";
            titre.Foreground = (System.Windows.Media.Brush)bc.ConvertFrom(foregroundcolor);
            //titre.Margin =  new Thickness(20, 50, 20, 30);
            titre.TextAlignment = TextAlignment.Center;
            innerquestionContainer.Children.Add(titre);
        }

        private void TextBoxGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox source = e.Source as TextBox;

            if (source != null)
            {
                keyboardContainer.IsOpen = true;
                keyboard.ActiveContainer = source;
                source.Background = Brushes.LightBlue;
            }
        }

        private void TextBoxLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox source = e.Source as TextBox;

            if (source != null)
            {
                keyboardContainer.IsOpen = false;
                source.Background = Brushes.White;
            }
        }

        private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //var item = (Grid)e.Source;
            //var item1 = item.Parent; 
            //var popupchild = keyboardContainer.Child;
            //var keys = keyboard.Children; 
            //if(!(e.Source is KeyBoardControl.Keyboard.Keys.OnScreenKey) && currentIndex > -1 && keyboardContainer.IsOpen)
            //{
            //    keyboardContainer.StaysOpen = false;
            //    keyboardContainer.IsOpen = false;
            //    keyboardContainer.StaysOpen = true;
            //}


            if (keyboardcontainer.Visibility == Visibility.Visible && !(e.Source is KeyBoardControl.Keyboard.Keys.OnScreenKey))
                keyboardcontainer.Visibility = Visibility.Collapsed;

            //if(!(e.Source is ToggleButton) && currentIndex > -1)
            //{
            //    keyboardContainer.StaysOpen = false;
            //    keyboardContainer.IsOpen = false;
            //}
        }

        private void choseRecepient(object sender, RoutedEventArgs e)
        {
            var vm = (FormStepOneViewmodel)DataContext;
            var source = (Button)e.Source;
            popuprecipient.IsOpen = true;
            Style textstyle = this.FindResource("questionStyle") as Style;
            popuprecipienttitle.Style = textstyle;
            if (source.Name == "btnEmail")
            {
                popuprecipienttitle.Text = "E-mail"; 
                validaterecipientvalue.Click += SaveUserEmail;
                vm.UserEmail = recipientvalue.Text;
            }

            if (source.Name == "btnPortable")
            {
                popuprecipienttitle.Text = "PORTABLE";
                validaterecipientvalue.Click += SavePortable;
                vm.PhoneNumber = recipientvalue.Text;
            }

            if (source.Name == "btnCodePostal")
            {
                popuprecipienttitle.Text = "CODE POSTAL";
                validaterecipientvalue.Click += SaveCodePostal;
                vm.PostalCode = recipientvalue.Text;
            }
        }

        private void SaveUserEmail(object sender, RoutedEventArgs e)
        {
            var useremail = recipientvalue.Text;
            string csvresult = Globals._formResultPath;
            if (!File.Exists(csvresult))
                File.Create(csvresult);
            StreamWriter sw = new StreamWriter(csvresult, true, System.Text.Encoding.UTF8);
            sw.WriteLine(" ");
            sw.WriteLine(" ");
            sw.WriteLine($"User Email;{useremail}");
            sw.Dispose();
            sw.Close();
            popuprecipient.StaysOpen = false;
            popuprecipient.IsOpen = false;
        }

        private void SavePortable(object sender, RoutedEventArgs e)
        {
            var portable = recipientvalue.Text;
            string csvresult = Globals._formResultPath;
            if (!File.Exists(csvresult))
                File.Create(csvresult);
            StreamWriter sw = new StreamWriter(csvresult, true, System.Text.Encoding.UTF8);
            sw.WriteLine(" ");
            sw.WriteLine(" ");
            sw.WriteLine($"User Portable;{portable}");
            sw.Dispose();
            sw.Close();
            popuprecipient.StaysOpen = false;
            popuprecipient.IsOpen = false;
        }

        private void SaveCodePostal(object sender, RoutedEventArgs e)
        {
            var codepostal = recipientvalue.Text;
            string csvresult = Globals._formResultPath;
            if (!File.Exists(csvresult))
                File.Create(csvresult);
            StreamWriter sw = new StreamWriter(csvresult, true, System.Text.Encoding.UTF8);
            sw.WriteLine(" ");
            sw.WriteLine(" ");
            sw.WriteLine($"User Postal Code;{codepostal}");
            sw.Dispose();
            sw.Close();
            popuprecipient.StaysOpen = false;
            popuprecipient.IsOpen = false;
        }

        private void CancelRecipientValue(object sender, RoutedEventArgs e)
        {
            popuprecipient.IsOpen = false;
        }

        private void CancelMandatory(object sender, RoutedEventArgs e)
        {
            mandatoryContainer.Visibility = Visibility.Collapsed;
        }

        private void validateMandatory(object sender, RoutedEventArgs e)
        {
            List<StackPanel> listToRemove = new List<StackPanel>();
            List<StackPanel> listToAdd = new List<StackPanel>();
            foreach (var parent in mandatoryinnerquestionContainer.Children)
            {
                bool empty = false;

                if(parent is StackPanel)
                {
                    bool knownControl = false;
                    StackPanel innerParent = (StackPanel)parent;
                    foreach (var child in innerParent.Children)
                    {

                        if (child is TextBox)
                        {
                            knownControl = true;
                            TextBox txtBox = (TextBox)child;
                            if (string.IsNullOrEmpty(txtBox.Text))
                            {
                                listToAdd.Add(innerParent);
                                empty = false;
                            }
                            else
                            {
                                empty = true;
                                //listToRemove.Add(innerParent);
                                foreach (MandatoryControl control in Globals.obligatoryField)
                                {
                                    string controlName = txtBox.Tag.ToString().Replace("_mandatory", "");
                                    Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == controlName).SingleOrDefault();
                                    if (currentControl != null)
                                    {
                                        var txteBox = (TextBox)currentControl;
                                        if (!String.IsNullOrEmpty(txtBox.Text))
                                        {
                                            control.response = txtBox.Text.ToString();
                                        }
                                        else
                                        {
                                            control.response = "";
                                        }
                                    }
                                }
                            }
                        }

                        if (child is ComboBox)
                        {
                            knownControl = true;
                            ComboBox cmbBox = (ComboBox)child;
                            string selectedText = "";
                            if (cmbBox.SelectedItem as ComboBoxItem != null)
                            {
                                selectedText = (cmbBox.SelectedItem as ComboBoxItem).Content.ToString();
                            }
                            else
                            {
                                selectedText = "";
                            }
                            if (string.IsNullOrEmpty(selectedText))
                            {
                                empty = false;
                                listToAdd.Add(innerParent);
                            }
                            else
                            {
                                empty = true;
                                foreach (MandatoryControl control in Globals.obligatoryField)
                                {
                                    string controlName = cmbBox.Tag.ToString().Replace("_mandatory", "");
                                    Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == controlName).SingleOrDefault();
                                    if (currentControl != null)
                                    {
                                        var comboBox = (ComboBox)currentControl;
                                        //if (comboBox.IsChecked == true)
                                        //{
                                        control.response = selectedText;
                                        //}
                                    }
                                }
                            }
                        }

                        if (child is RadioButton)
                        {
                            knownControl = true;
                            RadioButton rdButton = (RadioButton)child;
                            if (!empty)
                            {
                                if (rdButton.IsChecked == true)
                                {

                                    empty = true;
                                    foreach (MandatoryControl control in Globals.obligatoryField)
                                    {
                                        string controlName = rdButton.Name.ToString().Replace("_mandatory", "");
                                        Control currentControl = control.lstControl.Where(t => t.Name.ToString() == controlName).SingleOrDefault();
                                        if (currentControl != null)
                                        {
                                            var rdaButton = (RadioButton)currentControl;
                                            if (rdaButton.IsChecked == true)
                                            {
                                                control.response = rdaButton.Content.ToString();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    listToAdd.Add(innerParent);
                                    empty = false;
                                }
                            }
                        }

                        if (child is StackPanel)
                        {
                            StackPanel stackInner = (StackPanel)child;
                            foreach(var inner in stackInner.Children)
                            {

                                //if(inner is StackPanel)
                                //{
                                //    StackPanel innerRadio = (StackPanel)inner;
                                //    foreach(var innRadio in innerRadio.Children)
                                //    {

                                //    }

                                //}

                                if (inner is RadioButton)
                                {
                                    knownControl = true;
                                    RadioButton rdButton = (RadioButton)inner;
                                    if (!empty)
                                    {
                                        if (rdButton.IsChecked == true)
                                        {
                                            //listToRemove.Add(stackInner);
                                            empty = true;
                                            foreach (MandatoryControl control in Globals.obligatoryField)
                                            {
                                                string controlName = rdButton.Name.ToString().Replace("_mandatory", "");
                                                foreach (var currControl in control.lstControl)
                                                {
                                                    if(currControl is RadioButton)
                                                    {
                                                        if (currControl.Name.ToString() == controlName)
                                                        {
                                                            var rdaButton = (RadioButton)rdButton;
                                                            if (rdaButton.IsChecked == true)
                                                            {
                                                                control.response = rdaButton.Content.ToString();
                                                                break;
                                                            }
                                                        }
                                                    }
                                                   
                                                }
                                                //Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == controlName).SingleOrDefault();
                                                //if (currentControl != null)
                                                //{
                                                    
                                                //}
                                            }
                                        }
                                        //else
                                        //{
                                        //    empty = false;
                                        //}
                                    }
                                }

                                if (inner is CheckBox)
                                {
                                    knownControl = true;
                                    CheckBox chkBox = (CheckBox)inner;
                                    if (chkBox.IsChecked == true)
                                    {
                                        //listToRemove.Add(stackInner);
                                        empty = true;
                                        string controlName = chkBox.Tag.ToString().Replace("_mandatory", "");
                                        foreach (MandatoryControl control in Globals.obligatoryField)
                                        {
                                            Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == controlName).SingleOrDefault();
                                            if (currentControl != null)
                                            {
                                                var chekBox = (CheckBox)currentControl;
                                                if (chkBox.IsChecked == true)
                                                {
                                                    control.response += chekBox.Content.ToString() + ",";
                                                }
                                            }
                                        }
                                    }
                                    //else
                                    //{
                                    //    empty = false;
                                    //}
                                }
                                else if (inner is ToggleButton)
                                {
                                    knownControl = true;
                                    ToggleButton tgButon = (ToggleButton)inner;
                                    if (empty == false)
                                    {
                                        if (tgButon.IsChecked == true)
                                        {
                                            //listToRemove.Add(stackInner);
                                            empty = true;
                                            foreach (MandatoryControl control in Globals.obligatoryField)
                                            {
                                                string controlName = tgButon.Tag.ToString().Replace("_mandatory", "");
                                                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == controlName).SingleOrDefault();
                                                if (currentControl != null)
                                                {
                                                    var tgButton = (ToggleButton)currentControl;
                                                    if (tgButton != null)
                                                    {
                                                        control.response = tgButton.Content.ToString();
                                                    }
                                                    else
                                                    {
                                                        control.response = "";
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        //else
                                        //{
                                        //    empty = false;
                                        //}
                                    }

                                }
                                


                            }
                        }
                    }
                    if(empty == false && knownControl)
                    {
                        listToAdd.Add(innerParent);
                    }
                    if (empty == true && knownControl)
                    {
                        listToRemove.Add(innerParent);
                    }

                }
                
                
            }
            int countToAdd = listToAdd.Count;
            int countToRemove = listToRemove.Count;

            foreach (var panel in listToRemove)
            {
                StackPanel stkPanel = (StackPanel)panel;
                foreach(var ctrl in stkPanel.Children)
                {
                    if(ctrl is TextBlock)
                    {
                        TextBlock txtBlock = (TextBlock)ctrl;
                        txtBlock.Visibility = Visibility.Collapsed;
                    }
                    stkPanel.Visibility = Visibility.Collapsed;


                }
                //stkPanel.Visibility = Visibility.Collapsed;
                //mandatoryinnerquestionContainer.Children.Remove(panel);
            }
            listToRemove.Clear();

            //Next step
            if(countToAdd <= 0)
            {
                currentIndex++;
                mandatoryContainer.Visibility = Visibility.Collapsed;
                if (innerquestionContainer.Children.Count > 0) innerquestionContainer.Children.Clear();
                if (currentIndex >= fenetres.Count)
                {
                    saveFormResult();
                    if (Globals.printed)
                   {
                        Globals.printed = false;
                        GoToThank();
                    }
                    else
                    {
                        var vm = (FormStepOneViewmodel)DataContext;
                        if (vm.GoToVisualisation.CanExecute(null))
                            vm.GoToVisualisation.Execute(null);
                    }
                  
                    return;
                }

                if (questionContainer.Visibility == Visibility.Collapsed)
                    questionContainer.Visibility = Visibility.Visible;
                AddFormTitre();
                Fenetre currentFenetre = fenetres[currentIndex];
                AddQuestionControl(currentFenetre.Questions);
                AddNavigationButton();
            }
        }

        
        private T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            T parent = VisualTreeHelper.GetParent(child) as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parent);
        }
    }
}
