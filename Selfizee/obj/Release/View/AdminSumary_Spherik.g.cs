﻿#pragma checksum "..\..\..\View\AdminSumary_Spherik.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C3C358F00C3692F54A9AB8C3715B6AFA86645328"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Selfizee.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Selfizee.View {
    
    
    /// <summary>
    /// AdminSumary_Spherik
    /// </summary>
    public partial class AdminSumary_Spherik : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Selfizee.View.AdminSumary_Spherik EventAdminSumarySpherik;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid dockGrid;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EventName;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label codeEvent;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label dateEventBegin;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label dateEventEnd;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border gotoEvent;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCercle;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Timer;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NbPhoto;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtPhoto;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Img_puce1;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Nbprint;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtImpression;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Img_puce2;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NbCoordonate;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtCoordonnee;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label libProgressBar;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar progressBar1;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border firstGrid;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid firstContainer;
        
        #line default
        #line hidden
        
        
        #line 201 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image photos_Icone;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image icone_pucebtn2;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock last3photos;
        
        #line default
        #line hidden
        
        
        #line 230 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lst3Image;
        
        #line default
        #line hidden
        
        
        #line 280 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblphotolist;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border secondGrid;
        
        #line default
        #line hidden
        
        
        #line 293 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image timelines_Icone;
        
        #line default
        #line hidden
        
        
        #line 309 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image icone_pucebtn1;
        
        #line default
        #line hidden
        
        
        #line 317 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel timelineStack;
        
        #line default
        #line hidden
        
        
        #line 325 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border ThirdGrid;
        
        #line default
        #line hidden
        
        
        #line 333 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image setting_icone;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image icone_pucebtn3;
        
        #line default
        #line hidden
        
        
        #line 352 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border FourthGrid;
        
        #line default
        #line hidden
        
        
        #line 360 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image assist_icone;
        
        #line default
        #line hidden
        
        
        #line 370 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image icone_pucebtn4;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label numVersion;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label numBorne;
        
        #line default
        #line hidden
        
        
        #line 389 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img_logo;
        
        #line default
        #line hidden
        
        
        #line 436 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollCopie;
        
        #line default
        #line hidden
        
        
        #line 454 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtDate;
        
        #line default
        #line hidden
        
        
        #line 457 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelCopie;
        
        #line default
        #line hidden
        
        
        #line 462 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Previous;
        
        #line default
        #line hidden
        
        
        #line 464 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel imageViewer;
        
        #line default
        #line hidden
        
        
        #line 465 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img_View;
        
        #line default
        #line hidden
        
        
        #line 467 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Next;
        
        #line default
        #line hidden
        
        
        #line 471 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button print;
        
        #line default
        #line hidden
        
        
        #line 475 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_CurrentPrinting;
        
        #line default
        #line hidden
        
        
        #line 476 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_loading;
        
        #line default
        #line hidden
        
        
        #line 477 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollCoordonateMandatory;
        
        #line default
        #line hidden
        
        
        #line 483 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stepOneCoordonateMandatoryContainer;
        
        #line default
        #line hidden
        
        
        #line 484 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock coordonneeMandatoryTitle;
        
        #line default
        #line hidden
        
        
        #line 488 "..\..\..\View\AdminSumary_Spherik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button continueFormOneMandatory;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Selfizee;component/view/adminsumary_spherik.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\AdminSumary_Spherik.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.EventAdminSumarySpherik = ((Selfizee.View.AdminSumary_Spherik)(target));
            return;
            case 2:
            
            #line 63 "..\..\..\View\AdminSumary_Spherik.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Page_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dockGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.EventName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.codeEvent = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.dateEventBegin = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.dateEventEnd = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.gotoEvent = ((System.Windows.Controls.Border)(target));
            
            #line 94 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.gotoEvent.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.GoToListEvent_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 97 "..\..\..\View\AdminSumary_Spherik.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.GotoAccueil_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.imgCercle = ((System.Windows.Controls.Image)(target));
            return;
            case 11:
            this.Timer = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.NbPhoto = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.txtPhoto = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.Img_puce1 = ((System.Windows.Controls.Image)(target));
            return;
            case 15:
            this.Nbprint = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.txtImpression = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.Img_puce2 = ((System.Windows.Controls.Image)(target));
            return;
            case 18:
            this.NbCoordonate = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.txtCoordonnee = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.libProgressBar = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.progressBar1 = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 22:
            this.firstGrid = ((System.Windows.Controls.Border)(target));
            return;
            case 23:
            this.firstContainer = ((System.Windows.Controls.Grid)(target));
            return;
            case 24:
            this.photos_Icone = ((System.Windows.Controls.Image)(target));
            return;
            case 25:
            
            #line 214 "..\..\..\View\AdminSumary_Spherik.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.ShowPhotos_Click);
            
            #line default
            #line hidden
            return;
            case 26:
            this.icone_pucebtn2 = ((System.Windows.Controls.Image)(target));
            return;
            case 27:
            this.last3photos = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.lst3Image = ((System.Windows.Controls.ListBox)(target));
            
            #line 230 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.lst3Image.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.LbSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.lblphotolist = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.secondGrid = ((System.Windows.Controls.Border)(target));
            return;
            case 31:
            this.timelines_Icone = ((System.Windows.Controls.Image)(target));
            return;
            case 32:
            
            #line 308 "..\..\..\View\AdminSumary_Spherik.xaml"
            ((System.Windows.Controls.TextBlock)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.timelines_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.icone_pucebtn1 = ((System.Windows.Controls.Image)(target));
            return;
            case 34:
            this.timelineStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 35:
            this.ThirdGrid = ((System.Windows.Controls.Border)(target));
            return;
            case 36:
            this.setting_icone = ((System.Windows.Controls.Image)(target));
            return;
            case 37:
            
            #line 343 "..\..\..\View\AdminSumary_Spherik.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Config_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.icone_pucebtn3 = ((System.Windows.Controls.Image)(target));
            return;
            case 39:
            this.FourthGrid = ((System.Windows.Controls.Border)(target));
            return;
            case 40:
            this.assist_icone = ((System.Windows.Controls.Image)(target));
            return;
            case 41:
            this.icone_pucebtn4 = ((System.Windows.Controls.Image)(target));
            return;
            case 42:
            this.numVersion = ((System.Windows.Controls.Label)(target));
            return;
            case 43:
            this.numBorne = ((System.Windows.Controls.Label)(target));
            return;
            case 44:
            this.img_logo = ((System.Windows.Controls.Image)(target));
            return;
            case 45:
            this.scrollCopie = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 46:
            this.txtDate = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 47:
            this.cancelCopie = ((System.Windows.Controls.Button)(target));
            
            #line 457 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.cancelCopie.Click += new System.Windows.RoutedEventHandler(this.copieCancel);
            
            #line default
            #line hidden
            return;
            case 48:
            this.Previous = ((System.Windows.Controls.Button)(target));
            
            #line 462 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.Previous.Click += new System.Windows.RoutedEventHandler(this.Previous_Click);
            
            #line default
            #line hidden
            return;
            case 49:
            this.imageViewer = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 50:
            this.img_View = ((System.Windows.Controls.Image)(target));
            return;
            case 51:
            this.Next = ((System.Windows.Controls.Button)(target));
            
            #line 467 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.Next.Click += new System.Windows.RoutedEventHandler(this.Next_Click);
            
            #line default
            #line hidden
            return;
            case 52:
            this.print = ((System.Windows.Controls.Button)(target));
            
            #line 471 "..\..\..\View\AdminSumary_Spherik.xaml"
            this.print.Click += new System.Windows.RoutedEventHandler(this.Imprimer);
            
            #line default
            #line hidden
            return;
            case 53:
            this.lbl_CurrentPrinting = ((System.Windows.Controls.Label)(target));
            return;
            case 54:
            this.lbl_loading = ((System.Windows.Controls.Label)(target));
            return;
            case 55:
            this.scrollCoordonateMandatory = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 56:
            this.stepOneCoordonateMandatoryContainer = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 57:
            this.coordonneeMandatoryTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 58:
            this.continueFormOneMandatory = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

