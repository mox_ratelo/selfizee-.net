﻿#pragma checksum "..\..\..\View\WifiList.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C64042B9AD6C59C1341DD57D6BA53E5DD9FA96A4"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using Selfizee.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Selfizee.View {
    
    
    /// <summary>
    /// WifiList
    /// </summary>
    public partial class WifiList : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Selfizee.View.WifiList ListOfWifi;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border borderTB;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image icoHome;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTB;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border borderRetour;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCercle;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Timer;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackTitle;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Title;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnActualiser;
        
        #line default
        #line hidden
        
        
        #line 281 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel wflist;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\..\View\WifiList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblInfo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Selfizee;component/view/wifilist.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\WifiList.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ListOfWifi = ((Selfizee.View.WifiList)(target));
            return;
            case 2:
            
            #line 98 "..\..\..\View\WifiList.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Page_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.borderTB = ((System.Windows.Controls.Border)(target));
            
            #line 149 "..\..\..\View\WifiList.xaml"
            this.borderTB.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Sumary_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.icoHome = ((System.Windows.Controls.Image)(target));
            return;
            case 5:
            this.btnTB = ((System.Windows.Controls.Button)(target));
            
            #line 159 "..\..\..\View\WifiList.xaml"
            this.btnTB.Click += new System.Windows.RoutedEventHandler(this.Sumary_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.borderRetour = ((System.Windows.Controls.Border)(target));
            
            #line 162 "..\..\..\View\WifiList.xaml"
            this.borderRetour.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.GotoAccueil_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.imgCercle = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.Timer = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.stackTitle = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.Title = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.btnActualiser = ((System.Windows.Controls.Button)(target));
            
            #line 186 "..\..\..\View\WifiList.xaml"
            this.btnActualiser.Click += new System.Windows.RoutedEventHandler(this.OnUcLoaded);
            
            #line default
            #line hidden
            return;
            case 12:
            this.wflist = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 13:
            this.lblInfo = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

