﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class InstantGagnant
    {
        public DateTime startDate { get; set; }
        public string gift { get; set; }
        public bool win { get; set; }
        public string originalString { get; set; }
        public int index { get; set; }
        public int TypeCadeau { get; set; }
    }
}
