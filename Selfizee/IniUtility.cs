﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee
{
    public class IniUtility
    {
        string Path;
        string EXE = Assembly.GetExecutingAssembly().GetName().Name;

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public IniUtility(string IniPath = null)
        {
            Path = new FileInfo(IniPath ?? EXE + ".ini").FullName.ToString();
        }

        public string Read(string Key, string Section = null)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section ?? EXE, Key, "", RetVal, 255, Path);
            string retour = RetVal.ToString();
            return retour;
        }

        public void Write(string Key, string Value, string Section = null)
        {
            WritePrivateProfileString(Section ?? EXE, Key, Value, Path);
        }

        public void DeleteKey(string Key, string Section = null)
        {
            Write(Key, null, Section ?? EXE);
        }

        public void DeleteSection(string Section = null)
        {
            Write(null, null, Section ?? EXE);
        }

        public bool KeyExists(string Key, string Section = null)
        {
            return Read(Key, Section).Length > 0;
        }

        public string[] GetSections(string strSource)
        {
            int intLastPos = 0;
            int intEndSectPos;
            List<string> lstSections = new List<string>();
            //Ok, the idea is to keep looking for indexes of section beginnings,

            intLastPos = strSource.IndexOf("[", intLastPos, StringComparison.InvariantCulture);
            while (intLastPos != -1)
            {
                intEndSectPos = strSource.IndexOf("]", intLastPos, StringComparison.InvariantCulture);
                lstSections.Add(strSource.Substring(intLastPos + 1, (intEndSectPos - 1) - (intLastPos)).ToLower());
                intLastPos += 1;

                intLastPos = strSource.IndexOf("[", intLastPos, StringComparison.InvariantCulture);
            }
            string[] eventArray = lstSections.ToArray();
            int index = 0;
            var lstEvent = eventArray.ToList();
            var lstDest = new List<string>();
            foreach (string currentEvent in lstEvent)
            {
                index++;
                if (currentEvent == "cadre" || currentEvent == "strip" || currentEvent == "polaroid" || currentEvent == "multishoot")
                {
                    lstDest.Add(currentEvent);
                }
            }

            return lstDest.ToArray();

        }
     }
}
