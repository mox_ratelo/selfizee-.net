﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class RegistryManager
    {
        public void WriteRegistryValue(string value)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);

            key.CreateSubKey("Selfizee");
            key = key.OpenSubKey("Selfizee", true);


            key.CreateSubKey("Version");
            key = key.OpenSubKey("Version", true);

            key.SetValue("ID", value);
        }

        public string ReadRegistryValue()
        {
            string toReturn = "";
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);
            key = key.OpenSubKey("Selfizee", true);
            key = key.OpenSubKey("Version", true);
            if (key != null)
            {
                toReturn = key.GetValue("ID").ToString();
                key.Close();
            }
            return toReturn;
        }

        public string ReadRegistryBisValue()
        {
            string toReturn = "";
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);
            key = key.OpenSubKey("Selfizee", true);
            key = key.OpenSubKey("Version", true);
            if (key != null)
            {
                toReturn = key.GetValue("ID").ToString();
                key.Close();
            }
            return toReturn;
        }
    }
}
