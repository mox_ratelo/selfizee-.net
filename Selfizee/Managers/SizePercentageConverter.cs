﻿using System;
using System.Windows.Data;

namespace Selfizee.Managers
{
    public class SizePercentageConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null)
                return 1.0 * (double)value;
            string[] split = parameter.ToString().Split('.');
            double parameterDouble = System.Convert.ToDouble(split[0]) + System.Convert.ToDouble(split[1]) / Math.Pow(10, split[1].Length);
            return System.Convert.ToDouble(value) * parameterDouble; 
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null; 
        }
    }
}
