﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Selfizee
{
    public class XmlManager
    {
        public DateTime? GetLastModification(string path)
        {
            DateTime? toReturn = null;
            try
            {
                if (File.Exists(path))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(path);
                    foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)
                    {
                        string str_date = node.Attributes["datetime"].Value;
                        toReturn = Convert.ToDateTime(str_date);
                        break;
                    }
               }
                return toReturn;
            }
            catch(Exception ex)
            {

            }
            return null;
        }

        public List<Formulaire> loadDataForm(string path)
        {
            int increment = 0;
            try
            {
                if (File.Exists(path))
                {
                    List<Formulaire> lstForm = new List<Formulaire>();
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(path);
                    foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)
                    {
                        Formulaire currentForm = new Formulaire();
                        string type_form = "";
                        bool activated = false;
                        List<Page> lstPage = new List<Page>();
                        type_form = node.Attributes["type"].Value;
                        if (type_form.ToLower() == "empty")
                        {
                            lstForm = null;
                        }
                        else
                        {
                            currentForm.type = type_form;
                            currentForm.activated = activated;
                            foreach (XmlNode formulaire in node)
                            {

                                foreach (XmlNode page in formulaire.ChildNodes)
                                {
                                    increment++;
                                    Page currentPage = new Page();
                                    currentPage.lstElements = new List<element>();

                                    foreach (XmlNode page_value in page.ChildNodes)
                                    {

                                        List<element> lstElements = new List<element>();
                                        if (page_value.Name == "elements")
                                        {
                                            element currentElement = new element();
                                            Textbox_Page lstTextBox = new Textbox_Page();
                                            Combobox_page lstCombo = new Combobox_page();
                                            Radiobutton_Page lstRadio = new Radiobutton_Page();
                                            List<Optin> lstOptin = new List<Optin>();
                                            foreach (XmlNode element in page_value.ChildNodes)
                                            {
                                                if (element.Name == "Head")
                                                {
                                                    XmlNode titreNode = element.FirstChild;
                                                    string titre = titreNode.InnerText;
                                                    Title headTitle = new Title();
                                                    headTitle._Title = titre.ToLower();
                                                    currentPage.title = headTitle;
                                                }
                                                else
                                                {
                                                    foreach (XmlNode element_field in element)
                                                    {
                                                        if (element_field.Name == "textbox")
                                                        {
                                                            if(increment == 0)
                                                            {
                                                                Globals._DataFirstPage = true;
                                                            }
                                                            Globals.nbField++;
                                                            lstElements = new List<Selfizee.element>();
                                                            currentElement = new Selfizee.element();
                                                            Textbox_Page currentTextBox = new Textbox_Page();
                                                            foreach (XmlNode textboxField in element_field)
                                                            {

                                                                string type = "";
                                                                string text = "";
                                                                string name = "";
                                                                bool mendatory = false;
                                                                if (textboxField.Name.ToString().ToLower() == "type")
                                                                {
                                                                    type = textboxField.InnerText;
                                                                    currentTextBox.type = type;
                                                                }
                                                                else if (textboxField.Name.ToString().ToLower() == "text")
                                                                {
                                                                    text = textboxField.InnerText.ToLower();
                                                                    currentTextBox.text = text;
                                                                }
                                                                else if (textboxField.Name.ToString().ToLower() == "id" )
                                                                {
                                                                    name = textboxField.InnerText;
                                                                    currentTextBox.Name = "field_" + name;
                                                                }
                                                                else if (textboxField.Name.ToString().ToLower() == "mandatory")
                                                                {
                                                                    mendatory = bool.Parse(textboxField.InnerText);
                                                                    currentTextBox.mendatory = mendatory;
                                                                }
                                                            }
                                                            lstTextBox = currentTextBox;
                                                            currentElement.lst_textBox = lstTextBox;
                                                            currentPage.lstElements.Add(currentElement);
                                                        }
                                                        else if (element_field.Name == "combobox")
                                                        {
                                                            Globals.nbField++;
                                                            lstElements = new List<Selfizee.element>();
                                                            currentElement = new Selfizee.element();
                                                            Combobox_page currentCombo = new Combobox_page();
                                                            foreach (XmlNode comboboxField in element_field)
                                                            {
                                                                string title = "";
                                                                string name = "";
                                                                bool mendatory = false;
                                                                List<string> lstComboboxItems = new List<string>();
                                                                if (comboboxField.Name.ToString().ToLower() == "title")
                                                                {
                                                                    title = comboboxField.InnerText.ToLower();
                                                                    currentCombo.Title = title;
                                                                }
                                                                else if (comboboxField.Name.ToString().ToLower() == "text")
                                                                {
                                                                    title = comboboxField.InnerText.ToLower();
                                                                    currentCombo.Title = title;
                                                                }
                                                                else if (comboboxField.Name.ToString().ToLower() == "id")
                                                                {
                                                                    name = comboboxField.InnerText;
                                                                    currentCombo.Name = "field_" + name;
                                                                }
                                                                else if (comboboxField.Name.ToString().ToLower() == "comboboxitems")
                                                                {
                                                                    foreach (XmlNode itemsCombo in comboboxField)
                                                                    {
                                                                        lstComboboxItems.Add(itemsCombo.InnerText.ToLower());
                                                                    }
                                                                    currentCombo.Combobox_items = lstComboboxItems;
                                                                }
                                                                else if (comboboxField.Name.ToString().ToLower() == "mandatory")
                                                                {
                                                                    mendatory = bool.Parse(comboboxField.InnerText);
                                                                    currentCombo.mendatory = mendatory;
                                                                }
                                                            }
                                                            lstCombo = currentCombo;
                                                            currentElement.lst_comboBoxItem = lstCombo;
                                                            currentPage.lstElements.Add(currentElement);
                                                        }
                                                        else if (element_field.Name.ToString().ToLower() == "radiobutton")
                                                        {
                                                            Globals.nbField++;
                                                            currentElement = new Selfizee.element();
                                                            lstElements = new List<Selfizee.element>();
                                                            Radiobutton_Page currentRadio = new Radiobutton_Page();
                                                            foreach (XmlNode radiobuttonField in element_field)
                                                            {
                                                                string title = "";
                                                                string name = "";
                                                                bool mendatory = false;
                                                                List<RadiobuttonItem> lstradioButtonsItems = new List<RadiobuttonItem>();
                                                                if (radiobuttonField.Name.ToString().ToLower() == "title")
                                                                {
                                                                    title = radiobuttonField.InnerText.ToLower();
                                                                    currentRadio.Title = title;
                                                                }
                                                                else if (radiobuttonField.Name.ToString().ToLower() == "id")
                                                                {
                                                                    name = radiobuttonField.InnerText;
                                                                    currentRadio.Name = "field_" + name;
                                                                }
                                                                else if (radiobuttonField.Name.ToString().ToLower() == "items")
                                                                {
                                                                    foreach (XmlNode itemsRadio in radiobuttonField)
                                                                    {
                                                                        string value = itemsRadio.Attributes["Value"].Value;
                                                                        RadiobuttonItem temp = new RadiobuttonItem();
                                                                        temp.item = itemsRadio.InnerText.ToLower();
                                                                        temp.value = value;
                                                                        lstradioButtonsItems.Add(temp);
                                                                    }
                                                                    currentRadio.Radiobuttons_items = lstradioButtonsItems;
                                                                }
                                                                else if (radiobuttonField.Name.ToString().ToLower() == "mandatory")
                                                                {
                                                                    mendatory = bool.Parse(radiobuttonField.InnerText);
                                                                    currentRadio.mendatory = mendatory;
                                                                }
                                                            }
                                                            lstRadio = currentRadio;
                                                            currentElement.lst_radioButton = lstRadio;
                                                            currentPage.lstElements.Add(currentElement);
                                                        }
                                                        else if (element_field.Name.ToString().ToLower() == "optin")
                                                        {
                                                            Globals.nbField++;
                                                            Optin currentOptin = new Optin();
                                                            lstElements = new List<Selfizee.element>();
                                                            //foreach (XmlNode optin in element_field)
                                                            //{
                                                            currentElement = new Selfizee.element();
                                                            foreach (XmlNode optinField in element_field)
                                                            {
                                                                string Type = "";
                                                                string id = "";
                                                                string Title = "";
                                                                string name = "";
                                                                bool mendatory = false;
                                                                if (optinField.Name.ToString().ToLower() == "type")
                                                                {
                                                                    Type = optinField.InnerText;
                                                                    currentOptin.Type = Type;
                                                                }
                                                                else if (optinField.Name.ToString().ToLower() == "id")
                                                                {
                                                                    name = optinField.InnerText;
                                                                    currentOptin.identifiant = "field_" + name;
                                                                    currentOptin.Name = "field_" + name;
                                                                }
                                                                else if (optinField.Name.ToString().ToLower() == "title")
                                                                {
                                                                    Title = optinField.InnerText.ToLower();
                                                                    currentOptin.Label = Title;
                                                                }
                                                                else if (optinField.Name.ToString().ToLower() == "mandatory")
                                                                {
                                                                    mendatory = bool.Parse(optinField.InnerText);
                                                                    currentOptin.mendatory = mendatory;
                                                                }
                                                            }
                                                            currentElement.lst_Optin = currentOptin;
                                                            currentPage.lstElements.Add(currentElement);
                                                            //}
                                                        }

                                                    }


                                                }

                                            }

                                        }

                                    }
                                    lstPage.Add(currentPage);
                                }

                            }
                            currentForm.lst_Page = lstPage;
                            lstForm.Add(currentForm);
                        }
                        
                    }
                    return lstForm;
                }
                
            }
            catch(XmlException e)
            {
                return null;
            }
            return null;
        }
            
    }
}
