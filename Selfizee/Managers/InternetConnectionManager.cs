﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public static class InternetConnectionManager
    {
        public static bool checkConnection(string url)
        {
            //Ping myPing = new Ping();
            //String host = url;
            //byte[] buffer = new byte[32];
            //int timeout = 1000;
            //PingOptions pingOptions = new PingOptions();
            //PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
            //return (reply.Status == IPStatus.Success);

            try
            {
                Ping myPing = new Ping();
                String host = url;
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }

                //using (var client = new WebClient())
                //using (var stream = client.OpenRead("http://www.google.com"))
                //{
                //    return true;
                //}
            }
            catch
            {
                return false;
            }
            return false;
        }

    }
}
