﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class BinaryFileManager
    {
        private string filename;
        public void createBinaryFile(string path)
        {
            filename = path;
            if (!File.Exists(filename))
            {
                BinaryWriter bwStream = new BinaryWriter(new FileStream(path, FileMode.Create));
                bwStream.Dispose();
                Encoding ascii = Encoding.ASCII;
                BinaryWriter bwEncoder = new BinaryWriter(new FileStream(path, FileMode.Create), ascii);
                bwEncoder.Dispose();
            }
            
        }

        public void writeData(List<Object> arrayToSave)
        {
            var _eventId = Globals.GetEventId();
            if (!String.IsNullOrEmpty(_eventId))
            {
                //string filename = Globals.EventConfigFolder() + "\\Media\\Photos\\" + _eventId + ".SELFIZEE";
                if (!File.Exists(filename))
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Create(filename))
                    {
                        serializer.Serialize(stream, arrayToSave);
                    }
                }
                else
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(filename, FileMode.Open))
                    {
                        serializer.Serialize(stream, arrayToSave);
                    }
                }
            }
            
        }

        public void writeSerialData(string toSave, string path)
        {
                if (!File.Exists(path))
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Create(path))
                    {
                        serializer.Serialize(stream, toSave);
                    }
                }
                else
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(path, FileMode.Open))
                    {
                        serializer.Serialize(stream, toSave);
                    }
                }

        }

        public void writeData(int intToSave,string fileName)
        {
            var _eventId = Globals.GetEventId();
            if (!String.IsNullOrEmpty(_eventId))
            {
                //string filename = Globals.EventConfigFolder() + "\\Media\\Photos\\" + _eventId + ".SELFIZEE";
                if (!File.Exists(fileName))
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Create(fileName))
                    {
                        serializer.Serialize(stream, intToSave);
                    }
                }
                else
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(fileName, FileMode.Open))
                    {
                        serializer.Serialize(stream, intToSave);
                    }
                }
            }

        }

        public PrinterSettings readPrinterSettings(string binpath)
        {
            PrinterSettings pSettings = new PrinterSettings();
            if (File.Exists(binpath))
            {
               
                var serializer = new BinaryFormatter();
                using (var stream = File.Open(binpath, FileMode.Open))
                {
                    pSettings = (PrinterSettings)serializer.Deserialize(stream);
                }
            }
            return pSettings;
        }

        public List<Object> readData()
        {
            var _eventId = Globals.GetEventId();
            List<Object> arrayToLoad = null;
            if (!String.IsNullOrEmpty(_eventId))
            {
                //string filename = Globals.EventConfigFolder() + "\\Media\\Photos\\" + _eventId + ".SELFIZEE";
                if (File.Exists(filename))
                {
                    arrayToLoad = new List<object>();
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(filename, FileMode.Open))
                    {
                        arrayToLoad = (List<Object>)serializer.Deserialize(stream);
                    }
                }
            }

            return arrayToLoad;
            
        }

        public string readSerialData(string pathName)
        {
           
            string toReturn = null;
            
                //string filename = Globals.EventConfigFolder() + "\\Media\\Photos\\" + _eventId + ".SELFIZEE";
                if (File.Exists(pathName))
                {
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(filename, FileMode.Open))
                    {
                    toReturn = (string)serializer.Deserialize(stream);
                    }
                }

            return toReturn;

        }

        public int readIntData(string fileName)
        {
            int intToLoad = 0;
            var _eventId = Globals.GetEventId();
            if (!String.IsNullOrEmpty(_eventId))
            {
                //string filename = Globals.EventConfigFolder() + "\\Media\\Photos\\" + _eventId + ".SELFIZEE";
                if (File.Exists(fileName))
                {
                    
                    var serializer = new BinaryFormatter();
                    using (var stream = File.Open(fileName, FileMode.Open))
                    {
                        intToLoad = (int)serializer.Deserialize(stream);
                    }
                }
            }

            return intToLoad;

        }

        public void writeSerialString(string content, string path)
        {
            writeSerialData(content, path);
        }

        public void writeString(string csvContent)
        {
            
            List<Object> arrayToLoad = new List<object>();
            arrayToLoad = readData();
            int i = 0;
            foreach(object str in arrayToLoad)
            {
                if (str.GetType() == typeof(String))
                {
                    arrayToLoad[i] = csvContent;
                }
                i++;
            }
            arrayToLoad.Add(csvContent);
            writeData(arrayToLoad);
        }
    }
}
