﻿using Selfizee.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class FtpManager
    {

        //private string ftpVersionPath = "ftp://37.187.132.132/Devnet/Version.txt";
        //private string ftpFileNamePath = "ftp://37.187.132.132/Devnet/Selfizee_setup.exe";
        //private string userName = "release";
        //private string password = "c7j!RBQ{NG68}kg5n)?t5HBL";

        public string ftpVersionPath = "ftp://37.187.132.132/Devnet/Version.txt";
        public string ftpFileNamePath = "ftp://37.187.132.132/Devnet/Selfizee_setup.exe";
        public string userName = "release";
        public string password = "c7j!RBQ{NG68}kg5n)?t5HBL";

        public string GetFtpFileVersion()
        {
            WebClient request = new WebClient();
            string fileString = "";
            request.Credentials = new NetworkCredential(userName, password);
            try
            {
                byte[] newFileData = request.DownloadData(ftpVersionPath);
                fileString = System.Text.Encoding.UTF8.GetString(newFileData);
            }
            catch (WebException eg)
            {
                // Do something such as log error, but this is based on OP's original code
                // so for now we do nothing.
            }
            return fileString;
            
        }

        public List<DirectoryItem> GetDirectoryInformation(string address, string username, string password)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(address);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Timeout = 6000;
            request.ReadWriteTimeout = 6000;
            request.Credentials = new NetworkCredential(username, password);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            foreach (string line in list)
            {
                // Windows FTP Server Response Format
                // DateCreated    IsDirectory    Name
                string data = line;

                // Parse date
                

                // Parse <DIR>
                string dir = data.Substring(0, 5);
                bool isDirectory = dir.Contains("drwxr")? true : false;

                // Parse name
                string name = line.Split(' ').Last();

                // Create directory info
                DirectoryItem item = new DirectoryItem();
                item.BaseUri = new Uri(address);
                item.IsDirectory = isDirectory;
                item.Name = name;

                returnValue.Add(item);
            }

            return returnValue;
        }

        public long GetFileSize()
        {
            NetworkCredential networkCredential = new NetworkCredential(userName, password);
            var ftpWebRequest = GetFtpWebRequest(new Uri(ftpFileNamePath), networkCredential, WebRequestMethods.Ftp.GetFileSize);
            try { return ((FtpWebResponse)ftpWebRequest.GetResponse()).ContentLength; } //Incase of success it'll return the File Size.
            catch (Exception) { return default(long); }
        }

        public FtpWebRequest GetFtpWebRequest(Uri requestUri, NetworkCredential networkCredential, string method = null)
        {
            var ftpWebRequest = (FtpWebRequest)WebRequest.Create(requestUri); //Create FtpWebRequest with given Request Uri.
            ftpWebRequest.Credentials = networkCredential; //Set the Credentials of current FtpWebRequest.

            if (!string.IsNullOrEmpty(method))
                ftpWebRequest.Method = method; //Set the Method of FtpWebRequest incase it has a value.
            return ftpWebRequest; //Return the configured FtpWebRequest.
        }

        //public void DownloadFile()
        //{
        //    try
        //    {
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpFileNamePath);
        //        request.Credentials = new NetworkCredential(userName, password);
        //        request.Method = WebRequestMethods.Ftp.DownloadFile;
        //        if (!Directory.Exists(Globals._defaultUpdateDirectory))
        //        {
        //            Directory.CreateDirectory(Globals._defaultUpdateDirectory);
        //        }
        //        using (Stream ftpStream = request.GetResponse().GetResponseStream())
        //        using (Stream fileStream = File.Create(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe"))
        //        {
        //            byte[] buffer = new byte[10240];
        //            int read;
        //            while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
        //            {
        //                fileStream.Write(buffer, 0, read);
        //                Console.WriteLine("Downloaded {0} bytes", fileStream.Position);
        //            }
        //        }
        //    }
        //    catch(Exception e)
        //    {

        //    }

        //}

        
    }
}
