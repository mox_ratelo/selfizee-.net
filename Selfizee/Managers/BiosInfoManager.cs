﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class BiosInfoManager
    {
        public string biosName { get; set; }
        public string GetBiosSerial()
        {
            var psi = new ProcessStartInfo("wmic");
            psi.Arguments = @"bios get serialnumber";
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            var p = Process.Start(psi);
            p.WaitForExit();
            String output = p.StandardOutput.ReadToEnd();
            return output;
        }
    }
}
