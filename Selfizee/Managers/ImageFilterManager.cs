﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImageProcessor;
using ImageProcessor.Imaging.Filters.Photo;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using ImageMagick;
using Aurigma.GraphicsMill;
using Aurigma.GraphicsMill.Codecs;
using Aurigma.GraphicsMill.Transforms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Selfizee.Managers;
using System.Threading.Tasks;

namespace Selfizee.Manager
{
    public class ImageFilterManager
    {
        private static string filterdPath = "/Filtered";
        string _imageFolder = @"View";
        string finalPath = $"{Globals._tmpDir}"; 

        public string Applyfilter(string imageFilePath,List<string> filtres)
        {

            var exepath = Globals.ExeDir();
            if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                throw new ArgumentNullException();
            bool onecolor = false;
            bool popart = false;
            bool blackwhite = false;
            bool sepia = false;
            int rangeBW = 0;
            int rangeSepia = 0;

            string _imageFolder = @"View";
            List<IMatrixFilter> matrixs = new List<IMatrixFilter>();
            int k = 0;

            foreach(var filtre in filtres)
            {
               
                switch(filtre.ToUpper())
                {
                    case "BLACKWHITE":
                        k++;
                        rangeBW = k;
                        blackwhite = true;
                        matrixs.Add(MatrixFilters.GreyScale);
                        break;
                    case "SEPIA":
                        k++;
                        rangeSepia = k;
                        sepia = true;
                         matrixs.Add(MatrixFilters.Sepia);
                        break;
                    case "GREYSCALE":
                        matrixs.Add(MatrixFilters.BlackWhite);
                        break;
                    case "ONECOLOR":
                        onecolor = true;
                        break;
                    case "POPART":
                        popart = true;
                        break;
                }                  
            }

            
        
            var currDir = Globals.currDir;
            //var temp = currDir + $"\\..\\..\\{_imageFolder}";
            var temp = currDir + $"\\{_imageFolder}";
            int i = 0;
            string Name = "";
            foreach (var item in matrixs)
            {
                i++;
                Name = "";
                if (i == rangeBW)
                {
                    Name = "BLACKWHITE";
                    var imageFactory = new ImageFactory();
                    var image = imageFactory.Load(imageFilePath).Filter(item).Image;
                    int j = NewInt($"{temp}", i);
                    image.Save($"{temp}\\IMG_{Name}.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //ImageUtility.writeDebug(Globals._debugText, "Black white saved, File name: " + $"{temp}\\IMG_{Name}.jpeg");
                    image.Dispose();
                }
                else if(i == rangeSepia)
                {
                    Name = "SEPIA";
                    var imageFactory = new ImageFactory();
                    var image = imageFactory.Load(imageFilePath).Filter(item).Image;
                    int j = NewInt($"{temp}", i);
                    image.Save($"{temp}\\IMG_{Name}.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //ImageUtility.writeDebug(Globals._debugText, "sepia saved, File name: " + $"{temp}\\IMG_{Name}.jpeg");
                    image.Dispose();
                }
                else { 
}
               
               
               
            }
            if (onecolor) Name = "ONECOLOR";
            if (popart) Name = "POPART";
            if (onecolor)
            {
                ApplyOneCustomFilter(imageFilePath, "OneColor", 0);
            }
            if (popart)
            {
                ApplyOneCustomFilter(imageFilePath, "popart", 0);
            }
            return filterdPath; 
        }

       public void resizeGifImage(string path,int width,int height)
        {
            using (var collection = new MagickImageCollection(new FileInfo(path)))
            {
                collection.Coalesce();
                foreach (var image in collection)
                {
                    image.Resize(width, height);
                }
                collection.Write(path);
            }
        }

        public System.Drawing.Bitmap removeGraphicsMillGreen(System.Drawing.Bitmap input, string BG,int strength,int transparency)
        {
            using (var background = new Aurigma.GraphicsMill.Bitmap(BG))
            {
                //using (var resize = new Resize(1048, 0, ResizeInterpolationMode.High))

                using (var resize = new Resize(0, background.Height, ResizeInterpolationMode.High))
                using (var bitmap = new Aurigma.GraphicsMill.Bitmap(input))
                {
                    Single _strength = 3;//3 -w 30
                    Single _transparency = 2000;
                    if (strength <= 0 || transparency <= 0)
                    {
                        string code = Globals.GetEventId();
                        string iniPath = "c:\\Events\\Assets\\" + code + "\\Config.ini";
                        INIFileManager _ini = new INIFileManager(iniPath);
                        int _strengthini = Convert.ToInt32(_ini.GetSetting("EVENT", "strength"));
                        int _transparencyini = Convert.ToInt32(_ini.GetSetting("EVENT", "transparency"));
                        if(_strengthini >=3 && _strengthini <= 30)
                        {
                            _strength = (Single)_strengthini; ;//3 -> 30
                        }
                       
                        //_transparency = (Single)_transparencyini; ; //400 -> 2000
                    }
                    else
                    {
                        _strength = (Single)strength; ;//3 -w 30
                        //_transparency = (Single)transparency; ; //400 -> 2000
                    }
                    bitmap.Transforms.Resize(background.Width, background.Height, ResizeInterpolationMode.High);
                    if(_strength > 0 || _transparency > 0)
                    {
                        bitmap.Transforms.RemoveGreenScreen(_strength, _transparency);
                    }
                    else
                    {
                        bitmap.Transforms.RemoveGreenScreen();
                    }
                    
                    //Draw the image without green-colored areas on the new background
                    background.Draw(bitmap, 0, 0, CombineMode.Alpha);
                    bitmap.Dispose();
                    return (System.Drawing.Bitmap)background;
                }
            }
        }

        public System.Drawing.Bitmap removeGraphicsMillGreenAsync(System.Drawing.Bitmap input, string BG, int strength, int transparency, System.Drawing.Bitmap bgChromakey, int widthresized,int heightresized)
        {
            Single _strength = 0;//3 -w 30
            Single _transparency = 2000;
            if (strength <= 0 || transparency <= 0)
            {
                string code = Globals.GetEventId();
                string iniPath = "c:\\Events\\Assets\\" + code + "\\Config.ini";
                INIFileManager _ini = new INIFileManager(iniPath);
                int _strengthini = Convert.ToInt32(_ini.GetSetting("EVENT", "strength"));
                int _transparencyini = Convert.ToInt32(_ini.GetSetting("EVENT", "transparency"));
                _strength = (Single)_strengthini; ;//3 -> 30
                                                   //_transparency = (Single)_transparencyini; ; //400 -> 2000
                if (strength <= 0) _strength = 18;
            }
            else
            {
                _strength = (Single)strength; ;//3 -w 30
                                               //_transparency = (Single)transparency; ; //400 -> 2000
            }
            using (var background = new Aurigma.GraphicsMill.Bitmap(bgChromakey))
            {
                //using (var resize = new Resize(1048, 0, ResizeInterpolationMode.High))

                using (var resize = new Resize(widthresized, heightresized, ResizeInterpolationMode.High))
                using (var bitmap = new Aurigma.GraphicsMill.Bitmap(input))
                {
                    
                    //Aurigma.GraphicsMill.Bitmap _background = new Aurigma.GraphicsMill.Bitmap();
                    //if(bgChromakey != null)
                    //{
                    //    _background = (Aurigma.GraphicsMill.Bitmap)bgChromakey;
                    //}
                    //else
                    //{
                    //    _background = background;
                    //}
                    
                    if (_strength > 0 && _transparency > 0)
                    {
                        bitmap.Transforms.RemoveGreenScreen(_strength, _transparency);
                    }
                    else
                    {
                        bitmap.Transforms.RemoveGreenScreen();
                    }

                    //Draw the image without green-colored areas on the new background
                    background.Draw(bitmap, 0, 0, CombineMode.Alpha);
                    bitmap.Dispose();
                    return (System.Drawing.Bitmap)background;
                }
            }
        }

        public System.Drawing.Bitmap removeGraphicsMillGreenToSave(System.Drawing.Bitmap input, string BG)
        {
            using (var background = new Aurigma.GraphicsMill.Bitmap(BG))
            {
                //using (var resize = new Resize(1048, 0, ResizeInterpolationMode.High))

                using (var resize = new Resize(0, background.Height, ResizeInterpolationMode.High))
                using (var bitmap = new Aurigma.GraphicsMill.Bitmap(input))
                {
                    bitmap.Transforms.Resize(background.Width, background.Height, ResizeInterpolationMode.High);
                    bitmap.Transforms.RemoveGreenScreen();
                    //Draw the image without green-colored areas on the new background
                    background.Draw(bitmap, 0, 0, CombineMode.Alpha);
                    bitmap.Dispose();
                    return (System.Drawing.Bitmap)background;
                    //Save the resulting image
                    //background.Save("../../../../_Output/out.jpg");
                }
                //Remove the green background
            }
            //using (var bitmap = new Aurigma.GraphicsMill.Bitmap(input))
            
            //using (var GreenRemove = new GreenScreenRemoval())
            //{
            //    using (var newBitmap = GreenRemove.Apply(bitmap))
            //    {
            //        bitmap.Dispose();
            //        return (System.Drawing.Bitmap)newBitmap;

            //    }
            //}
        }

        public void ApplyOneFilter(string imagePath, string filter,int index)
        {
             
            IMatrixFilter matrix;
            var currDir = Globals.currDir;
            var temp = currDir + $"\\{_imageFolder}\\Temp";
            string filename = "";
            switch (filter)
            {
                case "Sepia":
                    matrix = MatrixFilters.Sepia;
                    var imageFactory = new ImageFactory();
                    var image = imageFactory.Load(imagePath).Filter(matrix).Image;
                    int j = NewInt($"{temp}", index);
                    //image.Save($"{temp}\\" + filter +"\\" + $"{index.ToString()}_Sepia.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    image.Save(imagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    image.Dispose();
                    //File.Delete(imagePath);
                    break;
                case "Blackwhite":
                    matrix = MatrixFilters.GreyScale;
                    var imageFactoryBW = new ImageFactory();
                    var imageBW = imageFactoryBW.Load(imagePath).Filter(matrix).Image;
                    int k = NewInt($"{temp}", index);
                    filename = $"{temp}\\" + filter + "\\" + $"{index.ToString()}_BLACKWHITE.jpg";
                    //imageBW.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    imageBW.Save(imagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    imageBW.Dispose();
                    //File.Delete(imagePath);
                    break;
                case "Color":
                    filename = $"{temp}\\" + filter + "\\" + $"{index.ToString()}_Color.jpg";
                    File.Copy(imagePath, filename);
                    break;
                case "OneColor":
                    filename = $"{temp}\\" + filter + "\\" + $"{index.ToString()}_OneColor.jpg";
                    System.Drawing.Bitmap imageTemp = new System.Drawing.Bitmap(imagePath);
                    imageTemp = ApplyFilterOneColorActivated(imageTemp, System.Drawing.Color.Red, 12);
                    imageTemp.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    imageTemp.Dispose();
                    //File.Delete(imagePath);
                    break;
                case "Popart":
                    filename = $"{temp}\\" + filter + "\\" + $"{index.ToString()}_popart.jpg";
                    System.Drawing.Bitmap imagePopart = new System.Drawing.Bitmap(imagePath);
                    imagePopart = BitmapFilter.EmbossLaplacian(imagePopart);
                    //imagePopart = ImageUtility.GradientBasedEdgeDetectionFilter(imagePopart, ImageUtility.EdgeFilterType.EdgeDetectMono, ImageUtility.DerivativeLevel.First, 1, 1, 1, 12);
                    imagePopart.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //File.Delete(imagePath);
                    break;
            }
            
            
            


        }

        public void ApplyOneCustomFilter(string imagePath, string filter, int index)
        {
            var currDir = Globals.currDir;
            var temp = currDir + $"\\{_imageFolder}";
            string filename = "";
            switch (filter)
            {
                case "OneColor":
                    filename = $"{temp}\\" + $"IMG_ONECOLOR.jpg";
                    System.Drawing.Bitmap imageTemp = new System.Drawing.Bitmap(imagePath);
                    imageTemp = ApplyFilterOneColorActivated(imageTemp, System.Drawing.Color.Red, 12);
                    imageTemp.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //ImageUtility.writeDebug(Globals._debugText, "one color saved, File name: " + filename);
                    //File.Delete(imagePath);
                    break;
                case "popart":
                    filename = $"{temp}\\" + $"IMG_POPART.jpg";
                    System.Drawing.Bitmap imagePopart = new System.Drawing.Bitmap(imagePath);
                    imagePopart = BitmapFilter.EmbossLaplacian(imagePopart);

                    //imagePopart = ImageUtility.GradientBasedEdgeDetectionFilter(imagePopart, ImageUtility.EdgeFilterType.EdgeDetectMono, ImageUtility.DerivativeLevel.First, 1, 1, 1, 12);
                    imagePopart.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //ImageUtility.writeDebug(Globals._debugText, "pop art saved, File name: " + filename);
                    //popartImage.Write(filename);
                    //File.Delete(imagePath);
                    break;
            }

        }

        



        public static System.Drawing.Bitmap ApplyFilterOneColorActivated(System.Drawing.Bitmap input, System.Drawing.Color co, int tolerance)
        {
            return input.ColorSubstitution();
        }

        public static bool ColorsAreClose(System.Drawing.Color color1, System.Drawing.Color color2 )
        {
            int Threshold = 50;
            var rDist = Math.Abs(color1.R - color2.R);
            var gDist = Math.Abs(color1.G - color2.G);
            var bDist = Math.Abs(color1.B - color2.B);

            if (rDist + gDist + bDist > Threshold)
                return false;

            return true;

        }

        private int NewInt(string path, int i)
        {
            var _path = $"{path}\\{i.ToString()}.jpg"; 
            if (File.Exists(_path))
            {
                i += 1; 
                NewInt(path, i);
            }
            return i; 
        }
        public static String CreateFileName(int i)
        {
            DateTime dt = DateTime.Now;
            String formattedDate = (String.Format("{0:u}", dt));
            formattedDate = formattedDate.Replace(":", "");
            formattedDate = formattedDate.Trim();
            return "IMG_" + formattedDate + i + ".jpg";
        }

        public void ChromaKeyManager(string imagePath)
        {           
            using (var shirt = new MagickImage(imagePath))
            {             
                shirt.Modulate(new Percentage(100), new Percentage(100), new Percentage(33.3));
                shirt.ColorSpace = ImageMagick.ColorSpace.HSV;              
                var clone0 = new MagickImage();
                using (MagickImageCollection imges = new MagickImageCollection(shirt.Separate(Channels.RGB)))
                {
                    imges[0].BackgroundColor = MagickColors.None;
                    imges[0].ColorFuzz = new Percentage(5);
                    imges[0].Transparent(new MagickColor("grey32"));
                    imges[1].BackgroundColor = MagickColors.None;
                    imges[1].ColorFuzz = new Percentage(10);
                    imges[1].Transparent(new MagickColor("grey50"));
                    imges[2].BackgroundColor = MagickColors.None;
                    imges[2].ColorFuzz = new Percentage(20);
                    imges[2].Transparent(new MagickColor("grey60"));
                    imges[0].Alpha(AlphaOption.Extract);         
                    imges[0].Composite(imges[1], CompositeOperator.Multiply);
                    imges[0].Composite(imges[2], CompositeOperator.Multiply);
                    imges[0].Negate(true);
                    imges[0].Write(finalPath + "shirt_mask_prime.png");
                }
              
                using (MagickImageCollection images = new MagickImageCollection(shirt.Separate(Channels.Red | Channels.Green)))
                {
                    images[0].BackgroundColor = MagickColors.None;
                    images[0].ColorFuzz = new Percentage(5);
                    images[0].InverseTransparent(new MagickColor("grey64"));
                    images[1].BackgroundColor = MagickColors.None;
                    images[1].ColorFuzz = new Percentage(10);
                    images[1].Transparent(new MagickColor("black"));
                    images[0].Alpha(AlphaOption.Extract);
                    images[1].Alpha(AlphaOption.Extract);
                    images[0].Composite(images[1], CompositeOperator.Multiply);
                    images[0].Write(finalPath + "shirt_mask.png");
                }                               
                shirt.TransparentChroma(new MagickColor(0, 0, 0), new MagickColor(0, 255, 0));
                shirt.TransparentChroma(new ColorRGB(0, 0, 0), new ColorRGB(0, 255, 0));
                shirt.Write(finalPath + "shirt_mask.png");
            }

            using (MagickImageCollection images = new MagickImageCollection())
            {
                MagickImage first = new MagickImage(imagePath);
                first.Alpha(AlphaOption.Off); 
                images.Add(first);
                MagickImage second = new MagickImage(finalPath + "shirt_mask_prime.png");
                images.Add(second);
                first.Composite(second, CompositeOperator.CopyAlpha);
                first.Write(finalPath + "result.png");
            }
        }

        public void ChromaKey2(string imagepath)
        {
            var greenscreen = new ImageMagick.MagickImage(imagepath);
            greenscreen.Alpha(AlphaOption.On);

            using (var _clone = greenscreen.Clone())
            {
                _clone.Blur(0, 5);
                _clone.ColorSpace = ImageMagick.ColorSpace.Lab;
                _clone.Evaluate(Channels.Red, EvaluateOperator.Set,new  Percentage(50));
                _clone.ColorSpace = ImageMagick.ColorSpace.sRGB;
                _clone.Quantize(new QuantizeSettings()
                {
                    DitherMethod = DitherMethod.No,
                    Colors = 30
                });
                _clone.ColorFuzz = (Percentage)15;
                _clone.Settings.FillColor = MagickColors.None;
                _clone.Draw(new DrawableAlpha(0, 0, PaintMethod.Replace));
                var _fc = new DrawableFillColor(System.Drawing.Color.Transparent);
                _clone.Draw(_fc);
                var da = new DrawableAlpha(0, 0, PaintMethod.Replace);
                _clone.Draw(da);

                _clone.Negate(Channels.Alpha);
                greenscreen.Composite(_clone, CompositeOperator.Copy);
            }

            greenscreen.Resize(new Percentage(10));
            greenscreen.GaussianBlur(8, 9);
            greenscreen.Resize(new Percentage(1000));
            greenscreen.Alpha(ImageMagick.AlphaOption.Off);
            greenscreen.Write(finalPath + "\\_result.png");
        }

        public void Chromakey3(string imagepath)
        {
            var image = new MagickImage(imagepath);
            image.ColorFuzz = new Percentage(20);
            image.TransparentChroma(new MagickColor("#0F562A"), new MagickColor("#43B788"));
            image.Write(finalPath + "\\_resss.png");
        }

        public System.Drawing.Bitmap GetCopyOf(System.Drawing.Bitmap originalImage)
        {
            System.Drawing.Bitmap copy = new System.Drawing.Bitmap(1920, 1080);
            using (Graphics graphics = Graphics.FromImage(copy))
            {
                Rectangle imageRectangle = new Rectangle(0, 0, copy.Width, copy.Height);
                graphics.DrawImage(originalImage, imageRectangle, imageRectangle, GraphicsUnit.Pixel);
            }
            return copy;
        }

        public System.Drawing.Bitmap RemoveBackground(System.Drawing.Bitmap input , string BG)
        {
            System.Drawing.Bitmap clone = new System.Drawing.Bitmap(input.Width, input.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            {
                using (input)
                using (Graphics gr = Graphics.FromImage(clone))
                {
                    gr.DrawImage(input, new Rectangle(0, 0, clone.Width, clone.Height));
                }

                var data = clone.LockBits(new Rectangle(0, 0, clone.Width, clone.Height), ImageLockMode.ReadWrite, clone.PixelFormat);

                var bytes = Math.Abs(data.Stride) * clone.Height;
                byte[] rgba = new byte[bytes];
                System.Runtime.InteropServices.Marshal.Copy(data.Scan0, rgba, 0, bytes);

                var pixels = Enumerable.Range(0, rgba.Length / 4).Select(x => new {
                    B = rgba[x * 4],
                    G = rgba[(x * 4) + 1],
                    R = rgba[(x * 4) + 2],
                    A = rgba[(x * 4) + 3],
                    MakeTransparent = new Action(() => rgba[(x * 4) + 3] = 0)
                });

                pixels
                    .AsParallel()
                    .ForAll(p =>
                    {
                        byte max = Math.Max(Math.Max(p.R, p.G), p.B);
                        byte min = Math.Min(Math.Min(p.R, p.G), p.B);

                        if (p.G != min && (p.G == max || max - p.G < 2) && (max - min) > 20 && p.G > p.R && p.G > p.B)
                            p.MakeTransparent();
                    });

                System.Runtime.InteropServices.Marshal.Copy(rgba, 0, data.Scan0, bytes);
                clone.UnlockBits(data);
                System.Drawing.Bitmap bitmapBG = null;
                double coeffW = 0;
                double coeffH = 0;
                if (BG != "")
                {
                     bitmapBG = new System.Drawing.Bitmap(BG);
                    var bgwidth = bitmapBG.Width;
                    var bgheigth = bitmapBG.Height;
                    var inputwidth = clone.Width;
                    var inputheigth = clone.Height;

                     coeffW = (double)inputwidth / (double)bgwidth;
                     coeffH = (double)inputheigth / (double)bgheigth;
                }
                

               
               

                if(bitmapBG!=null)
                {
                    bitmapBG = new System.Drawing.Bitmap(bitmapBG, new Size((int)(bitmapBG.Width * coeffW), (int)(bitmapBG.Height * coeffH)));
                }
                else
                {
                    bitmapBG = new System.Drawing.Bitmap(clone.Width, clone.Height);
                    using (Graphics gfx = Graphics.FromImage(bitmapBG))
                    using (SolidBrush brush = new SolidBrush(System.Drawing.Color.Transparent))
                    {
                        gfx.FillRectangle(brush, 0, 0, clone.Width, clone.Height);
                    }
                }


                MagickImage first = new MagickImage(clone);
                //if (Globals.gswithoutframe)
                //{
                //    first.Write(Globals._tmpDir + "\\IMG_COLOR.jpg");
                //    first.Dispose();
                //    return clone;
                //}
                MagickImage second = new MagickImage(bitmapBG);
               
                using (MagickImageCollection images = new MagickImageCollection())
                {
                    second.Composite(first, Gravity.South, CompositeOperator.Over); 
                }

                second.Write(Globals._tmpDir + "\\IMG_COLOR.jpg");
                clone.Dispose();
                second.Dispose();
                bitmapBG.Dispose(); 
                return clone;
            }


        }


        public System.Drawing.Bitmap RemoveBlackBackground(System.Drawing.Bitmap input)
        {
            System.Drawing.Bitmap clone = new System.Drawing.Bitmap(input.Width, input.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            {

                using (input)
                using (Graphics gr = Graphics.FromImage(clone))
                {
                    gr.DrawImage(input, new Rectangle(0, 0, clone.Width, clone.Height));
                }

                var data = clone.LockBits(new Rectangle(0, 0, clone.Width, clone.Height), ImageLockMode.ReadWrite, clone.PixelFormat);

                var bytes = Math.Abs(data.Stride) * clone.Height;
                byte[] rgba = new byte[bytes];
                System.Runtime.InteropServices.Marshal.Copy(data.Scan0, rgba, 0, bytes);

                var pixels = Enumerable.Range(0, rgba.Length / 4).Select(x => new {
                    B = rgba[x * 4],
                    G = rgba[(x * 4) + 1],
                    R = rgba[(x * 4) + 2],
                    A = rgba[(x * 4) + 3],
                    MakeTransparent = new Action(() => rgba[(x * 4) + 3] = 0)
                });

                pixels
                    .AsParallel()
                    .ForAll(p =>
                    {

                        if (p.R == 0 && p.G == 0  && p.B == 0)
                            p.MakeTransparent();
                    });

                System.Runtime.InteropServices.Marshal.Copy(rgba, 0, data.Scan0, bytes);
                clone.UnlockBits(data);
                var inputwidth = clone.Width;
                var inputheigth = clone.Height;
                return clone;
            }


        }

        bool ColorsAreClose(int rBase, int gBase, int bBase, int rCompare, int gCompare, int bCompare, int threshold = 50)
        {
            int r = (int)rBase - rCompare,
                g = (int)gBase - gCompare,
                b = (int)bBase - bCompare;
            return (r * r + g * g + b * b) <= threshold * threshold;
        }

        public System.Drawing.Bitmap RemoveBackgroundLiveView(System.Drawing.Bitmap input, string BG)
        {
            System.Drawing.Bitmap clone = new System.Drawing.Bitmap(input.Width, input.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            {
                using (input)
                using (Graphics gr = Graphics.FromImage(clone))
                {
                    gr.DrawImage(input, new Rectangle(0, 0, clone.Width, clone.Height));
                }

                var data = clone.LockBits(new Rectangle(0, 0, clone.Width, clone.Height), ImageLockMode.ReadWrite, clone.PixelFormat);

                var bytes = Math.Abs(data.Stride) * clone.Height;
                byte[] rgba = new byte[bytes];
                System.Runtime.InteropServices.Marshal.Copy(data.Scan0, rgba, 0, bytes);
               

                var pixels = Enumerable.Range(0, rgba.Length / 4).Select(x => new
                {
                    B = rgba[x * 4],
                    G = rgba[(x * 4) + 1],
                    R = rgba[(x * 4) + 2],
                    A = rgba[(x * 4) + 3],
                    MakeTransparent = new Action(() => rgba[(x * 4) + 3] = 0)
                });

                float ToleranceHue = 100;
                float ToleranceSaturnation = 1f;
                float ToleranceBrightness = 0.5f;

                pixels
                    .AsParallel()
                    .ForAll(p =>
                    {
                        //int rgb = (int)p;

                        //int red = (rgb >> 16) & 0xff;
                        //int green = (rgb >> 8) & 0xff;
                        //int blue = rgb & 0xff;
                        //int red = p.R;
                        //int green = p.G;
                        //int blue = p.B;
                        //HSB hsb = ColorSpaceHelper.RGBtoHSB(red, green, blue);

                        //if (Math.Abs(hsb.Hue - keyHsb.Hue) < ToleranceHue && Math.Abs(hsb.Saturation - keyHsb.Saturation) < ToleranceSaturnation && Math.Abs(hsb.Brightness - keyHsb.Brightness) < ToleranceBrightness)
                        //{
                        //    //p = rgb & 0xffffff;
                        //    p.MakeTransparent();
                        //}
                        //else
                        //{
                        //    pixels[i] = rgb;
                        //}

                        byte max = Math.Max(Math.Max(p.R, p.G), p.B);
                        byte min = Math.Min(Math.Min(p.R, p.G), p.B);

                        //if(p.G != min && (p.G == max || max - p.G < 8) && (max - min) > 96)
                        //    p.MakeTransparent();
                        if (p.G != min && (p.G == max || max - p.G < 7) && (max - min) > 20)
                            p.MakeTransparent();
                        //if (p.G != min && (p.G == max || max - p.G < 2) && (max - min) > 20 && p.G > p.R && p.G > p.B)
                        //    p.MakeTransparent();
                    });

                System.Runtime.InteropServices.Marshal.Copy(rgba, 0, data.Scan0, bytes);
                clone.UnlockBits(data);

                var bitmapBG = new System.Drawing.Bitmap(BG);
                var bgwidth = bitmapBG.Width;
                var bgheigth = bitmapBG.Height;
                var inputwidth = clone.Width;
                var inputheigth = clone.Height;

                var coeffW = (double)inputwidth / (double)bgwidth;
                var coeffH = (double)inputheigth / (double)bgheigth;

                bitmapBG = new System.Drawing.Bitmap(bitmapBG, new Size((int)(bitmapBG.Width * coeffW), (int)(bitmapBG.Height * coeffH)));

                MagickImage first = new MagickImage(clone);
                MagickImage second = new MagickImage(bitmapBG);

                using (MagickImageCollection images = new MagickImageCollection())
                {
                    second.Composite(first, Gravity.South, CompositeOperator.Over);
                }

                //second.Write(finalPath + $"\\IMGCK_temp.jpeg");
                //second.ToBitmap();
                clone.Dispose();
                //second.Dispose();
                System.Drawing.Bitmap result = second.ToBitmap();
                //await Task.Delay(500);
                second.Dispose();
                bitmapBG.Dispose();
                //return second.ToBitmap(); ;
                return result;
            }


        }
    }
}
