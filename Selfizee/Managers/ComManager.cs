﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class ComManager
    {
        private ComManager instance = null;
        SerialPort port { get; set; }
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ComManager(string name,int portName)
        {
            try
            {
                if (port==null)
                {
                    port = new SerialPort(name, portName, Parity.None, 8, StopBits.One);
                    port.Open();
                }
            }
            catch (UnauthorizedAccessException) { }
            catch (System.IO.IOException) { }
            catch (ArgumentException) { }
        }

        public void CloseCom()
        {
            if (port.IsOpen)
            {
                port.Close();
            }
        }

        public string readData()
        {
            if (port.IsOpen)
            {
                return port.ReadExisting();
            }
            else { Log.Warn("Le port est fermé"); }
            return "";
        }

        public void writeData(string message)
        {
            if (port.IsOpen)
            {
                port.Write(message);
                CloseCom();
            }
            else { Log.Warn("Le port est fermé"); }

        }
    }
}
