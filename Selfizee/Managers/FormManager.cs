﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KeyBoardControl.Keyboard;
using System.Windows.Controls.Primitives;
using Selfizee.Models.Form; 

namespace Selfizee.Managers
{
    public class FormManager
    {
        public delegate void EventForTextBox(object sender, KeyboardFocusChangedEventArgs e);
        private Popup _popup;
        private OnScreenKeyboard _keyboard;
        private Style _keyboardStyle;
        private Grid _keyboardContainer;
        private string Name {get;set;}

        
        public void AddQuestionFermer(StackPanel panel, string title, Style btnstyle, Style questionStyle)
        {
            var guid = Guid.NewGuid().ToString();
            guid = new string((from c in guid
                               where char.IsLetter(c)
                               select c
       ).ToArray()).ToUpper();
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle; 
            StackPanel btnpanel = new StackPanel();
            btnpanel.HorizontalAlignment = HorizontalAlignment.Center;
            btnpanel.VerticalAlignment = VerticalAlignment.Center;
            btnpanel.Orientation = Orientation.Horizontal;
            ToggleButton btnoui = new ToggleButton();
            btnoui.Content = "OUI";
            btnoui.TouchLeave += toggle_TouchLeave;
            btnoui.PreviewMouseDown += toggle_checked;
            btnoui.Name = guid + "_OUI";
            btnoui.FontSize = 20;
            btnoui.Tag = guid + "_OUI";
            btnoui.Style = btnstyle;
            btnpanel.Children.Add(btnoui);
           

            ToggleButton btnnon = new ToggleButton();
            btnnon.Name = guid + "_NON";
            btnnon.Checked += toggle_checked;
            btnnon.TouchLeave += toggle_TouchLeave;
            btnnon.Content = "NON";
            btnnon.FontSize = 20;
            btnnon.Tag = guid + "_NON";
            btnnon.Style = btnstyle; 
            btnpanel.Children.Add(btnnon);

            if (isTitleMandatory(title))
            {
                MandatoryControl obligatory = new MandatoryControl();
                obligatory.title = title;
                obligatory.isMandatory = true;
                obligatory.lstControl = new List<Control>();
                obligatory.lstControl.Add(btnoui);
                obligatory.lstControl.Add(btnnon);
                obligatory.response = "";
                Globals.obligatoryField.Add(obligatory);
            }
            else
            {
                MandatoryControl obligatory = new MandatoryControl();
                obligatory.title = title;
                obligatory.isMandatory = false;
                obligatory.lstControl = new List<Control>();
                obligatory.lstControl.Add(btnoui);
                obligatory.lstControl.Add(btnnon);
                obligatory.response = "";
                Globals.obligatoryField.Add(obligatory);
            }
            
            panel.Children.Add(textbloc);
            TextBlock emptyText = new TextBlock();
            panel.Children.Add(emptyText);
            panel.Children.Add(btnpanel);

            Separator separator = new Separator();
            panel.Children.Add(separator);
        }

        private void toggle_TouchLeave(object sender, EventArgs e)
        {
            ToggleButton tgBtn = sender as ToggleButton;
            bool tgBtnChecked = (bool)tgBtn.IsChecked;
            string name = tgBtn.Name;
            string currentGuid = name.Split('_').First();
            string label = name.Split('_').Last();

            StackPanel parent = (StackPanel)VisualTreeHelper.GetParent(tgBtn);
            if (parent.Children.Count > 0)
            {
                foreach (var child in parent.Children)
                {
                    var currentToggleBtn = (ToggleButton)child;
                    currentToggleBtn.Checked += toggle_TouchLeave;
                }
            }
        }

        public bool isTitleMandatory(string title)
        {
            char last = title[title.Length - 1];
            return (last == '*') ? true : false;
        }


        private void toggle_checked(object sender, EventArgs e)
        {
            var tgBtn = sender as ToggleButton;
            //tgBtn.IsChecked = true;
            var tgBtnChecked = (bool)tgBtn.IsChecked;
            //if (tgBtnChecked)
            //{
                var name = tgBtn.Name;
                var currentGuid = name.Split('_').First();
                var label = tgBtn.Content.ToString();

                var parent = (StackPanel)VisualTreeHelper.GetParent(tgBtn);
                if (parent.Children.Count > 0)
                {
                    foreach (var child in parent.Children)
                    {
                        string currentLabel = "";
                        if (child is ToggleButton)
                        {
                            currentLabel = "";
                            var currentToggleBtn = (ToggleButton)child;
                            bool btnChecked = (bool)currentToggleBtn.IsChecked;
                            currentLabel = currentToggleBtn.Content.ToString();
                            if (!currentLabel.Contains(label))
                            {
                               currentToggleBtn.IsChecked = false;
                            }
                        }

                    }
                }

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == tgBtn.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var tgButton = (ToggleButton)currentControl;
                    if (tgButton!=null)
                    {
                        control.response = tgButton.Content.ToString();
                    }
                    else
                    {
                        control.response = "";
                    }
                }
            }

        }

        public void AddQuestionOuvert(StackPanel panel, string title, Style questionStyle, Popup popup, OnScreenKeyboard keyboard, Grid keyboardcontainer, bool? isMandatory)
        {
            StackPanel container = new StackPanel();
            container.Orientation = Orientation.Vertical;
            container.HorizontalAlignment = HorizontalAlignment.Center;
            container.VerticalAlignment = VerticalAlignment.Center;
            container.Width = 600; 
            container.Tag = "formcontainer"; 
            
            _keyboardContainer = keyboardcontainer;
            _keyboard = keyboard; 
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle; 
            TextBox textbox = new TextBox();
            _popup = popup;
            // _keyboardStyle = keyboardstyle; 
            textbox.GotKeyboardFocus += TextBoxGotKeyboardFocus;
           // textbox.GotFocus += TextBoxGotFocus;
            //textbox.GotTouchCapture += TextBoxGotKeyboardFocus;
            textbox.Tag = title;
            textbox.TextWrapping = TextWrapping.Wrap;
            textbox.AcceptsReturn = true;
            textbox.Height = 75;
            textbox.FontSize = 20;
            textbox.TextChanged += TextArea_changed;
            textbox.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            textbox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            // textbox.LostKeyboardFocus += TextBoxLostKeyboardFocus;
            textbox.HorizontalContentAlignment = HorizontalAlignment.Left;
            textbox.VerticalContentAlignment = VerticalAlignment.Top;
            
            MandatoryQuestionHelper.SetIsMandatory(textbox, isMandatory ?? false);

            if(isTitleMandatory(title))
            {
                MandatoryControl obligatory = new MandatoryControl();
                obligatory.title = title;
                obligatory.isMandatory = true;
                obligatory.response = "";
                obligatory.lstControl = new List<Control>();
                obligatory.lstControl.Add(textbox);
                Globals.obligatoryField.Add(obligatory);
            }
            else
            {
                MandatoryControl obligatory = new MandatoryControl();
                obligatory.title = title;
                obligatory.isMandatory = false;
                obligatory.response = "";
                obligatory.lstControl = new List<Control>();
                obligatory.lstControl.Add(textbox);
                Globals.obligatoryField.Add(obligatory);
            }
            container.Children.Add(textbloc);
            container.Children.Add(textbox);
            TextBlock emptyText = new TextBlock();
            container.Children.Add(emptyText);
            panel.Children.Add(container);
            Separator separator = new Separator();
            panel.Children.Add(separator);
            //panel.Children.Add(textbloc);
            //panel.Children.Add(textbox); 
        }


        public void AddQuestionOuvertToModal(StackPanel panel, StackPanel receiver, string title, Style questionStyle, Popup popup, OnScreenKeyboard keyboard, Grid keyboardcontainer)
        {
            StackPanel container = new StackPanel();
            container.Orientation = Orientation.Vertical;
            container.HorizontalAlignment = HorizontalAlignment.Center;
            container.VerticalAlignment = VerticalAlignment.Center;
            container.Width = 600;
            container.Tag = "formcontainer_mandatory";

            _keyboardContainer = keyboardcontainer;
            _keyboard = keyboard;
            _popup = popup;
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            container.Children.Add(textbloc);
            foreach (var child in panel.Children)
            {
                if(child is TextBox)
                {
                    TextBox source = (TextBox)child;
                    TextBox textboxDest = new TextBox();
                    _popup = popup;
                    textboxDest.GotKeyboardFocus += TextBoxGotKeyboardFocus;
                    textboxDest.Tag = title + "_mandatory";
                    textboxDest.TextWrapping = TextWrapping.Wrap;
                    textboxDest.AcceptsReturn = true;
                    textboxDest.Height = source.Height;
                    textboxDest.FontSize = source.FontSize;
                    textboxDest.TextChanged += TextArea_changed;
                    textboxDest.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    textboxDest.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                    textboxDest.HorizontalContentAlignment = HorizontalAlignment.Left;
                    textboxDest.VerticalContentAlignment = VerticalAlignment.Top;
                    keyboard.ActiveContainer = textboxDest;
                    container.Children.Add(textboxDest);
                }
            }
            TextBlock emptyText = new TextBlock();
            container.Children.Add(emptyText);
            
            TextBlock emptyText1 = new TextBlock();
            container.Children.Add(emptyText1);
            Separator separator = new Separator();
            container.Children.Add(separator);
            receiver.Children.Add(container);
        }

        private void TextArea_changed(object sender, EventArgs e)
        {
            var textArea = (TextBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == textArea.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var txtBox = (TextBox)currentControl;
                    if (!String.IsNullOrEmpty(txtBox.Text))
                    {
                        control.response = txtBox.Text.ToString();
                    }
                    else
                    {
                        control.response = "";
                    }
                }
            }
        }

        private void checkBox_checked(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == checkBox.Tag.ToString()).SingleOrDefault();
                if (currentControl!=null)
                {
                    var chkBox = (CheckBox)currentControl;
                    if (checkBox.IsChecked==true)
                    {
                        if (!control.response.Contains(checkBox.Content.ToString()))
                        {
                            control.response +=  checkBox.Content.ToString()  + ",";
                        }
                        
                    }
                }
            }

        }

        private void checkBox_Unchecked(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == checkBox.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var chkBox = (CheckBox)currentControl;
                    if (checkBox.IsChecked == true)
                    {
                        if (control.response.Contains(checkBox.Content.ToString()))
                        {
                            control.response.Replace('\u0022' + checkBox.Content.ToString() + '\u0022', "");
                        }

                    }
                }
            }

        }

        private void radiobutton_checked(object sender, EventArgs e)
        {
            var radButton = (RadioButton)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Name.ToString() == radButton.Name.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var rdButton = (RadioButton)currentControl;
                    if (rdButton.IsChecked == true)
                    {
                        control.response = rdButton.Content.ToString();
                    }
                }
            }

        }

        public void AddCheckboxForm(StackPanel panel, string title, List<string>items, Style questionStyle)
        {
            TextBlock textbloc = new TextBlock();
            //byte[] bytes = Encoding.UTF8.GetBytes(title);
            //var encodedString = Encoding.Default.GetString(bytes);
            textbloc.Text = title;
            textbloc.Style = questionStyle; 
            panel.Children.Add(textbloc); 
            if(items.Count>0)
            {
                StackPanel checkboxpanel = new StackPanel();
                checkboxpanel.Orientation = Orientation.Horizontal;
                checkboxpanel.VerticalAlignment = VerticalAlignment.Center;
                checkboxpanel.HorizontalAlignment = HorizontalAlignment.Center; 
                foreach (var item in items)
                {
                    CheckBox chb = new CheckBox();
                    chb.Content = "  " + item + "  ";
                    chb.IsChecked = false;
                    chb.Checked += checkBox_checked;
                    chb.Unchecked += checkBox_Unchecked;
                    Globals.check_increment++;
                    string chkName = "checkBox_checked_" + Globals.check_increment;
                    chb.Tag = chkName; 

                    // chb.Foreground = new SolidColorBrush(Colors.Orange);
                    checkboxpanel.Children.Add(chb);
                    TextBlock sep = new TextBlock();
                    sep.Width = 20;
                }
                if (isTitleMandatory(title))
                {
                    MandatoryControl obligatory = new MandatoryControl();
                    obligatory.title = title;
                    obligatory.isMandatory = true;
                    obligatory.response = "";
                    obligatory.lstControl = new List<Control>();
                    foreach(var child in checkboxpanel.Children)
                    {
                        if( child is CheckBox)
                        {
                            var chkControl = (CheckBox)child;
                            obligatory.lstControl.Add(chkControl);
                        }
                    }
                    
                    Globals.obligatoryField.Add(obligatory);
                }
                else
                {
                    MandatoryControl obligatory = new MandatoryControl();
                    obligatory.title = title;
                    obligatory.isMandatory = false;
                    obligatory.lstControl = new List<Control>();
                    obligatory.response = "";
                    foreach (var child in checkboxpanel.Children)
                    {
                        if (child is CheckBox)
                        {
                            var chkControl = (CheckBox)child;
                            obligatory.lstControl.Add(chkControl);
                        }
                    }
                    Globals.obligatoryField.Add(obligatory);
                }
                panel.Children.Add(checkboxpanel);
                TextBlock emptyText = new TextBlock();
                panel.Children.Add(emptyText);
                Separator separator = new Separator();
                panel.Children.Add(separator);
            }          
        }

        public void AddComboBoxToModal(StackPanel panel, StackPanel receiver, string title, ComboBox item, Style questionstyle)
        {
            Globals.combo_increment++;
            var comboContainer = new StackPanel()
            {
                Orientation = Orientation.Vertical
            };
           
            TextBlock texte = new TextBlock();
            string comboText = title;
            texte.Text = comboText;
            texte.Style = questionstyle;
            comboContainer.Children.Add(texte);
            //comboContainer.Children.Add(texte);
            foreach (var child in panel.Children)
            {
                if(child is ComboBox)
                {
                    var cbSource = (ComboBox)child;
                    ComboBox cbox = new ComboBox();
                    
                    cbox.Name = "Combo_CB_mandatory" + Globals.combo_increment;
                    cbox.Tag = title + "_CB_mandatory";
                    cbox.SelectionChanged += combo_SelectionChanged;
                    cbox.Width = 250;
                    cbox.FontSize = 20;
                    int i = 0;
                    ComboBoxItem defautItemBox = new ComboBoxItem();
                    foreach (var itemCb in cbSource.Items)
                    {
                        i++;
                        var itemTemp = (ComboBoxItem)itemCb;
                        ComboBoxItem cboxitem = new ComboBoxItem();

                        cboxitem.Content = itemTemp.Content;
                        cbox.Items.Add(cboxitem);

                    }
                    comboContainer.Children.Add(cbox);
                }

            }
            TextBlock emptyTexte = new TextBlock();
            comboContainer.Children.Add(emptyTexte);
            //if (comboContainer.Parent != null)
            //{
            //    StackPanel pan = (StackPanel)comboContainer.Parent;
            //    pan.Children.Remove(comboContainer);
            //}
            receiver.Children.Add(comboContainer);
        }

        public void AddComboBoxForm(StackPanel panel, string title, List<string> items, Style questionstyle,ControlTemplate template)
        {
            TextBlock text = new TextBlock();
            text.Text = title;
            text.Style = questionstyle;
            StackPanel comboContainer = new StackPanel();
            comboContainer.Children.Add(text);
            if (items.Count > 0)
            {
                ComboBox cbox = new ComboBox();

                cbox.Template = template;
                cbox.Tag = title + "_CB";
                cbox.SelectionChanged += combo_SelectionChanged;
                cbox.Width = 400;
                cbox.FontSize = 20;
                
                int i = 0;
                string defaultItem = "";
                List<ComboBoxItem> lstCbItem = new List<ComboBoxItem>();
                ComboBoxItem defautItemBox = new ComboBoxItem();
                foreach (var item in items)
                {
                    i++;
                   
                    ComboBoxItem cboxitem = new ComboBoxItem();
                    
                    cboxitem.Content = item;
                    if (i == 1)
                    {
                        defautItemBox = cboxitem;
                        defaultItem = item;
                    }
                    lstCbItem.Add(cboxitem);
                    //cbox.Items.Add(cboxitem);
                    
                }
                cbox.ItemsSource = lstCbItem;
                //cbox.SelectedItem = defautItemBox;
                if (isTitleMandatory(title))
                {
                    MandatoryControl obligatory = new MandatoryControl();
                    obligatory.title = title;
                    obligatory.isMandatory = true;
                    obligatory.lstControl = new List<Control>();
                    obligatory.lstControl.Add(cbox);
                    
                    obligatory.response = "";

                    Globals.obligatoryField.Add(obligatory);
                }
                else
                {
                    MandatoryControl obligatory = new MandatoryControl();
                    obligatory.title = title;
                    obligatory.isMandatory = false;
                    obligatory.lstControl = new List<Control>();
                    obligatory.lstControl.Add(cbox);

                    obligatory.response = "";//(cbox.SelectedItem as ComboBoxItem).Content.ToString();
                    
                    Globals.obligatoryField.Add(obligatory);
                }
                comboContainer.Children.Add(cbox);
                TextBlock emptyText = new TextBlock();
                comboContainer.Children.Add(emptyText);
                Separator separator = new Separator();
                comboContainer.Children.Add(separator);
                panel.Children.Add(comboContainer);
            }
        }

        private void combo_SelectionChanged(object sender, EventArgs e)
        {
            var combo = (ComboBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == combo.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var comboBox = (ComboBox)currentControl;
                    //if (comboBox.IsChecked == true)
                    //{
                    control.response = (comboBox.SelectedItem as ComboBoxItem).Content.ToString();
                    //}
                }
            }
        }

        public void AddRadioButtonToModal(StackPanel panel, StackPanel receiver, string title, StackPanel item, Style questionStyle)
        {
            StackPanel container = new StackPanel();
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            container.Children.Add(textbloc);
            StackPanel rdbboxpanel = new StackPanel();
            rdbboxpanel.Orientation = Orientation.Horizontal;
            rdbboxpanel.VerticalAlignment = VerticalAlignment.Center;
            rdbboxpanel.HorizontalAlignment = HorizontalAlignment.Center;
            foreach (var child in item.Children)
            {
                if (child is RadioButton)
                {
                    RadioButton rdbSource = (RadioButton)child;
                    RadioButton chb = new RadioButton();
                    chb.Content = "  " + rdbSource.Content + "  ";
                    chb.IsChecked = false;
                    chb.Checked += radiobutton_checked;
                    chb.Name = rdbSource.Name + "_mandatory";
                    chb.Tag = title + "." + rdbSource.Content + "_mandatory";
                    rdbboxpanel.Children.Add(chb);
                }
            }
            container.Children.Add(rdbboxpanel);
            TextBlock emptyText = new TextBlock();
            container.Children.Add(emptyText);
            Separator separator = new Separator();
            container.Children.Add(separator);
            receiver.Children.Add(container);
        }

        public void AddCheckBoxToModal(StackPanel panel, StackPanel receiver, string title, CheckBox item, Style questionStyle)
        {
            StackPanel container = new StackPanel();
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            container.Children.Add(textbloc);
            StackPanel checkboxpanel = new StackPanel();
            checkboxpanel.Orientation = Orientation.Horizontal;
            checkboxpanel.VerticalAlignment = VerticalAlignment.Center;
            checkboxpanel.HorizontalAlignment = HorizontalAlignment.Center;
            foreach (var child in panel.Children)
            {
                if(child is CheckBox)
                {
                    CheckBox chkSource = (CheckBox)child;
                    CheckBox chb = new CheckBox();
                    chb.Content = "  " + chkSource.Content + "  ";
                    chb.IsChecked = false;
                    chb.Checked += checkBox_checked;
                    chb.Unchecked += checkBox_Unchecked;
                    chb.Tag = chkSource.Tag + "_mandatory";
                    checkboxpanel.Children.Add(chb);
                }
            }
            container.Children.Add(checkboxpanel);
            TextBlock emptyText = new TextBlock();
            container.Children.Add(emptyText);
            Separator separator = new Separator();
            container.Children.Add(separator);
            receiver.Children.Add(container);
        }

        public void AddTextBoxToModal(StackPanel receiver, string title, TextBox item,Style textBlockStyle, Style questionStyle, OnScreenKeyboard keyboard, Grid keyboardcontainer, Popup popup)
        {
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            receiver.Children.Add(textbloc);
            TextBox newItem = new TextBox();
            _keyboardContainer = keyboardcontainer;
            _keyboard = keyboard;
            string nameControl = item.Name.ToString() + "_mandatory";
            newItem.Name = nameControl;
            _popup = popup;
            //newItem.LostFocus += textBox_LostFocus;
            //newItem.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            //newItem.TextChanged += textBox_TextChanged;
           newItem.GotKeyboardFocus += TextBoxNormaltKeyboardFocus;
            newItem.Width = 400;
            //newItem.Tag = item.Tag + "_mandatory";
            newItem.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            //newItem.Style = textBlockStyle;
            _keyboard.ActiveContainer = newItem;
            StackPanel closedQuestionpanel = new StackPanel();
            closedQuestionpanel.Orientation = Orientation.Horizontal;
            closedQuestionpanel.VerticalAlignment = VerticalAlignment.Center;
            closedQuestionpanel.HorizontalAlignment = HorizontalAlignment.Center;
            closedQuestionpanel.Children.Add(newItem);
            receiver.Children.Add(closedQuestionpanel);

        }

        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            //var textbox = sender as TextBox;
            ////textbox.Text = ""; 
            //if (string.IsNullOrEmpty(textbox.Text) || textbox.Text.Trim() == "EMAIL" || textbox.Text.Trim() == "TELEPHONE" || textbox.Text.Trim() == "CODEPOSTAL")
            //{
            //    textbox.Text = "";
            //}
            //string type = textbox.Tag.ToString();

            //if (tag == "NUMERIQUE")
            //{
            //    alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            //    numeriqueKeyboard.Visibility = Visibility.Visible;
            //    numeriqueKeyboard.ActiveContainer = textbox;
            //}

            //if (tag == "STRING")
            //{
            //    alphanumeriquekeyboard.Visibility = Visibility.Visible;
            //    alphanumeriquekeyboard.ActiveContainer = textbox;
            //    alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
            //    numeriqueKeyboard.Visibility = Visibility.Collapsed;
            //}

        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;

            foreach (MandatoryControl control in Globals.obligatoryField)
            {
                Control currentControl = control.lstControl.Where(t => t.Tag.ToString() == txt.Tag.ToString()).SingleOrDefault();
                if (currentControl != null)
                {
                    var txtBox = (TextBox)currentControl;
                    //if (comboBox.IsChecked == true)
                    //{
                    control.response = txtBox.Text.ToString();
                    //}
                }
            }
        }

        public void AddQuestionFermerToModal(StackPanel panel, StackPanel receiver, string title, ToggleButton item, Style questionStyle)
        {
            StackPanel container = new StackPanel();
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            container.Children.Add(textbloc);
            
            StackPanel btnpanelmandatory = new StackPanel();
            btnpanelmandatory.HorizontalAlignment = HorizontalAlignment.Center;
            btnpanelmandatory.VerticalAlignment = VerticalAlignment.Center;
            btnpanelmandatory.Orientation = Orientation.Horizontal;
            foreach (var child in panel.Children)
            {
                ToggleButton tgSource = (ToggleButton)child;
                ToggleButton btntg = new ToggleButton();
                btntg.Content = tgSource.Content;
                btntg.TouchLeave += toggle_TouchLeave;
                btntg.PreviewMouseDown += toggle_checked;
                btntg.Name = tgSource.Name + "_mandatory";
                btntg.FontSize = 20;
                btntg.Tag = tgSource.Name + "_mandatory";
                btntg.Style = tgSource.Style;
                btnpanelmandatory.Children.Add(btntg);
            }
            container.Children.Add(btnpanelmandatory);
            TextBlock emptyText = new TextBlock();
            container.Children.Add(emptyText);
            receiver.Children.Add(container);
            //panel.Children.Add(btnpanel);

            //Separator separator = new Separator();
            //receiver.Children.Add(separator);
        }

        //public void AddQuestionFermerToModal(StackPanel panel, string title, TextBox item, Style questionStyle)
        //{
        //    if (panel.Children.Count > 0)
        //    {
        //        TextBlock textbloc = new TextBlock();
        //        byte[] bytes = Encoding.UTF8.GetBytes(title);
        //        textbloc.Text = Encoding.Default.GetString(bytes);
        //        textbloc.Style = questionStyle;
        //        panel.Children.Add(textbloc);
        //        StackPanel openedQuestionpanel = new StackPanel();
        //        openedQuestionpanel.Orientation = Orientation.Horizontal;
        //        openedQuestionpanel.VerticalAlignment = VerticalAlignment.Center;
        //        openedQuestionpanel.HorizontalAlignment = HorizontalAlignment.Center;
        //        openedQuestionpanel.Children.Add(item);
        //        panel.Children.Add(openedQuestionpanel);
        //    }
        //}

        public void AddRadioButtonForm(StackPanel panel, string title, List<string> items, Style questionStyle)
        {
            TextBlock textbloc = new TextBlock();
            textbloc.Text = title;
            textbloc.Style = questionStyle;
            StackPanel radioContainer = new StackPanel();
            radioContainer.Children.Add(textbloc);
            if (items.Count > 0)
            {
                StackPanel radiopanel = new StackPanel();
                radiopanel.Orientation = Orientation.Horizontal;
                radiopanel.VerticalAlignment = VerticalAlignment.Center;
                radiopanel.HorizontalAlignment = HorizontalAlignment.Center;
                int i = 0;
                MandatoryControl obligatory = new MandatoryControl();
                string defaultValue = "";
                obligatory.lstControl = new List<Control>();
                foreach (var item in items)
                {
                    i++;
                    RadioButton radio = new RadioButton();
                    Globals.radio_increment++;
                    string radioName = "RADIO_" + Globals.radio_increment;
                    radio.Content = "  " + item;
                    radio.Name = radioName;
                    radio.Tag = title + "_RADIO";
                    radio.Checked += radiobutton_checked;
                    if (i == 1)
                    {
                        defaultValue = radio.Content.ToString();
                        //radio.IsChecked = true;
                    }
                    else{
                        //radio.IsChecked = false;
                    }
                    
                    //radio.Tag = title;
                    obligatory.lstControl.Add(radio);
                    // chb.Foreground = new SolidColorBrush(Colors.Orange);
                    radiopanel.Children.Add(radio);
                    TextBlock sep = new TextBlock();
                    sep.Width = 20;
                    radiopanel.Children.Add(sep);
                }

                if (isTitleMandatory(title))
                {
                    
                    obligatory.title = title;
                    obligatory.isMandatory = true;
                    obligatory.response = "";
                    Globals.obligatoryField.Add(obligatory);
                }
                else
                {
                    obligatory.title = title;
                    obligatory.isMandatory = false;
                    obligatory.response = "";
                    Globals.obligatoryField.Add(obligatory);
                }
                radioContainer.Children.Add(radiopanel);
                
                TextBlock emptyText = new TextBlock();
                radioContainer.Children.Add(emptyText);
                Separator separator = new Separator();
                radioContainer.Children.Add(separator);
                panel.Children.Add(radioContainer);
            }
        }

        private void TextBoxGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox source = e.Source as TextBox;

            if (source != null)
            {
                //_popup.IsOpen = true;
                //_popup.PlacementTarget = source;
                //_popup.Placement = PlacementMode.Mouse;

                // _keyboard.ActiveContainer = source;
                //source.Background = Brushes.LightBlue;
                //var parentpanel = source.Parent as StackPanel;
                //AddKeyboard(parentpanel, source, _keyboardStyle);

                //_popup.IsOpen = true;
                _keyboardContainer.Visibility = Visibility.Visible;
                _keyboard.ActiveContainer = source;

            }
        }

        private void TextBoxNormaltKeyboardFocus(object sender, EventArgs e)
        {
            TextBox source = sender as TextBox;

            if (source != null)
            {
                //_popup.IsOpen = true;
                _keyboardContainer.Visibility = Visibility.Visible;
                _keyboard.ActiveContainer = source;

            }
        }

        private void TextBoxLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            //TextBox source = e.Source as TextBox;

            //if (source != null)
            //{
            //    _popup.IsOpen = false;
            //    source.Background = Brushes.White;
            //}
            if(_keyboardContainer.Visibility == Visibility.Visible)
                 _keyboardContainer.Visibility = Visibility.Collapsed;
        }

        private void TextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox source = e.Source as TextBox;

            if (source != null)
            {
                _popup.IsOpen = false;
                _keyboard.ActiveContainer = source;
                source.Background = Brushes.LightBlue;
                //source.Background = Brushes.White;
            }
        }

        private void AddKeyboard(StackPanel panel, TextBox source, Style style)
        {
            Grid grid = new Grid(); 
            OnScreenKeyboard keyboard = new OnScreenKeyboard();
            keyboard.ToggleButtonStyle = style;
            keyboard.Width = System.Convert.ToDouble(System.Windows.SystemParameters.WorkArea.Width) * 0.53; 
            keyboard.Height = System.Convert.ToDouble(System.Windows.SystemParameters.WorkArea.Height) * 0.33;
            keyboard.ActiveContainer = source;
            keyboard.Name = "formkeyboard";
            grid.Children.Add(keyboard);
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            panel.Children.Add(grid); 
        }
    }
}
