﻿using System.IO;
using System.Net;

namespace Selfizee.Managers
{
    public class WebservicesManager
    {
        public string GetInfoSerialStored(string url)
        {
            // Create a request for the URL.         
            WebRequest request = WebRequest.Create(url);

            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;

            //Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();


            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            // Cleanup the streams and the response.

            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }
    }
}
