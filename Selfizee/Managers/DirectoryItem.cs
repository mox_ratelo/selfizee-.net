﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class DirectoryItem
    {
        public Uri BaseUri;

        public string AbsolutePath
        {
            get
            {
                return string.Format("{0}/{1}", BaseUri, Name);
            }
        }

        public bool IsDirectory { get; set; }
        public string Name { get; set; }
        public List<DirectoryItem> Items { get; set; }
    }
}
