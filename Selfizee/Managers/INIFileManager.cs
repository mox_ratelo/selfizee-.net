﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Selfizee.Models;
using Selfizee.Models.Form;
using System.Globalization;
using log4net;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Selfizee.Managers
{
    public class INIFileManager
    {
        string Path;
        private Hashtable keyPairs = new Hashtable();
        private string iniFilePath;
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string EXE = Assembly.GetExecutingAssembly().GetName().Name;
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        private struct SectionPair
        {
            public string Section;
            public string Key; 
        }

        public List<DateTime> getAllHours(string section)
        {
            List<DateTime> dtTime = new List<DateTime>();
            string starttime = GetSetting(section,"starttime");
            string endtime = GetSetting(section,"endtime");
            dtTime.Add(Convert.ToDateTime(starttime));
            dtTime.Add(Convert.ToDateTime(endtime));
            return dtTime;
        }

        public string Read(string Key, string Section = null)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section ?? EXE, Key, "", RetVal, 255, Path);
            return RetVal.ToString();
        }

        public INIFileManager(string filePath)
        {
            TextReader iniFile = null;
            string strLine = null;
            string currentRoot = null;
            string[] keyPair = null;
            iniFilePath = filePath;
            keyPairs = new Hashtable();
            
            if (File.Exists(iniFilePath))
            {
                //ImageUtility.writeDebug(Globals._debugText, "Ini file exist");
                try
                {
                    iniFile = new StreamReader(iniFilePath, System.Text.Encoding.UTF7);
                    strLine = iniFile.ReadLine();
                    while(strLine != null)
                    {
                        //ImageUtility.writeDebug(Globals._debugText, "Reading line");
                        strLine = strLine.Trim();
                        //strLine = "Veuillez renseigner vos coordonn�es pour recevoir votre photo en num�rique";
                        //if (strLine.Contains("é")){
                        //    bool reak = true;
                        //}
                        //byte[] bytes = ASCIIEncoding.ASCII.GetBytes(strLine);
                        //strLine = ASCIIEncoding.ASCII.GetString(bytes);
                        //strLine = RemoveDiacritics(strLine, true);
                        strLine = strLine.ToUpper();
                        if (strLine != "" && strLine.StartsWith("'")==false)
                        {

                            //ImageUtility.writeDebug(Globals._debugText, "strline not null");
                            //ImageUtility.writeDebug(Globals._debugText, "strline value: " + strLine);
                            if (strLine.StartsWith("[") && strLine.EndsWith("]"))
                            {
                                currentRoot = strLine.Substring(1, strLine.Length - 2);
                            }
                            else
                            {
                                keyPair = strLine.Split(new char[] { '=' }, 2);
                                //ImageUtility.writeDebug(Globals._debugText, "keypair filled");
                                SectionPair sectionPair;
                                String value = null;

                                if (currentRoot == null)
                                    currentRoot = "ROOT";

                                sectionPair.Section = currentRoot;
                                sectionPair.Key = RemoveDiacritics(keyPair[0],true);

                                if (keyPair.Length > 1)
                                    value = RemoveDiacritics(keyPair[1],true);

                                keyPairs.Add(sectionPair, value);
                                //ImageUtility.writeDebug(Globals._debugText, "section added");
                            }
                        }
                        else
                        {
                            int i = 2;
                            i++;
                        }

                        strLine = iniFile.ReadLine();

                    }
                    
                }catch(Exception ex)
                {
                    Log.Fatal("Fichier INI vide ");

                    //iniFile = new StreamReader(iniFilePath);
                    //strLine = iniFile.ReadLine();
                    //while (strLine != null)
                    //{
                    //    //ImageUtility.writeDebug(Globals._debugText, "Reading line");
                    //    strLine = strLine.Trim();
                    //    //strLine = "Veuillez renseigner vos coordonn�es pour recevoir votre photo en num�rique";
                    //    //if (strLine.Contains("é")){
                    //    //    bool reak = true;
                    //    //}
                    //    //byte[] bytes = ASCIIEncoding.ASCII.GetBytes(strLine);
                    //    //strLine = ASCIIEncoding.ASCII.GetString(bytes);
                    //    //strLine = RemoveDiacritics(strLine, true);
                    //    strLine = strLine.ToUpper();
                    //    if (strLine != "" && strLine.StartsWith("'") == false)
                    //    {

                    //        //ImageUtility.writeDebug(Globals._debugText, "strline not null");
                    //        //ImageUtility.writeDebug(Globals._debugText, "strline value: " + strLine);
                    //        if (strLine.StartsWith("[") && strLine.EndsWith("]"))
                    //        {
                    //            currentRoot = strLine.Substring(1, strLine.Length - 2);
                    //        }
                    //        else
                    //        {
                    //            keyPair = strLine.Split(new char[] { '=' }, 2);
                    //            //ImageUtility.writeDebug(Globals._debugText, "keypair filled");
                    //            SectionPair sectionPair;
                    //            String value = null;

                    //            if (currentRoot == null)
                    //                currentRoot = "ROOT";

                    //            sectionPair.Section = currentRoot;
                    //            sectionPair.Key = RemoveDiacritics(keyPair[0], true);

                    //            if (keyPair.Length > 1)
                    //                value = RemoveDiacritics(keyPair[1], true);

                    //            keyPairs.Add(sectionPair, value);
                    //            //ImageUtility.writeDebug(Globals._debugText, "section added");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        int i = 2;
                    //        i++;
                    //    }

                    //    strLine = iniFile.ReadLine();
                    //}
                    //Environment.Exit(0);
                }
                finally
                {
                    if (iniFile != null)
                        iniFile.Close(); 
                }
                
            }
            else
                Log.Fatal($"Le fichier {iniFilePath} est introuvable");
                //Environment.Exit(0);
        }

        public static IEnumerable<char> RemoveDiacriticsEnum(string src, bool compatNorm, Func<char, char> customFolding)
        {
            foreach (char c in src.Normalize(compatNorm ? NormalizationForm.FormKD : NormalizationForm.FormD))
                switch (CharUnicodeInfo.GetUnicodeCategory(c))
                {
                    case UnicodeCategory.NonSpacingMark:
                    case UnicodeCategory.SpacingCombiningMark:
                    case UnicodeCategory.EnclosingMark:
                        //do nothing
                        break;
                    default:
                        yield return customFolding(c);
                        break;
                }
        }
        public static IEnumerable<char> RemoveDiacriticsEnum(string src, bool compatNorm)
        {
            return RemoveDiacritics(src, compatNorm, c => c);
        }
        public static string RemoveDiacritics(string src, bool compatNorm, Func<char, char> customFolding)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in RemoveDiacriticsEnum(src, compatNorm, customFolding))
                sb.Append(c);
            return sb.ToString();
        }
        public static string RemoveDiacritics(string src, bool compatNorm)
        {
            return RemoveDiacritics(src, compatNorm, c => c);
        }

        public String GetSetting(String sectionName, String settingName)
        {
            SectionPair sectionPair;
            sectionPair.Section = sectionName.ToUpper();
            sectionPair.Key = settingName.ToUpper();
            string value = (String)keyPairs[sectionPair];
            if (string.IsNullOrEmpty(value))
            {
                //Log.Warn("La clé [" + settingName + "] est introuvable dans la section [[" + sectionName + "]]");
            }
            return value;
        }

        public String[] EnumSection(String sectionName)
        {
            ArrayList tmpArray = new ArrayList();
            foreach (SectionPair pair in keyPairs.Keys)
            {
                if (pair.Section == sectionName.ToUpper())
                    tmpArray.Add(pair.Key);
            }
            return (String[])tmpArray.ToArray(typeof(String));
        }

        public void AddSetting(String sectionName, String settingName, String settingValue)
        {
            SectionPair sectionPair;
            sectionPair.Section = sectionName.ToUpper();
            sectionPair.Key = settingName.ToUpper();

            if (keyPairs.ContainsKey(sectionPair))
                keyPairs.Remove(sectionPair);

            keyPairs.Add(sectionPair, settingValue);
        }

        public void AddSetting(String sectionName, String settingName)
        {
            AddSetting(sectionName, settingName, null);
        }

        public void DeleteSetting(String sectionName, String settingName)
        {
            SectionPair sectionPair;
            sectionPair.Section = sectionName.ToUpper();
            sectionPair.Key = settingName.ToUpper();

            if (keyPairs.ContainsKey(sectionPair))
                keyPairs.Remove(sectionPair);
        }

        public void SaveSettings(String newFilePath)
        {
            ArrayList sections = new ArrayList();
            String tmpValue = "";
            String strToSave = "";

            foreach (SectionPair sectionPair in keyPairs.Keys)
            {
                if (!sections.Contains(sectionPair.Section))
                    sections.Add(sectionPair.Section);
            }

            foreach (String section in sections)
            {
                strToSave += ("[" + section + "]\r\n");

                foreach (SectionPair sectionPair in keyPairs.Keys)
                {
                    if (sectionPair.Section == section)
                    {
                        tmpValue = (String)keyPairs[sectionPair];

                        if (tmpValue != null)
                            tmpValue = "=" + tmpValue;

                        strToSave += (sectionPair.Key + tmpValue + "\r\n");
                    }
                }

                strToSave += "\r\n";
            }

            try
            {
                TextWriter tw = new StreamWriter(newFilePath);
                tw.Write(strToSave);
                tw.Close();
            }
            catch (Exception ex)
            {
                Log.Error("Imossible d'enregistrer dans le fichier " + iniFilePath);
            }
        }

        public void SaveSettings()
        {
            SaveSettings(iniFilePath);
        }   

        public List<String> GetKeysByValue(string sectionName, string value)
        {
            string pairString = "";
            List<String> result = new List<string>();
            if (keyPairs.Count >0){
                foreach (SectionPair pair in keyPairs.Keys )
                    if (pair.Section.ToUpper() == sectionName.ToUpper())
                    {
                        if (keyPairs[pair].ToString().ToLower() != "")
                        {
                            pairString = keyPairs[pair].ToString().ToLower();
                            //ImageUtility.writeDebug(Globals._debugText, "pair name: " + pairString);
                            if (String.Compare(pairString, value.ToLower()) == 0)
                            {
                                //ImageUtility.writeDebug(Globals._debugText, pair.Key);
                                result.Add(pair.Key);
                            }
                        }
                       
                    }
                        
            }
            
            if(result.Count <= 0)
            {
                Log.Warn("La valeur [" + value + "] est introuvable dans le fichier INI");
            }
            return result;                                    
        }

        public bool CheckIfGreenScreen()
        {
            //var inimanager = new Managers.INIFileManager($"{Globals.ExeDir()}\\Config.ini");
            var value = GetSetting("EVENT", "GreenScreen");
            int _value = 0;
            if (int.TryParse(value, out _value))
                _value = Convert.ToInt32(value);
            return _value != 0;
        }
        public bool CheckIfGSNoFrame()
        {
            var value = GetSetting("EVENT", "gswithoutframe");
            int _value = 0;
            if (int.TryParse(value, out _value))
                _value = Convert.ToInt32(value);
            return _value != 0;
        }

        public bool CheckIfIsMirrored()
        {
            //var inimanager = new Managers.INIFileManager($"{Globals.ExeDir()}\\Config.ini");
            var value = GetSetting("LIVEVIEW", "mirror");
            int _value = 0;
            if (int.TryParse(value, out _value))
                _value = Convert.ToInt32(value);
            return _value != 0;
        }

        public string[] GetBtnPosition(string sectionName, string settingName)
        {
            string[] result = { "", "" }; 
            var iniValue = GetSetting(sectionName, settingName);
            if(!string.IsNullOrWhiteSpace(iniValue))
                return iniValue.Split(',');
            return result; 

        }

        public int GetNumberOfActiveFilter()
        {
            var result = 0;
            var sectionName = "FILTRE"; 
            SectionPair sectionPair;
            sectionPair.Section = sectionName.ToUpper();
            sectionPair.Key = "GreyScale";

           
            if(keyPairs.Count>0)
            {
                var _keys = keyPairs.Keys;
                foreach(var key in _keys )
                {
                    var currentSectionPair = (SectionPair)key; 
                    if(currentSectionPair.Section==sectionName)
                    {
                        if ((string)keyPairs[key] == "1")
                            result += 1; 
                    }
                }              
            }
            return result; 
        }

        public ViewBackgroundAttributeModel GetBackGroundAttributes(string viewName)
        {
            ViewBackgroundAttributeModel viewAttributes = new ViewBackgroundAttributeModel();
            viewAttributes.IsImage = (String)keyPairs[new SectionPair() {Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.BACKGROUNDISIMAGE" }] == "0" ? false : true;
            viewAttributes.BackgroundColor = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.BACKGROUNDCOLOR" }];
            viewAttributes.EnableTitle = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.SHOWTITLE" }] == "0" ? false : true;
            viewAttributes.Title = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLE" }];
            viewAttributes.TitleFontColor = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLEFONTCOLOR" }];
            viewAttributes.TitlePosition = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLEPOSITION" }];
            viewAttributes.TitleFontFamily = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLEFONTFAMILY" }];
            viewAttributes.TitleFontColor = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLEFONTCOLOR" }];
            viewAttributes.TitleFontSize = (String)keyPairs[new SectionPair() { Section = "BACKGROUNDS", Key = $"{viewName.ToUpper()}.TITLEFONTSIZE" }];
            return viewAttributes; 
        }

        public List<Fenetre> GetFenetre()
        {
            List<Fenetre> fenetres = new List<Fenetre>();
            var keys = EnumSection("FORM");
            List<IniTmpObject> inilines = new List<IniTmpObject>(); 
           
            for(int i= 0; i<keys.Count(); i++)
            {
                IniTmpObject currentline = new IniTmpObject(); 
                var valuefromkey = keys[i].Split('.');

                if (valuefromkey.Count() != 3) continue; 
                currentline.Fenetre = valuefromkey[0];
                currentline.Question = valuefromkey[1];
                currentline.Attribute = valuefromkey[2];
                currentline.AttribueValue = GetSetting("FORM", keys[i]);
                inilines.Add(currentline); 
            }

            var _fenetres = inilines.Select(s => s.Fenetre).Distinct(); 
            foreach(var fen in _fenetres)
            {
                var currentFenetre = new Fenetre();
                currentFenetre.Name = fen;
                currentFenetre.Rank = Convert.ToInt32(fen.Substring(6, fen.Length - 6));
                currentFenetre.Title = ! string.IsNullOrEmpty(GetSetting("form",$"WINDOW{currentFenetre.Rank}.TITLE")) ? GetSetting("form", $"WINDOW{currentFenetre.Rank}.TITLE") : $"WINDOW_{currentFenetre.Rank}"; 
                currentFenetre.Questions = new List<Question>(); 
                var _questions = inilines.Where(q => q.Fenetre == fen).Select(s => s.Question).Distinct(); 
                foreach(var qts in _questions)
                {
                    var currentQuestion = inilines.Where(s => s.Fenetre == fen && s.Question == qts).ToList();
                    var question = new Question();
                    question.Rank = Convert.ToInt32(currentQuestion.Where(s => s.Attribute == "NUMERO").Select(s => s.AttribueValue).FirstOrDefault());
                    question.Title = currentQuestion.Where(s => s.Attribute == "TITLE").Select(s => s.AttribueValue).FirstOrDefault()!=null?
                        currentQuestion.Where(s => s.Attribute == "TITLE").Select(s => s.AttribueValue).FirstOrDefault():
                        currentQuestion.Where(s => s.Attribute == "PLACEHOLDER").Select(s => s.AttribueValue).FirstOrDefault();
                    question.Type = currentQuestion.Where(s => s.Attribute == "TYPE").Select(s => s.AttribueValue).FirstOrDefault();
                    question.IsMandatory = currentQuestion.Where(s => s.Attribute == "MANDATORY").Select(s => s.AttribueValue).FirstOrDefault()==null? false :
                         currentQuestion.Where(s => s.Attribute == "MANDATORY").Select(s => s.AttribueValue).FirstOrDefault()=="0"? false : true;
                    question.Items = new List<string>(); 
                    if(currentQuestion.Count>3)
                    {
                        var items = currentQuestion.Where(s => s.Attribute.IndexOf("ITEM") >= 0).OrderBy(i=>i.Attribute).Select(s => s.AttribueValue).ToList();
                        question.Items = items; 
                    }
                    currentFenetre.Questions.Add(question);                  
                }
                fenetres.Add(currentFenetre); 
            }
            return fenetres.OrderBy(s=>s.Name).ToList(); 
        }
    }
}
