﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class FtpDownloadEventManager
    {
        public string ftpFileNamePath { get; set; }
        public string userName { get; set; }
        public string password { get; set; }

        public long GetFileSize()
        {
            NetworkCredential networkCredential = new NetworkCredential(userName, password);
            var ftpWebRequest = GetFtpWebRequest(new Uri(ftpFileNamePath), networkCredential, WebRequestMethods.Ftp.GetFileSize);
            try { return ((FtpWebResponse)ftpWebRequest.GetResponse()).ContentLength; } //Incase of success it'll return the File Size.
            catch (WebException e) { return default(long); }
        } 

        public FtpWebRequest GetFtpWebRequest(Uri requestUri, NetworkCredential networkCredential, string method = null)
        {
            var ftpWebRequest = (FtpWebRequest)WebRequest.Create(requestUri); //Create FtpWebRequest with given Request Uri.
            ftpWebRequest.Credentials = networkCredential; //Set the Credentials of current FtpWebRequest.

            if (!string.IsNullOrEmpty(method))
                ftpWebRequest.Method = method; //Set the Method of FtpWebRequest incase it has a value.
            return ftpWebRequest; //Return the configured FtpWebRequest.
        }

        public void DownloadFile()
        {
            try
            {
                FtpWebRequest request =
  (FtpWebRequest)WebRequest.Create(ftpFileNamePath);
                request.Credentials = new NetworkCredential(userName, password);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                if (!Directory.Exists(Globals._defaultUpdateDirectory))
                {
                    Directory.CreateDirectory(Globals._defaultUpdateDirectory);
                }
                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe"))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                        Console.WriteLine("Downloaded {0} bytes", fileStream.Position);
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
