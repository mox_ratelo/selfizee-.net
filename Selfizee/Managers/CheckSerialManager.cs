﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class CheckSerialManager
    {
        public bool IsValidLicence(string cryptedSerial)
        {
            RSAManager rsaManager = new RSAManager();
            string decryptedString = rsaManager.DecryptString(cryptedSerial);
            return false;
        }
    }
}
