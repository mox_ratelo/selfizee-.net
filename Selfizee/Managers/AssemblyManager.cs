﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Selfizee.Managers
{
    public class AssemblyManager
    {
        string installerFilePath = Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe";
        public string GetAssemblyVersion()
        {
            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            return name.Version.ToString();
        }

        public string SendVersionData()
        {
            string path = "C:\\EVENTS\\Events\\AppConfig.ini";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);
            string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");

            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            string localVersion = name.Version.ToString();

            DateTime datenow = DateTime.Now;
            string sdatenow = datenow.ToString("yyyy-MM-dd HH:mm:ss");

            VersionData version = new VersionData();
            version.numero_borne = idborne;
            version.numero_version = localVersion;
            version.date_heure_installation = sdatenow;
            string responseInString = "";
            try
            {
                string url = "https://booth.selfizee.fr/api/setVersionLogiciel";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["numero_borne"] = version.numero_borne;
                    data["numero_version"] = version.numero_version;
                    data["date_heure_installation"] = version.date_heure_installation;
                    var response = wb.UploadValues(url, "POST", data);

                    responseInString = Encoding.UTF8.GetString(response);
                }
            }
            catch (Exception e)
            {

            }
            return responseInString;

        }

        public void InstallUpdate()
        {
            System.Diagnostics.Process installerProcess=new System.Diagnostics.Process();
            installerProcess = System.Diagnostics.Process.Start(installerFilePath, "/silents");
           
            while (installerProcess.HasExited == false)
            {
                //indicate progress to user 
                // Application.DoEvents(); System.Threading.Thread.Sleep(250);
                string dir = "C:\\EVENTS\\Events\\AppConfig.ini";
                IniUtility _iniUtility = new IniUtility(dir);
                _iniUtility.Write("IsUpdate", "true", "UPDATE");
                SendVersionData();
                Environment.Exit(0);
            }
            //MessageBox.Show("done installing");
        }

        public long GetLocalFileSize()
        {
            FileInfo fi = new FileInfo(installerFilePath);
            return fi.Length;
        }
        
    }
}
