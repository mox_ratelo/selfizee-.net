﻿using Selfizee.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Drawing;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Selfizee.Managers;
using Selfizee.Models.Enum;
using Selfizee.Models.Form;
using Selfizee.View;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for BackgroundWindow.xaml
    /// </summary>
    public partial class BackgroundWindow : UserControl
    {
        private ImageCollection _photos;
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private string currDir = Globals.currDir;
        private string pathOfImage = "";
        
        public BackgroundWindow()
        {
            InitializeComponent();
            try
            {
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                Globals._currentBackground = "";
                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("BackgroundWindow");

            if (backgroundAttributes.IsImage)
            {
                ImageBrush imgBrush = new ImageBrush();
                var fileName = Globals.LoadBG(TypeFond.ForCadre);
                if (!File.Exists(fileName))
                {
                    fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                }
                imgBrush.ImageSource = new BitmapImage(new Uri(fileName, UriKind.Absolute));
                imgBrush.Stretch = Stretch.Fill;
                Globals._currentChromakeyBG = "";
                backgroundPage.Background = imgBrush;
            }

            if (!backgroundAttributes.IsImage)
            {
                if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                {
                    var bgcolor = backgroundAttributes.BackgroundColor;
                    var bc = new BrushConverter();
                    this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                }
                
            }
            if (Globals.EventChosen == 1)
            {
                string[] imageToDelete = Directory.GetFiles(Globals.stripFrame);
                foreach (string image in imageToDelete)
                    File.Delete(image);
            }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                       
                        var viewModel = (VisualisationViewModel)DataContext;
                        Globals._FlagConfig = "client";
                        if (viewModel.GoToClientSumary.CanExecute(null))
                            viewModel.GoToClientSumary.Execute(null);
                        break;
                    case Key.F2:
                    
                        Globals._FlagConfig = "admin";
                        var viewModel2 = (VisualisationViewModel)DataContext;
                        if (viewModel2.GoToConnexionConfig.CanExecute(null))
                            viewModel2.GoToConnexionConfig.Execute(null);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            try
            {
                this.Focusable = true;
            Keyboard.Focus(this);
            if (Globals.EventChosen == 0)
            {
                pathOfImage = Globals.EventAssetFolder() + "\\Frames\\Cadre";
            }
            else if (Globals.EventChosen == 2)
            {
                pathOfImage = Globals.EventAssetFolder() + "\\Frames\\Polaroid";
            }
            else if (Globals.EventChosen == 1)
            {
                pathOfImage = Globals.EventAssetFolder() + "\\Frames\\Strip";
                }
            else if (Globals.EventChosen == 3)
            {

            }

            var imageFiles = new ArrayList();
            imageFiles = GetImageFileInfo(pathOfImage);
                var vm = (BackgroundWindowViewModel)DataContext;
                if (imageFiles.Count == 1)
            {
                
                string image = imageFiles[0].ToString();
                //vm.selectedBackground = image;
                Globals._currentBackground = image;
                INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
                int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
                if (webcam == 1)
                {
                    
                    if (vm.GoToTakeWecam.CanExecute(null))
                        vm.GoToTakeWecam.Execute(null);
                }
                else
                {
                    if (vm.GoToTakePhoto.CanExecute(null))
                        vm.GoToTakePhoto.Execute(null);
                }
                
            }
            else if (imageFiles.Count <= 0)
            {
                    string title = "Erreur";
                    string message = "L'image du cadre est manquant!!";
                    bool result = CustomInformationBox.Prompt( title, message);
                    if (vm.GoToAccueil.CanExecute(null))
                        vm.GoToAccueil.Execute(null);
            }
            else
            {
                    if (Globals.EventChosen == 1)
                    {
                        string[] imageStripFiles = Directory.GetFiles(Globals.originalStripFrame);
                        foreach (string image in imageStripFiles)
                        {
                            string newPath = System.IO.Path.GetFileName(image);
                            newPath = Globals.stripFrame + "\\" + newPath;
                            ImageUtility.divideBitmap(image, newPath);
                            pathOfImage = Globals.stripFrame;
                        }
                    }
                    _photos = new ImageCollection(pathOfImage);

                    ((CollectionViewSource)this.Resources["MyPhotos"]).Source = _photos;
                    ((CollectionViewSource)this.Resources["MyPhotos"]).View.MoveCurrentToPosition(-1);
                    //((CollectionViewSource)this.Resources["MyPhotos"]).Filter += new FilterEventHandler(view_Filter);
                    //listimages.ItemsSource = BuildItems(_photos);
                    int itemcount = _photos.Count;
                    if (itemcount <= 2)
                    {
                        listimages.Style = (Style)(this.Resources["horizontalStyle"]);
                        listimages.ItemContainerStyle = (Style)(this.Resources["HorizontalItemStyle"]);
                    }
                    else
                    {
                        listimages.Style = (Style)(this.Resources["verticalStyle"]);
                        listimages.ItemContainerStyle = (Style)(this.Resources["VerticalItemStyle"]);
                    }
                }

            
            

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

       

        private ArrayList GetImageFileInfo(string directory)
        {
            var vm = (BackgroundWindowViewModel)DataContext;
            var imageFiles = new ArrayList();
            var temp = directory;
            var manager = new Manager.ImageFilterManager();
            var files = Directory.GetFiles(temp);

            foreach (var image in files)
            {
                string file_name = System.IO.Path.GetFileName(image);
                if (file_name == "file_frame.png" && imageFiles.Count <= 0)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
                else if(file_name== "file_frame.jpg" && imageFiles.Count <= 0)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
               
            }

            return imageFiles;
        }

        private void SelectedFilter(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var vm = (BackgroundWindowViewModel)DataContext;
            if (vm.GoToTakePhoto.CanExecute(null))
                vm.GoToTakePhoto.Execute(null);
        }

        public bool GetCursorPos(int xmin, int xmax, int ymin, int ymax)
        {
            bool result = false;
            //get the mouse position and show on the TextBlock
            //System.Drawing.Point p = System.Windows.Forms.Cursor.Position;
            System.Windows.Point pointToWindow = Mouse.GetPosition(this);
            System.Windows.Point pointToScreen = PointToScreen(pointToWindow);
            if (pointToScreen.X >= xmin && pointToScreen.X <= xmax && pointToScreen.Y >= ymin && pointToScreen.Y <= ymax)
            {
                result = true;
            }
            else
                result = false;

            return result;
        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            //var  selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as ListBoxItem);
            INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
            int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
            if (webcam == 1)
            {
                var selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                var vm = (BackgroundWindowViewModel)DataContext;
                vm.selectedBackground = selectedImage.Source.ToString();
                Globals._currentBackground = selectedImage.Source.ToString();
                if (vm.GoToTakeWecam.CanExecute(null))
                    vm.GoToTakeWecam.Execute(null);
            }
            else
            {
                var selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                var vm = (BackgroundWindowViewModel)DataContext;
                vm.selectedBackground = selectedImage.Source.ToString();
                Globals._currentBackground = selectedImage.Source.ToString();
                if (vm.GoToTakePhoto.CanExecute(null))
                    vm.GoToTakePhoto.Execute(null);
            }
        }

        private System.Collections.IEnumerable BuildItems(ImageCollection source)
        {
            bool twoItems = false;
            if(source.Count == 2)
            {
                foreach (var image in source)
                {
                    int width = Convert.ToInt32(image.Image.Width);
                    int height = Convert.ToInt32(image.Image.Height);
                    if (width < height)
                    {
                        twoItems = true;
                    }
                }
                foreach (var image in source)
                {
                    int width = Convert.ToInt32(image.Image.Width);
                    int height = Convert.ToInt32(image.Image.Height);
                    if (width > height && twoItems)
                    {
                        width = width + (width / 2);
                        height = height + (height / 3);
                        yield return new ItemData(height, width, image);
                    }
                }

            }
            else
            {
                foreach (var image in source)
                {
                    int width = Convert.ToInt32(image.Image.Width);
                    int height = Convert.ToInt32(image.Image.Height);
                    if (width > height && twoItems)
                    {
                        width = width + (width / 2);
                        height = height + (height / 3);
                        yield return new ItemData(Convert.ToInt32(image.Image.Height), Convert.ToInt32(image.Image.Width), image);
                    }
                }
            }
           
            //yield return new ItemData(20, 200, "First");
            //yield return new ItemData(40, 300, "Second");
            //yield return new ItemData(60, 100, "Third");
        }
    }

    public class ItemData
    {
        public int ItemHeight { get; private set; }
        public int ItemWidth { get; private set; }
        public Selfizee.Models.Images image { get; private set; }
        public ItemData(int itemHeight, int itemWidth, Selfizee.Models.Images itemImage)
        {
            ItemHeight = itemHeight;
            ItemWidth = itemWidth;
            image = itemImage;
        }
    }
}
