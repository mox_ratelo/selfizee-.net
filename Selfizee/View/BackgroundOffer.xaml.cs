﻿using Selfizee.Managers;
using Selfizee.Models;
using Selfizee.Models.Enum;
using Selfizee.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for BackgroundOffer.xaml
    /// </summary>
    public partial class BackgroundOffer : UserControl
    {

        private ImageCollection _photos;
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private string currDir = Globals.currDir;
        private string pathOfImage = "";
        int currentPageIndex = 0;
        int itemPerPage = 6;
        int totalPage = 0;
        public BackgroundOffer()
        {
            InitializeComponent();
            try
            {
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                Globals._currentBackground = "";
                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("BackgroundWindow");

                if (backgroundAttributes.IsImage)
                {
                    ImageBrush imgBrush = new ImageBrush();
                    var fileName = Globals.LoadBG(TypeFond.ForCadre);
                    if (!File.Exists(fileName))
                    {
                        fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                    }
                    imgBrush.ImageSource = new BitmapImage(new Uri(fileName, UriKind.Absolute));
                    imgBrush.Stretch = Stretch.Fill;
                    Globals._currentChromakeyBG = "";
                    backgroundPage.Background = imgBrush;
                }

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }

                }
                if (Globals.EventChosen == 1)
                {
                    string[] imageToDelete = Directory.GetFiles(Globals.stripFrame);
                    foreach (string image in imageToDelete)
                        File.Delete(image);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
            {
                switch (e.Key)
                {
                    case Key.F1:

                        var viewModel = (VisualisationViewModel)DataContext;
                        Globals._FlagConfig = "client";
                        if (viewModel.GoToClientSumary.CanExecute(null))
                            viewModel.GoToClientSumary.Execute(null);
                        break;
                    case Key.F2:

                        Globals._FlagConfig = "admin";
                        var viewModel2 = (VisualisationViewModel)DataContext;
                        if (viewModel2.GoToConnexionConfig.CanExecute(null))
                            viewModel2.GoToConnexionConfig.Execute(null);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            //var  selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as ListBoxItem);
            INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
            int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
            if (webcam == 1)
            {
                var selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                var vm = (BackgroundOfferViewModel)DataContext;
                vm.selectedBackground = selectedImage.Source.ToString();
                Globals._currentBackground = selectedImage.Source.ToString();
                if (vm.GoToTakeWecam.CanExecute(null))
                    vm.GoToTakeWecam.Execute(null);
            }
            else
            {
                var selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                var vm = (BackgroundOfferViewModel)DataContext;
                vm.selectedBackground = selectedImage.Source.ToString();
                Globals._currentBackground = selectedImage.Source.ToString();
                if (vm.GoToTakePhoto.CanExecute(null))
                    vm.GoToTakePhoto.Execute(null);
            }
        }

        private void btnNext_click(object sender, RoutedEventArgs e)
        {
            // Display next page
            if (currentPageIndex < totalPage - 1)
            {
                currentPageIndex++;
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }

        private void btnPreview_click(object sender, RoutedEventArgs e)
        {
            // Display previous page
            if (currentPageIndex > 0)
            {
                currentPageIndex--;
                ((CollectionViewSource)this.Resources["MyPhotos"]).View.Refresh();
            }
            e.Handled = true;
        }

        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            Globals._FlagConfig = "client";
            var viewModel = (BackgroundOfferViewModel)DataContext;
            if (viewModel != null)
            {
                if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }
            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            try
            {
                this.Focusable = true;
                Keyboard.Focus(this);
                pathOfImage = Globals.EventAssetFolder() + "\\Frames\\Cadre\\Offer";
                var imageFiles = new ArrayList();
                imageFiles = GetImageFileInfo(pathOfImage);
                var vm = (BackgroundOfferViewModel)DataContext;
                if (imageFiles.Count == 1)
                {
                   
                    string image = imageFiles[0].ToString();
                    //vm.selectedBackground = image;
                    Globals._currentBackground = image;
                    INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
                    int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
                    if (webcam == 1)
                    {

                        if (vm.GoToTakeWecam.CanExecute(null))
                            vm.GoToTakeWecam.Execute(null);
                    }
                    else
                    {
                        if (vm.GoToTakePhoto.CanExecute(null))
                            vm.GoToTakePhoto.Execute(null);
                    }

                }
                else if(imageFiles.Count <= 0)
                {
                    string title = "Erreur";
                    string message = "L'image du cadre est manquant!!";
                    bool result = CustomInformationBox.Prompt(message, title);
                    if (vm.GoToAccueil.CanExecute(null))
                        vm.GoToAccueil.Execute(null);
                }
                else
                {
                   
                        _photos = new ImageCollection(pathOfImage);

                        ((CollectionViewSource)this.Resources["MyPhotos"]).Source = _photos;
                        ((CollectionViewSource)this.Resources["MyPhotos"]).View.MoveCurrentToPosition(-1);
                        //((CollectionViewSource)this.Resources["MyPhotos"]).Filter += new FilterEventHandler(view_Filter);

                        int itemcount = _photos.Count;

                        // Calculate the total pages
                        totalPage = itemcount / itemPerPage;
                        if (itemcount % itemPerPage != 0)
                        {
                            totalPage += 1;
                        }

                        string btnleft = "";
                        if (File.Exists(Globals._arrowLeft))
                        {
                            btnleft = Globals._arrowLeft;
                        }

                        string btnright = Globals._arrowRight;

                }




            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }



        private ArrayList GetImageFileInfo(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var vm = (BackgroundOfferViewModel)DataContext;
            var imageFiles = new ArrayList();
            var temp = directory;
            var manager = new Manager.ImageFilterManager();
            var files = Directory.GetFiles(temp);

            foreach (var image in files)
            {
                
                //string file_name = System.IO.Path.GetFileName(image);
                //if (file_name == "file_frame.png")
                //{
                //    var info = new FileInfo(image);
                //    imageFiles.Add(info);
                //}
                //else if (file_name == "file_frame.jpg")
                //{
                //    var info = new FileInfo(image);
                //    imageFiles.Add(info);
                //}
                var info = new FileInfo(image);
                imageFiles.Add(info);
            }
            if(imageFiles.Count <= 0)
            {
                string filename = "C:\\Events\\Assets\\" + Globals.GetEventId() + "\\Frames\\Cadre\\file_frame.png";
                if (File.Exists(filename))
                {
                    var info = new FileInfo(filename);
                    imageFiles.Add(info);
                }
            }
            return imageFiles;
        }

    }
}
