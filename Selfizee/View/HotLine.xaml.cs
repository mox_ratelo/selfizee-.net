﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for HotLine.xaml
    /// </summary>
    public partial class HotLine : UserControl
    {
        private DispatcherTimer timerCountdown;
        private int countdown = 60;
        public HotLine()
        {
            InitializeComponent();
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            string status = getPrinterStatus(getprinterName());
            status = "Statut imprimante : " + status;
            string lblNbPrinted = "";
            int nbPrinted = 0;
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            nbPrinted = mgrBin.readIntData(binFileName);
            if (nbPrinted <= 0)
            {
                lblNbPrinted = "Aucune impression effectuée";
            }
            else
            {
                lblNbPrinted = (nbPrinted > 1) ? $"{nbPrinted} photos déjà imprimées" : $"{nbPrinted} photo imprimée";

            }
            printerStatus.Text = status;
            printedPhotos.Text = lblNbPrinted;
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        var viewModel = (HotLineViewModel)DataContext;
                        if (viewModel.GoToConnexionConfig.CanExecute(null))
                            viewModel.GoToConnexionConfig.Execute(null);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            BitmapImage bitmapSelfizee = new BitmapImage();
            bitmapSelfizee.BeginInit();
            bitmapSelfizee.UriSource = new Uri(Globals._defaultImageSelfizee);
            bitmapSelfizee.EndInit();
            BitmapImage bitmapPrinter = new BitmapImage();
            bitmapPrinter.BeginInit();
            bitmapPrinter.UriSource = new Uri(Globals._defaultImagePrinter);
            bitmapPrinter.EndInit();
            BitmapImage bitmapHotline = new BitmapImage();
            bitmapHotline.BeginInit();
            bitmapHotline.UriSource = new Uri(Globals._defaultImageHotLine);
            bitmapHotline.EndInit();
            printerImage.Source = bitmapPrinter;
            hotlineImage.Source = bitmapHotline;
            SelfizeeImage.Source = bitmapSelfizee;
            int leftImage = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            ImageBrush arrowBrush = new ImageBrush();
            arrowBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultimageArrow, UriKind.Relative));
            vbtnBack.Background = arrowBrush;
            //stackSelfizee.Width = leftImage;
            //leftImage -= Convert.ToInt32(SelfizeeImage.Width);
            //leftImage -= 20;
            ////Canvas.SetRight(SelfizeeImage, leftImage);
            //SelfizeeImage.Margin = new Thickness(leftImage,10,0,0);
            LaunchTimerCountdown();
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            countdown--;
            if (countdown == 0)
            {
                lbl_CountDown.Content = "";
            }
            else
            {
                lbl_CountDown.Content = countdown;
            }

            if (countdown == -1)
            {
                lbl_CountDown.Content = "";
                timerCountdown.Stop();
                var vm = (HotLineViewModel)DataContext;
                if (vm.GoToVisualisation.CanExecute(null))
                    vm.GoToVisualisation.Execute(null);
            }
        }

        private string getprinterName()
        {
            string printerName;
            var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            //printerName = inimanager.GetSetting("PRINTING", "printerName");
            //printerName = printerName.Replace('"', ' ');
            //printerName = printerName.Replace('\\', ' ');
            //printerName = printerName.TrimEnd().TrimStart();

            //if (printerName.ToUpper() == "DEFAULT")
            //{
                printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;
            //}
            return printerName;
        }

        private string getPrinterStatus(string printername)
        {
            string status = "";
            ManagementObjectSearcher searcher = new
            ManagementObjectSearcher("SELECT * FROM Win32_Printer");

            string printerName = "";
            foreach (ManagementObject printer in searcher.Get())
            {
                printerName = printer["Name"].ToString().ToLower();
                if (printerName.ToLower().Equals(printername.ToLower()))
                {
                    Console.WriteLine("Printer = " + printer["Name"]);
                    if (printer["WorkOffline"].ToString().ToLower().Equals("true"))
                    {
                        // printer is offline by user
                        status = "Non connecté";
                    }
                    else
                    {
                        // printer is not offline
                        status = "Connectée";
                    }
                }
            }
            return status;
        }
    

        
        private void GoBack(object sender, RoutedEventArgs e)
        {
            timerCountdown.Stop();
            var vm = (HotLineViewModel)DataContext;
            if (vm.GoToVisualisation.CanExecute(null))
                vm.GoToVisualisation.Execute(null);
        }
        }
}
