﻿using log4net;
using Selfizee;
using Selfizee.Managers;
using Selfizee.Models;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for ConfigsPhotosDetails.xaml
    /// </summary>
    public partial class ConfigsPhotosDetails : UserControl
    {
        private ImageCollection _photos;
        private string mediaDataPath;
        private int selected = 0;
        private bool selectAllChecked = false;
        private bool strip = false;
        private int begin = 0;
        private string toPrint = "";
        private ObservableCollection<ImageItemViewModel> toDelete = new ObservableCollection<ImageItemViewModel>();
        private int end = 0;
        private string[] imageFiles = new string[0];
        private bool multiselectionMode = false;
        public string code { get; set; }
        private string photosDirectory = string.Empty;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Wifi wifi;
        private DispatcherTimer timerCountdown;
        public int timeout;
        string push = "";

        private double cumulativeDeltaX;
        private double cumulativeDeltaY;
        private double linearVelocity;

        public ConfigsPhotosDetails()
        {
            wifi = new Wifi();
            InitializeComponent();
            selectAllChecked = false;
            multiselectionMode = false;
            toDelete.Clear();
            lstImages.SelectionMode = SelectionMode.Single;
            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;
            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Absolute));
            //_back.Background = btnback;

            //List_event.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
            //Add_event.Foreground = System.Windows.Media.Brushes.White;
            code = Globals.codeEvent_toEdit;
            var brushCroix = new ImageBrush();
            mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix, UriKind.Relative));
            cancelCopie.Background = brushCroix;

            //Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            //ImageBrush btnLaunchAnimBrush = new ImageBrush();
            //btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
            //btn_lanchAnimation.Background = btnLaunchAnimBrush;

            ImageBrush btndeletephotosBrush = new ImageBrush();
            btndeletephotosBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultdeletephotosconfig, UriKind.Absolute));
            //delete_photos.Background = btndeletephotosBrush;

            var brushTB = new ImageBrush();
            brushTB.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnTB, UriKind.Relative));
            //btnTB.Background = brushTB;

            var brushRetour = new ImageBrush();
            brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAnim, UriKind.Relative));
            //btnRetour.Background = brushRetour;

            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);

            //ImageBrush btnback = new ImageBrush();
            //btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConfigBack, UriKind.Absolute));
            //btn_Back.Background = btnback;

            //ImageBrush btnbackToEvent = new ImageBrush();
            //btnbackToEvent.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnBackToEvent, UriKind.Absolute));
            //btn_BackToEvent.Background = btnbackToEvent;

            var brushNext = new ImageBrush();
            brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhiteNext, UriKind.Relative));
            Next.Background = brushNext;

            var brushPrevious = new ImageBrush();
            brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhitePrevious, UriKind.Relative));
            Previous.Background = brushPrevious;

            var brushdelete = new ImageBrush();
            brushdelete.ImageSource = new BitmapImage(new Uri(Globals._defaultdeleteImage, UriKind.Relative));
            delete.Background = brushdelete;

            var brushcancel = new ImageBrush();
            brushcancel.ImageSource = new BitmapImage(new Uri(Globals._defaultcanceldelete, UriKind.Relative));
            Cancel.Background = brushcancel;

            cancelCopieDelete.Background = brushCroix;

            var brushdeletePhoto = new ImageBrush();
            brushdeletePhoto.ImageSource = new BitmapImage(new Uri(Globals._defaultdeleteconfig, UriKind.Relative));
            deletePhoto.Background = brushdeletePhoto;
            var brushPrint = new ImageBrush();
            brushPrint.ImageSource = new BitmapImage(new Uri(Globals._defaultprintconfig, UriKind.Relative));
            print.Background = brushPrint;

            Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);


            // scrollCopie.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            // scrollCopie.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            // scrollDelete.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            //lstImages.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width - 80;

            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
            if (_print <= 0)
            {
                print.IsEnabled = false;
            }
            if (Globals.ScreenType == "SPHERIK")
            {
                txtDate.Margin = new Thickness(5, 50, 5, 5);
                img_View.Width = 900;
                img_View.Height = 500;
            }

            setTimeout();
            photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            
            imgCercle.Visibility = Visibility.Hidden;
           // LoadingPanel.ClosePanel();
            //LaunchTimerCountdown();
        }
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            timerCountdown.Stop();
            imgCercle.Visibility = Visibility.Hidden;
            LaunchTimerAfter2();
        }
        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }

        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(ConfigsPhotosDetails));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }

        private void Configuration_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToConfiguration.CanExecute(null))
                viewModel.GoToConfiguration.Execute(null);
        }

        private void btnSelectOneByOne_Click(object sender, RoutedEventArgs e)
        {
            if(btnSeslectAll.Content.ToString() == "SÉLECTIONNER DES PHOTOS")
            {
                timerCountdown.Stop();
                multiselectionMode = true;
                lstImages.SelectionMode = SelectionMode.Multiple;
                btnSeslectAll.Content = "ANNULER";
                selectAll_photos.Visibility = Visibility.Visible;
                deselectAll_photos.Visibility = Visibility.Visible;
                delete_photos.Visibility = Visibility.Visible;
            }
            else
            {
                timerCountdown.Start();
                lstImages.SelectedItems.Clear();
                toDelete.Clear();
                selectAllChecked = false;
                multiselectionMode = false;
                lstImages.SelectionMode = SelectionMode.Single;
                btnSeslectAll.Content = "SÉLECTIONNER DES PHOTOS";
                delete_photos.Visibility = Visibility.Hidden;
                selectAll_photos.Visibility = Visibility.Hidden;
                deselectAll_photos.Visibility = Visibility.Hidden;
            }
            
            //var Items = lstImages.Items;
            //foreach (ImageItemViewModel item in Items)
            //{
            //    item.IsChecked = true;
            //}
        }

        private void btnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            lstImages.SelectedItems.Clear();
            toDelete.Clear();
            selectAllChecked = true;
            foreach (var item in lstImages.Items)
            {
                ImageItemViewModel item_object = (ImageItemViewModel)item;
                lstImages.SelectedItems.Add(item);
                toDelete.Add(item_object);
            }
        }
        private void btnDeselectAll_Click(object sender, RoutedEventArgs e)
        {
            lstImages.SelectedItems.Clear();
            toDelete.Clear();
            selectAllChecked = false;
        }

        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter le logiciel, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void LoadLstImages(string pathDirectory)
        {

            this.Dispatcher.BeginInvoke((Action)(() =>
            {

                var result = new ObservableCollection<ImageItemViewModel>();
                if (!string.IsNullOrEmpty(pathDirectory))
                {
                    getFiles(pathDirectory);
                    foreach (string myFile in Directory.GetFiles(pathDirectory))
                    {
                        System.Windows.Controls.Image myLocalImage = new System.Windows.Controls.Image();

                        BitmapImage myImageSource = new BitmapImage();
                        myImageSource.BeginInit();
                        myImageSource.CacheOption = BitmapCacheOption.OnLoad;
                        myImageSource.UriSource = new Uri(@"file:///" + myFile);
                        myImageSource.DecodePixelHeight = 200;
                        myImageSource.EndInit();
                        myLocalImage.Source = myImageSource;

                        //var path = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath);

                        //result.Add(myLocalImage);
                        result.Add(new ImageItemViewModel { ImageItem = myImageSource, IsChecked = false, ImagePath = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath) });
                    }
                }
                lstImages.ItemsSource = result;
            }));

            
          }


        private void Printing(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;
                    //string binPath;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    //if (printerName != "DEFAULT")
                    //    printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    
                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));  
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    //lbl_CurrentPrinting.Content = "";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }

        public void removePhoto(string path)
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(path);   
        }

        public void removeAllPhoto(string directory)
        {
            foreach(var file in Directory.GetFiles(directory))
            {
                removePhoto(file);
            }
        }

        public void removeAllLines(string path)
        {
            if(File.Exists(path))
            {
                List<String> lines = new List<string>();
                string line;
                using (System.IO.StreamReader file = new System.IO.StreamReader(path))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
                for (int i = 0; i < lines.Count; i++)
                {
                    if (i > 0)
                    {
                        lines[i] = "";
                    }
                }
                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(path))
                {
                    outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
                }
            }
        }

        private void removeRowOnCSV(string csvpath,string filePath)
        {
            if (File.Exists(csvpath))
            {
                string identity = System.IO.Path.GetFileNameWithoutExtension(filePath);
                List<String> lines = new List<string>();
                string line;

                using (System.IO.StreamReader file = new System.IO.StreamReader(csvpath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }

                lines.RemoveAll(l => l.Contains(identity));
                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(csvpath))
                {
                    outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
                }
            }
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void deletePhoto_click(object sender, RoutedEventArgs e)
        {
            string title = "Confirmation";
            string message = "Voulez-vous vraiment supprimer les photos sélectionnées?";
            bool result = CustomMessageBox.Prompt(message, title);
            //MessageBoxResult messageBoxResult = MessageBox.Show(, "Confirmation", MessageBoxButton.YesNo,MessageBoxImage.Question);
            if (result)
            {
                string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";
                //var path = toPrint;
                removeRowOnCSV(csvPath, toPrint);
                removePhoto(toPrint);
                LoadLstImages(photosDirectory);

                /*****Close scrollCopie (ScrollViewer)****/
                copieCancel(sender, e);
            }


        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
                    int toWrite = mgrBin.readIntData(binFileName);
                    toWrite += Convert.ToInt32(1);
                    writeTxtFileData(mediaDataPath, toWrite.ToString());
                    mgrBin.writeData(toWrite, binFileName);
                    Printing(false);
              

        }

        private void writeTxtFileData(string filename, string toWrite)
        {
            //int i = 0;

            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint); 
            if (bmp.Height < bmp.Width) 
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void BackToEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void GotoAdminSumary_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (Globals.ScreenType == "SPHERIK")
            {
                if (viewModel.GoToAdminSumarySpherik.CanExecute(null))
                    viewModel.GoToAdminSumarySpherik.Execute(null);
            }
            else if (Globals.ScreenType == "DEFAULT")
            {
                if (viewModel.GoToAdminSumary.CanExecute(null))
                    viewModel.GoToAdminSumary.Execute(null);
            }
        }
        private void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;

            if (viewModel.GoToAdminSumary.CanExecute(null))
                viewModel.GoToAdminSumary.Execute(null);
        }
        private void btnAfficher_Click(object sender, RoutedEventArgs e)
        {
            var listeImages = (ObservableCollection<ImageItemViewModel>)lstImages.ItemsSource;
            var nbImageSelected = listeImages.Where(x => x.IsChecked).ToList().Count;
            if(nbImageSelected == 1)
            {
                    
                    this.Opacity = 1;
                    var selectedImage = listeImages.Where(x => x.IsChecked).FirstOrDefault(); 
                    ShowImage(selectedImage.ImagePath);
                    dockGrid.IsEnabled = false;
                    DateTime creation = File.GetCreationTime(selectedImage.ImagePath);
                    string day = creation.Day.ToString();
                    string month = creation.Month.ToString();
                    string year = creation.Year.ToString();
                    string hour = creation.Hour.ToString();
                    string minutes = creation.Minute.ToString();
                    if (day.Length == 1) day = "0" + day;
                    if (month.Length == 1) month = "0" + month;
                    if (hour.Length == 1) hour = "0" + hour;
                    if (minutes.Length == 1) minutes = "0" + minutes;
                    txtDate.Text = GetDatePhoto(selectedImage.ImagePath);
                    scrollCopie.Visibility = Visibility.Visible;
                
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner une photo!", "Information", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private string GetDatePhoto(string pathPhoto)
        {
            DateTime creation = File.GetCreationTime(pathPhoto);
            string day = creation.Day.ToString();
            string month = creation.Month.ToString();
            string year = creation.Year.ToString();
            string hour = creation.Hour.ToString();
            string minutes = creation.Minute.ToString();
            if (day.Length == 1) day = "0" + day;
            if (month.Length == 1) month = "0" + month;
            if (hour.Length == 1) hour = "0" + hour;
            if (minutes.Length == 1) minutes = "0" + minutes;
            return "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            //bool selectedExists = false;
            //var listeImages = (ObservableCollection<ImageItemViewModel>)lstImages.ItemsSource;
            //selectedExists = listeImages.Any(x => x.IsChecked);
            if (toDelete.Count > 0) {
                delete_selectedImages();
                //dockGrid.IsEnabled = false;
                //scrollDelete.Visibility = Visibility.Visible;
            }
            else
            {
                string title = "Information";
                string message = "Veuillez sélectionner les photos à supprimer!";
                CustomInformationBox.Prompt(title, message);
            }

        }

        private void delete_selectedImages()
        {
            string title = "Confirmation";
            string message = "Voulez-vous vraiment supprimer les photos sélectionnées?";
            bool result = CustomMessageBox.Prompt(title, message);
            if (result)
            {
                string photoPath = "C:\\EVENTS\\Media\\" + code + "\\Photos";
                string photoOriginalPath = "C:\\EVENTS\\Media\\" + code + "\\Photos\\Original";
                string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";

                foreach (ImageItemViewModel item in toDelete)
                {
                        removePhoto(item.ImagePath);
                        removeRowOnCSV(csvPath, item.ImagePath);
                        var photoToOriginal = $"{photoOriginalPath}\\{System.IO.Path.GetFileName(item.ImagePath)}";
                        removePhoto(photoToOriginal);
                }


                LoadLstImages(photoPath);
            }
        }

        private void deleteFilesPhotos(object sender, RoutedEventArgs e)
        {
            string photoPath = "C:\\EVENTS\\Media\\" + code + "\\Photos";
            string photoOriginalPath = "C:\\EVENTS\\Media\\" + code + "\\Photos\\Original";
            string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";

            foreach (ImageItemViewModel item in lstImages.ItemsSource) {
                if (item.IsChecked)
                {
                    removePhoto(item.ImagePath);
                    removeRowOnCSV(csvPath, item.ImagePath);
                    var photoToOriginal = $"{photoOriginalPath}\\{System.IO.Path.GetFileName(item.ImagePath)}";
                    removePhoto(photoToOriginal);
                }
            }


            LoadLstImages(photoPath);            
            dockGrid.IsEnabled = true;
            scrollDelete.Visibility = Visibility.Collapsed;
            //nb_Photos.Text = "0 PHOTO";           
        }
        private void getFiles(string photosDirectory)
        {
            int index = 0;
            var getFilesDirectory = Directory.GetFiles(photosDirectory);
            foreach (var image in getFilesDirectory)
            {
                index++;
                System.Array.Resize(ref imageFiles, index);
                imageFiles[index-1] = image;
            }
            this.selected = 0;
            this.begin = 0;
            this.end = imageFiles.Length;

            ChangeLblPhotos(getFilesDirectory.Length);
            if(getFilesDirectory.Length > 0)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    btnsSuppression.Visibility = Visibility.Visible;
                }));
                
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    btnsSuppression.Visibility = Visibility.Collapsed;
                }));
               
            }

        }

        private void ChangeLblPhotos(int nbPhotos)
        {
            if (nbPhotos > 1)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    nb_Photos.Content = $"{nbPhotos} PHOTOS";
                }));
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    nb_Photos.Content = $"{nbPhotos} PHOTO";
                }));
               
            }
        }

        public void ShowImage(string path)
        {
            //selected = Array.IndexOf(imageFiles, path);
            Bitmap imageBitmap = new Bitmap(path);
            try
            {
                img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                var actual_Width = img_View.Width;
                var actual_Height = img_View.Height;
                var x = (int)SystemParameters.PrimaryScreenWidth * 50 / 100;
                var y = (int)SystemParameters.PrimaryScreenHeight * 50 / 100;

                if (actual_Width > actual_Height)
                {
                    img_View.Height = img_View.Height * x / img_View.Width;
                    img_View.Width = x;
                }
                else if (actual_Width < actual_Height)
                {
                    img_View.Width = img_View.Width * y / img_View.Height;
                    img_View.Height = y;
                }
                else
                {
                    img_View.Width = x;
                    img_View.Height = x;
                }
            }
            catch(Exception e)
            {
                int ie = 1;
            }
           
                if (selected == 0)
                {
                    Previous.Visibility = Visibility.Hidden;
                }
                else
                {
                    Previous.Visibility = Visibility.Visible;
                }
            
                if (selected == imageFiles.Length - 1)
                {
                    Next.Visibility = Visibility.Hidden;
                }
                else
                {
                    Next.Visibility = Visibility.Visible;
                }

                
        }

        private void ShowPrevImage()
        {
            var pathPhoto = this.imageFiles[(--this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ShowNextImage();
            if(Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
            
        }

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }
            
        }

        /// <summary>
        /// Show the next image.
        /// </summary>
        private void ShowNextImage()
        {
            //ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(++this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }

        private void copieCancel(object sender, EventArgs e)
        {
            timerCountdown.Start();
            closeImageViewer();
        }

        private void closeImageViewer()
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
           // lstImages.Background = System.Windows.Media.Brushes.Transparent;
            scrollCopie.Visibility = Visibility.Collapsed;
            lstImages.Visibility = Visibility.Visible;
            //Deselectionner l'image de la ListBox pour pouvoir re-cliquer
            lstImages.SelectedIndex = -1;
        }

        private void copieCancelDelete(object sender, EventArgs e)
        {
            timerCountdown.Start();
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollDelete.Visibility = Visibility.Collapsed;
        }

        private void PlaceholdersListBox_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ImageItemViewModel;
            if (item != null)
            {
                if(item.IsChecked == false)
                {
                    int listIndex = -1;
                    if (toDelete.Count > 0)
                    {
                        var exist = toDelete.FirstOrDefault(x => x == item);
                        if (exist != null)
                        {
                            listIndex = toDelete.IndexOf(toDelete.Single(i => i == exist));
                        }
                        else listIndex = -1;
                        if(listIndex >= 0)
                        {
                            toDelete.RemoveAt(listIndex);
                        }
                    }
                }
            }
        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            strip = false;
            timerCountdown.Stop();
            lbl_CurrentPrinting.Content = "";

            ListBox currentList = (ListBox)sender;
            if (multiselectionMode && selectAllChecked==false)
            {
                List<ImageItemViewModel> _selectedImage = currentList.SelectedItems.Cast<ImageItemViewModel>().ToList();
                ImageItemViewModel _imageSelected = new ImageItemViewModel();
                if (currentList.SelectedItems.Count > 0)
                {
                    _imageSelected = (ImageItemViewModel)currentList.SelectedItems[currentList.SelectedItems.Count - 1];
                }
                
                HashSet <string> sentIDs = new HashSet<string>(_selectedImage.Select(s => s.ImagePath));
                var results = toDelete.Where(m => !sentIDs.Contains(m.ImagePath)).FirstOrDefault();
                int listIndex = -1;

                if (results != null)
                {
                     listIndex = toDelete.IndexOf(toDelete.Single(i => i == results));
                }
                else
                {
                    results = null;
                }

                if (results != null)
                {
                    if (listIndex >= 0)
                    {
                        toDelete.RemoveAt(listIndex);
                    }
                }
                else
                {
                    toDelete.Add(_imageSelected);
                }
            }
            else if(multiselectionMode==false && selectAllChecked == false)
            {
                var selectedImage = (currentList).SelectedIndex;
                if (selectedImage >= 0)
                {
                    this.Opacity = 1;
                    var item = (ImageItemViewModel)lstImages.Items[selectedImage];

                    //var listeImages = (ObservableCollection<ImageItemViewModel>)lstImage.ItemsSource;
                    //var item = listeImages.FindIndex(x => x.ImagePath == path);            
                    //selected = Array.IndexOf(listeImages, item);
                    //selected = lstImage.Select((c, i) => new { value = c, Index = i })
                    //                    .Where(x => x.value.ImagePath == item.ImagePath)
                    //                    .Select(x => x.Index).FirstOrDefault();
                   
                    selected = selectedImage;
                    toPrint = item.ImagePath;
                    ShowImage(toPrint);
                    currentList.Visibility = Visibility.Hidden; 
                    if (toPrint.ToUpper().Contains("STRIP"))
                    {
                        strip = true;
                    }
                    dockGrid.IsEnabled = false;
                    DateTime creation = File.GetCreationTime(toPrint);
                    string day = creation.Day.ToString();
                    string month = creation.Month.ToString();
                    string year = creation.Year.ToString();
                    string hour = creation.Hour.ToString();
                    string minutes = creation.Minute.ToString();
                    if (day.Length == 1) day = "0" + day;
                    if (month.Length == 1) month = "0" + month;
                    if (hour.Length == 1) hour = "0" + hour;
                    if (minutes.Length == 1) minutes = "0" + minutes;
                    txtDate.Text = "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
                 //   lstImages.Background = System.Windows.Media.Brushes.Transparent;
                    scrollCopie.Visibility = Visibility.Visible;
                }

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {

            //string iniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            //INIFileManager iniFile = new INIFileManager(iniPath);
            //string Name = iniFile.GetSetting("EVENT", "Name");         
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                    // lbl_loading.Visibility = Visibility.Visible;
                }));
                Thread.Sleep(100);

            }).ContinueWith(task =>
            {
                LoadLstImages(photosDirectory);
                LaunchTimerAfter2();
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ClosePanel();
                    //lbl_loading.Visibility = Visibility.Hidden;
                }));
            });

        }

        

        //////////////////////////////////////////////
        private void Printing1(bool sendMail)
        {
            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    //printerName = inimanager.GetSetting("PRINTING", "printerName");
                    //printerName = printerName.Replace('"', ' ');
                    //printerName = printerName.Replace('\\', ' ');
                    //printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    //if (printerName != "DEFAULT")
                    //    printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413);
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage1;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage1;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }

        private void Imprimer1(object sender, RoutedEventArgs e)
        {
            //GENERATE FILE PRINTER SETTINGS .bin
            PrintDocument pd = new PrintDocument();
            var fileNameSettings = "Settings.bin";
            if (SettingsPrinter.GetPrinterSettings(pd.PrinterSettings, fileNameSettings))
            {
                MessageBox.Show($"{fileNameSettings} a été bien créé!");
            }

            //SHOW DIALOG PRINTER SETTINGS
            //var printer = SettingsPrinter.OpenPrinterPropertiesDialog(pd.PrinterSettings);

            /***** BEGIN
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing1(false);
            END  *******/

        }

        private void PrintPage1(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }
        /////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////
        private void Printing2(bool sendMail)
        {
            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413);
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage2;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage2;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });
        }

        private void Imprimer2(object sender, RoutedEventArgs e)
        {
            //PrintDocument pd = new PrintDocument();
            //var fileNameSettings = "Settings2InchCut.bin";
            //if (SettingsPrinter.GetPrinterSettings(pd.PrinterSettings, fileNameSettings))
            //{
            //    MessageBox.Show($"{fileNameSettings} a été bien créé!");
            //}

            //BinaryFileManager mgrBin = new BinaryFileManager();
            //string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            //lbl_CurrentPrinting.Content = "Impression en cours....";
            //int toWrite = mgrBin.readIntData(binFileName);
            //toWrite += Convert.ToInt32(1);
            //writeTxtFileData(mediaDataPath, toWrite.ToString());
            //mgrBin.writeData(toWrite, binFileName);
            //Printing2(false);
        }

        private void PrintPage2(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void Printing3(bool sendMail)
        {


            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 413, 615); //PR6x4 - 2Inch-Cut **** new PaperSize("Custom 6x4", 615, 413);
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    //}
                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage3;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage3;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }


        private void Imprimer3(object sender, RoutedEventArgs e)
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing3(false);
        }

        private void PrintPage3(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }
        private void Printing4(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413);
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage4;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage4;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }

        private void Imprimer4(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing4(false);


        }

        private void PrintPage4(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void Printing5(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413);
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    //}
                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage5;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage5;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    //lbl_CurrentPrinting.Content = "";
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }

        private void Imprimer5(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing5(false);


        }

        private void PrintPage5(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }


        public void GotoAccueil()
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ConfigPhotosDetailsViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }
        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
               
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    timerCountdown.Stop();
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }
        public void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }
        void LaunchTimerAfter2()
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromMinutes(2);
            timerCountdown.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown.Start();
        }
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                timeout = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                timeout = 10;
            }


            if (Globals.timeOut < 0)
            {
                timeout *= -1;
            }
        }

        private void Next_Photo()
        {
            if(selected < imageFiles.Length)
            {
                ShowNextImage();
            }                
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
        }

        private void Previous_Photo()
        {
            if(selected > 0)
            {
                ShowPrevImage();
            }
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }
        }


        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                Next_Photo();
            }
            else
            {
                Previous_Photo();
            }
        }

        private void centerImageView_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = this;
            e.Handled = true;
        }

        private void centerImageView_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {

            //store values of horizontal & vertical cumulative translation

            cumulativeDeltaX = e.CumulativeManipulation.Translation.X;

            cumulativeDeltaY = e.CumulativeManipulation.Translation.Y;

            //store value of linear velocity into horizontal direction  

            linearVelocity = e.Velocities.LinearVelocity.X;
        }

        private void centerImageView_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
            e.ExpansionBehavior = new InertiaExpansionBehavior()
            {
                InitialVelocity = e.InitialVelocities.ExpansionVelocity,
                DesiredDeceleration = 10.0 * 96.0 / 1000000.0
            };
        }

        private void centerImageView_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            //check if this is swipe gesture
            if (cumulativeDeltaX < 0)
            {
                //show previous image
                Previous_Photo();
            }
            else
            {
                //show next image
                Next_Photo();
            }
        }
    }
}
