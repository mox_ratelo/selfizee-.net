﻿using Selfizee.Managers;
using Selfizee.Models;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Printing;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for EditEvent.xaml
    /// </summary>
    public partial class EditEvent : UserControl
    {
        private ImageCollection _photos;
        private string mediaDataPath  = "";
        private string toPrint = "";
        private int selected = 0;
        private int begin = 0;
        private int end = 0;
        string ftpServerIP = "37.187.132.132";        
        FtpManager _ftpManager = new FtpManager();
        string idborne = "";
        private string photosDirectory = string.Empty;
        private string[] imageFiles = new string[0];
        private List<ModifyEvent> lstCoordonate = new List<ModifyEvent>();
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        string ftpUserName = "dev@upload.selfizee.fr";
        string ftpPassword = "qB06JE0K#PvVB4^j7Ahxq";
        string ftpUserNameEvent = "upload@syncdev.selfizee.fr"; //"syncdev"; //"sync@uploadv2.selfizee.fr";
        string ftpPasswordEvent = "^G_yvAY@n~$A}}op<B(*JLN6"; //"T2=-rB1yn}MJ?~D?Yb51n$4M"; //"S99e)%VxI?q*5*q_$6zK/8k/";
        public string code { get; set; }
        private static Wifi wifi;

        public double progress;


        public EditEvent()
        {
            wifi = new Wifi();
            InitializeComponent();
            
            DeleteEvent();

            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
            List_event.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
            Add_event.Foreground = System.Windows.Media.Brushes.White;
            var _uriSource = new Uri(Globals._defaultNoneImage, UriKind.Relative);
            for (int i = 0; i < 8; i++)
            {
                //System.Windows.Controls.Image imgTemp = new System.Windows.Controls.Image();
                
                ModifyEvent mdfEvent = new ModifyEvent();
                mdfEvent.Image = Globals._defaultNoneImage;
                mdfEvent.Date = "12/10/2017 à 14h11";
                mdfEvent.Email = "johndoe@gmail.com";
                mdfEvent.Optin = "OUI";
                mdfEvent.Tel = "06 12 13 14 15";
                lstCoordonate.Add(mdfEvent);
            }
            _photos = new ImageCollection();
            code = Globals.codeEvent_toEdit;
            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConfigBack, UriKind.Absolute));
            btn_Back.Background = btnback;

            ImageBrush btndelete = new ImageBrush();
            btndelete.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDeleteContacts, UriKind.Absolute));
            delete_contacts.Background = btndelete;
            nb_photos.Text = getNbPhotos().ToString();
            nb_impressions.Text = getNbPrinted().ToString();

            //if(getNbPrinted() == 1 || getNbPrinted()==0 )
            //{
            //    lbl_Impression.Text = "Impression";
            //}

            //if (getNbPhotos() == 1 || getNbPhotos() == 0)
            //{
            //    lbl_photos.Text = "Photo";
            //}
            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            cancelCopie.Background = brushCroix;
            CancelReinitCross.Background = brushCroix;

            var brushNext = new ImageBrush();
            brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultnext, UriKind.Relative));
            Next.Background = brushNext;

            var brushPrevious = new ImageBrush();
            brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultprevious, UriKind.Relative));
            Previous.Background = brushPrevious;

            var brushdelete = new ImageBrush();
            brushdelete.ImageSource = new BitmapImage(new Uri(Globals._defaultdeleteImage, UriKind.Relative));
            delete.Background = brushdelete;

            var brushPrint = new ImageBrush();
            brushPrint.ImageSource = new BitmapImage(new Uri(Globals._defaultprintconfig, UriKind.Relative));
            print.Background = brushPrint;

            var brushValider = new ImageBrush();
            brushValider.ImageSource = new BitmapImage(new Uri(Globals._defaultValidate, UriKind.Relative));
            ReinitPrintDocNumber.Background = brushValider;

            var brushAnnuler = new ImageBrush();
            brushAnnuler.ImageSource = new BitmapImage(new Uri(Globals._defaultcanceldelete, UriKind.Relative));
            Cancel.Background = brushAnnuler;

            scrollCopie.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollCopie.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            scrollReinit.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollReinit.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

            ShowHideAucunePhoto(getNbPhotos());
            ChangeLblPhotos(getNbPhotos());
            ChangeLblImpressions(getNbPrinted());
            ChangeLblContacts(int.Parse(nb_contacts.Text));

            ImageBrush btnRazPrinted = new ImageBrush();
            btnRazPrinted.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnResetPrinted, UriKind.Absolute));
            RAZ_Printed.Background = btnRazPrinted;
            mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
            btn_lanchAnimation.Background = btnLaunchAnimBrush;
            setCoordonate();
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
            if (_print <= 0)
            {
                print.IsEnabled = false;
            }
            setTimeLine();
            //setProgressValue();
            Synchro();
            photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            LoadLstImages(photosDirectory);
        }
        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }
        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(EditEvent));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }
        private void ShowPhotosStack_Click(object sender, MouseButtonEventArgs e)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                showLoader();
            }));
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    var viewModel = (EditEventViewModel)DataContext;
                    if (viewModel.GoToPhotos.CanExecute(null))
                        viewModel.GoToPhotos.Execute(null);
                }));
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_loading.Visibility = Visibility.Hidden;
                }));

            });
        }
        private void ShowCoordonateStack_Click(object sender, MouseButtonEventArgs e)
        {
            showLoader();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {

            }));
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    var viewModel = (EditEventViewModel)DataContext;
                    if (viewModel.GoToCoordonate.CanExecute(null))
                        viewModel.GoToCoordonate.Execute(null);
                }));
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_loading.Visibility = Visibility.Hidden;
                }));

            });
        }
        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
        private async Task<int> getProgressSynchroValue(string code)
        {
            //string localPath = Globals._MediaFolder + "\\" + code + "\\Photos";
            //string ftpDirectoryPathItem = $"ftp://{this.ftpServerIP}/{idborne}/files/{code}/Photos";
            string ftpDirectoryPathEvent = $"ftp://{ftpServerIP}/EVENTS/{code}";

            int remoteValue = 0;
            if (chk_con())
            {
                //var lstFileNotSynchronized = Compare.GetMissingFilesInFtpNotInLocalAndCount(localPath, ftpDirectoryPathItem, new NetworkCredential(ftpUserName, ftpPassword), out remoteValue);
                //var photos = await Compare.CountRemotePhotos(ftpDirectoryPathItem, new NetworkCredential(ftpUserName, ftpPassword));
                //int countRemotePhotos = photos.Count();
                var images = await Compare.CountRemoteImagesEvent(ftpDirectoryPathEvent, new NetworkCredential(ftpUserNameEvent, ftpPasswordEvent));
                //int countRemoteImagesEvent = images.Count();
                remoteValue = images.Count(); //countRemoteImagesEvent + countRemotePhotos;
            }
            return remoteValue;
        }
        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter l'application, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
        private bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        private int getProgressLocalValue(string code)
        {
            int nb = 0;
            //string pathPhotos = Globals._MediaFolder + "\\" + code + "\\Photos";
            string pathEvents = $"C:/EVENTS/Assets/{code}";

            //nb += Directory.Exists(pathPhotos) ? Directory.GetFiles(pathPhotos).Length : 0;
            nb += Directory.Exists(pathEvents) ? Directory.GetFiles($"C:/EVENTS/Assets/{code}", "*.*", SearchOption.AllDirectories).Count() : 0;

            return nb;
        }
        private void deletePhoto_click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment supprimer ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";

                removeRowOnCSV(csvPath);
                removePhoto(toPrint);
                LoadLstImages(photosDirectory);

                IsSynchro();

                /*****Close scrollCopie (ScrollViewer)****/
                copieCancel(sender, e);
            }
        }
        private void ChangeLblPhotos(int nbPhotos)
        {
            if(nbPhotos > 1)
            {
                lbl_photos.Text = "Photos";
            }
            else
            {
                lbl_photos.Text = "Photo";
            }
        }
        private void ShowHideAucunePhoto(int nbPhotos)
        {
            if (nbPhotos > 0)
                lbl_aucune_photo.Visibility = Visibility.Collapsed;
            else
                lbl_aucune_photo.Visibility = Visibility.Visible;
        }
        private void ChangeLblImpressions(int nbImpressions)
        {
            if (nbImpressions > 1)
            {
                lbl_Impression.Text = "Impressions";
            }
            else
            {
                lbl_Impression.Text = "Impression";
            }
        }
        private void ChangeLblContacts(int nbContacts)
        {
            if (nbContacts > 1)
            {
                lbl_contacts.Text = "Contacts";
            }
            else
            {
                lbl_contacts.Text = "Contact";
            }
        }
        private void writeTxtFileData(string filename, string toWrite)
        {
            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }        
        private void setTimeLine()
        {
            string path = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";
            DateTime histoDate= new DateTime();
            string curTime="";
            if (File.Exists(path))
            {
                DateTime? lastDate = null;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                //for (int i = 1; i < rows.Length; i++)
                for (int i = rows.Length-1; i > 0 ; i--)
                {
                    string[] tabText = rows[i].Split(',');

                    if (tabText == null || tabText.Count() < 8)
                        tabText = rows[i].Split(';');

                    if (tabText != null && tabText.Count() >= 8)
                    {
                        string dtPhoto = tabText[7];
                        string dtPrint = tabText[8];
                        string current = "";
                        string currentLib = "";
                        if (String.IsNullOrEmpty(dtPrint))
                        {
                            current = dtPhoto;
                            currentLib = "photo";
                            if (!string.IsNullOrEmpty(current))
                            {
                                Regex regex = new Regex(@"\s");
                                string[] splitted = regex.Split(current.ToLower());
                                curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                string curDate = splitted[0];

                                //DateTime histoDate = Convert.ToDateTime(curDate);
                                //DateTime histoDate = DateTime.Parse(curDate);
                                histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null);

                            }
                        }
                        else
                        {
                            current = dtPrint;
                            currentLib = "photo + impression";
                            if (!string.IsNullOrEmpty(current))
                            {
                                Regex regex = new Regex(@"\s");
                                string[] splitted = regex.Split(current.ToLower());
                                curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                string curDate = splitted[0];

                                //DateTime histoDate = Convert.ToDateTime(curDate);
                                //DateTime histoDate = DateTime.Parse(curDate);
                                histoDate = DateTime.ParseExact(curDate, "dd/MM/yyyy", null);

                            }
                        }

                        if (!lastDate.HasValue || lastDate != histoDate)
                            {
                                TextBlock dateHisto = new TextBlock();
                                dateHisto.Text = histoDate.ToLongDateString().UppercaseFirstEach();
                                dateHisto.Height = 15;
                                dateHisto.FontSize = 12;
                                dateHisto.Margin = new Thickness(20, 20, 0, 0);
                                dateHisto.FontWeight = FontWeights.Bold;
                                dateHisto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                                timelineStack.Children.Add(dateHisto);
                                lastDate = histoDate;
                            }
                            TextBlock infoHisto = new TextBlock();
                            infoHisto.Text = curTime + "  " + currentLib;
                            infoHisto.Height = 15;
                            infoHisto.FontSize = 12;
                            infoHisto.Margin = new Thickness(50, 20, 0, 0);
                            infoHisto.FontWeight = FontWeights.Bold;
                            infoHisto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                            //rowHisto.Width = 250;
                            timelineStack.Children.Add(infoHisto);
                    }
                }
            }
        }
        private void ReinitPrinted(object sender, RoutedEventArgs e)
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            mgrBin.writeData(0, binFileName);
            writeTxtFileData(mediaDataPath, "0");
            nb_impressions.Text = "0";
        }
        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }
        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }
        private void Configuration_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToConfiguration.CanExecute(null))
                viewModel.GoToConfiguration.Execute(null);
        }
        private void ShowCoordonates_Click(object sender, RoutedEventArgs e)
        {
            showLoader();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
               
            }));
            Task.Factory.StartNew(() =>
            {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToCoordonate.CanExecute(null))
                viewModel.GoToCoordonate.Execute(null);
            }));
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_loading.Visibility = Visibility.Hidden;
                }));

            });
        }
        private void ShowHistoriques_Click(object sender, RoutedEventArgs e)
        {
            showLoader();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {

            }));
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    var viewModel = (EditEventViewModel)DataContext;
                    if (viewModel.GoToHistorique.CanExecute(null))
                        viewModel.GoToHistorique.Execute(null);
                }));
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_loading.Visibility = Visibility.Hidden;
                }));

            });
        }
        private void ShowPhotos_Click(object sender, RoutedEventArgs e)
        {

            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                showLoader();
            }));
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    var viewModel = (EditEventViewModel)DataContext;
                if (viewModel.GoToPhotos.CanExecute(null))
                viewModel.GoToPhotos.Execute(null);
                }));
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_loading.Visibility = Visibility.Hidden;
                }));

            });
        }
        //private async Task setProgressValue()
        //{
        //    //await Task.Run(() =>
        //    //{

        //    int valueProgress = 0;
        //    int remoteValue = 0;
        //    try
        //    {
        //        remoteValue = await getProgressSynchroValue(code);
        //    }
        //    catch
        //    {
        //        remoteValue = 0;
        //    }
        //    int localValue = getProgressLocalValue(code);
        //    if (remoteValue > localValue)
        //    {
        //        valueProgress = localValue * 100 / remoteValue;
        //    }
        //    else if (localValue > remoteValue)
        //    {
        //        valueProgress = remoteValue * 100 / localValue;
        //    }
        //    else if (remoteValue == localValue && remoteValue > 0)
        //    {
        //        valueProgress = 100;
        //    }
        //    else
        //    {
        //        valueProgress = 0;
        //    }
        //    libProgressBar.Text = valueProgress + "%";
        //    progressBar1.Value = valueProgress;

        //    //});
        //}
        private void setCoordonate()
        {
            foreach(var coordonate in lstCoordonate)
            {
                StackPanel detailsLineStack = new StackPanel();
                detailsLineStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#fafafa"));
                detailsLineStack.Orientation = Orientation.Horizontal;
                detailsLineStack.Margin = new Thickness(40, 0, 0, 0);
                StackPanel imgStack = new StackPanel();
                imgStack.Height = 80;
                System.Windows.Controls.Image imgView = new System.Windows.Controls.Image();
                imgView.Height = 100;
                imgView.Width = 175;
                _photos = null;
                //imgView.Margin = new Thickness(20);
                Bitmap imageBitmap = new Bitmap(coordonate.Image);

                imgView.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                //imgView = coordonate.Image;
                imgStack.Children.Add(imgView);
                detailsLineStack.Children.Add(imgStack);

                StackPanel mailStack = new StackPanel();
                mailStack.Height = 15;
                mailStack.Margin = new Thickness(-10, 0, 0, 0);
                TextBlock mailTxt = new TextBlock();
                mailTxt.Text = coordonate.Email;
                mailTxt.FontSize = 12;
                mailTxt.FontWeight = FontWeights.Bold;
                mailTxt.Margin = new Thickness(15,0,0,0);
                mailTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                mailTxt.Height = 15;
                //mailTxt.Width = 250;
                mailStack.Children.Add(mailTxt);
                detailsLineStack.Children.Add(mailStack);

                StackPanel telStack = new StackPanel();
                telStack.Margin = new Thickness(15, 0, 0, 0);
                telStack.Height = 15;
                TextBlock telTxt = new TextBlock();
                telTxt.Text = coordonate.Tel;
                telTxt.Height = 15;
                telTxt.FontSize = 12;
                telTxt.Margin = new Thickness(15,0,0,0);
                telTxt.FontWeight = FontWeights.Bold;
                telTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                telTxt.Width = 100;
                telStack.Children.Add(telTxt);
                detailsLineStack.Children.Add(telStack);

                StackPanel optinStack = new StackPanel();
                optinStack.Height = 15;
                TextBlock optinTxt = new TextBlock();
                optinTxt.Text = coordonate.Optin;
                optinTxt.Height = 15;
                optinTxt.FontSize = 12;
                //optinTxt.Margin = new Thickness(20);
                optinTxt.FontWeight = FontWeights.Bold;
                optinTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                optinTxt.Width = 50;
                optinStack.Children.Add(optinTxt);
                detailsLineStack.Children.Add(optinStack);

                StackPanel dateStack = new StackPanel();
                dateStack.Height = 15;
                TextBlock dateTxt = new TextBlock();
                dateTxt.Text = coordonate.Date;
                dateTxt.Height = 15;
                dateTxt.FontSize = 12;
                //dateTxt.Margin = new Thickness(20);
                dateTxt.FontWeight = FontWeights.Bold;
                dateTxt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303030"));
                dateTxt.Width = 500;
                dateTxt.HorizontalAlignment = HorizontalAlignment.Left;
                dateStack.Children.Add(dateTxt);
                detailsLineStack.Children.Add(dateStack);
                coordonateStack.Children.Add(detailsLineStack);
                Separator sep = new Separator();
                sep.Height = 3;
                sep.FontSize = 3;
                sep.FontWeight = FontWeights.Bold;
                sep.Background = System.Windows.Media.Brushes.Transparent;
                sep.Foreground = System.Windows.Media.Brushes.Transparent;
                coordonateStack.Children.Add(sep);

                //lstViewCoordonate.Items.Add(new ModifyEvent { Date = coordonate.Date, Email = coordonate.Email, Image = coordonate.Image, Optin = coordonate.Optin, Tel = coordonate.Tel });
            }
        }
        public int getNbPhotos()
        {
            string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            if (Directory.Exists(photosDirectory))
            {
                return Directory.GetFiles(photosDirectory).Length;
            }
            return 0;
        }
        private void CancelReinit(object sender, RoutedEventArgs e)
        {
            dockGrid.IsEnabled = true;
            scrollReinit.Visibility = Visibility.Collapsed;
        }
        private void ReinitializationPrinted(object sender, RoutedEventArgs e)
        {
            //dockGrid.IsEnabled = false;
            //scrollReinit.Visibility = Visibility.Visible;

            MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment réinitialiser le nombre de photos imprimées ? \nEtes-vous sûr ? ", "Réinitialisation du nombre d'impressions", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {                
                ReinitPrinted(sender, e);
            }
        }
        public int getNbPrinted()
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            return mgrBin.readIntData(binFileName);
        }
        void hideLoader()
        {
           
                //GIFCtrl.StopAnimate();
                //GIFCtrl.Visibility = Visibility.Hidden;
            lbl_loading.Visibility = Visibility.Hidden;
        }
        void showLoader()
        {

            //GIFCtrl.Visibility = Visibility.Visible;
            //GIFCtrl.StartAnimate();
            lbl_loading.Visibility = Visibility.Visible;
        }
        public void ShowImage(string path)
        {

            Bitmap imageBitmap = new Bitmap(path);
            img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
        }
        private void ShowPrevImage()
        {
            //ShowImage(this.imageFiles[(--this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(--this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ShowNextImage();
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }

        }
        private string GetDatePhoto(string pathPhoto)
        {
            DateTime creation = File.GetCreationTime(pathPhoto);
            string day = creation.Day.ToString();
            string month = creation.Month.ToString();
            string year = creation.Year.ToString();
            string hour = creation.Hour.ToString();
            string minutes = creation.Minute.ToString();
            if (day.Length == 1) day = "0" + day;
            if (month.Length == 1) month = "0" + month;
            if (hour.Length == 1) hour = "0" + hour;
            if (minutes.Length == 1) minutes = "0" + minutes;
            return "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
        }
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Show the next image.
        /// </summary>
        private void ShowNextImage()
        {
            //ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(++this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }

        private void copieCancel(object sender, EventArgs e)
        {
            closeImageViewer();
        }

        private void closeImageViewer()
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollCopie.Visibility = Visibility.Collapsed;

            //Deselectionner l'image de la ListBox pour pouvoir re-cliquer
            lstImage.SelectedIndex = -1;
        }

        private void removeRowOnCSV(string csvpath)
        {
            if (File.Exists(csvpath))
            {
                string identity = System.IO.Path.GetFileNameWithoutExtension(toPrint);
                List<String> lines = new List<string>();
                string line;

                using (System.IO.StreamReader file = new System.IO.StreamReader(csvpath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }

                lines.RemoveAll(l => l.Contains(identity));
                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(csvpath))
                {
                    outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
                }
            }
        }

        private void getFiles(string photosDirectory)
        {
            if (!Directory.Exists(photosDirectory))
            {
                Directory.CreateDirectory(photosDirectory);
            }
            int index = 0;
            var getFilesDirectory = Directory.GetFiles(photosDirectory);
            DirectoryInfo dir = new DirectoryInfo(photosDirectory);
            FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();

            foreach (FileInfo image in files)
            //foreach (var image in getFilesDirectory)
            {
                index++;
                System.Array.Resize(ref imageFiles, index);
                imageFiles[index - 1] = image.FullName;
            }
            this.selected = 0;
            this.begin = 0;
            this.end = imageFiles.Length;

            ChangeLblPhotos(getFilesDirectory.Length);
        }

        private void LoadLstImages(string pathDirectory)
        {
            //var result = new ObservableCollection<System.Windows.Controls.Image>();
            var result = new ObservableCollection<ImageItemViewModel>();
            if (!string.IsNullOrEmpty(pathDirectory))
            {
                getFiles(pathDirectory);
                DirectoryInfo dir = new DirectoryInfo(pathDirectory);
                FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();
                foreach (FileInfo myFile in files)
                //foreach (string myFile in Directory.GetFiles(pathDirectory))
                {
                    System.Windows.Controls.Image myLocalImage = new System.Windows.Controls.Image();

                    BitmapImage myImageSource = new BitmapImage();
                    myImageSource.BeginInit();
                    myImageSource.CacheOption = BitmapCacheOption.OnLoad;
                    myImageSource.UriSource = new Uri(@"file:///" + myFile.FullName); //new Uri(@"file:///" + myFile.FullName);
                    myImageSource.EndInit();
                    myLocalImage.Source = myImageSource;

                    //result.Add(myLocalImage);
                    result.Add(new ImageItemViewModel { ImageItem = myImageSource, IsChecked = false, ImagePath = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath) });
                }
            }
            lstImage.ItemsSource = result;
        }

        public void removePhoto(string path)
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(path);
        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing(false);
        }

        private void Printing(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;
                    //string binPath;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrintPage += PrintPage;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }

                    closeImageViewer();
                    Thread.Sleep(5000);
                    lbl_CurrentPrinting.Visibility = Visibility.Hidden;
                }));

            });

        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            var selectedImage = (sender as ListBox).SelectedIndex;
            if (selectedImage >= 0)
            {
                this.Opacity = 1;
                var item = (ImageItemViewModel)lstImage.Items[selectedImage];

                //var listeImages = (ObservableCollection<ImageItemViewModel>)lstImage.ItemsSource;
                //var item = listeImages.FindIndex(x => x.ImagePath == path);            
                //selected = Array.IndexOf(listeImages, item);
                //selected = lstImage.Select((c, i) => new { value = c, Index = i })
                //                    .Where(x => x.value.ImagePath == item.ImagePath)
                //                    .Select(x => x.Index).FirstOrDefault();
                selected = selectedImage;
                toPrint = item.ImagePath;
                ShowImage(toPrint);
                dockGrid.IsEnabled = false;
                DateTime creation = File.GetCreationTime(toPrint);
                string day = creation.Day.ToString();
                string month = creation.Month.ToString();
                string year = creation.Year.ToString();
                string hour = creation.Hour.ToString();
                string minutes = creation.Minute.ToString();
                if (day.Length == 1) day = "0" + day;
                if (month.Length == 1) month = "0" + month;
                if (hour.Length == 1) hour = "0" + hour;
                if (minutes.Length == 1) minutes = "0" + minutes;
                txtDate.Text = "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
                scrollCopie.Visibility = Visibility.Visible;
            }
        }
        private void WindowLoaded(object sender, EventArgs e)
        {
            string iniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(iniPath);
            string Name = iniFile.GetSetting("EVENT", "Name");
            eventName.Text = Name;
            codeEvent.Text = $"({code})";
        }
        
        public async Task Synchro()
        {
            string FileUrl = "https://managerdev.selfizee.fr/transfert/files_" + code + ".txt";
            if (chk_con())
            {
                WebClient wc = new WebClient();
                wc.DownloadFile(FileUrl, Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt");
            }
            string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            string TxtFileName = System.IO.Path.GetFileName(FilePath);
            char[] delimiters = { '_', '.' };
            string TxtEventName = TxtFileName.Split(delimiters)[1];
            string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            List<string> TxtLine = new List<string>();
            List<string> MediaLine = new List<string>();
            if (!File.Exists(MediaPath))
            {
                progress = 0;
            }
            else
            {
                string line;
                using (StreamReader MediaSR = new StreamReader(MediaPath))
                {
                    while ((line = MediaSR.ReadLine()) != null)
                    {
                        MediaLine.Add(line);
                    }
                }
                //MediaLine.RemoveAt(0);
                int totalMedia = MediaLine.Count();
                using (StreamReader TxtSR = new StreamReader(FilePath))
                {
                    while ((line = TxtSR.ReadLine()) != null)
                    {
                        TxtLine.Add(line);
                    }
                }
                int totalTxt = TxtLine.Count();
                try
                {
                    if (totalMedia >= totalTxt)
                    {
                        int totalTxtMatched = 0;
                        foreach (var item in MediaLine)
                        {
                            string photo = item.Split(',')[1];
                            if (TxtLine.Contains(photo))
                            {
                                totalTxtMatched = totalTxtMatched + 1;
                            }
                            else
                            {
                                break;
                            }
                        }
                        progress = (double)totalTxtMatched / totalMedia * 100;
                    }
                    else
                    {
                        int totalMediaMatched = 0;
                        foreach (var item in TxtLine)
                        {
                            foreach (var itm in MediaLine)
                            {
                                string photo = itm.Split(',')[1];
                                if (item == photo)
                                {
                                    totalMediaMatched = totalMediaMatched + 1;
                                }
                            }
                        }
                        progress = (double)totalMediaMatched / totalTxt * 100;
                    }

                }
                catch (Exception e)
                {

                }
            }
            libProgressBar.Text = (int)progress + "%";
            progressBar1.Value = (int)progress;
        }
        public async Task<bool> IsSynchro()
        {
            string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            //string FileUrl = "";
            //if (chk_con())
            //{
            //    WebClient wc = new WebClient();
            //    wc.DownloadFile(FileUrl, Globals._defaultUpdateDirectory + "\\files_"+code+".txt");
            //}
            string TxtFileName = System.IO.Path.GetFileName(FilePath);
            char[] delimiters = { '_', '.' };
            string TxtEventName = TxtFileName.Split(delimiters)[1];
            string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            List<string> TxtLine = new List<string>();
            List<string> MediaLine = new List<string>();
            bool result = false;
            if (!File.Exists(MediaPath))
            {
                result = false;
            }
            else
            {
                string line;
                using (StreamReader MediaSR = new StreamReader(MediaPath))
                {
                    while ((line = MediaSR.ReadLine()) != null)
                    {
                        MediaLine.Add(line);
                    }
                }
                if (MediaLine[0].Contains("Date and time;Photo;Filter;Type of format;Printed;Number of printing;Green screen;Photo time;Print time;"))
                {
                    MediaLine.RemoveAt(0);
                }
                int totalMedia = MediaLine.Count();
                using (StreamReader TxtSR = new StreamReader(FilePath))
                {
                    while ((line = TxtSR.ReadLine()) != null)
                    {
                        TxtLine.Add(line);
                    }
                }
                int totalTxt = TxtLine.Count();
                try
                {
                    if (totalMedia >= totalTxt)
                    {
                        foreach (var item in MediaLine)
                        {
                            char[] delimiter = { ',', ';' };
                            string photo = item.Split(delimiter)[1];
                            if (TxtLine.Contains(photo))
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in TxtLine)
                        {
                            foreach (var itm in MediaLine)
                            {
                                string photo = itm.Split(',')[1];
                                if (item == photo)
                                {
                                    result = true;
                                }
                            }
                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return result;
        }

        public async Task DeleteEvent()
        {
            try
            {
                string DataPath = Globals._MediaFolder + "\\" + code + "\\Data";
                string PhotoPath = Globals._MediaFolder + "\\" + code + "\\Photos";
                FileInfo fi_Data = new FileInfo(PhotoPath);
                string d = "10/10/2019 11:56:13";
                DateTime dateEvent = DateTime.Parse(d);
                //DateTime dateEvent = fi_Data.LastWriteTime;
                TimeSpan ts = DateTime.Now - dateEvent;
                bool isSynnchro = await IsSynchro();
                if (ts.TotalDays >= 31 && isSynnchro == true)
                {
                    foreach (string file in Directory.GetFiles(DataPath))
                    {
                        File.Delete(file);
                    }
                    foreach (string file in Directory.GetFiles(PhotoPath))
                    {
                        File.Delete(file);
                    }
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    public class ModifyEvent
    {
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Image { get; set; }
        public string Optin { get; set; }
        public string Date { get; set; }
    }
}
