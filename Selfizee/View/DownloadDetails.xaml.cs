﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Compression;
using Selfizee.Manager;
using System.Drawing;
using System.Threading;
using log4net;
using System.Collections.Specialized;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for DownloadDetails.xaml
    /// </summary>
    public partial class DownloadDetails : System.Windows.Controls.UserControl
    {


        string ftpServerIP = "37.187.132.132";
        //string ftpUserName = "sync@uploadv2.selfizee.fr";
        string ftpUserName = "dev@sync.selfizee.fr"; //"syncdev";
        FtpManager _ftpManager = new FtpManager();
        string currentDirectory = "";
        string currentFile = "";
        //string ftpPassword = "S99e)%VxI?q*5*q_$6zK/8k/";
        string ftpPassword = "[2WLR1FHxCZmXxUKr7>dc[=4"; //"-rB1yn}MJ?~D?Yb51n$4M";
        List<DirectoryItem> lstItem = new List<DirectoryItem>();
        BackgroundWorker worker = new BackgroundWorker();
        WebClient client = new WebClient();
        private bool error = false;
        private static readonly ILog Log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        FtpDownloadEventManager eventDownloader = new FtpDownloadEventManager();
        public DownloadDetails()
        {
            InitializeComponent();

            this.progressBar1.Minimum = 0;
            this.progressBar1.Maximum = 100;
            this.progressBar1.Value = 0;
            ImageBrush btnLaunchAnimation = new ImageBrush();
            btnLaunchAnimation.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
            LaunchAnimation.Background = btnLaunchAnimation;

            ImageBrush btndownload = new ImageBrush();
            btndownload.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDownloadOther, UriKind.Absolute));
            Download_another.Background = btndownload;

            Bitmap imageBitmap4 = new Bitmap(Globals._defaultImageSelfizee);


            Bitmap imageBitmap = new Bitmap(Globals._defaultImageLoader);
            img_visu.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);


            eventDownloader.ftpFileNamePath = "ftp://37.187.132.132/Events/" + Globals._eventToDownload + ".zip";
            eventDownloader.userName = "release";
            eventDownloader.password = "c7j!RBQ{NG68}kg5n)?t5HBL";

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            close.Background = brushCroix;

            scrollNoConnection.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollNoConnection.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

            worker.DoWork += backgroundWorker1_DoWork;
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            worker.RunWorkerAsync();
            string iniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(iniPath);
            name.Text = Globals._nameEventToDownload;
            code.Text = Globals._eventToDownload;
        }

        public void runDownload()
        {
            string adressDirectory = "ftp://37.187.132.132/" + Globals._eventToDownload;


            try
            {
                lstItem = _ftpManager.GetDirectoryInformation(adressDirectory, ftpUserName, ftpPassword);
                if (Globals.paramDownload.Equals("config"))
                {
                    getDirectoryContents("Configuration");
                }
                else
                {
                    foreach (var item in lstItem)
                    {
                        getDirectoryContents(item.Name);
                    }
                }
            }
            catch (WebException ex)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    error = true;
                    KillMe();
                    Log.Error("Connexion error: " + ex.Message);
                    noConnection.Text = "Impossible de télécharger les fichiers pour le moment: " + "\r\n" + "    - Vérifier votre connexion internet \r\n    - Vérifier si l'événement existe dans le répertoire distant ";
                    scrollNoConnection.Visibility = Visibility.Visible;
                }));



                //        break;
                //        //case WebExceptionStatus
                //}
            }

        }

        private void sendInfoAfterDownload(string code)
        {
            try
            {
                INIFileManager iniFile = new INIFileManager(Globals._appConfigFile);
                string idBorne = iniFile.GetSetting("EVENTSCONFIG", "idborne");
                string currentDate = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
                string url = "https://booth.selfizee.fr/configurations/isDownloadFinish";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["code_logiciel"] = code;
                    data["is_down_finish"] = "1";
                    data["date_down"] = currentDate;
                    data["num_borne"] = idBorne;

                    if (InternetConnectionManager.checkConnection("www.google.fr"))
                    {
                        var response = wb.UploadValues(url, "POST", data);
                        string responseInString = Encoding.UTF8.GetString(response);
                    }

                    //return bool.Parse(responseInString);
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void getDirectoryContents(string dirName)
        {

            currentDirectory = dirName;
            long fileSize = 60000;
            //string ftpDirectoryPath = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName;
            string ftpDirectoryPath = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}";
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpDirectoryPath);
                request.Timeout = 6000;
                request.ReadWriteTimeout = 6000;
                if (Globals.paramDownload.Equals("config"))
                {
                    //if (item.IsDirectory && dirName.ToLower().Equals("configuration"))
                    //{
                    currentDirectory = dirName;
                    //string ftpDirectoryPath1 = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/Configuration/Config.ini";
                    string ftpDirectoryPath1 = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/Configuration/Config.ini";
                    FtpWebRequest request1 = (FtpWebRequest)WebRequest.Create(ftpDirectoryPath1);
                    request1.Timeout = 6000;
                    request1.ReadWriteTimeout = 6000;
                    List<DirectoryItem> files1 = _ftpManager.GetDirectoryInformation(ftpDirectoryPath1, ftpUserName, ftpPassword);

                    foreach (var item1 in files1)
                    {
                        if (!item1.IsDirectory)
                        {
                            currentFile = item1.Name;

                            //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/Configuration/" + item1.Name;
                            string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/Configuration/{item1.Name}";

                            request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                            request.Timeout = 6000;
                            request.ReadWriteTimeout = 6000;
                            request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                            request.Method = WebRequestMethods.Ftp.DownloadFile;
                            if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                            {
                                Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                            }
                            if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                            {
                                Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                            }
                            //if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name))
                            //{
                            //    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name);
                            //}

                            //using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                            FtpWebResponse responseFileDownload = null;
                            responseFileDownload = (FtpWebResponse)request.GetResponse();
                            using (Stream responseStream = responseFileDownload.GetResponseStream())
                            using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item1.Name, FileMode.Create))
                            {

                                int Length = 2048;
                                Byte[] buffer = new Byte[Length];
                                int bytesRead = responseStream.Read(buffer, 0, Length);
                                int bytes = 0;

                                while (bytesRead > 0)
                                {
                                    writeStream.Write(buffer, 0, bytesRead);
                                    bytesRead = responseStream.Read(buffer, 0, Length);
                                    bytes += bytesRead;// don't forget to increment bytesRead !
                                    int totalSize = (int)(fileSize) / 1000; // Kbytes
                                    worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                }
                            }
                        }
                    }
                    //}
                }
                else
                {
                    List<DirectoryItem> files = _ftpManager.GetDirectoryInformation(ftpDirectoryPath, ftpUserName, ftpPassword);
                    foreach (var item in files)
                    {

                        if (Globals.paramDownload.Equals("config"))
                        {

                        }
                        else if (Globals.paramDownload.Equals("images"))
                        {

                            if (item.IsDirectory == false && currentDirectory.ToLower() != "configuration")
                            {
                                currentFile = item.Name;
                                //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name;
                                string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}";
                                request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                                request.Timeout = 6000;
                                request.ReadWriteTimeout = 6000;
                                request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                                request.Method = WebRequestMethods.Ftp.DownloadFile;
                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                                {
                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                                }
                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                                {
                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                                }
                                using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                                using (Stream responseStream = responseFileDownload.GetResponseStream())
                                using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name, FileMode.Create))
                                {

                                    int Length = 2048;
                                    Byte[] buffer = new Byte[Length];
                                    int bytesRead = responseStream.Read(buffer, 0, Length);
                                    int bytes = 0;

                                    while (bytesRead > 0)
                                    {
                                        writeStream.Write(buffer, 0, bytesRead);
                                        bytesRead = responseStream.Read(buffer, 0, Length);
                                        bytes += bytesRead;// don't forget to increment bytesRead !
                                        int totalSize = (int)(fileSize) / 1000; // Kbytes
                                        worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                    }
                                }
                            }
                            else
                            {
                                currentDirectory = item.Name;
                                string lowerDirName = item.Name.ToLower();
                                if (!lowerDirName.Equals("configuration") && !lowerDirName.Contains("config.ini"))
                                {
                                    //string ftpDirectoryPath1 = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name;
                                    string ftpDirectoryPath1 = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}";
                                    FtpWebRequest request1 = (FtpWebRequest)WebRequest.Create(ftpDirectoryPath1);
                                    request1.Timeout = 6000;
                                    request1.ReadWriteTimeout = 6000;
                                    List<DirectoryItem> files1 = _ftpManager.GetDirectoryInformation(ftpDirectoryPath1, ftpUserName, ftpPassword);

                                    foreach (var item1 in files1)
                                    {
                                        if (!item1.IsDirectory)
                                        {
                                            currentFile = item1.Name;

                                            //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name + "/" + item1.Name;
                                            string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}/{item1.Name}";

                                            request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                                            request.Timeout = 6000;
                                            request.ReadWriteTimeout = 6000;
                                            request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                                            request.Method = WebRequestMethods.Ftp.DownloadFile;
                                            if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                                            {
                                                Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                                            }
                                            if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                                            {
                                                Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                                            }
                                            if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name))
                                            {
                                                Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name);
                                            }

                                            using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                                            using (Stream responseStream = responseFileDownload.GetResponseStream())
                                            using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name + "\\" + item1.Name, FileMode.Create))
                                            {

                                                int Length = 2048;
                                                Byte[] buffer = new Byte[Length];
                                                int bytesRead = responseStream.Read(buffer, 0, Length);
                                                int bytes = 0;

                                                while (bytesRead > 0)
                                                {
                                                    writeStream.Write(buffer, 0, bytesRead);
                                                    bytesRead = responseStream.Read(buffer, 0, Length);
                                                    bytes += bytesRead;// don't forget to increment bytesRead !
                                                    int totalSize = (int)(fileSize) / 1000; // Kbytes
                                                    worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            //}
                        }
                        else
                        {
                            if (item.IsDirectory == false)
                            {
                                currentFile = item.Name;
                                //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name;
                                string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}";
                                request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                                request.Timeout = 6000;
                                request.ReadWriteTimeout = 6000;
                                request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                                request.Method = WebRequestMethods.Ftp.DownloadFile;
                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                                {
                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                                }
                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                                {
                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                                }
                                using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                                using (Stream responseStream = responseFileDownload.GetResponseStream())
                                using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name, FileMode.Create))
                                {

                                    int Length = 2048;
                                    Byte[] buffer = new Byte[Length];
                                    int bytesRead = responseStream.Read(buffer, 0, Length);
                                    int bytes = 0;

                                    while (bytesRead > 0)
                                    {
                                        writeStream.Write(buffer, 0, bytesRead);
                                        bytesRead = responseStream.Read(buffer, 0, Length);
                                        bytes += bytesRead;// don't forget to increment bytesRead !
                                        int totalSize = (int)(fileSize) / 1000; // Kbytes
                                        worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                    }
                                }
                            }
                            else
                            {
                                currentDirectory = item.Name;
                                //string ftpDirectoryPath1 = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name;
                                string ftpDirectoryPath1 = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}";
                                FtpWebRequest request1 = (FtpWebRequest)WebRequest.Create(ftpDirectoryPath1);

                                List<DirectoryItem> files1 = _ftpManager.GetDirectoryInformation(ftpDirectoryPath1, ftpUserName, ftpPassword);

                                foreach (var item1 in files1)
                                {
                                    if (!item1.IsDirectory)
                                    {
                                        currentFile = item1.Name;

                                        //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name + "/" + item1.Name;
                                        string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}/{item1.Name}";

                                        request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                                        request.Timeout = 6000;
                                        request.ReadWriteTimeout = 6000;
                                        request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                                        if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                                        {
                                            Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                                        }
                                        if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                                        {
                                            Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                                        }
                                        if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name))
                                        {
                                            Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name);
                                        }

                                        using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                                        using (Stream responseStream = responseFileDownload.GetResponseStream())
                                        using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name + "\\" + item1.Name, FileMode.Create))
                                        {

                                            int Length = 2048;
                                            Byte[] buffer = new Byte[Length];
                                            int bytesRead = responseStream.Read(buffer, 0, Length);
                                            int bytes = 0;

                                            while (bytesRead > 0)
                                            {
                                                writeStream.Write(buffer, 0, bytesRead);
                                                bytesRead = responseStream.Read(buffer, 0, Length);
                                                bytes += bytesRead;// don't forget to increment bytesRead !
                                                int totalSize = (int)(fileSize) / 1000; // Kbytes
                                                worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        currentDirectory = item1.Name;
                                        //string ftpDirectoryPath1 = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name;
                                        string ftpDirectoryPath2 = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}/{item1.Name}";
                                        FtpWebRequest request2 = (FtpWebRequest)WebRequest.Create(ftpDirectoryPath2);

                                        List<DirectoryItem> files2 = _ftpManager.GetDirectoryInformation(ftpDirectoryPath2, ftpUserName, ftpPassword);
                                        foreach (var item2 in files2)
                                        {
                                            if (!item2.IsDirectory)
                                            {
                                                currentFile = item2.Name;

                                                //string ftpfileName = "ftp://" + this.ftpServerIP + "/EVENTS/" + Globals._eventToDownload + "/" + dirName + "/" + item.Name + "/" + item1.Name;
                                                string ftpfileName = $"ftp://{ftpServerIP}/{Globals._eventToDownload}/{dirName}/{item.Name}/{item1.Name}/{ item2.Name}";

                                                request = (FtpWebRequest)WebRequest.Create(ftpfileName);
                                                request.Timeout = 6000;
                                                request.ReadWriteTimeout = 6000;
                                                request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
                                                request.Method = WebRequestMethods.Ftp.DownloadFile;
                                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload))
                                                {
                                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                                                }
                                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName))
                                                {
                                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName);
                                                }
                                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name))
                                                {
                                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name);
                                                }
                                                if (!Directory.Exists(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name + "\\" + item1.Name))
                                                {
                                                    Directory.CreateDirectory(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name + "\\" + item1.Name);
                                                }

                                                using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                                                using (Stream responseStream = responseFileDownload.GetResponseStream())
                                                using (FileStream writeStream = new FileStream(Globals.downloadDirectory + "\\" + Globals._eventToDownload + "\\" + dirName + "\\" + item.Name + "\\" + item1.Name + "\\" + item2.Name, FileMode.Create))
                                                {

                                                    int Length = 2048;
                                                    Byte[] buffer = new Byte[Length];
                                                    int bytesRead = responseStream.Read(buffer, 0, Length);
                                                    int bytes = 0;

                                                    while (bytesRead > 0)
                                                    {
                                                        writeStream.Write(buffer, 0, bytesRead);
                                                        bytesRead = responseStream.Read(buffer, 0, Length);
                                                        bytes += bytesRead;// don't forget to increment bytesRead !
                                                        int totalSize = (int)(fileSize) / 1000; // Kbytes
                                                        worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (WebException ex)
            {
                //switch (ex.Status)
                //{
                //    case WebExceptionStatus.NameResolutionFailure:
                //        KillMe();
                //        Log.Error("Connexion error: " + ex.Message);
                //        noConnection.Text = "Aucune connexion internet disponible";
                //        scrollNoConnection.Visibility = Visibility.Visible;
                //        break;
                //        //case WebExceptionStatus
                //}
                ////this.Dispatcher.Invoke((Action)(() =>
                ////{

                ////}));
                throw ex;
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void closeInfo(object sender, EventArgs e)
        {

            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollNoConnection.Visibility = Visibility.Collapsed;
            var viewModel = (DownloadDetailsViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void CancelDownload(object sender, RoutedEventArgs e)
        {
            var viewModel = (DownloadDetailsViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            runDownload();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            //Debug.WriteLine(e.ProgressPercentage * (int)e.UserState / 100 + " bytes / " + e.UserState + " bytes" + " % = " + e.ProgressPercentage);
            tlcVisu.Text = "TELECHARGEMENT VISUELS.. (" + currentDirectory + "/" + currentFile + ")";
            if (e.ProgressPercentage <= 100)
            {
                progressTXT.Text = e.ProgressPercentage + " %";
            }
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (error)
            {
                img_visu.Visibility = Visibility.Hidden;
                img_config.Visibility = Visibility.Hidden;


                img_file.Visibility = Visibility.Hidden;

                txtdownloadConfig.Visibility = Visibility.Hidden;

                img_save.Visibility = Visibility.Hidden;

                txtverifFile.Visibility = Visibility.Hidden;
                img_save.Visibility = Visibility.Hidden;
                txtSave.Visibility = Visibility.Hidden;
            }
            else
            {
                progressTXT.Text = "100%";
                progressBar1.Value = 100;
                Thread.Sleep(1000);
                progressTXT.Text = "";
                progressBar1.Value = 0;

                if (!Directory.Exists(Globals._assetsFolder + "\\" + Globals._eventToDownload))
                {
                    Directory.CreateDirectory(Globals._assetsFolder + "\\" + Globals._eventToDownload);
                }
                if (!Directory.Exists("C:\\EVENTS\\Media\\" + Globals._eventToDownload))
                {
                    Directory.CreateDirectory("C:\\EVENTS\\Media\\" + Globals._eventToDownload);
                    Directory.CreateDirectory("C:\\EVENTS\\Media\\" + Globals._eventToDownload + "\\Photos");
                    Directory.CreateDirectory("C:\\EVENTS\\Media\\" + Globals._eventToDownload + "\\Photos\\Original");
                }
                if (!Directory.Exists("C:\\EVENTS\\Media\\" + Globals._eventToDownload + "\\Data"))
                {
                    Directory.CreateDirectory("C:\\EVENTS\\Media\\" + Globals._eventToDownload + "\\Data");
                }

                Bitmap imageBitmap1 = new Bitmap(Globals._defaultImageLoader);
                img_config.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                Bitmap imageBitmap = new Bitmap(Globals._defaultokconfig);
                img_visu.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                img_config.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);

                sendInfoAfterDownload(Globals._eventToDownload.ToLower());

                tlcVisu.Text = "TELECHARGEMENT VISUELS..";
                DirectoryInfo source = new DirectoryInfo(Globals.downloadDirectory + "\\" + Globals._eventToDownload);
                DirectoryInfo target = new DirectoryInfo(Globals._assetsFolder + "\\" + Globals._eventToDownload);
                string configurationFile = Globals._assetsFolder + "\\" + Globals._eventToDownload + "\\Configuration\\Config.ini";

                CopyFilesRecursively(source, target);
                if (System.IO.File.Exists(Globals._assetsFolder + "\\" + Globals._eventToDownload + "\\Config.ini"))
                {
                    File.Delete(Globals._assetsFolder + "\\" + Globals._eventToDownload + "\\Config.ini");
                }
                System.IO.File.Move(configurationFile, Globals._assetsFolder + "\\" + Globals._eventToDownload + "\\Config.ini");
                var dir = new DirectoryInfo(Globals._assetsFolder + "\\" + Globals._eventToDownload + "\\Configuration");
                dir.Delete(true);
                tlcVisu.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                img_visu.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                img_config.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                Thread.Sleep(1000);
                img_config.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                img_file.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                Thread.Sleep(1000);
                txtdownloadConfig.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                img_file.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                img_save.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                Thread.Sleep(1000);
                txtverifFile.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                img_save.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                txtSave.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                //mainEnd.Visibility = Visibility.Visible;
                //endDownload.Visibility = Visibility.Visible;
                //LaunchAnimation.Visibility = Visibility.Visible;
                //Download_another.Visibility = Visibility.Visible;
                var viewModel = (DownloadDetailsViewModel)DataContext;
                if (viewModel.GoToPostInstall.CanExecute(null))
                    viewModel.GoToPostInstall.Execute(null);
            }
        }

        public static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                if (dir.Name.ToLower() != "remote")
                {
                    CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
                }
            }

            foreach (FileInfo file in source.GetFiles())
            {
                if (Globals.paramDownload.Equals("config"))
                {
                    if (source.Name.ToLower().Equals("configuration"))
                    {
                        string filename = target.FullName + "\\" + file.Name;
                        if ((!System.IO.File.Exists(filename)))
                        {
                            file.CopyTo(filename);
                        }
                        else
                        {
                            System.IO.File.Delete(filename);
                            file.CopyTo(filename);
                        }
                    }
                }
                else
                {
                    string filename = target.FullName + "\\" + file.Name;
                    if ((!System.IO.File.Exists(filename)))
                    {
                        file.CopyTo(filename);
                    }
                    else
                    {
                        System.IO.File.Delete(filename);
                        file.CopyTo(filename);
                    }
                }


            }

        }

        public void KillMe()
        {
            worker.CancelAsync();
            worker.Dispose();
            worker = null;
            GC.Collect();
        }
        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (DownloadDetailsViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }


        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals._eventToDownload;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (DownloadDetailsViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (DownloadDetailsViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void RunDownload_Click(object sender, RoutedEventArgs e)
        {

        }

    }

    class FTPClient : WebClient
    {
        protected override WebRequest GetWebRequest(System.Uri address)
        {
            FtpWebRequest req = (FtpWebRequest)base.GetWebRequest(address);
            req.UsePassive = false;
            return req;
        }
    }
}
