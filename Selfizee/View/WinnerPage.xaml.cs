﻿using Selfizee.Managers;
using Selfizee.Models.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Printing;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for WinnerPage.xaml
    /// </summary>
    public partial class WinnerPage : UserControl
    {
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        Dictionary<string, string> hashTextBox = new Dictionary<string, string>();
        public static string csvHoraires = Globals.EventAssetFolder() + "\\InstantGagnant\\horaires.csv";
        private string mediaDataPath = Globals.EventMediaFolder() + "\\Data\\print.txt";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private string Gift_image = "";
        private int nbmaxprinting = 0;

        private string form_rgpd_Name { get; set; }
        private string form_rgpd_lastname { get; set; }
        private string form_rgpd_email { get; set; }
        private string form_rgpd_postalcode { get; set; }
        private string form_rgpd_country { get; set; }
        private string form_rgpd_accept { get; set; }
        private DispatcherTimer timerCountdown;
        private string current_form { get; set; }

        public WinnerPage()
        {
            InitializeComponent();
            hashTextBox.Add("key_" + _Name.Name, _Name.Text);
            hashTextBox.Add("key_" + _lastname.Name, _lastname.Text);
            hashTextBox.Add("key_" + _emailRgpd.Name, _emailRgpd.Text);
            hashTextBox.Add("key_" + _postalCode.Name, _postalCode.Text);
            hashTextBox.Add("key_" + _country.Name, _country.Text);
            ImageBrush imgBrush = new ImageBrush();
            ImageBrush homeBrush = new ImageBrush();
            homeBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationHome, UriKind.Absolute));
            vbtnGoToHome.Background = homeBrush;
            ImageBrush printBrush = new ImageBrush();
            printBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationPrinting, UriKind.Absolute));
            vbtnImprimer.Background = printBrush;
            savingData();
            string bg_filename = getBackGroundImage();
            imgBrush.ImageSource = new BitmapImage(new Uri(bg_filename, UriKind.Relative));
            imgBrush.Stretch = Stretch.Fill;
            winnerPage.Background = imgBrush;
            _Name.GotFocus += CoordonneeTextBoxRgpdGetFocused;
            _Name.LostFocus += CoordonneeTextBoxRgpdLostFocused;
            _lastname.GotFocus += CoordonneeTextBoxRgpdGetFocused;
            _lastname.LostFocus += CoordonneeTextBoxRgpdLostFocused;
            _country.GotFocus += CoordonneeTextBoxRgpdGetFocused;
            _country.LostFocus += CoordonneeTextBoxRgpdLostFocused;
            _emailRgpd.GotFocus += CoordonneeTextBoxRgpdGetFocused;
            _emailRgpd.LostFocus += CoordonneeTextBoxRgpdLostFocused;
            _postalCode.GotFocus += CoordonneeTextBoxRgpdGetFocused;
            _postalCode.LostFocus += CoordonneeTextBoxRgpdLostFocused;
            _postalCode.TextChanged += textBox_TextChanged;
            //GetCountryList();
            getBasicInfo();
            ImageBrush btnnext = new ImageBrush();
            btnnext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormNext, UriKind.Absolute));
            ImageBrush btncancelform = new ImageBrush();
            btncancelform.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnCancelForm, UriKind.Absolute));
            ImageBrush btnprevious = new ImageBrush();
            btnprevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormPrevious, UriKind.Absolute));

            hideFormRgpd.Background = btncancelform;
            hideFormRgpd1.Background = btncancelform;

            continueFormOneRgpd.Background = btnnext;
            continueToPrintexitRgpd.Background = btnnext;
            continueFormOneMandatory.Background = btnnext;
            scrollRgpd.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollRgpd.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            scrollCoordonateRGPD.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollCoordonateRGPD.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            scrollCoordonateMandatory.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollCoordonateMandatory.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            setTimeout();
            backToCoordonateRgpd.Background = btnprevious;
            LaunchTimerCountdown();
        }

        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0 )
            //{
            //    setTimeout();
            //}
            Globals.timeOut--;
            if (Globals.timeOut == 0)
            {
                lbl_CountDown.Content = "";
            }
            else
            {
                lbl_CountDown.Content = Globals.timeOut;
            }

            if (Globals.timeOut == -1)
            {
                lbl_CountDown.Content = "";
                timerCountdown.Stop();
                var viewModel = (WinnerPageViewModel)DataContext;
                if (viewModel.GoToAcceuil.CanExecute(null))
                    viewModel.GoToAcceuil.Execute(null);
            }
        }

        private void GoToThanks(object sender, RoutedEventArgs e)
        {
            timerCountdown.Stop();
            var viewModel = (WinnerPageViewModel)DataContext;
            if (viewModel.GoToThanks.CanExecute(null))
                viewModel.GoToThanks.Execute(null);
        }


        //public void GetCountryList()
        //{
        //    List<string> cultureList = new List<string>();

        //    CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        //    foreach (CultureInfo culture in cultures)
        //    {
        //        RegionInfo region = new RegionInfo(culture.LCID);

        //        if (!(cultureList.Contains(region.EnglishName)))
        //        {
        //            cultureList.Add(region.EnglishName);
        //        }
        //    }
        //    cultureList = cultureList.OrderBy(q => q).ToList();
        //    foreach(string str in cultureList)
        //    {
        //        cbCountry.Items.Add(str);
        //    }
        //    //return cultureList;
        //}

        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_postalCode.Text))
            {
                if (_postalCode.Text != "Code postal *")
                {
                    string textBoxText = _postalCode.Text;
                    string lastCharacter = textBoxText.Substring(textBoxText.Length - 1);
                    if (!IsNumeric(lastCharacter))
                    {
                        _postalCode.Text = _postalCode.Text.Substring(0, textBoxText.Length - 1);
                        _postalCode.SelectionStart = _postalCode.Text.Length;
                    }
                }

            }

        }

        private void backtcoordonateRgpd(object sender, RoutedEventArgs e)
        {
            form_rgpd_Name = "";
            form_rgpd_lastname = "";
            form_rgpd_email = "";
            form_rgpd_postalcode = "";
            form_rgpd_country = "";
            scrollCoordonateRGPD.Visibility = Visibility.Visible;
            scrollRgpd.Visibility = Visibility.Collapsed;
        }

        private void gotoUserFromMendatory(object sender, RoutedEventArgs e)
        {
            if (current_form == "rgpdcoordonate")
            {
                scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
                scrollCoordonateRGPD.Visibility = Visibility.Visible;
            }
            else if (current_form == "textrgpd")
            {
                scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
                scrollRgpd.Visibility = Visibility.Visible;
            }
        }

        private void goToPrintRgpd(object sender, RoutedEventArgs e)
        {
            scrollRgpd.Visibility = Visibility.Collapsed;
            if (accept.IsChecked == false)
            {
                coordonneeMandatoryTitle.Text = "Veuillez cocher la case \"Accepter\" pour continuer";
                current_form = "textrgpd";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
            }
            else
            {
                form_rgpd_accept = "OUI";
                var nbToPrint = 1;
                var nbPrintFinal = getNbPrinted() + nbToPrint;
                nbmaxprinting = Convert.ToInt32(_inimanager.GetSetting("PRINTING", "eventauthorizedprint"));
                if ((nbmaxprinting == 0) || (nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
                {
                    MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    Globals.dtPrint = DateTime.Now;
                    writeformData();
                    BinaryFileManager mgrBin = new BinaryFileManager();
                    string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                    

                    
                        //lbl_CurrentPrinting.Content = "Impression en cours....";

                        //nbPrinted = 1;
                        nbPrintFinal = getNbPrinted() + 1;
                        vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));

                        int toWrite = mgrBin.readIntData(binFileName);
                        toWrite += Convert.ToInt32(1);
                        writeTxtFileData(mediaDataPath, toWrite.ToString());
                        mgrBin.writeData(toWrite, binFileName);
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            lbl_CurrentPrinting.Content = "Impression en cours....";
                        }));
                        Printing(false);
                        PrintingGift(false);
                    var viewModel = (WinnerPageViewModel)DataContext;
                    if (viewModel.GoToThanks.CanExecute(null))
                        viewModel.GoToThanks.Execute(null);
                }
            }
        }

        private void writeformData()
        {
                string dateHeure = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                string form_data = form_rgpd_Name + "," + form_rgpd_lastname + "," + form_rgpd_email + "," + form_rgpd_postalcode + "," + form_rgpd_country + "," + form_rgpd_accept + "," + Globals.gift + "," + dateHeure + "," + Globals.point_relais + ",";
                string path = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
                string allText = File.ReadAllText(path);
                string text = File.ReadLines(path).Last();
                string finalText = text + form_data;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8);
                for (int i = 0; i < rows.Length; i++)
                {
                    if (rows[i].Contains(text))
                    {
                        rows[i] = rows[i].Replace(text, finalText);
                    }
                    if (rows[i] != "")
                        sw.WriteLine(rows[i]);
                }
                sw.Close();
                ReadReplace("OUI");
                ReadReplaceWin(Globals._originalString);
        }

        public void ReadReplace(string value)
        {
            string path = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
            string allText = File.ReadAllText(path);
            string text = File.ReadLines(path).Last();
            string[] tabText = text.Split(',');
            tabText[4] = value;
            tabText[5] = "1";
            tabText[8] = Globals.dtPrint.ToString("dd/MM/yyyy HH:mm"); ;
            string finalText = "";
            for (int i = 0; i < tabText.Length; i++)
            {
                finalText += tabText[i] + ",";
            }
            StreamReader sr = new StreamReader(path);
            String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            StreamWriter sw = new StreamWriter(path);
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i].Contains(text))
                {
                    rows[i] = rows[i].Replace(text, finalText);
                }
                if (rows[i] != "")
                    sw.WriteLine(rows[i]);
            }
            sw.Close();
        }

        private void OpenForm(object sender, EventArgs e)
        {
            timerCountdown.Stop();
           
                form_rgpd_accept = "";
                form_rgpd_email = "";
                form_rgpd_country = "";
                form_rgpd_lastname = "";
                form_rgpd_email = "";
                form_rgpd_postalcode = "";
                scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
                current_form = "rgpdcoordonate";
                scrollCoordonateRGPD.Visibility = Visibility.Visible;
                alphanumeriquekeyboardRgpd.Visibility = Visibility.Collapsed;
                numeriqueKeyboardRgpd.Visibility = Visibility.Collapsed;
        }

        public void ReadReplaceWin(string originalString)
        {
            string allText = File.ReadAllText(csvHoraires);
            string[] tabText = originalString.Split(';');
            tabText[4] = "OUI";
            string finalText = "";
            for (int i = 0; i < tabText.Length; i++)
            {
                finalText += tabText[i] + ";";
            }
            StreamReader sr = new StreamReader(csvHoraires);
            String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            StreamWriter sw = new StreamWriter(csvHoraires);
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i].Contains(originalString))
                {
                    rows[i] = rows[i].Replace(originalString, finalText);
                }
                if (rows[i] != "")
                    sw.WriteLine(rows[i]);
            }
            sw.Close();
        }

        private void getBasicInfo()
        {
            bool newFile = false;
            string Result = $"{Globals.EventMediaFolder()}\\Data\\data.csv";
            if (!File.Exists(Result))
            {
                var file = File.Create(Result);
                file.Close();
                newFile = true;
            }
            else
            {
                string contentFile = File.ReadAllText(Result);
                if (string.IsNullOrEmpty(contentFile))
                {
                    newFile = true;
                }
            }
            //string[] headerTab = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen","Photo time", "Print time","email","Profil","Position","Job","Activity area","J'accepte de recevoir ma photo par email","J’autorise Infopro Digital à utiliser mon email pour m’envoyer les actualités et offres promotionnelles de la société","J’autorise Infopro Digital  à diffuser ma photo sur les supports de communication internes et externes à la société" };
           
            string[] headerTabRgpd = new string[] { "Date and time", "Photo", "Filter", "Type of format", "Printed", "Number of printing", "Green screen", "Photo time", "Print time", "Name", "First name", "E-mail", "Postal code", "Country", "Accept","Gift","Participaton Time", "relay point" };

            string finalHeader = "";
            
            for (int i = 0; i < headerTabRgpd.Length; i++)
            {
                finalHeader += headerTabRgpd[i] + ",";
             }
            
            

            //finalHeader += "\r\n";
            string dateHeure = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            string image = System.IO.Path.GetFileName(Globals.printedImage);
            string eventName = "";
            switch (Globals.EventChosen)
            {
                case 0:
                    eventName = "CADRE";
                    break;
                case 1:
                    eventName = "STRIP";
                    break;
                case 2:
                    eventName = "POLAROID";
                    break;
                case 3:
                    eventName = "MULTISHOOT";
                    break;
            }
            string bgChromaKey = Globals.greenScreen;
            if (String.IsNullOrEmpty(bgChromaKey) && Globals.gswithoutframe)
            {
                bgChromaKey = "Without frame";
            }
            else
            {
                bgChromaKey = "";
            }
            DateTime photoTime = DateTime.MinValue;
            if (Globals.dtTakenPhoto != DateTime.MinValue)
            {
                photoTime = Globals.dtTakenPhoto;
            }
            DateTime printTime = DateTime.MinValue;
            if (Globals.dtPrint != DateTime.MinValue)
            {
                printTime = Globals.dtPrint;
            }
            string dateHeurePrint = "";
            string dateHeurePhoto = "";
            if (printTime != DateTime.MinValue)
            {
                dateHeurePrint = printTime.ToString("dd/MM/yy HH:mm");
            }
            if (photoTime != DateTime.MinValue)
            {
                dateHeurePhoto = photoTime.ToString("dd/MM/yy HH:mm");
            }
            string printed = "NON";

            using (StreamWriter sw = File.AppendText(Result))
            {
                string toWrite = dateHeure + "," + image + "," + Globals.filterChosen + "," + eventName + "," + printed + "," + 1 + "," + bgChromaKey + "," + dateHeurePhoto + "," + dateHeurePrint + ",";
                if (newFile)
                {
                    sw.WriteLine(finalHeader);
                }

                sw.WriteLine(toWrite);
                sw.Close();
                sw.Dispose();
            }
        }

        private FileInfo GetFiltred()
        {
            var vm = (WinnerPageViewModel)DataContext;
            var uri = vm.selectedFilter;
            if (string.IsNullOrEmpty(uri))
            {
                uri = Globals.imageChosen;
            }
            if (uri.IndexOf("file:") >= 0)
            {
                int strindex = "file:///".Length;
                int len = uri.Length - strindex;
                uri = uri.Substring(strindex, len);
            }
            var image = new FileInfo(uri);
            return image;
        }

        private void savingData()
        {
            var image = Globals.imageChosen;
            string imgName = Globals.imageChosen;
            string shortName = System.IO.Path.GetFileNameWithoutExtension(Globals.imageChosen);
            DateTime date = DateTime.Now;
            int year = date.Year;
            int month = date.Month;
            int day = date.Day;
            int hour = date.Hour;
            int minute = date.Minute;
            string second = date.Second.ToString();
            if (second.Length == 1)
            {
                second = "0" + second;
            }
            string OriginalFinalDirectory = Globals._finalFolder + "\\Original\\" + System.IO.Path.GetFileName(Globals.imageChosen);
            string longName = "EVENT_" + Globals.GetEventId() + "__DTM_" + year + "-" + month + "-" + day + "-" + hour + minute + second + ".jpg";
            Globals.printedImage = longName;
            if (Globals.gswithoutframe)
            {
                if (!Directory.Exists(Globals.EventMediaFolder() + "\\Photos\\GSNoFrame"))
                {
                    Directory.CreateDirectory(Globals.EventMediaFolder() + "\\Photos\\GSNoFrame");
                }
            }

            if (Globals.EventChosen != 1)
            {
                if (File.Exists(Globals.imageChosen) && Directory.Exists(Globals.nextcloudPath))
                {
                    File.Copy(Globals.imageChosen, Globals.EventMediaFolder() + "\\Photos\\" + longName);
                    if (Globals.gswithoutframe)
                    {
                        File.Copy(Globals.imageChosen, Globals.EventMediaFolder() + "\\Photos\\GSNoFrame\\" + longName);
                    }
                }
            }
            else
            {
                File.Copy(OriginalFinalDirectory, Globals.EventMediaFolder() + "\\Photos\\Original\\" + longName);
                if (Globals.gswithoutframe)
                {
                    File.Copy(OriginalFinalDirectory, Globals.EventMediaFolder() + "\\Photos\\GSNoFrame" + longName);
                }
                File.Copy(Globals.imageChosen, Globals.EventMediaFolder() + "\\Photos\\" + longName);
            }
        }

        private void gotoRgpdText(object sender, RoutedEventArgs e)
        {
            scrollCoordonateRGPD.Visibility = Visibility.Collapsed;
            if (_Name.Text == "" || _Name.Text == "Nom")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_lastname.Text == "" || _lastname.Text == "Prénom")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_emailRgpd.Text == "" || _emailRgpd.Text == "E-mail *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_postalCode.Text == "" || _postalCode.Text == "Code postal")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (!checkMailAdress(_emailRgpd.Text))
            {
                coordonneeMandatoryTitle.Text = "Veuillez saisir une adresse e-mail valide.";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else if (_country.Text == "" || _country.Text == "E-mail *")
            {
                coordonneeMandatoryTitle.Text = "Veuillez remplir le(s) champ(s) obligatoire(s). (*)";
                scrollCoordonateMandatory.Visibility = Visibility.Visible;
                current_form = "rgpdcoordonate";
            }
            else
            {
                
                form_rgpd_Name = _Name.Text;
                form_rgpd_lastname = _lastname.Text;
                form_rgpd_email = _emailRgpd.Text;
                form_rgpd_postalcode = _postalCode.Text;
                form_rgpd_country = _country.Text;
                scrollRgpd.Visibility = Visibility.Visible;
            }
        }

        private bool checkMailAdress(string adress)
        {
            string strEMail = adress;
            Regex regx = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regx.Match(strEMail);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private string getBackGroundImage()
        {
            string bg_image = "";
            switch (Globals.gift_index)
            {
                case 0:
                    bg_image = "BG_BONNETALPES.jpg";
                    break;
                case 1:
                    bg_image = "BG_BONNETLASAUZE.jpg";
                    break;
                case 2:
                    bg_image = "BG_BONNETLESORRES.jpg";
                    break;
                case 3:
                    bg_image = "BG_BONNETPRALOUP.jpg";
                    break;
                case 4:
                    bg_image = "BG_CINEMAPRALOUP.jpg";
                    break;
                case 5:
                    bg_image = "BG_FLASQUEALPES.jpg";
                    break;
                case 6:
                    bg_image = "BG_FORFAITCHAMPSAUR.jpg";
                    break;
                case 7:
                    bg_image = "BG_FORFAITDEVOLUY.jpg";
                    break;
                case 8:
                    bg_image = "BG_FORFAITLESAUZE.jpg";
                    break;
                case 9:
                    bg_image = "BG_FORFAITPRALOUP.jpg";
                    break;
                case 10:
                    bg_image = "BG_MUGSALPES.jpg";
                    break;
                case 11:
                    bg_image = "BG_SACOCHEALPES.jpg";
                    break;
                case 12:
                    bg_image = "BG_SKIPASSSERRECHEVALIER.jpg";
                    break;
                case 13:
                    bg_image = "BG_TOTEBAGEDEVOLUY.jpg";
                    break;
                case 14:
                    bg_image = "BG_TOURDECOUUBAYE.jpg";
                    break;
            }

            return $"C:\\Events\\Assets\\" + Globals.GetEventId() + "\\Backgrounds\\Gagnants\\" + bg_image;
        }

        private void hideFormulaire(object sender, RoutedEventArgs e)
        {
            timerCountdown.Start();
            _emailRgpd.Text = "E-mail *";
            _lastname.Text = "Prénom *";
            _Name.Text = "Nom *";
            _postalCode.Text = "Code postal *";
            _country.Text = "";
            scrollCoordonateMandatory.Visibility = Visibility.Collapsed;
            scrollRgpd.Visibility = Visibility.Collapsed;
            scrollCoordonateRGPD.Visibility = Visibility.Collapsed;
        }

        private string getGift()
        {
            string gift_image = "";
            switch (Globals.gift_index)
            {
                case 0:
                    
                    gift_image = "coupon_paysage_bonnetalpes.jpg";
                    break;
                case 1:
                    
                    gift_image = "coupon_paysage_bonnetlesauze.jpg";
                    break;
                case 2:
                    
                    gift_image = "coupon_paysage_bonnetlesorres.jpg";
                    break;
                case 3:
                    
                    gift_image = "coupon_paysage_bonnetpraloup.jpg";
                    break;
                case 4:
                    
                    gift_image = "coupon_paysage_cinemapraloup.jpg";
                    break;
                case 5:
                    
                    gift_image = "coupon_paysage_flasquealpes.jpg";
                    break;
                case 6:
                    
                    gift_image = "coupon_paysage_forfaitchampsaur.jpg";
                    break;
                case 7:
                    
                    gift_image = "cadre_paysage_forfaitdevoluy.jpg";
                    break;
                case 8:
                    
                    gift_image = "coupon_paysage_forfaitlesauze.jpg";
                    break;
                case 9:
                    
                    gift_image = "coupon_paysage_forfaitpraloup.jpg";
                    break;
                case 10:
                    
                    gift_image = "coupon_paysage_mugsalpes.jpg";
                    break;
                case 11:
                    
                    gift_image = "coupon_paysage_sacochealpes.jpg";
                    break;
                case 12:
                    
                    gift_image = "coupon_paysage_skipassserrechevalier.jpg";
                    break;
                case 13:
                   
                    gift_image = "cadre_paysage_tourdecouubaye.jpg";
                    break;
                case 14:
                    
                    gift_image = "coupon_paysage_totebagdevoluy.jpg";
                    break;
            }

            return $"C:\\Events\\Assets\\" + Globals.GetEventId() + "\\Frames\\" + gift_image;
        }

        public int getNbPrinted()
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            return mgrBin.readIntData(binFileName);
        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {
            var nbToPrint = 1;
            var nbPrintFinal = getNbPrinted() + nbToPrint;
            nbmaxprinting = Convert.ToInt32(_inimanager.GetSetting("PRINTING", "eventauthorizedprint"));
            if ((nbmaxprinting == 0) || (nbmaxprinting != 0 && (nbPrintFinal > nbmaxprinting)))
            {
                MessageBox.Show("Nombre d'impression autorisée atteint !!", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Globals.dtPrint = DateTime.Now;

                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
               
                    lbl_CurrentPrinting.Content = "Impression en cours....";

                    //nbPrinted = 1;
                    nbPrintFinal = getNbPrinted() + 1;
                    vbtnImprimer.IsEnabled = (nbmaxprinting != 0 && (nbPrintFinal < nbmaxprinting));
                    int toWrite = mgrBin.readIntData(binFileName);
                    toWrite += 1;
                    //writeTxtFileData(mediaDataPath,"");
                    writeTxtFileData(mediaDataPath, toWrite.ToString());
                    mgrBin.writeData(toWrite, binFileName);
                    Printing(false);
                }


        }

        public void CoordonneeTextBoxRgpdGetFocused(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            //textbox.Text = ""; 
            if (string.IsNullOrEmpty(textbox.Text) || textbox.Text.Trim() == "Nom *" || textbox.Text.Trim() == "Prénom *" || textbox.Text.Trim() == "E-mail *" || textbox.Text.Trim() == "Code postal *" || textbox.Text.Trim() == "Pays *")
            {
                textbox.Text = "";
            }
            string tag = textbox.Tag.ToString();
            //string tag = hash[type];

            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboardRgpd.Visibility = Visibility.Collapsed;
                numeriqueKeyboardRgpd.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboardRgpd.Visibility = Visibility.Visible;
                numeriqueKeyboardRgpd.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboardRgpd.Visibility = Visibility.Visible;
                alphanumeriquekeyboardRgpd.ActiveContainer = textbox;
                alphanumeriquekeyboardRgpd.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboardRgpd.Visibility = Visibility.Collapsed;
            }

        }

        public void CoordonneeTextBoxRgpdLostFocused(object sender, EventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            string keyName = "key_" + textbox.Name;
            if (string.IsNullOrEmpty(textbox.Text) || textbox.Text == "Nom *" || textbox.Text == "Prénom *" || textbox.Text == "E-mail *" || textbox.Text == "Code postal *" || textbox.Text.Trim() == "Pays *")
            {
                string temp = hashTextBox[keyName];
                textbox.Text = temp;
            }
            alphanumeriquekeyboardRgpd.Visibility = Visibility.Collapsed;
            numeriqueKeyboardRgpd.Visibility = Visibility.Collapsed;
        }

        private void writeTxtFileData(string filename, string toWrite)
        {
            int i = 0;

            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(Globals.imageChosen);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void PrintPageGift(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(getGift());
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        private void Printing(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;
                    TypeEvent chosenEvent;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();
                    chosenEvent = (TypeEvent)Globals.EventChosen;

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    short copies = 1;

                    printerSettings.Copies = copies;

                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrinterSettings.Copies = copies;
                                pd.PrintPage += PrintPage;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                            lbl_CurrentPrinting.Content = "";
                        }));

                        if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                        {
                            ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                            comMgr.writeData("printed");
                        }
                        string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    lbl_CurrentPrinting.Content = "";
                    BinaryFileManager mgrBin = new BinaryFileManager();
                    string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
                    
                }));

            });

        }

        private void PrintingGift(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    string printerName;
                    TypeEvent chosenEvent;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();
                    chosenEvent = (TypeEvent)Globals.EventChosen;

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    short copies = 1;

                    printerSettings.Copies = copies;

                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrinterSettings.Copies = copies;
                                pd.PrintPage += PrintPageGift;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                            lbl_CurrentPrinting.Content = "";
                        }));

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    lbl_CurrentPrinting.Content = "";
                   

                }));

            });

        }

    }
}
