﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for USBCameraManquant.xaml
    /// </summary>
    public partial class USBCameraManquant : UserControl
    {
        private DispatcherTimer timerCountdown;
        private int timeout = 20;
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }

        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }
        private FilterInfo _currentDevice;
        public USBCameraManquant()
        {
            InitializeComponent();
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(Globals._defaultImageCamera);
            bitmap.EndInit();
            imageCamera.Source = bitmap;
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        var viewModel = (USBCameraManquantViewModel)DataContext;
                        Globals._FlagConfig = "client";
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                viewModel.GoToClientSumarySpherik.Execute(null);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            if (viewModel.GoToClientSumary.CanExecute(null))
                                viewModel.GoToClientSumary.Execute(null);
                        }
                        break;
                    case Key.F2:
                        Globals._FlagConfig = "admin";
                        var viewModel2 = (USBCameraManquantViewModel)DataContext;
                        if (viewModel2.GoToConnexionConfig.CanExecute(null))
                            viewModel2.GoToConnexionConfig.Execute(null);
                        break;

                }
            }
            catch (Exception ex)
            {

            }
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {

            timeout--;
            if (timeout > 0)
            {
                if (RefreshCamera())
                {
                    timerCountdown.Stop();
                    GoToChoiceEvent();
                }
            }
            if (timeout == -1)
            {
                timeout = 20;
            }
        }

        private bool RefreshCamera()
        {
            bool found = false;
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (VideoDevices.Any())
            {
                foreach (var _videoDevice in VideoDevices)
                {
                    if (_videoDevice.Name.ToLower().Equals("usb camera"))
                    {
                        found = true;
                        CurrentDevice = _videoDevice;
                    }
                }
            }
            return found;
        }

        public void exitSoftware(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Reload(object sender, RoutedEventArgs e)
        {
            if (RefreshCamera())
            {
                timerCountdown.Stop();
                GoToChoiceEvent();
            }
        }

        public void GoToChoiceEvent(object sender, RoutedEventArgs e)
        {
            var vm = (USBCameraManquantViewModel)DataContext;
            if (vm.GoToChoiceEvent.CanExecute(null))
                vm.GoToChoiceEvent.Execute(null);
        }

        public void GoToChoiceEvent()
        {
            var vm = (USBCameraManquantViewModel)DataContext;
            if (vm.GoToChoiceEvent.CanExecute(null))
                vm.GoToChoiceEvent.Execute(null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
