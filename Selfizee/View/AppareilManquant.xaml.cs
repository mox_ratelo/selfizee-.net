﻿using EOSDigital.API;
using Selfizee.Managers;
using Selfizee.Models;
using Selfizee.Models.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for AppareilManquant.xaml
    /// </summary>
    public partial class AppareilManquant : UserControl
    {
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private DispatcherTimer timerCountdown;
        private CanonAPI APIHandler;
        private int timeout = 20;
        public AppareilManquant()
        {
            InitializeComponent();
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("NoCamera");
            //Bitmap bitSel = new Bitmap(Globals._defaultImageSelfizee);
            //bitSel.MakeTransparent(System.Drawing.Color.White);
            //bitSel.Save(Globals._defaultImageSelfizee);

            Globals.leaveHome = false;
            if (backgroundAttributes.IsImage)
            {
                //ImageBrush imgBrush = new ImageBrush();
                //var fileName = Globals.LoadBG(TypeFond.ForNoCamera);
                ////if (!File.Exists(fileName))
                ////{
                ////    fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                ////}
                ////imgBrush.ImageSource = new BitmapImage(new Uri(fileName, UriKind.Absolute));
                ////imgBrush.Stretch = Stretch.Fill;
                Globals._currentChromakeyBG = "";
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Globals._defaultImageCamera);
                bitmap.EndInit();
                imageCamera.Source = bitmap;
                //bgImage.Background = imgBrush;
            }

            //if (!backgroundAttributes.EnableTitle)
            //    accueilTitle.Visibility = Visibility.Collapsed;

            if (!backgroundAttributes.IsImage)
            {
                if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                {
                    var bgcolor = backgroundAttributes.BackgroundColor;
                    var bc = new BrushConverter();
                    this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                }

            }
            APIHandler = new CanonAPI();
            LaunchTimerCountdown();
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        var viewModel = (AppareilManquantViewModel)DataContext;
                        Globals._FlagConfig = "client";
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                viewModel.GoToClientSumarySpherik.Execute(null);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            if (viewModel.GoToClientSumary.CanExecute(null))
                                viewModel.GoToClientSumary.Execute(null);
                        }
                        break;
                    case Key.F2:
                        Globals._FlagConfig = "admin";
                        var viewModel2 = (AppareilManquantViewModel)DataContext;
                        if (viewModel2.GoToConnexionConfig.CanExecute(null))
                            viewModel2.GoToConnexionConfig.Execute(null);
                        break;

                }
            }
            catch (Exception ex)
            {

            }
        }

        void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        private bool RefreshCamera()
        {
            List<Camera> cameraList = APIHandler.GetCameraList();
            if (cameraList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Reload(object sender, RoutedEventArgs e)
        {
            if (RefreshCamera())
            {
                timerCountdown.Stop();
                GoToChoiceEvent();
            }
        }

            void timerCountdown_Tick(object sender, EventArgs e)
        {

            timeout--;
            if (timeout > 0)
            {
                if (RefreshCamera())
                {
                    timerCountdown.Stop();
                    GoToChoiceEvent();
                }
            }
            if (timeout == -1)
            {
                timeout = 20;
            }
        }

        public void exitSoftware(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        public void GoToChoiceEvent(object sender, RoutedEventArgs e)
        {
            var vm = (AppareilManquantViewModel)DataContext;
            if (vm.GoToChoiceEvent.CanExecute(null))
                vm.GoToChoiceEvent.Execute(null);
        }

        public void GoToChoiceEvent()
        {
            var vm = (AppareilManquantViewModel)DataContext;
            if (vm.GoToChoiceEvent.CanExecute(null))
                vm.GoToChoiceEvent.Execute(null);
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
    }
}
