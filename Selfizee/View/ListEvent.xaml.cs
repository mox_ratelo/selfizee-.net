﻿using log4net;
using Newtonsoft.Json;
using Selfizee.Managers;
using Selfizee.Models;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for ListEvent.xaml
    /// </summary>
    public partial class ListEvent : UserControl //,INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        List<string> lstEventCode = new List<string>();
        List<string> lstArchive = new List<string>();
        string ftpServerIP = "37.187.132.132";
        string ftpUserName = "dev@upload.selfizee.fr";
        private bool connectedButNowifi = false;
        List<string> lstCurrentUpdate = new List<string>();
        string ftpUserNameEvent = "dev@sync.selfizee.fr"; //"syncdev"; //"sync@uploadv2.selfizee.fr";
        FtpManager _ftpManager = new FtpManager();
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        static AbortableBackgroundWorker Synchro_worker = null ;
        static AbortableBackgroundWorker Update_worker = null;
        
        string idborne = "";
        string code_todelete = "";
        string codeArchive_todelete = "";
        string code_toarchive = "";
        string code_torestore = "";
        string configIniPath = string.Empty;
        string ftpPassword = "qB06JE0K#PvVB4^j7Ahxq";
        string ftpPasswordEvent = "[2WLR1FHxCZmXxUKr7>dc[=4";//"^G_yvAY@n~$A}}op<B(*JLN6"; //"T2=-rB1yn}MJ?~D?Yb51n$4M"; //"S99e)%VxI?q*5*q_$6zK/8k/";
        private double widthWorkArea = SystemParameters.WorkArea.Width;
        //private IEnumerable<string> lstPhotosNotSynchronized;
        private IEnumerable<string> lstImgEventsNotSynchronized;
        private GridLengthConverter convGridLength = new GridLengthConverter();
       // private DispatcherTimer dispatcherTimer;
        private bool isArchive = false;
        private static Wifi wifi;
        private CancellationTokenSource cts;
        private bool autoupdate = false;
        bool connected = false;

        private string url = "https://booth.selfizee.fr/api/evenements/setIsSynchro.json";
        
        public double valueProgress;
        public int timeout;
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        public string code { get; set; }
        static bool stopSynchro = false;
        bool continuSynchro = true;
        List<DispatcherOperation> LastOperations ;
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            GC.SuppressFinalize(this);
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            KillMe();
            var viewModel = (ListEventViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        private void StopUpdating()
        {
            foreach (var operation in LastOperations)
            {
                if (operation != null)
                {
                    if(operation.Status == DispatcherOperationStatus.Executing)
                    {
                        operation.Wait();
                    }
                    var res = operation.Abort();
                   
                           
                }
            }
            


            LastOperations.Clear();
        }

        public ListEvent()
        {
            try
            {
                

                wifi = new Wifi();
                InitializeComponent();
                LastOperations = new List<DispatcherOperation>();
                LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ClosePanel();
                })));
                
                stopSynchro = false;

                Globals._FlagConfig = "admin";
                ImageBrush btnExit = new ImageBrush();
                btnExit.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnExit, UriKind.Absolute));

                ImageBrush btnReglage = new ImageBrush();
                btnReglage.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnReglages, UriKind.Absolute));
                btn_Reglage.Background = btnReglage;
                btn_Exit.Background = btnExit;

                code = Globals.codeEvent_toEdit;//
                if (string.IsNullOrEmpty(code))
                {
                    code = Globals.GetEventId();
                }
                if (!string.IsNullOrEmpty(code))
                {
                    INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
                    timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
                }
                else
                {
                    timeout = 80;
                }

                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string localVersion = name.Version.ToString();
                numVersion.Content = localVersion;

                Flag = Status();
                wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;
                AccessPoint accessPoint;
                try
                {
                    accessPoint = wifi.GetAccessPoints().Where(ap => ap.IsConnected == true).First();
                }
                catch(Exception e)
                {
                    accessPoint = null;
                }
                
                if (InternetConnectionManager.checkConnection("www.google.com"))
                {
                    connected = true;
                }
                ImageBrush btnConnected = new ImageBrush();
                img_connected.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoConnect, UriKind.Absolute));
                if(wifi.ConnectionStatus == WifiStatus.Connected && connected)
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c")); 
                    img_connected.Visibility = Visibility.Visible;
                    connectedButNowifi = true;
                    if (accessPoint != null)
                    {
                        wifi_connected.Content = accessPoint.Name;
                    }
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    ImageBrush btnwifi = new ImageBrush();
                    img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifi, UriKind.Absolute));
                }
                else if (wifi.ConnectionStatus == WifiStatus.Disconnected && connected)
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    img_connected.Visibility = Visibility.Visible;
                    connectedButNowifi = false;
                    wifi_connected.Content = "Connecté";
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    //ImageBrush btnwifi = new ImageBrush();
                    //img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifi, UriKind.Absolute));
                    img_wifi.Visibility = Visibility.Hidden;
                }
                else
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    img_connected.Visibility = Visibility.Hidden;
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    wifi_connected.Content = "Non connecté";
                    connectedButNowifi = false;
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    ImageBrush btnwifiDisconnect = new ImageBrush();
                    img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifiOff, UriKind.Absolute));
                }
                
                idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
                numBorne.Content = idborne;

                Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
                //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                //SetListData();
                //List_event.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
                add_event.Foreground =  (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d")); //System.Windows.Media.Brushes.White;
                //AddListToStackPanel().GetAwaiter().GetResult();
                selfizeeLogoBlack.Source = new BitmapImage(new Uri(Globals._defaultLogoNoir, UriKind.Absolute));

                

                ImageBrush btnOk = new ImageBrush();
                btnOk.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnOkPopup, UriKind.Absolute));
                continuePopup.Background = btnOk;

                ImageBrush btnYes = new ImageBrush();
                btnYes.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnyesPopup, UriKind.Absolute));
                yesArchive.Background = btnYes;
                yesDelete.Background = btnYes;
                yesArchiveDelete.Background = btnYes;
                yesRemettreArchive.Background = btnYes;

                ImageBrush btnNo = new ImageBrush();
                btnNo.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnnoPopup, UriKind.Absolute));
                noArchive.Background = btnNo;
                noDelete.Background = btnNo;
                noArchiveDelete.Background = btnNo;
                noRemettreArchive.Background = btnNo;

                ImageBrush btnCroix = new ImageBrush();
                btnCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Absolute));
                hidePopUp.Background = btnCroix;
                hidePopUpArchive.Background = btnCroix;
                hidePopUpdelete.Background = btnCroix;
                hidePopUpRemettreArchive.Background = btnCroix;


                scrollRemettreArchive.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollRemettreArchive.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

                scrollArchive.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollArchive.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

                scrollDelete.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollDelete.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

                scrollArchiveDelete.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollArchiveDelete.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

                scrollPopuUpUpdate.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollPopuUpUpdate.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                GetEventList(isArchive);
                isArchive = true;
                GetEventList(isArchive);
                isArchive = false;
                
                //createWorker();
                AddListToStackPanel(null,isArchive); //.GetAwaiter().GetResult();
                if (Globals.showUpdateSoftware)
                {
                    Globals.showUpdateSoftware = false;
                    AutoUpdate();
                }
                
                imgCercle.Visibility = Visibility.Hidden;

                lbl_loading.Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {
                Log.Error("ListEvent error: " + ex);
            }
            
        }

        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            if (!string.IsNullOrEmpty(code))
            {
                INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
                timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            }
            else
            {
                timeout = 80;
            }
            
            LaunchTimerAfter2();
        }

        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();
        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    if (timerCountdown != null) timerCountdown.Stop();
                    if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void GotoAccueil()
        {
            if(timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            KillMe();
            var viewModel = (ListEventViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            if (chk_con())
            {
                backgroundWorker1_DoWork();
                update_worker_event();


            }
            LaunchTimerAfter2();

        }
        private void update_worker_event()
        {
            backgroundWorkerUpdate_DoWork();

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //for (; !e.Cancel;)
            //{
                SynchronizeEvents();
            //}
        }

        private void backgroundWorker1_DoWork()
        {
            //for (; !e.Cancel;)
            //{
            SynchronizeEvents();
            //}
        }
        /*
                private void backgroundWorkerUpdate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
                {
                }

                private void backgroundWorkerUpdate_ProgressChanged(object sender, ProgressChangedEventArgs e)
                {
                   // lbl_loading.Visibility = Visibility.Visible;
                }*/

        public void createWorker()
        {
            if (chk_con())
            {
                if (String.IsNullOrEmpty(Globals.auth_token))
                {
                    GetToken(Globals.ws_login, Globals.ws_pwd);
                }
               /* Synchro_worker = new AbortableBackgroundWorker();
                Synchro_worker.DoWork += backgroundWorker1_DoWork;
                Synchro_worker.WorkerSupportsCancellation = true;
                Synchro_worker.WorkerReportsProgress = true;
                */
                backgroundWorker1_DoWork();
                backgroundWorkerUpdate_DoWork();
            }
            
        }

        private void launch_update_worker()
        {


            Update_worker = new AbortableBackgroundWorker();
            Update_worker.DoWork += backgroundWorkerUpdate_DoWork;
            Update_worker.WorkerSupportsCancellation = true;
            Update_worker.WorkerReportsProgress = true;
           // Update_worker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorkerUpdate_ProgressChanged);
            //Update_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerUpdate_RunWorkerCompleted);
        }

        public void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            stopSynchro = true;
          /*  if(Synchro_worker != null)
            {
                Synchro_worker.WorkerSupportsCancellation = true;
                Synchro_worker.CancelAsync();
                Synchro_worker.Abort();
                Synchro_worker.Dispose();
                
                Synchro_worker = null;
            }
            if(Update_worker != null)
            {
                Update_worker.WorkerSupportsCancellation = true;
                Update_worker.CancelAsync();
                Update_worker.Abort();
                Update_worker.Dispose();
                
                Update_worker = null;
            }*/
            StopUpdating();

            //  System.GC.Collect();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();

        }

        private void backgroundWorkerUpdate_DoWork(object sender, DoWorkEventArgs e)
        {
            //for (; !e.Cancel;)
            //{
            CheckeventsUpdate();
            //}
        }

        private void backgroundWorkerUpdate_DoWork()
        {
            //for (; !e.Cancel;)
            //{
            CheckeventsUpdate();
            //}
        }

        public void SynchronizeEvents()
        {
            if (!isArchive)
            {
                SynchroEventList(cts, isArchive); // FAUT PAS METTRE : .GetAwaiter().GetResult();

            }
        }

        public void CheckUpdateEvents()
        {
            //Dispatcher.BeginInvoke(new Action(() =>
            //{
                MiseAJourEventList(cts, isArchive); // FAUT PAS METTRE : .GetAwaiter().GetResult();

            //}), DispatcherPriority.ContextIdle, null);

        }

        public void AutoUpdate()
        {
            //FtpManager ftpMgr = new FtpManager();
            var UpdateW = new UpdateWindow();

            if (!CheckUpdate(UpdateW.RemoteVersion) && InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                LastOperations.Add(this.Dispatcher.BeginInvoke(new Action(delegate
                {
                    UpdateW.ShowDialog();
                    bool updateResult = UpdateW.FlagDownload;

                    string urlExe = UpdateW.ExeLink;

                    if (!updateResult)
                    {
                        this.IsEnabled = true;
                        //vue erreur connexion
                    }
                    else
                    {
                        if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                        {
                            //KillMe();
                            this.IsEnabled = false;
                            autoupdate = true;
                            //var proUpdateW = new ProgressUpdateWindow(filesize, ftpFileNamePath, username, password);
                            var proUpdateW = new ProgressUpdateWindow(urlExe, Globals._defaultUpdateDirectory);
                            proUpdateW.ShowDialog();
                        }
                        else
                        {
                            var noConexW = new NoConnexionWindow();
                            noConexW.ShowDialog();
                        }
                    }
                })));
                
               
            }

        }

        public static bool CheckUpdate(string remoteVersion)
        {
            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                //string remoteVersion = ftpMgr.GetFtpFileVersion();
                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string localVersion = name.Version.ToString();
                if (localVersion == remoteVersion)
                {
                    return true;
                }
                else { return false; }
            }
            return false;
        }

        private void hideFormulaire(object sender, RoutedEventArgs e)
        {
            scrollPopuUpUpdate.Visibility = Visibility.Collapsed;
            scrollArchive.Visibility = Visibility.Collapsed;
            scrollDelete.Visibility = Visibility.Collapsed;
            scrollArchiveDelete.Visibility = Visibility.Collapsed;
        }

        private void OkUpdate(object sender, RoutedEventArgs e)
        {
            scrollPopuUpUpdate.Visibility = Visibility.Collapsed;
        }

        private void YesRemettreArchive(object sender, RoutedEventArgs e)
        {
            try { 

                    var eventSource = $"C:/Events/Assets/Archives/{code_torestore}";
                    var eventTarget = $"C:/Events/Assets/{code_torestore}";


                    DirectoryInfo diSource = new DirectoryInfo(eventSource);
                    DirectoryInfo diTarget = new DirectoryInfo(eventTarget);

                    CopyAll(diSource, diTarget);

                    CacherEventLine(code_torestore);

                    Directory.Delete(eventSource, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - remettre_Click ERROR : {ex}");
                }
            code_torestore = "";
            setEventsLabel();
            scrollRemettreArchive.Visibility = Visibility.Collapsed;
        }

        private void YesDelete(object sender, RoutedEventArgs e)
        {
            try
            {
                CacherEventLine(code_todelete);
                var eventFolderPath = $"C:/Events/Assets/{code_todelete}";
                Directory.Delete(eventFolderPath, true);
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - supprimer_Click ERROR : {ex}");
            }

            code_todelete = "";
            setEventsLabel();
            scrollDelete.Visibility = Visibility.Collapsed;
        }

        private void YesArchiveDelete(object sender, RoutedEventArgs e)
        {
            try
            {
                CacherEventLine(codeArchive_todelete);
                var eventFolderPath = $"C:/Events/Assets/Archives/{codeArchive_todelete}";
                Directory.Delete(eventFolderPath, true);
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - supprimer_Click ERROR : {ex}");
            }

            codeArchive_todelete = "";
            setEventsLabel();
            scrollArchiveDelete.Visibility = Visibility.Collapsed;
        }

        private void NoArchiveDelete(object sender, RoutedEventArgs e)
        {
            code_todelete = "";
            scrollArchiveDelete.Visibility = Visibility.Collapsed;
        }

        private void NoDelete(object sender, RoutedEventArgs e)
        {
            code_todelete = "";
            scrollDelete.Visibility = Visibility.Collapsed;
        }

        private void YesArchive(object sender, RoutedEventArgs e)
        {
            try
            {
                var eventSource = $"C:/Events/Assets/{code_toarchive}";
                var eventTarget = $"C:/Events/Assets/Archives/{code_toarchive}";


                DirectoryInfo diSource = new DirectoryInfo(eventSource);
                DirectoryInfo diTarget = new DirectoryInfo(eventTarget);

                CopyAll(diSource, diTarget);

                CacherEventLine(code_toarchive);

                Directory.Delete(eventSource, true);
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - archiver_Click ERROR : {ex}");
            }
        
            setEventsLabel();
            code_toarchive = "";
            scrollArchive.Visibility = Visibility.Collapsed;
        }

        private void NoRemettreArchive(object sender, RoutedEventArgs e)
        {
            code_torestore = "";
            scrollRemettreArchive.Visibility = Visibility.Collapsed;
        }

        private void NoArchive(object sender, RoutedEventArgs e)
        {
            code_toarchive = "";
            scrollArchive.Visibility = Visibility.Collapsed;
        }


        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }



        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(ListEvent));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));             
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            if (InternetConnectionManager.checkConnection("www.google.com"))
            {
                connected = true;
            }
            else
            {
                connected = false;
            }
            AccessPoint accessPoint;
            try
            {
                accessPoint = wifi.GetAccessPoints().Where(ap => ap.IsConnected == true).First();
            }
            catch (Exception ex)
            {
                accessPoint = null;
            }
            if (wifi.ConnectionStatus == WifiStatus.Connected && connected)
            {
                LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    img_connected.Visibility = Visibility.Visible;
                    connectedButNowifi = true;
                    if (accessPoint != null)
                    {
                        wifi_connected.Content = accessPoint.Name;
                    }
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    ImageBrush btnwifi = new ImageBrush();
                    img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifi, UriKind.Absolute));
                })));
               
            }
            else if (wifi.ConnectionStatus == WifiStatus.Disconnected && connected)
            {
                LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    img_connected.Visibility = Visibility.Visible;
                    connectedButNowifi = false;
                    wifi_connected.Content = "Connecté";
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4cad5c"));
                    //ImageBrush btnwifi = new ImageBrush();
                    //img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifi, UriKind.Absolute));
                    img_wifi.Visibility = Visibility.Hidden;
                })));
               
            }
            else
            {

                LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    wifiStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    img_connected.Visibility = Visibility.Hidden;
                    connexion.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    wifi_connected.Content = "Non connecté";
                    connectedButNowifi = false;
                    wifi_connected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3F3F3F"));
                    ImageBrush btnwifiDisconnect = new ImageBrush();
                    img_wifi.Source = new BitmapImage(new Uri(Globals._defaultBtnIcoWifiOff, UriKind.Absolute));
                })));
              
            }
        }
        #endregion

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
           // lbl_loading.Visibility = Visibility.Hidden;
            //Disable the timer
          //  dispatcherTimer.IsEnabled = false;
        }

        public void GetEventList(bool isarchive = false)
        {
            
            if (isArchive)
            {
                lstArchive.Clear();
                var pathArchives = "C:/Events/Assets/Archives";
                if (!Directory.Exists(pathArchives))
                    Directory.CreateDirectory(pathArchives);
                var di = new DirectoryInfo(pathArchives);
                var directories = di.EnumerateDirectories()
                                    .OrderByDescending(d => d.CreationTime)
                                    .Select(d => d.Name)
                                    .ToList();

                foreach (var d in directories)
                {
                    var dirName = new DirectoryInfo(d).Name;

                    lstArchive.Add(dirName);
                }
            }
            else
            {
                lstEventCode.Clear();
                var di = new DirectoryInfo(Globals._assetsFolder);
                var directories = di.EnumerateDirectories()
                                   .OrderByDescending(d => d.CreationTime)
                                   .Select(d => d.Name)
                                   .ToList();
                foreach (var d in directories) 
                {
                    var dirName = new DirectoryInfo(d).Name;
                    if (!dirName.ToLower().Equals("default") && !dirName.ToLower().Equals("archives"))
                    {
                        lstEventCode.Add(dirName);
                    }
                
                }
            }
        }

        private Task<int> getProgressSynchroValue(string code)
        {
            configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(configIniPath);
            //idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne");
            int fileNumber = 0;
            string ftpDirectoryPathCode = $"ftp://{this.ftpServerIP}/{idborne}/files/{code}/Photos";
            FtpWebRequest request1 = (FtpWebRequest)WebRequest.Create(ftpDirectoryPathCode);
            //request1.Timeout = 6000;
            //request1.ReadWriteTimeout = 6000;
            List <DirectoryItem> files1 = _ftpManager.GetDirectoryInformation(ftpDirectoryPathCode, ftpUserName, ftpPassword);

            foreach (var item1 in files1)
            {
                if (!item1.IsDirectory && item1.Name != "data.csv")
                {
                    fileNumber++;
                }

            }
            return Task.FromResult<int>(fileNumber);
        }

        private int getProgressLocalValue(string code)
        {
            int nb = 0;
            //string pathPhotos = Globals._MediaFolder + "\\" + code + "\\Photos";
            string pathEvents = $"C:/EVENTS/Assets/{code}";

            //nb += Directory.Exists(pathPhotos) ? Directory.GetFiles(pathPhotos).Length : 0;
            nb += Directory.Exists(pathEvents) ? Directory.GetFiles($"C:/EVENTS/Assets/{code}", "*.*", SearchOption.AllDirectories).Count() : 0;

            return nb;
        }

        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null) timerCountdown.Stop(); 
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            KillMe();
            if (lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                if (cts != null)
                {
                    cts.Cancel();
                }
                string code = (string)((Button)sender).Tag;
                //code = code.Substring(1, code.Length - 2);
                IniUtility iniFile = new IniUtility(Globals._appConfigFile);
                iniFile.Write("id", code, "EVENTSCONFIG");
                var viewModel = (ListEventViewModel)DataContext;
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
                //System.Windows.Forms.Application.Restart();
                //Environment.Exit(0);
            }
        }

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            if (connectedButNowifi)
            {
                KillMe();
                if (lstCurrentUpdate.Count > 0)
                {
                    string title = "Information";
                    string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                    bool result = CustomInformationBox.Prompt(title, message);
                }
                else
                {
                    if (cts != null)
                    {
                        cts.Cancel();
                    }
                    var viewModel = (ListEventViewModel)DataContext;
                    if (viewModel.GoToWifiList.CanExecute(null))
                        viewModel.GoToWifiList.Execute(null);
                }
            }
            else if(!connected && !connectedButNowifi)
            {
                KillMe();
                if (lstCurrentUpdate.Count > 0)
                {
                    string title = "Information";
                    string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                    bool result = CustomInformationBox.Prompt(title, message);
                }
                else
                {
                    if (cts != null)
                    {
                        cts.Cancel();
                    }
                    var viewModel = (ListEventViewModel)DataContext;
                    if (viewModel.GoToWifiList.CanExecute(null))
                        viewModel.GoToWifiList.Execute(null);
                }
            }
           
          
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            if(lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                if (cts != null)
                {
                    cts.Cancel();
                }
                var viewModel = (ListEventViewModel)DataContext;
                if (viewModel.GoToDownload.CanExecute(null))
                    viewModel.GoToDownload.Execute(null);
            }
            
        }

        private void Details_Click(object sender, RoutedEventArgs e)
        {
            var currentButton = (Button)sender;
            string code = (string)((Button)sender).Tag;
            //code = code.Substring(1, code.Length - 2);
            var child = FindChild(mainContainer, control =>
            {
                var Stack = control as StackPanel;
                if (Stack != null && Stack.Name == code)
                    return true;
                else
                    return false;
            }) as StackPanel;

            if (child.Visibility == Visibility.Collapsed)
            {
                ImageBrush btnBrush = new ImageBrush();
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsOpen, UriKind.Absolute));
                currentButton.Background = btnBrush;
                child.Visibility = Visibility.Visible;
            }
            else
            {
                ImageBrush btnBrush = new ImageBrush();
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsClosed, UriKind.Absolute));
                currentButton.Background = btnBrush;
                child.Visibility = Visibility.Collapsed;
            }

            var child1 = FindChild(mainContainer, control =>
            {
                string name = "stack1" + code.Split(new string[] { "stack" }, StringSplitOptions.None).Last();
                var Stack = control as StackPanel;
                if (Stack != null && Stack.Name == name)
                    return true;
                else
                    return false;
            }) as StackPanel;

            if (child1.Visibility == Visibility.Collapsed)
            {
                ImageBrush btnBrush = new ImageBrush();
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsOpen, UriKind.Absolute));
                currentButton.Background = btnBrush;
                child1.Visibility = Visibility.Visible;
            }
            else
            {
                ImageBrush btnBrush = new ImageBrush();
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsClosed, UriKind.Absolute));
                currentButton.Background = btnBrush;
                child1.Visibility = Visibility.Collapsed;
            }
        }

        public DependencyObject FindChild(DependencyObject parent, Func<DependencyObject, bool> predicate)
        {
            if (parent == null) return null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (predicate(child))
                {
                    return child;
                }
                else
                {
                    var foundChild = FindChild(child, predicate);
                    if (foundChild != null)
                        return foundChild;
                }
            }

            return null;
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            showLoader();
            KillMe();
            string code = (string)((Button)sender).Tag;
            Globals.codeEvent_toEdit = code;
            
            var viewModel = (ListEventViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private DateTime? DownloadInfoUpdate(string code)
        {
            string ftpfileName = $"ftp://37.187.132.132/{code}/Update/files.xml";
            string dirName = "C:\\Events\\Download\\" + code + "\\Remote\\Update\\";
            string fileName = "C:\\Events\\Download\\" + code + "\\Remote\\Update\\files.xml";
            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpfileName);
            //request.Timeout = 6000;
            //request.ReadWriteTimeout = 6000;
            //request.Credentials = new NetworkCredential(this.ftpUserNameEvent, this.ftpPasswordEvent);
            //request.Method = WebRequestMethods.Ftp.DownloadFile;
            DateTime? lastModifiedDate = null;
            try
            {
                Uri serverUri = new Uri(ftpfileName);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
                request.Credentials = new NetworkCredential(this.ftpUserNameEvent, this.ftpPasswordEvent);
                request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                lastModifiedDate = response.LastModified;
            }
            catch(WebException e)
            {
                lastModifiedDate = null;
            }
           
            //if (!Directory.Exists(dirName))
            //{
            //    Directory.CreateDirectory(dirName);
            //}
            //FtpWebResponse responseFileDownload = null;
            //try
            //{
            //    responseFileDownload = (FtpWebResponse)request.GetResponse();
            //    using (Stream responseStream = responseFileDownload.GetResponseStream())
            //    using (FileStream writeStream = new FileStream(fileName, FileMode.Create))
            //    {

            //        int Length = 2048;
            //        Byte[] buffer = new Byte[Length];
            //        int bytesRead = responseStream.Read(buffer, 0, Length);
            //        int bytes = 0;
            //        long fileSize = 60000;
            //        while (bytesRead > 0)
            //        {
            //            writeStream.Write(buffer, 0, bytesRead);
            //            bytesRead = responseStream.Read(buffer, 0, Length);
            //            bytes += bytesRead;// don't forget to increment bytesRead !
            //            int totalSize = (int)(fileSize) / 1000; // Kbytes
            //        }
            //    }
            //}
            //catch(Exception e)
            //{

            //}

            if (lastModifiedDate != null)
            {
                return lastModifiedDate;
            }
            return null;
        }



        private void EditTextblockEvent_Click(object sender, RoutedEventArgs e)
        {
            showLoader();
            string code = (string)((TextBlock)sender).Tag;
            
            KillMe();
            //code = code.Substring(1, code.Length - 2);
            Globals.codeEvent_toEdit = code;
            var viewModel = (ListEventViewModel)DataContext;
            if (Globals.ScreenType == "SPHERIK")
            {
                if (viewModel.GoToAdminSumarySpherik.CanExecute(null))
                    viewModel.GoToAdminSumarySpherik.Execute(null);
            }
            else if (Globals.ScreenType == "DEFAULT")
            {
                if (viewModel.GoToAdminSumary.CanExecute(null))
                    viewModel.GoToAdminSumary.Execute(null);
            }
        }

        public FileInfo GetNewestFile(DirectoryInfo directory)
        {
            if (directory.Exists)
            {
                return directory.GetFiles()
                .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
                .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
                .FirstOrDefault();
            }
            return null;
        }

        void hideLoader()
        {

            GIFCtrl.StopAnimate();
            GIFCtrl.Visibility = Visibility.Hidden;

        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            if (lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                if (cts != null)
                {
                    cts.Cancel();
                }
                var viewModel = (ListEventViewModel)DataContext;
                if (viewModel.GoToParametre.CanExecute(null))
                    viewModel.GoToParametre.Execute(null);
            }
        }


        void showLoader()
        {
          //  lbl_loading.Visibility = Visibility.Visible;
        }

        void showLoaderGIF()
        {
            LoaderGIFCtrl.Visibility = Visibility.Visible;
            LoaderGIFCtrl.StartAnimate();
        }
        void hideLoaderGIF()
        {
            LoaderGIFCtrl.StopAnimate();
            LoaderGIFCtrl.Visibility = Visibility.Hidden;
        }

        public int getNbPhotos(string code)
        {
            string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            if (Directory.Exists(photosDirectory))
            {
                return Directory.GetFiles(photosDirectory).Length;
            }
            return 0;
        }

        public int getNbPrinted(string code)
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            return mgrBin.readIntData(binFileName);
        }

        private void AddListToStackPanel(CancellationTokenSource cts,bool isarchive = false)
        {

            foreach (var code in lstEventCode)
            {
                //configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
                configIniPath = isarchive ? $"C:/Events/Assets/Archives/{code}//Config.ini" : Globals._assetsFolder + "\\" + code + "\\Config.ini";
                if (File.Exists(configIniPath))
                {
                    setEventsLabel();
                    Style buttonStyle = this.FindResource("noHighlight") as Style;
                    Style buttonRoudedStyle = this.FindResource("RoundButtonTemplate") as Style;

                    Style centerStackPanelContentStyle = this.FindResource("HorizontalStackPanel") as Style;
                    INIFileManager iniFile = new INIFileManager(configIniPath);
                    IniUtility _iniUtility = new IniUtility(configIniPath);
                    string Name = _iniUtility.Read("Name", "EVENT");
                    string codeEvent = _iniUtility.Read("code", "EVENT"); //iniFile.GetSetting("EVENT", "code");
                    string begin = _iniUtility.Read("Begin", "EVENT"); ///iniFile.GetSetting("EVENT", "Begin");
                    string end = _iniUtility.Read("End", "EVENT"); //iniFile.GetSetting("EVENT", "End");
                    string[] splitedBegin = begin.Split(new string[] { "/" }, StringSplitOptions.None);
                    string[] splitedEnd = end.Split(new string[] { "/" }, StringSplitOptions.None);
                    begin = splitedBegin[0] + "/" + splitedBegin[1] + "/" + splitedBegin[2].Substring(2, 2);
                    end = splitedEnd[0] + "/" + splitedEnd[1] + "/" + splitedEnd[2].Substring(2, 2);
                    string period = begin + " au " + end;

                    if (Globals.ScreenType == "SPHERIK")
                    {
                        Grid detailLineStack = new Grid();
                        detailLineStack.Margin = new Thickness(15, 0, 15, 0);
                        detailLineStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                        detailLineStack.Name = $"Detail_{code}";
                        detailLineStack.Height = 60;
                        detailLineStack.Width = widthWorkArea - 20;

                        StackPanel stackEventName = new StackPanel();
                        stackEventName.Margin = new Thickness(25, 0, 0, 0);
                        stackEventName.VerticalAlignment = VerticalAlignment.Center;
                        stackEventName.HorizontalAlignment = HorizontalAlignment.Left;
                        stackEventName.Orientation = Orientation.Horizontal;

                        TextBlock eventName = new TextBlock();
                        eventName.FontSize = 14;
                        //eventName.Height = 20;
                        eventName.MaxWidth = 200;
                        eventName.TextWrapping = TextWrapping.WrapWithOverflow;
                        eventName.Tag = codeEvent;
                        eventName.Foreground = System.Windows.Media.Brushes.White;
                        eventName.PreviewMouseDown += EditTextblockEvent_Click;
                        eventName.TextWrapping = TextWrapping.Wrap;
                        eventName.Margin = new Thickness(0, 0, 10, 0);
                        eventName.Inlines.Add(new Run(Name) { FontWeight = FontWeights.Bold });
                        eventName.VerticalAlignment = VerticalAlignment.Center;
                        eventName.HorizontalAlignment = HorizontalAlignment.Left;
                        stackEventName.Children.Add(eventName);

                        TextBlock roundSep = new TextBlock();
                        roundSep.Text = ".";
                        roundSep.FontSize = 40;
                        roundSep.Height = Double.NaN;
                        roundSep.VerticalAlignment = VerticalAlignment.Center;
                        roundSep.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep.Margin = new Thickness(15, -25, 15, 0);
                        stackEventName.Children.Add(roundSep);

                        TextBlock eventPeriod = new TextBlock();
                        eventPeriod.Text = period;
                        eventPeriod.HorizontalAlignment = HorizontalAlignment.Center;
                        eventPeriod.FontSize = 12;
                        eventPeriod.Foreground = System.Windows.Media.Brushes.White;
                        eventPeriod.Height = 15;
                        eventPeriod.Margin = new Thickness(0, 0, 20, 0);
                        stackEventName.Children.Add(eventPeriod);

                        Button btnDetails = new Button();
                        btnDetails.Name = "btnDetails";
                        btnDetails.Tag = "stack" + codeEvent;
                        btnDetails.Height = 27;
                        btnDetails.Margin = new Thickness(0, 0, 0, 0);
                        btnDetails.HorizontalAlignment = HorizontalAlignment.Left;
                        btnDetails.Style = buttonStyle;
                        btnDetails.Width = 100;
                        btnDetails.Click += Details_Click;
                        ImageBrush btnBrush = new ImageBrush();
                        btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsClosed, UriKind.Absolute));
                        btnDetails.Background = btnBrush;

                        stackEventName.Children.Add(btnDetails);

                        Button updateDispo = new Button();
                        updateDispo.FontSize = 10;
                        updateDispo.Height = 24;
                        //updateDispo.Width =90;// Double.NaN;

                        updateDispo.Name = $"maj_{code}";
                        updateDispo.Margin = new Thickness(10, 0, 0, 0);
                        updateDispo.VerticalAlignment = VerticalAlignment.Center;
                        updateDispo.Style = buttonRoudedStyle;
                        updateDispo.HorizontalAlignment = HorizontalAlignment.Left;
                        updateDispo.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFF")); //RED
                        updateDispo.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff0202"));
                        updateDispo.BorderBrush = new SolidColorBrush(Colors.Transparent);
                        updateDispo.BorderThickness = new Thickness(0);
                        updateDispo.Click += async (s, e) => await updateDispo_Clicked(s, e);
                        updateDispo.Visibility = Visibility.Hidden;
                        stackEventName.Children.Add(updateDispo);

                        detailLineStack.Children.Add(stackEventName);
                        /************FIN COLUMN 1**************/

                        /************DEBUT COLUMN 2**************/
                        StackPanel stackEventSynchro = new StackPanel();
                        stackEventSynchro.Orientation = Orientation.Horizontal;

                        TextBlock eventSynchro = new TextBlock();
                        eventSynchro.Name = $"synchro_{code}";
                        eventSynchro.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4dad5d"));
                        eventSynchro.Margin = new Thickness(0, 10, 10, 30);
                        eventSynchro.FontWeight = FontWeights.Bold;
                        eventSynchro.TextWrapping = TextWrapping.Wrap;
                        eventSynchro.HorizontalAlignment = HorizontalAlignment.Right;


                        TextBlock eventcode = new TextBlock();
                        eventcode.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        eventcode.Text = code.ToUpper();
                        eventcode.FontSize = 12;
                        eventcode.Height = 15;
                        eventcode.Width = 50;
                        eventcode.Tag = code;
                        eventcode.PreviewMouseDown += EditTextblockEvent_Click;
                        eventcode.TextWrapping = TextWrapping.Wrap;
                        eventcode.VerticalAlignment = VerticalAlignment.Center;

                        ProgressBar progres = new ProgressBar();
                        System.Windows.Media.Color color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#4dad5d");
                        progres.Foreground = new SolidColorBrush(color);
                        progres.Name = $"progress_{code}";
                        progres.Height = 11;
                        progres.Width = 120;
                        progres.BorderThickness = new Thickness(1);
                        progres.BorderBrush = System.Windows.Media.Brushes.Transparent;
                        progres.Margin = new Thickness(10, -50, 8, 0);
                        progres.Visibility = Visibility.Hidden;

                        StackPanel stackPanelColumn2 = new StackPanel();
                        stackPanelColumn2.Orientation = Orientation.Horizontal;
                        stackPanelColumn2.Children.Add(stackEventSynchro);
                        stackPanelColumn2.HorizontalAlignment = HorizontalAlignment.Left;
                        detailLineStack.Children.Add(stackPanelColumn2);
                        /************FIN COLUMN 2**************/

                        /************DEBUT COLUMN 3**************/
                        StackPanel rightButton = new StackPanel();
                        rightButton.Margin = new Thickness(0, 0, 40, 0);
                        rightButton.Orientation = Orientation.Horizontal;
                        rightButton.HorizontalAlignment = HorizontalAlignment.Right;
                        rightButton.Width = Double.NaN;

                        Button launchAnimation = new Button();
                        launchAnimation.Tag = codeEvent;
                        launchAnimation.Click += LaunchAnimation_Click;
                        launchAnimation.Name = "launchAnimation";
                        launchAnimation.Width = 120;
                        launchAnimation.Height = 20;
                        launchAnimation.Margin = new Thickness(10, 0, 35, 0);
                        launchAnimation.Style = buttonStyle;

                        ImageBrush btnLaunchAnimBrush = new ImageBrush();
                        btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
                        launchAnimation.Background = btnLaunchAnimBrush;
                        StackPanel stackSynchro = new StackPanel();
                        stackSynchro.Orientation = Orientation.Vertical;
                        stackSynchro.Children.Add(eventSynchro);
                        stackSynchro.Children.Add(progres);

                        rightButton.Children.Add(eventcode);
                        rightButton.Children.Add(stackSynchro);
                        rightButton.Children.Add(launchAnimation);
                        detailLineStack.Children.Add(rightButton);
                        /************FIN COLUMN 3**************/
                        mainContainer.Children.Add(detailLineStack);



                        string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";

                        StackPanel row2Container = new StackPanel();
                        row2Container.Margin = new Thickness(15, 0, 15, 0);
                        row2Container.Width = detailLineStack.Width - 40;
                        row2Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));

                        StackPanel stackTemp = new StackPanel();

                        stackTemp.Name = "stack" + codeEvent;
                        stackTemp.Orientation = Orientation.Horizontal;

                        int nbPhotos = getNbPhotos(codeEvent);
                        int nbprinted = getNbPrinted(codeEvent);

                        string dtlPhotos = (nbPhotos > 1) ? $"PHOTOS" : $"PHOTO";
                        string dtlPrinted = (nbprinted > 1) ? $"IMPRESSIONS" : $"IMPRESSION";
                        FileInfo newestFile = GetNewestFile(new DirectoryInfo(photosDirectory));
                        string dateLastFile = "";
                        if (newestFile != null)
                        {
                            DateTime lastCrea = newestFile.CreationTime;
                            string day = lastCrea.Day.ToString();
                            string month = lastCrea.Month.ToString();
                            string hour = lastCrea.Hour.ToString();
                            string minute = lastCrea.Minute.ToString();
                            if (day.Length == 1)
                            {
                                day = "0" + day;
                            }
                            if (month.Length == 1)
                            {
                                month = "0" + month;
                            }
                            if (hour.Length == 1)
                            {
                                hour = "0" + hour;
                            }
                            if (minute.Length == 1)
                            {
                                minute = "0" + minute;
                            }
                            //dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString().Substring(2, 2) + " à " + hour + "h" + minute;
                            dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString() + " à " + hour + "h" + minute;
                        }
                        else
                        {
                            dateLastFile = "Pas encore de photos prise";
                        }


                        TextBlock details_nbPhotos = new TextBlock();
                        details_nbPhotos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_nbPhotos.FontSize = 20;
                        details_nbPhotos.FontWeight = FontWeights.Bold;
                        details_nbPhotos.Margin = new Thickness(10, 17, 0, 0);
                        details_nbPhotos.Text = nbPhotos.ToString();
                        TextBlock details_photos = new TextBlock();
                        details_photos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_photos.FontSize = 14;
                        details_photos.FontWeight = FontWeights.Bold;
                        details_photos.Margin = new Thickness(20, 22, 0, 0);
                        details_photos.Text = dtlPhotos;
                        TextBlock details_nbprinted = new TextBlock();
                        details_nbprinted.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_nbprinted.FontSize = 20;
                        details_nbprinted.FontWeight = FontWeights.Bold;
                        details_nbprinted.Margin = new Thickness(0, 17, 0, 0);
                        details_nbprinted.Text = nbprinted.ToString();
                        TextBlock details_printed = new TextBlock();
                        details_printed.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_printed.FontSize = 14;
                        details_printed.FontWeight = FontWeights.Bold;
                        details_printed.Margin = new Thickness(20, 22, 0, 0);
                        details_printed.Text = dtlPrinted;  
                        StackPanel details_txt = new StackPanel();
                        details_txt.Orientation = Orientation.Horizontal;
                        details_txt.Children.Add(details_nbPhotos);
                        details_txt.Children.Add(details_photos);
                        TextBlock roundSep1 = new TextBlock();
                        roundSep1.Text = ".";
                        roundSep1.FontSize = 60;
                        //roundSep1.Height = Double.NaN;
                        roundSep1.VerticalAlignment = VerticalAlignment.Center;
                        roundSep1.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep1.Margin = new Thickness(25, -38, 25, 0);
                        details_txt.Children.Add(roundSep1);
                        details_txt.Children.Add(details_nbprinted);
                        details_txt.Children.Add(details_printed);
                        TextBlock roundSep2 = new TextBlock();
                        roundSep2.Text = ".";
                        roundSep2.FontSize = 60;
                        //roundSep2.Height = Double.NaN;
                        roundSep2.VerticalAlignment = VerticalAlignment.Center;
                        roundSep2.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep2.Margin = new Thickness(25, -38, 25, 0);
                        details_txt.Children.Add(roundSep2);
                        //details_txt.Children.Add(roundSep);
                        stackTemp.Children.Add(details_txt);

                        stackTemp.Height = 60;
                        stackTemp.VerticalAlignment = VerticalAlignment.Center;
                        stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#f4f3f8"));
                        stackTemp.Margin = new Thickness(15, 0, 15, 0);
                        stackTemp.Style = centerStackPanelContentStyle;
                        TextBlock lastCreation = new TextBlock();
                        lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                        lastCreation.FontSize = 12;
                        lastCreation.FontStyle = FontStyles.Italic;
                        lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        lastCreation.Margin = new Thickness(0, 23, 0, 0);
                        lastCreation.Text = dateLastFile;

                        stackTemp.Children.Add(lastCreation);


                        StackPanel detailsdownConfig = new StackPanel();
                        detailsdownConfig.Margin = new Thickness(15, 0, 15, 0);
                        detailsdownConfig.VerticalAlignment = VerticalAlignment.Center;
                        detailsdownConfig.HorizontalAlignment = HorizontalAlignment.Right;
                        detailsdownConfig.Height = 20;
                        detailsdownConfig.Orientation = Orientation.Horizontal;

                        DockPanel downConfig = new DockPanel();

                        downConfig.Margin = new Thickness(15, 0, 15, 0);
                        downConfig.VerticalAlignment = VerticalAlignment.Center;
                        downConfig.Width = detailLineStack.Width;
                        downConfig.Height = 80;
                        //downConfig.Orientation = Orientation.Horizontal;

                        StackPanel buttonEvent = new StackPanel();
                        buttonEvent.Margin = new Thickness(15, 0, 15, 0);
                        buttonEvent.HorizontalAlignment = HorizontalAlignment.Left;
                        buttonEvent.VerticalAlignment = VerticalAlignment.Center;
                        buttonEvent.Height = 80;
                        buttonEvent.Orientation = Orientation.Horizontal;


                        StackPanel downloadEvent = new StackPanel();
                        downloadEvent.Margin = new Thickness(15, 0, 15, 0);
                        downloadEvent.HorizontalAlignment = HorizontalAlignment.Right;
                        downloadEvent.VerticalAlignment = VerticalAlignment.Center;
                        downloadEvent.Height = 80;
                        downloadEvent.Orientation = Orientation.Horizontal;

                        System.Windows.Controls.Image imgOk = new System.Windows.Controls.Image();
                        imgOk.Margin = new Thickness(30, 0, 10, 0);
                        imgOk.Height = 10;
                        imgOk.Width = 10;
                        Bitmap imageBitmap = new Bitmap(Globals._defaultokconfig);

                        imgOk.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);



                        TextBlock lastVerif = new TextBlock();
                        //lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                        lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        lastVerif.Name = $"LastVerif_{codeEvent}";
                        lastVerif.FontSize = 12;
                        lastVerif.Height = 20;
                        lastVerif.Margin = new Thickness(-20, 2, 45, 0);
                        lastVerif.FontStyle = FontStyles.Italic;
                        lastVerif.Text = GetDateDerniereMAJ(code);

                        detailsdownConfig.Children.Add(lastVerif);
                        stackTemp.Children.Add(imgOk);
                        stackTemp.Children.Add(detailsdownConfig);
                        ImageBrush imgBrushDelete = new ImageBrush();
                        imgBrushDelete.ImageSource = new BitmapImage(new Uri(Globals._defaultbtndeleteEvent, UriKind.Absolute));

                        ImageBrush imgBrushArchive = new ImageBrush();
                        imgBrushArchive.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnarchiveEvent, UriKind.Absolute));


                        if (!isarchive)
                        {

                            Button ArchiverEvent = new Button();
                            ArchiverEvent.Content = "";
                            ArchiverEvent.FontWeight = FontWeights.Bold;
                            ArchiverEvent.Click += archiver_Click;
                            ArchiverEvent.Name = $"Archiver_{codeEvent}";
                            ArchiverEvent.Height = 50;
                            ArchiverEvent.Width = 60;

                            ArchiverEvent.Margin = new Thickness(0, 0, 10, 0);
                            ArchiverEvent.Style = buttonStyle;
                            ArchiverEvent.Background = imgBrushArchive;
                            ArchiverEvent.VerticalAlignment = VerticalAlignment.Center;
                            //ArchiverEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //ArchiverEvent.Background = new SolidColorBrush(Colors.Transparent);
                            ArchiverEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            buttonEvent.Children.Add(ArchiverEvent);

                            Button SupprimerEvent = new Button();
                            SupprimerEvent.Content = "";
                            SupprimerEvent.Click += supprimer_Click;
                            SupprimerEvent.Name = $"Supprimer_{codeEvent}";
                            SupprimerEvent.Height = 50;
                            SupprimerEvent.Width = 60;

                            SupprimerEvent.FontWeight = FontWeights.Bold;
                            SupprimerEvent.Margin = new Thickness(0, 0, 20, 0);
                            SupprimerEvent.Style = buttonStyle;
                            SupprimerEvent.Background = imgBrushDelete;
                            SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                            //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                            SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            buttonEvent.Children.Add(SupprimerEvent);

                            Button ForceDownloadConfig = new Button();
                            ForceDownloadConfig.Content = "TELECHARGER LA CONFIGURATION";
                            ForceDownloadConfig.Click += async (s, e) => await ForceDownloadConfig_Click(s, e);
                            ForceDownloadConfig.Name = $"ForcDownConfig_{codeEvent}";
                            ForceDownloadConfig.Height = 40;
                            ForceDownloadConfig.Width = 225;
                            ForceDownloadConfig.HorizontalAlignment = HorizontalAlignment.Right;
                            ForceDownloadConfig.Margin = new Thickness(20, 0, 75, 0);
                            ForceDownloadConfig.Style = buttonRoudedStyle;
                            ForceDownloadConfig.VerticalAlignment = VerticalAlignment.Center;
                            ForceDownloadConfig.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));//gray
                            ForceDownloadConfig.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828")); ;
                            ForceDownloadConfig.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(ForceDownloadConfig);


                        }
                        else
                        {
                            Button RemettreEvent = new Button();
                            RemettreEvent.Content = "";
                            RemettreEvent.FontWeight = FontWeights.Bold;
                            RemettreEvent.Click += remettre_Click;
                            RemettreEvent.Name = $"Remettre_{codeEvent}";
                            RemettreEvent.Height = 50;
                            RemettreEvent.Width = 60;
                            RemettreEvent.Margin = new Thickness(20, 0, 10, 0);
                            RemettreEvent.Style = buttonStyle;
                            RemettreEvent.VerticalAlignment = VerticalAlignment.Center;
                            //RemettreEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //RemettreEvent.Background = new SolidColorBrush(Colors.Transparent);
                            RemettreEvent.Background = imgBrushArchive;
                            //RemettreEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(RemettreEvent);

                            Button SupprimerEvent = new Button();
                            SupprimerEvent.Content = "";
                            SupprimerEvent.Click += supprimerArchive_Click;
                            SupprimerEvent.Name = $"SupprimerArchive_{codeEvent}";
                            SupprimerEvent.Height = 50;
                            SupprimerEvent.Width = 60;

                            SupprimerEvent.FontWeight = FontWeights.Bold;
                            SupprimerEvent.Margin = new Thickness(0, 0, 70, 0);
                            SupprimerEvent.Style = buttonStyle;
                            SupprimerEvent.Background = imgBrushDelete;
                            SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                            //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                            SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(SupprimerEvent);
                        }



                        downConfig.Children.Add(buttonEvent);
                        downConfig.Children.Add(downloadEvent);
                        //stackTemp.Children.Add(downConfig);
                        stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3d3b3c"));

                        stackTemp.Visibility = Visibility.Collapsed;

                        row2Container.Children.Add(stackTemp);
                        mainContainer.Children.Add(row2Container);

                        StackPanel row3Container = new StackPanel();
                        row3Container.Name = "stack1" + codeEvent;
                        row3Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                        row3Container.Width = detailLineStack.Width;
                        row3Container.Height = 90;
                        row3Container.Margin = new Thickness(15, 0, 15, 0);
                        row3Container.Visibility = Visibility.Collapsed;
                        row3Container.Children.Add(downConfig);
                        mainContainer.Children.Add(row3Container);
                        Separator sep = new Separator();
                        sep.Name = $"sep_{code}";
                        sep.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828"));
                        sep.Style = this.FindResource("separatorStyle") as Style;
                        sep.Margin = new Thickness(20, 0, 20, 0);
                        sep.Height = 20;
                        mainContainer.Children.Add(sep);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        Grid detailLineStack = new Grid();
                        detailLineStack.Margin = new Thickness(20, 0, 20, 0);
                        detailLineStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                        detailLineStack.Name = $"Detail_{code}";
                        detailLineStack.Height = 80;
                        detailLineStack.Width = widthWorkArea - 40;

                        StackPanel stackEventName = new StackPanel();
                        stackEventName.Margin = new Thickness(35, 0, 0, 0);
                        stackEventName.VerticalAlignment = VerticalAlignment.Center;
                        stackEventName.HorizontalAlignment = HorizontalAlignment.Left;
                        stackEventName.Orientation = Orientation.Horizontal;

                        TextBlock eventName = new TextBlock();
                        eventName.FontSize = 20;
                        eventName.Height = 25;
                        eventName.Tag = codeEvent;
                        eventName.Foreground = System.Windows.Media.Brushes.White;
                        eventName.PreviewMouseDown += EditTextblockEvent_Click;
                        eventName.TextWrapping = TextWrapping.Wrap;
                        eventName.Margin = new Thickness(0, 0, 0, 0);
                        eventName.Inlines.Add(new Run(Name) { FontWeight = FontWeights.Bold });
                        eventName.VerticalAlignment = VerticalAlignment.Center;
                        eventName.HorizontalAlignment = HorizontalAlignment.Left;
                        stackEventName.Children.Add(eventName);

                        TextBlock roundSep = new TextBlock();
                        roundSep.Text = ".";
                        roundSep.FontSize = 60;
                        roundSep.Height = Double.NaN;
                        roundSep.VerticalAlignment = VerticalAlignment.Center;
                        roundSep.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep.Margin = new Thickness(25, -35, 25, 0);
                        stackEventName.Children.Add(roundSep);

                        TextBlock eventPeriod = new TextBlock();
                        eventPeriod.Text = period;
                        eventPeriod.HorizontalAlignment = HorizontalAlignment.Center;
                        eventPeriod.FontSize = 15;
                        eventPeriod.Foreground = System.Windows.Media.Brushes.White;
                        eventPeriod.Height = 20;
                        eventPeriod.Margin = new Thickness(0, 0, 20, 0);
                        stackEventName.Children.Add(eventPeriod);

                        Button btnDetails = new Button();
                        btnDetails.Name = "btnDetails";
                        btnDetails.Tag = "stack" + codeEvent;
                        btnDetails.Height = 37;
                        btnDetails.Margin = new Thickness(0, 0, 0, 0);
                        btnDetails.HorizontalAlignment = HorizontalAlignment.Left;
                        btnDetails.Style = buttonStyle;
                        btnDetails.Width = 127;
                        btnDetails.Click += Details_Click;
                        ImageBrush btnBrush = new ImageBrush();
                        btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsClosed, UriKind.Absolute));
                        btnDetails.Background = btnBrush;

                        stackEventName.Children.Add(btnDetails);

                        Button updateDispo = new Button();
                        updateDispo.FontSize = 12;
                        updateDispo.Height = 30;
                        //updateDispo.Width =100;// Double.NaN;

                        updateDispo.Name = $"maj_{code}";
                        updateDispo.Margin = new Thickness(20, 0, 0, 0);
                        updateDispo.VerticalAlignment = VerticalAlignment.Center;
                        updateDispo.Style = buttonRoudedStyle;
                        updateDispo.HorizontalAlignment = HorizontalAlignment.Left;
                        updateDispo.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFF")); //RED
                        updateDispo.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff0202"));
                        updateDispo.BorderBrush = new SolidColorBrush(Colors.Transparent);
                        updateDispo.BorderThickness = new Thickness(0);
                        updateDispo.Click += async (s, e) => await updateDispo_Clicked(s, e);
                        updateDispo.Visibility = Visibility.Hidden;
                        stackEventName.Children.Add(updateDispo);

                        detailLineStack.Children.Add(stackEventName);
                        /************FIN COLUMN 1**************/

                        /************DEBUT COLUMN 2**************/


                        StackPanel stackEventSynchro = new StackPanel();
                        stackEventSynchro.Orientation = Orientation.Horizontal;

                        TextBlock eventSynchro = new TextBlock();
                        eventSynchro.Name = $"synchro_{code}";
                        eventSynchro.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4dad5d"));
                        eventSynchro.Margin = new Thickness(0, 10, 10, 30);
                        eventSynchro.FontWeight = FontWeights.Bold;
                        eventSynchro.TextWrapping = TextWrapping.Wrap;
                        eventSynchro.HorizontalAlignment = HorizontalAlignment.Right;


                        TextBlock eventcode = new TextBlock();
                        eventcode.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        eventcode.Text = code.ToUpper();
                        eventcode.FontSize = 15;
                        eventcode.Height = 20;
                        eventcode.Width = 60;
                        eventcode.Tag = code;
                        eventcode.PreviewMouseDown += EditTextblockEvent_Click;
                        eventcode.TextWrapping = TextWrapping.Wrap;
                        eventcode.VerticalAlignment = VerticalAlignment.Center;

                        ProgressBar progres = new ProgressBar();
                        System.Windows.Media.Color color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#4dad5d");
                        progres.Foreground = new SolidColorBrush(color);
                        progres.Name = $"progress_{code}";
                        progres.Height = 11;
                        progres.Width = 150;
                        progres.BorderThickness = new Thickness(1);
                        progres.BorderBrush = System.Windows.Media.Brushes.Transparent;
                        progres.Margin = new Thickness(10, -30, 8, 0);
                        progres.Visibility = Visibility.Hidden;

                        StackPanel stackPanelColumn2 = new StackPanel();
                        stackPanelColumn2.Orientation = Orientation.Horizontal;
                        stackPanelColumn2.Children.Add(stackEventSynchro);
                        stackPanelColumn2.HorizontalAlignment = HorizontalAlignment.Left;
                        detailLineStack.Children.Add(stackPanelColumn2);
                        /************FIN COLUMN 2**************/

                        /************DEBUT COLUMN 3**************/
                        StackPanel rightButton = new StackPanel();
                        rightButton.Margin = new Thickness(0, 0, 40, 0);
                        rightButton.Orientation = Orientation.Horizontal;
                        rightButton.HorizontalAlignment = HorizontalAlignment.Right;
                        rightButton.Width = Double.NaN;

                        Button launchAnimation = new Button();
                        launchAnimation.Tag = codeEvent;
                        launchAnimation.Click += LaunchAnimation_Click;
                        launchAnimation.Name = "launchAnimation";
                        launchAnimation.Width = 150;
                        launchAnimation.Height = 30;
                        launchAnimation.Margin = new Thickness(10, 0, 15, 0);
                        launchAnimation.Style = buttonStyle;

                        ImageBrush btnLaunchAnimBrush = new ImageBrush();
                        btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
                        launchAnimation.Background = btnLaunchAnimBrush;
                        StackPanel stackSynchro = new StackPanel();
                        stackSynchro.Orientation = Orientation.Vertical;
                        stackSynchro.Children.Add(eventSynchro);
                        stackSynchro.Children.Add(progres);

                        rightButton.Children.Add(eventcode);
                        rightButton.Children.Add(stackSynchro);
                        rightButton.Children.Add(launchAnimation);
                        detailLineStack.Children.Add(rightButton);
                        /************FIN COLUMN 3**************/
                        mainContainer.Children.Add(detailLineStack);



                        string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";

                        StackPanel row2Container = new StackPanel();
                        row2Container.Margin = new Thickness(20, 0, 20, 0);
                        row2Container.Width = detailLineStack.Width - 40;
                        row2Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));

                        StackPanel stackTemp = new StackPanel();

                        stackTemp.Name = "stack" + codeEvent;
                        stackTemp.Orientation = Orientation.Horizontal;

                        int nbPhotos = getNbPhotos(codeEvent);
                        int nbprinted = getNbPrinted(codeEvent);

                        string dtlPhotos = (nbPhotos > 1) ? $"PHOTOS" : $"PHOTO";
                        string dtlPrinted = (nbprinted > 1) ? $"IMPRESSIONS" : $"IMPRESSION";
                        FileInfo newestFile = GetNewestFile(new DirectoryInfo(photosDirectory));
                        string dateLastFile = "";
                        if (newestFile != null)
                        {
                            DateTime lastCrea = newestFile.CreationTime;
                            string day = lastCrea.Day.ToString();
                            string month = lastCrea.Month.ToString();
                            string hour = lastCrea.Hour.ToString();
                            string minute = lastCrea.Minute.ToString();
                            if (day.Length == 1)
                            {
                                day = "0" + day;
                            }
                            if (month.Length == 1)
                            {
                                month = "0" + month;
                            }
                            if (hour.Length == 1)
                            {
                                hour = "0" + hour;
                            }
                            if (minute.Length == 1)
                            {
                                minute = "0" + minute;
                            }
                            //dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString().Substring(2, 2) + " à " + hour + "h" + minute;
                            dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString() + " à " + hour + "h" + minute;
                        }
                        else
                        {
                            dateLastFile = "Pas encore de photos prise";
                        }


                        TextBlock details_nbPhotos = new TextBlock();
                        details_nbPhotos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_nbPhotos.FontSize = 20;
                        details_nbPhotos.FontWeight = FontWeights.Bold;
                        details_nbPhotos.Margin = new Thickness(30, 13, 0, 0);
                        details_nbPhotos.Text = nbPhotos.ToString();
                        TextBlock details_photos = new TextBlock();
                        details_photos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_photos.FontSize = 14;
                        details_photos.FontWeight = FontWeights.Bold;
                        details_photos.Margin = new Thickness(20, 20, 0, 0);
                        details_photos.Text = dtlPhotos;
                        TextBlock details_nbprinted = new TextBlock();
                        details_nbprinted.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_nbprinted.FontSize = 20;
                        details_nbprinted.FontWeight = FontWeights.Bold;
                        details_nbprinted.Margin = new Thickness(20, 13, 0, 0);
                        details_nbprinted.Text = nbprinted.ToString();
                        TextBlock details_printed = new TextBlock();
                        details_printed.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        details_printed.FontSize = 14;
                        details_printed.FontWeight = FontWeights.Bold;
                        details_printed.Margin = new Thickness(20, 20, 0, 0);
                        details_printed.Text = dtlPrinted;
                        StackPanel details_txt = new StackPanel();
                        details_txt.Orientation = Orientation.Horizontal;
                        details_txt.Children.Add(details_nbPhotos);
                        details_txt.Children.Add(details_photos);
                        TextBlock roundSep1 = new TextBlock();
                        roundSep1.Text = ".";
                        roundSep1.FontSize = 60;
                        //roundSep1.Height = Double.NaN;
                        roundSep1.VerticalAlignment = VerticalAlignment.Center;
                        roundSep1.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep1.Margin = new Thickness(25, -41, 25, 0);
                        details_txt.Children.Add(roundSep1);
                        details_txt.Children.Add(details_nbprinted);
                        details_txt.Children.Add(details_printed);
                        TextBlock roundSep2 = new TextBlock();
                        roundSep2.Text = ".";
                        roundSep2.FontSize = 60;
                        //roundSep2.Height = Double.NaN;
                        roundSep2.VerticalAlignment = VerticalAlignment.Center;
                        roundSep2.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        roundSep2.Margin = new Thickness(25, -41, 25, 0);
                        details_txt.Children.Add(roundSep2);
                        //details_txt.Children.Add(roundSep);
                        stackTemp.Children.Add(details_txt);

                        stackTemp.Height = 60;
                        stackTemp.VerticalAlignment = VerticalAlignment.Center;
                        stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#f4f3f8"));
                        stackTemp.Margin = new Thickness(25, 0, 18, 0);
                        stackTemp.Style = centerStackPanelContentStyle;
                        TextBlock lastCreation = new TextBlock();
                        lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                        lastCreation.FontSize = 12;
                        lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        lastCreation.Margin = new Thickness(30, 22, 0, 0);
                        lastCreation.Text = dateLastFile;

                        stackTemp.Children.Add(lastCreation);


                        StackPanel detailsdownConfig = new StackPanel();
                        detailsdownConfig.Margin = new Thickness(30, 0, 0, 0);
                        detailsdownConfig.VerticalAlignment = VerticalAlignment.Center;
                        detailsdownConfig.HorizontalAlignment = HorizontalAlignment.Right;
                        detailsdownConfig.Height = 20;
                        detailsdownConfig.Orientation = Orientation.Horizontal;

                        DockPanel downConfig = new DockPanel();

                        downConfig.Margin = new Thickness(30, 0, 0, 0);
                        downConfig.VerticalAlignment = VerticalAlignment.Center;
                        downConfig.Width = detailLineStack.Width;
                        downConfig.Height = 80;
                        //downConfig.Orientation = Orientation.Horizontal;

                        StackPanel buttonEvent = new StackPanel();
                        //buttonEvent.Margin = new Thickness(30, 0, 0, 0);
                        buttonEvent.HorizontalAlignment = HorizontalAlignment.Left;
                        buttonEvent.VerticalAlignment = VerticalAlignment.Center;
                        buttonEvent.Height = 80;
                        buttonEvent.Orientation = Orientation.Horizontal;


                        StackPanel downloadEvent = new StackPanel();
                        downloadEvent.Margin = new Thickness(0, 0, 50, 0);
                        downloadEvent.HorizontalAlignment = HorizontalAlignment.Right;
                        downloadEvent.VerticalAlignment = VerticalAlignment.Center;
                        downloadEvent.Height = 80;
                        downloadEvent.Orientation = Orientation.Horizontal;

                        System.Windows.Controls.Image imgOk = new System.Windows.Controls.Image();
                        imgOk.Margin = new Thickness(50, 0, 0, 0);
                        imgOk.Height = 10;
                        imgOk.Width = 10;
                        Bitmap imageBitmap = new Bitmap(Globals._defaultokconfig);

                        imgOk.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);



                        TextBlock lastVerif = new TextBlock();
                        //lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                        lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                        lastVerif.Name = $"LastVerif_{codeEvent}";
                        lastVerif.FontSize = 12;
                        lastVerif.Height = 20;
                        lastVerif.Margin = new Thickness(-10, 1, 0, 0);
                        lastVerif.FontStyle = FontStyles.Italic;
                        lastVerif.Text = GetDateDerniereMAJ(code);

                        detailsdownConfig.Children.Add(lastVerif);
                        stackTemp.Children.Add(imgOk);
                        stackTemp.Children.Add(detailsdownConfig);
                        ImageBrush imgBrushDelete = new ImageBrush();
                        imgBrushDelete.ImageSource = new BitmapImage(new Uri(Globals._defaultbtndeleteEvent, UriKind.Absolute));

                        ImageBrush imgBrushArchive = new ImageBrush();
                        imgBrushArchive.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnarchiveEvent, UriKind.Absolute));


                        if (!isarchive)
                        {

                            Button ArchiverEvent = new Button();
                            ArchiverEvent.Content = "";
                            ArchiverEvent.FontWeight = FontWeights.Bold;
                            ArchiverEvent.Click += archiver_Click;
                            ArchiverEvent.Name = $"Archiver_{codeEvent}";
                            ArchiverEvent.Height = 50;
                            ArchiverEvent.Width = 60;

                            ArchiverEvent.Margin = new Thickness(0, 0, 10, 0);
                            ArchiverEvent.Style = buttonStyle;
                            ArchiverEvent.Background = imgBrushArchive;
                            ArchiverEvent.VerticalAlignment = VerticalAlignment.Center;
                            //ArchiverEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //ArchiverEvent.Background = new SolidColorBrush(Colors.Transparent);
                            ArchiverEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            buttonEvent.Children.Add(ArchiverEvent);

                            Button SupprimerEvent = new Button();
                            SupprimerEvent.Content = "";
                            SupprimerEvent.Click += supprimer_Click;
                            SupprimerEvent.Name = $"Supprimer_{codeEvent}";
                            SupprimerEvent.Height = 50;
                            SupprimerEvent.Width = 60;

                            SupprimerEvent.FontWeight = FontWeights.Bold;
                            SupprimerEvent.Margin = new Thickness(0, 0, 20, 0);
                            SupprimerEvent.Style = buttonStyle;
                            SupprimerEvent.Background = imgBrushDelete;
                            SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                            //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                            SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            buttonEvent.Children.Add(SupprimerEvent);

                            Button ForceDownloadConfig = new Button();
                            ForceDownloadConfig.Content = "TELECHARGER LA CONFIGURATION";
                            ForceDownloadConfig.Click += async (s, e) => await ForceDownloadConfig_Click(s, e);
                            ForceDownloadConfig.Name = $"ForcDownConfig_{codeEvent}";
                            ForceDownloadConfig.Height = 40;
                            ForceDownloadConfig.Width = 225;
                            ForceDownloadConfig.HorizontalAlignment = HorizontalAlignment.Right;
                            ForceDownloadConfig.Margin = new Thickness(20, 0, 35, 0);
                            ForceDownloadConfig.Style = buttonRoudedStyle;
                            ForceDownloadConfig.VerticalAlignment = VerticalAlignment.Center;
                            ForceDownloadConfig.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));//gray
                            ForceDownloadConfig.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828")); ;
                            ForceDownloadConfig.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(ForceDownloadConfig);


                        }
                        else
                        {
                            Button RemettreEvent = new Button();
                            RemettreEvent.Content = "";
                            RemettreEvent.FontWeight = FontWeights.Bold;
                            RemettreEvent.Click += remettre_Click;
                            RemettreEvent.Name = $"Remettre_{codeEvent}";
                            RemettreEvent.Height = 50;
                            RemettreEvent.Width = 60;
                            RemettreEvent.Margin = new Thickness(20, 0, 10, 0);
                            RemettreEvent.Style = buttonStyle;
                            RemettreEvent.VerticalAlignment = VerticalAlignment.Center;
                            //RemettreEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //RemettreEvent.Background = new SolidColorBrush(Colors.Transparent);
                            RemettreEvent.Background = imgBrushArchive;
                            //RemettreEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(RemettreEvent);

                            Button SupprimerEvent = new Button();
                            SupprimerEvent.Content = "";
                            SupprimerEvent.Click += supprimerArchive_Click;
                            SupprimerEvent.Name = $"SupprimerArchive_{codeEvent}";
                            SupprimerEvent.Height = 50;
                            SupprimerEvent.Width = 60;

                            SupprimerEvent.FontWeight = FontWeights.Bold;
                            SupprimerEvent.Margin = new Thickness(0, 0, 70, 0);
                            SupprimerEvent.Style = buttonStyle;
                            SupprimerEvent.Background = imgBrushDelete;
                            SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                            //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                            //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                            SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                            downloadEvent.Children.Add(SupprimerEvent);
                        }



                        downConfig.Children.Add(buttonEvent);
                        downConfig.Children.Add(downloadEvent);
                        //stackTemp.Children.Add(downConfig);
                        stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3d3b3c"));

                        stackTemp.Visibility = Visibility.Collapsed;

                        row2Container.Children.Add(stackTemp);
                        mainContainer.Children.Add(row2Container);

                        StackPanel row3Container = new StackPanel();
                        row3Container.Name = "stack1" + codeEvent;
                        row3Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                        row3Container.Width = detailLineStack.Width;
                        row3Container.Height = 90;
                        row3Container.Margin = new Thickness(20, 0, 20, 0);
                        row3Container.Visibility = Visibility.Collapsed;
                        row3Container.Children.Add(downConfig);
                        mainContainer.Children.Add(row3Container);
                        Separator sep = new Separator();
                        sep.Name = $"sep_{code}";
                        sep.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828"));
                        sep.Style = this.FindResource("separatorStyle") as Style;
                        sep.Margin = new Thickness(20, 0, 20, 0);
                        sep.Height = 20;
                        mainContainer.Children.Add(sep);
                    }
                    
                }

            } //Fin foreach...
        }// Fin AddListToStackPanel...

        private string GetDateDerniereMAJ(string code)
        {
            try
            {
                var pathOfFiles = isArchive ? $"C:/EVENTS/Assets/Archives/{code}" : $"C:/EVENTS/Assets/{code}";
                FileInfo newestFile = Compare.GetNewestFile(new DirectoryInfo(pathOfFiles));

               if(newestFile != null)
               {
                    DateTime creation = File.GetLastWriteTime(newestFile.FullName); 
                    string day = creation.Day.ToString();
                    string month = creation.Month.ToString();
                    string year = creation.Year.ToString();
                    string hour = creation.Hour.ToString();
                    string minutes = creation.Minute.ToString();
                    if (day.Length == 1) day = $"0{day}";
                    if (month.Length == 1) month = $"0{month}";
                    if (hour.Length == 1) hour = $"0{hour}";
                    if (minutes.Length == 1) minutes = "0" + minutes;
                    return $"Dernière configuration téléchargée – Installée le : {day}/{month}/{year} à {hour}h{minutes}";
                }
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - GetDateDerniereMAJ ERROR : {ex}");
            }
            return string.Empty;
        }

        private string GetDateMAJDisponible(string code)
        {
            try
            {
                DateTime datevers= Compare.GetLatestFileDateInFolderContent(new NetworkCredential(ftpUserNameEvent, ftpPasswordEvent), $"ftp://{ftpServerIP}/{code}");
                string dateModif = "";
                try
                {
                    dateModif = datevers.ToString("dd/MM/yyyy");
                }
                catch (Exception ex)
                {
                    dateModif = datevers.ToString("yyyy-MM-dd");
                }
                string heureModif = datevers.ToString("HH");
                string minModif = datevers.ToString("mm");
                
                DateTime datelastmodif = Directory.GetLastWriteTime(Globals._assetsFolder + "\\" + code);
                if (datelastmodif < datevers)
                    return $"({dateModif} à {heureModif}h{minModif})";
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - GetDateMAJDisponible ERROR : {ex}");
            }
            return string.Empty;
        }

        private async Task<string> GetDateMAJDisponibleAsync(string code)
        {
            try
            {
                DateTime datevers = await Compare.GetLatestFileDateInFolderContentAsync(new NetworkCredential(ftpUserNameEvent, ftpPasswordEvent), $"ftp://{ftpServerIP}/{code}");
                string dateModif = "";
                try
                {
                    dateModif = datevers.ToString("dd/MM/yyyy");
                }
                catch(Exception ex)
                {
                    dateModif = datevers.ToString("yyyy-MM-dd");
                }
                string heureModif = datevers.ToString("HH");
                string minModif = datevers.ToString("mm");

                DateTime datelastmodif = Directory.GetLastWriteTime(Globals._assetsFolder + "\\" + code);
                if (datelastmodif < datevers)
                    return $"({dateModif} à {heureModif}h{minModif})";
            }
            catch (Exception ex)
            {
                Log.Error($"ListEvent.xaml.cs - GetDateMAJDisponible ERROR : {ex}");
            }
            return string.Empty;
        }


        private async Task ForceDownloadConfig_Click(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];

            NetworkCredential credentialsEvent = new NetworkCredential(ftpUserNameEvent, ftpPasswordEvent);
            string ftpDirectoryPathCode = $"ftp://{ftpServerIP}/{code}";
            string locDirectoryPathCode = $"C:/EVENTS/Assets/{code}";
            string remoteFolderToLocal = $"C:/EVENTS/Download/{code}/Remote";


            
            if (chk_con())
            {
                ctrl.Content = "Téléchargement en cours...";
                await Task.Run(() =>
                {
                    try
                    {
                        //MessageBox.Show(remoteFolderToLocal);
                        if (!Directory.Exists(remoteFolderToLocal))
                        {
                            Directory.CreateDirectory(remoteFolderToLocal);
                        }
                        Compare.DownloadFtpDirectory($"{ftpDirectoryPathCode}/", credentialsEvent, remoteFolderToLocal);
                        
                        var allLocalFiles = Directory.GetFiles(remoteFolderToLocal, "*", SearchOption.AllDirectories).Select(x => x.Replace("\\", "/").Replace($"{remoteFolderToLocal}/", "")).ToList();

                        DownloadFromFtp(code, allLocalFiles);
                        MajEnLigneMethod(code);
                        MessageBox.Show($"Téléchargement terminé, pour l'événement {code} ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Le Téléchargement a échoué : {ex.Message} ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }                    

                    
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ctrl.Content = "Télécharger la configuration";
                        var tblLastVerif = FindChild(mainContainer, control =>
                        {
                            var txtBlock = control as TextBlock;
                            if (txtBlock != null && txtBlock.Name == $"LastVerif_{code}")
                                return true;
                            else
                                return false;
                        }) as TextBlock;
                        tblLastVerif.Text = GetDateDerniereMAJ(code);
                    }));
                });

            }
            else
            {
                MessageBox.Show("Vous n'êtes pas connecté à internet. ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void CacherEventLine(string code)
        {
            var gridLineEvent = FindChild(mainContainer, control =>
            {
                var grdEvent = control as Grid;
                if (grdEvent != null && grdEvent.Name == $"Detail_{code}")
                    return true;
                else
                    return false;
            }) as Grid;

            gridLineEvent.Visibility = Visibility.Collapsed;

            var stackEvent = FindChild(mainContainer, control =>
            {
                var stkEvent = control as StackPanel;
                if (stkEvent != null && stkEvent.Name == $"stack{code}")
                    return true;
                else
                    return false;
            }) as StackPanel;

            stackEvent.Visibility = Visibility.Collapsed;

            var sepaEvent = FindChild(mainContainer, control =>
            {
                var spEvent = control as Separator;
                if (spEvent != null && spEvent.Name == $"sep_{code}")
                    return true;
                else
                    return false;
            }) as Separator;

            sepaEvent.Visibility = Visibility.Collapsed;

            var stackLine3 = FindChild(mainContainer, control =>
            {
                var stkEvent = control as StackPanel;
                if (stkEvent != null && stkEvent.Name == $"stack1{code}")
                    return true;
                else
                    return false;
            }) as StackPanel;

            stackLine3.Visibility = Visibility.Collapsed;
        }

        private void writeTxtFileData(string filename, string toWrite)
        {
            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }

        private void supprimer_Click(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];

            code_todelete = code;

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code_todelete + ".bin";
            mgrBin.writeData(0, binFileName);
            string mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
            writeTxtFileData(mediaDataPath, "0");
            string Titre = "Suppression de l'événement";
            string Message = "Que souhaitez-vous supprimer ?";
            string result = CustomDeleteMessageBox.Prompt(Titre, Message);
            if (result == "assets")
            {
                try
                { 
                    CacherEventLine(code_todelete);
                    var eventFolderPath = $"C:/Events/Assets/{code_todelete}";
                    var downloadFolderPath = $"C:/Events/Download/{code_todelete}";
                    Directory.Delete(eventFolderPath, true);
                    if(Directory.Exists(downloadFolderPath))
                        Directory.Delete(downloadFolderPath, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - supprimer_Click ERROR : {ex}");
                }

                code_todelete = "";
                setEventsLabel();
            }
            else if (result == "all")
            {
                try
                {
                    CacherEventLine(code_todelete);
                    var eventFolderPath = $"C:/Events/Assets/{code_todelete}";
                    var mediaFolderPath = $"C:/Events/Media/{code_todelete}";
                    var downloadFolderPath = $"C:/Events/Download/{code_todelete}";
                    Directory.Delete(eventFolderPath, true);
                    Directory.Delete(mediaFolderPath, true);
                    if (Directory.Exists(downloadFolderPath))
                        Directory.Delete(downloadFolderPath, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - supprimer_Click ERROR : {ex}");
                }

                code_todelete = "";
                setEventsLabel();
                code_todelete = "";
            }
            isArchive = false;
            GetEventNameAfterDelete();
            GetEventList(isArchive);
            isArchive = true;
            GetEventList(isArchive);
            //scrollDelete.Visibility = Visibility.Visible;
        }

        private void GetEventNameAfterDelete()
        {
            var eventPath = $"C:/Events/Assets/";
            List<string> dirs = new List<string>(Directory.GetDirectories(eventPath));
            code = "";
            foreach (var dir in dirs)
            {
                var dirName = Path.GetFileName(dir);
                if (!dirName.Equals("Archives") && !dirName.Equals("Default"))
                {
                    code = dirName;
                    break;
                }
            }
            
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
        }

        

        public void reinitPrinted(string code)
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            mgrBin.writeData(0, binFileName);
            string mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
            writeTxtFileData(mediaDataPath, "0");
        }

        private void supprimerArchive_Click(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];

            codeArchive_todelete = code;
            //scrollArchiveDelete.Visibility = Visibility.Visible;
            string Titre = "Suppression d'un événement archivé";
            string Message = "Voulez-vous vraiment supprimer l'événement archivé N°: " + code + " ?";
            bool result = CustomMessageBox.Prompt(Titre, Message);
            if (result)
            {
                try
                {
                    CacherEventLine(codeArchive_todelete);
                    var eventFolderPath = $"C:/Events/Assets/Archives/{codeArchive_todelete}";
                    Directory.Delete(eventFolderPath, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - supprimer_Click ERROR : {ex}");
                }

                codeArchive_todelete = "";
                setEventsLabel();
            }
            else
            {
                codeArchive_todelete = "";
            }
            isArchive = true;
            GetEventList(isArchive);
            isArchive = false;
            GetEventList(isArchive);
        }

        private void archiver_Click(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];
            code_toarchive = code;
            //scrollArchive.Visibility = Visibility.Visible;
            string Titre = "Archivage d'un événement";
            string Message = "Voulez-vous vraiment archiver l'événement N°: " + code + " ?";
            bool result = CustomMessageBox.Prompt(Titre, Message);
            if (result)
            {
                try
                {
                    var eventSource = $"C:/Events/Assets/{code_toarchive}";
                    var eventTarget = $"C:/Events/Assets/Archives/{code_toarchive}";


                    DirectoryInfo diSource = new DirectoryInfo(eventSource);
                    DirectoryInfo diTarget = new DirectoryInfo(eventTarget);

                    CopyAll(diSource, diTarget);

                    CacherEventLine(code_toarchive);

                    Directory.Delete(eventSource, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - archiver_Click ERROR : {ex}");
                }

                setEventsLabel();
                code_toarchive = "";
            }
            else
            {
                code_toarchive = "";
            }

            //MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment archiver cet événement ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            //if (messageBoxResult == MessageBoxResult.Yes)
            //{
            //    try
            //    {
            //        var eventSource = $"C:/Events/Assets/{code}";
            //        var eventTarget = $"C:/Events/Assets/Archives/{code}";


            //        DirectoryInfo diSource = new DirectoryInfo(eventSource);
            //        DirectoryInfo diTarget = new DirectoryInfo(eventTarget);

            //        CopyAll(diSource, diTarget);

            //        CacherEventLine(code);

            //        Directory.Delete(eventSource, true);
            //    }
            //    catch (Exception ex)
            //    {
            //        Log.Error($"ListEvent.xaml.cs - archiver_Click ERROR : {ex}");
            //    }
            //}
            //setEventsLabel();
            isArchive = false;
            GetEventList(isArchive);
            isArchive = true;
            GetEventList(isArchive);
        }

        private void setEventsLabel()
        {
            int eventFolder = Directory.GetDirectories(Globals._assetsFolder).Length - 2;
            int archivesFolder = Directory.GetDirectories(Globals._archivesFolder).Length;
            currentEvent.Content = "MES ÉVÉNEMENTS (" + eventFolder + ")";
            archiveEvent.Content = "ARCHIVES (" + archivesFolder + ")";
        }

        private void remettre_Click(object sender, RoutedEventArgs e)
        {
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];
            code_torestore = code;
            string Titre = "Restauration d'un événement";
            string Message = "Voulez-vous vraiment restaurer l'événement N°: " + code + " ?";
            bool result = CustomMessageBox.Prompt(Titre, Message);
            if (result)
            {
                try
                {

                    var eventSource = $"C:/Events/Assets/Archives/{code_torestore}";
                    var eventTarget = $"C:/Events/Assets/{code_torestore}";


                    DirectoryInfo diSource = new DirectoryInfo(eventSource);
                    DirectoryInfo diTarget = new DirectoryInfo(eventTarget);

                    CopyAll(diSource, diTarget);

                    CacherEventLine(code_torestore);

                    Directory.Delete(eventSource, true);
                }
                catch (Exception ex)
                {
                    Log.Error($"ListEvent.xaml.cs - remettre_Click ERROR : {ex}");
                }
                code_torestore = "";
                setEventsLabel();
            }
            else
            {
                code_torestore = "";
            }
            isArchive = true;
            GetEventList(isArchive);
            isArchive = false;
            GetEventList(isArchive);
        }

        public void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                //Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        private void OpenArchives_Click(object sender, RoutedEventArgs e)
        {
            //if (cts != null)
            //{
            //    cts.Cancel();
            //}
            
            if (lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                KillMe();
                //stopSynchro = false;
                createWorker();
                mainContainer.Children.Clear();
                archiveEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
                archiveBorder.BorderThickness = new Thickness(0, 0, 0, 6);
                eventBorder.BorderThickness = new Thickness(0, 0, 0, 0);
                currentEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                isArchive = true;
                OnUcLoaded(sender, e);
                setEventsLabel();
            }
        }
        private void OpenEvents_Click(object sender, RoutedEventArgs e)
        {
            //if (cts != null)
            //{
            //    cts.Cancel();
            //}
            KillMe();
            //createWorker();
            mainContainer.Children.Clear();
            currentEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
            eventBorder.BorderThickness = new Thickness(0, 0, 0, 6);
            archiveBorder.BorderThickness = new Thickness(0, 0, 0, 0);
            archiveEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
            isArchive = false;
            OnUcLoaded(sender, e);
            setEventsLabel();
        }

        private void TabControl_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
            switch (tabItem)
            {
                case "currentEvent":
                    OpenEvents_Click(sender, e);
                    break;

                case "archiveEvent":
                    OpenArchives_Click(sender, e);
                    break;
            }
        }

        private async Task updateDispo_Clicked(object sender, RoutedEventArgs e)
        {
            if (timerCountdown != null)
            {
                timerCountdown.Stop();
            }
            
            var ctrl = sender as Button;
            var code = ctrl.Name.Split('_')[1];
            lstCurrentUpdate.Add(code);
            if (chk_con())
            {
                ctrl.Content = "Mise à jour en cours...";
                await Task.Run(async () =>
                {
                    try
                    {
                        string remoteFolderToLocal = $"C:/EVENTS/Download/{code}/Remote";
                        if (!Directory.Exists(remoteFolderToLocal))
                        {
                            Directory.CreateDirectory(remoteFolderToLocal);
                        }
                        lstImgEventsNotSynchronized = Compare.CompareFtpVsLocalAndCountMissingFiles(code, true);
                        DownloadFromFtp(code, lstImgEventsNotSynchronized.ToList());

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show($"La mise à jour a échoué : {ex.Message} ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                        await Dispatcher.BeginInvoke(new Action(() =>
                        {
                            var dateMajDispo = GetDateMAJDisponibleAsync(code).GetAwaiter().GetResult();
                            var textBtnContent = new TextBlock()
                            {
                                FontSize = 11,
                                Text = !string.IsNullOrEmpty(dateMajDispo) ? $"Mise à jour disponible {dateMajDispo} - Mettre à jour" : string.Empty,
                                TextAlignment = TextAlignment.Left,
                                TextWrapping = TextWrapping.Wrap
                            };
                            ctrl.Content = textBtnContent;
                        }));
                    }
                    int index = lstCurrentUpdate.IndexOf(code);
                    if(index >= 0)
                    {
                        lstCurrentUpdate.RemoveAt(index);
                    }
                    await Dispatcher.BeginInvoke(new Action(() =>
                    {
                        string title = "Information";
                        string message = $"Mise à jour de l'événement {code} terminée ! ";
                        bool result = CustomInformationBox.Prompt(title, message);
                    }));
                    
                    //MessageBox.Show($"Mise à jour de l'événement {code} terminée ! ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                
                    await Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if(ctrl != null)
                        {
                            ctrl.Visibility = Visibility.Hidden;
                        }
                        if (timerCountdown != null)
                        {
                            timerCountdown.Start();
                        }
                        
                    }));
                   
                });
            }
            else
            {
                int index = lstCurrentUpdate.IndexOf(code);
                if (index >= 0)
                {
                    lstCurrentUpdate.RemoveAt(index);
                }
                MessageBox.Show("Vous n'êtes pas connecté à internet. ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            if (lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(message, title);
            }
            else
            {
                string Titre = "Fermeture de l'application";
                string Message = "Vous allez quitter l'application, êtes-vous sur ?";
                bool result = CustomMessageBox.Prompt(Titre, Message);
                if (result)
                {

                    Application.Current.Shutdown();
                }
            }
        }

        private void TS_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            TS.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            ScrollViewer scrollviewer = sender as ScrollViewer;
            if (e.Delta > 0)
            {
                scrollviewer.LineUp();
            }
            else
            {
                scrollviewer.LineDown();
            }
            e.Handled = true;
        }

        private void OnPreviewMouseMove(Object sender , MouseButtonEventArgs e )
        {
            System.Windows.Point currentMouse = e.GetPosition(TS);
            if(Math.Abs(currentMouse.Y - TS.ActualHeight) <= 50)
            {
                TS.ScrollToVerticalOffset(currentMouse.Y + 5);
            }

            if (currentMouse.Y <= 50)
            {
                TS.ScrollToVerticalOffset(currentMouse.Y - 5);
            }

        }

        private async void DownloadFromFtp(string eventCode, List<string> listeEventImages)//async Task
        {
            try
            {

                //this.Dispatcher.BeginInvoke((Action)(async () =>
                //{
                var isDownloadOk = Compare.SynchronizeFromFtpToLocalDirectory(eventCode, listeEventImages);//.GetAwaiter().GetResult();
                if (isDownloadOk)
                {
                    //Compare.DeleteLocalFileNotSynchro(eventCode, listeEventImages);
                    var childLblMaj = FindChild(mainContainer, control =>
                    {
                        var txtBlock = control as Button;
                        if (txtBlock != null && txtBlock.Name == $"maj_{eventCode}")
                            return true;
                        else
                            return false;
                    }) as Button;
                    childLblMaj.Content = string.Empty;

                    var lastMaj = FindChild(mainContainer, control =>
                    {
                        var txtBlock = control as Button;
                        if (txtBlock != null && txtBlock.Name == $"lastmaj_{eventCode}")
                            return true;
                        else
                            return false;
                    }) as Button;
                    childLblMaj.Content = string.Empty;
                     MajEnLigneMethod(eventCode);//await
                }
                // })
                // , DispatcherPriority.Background);
            }
            catch (Exception ex)
            {

                Log.Error($"ListEvent.xaml.cs - DownloadFromFtp Error : {ex}");
            }
        }
        private void OnUcLoaded(object sender, RoutedEventArgs e)
        {
            KillMe();
            stopSynchro = false;
            createWorker();
            btnActualiser.IsEnabled = false;
            if (lstCurrentUpdate.Count > 0)
            {
                string title = "Information";
                string message = "Mise à jour d'un événement en cours. Veuillez patientez...";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                mainContainer.Children.Clear();
                if (!isArchive)
                {
                    AddListToStackPanel(cts, isArchive); //.GetAwaiter().GetResult();
                    Task.Factory.StartNew(() =>
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (InternetConnectionManager.checkConnection("www.google.fr"))
                            {
                                //  lbl_loading.Visibility = Visibility.Visible;
                                //Start the timer
                                //dispatcherTimer.Start();

                            }
                        }));
                    }).ContinueWith(task =>
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            //GetEventList(isArchive);

                          /*  if (Synchro_worker != null)
                            {
                                if (Synchro_worker.IsBusy)
                                {
                                    Synchro_worker.WorkerSupportsCancellation = true;
                                    Synchro_worker.CancelAsync();
                                    Synchro_worker.Abort();
                                    Synchro_worker.Dispose();
                                    Synchro_worker = new AbortableBackgroundWorker();
                                }


                                Synchro_worker.RunWorkerAsync();
                            }


                            if (Update_worker != null)
                            {
                                if (Update_worker.IsBusy)
                                {
                                    Update_worker.WorkerSupportsCancellation = true;
                                    Update_worker.CancelAsync();
                                    Update_worker.Abort();
                                    Update_worker.Dispose();
                                    Update_worker = new AbortableBackgroundWorker();
                                }
                                Update_worker.RunWorkerAsync();
                            }
                            */
                            //lbl_loading.Visibility = Visibility.Hidden;
                        }), DispatcherPriority.Background);

                    });
                }
                else
                {
                    AddArchiveListToStackPanel(cts, isArchive);
                }
               

                
            }
           
        } 
        private void SynchroEvent(string code)
        {

            LastOperations.Add(Dispatcher.BeginInvoke(new Action(() =>
             {
                 int valueProgress = (int)IsSynchro(code);
                 var childSynchro = FindChild(mainContainer, control =>
                   {
                       var txtBlock = control as TextBlock;
                       if (txtBlock != null && txtBlock.Name == $"synchro_{code}")
                           return true;
                       else
                           return false;
                   }) as TextBlock;
                 if (childSynchro != null)
                 {
                     childSynchro.Text = $"{valueProgress} %";
                 }


                 var childProgress = FindChild(mainContainer, control =>
                 {
                     var txtBlock = control as ProgressBar;
                     if (txtBlock != null && txtBlock.Name == $"progress_{code}")
                         return true;
                     else
                         return false;
                 }) as ProgressBar;
                 if (childProgress != null)
                 {
                     childProgress.Value = valueProgress;
                     childProgress.Visibility = Visibility.Visible;
                 }
                 var checkFileSynchro = Globals._assetsFolder + "\\" + code + "\\synchro.txt";
                 

                 if (!File.Exists(checkFileSynchro))
                 {

                     if (valueProgress == 100)
                     {
                         string configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
                         IniUtility _iniUtility = new IniUtility(configIniPath);
                         var d = _iniUtility.Read("end", "EVENT");
                         if (!string.IsNullOrEmpty(d))
                         {
                             DateTime endDate = DateTime.ParseExact(_iniUtility.Read("end", "EVENT"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                             DateTime today = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                             if (today > endDate)
                             {
                                 try
                                 {
                                     SendWs(code, idborne);
                                     File.Create(checkFileSynchro);
                                 }
                                 catch (Exception ex)
                                 {
                                     string Titre = "Connexion - Borne : " + idborne;
                                     string Message = ex.Message;
                                     CustomInformationBox.Prompt(Titre, Message);
                                 }

                             }
                         }
                         

                     }
                 }

             }), DispatcherPriority.Background, null));
        }

        private void GetToken(string login, string mdp)
        {
            try
            {
                var client = new WebClient();
                var method = "POST"; // If your endpoint expects a GET then do it.
                var parameters = new NameValueCollection();

                parameters.Add("username", login);
                parameters.Add("password", mdp);

                /* Always returns a byte[] array data as a response. */
                var response_data = client.UploadValues(Globals.ws_url_connexion, method, parameters);

                // Parse the returned data (if any) if needed.
                var responseString = Encoding.UTF8.GetString(response_data);
                ConnexionData obj = JsonConvert.DeserializeObject<ConnexionData>(responseString);
                if (obj.success)
                {
                    Globals.auth_token = "Bearer " + obj.data.token;
                }
            }
            catch (Exception ex)
            {
                string Titre = "Connexion";
                string Message = "Vérifiez votre connexion";
                CustomInformationBox.Prompt(Titre, Message);
            }
        }

        private Boolean SendWs(string code, string borne)
        {
            try
            {
                var client = new WebClient();
                var method = "POST"; // If your endpoint expects a GET then do it.
                var parameters = new NameValueCollection();

                parameters.Add("code_logiciel", code);
                parameters.Add("numero_borne", borne);

                client.Headers.Add("Authorization", Globals.auth_token);
                /* Always returns a byte[] array data as a response. */
                var response_data = client.UploadValues(url, method, parameters);

                // Parse the returned data (if any) if needed.
                var responseString = Encoding.UTF8.GetString(response_data);
            }
            catch(Exception e)
            {
                return true;
            }
            
            return true;
        }

        private void MajEnLigneMethod(string code)
        {
            
            Dispatcher.BeginInvoke(new Action(() =>
            {
                var child = FindChild(mainContainer, control =>
            {
                var txtBlock = control as Button;
                if (txtBlock != null && txtBlock.Name == $"maj_{code}")
                    return true;
                else
                    return false;
            }) as Button;
                var dateMajDispo = GetDateMAJDisponible(code);
                var textBtnContent = new TextBlock()
                {
                    FontSize = 11,
                    Text = (!string.IsNullOrEmpty(dateMajDispo)) ? $"Mise à jour disponible {dateMajDispo}" : string.Empty,//(majExists &&!string.IsNullOrEmpty(dateMajDispo // - Mettre à jour
                    TextAlignment = TextAlignment.Left,
                    TextWrapping = TextWrapping.Wrap
                };
                if (!string.IsNullOrEmpty(textBtnContent.Text))
                {

                    child.Content = textBtnContent;
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        child.Width = 225;
                    }
                    else
                    {
                        child.Width = 225;
                    }
                    
                    child.Visibility = Visibility.Visible;
                }
                else
                {
                    if(child != null)
                    {
                        child.Visibility = Visibility.Hidden;
                    }
                    
                }
            }), DispatcherPriority.Normal, null);
        }

        private DateTime? CompareDateUpdate(string code)
        {
            DateTime? toReturn = null;
            XmlManager _manager = new XmlManager();
            DateTime? localModif = null;
            DateTime? remoteModif = null;
            string fileNameDownloaded = "C:\\Events\\Download\\" + code + "\\Remote\\Update\\files.xml";
            string currentFileName = "C:\\Events\\Assets\\" + code + "\\Update\\files.xml";
            remoteModif = DownloadInfoUpdate(code);
            if (File.Exists(currentFileName))
            {
                //localModif = _manager.GetLastModification(currentFileName);
                localModif = System.IO.File.GetLastWriteTime(currentFileName);
            }
            //if (File.Exists(fileNameDownloaded))
            //{
            //    remoteModif = _manager.GetLastModification(fileNameDownloaded);
            //}
            if(localModif != null && remoteModif != null)
            {
                if(localModif < remoteModif)
                {
                    toReturn = remoteModif;
                }
            }
            else if(localModif == null && remoteModif != null)
            {
                if (Directory.Exists(fileNameDownloaded))
                {
                    if (!Directory.Exists("C:\\Events\\Assets\\" + code + "\\Update"))
                    {
                        Directory.CreateDirectory("C:\\Events\\Assets\\" + code + "\\Update");
                    }
                    File.Copy(fileNameDownloaded, currentFileName);
                }
                
                toReturn = remoteModif;
            }
            return toReturn;
        }

        public void CheckeventsUpdate()
        {
            if (!isArchive)
            {
                IsStarted = true;
                foreach (var item in lstEventCode)
                {
                    UpdateLigneCheckingUpdate(item);
                }
            }
            else
            {
                foreach (var item in lstEventCode)
                {
                    UpdateLigneCheckingUpdate(item);
                }
            }
            
        }

        private void UpdateLigneCheckingUpdate(string code)
        {

            LastOperations.Add(Dispatcher.BeginInvoke(new Action(() =>
            {
                var child = FindChild(mainContainer, control =>
                {
                    var txtBlock = control as Button;
                    if (txtBlock != null && txtBlock.Name == $"maj_{code}")
                        return true;
                    else
                        return false;
                }) as Button;
                var dateMajDispo = CompareDateUpdate(code);

                var textBtnContent = new TextBlock()
                {
                    FontSize = 11,


                    Text = (!string.IsNullOrEmpty(dateMajDispo.ToString())) ? $"Mise à jour disponible {dateMajDispo}" : string.Empty,//(majExists &&!string.IsNullOrEmpty(dateMajDispo // - Mettre à jour
                    TextAlignment = TextAlignment.Left,
                    TextWrapping = TextWrapping.Wrap
                };
                if (child != null)
                {
                    if (!string.IsNullOrEmpty(textBtnContent.Text))
                    {

                        child.Content = textBtnContent;
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            child.Width = 225;
                        }
                        else
                        {
                            child.Width = 225;
                        }

                        child.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (child != null)
                        {
                            child.Visibility = Visibility.Hidden;
                        }

                    }
                }


            }), DispatcherPriority.Background, null));
        }
        private void AddArchiveListToStackPanel(CancellationTokenSource cts, bool isarchive = false)
        {
            foreach (var code in lstArchive)
            {
                //configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
                configIniPath = isarchive ? $"C:/Events/Assets/Archives/{code}//Config.ini" : Globals._assetsFolder + "\\" + code + "\\Config.ini";
                if (File.Exists(configIniPath))
                {
                    setEventsLabel();
                    Style buttonStyle = this.FindResource("noHighlight") as Style;
                    Style buttonRoudedStyle = this.FindResource("RoundButtonTemplate") as Style;

                    Style centerStackPanelContentStyle = this.FindResource("HorizontalStackPanel") as Style;
                    INIFileManager iniFile = new INIFileManager(configIniPath);
                    IniUtility _iniUtility = new IniUtility(configIniPath);
                    string Name = _iniUtility.Read("Name", "EVENT");
                    string codeEvent = _iniUtility.Read("code", "EVENT"); //iniFile.GetSetting("EVENT", "code");
                    string begin = _iniUtility.Read("Begin", "EVENT"); ///iniFile.GetSetting("EVENT", "Begin");
                    string end = _iniUtility.Read("End", "EVENT"); //iniFile.GetSetting("EVENT", "End");
                    string[] splitedBegin = begin.Split(new string[] { "/" }, StringSplitOptions.None);
                    string[] splitedEnd = end.Split(new string[] { "/" }, StringSplitOptions.None);
                    begin = splitedBegin[0] + "/" + splitedBegin[1] + "/" + splitedBegin[2].Substring(2, 2);
                    end = splitedEnd[0] + "/" + splitedEnd[1] + "/" + splitedEnd[2].Substring(2, 2);
                    string period = begin + " au " + end;


                    Grid detailLineStack = new Grid();
                    detailLineStack.Margin = new Thickness(15, 0, 15, 0);
                    detailLineStack.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                    detailLineStack.Name = $"Detail_{code}";
                    detailLineStack.Height = 60;
                    detailLineStack.Width = widthWorkArea - 20;


                    StackPanel stackEventName = new StackPanel();
                    stackEventName.Margin = new Thickness(25, 0, 0, 0);
                    stackEventName.VerticalAlignment = VerticalAlignment.Center;
                    stackEventName.HorizontalAlignment = HorizontalAlignment.Left;
                    stackEventName.Orientation = Orientation.Horizontal;

                    TextBlock eventName = new TextBlock();
                    eventName.FontSize = 14;
                    eventName.MaxWidth = 200;
                    eventName.Tag = codeEvent;
                    eventName.Foreground = System.Windows.Media.Brushes.White;
                    eventName.PreviewMouseDown += EditTextblockEvent_Click;
                    eventName.TextWrapping = TextWrapping.Wrap;
                    eventName.Margin = new Thickness(0, 0, 10, 0);
                    eventName.Inlines.Add(new Run(Name) { FontWeight = FontWeights.Bold });
                    eventName.VerticalAlignment = VerticalAlignment.Center;
                    eventName.HorizontalAlignment = HorizontalAlignment.Left;
                    stackEventName.Children.Add(eventName);
                    TextBlock roundSep = new TextBlock();
                    roundSep.Text = ".";
                    roundSep.FontSize = 40;
                    roundSep.Height = Double.NaN;
                    roundSep.VerticalAlignment = VerticalAlignment.Center;
                    roundSep.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    roundSep.Margin = new Thickness(15, -25, 15, 0);
                    stackEventName.Children.Add(roundSep);

                    TextBlock eventPeriod = new TextBlock();
                    eventPeriod.Text = period;
                    eventPeriod.HorizontalAlignment = HorizontalAlignment.Center;
                    eventPeriod.FontSize = 12;
                    eventPeriod.Foreground = System.Windows.Media.Brushes.White;
                    eventPeriod.Height = 15;
                    eventPeriod.Margin = new Thickness(0, 0, 20, 0);
                    stackEventName.Children.Add(eventPeriod);

                    Button btnDetails = new Button();
                    btnDetails.Name = "btnDetails";
                    btnDetails.Tag = "stack" + codeEvent;
                    btnDetails.Height = 27;
                    btnDetails.Margin = new Thickness(0, 0, 0, 0);
                    btnDetails.HorizontalAlignment = HorizontalAlignment.Left;
                    btnDetails.Style = buttonStyle;
                    btnDetails.Width = 100;
                    btnDetails.Click += Details_Click;
                    ImageBrush btnBrush = new ImageBrush();
                    btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnDetailsClosed, UriKind.Absolute));
                    btnDetails.Background = btnBrush;


                    stackEventName.Children.Add(btnDetails);

                    Button updateDispo = new Button();
                    updateDispo.FontSize = 10;
                    updateDispo.Height = 24;
                    //updateDispo.Width =100;// Double.NaN;

                    updateDispo.Name = $"maj_{code}";
                    updateDispo.Margin = new Thickness(10, 0, 0, 0);
                    updateDispo.VerticalAlignment = VerticalAlignment.Center;
                    updateDispo.Style = buttonRoudedStyle;
                    updateDispo.HorizontalAlignment = HorizontalAlignment.Left;
                    updateDispo.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFF")); //RED
                    updateDispo.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ff0202"));
                    updateDispo.BorderBrush = new SolidColorBrush(Colors.Transparent);
                    updateDispo.BorderThickness = new Thickness(0);
                    updateDispo.Click += async (s, e) => await updateDispo_Clicked(s, e);
                    updateDispo.Visibility = Visibility.Hidden;
                    stackEventName.Children.Add(updateDispo);

                    detailLineStack.Children.Add(stackEventName);
                    /************FIN COLUMN 1**************/

                    /************DEBUT COLUMN 2**************/


                    StackPanel stackEventSynchro = new StackPanel();
                    stackEventSynchro.Orientation = Orientation.Horizontal;

                    TextBlock eventSynchro = new TextBlock();
                    eventSynchro.Name = $"synchro_{code}";
                    eventSynchro.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4dad5d"));
                    eventSynchro.Margin = new Thickness(0, 10, 10, 30);
                    eventSynchro.FontWeight = FontWeights.Bold;
                    eventSynchro.TextWrapping = TextWrapping.Wrap;
                    eventSynchro.HorizontalAlignment = HorizontalAlignment.Right;

                    TextBlock eventcode = new TextBlock();
                    eventcode.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    eventcode.Text = code.ToUpper();
                    eventcode.FontSize = 12;
                    eventcode.Height = 15;
                    eventcode.Width = 50;
                    eventcode.Tag = code;
                    eventcode.PreviewMouseDown += EditTextblockEvent_Click;
                    eventcode.TextWrapping = TextWrapping.Wrap;
                    eventcode.VerticalAlignment = VerticalAlignment.Center;


                    ProgressBar progres = new ProgressBar();
                    System.Windows.Media.Color color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#4dad5d");
                    progres.Foreground = new SolidColorBrush(color);
                    progres.Name = $"progress_{code}";
                    progres.Height = 11;
                    progres.Width = 120;
                    progres.BorderThickness = new Thickness(1);
                    progres.BorderBrush = System.Windows.Media.Brushes.Transparent;
                    progres.Margin = new Thickness(10, -50, 8, 0);
                    progres.Visibility = Visibility.Hidden;

                    StackPanel stackPanelColumn2 = new StackPanel();
                    stackPanelColumn2.Orientation = Orientation.Horizontal;
                    stackPanelColumn2.Children.Add(stackEventSynchro);
                    stackPanelColumn2.HorizontalAlignment = HorizontalAlignment.Left;
                    detailLineStack.Children.Add(stackPanelColumn2);
                    /************FIN COLUMN 2**************/

                    /************DEBUT COLUMN 3**************/
                    StackPanel rightButton = new StackPanel();
                    rightButton.Margin = new Thickness(0, 0, 40, 0);
                    rightButton.Orientation = Orientation.Horizontal;
                    rightButton.HorizontalAlignment = HorizontalAlignment.Right;
                    rightButton.Width = Double.NaN;

                    Button launchAnimation = new Button();
                    launchAnimation.Tag = codeEvent;
                    launchAnimation.Click += LaunchAnimation_Click;
                    launchAnimation.Name = "launchAnimation";
                    launchAnimation.Width = 120;
                    launchAnimation.Height = 20;
                    launchAnimation.Margin = new Thickness(10, 0, 35, 0);
                    launchAnimation.Style = buttonStyle;

                    ImageBrush btnLaunchAnimBrush = new ImageBrush();
                    btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
                    launchAnimation.Background = btnLaunchAnimBrush;
                    StackPanel stackSynchro = new StackPanel();
                    stackSynchro.Orientation = Orientation.Vertical;
                    stackSynchro.Children.Add(eventSynchro);
                    stackSynchro.Children.Add(progres);

                    rightButton.Children.Add(eventcode);
                    rightButton.Children.Add(stackSynchro);
                    rightButton.Children.Add(launchAnimation);
                    detailLineStack.Children.Add(rightButton);
                    /************FIN COLUMN 3**************/
                    mainContainer.Children.Add(detailLineStack);



                    string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";

                    StackPanel row2Container = new StackPanel();
                    row2Container.Margin = new Thickness(15, 0, 15, 0);
                    row2Container.Width = detailLineStack.Width - 40;
                    row2Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));

                    StackPanel stackTemp = new StackPanel();

                    stackTemp.Name = "stack" + codeEvent;
                    stackTemp.Orientation = Orientation.Horizontal;

                    int nbPhotos = getNbPhotos(codeEvent);
                    int nbprinted = getNbPrinted(codeEvent);

                    string dtlPhotos = (nbPhotos > 1) ? $"PHOTOS" : $"PHOTO";
                    string dtlPrinted = (nbprinted > 1) ? $"IMPRESSIONS" : $"IMPRESSION";
                    FileInfo newestFile = GetNewestFile(new DirectoryInfo(photosDirectory));
                    string dateLastFile = "";
                    if (newestFile != null)
                    {
                        DateTime lastCrea = newestFile.CreationTime;
                        string day = lastCrea.Day.ToString();
                        string month = lastCrea.Month.ToString();
                        string hour = lastCrea.Hour.ToString();
                        string minute = lastCrea.Minute.ToString();
                        if (day.Length == 1)
                        {
                            day = "0" + day;
                        }
                        if (month.Length == 1)
                        {
                            month = "0" + month;
                        }
                        if (hour.Length == 1)
                        {
                            hour = "0" + hour;
                        }
                        if (minute.Length == 1)
                        {
                            minute = "0" + minute;
                        }
                        //dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString().Substring(2, 2) + " à " + hour + "h" + minute;
                        dateLastFile = "Dernière photo prise le " + day + "/" + month + "/" + lastCrea.Year.ToString() + " à " + hour + "h" + minute;
                    }
                    else
                    {
                        dateLastFile = "Pas encore de photos prise";
                    }
                    


                    TextBlock details_nbPhotos = new TextBlock();
                    details_nbPhotos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    details_nbPhotos.FontSize = 20;
                    details_nbPhotos.FontWeight = FontWeights.Bold;
                    details_nbPhotos.Margin = new Thickness(10, 17, 0, 0);
                    details_nbPhotos.Text = nbPhotos.ToString();
                    TextBlock details_photos = new TextBlock();
                    details_photos.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    details_photos.FontSize = 14;
                    details_photos.FontWeight = FontWeights.Bold;
                    details_photos.Margin = new Thickness(20, 22, 0, 0);
                    details_photos.Text = dtlPhotos;
                    TextBlock details_nbprinted = new TextBlock();
                    details_nbprinted.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    details_nbprinted.FontSize = 20;
                    details_nbprinted.FontWeight = FontWeights.Bold;
                    details_nbprinted.Margin = new Thickness(0, 17, 0, 0);
                    details_nbprinted.Text = nbprinted.ToString();
                    TextBlock details_printed = new TextBlock();
                    details_printed.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    details_printed.FontSize = 14;
                    details_printed.FontWeight = FontWeights.Bold;
                    details_printed.Margin = new Thickness(20, 22, 0, 0);
                    details_printed.Text = dtlPrinted;
                    StackPanel details_txt = new StackPanel();
                    details_txt.Orientation = Orientation.Horizontal;
                    details_txt.Children.Add(details_nbPhotos);
                    details_txt.Children.Add(details_photos);
                    TextBlock roundSep1 = new TextBlock();
                    roundSep1.Text = ".";
                    roundSep1.FontSize = 60;
                    roundSep1.Height = Double.NaN;
                    roundSep1.VerticalAlignment = VerticalAlignment.Center;
                    roundSep1.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    roundSep1.Margin = new Thickness(25, -30, 25, 0);
                    details_txt.Children.Add(roundSep1);
                    details_txt.Children.Add(details_nbprinted);
                    details_txt.Children.Add(details_printed);
                    TextBlock roundSep2 = new TextBlock();
                    roundSep2.Text = ".";
                    roundSep2.FontSize = 60;
                    roundSep2.Height = Double.NaN;
                    roundSep2.VerticalAlignment = VerticalAlignment.Center;
                    roundSep2.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    roundSep2.Margin = new Thickness(25, -30, 25, 0);
                    details_txt.Children.Add(roundSep2);
                    //details_txt.Children.Add(roundSep);
                    stackTemp.Children.Add(details_txt);

                    stackTemp.Height = 60;
                    stackTemp.VerticalAlignment = VerticalAlignment.Center;
                    stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#f4f3f8"));
                    stackTemp.Margin = new Thickness(25, 0, 18, 0);
                    stackTemp.Style = centerStackPanelContentStyle;
                    TextBlock lastCreation = new TextBlock();
                    lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                    lastCreation.FontSize = 12;
                    lastCreation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    lastCreation.Margin = new Thickness(30, 22, 0, 0);
                    lastCreation.Text = dateLastFile;

                    stackTemp.Children.Add(lastCreation);


                    StackPanel detailsdownConfig = new StackPanel();
                    detailsdownConfig.Margin = new Thickness(30, 0, 0, 0);
                    detailsdownConfig.VerticalAlignment = VerticalAlignment.Center;
                    detailsdownConfig.HorizontalAlignment = HorizontalAlignment.Right;
                    detailsdownConfig.Height = 20;
                    detailsdownConfig.Orientation = Orientation.Horizontal;

                    DockPanel downConfig = new DockPanel();

                    downConfig.Margin = new Thickness(30, 0, 0, 0);
                    downConfig.VerticalAlignment = VerticalAlignment.Center;
                    downConfig.Width = detailLineStack.Width;
                    downConfig.Height = 80;
                    //downConfig.Orientation = Orientation.Horizontal;

                    StackPanel buttonEvent = new StackPanel();
                    //buttonEvent.Margin = new Thickness(30, 0, 0, 0);
                    buttonEvent.HorizontalAlignment = HorizontalAlignment.Left;
                    buttonEvent.VerticalAlignment = VerticalAlignment.Center;
                    buttonEvent.Height = 80;
                    buttonEvent.Orientation = Orientation.Horizontal;


                    StackPanel downloadEvent = new StackPanel();
                    downloadEvent.Margin = new Thickness(0, 0, 50, 0);
                    downloadEvent.HorizontalAlignment = HorizontalAlignment.Right;
                    downloadEvent.VerticalAlignment = VerticalAlignment.Center;
                    downloadEvent.Height = 80;
                    downloadEvent.Orientation = Orientation.Horizontal;

                    System.Windows.Controls.Image imgOk = new System.Windows.Controls.Image();
                    imgOk.Margin = new Thickness(50, 0, 0, 0);
                    imgOk.Height = 10;
                    imgOk.Width = 10;
                    Bitmap imageBitmap = new Bitmap(Globals._defaultokconfig);

                    imgOk.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);



                    TextBlock lastVerif = new TextBlock();
                    //lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));
                    lastVerif.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#8d8d8d"));
                    lastVerif.Name = $"LastVerif_{codeEvent}";
                    lastVerif.FontSize = 12;
                    lastVerif.Height = 20;
                    lastVerif.Margin = new Thickness(-10, 0, 0, 0);
                    lastVerif.FontStyle = FontStyles.Italic;
                    lastVerif.Text = GetDateDerniereMAJ(code);

                    detailsdownConfig.Children.Add(lastVerif);
                    stackTemp.Children.Add(imgOk);
                    stackTemp.Children.Add(detailsdownConfig);
                    ImageBrush imgBrushDelete = new ImageBrush();
                    imgBrushDelete.ImageSource = new BitmapImage(new Uri(Globals._defaultbtndeleteEvent, UriKind.Absolute));

                    ImageBrush imgBrushArchive = new ImageBrush();
                    imgBrushArchive.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnarchiveEvent, UriKind.Absolute));


                    if (!isarchive)
                    {

                        Button ArchiverEvent = new Button();
                        ArchiverEvent.Content = "";
                        ArchiverEvent.FontWeight = FontWeights.Bold;
                        ArchiverEvent.Click += archiver_Click;
                        ArchiverEvent.Name = $"Archiver_{codeEvent}";
                        ArchiverEvent.Height = 50;
                        ArchiverEvent.Width = 60;

                        ArchiverEvent.Margin = new Thickness(0, 0, 10, 0);
                        ArchiverEvent.Style = buttonStyle;
                        ArchiverEvent.Background = imgBrushArchive;
                        ArchiverEvent.VerticalAlignment = VerticalAlignment.Center;
                        //ArchiverEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                        //ArchiverEvent.Background = new SolidColorBrush(Colors.Transparent);
                        ArchiverEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                        buttonEvent.Children.Add(ArchiverEvent);

                        Button SupprimerEvent = new Button();
                        SupprimerEvent.Content = "";
                        SupprimerEvent.Click += supprimer_Click;
                        SupprimerEvent.Name = $"Supprimer_{codeEvent}";
                        SupprimerEvent.Height = 50;
                        SupprimerEvent.Width = 60;

                        SupprimerEvent.FontWeight = FontWeights.Bold;
                        SupprimerEvent.Margin = new Thickness(0, 0, 20, 0);
                        SupprimerEvent.Style = buttonStyle;
                        SupprimerEvent.Background = imgBrushDelete;
                        SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                        //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                        //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                        SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                        buttonEvent.Children.Add(SupprimerEvent);

                        Button ForceDownloadConfig = new Button();
                        ForceDownloadConfig.Content = "TELECHARGER LA CONFIGURATION";
                        ForceDownloadConfig.Click += async (s, e) => await ForceDownloadConfig_Click(s, e);
                        ForceDownloadConfig.Name = $"ForcDownConfig_{codeEvent}";
                        ForceDownloadConfig.Height = 40;
                        ForceDownloadConfig.Width = 225;
                        ForceDownloadConfig.HorizontalAlignment = HorizontalAlignment.Right;
                        ForceDownloadConfig.Margin = new Thickness(20, 0, 35, 0);
                        ForceDownloadConfig.Style = buttonRoudedStyle;
                        ForceDownloadConfig.VerticalAlignment = VerticalAlignment.Center;
                        ForceDownloadConfig.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#575757"));//gray
                        ForceDownloadConfig.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828")); ;
                        ForceDownloadConfig.BorderBrush = new SolidColorBrush(Colors.Transparent);

                        downloadEvent.Children.Add(ForceDownloadConfig);


                    }
                    else
                    {
                        Button RemettreEvent = new Button();
                        RemettreEvent.Content = "";
                        RemettreEvent.FontWeight = FontWeights.Bold;
                        RemettreEvent.Click += remettre_Click;
                        RemettreEvent.Name = $"Remettre_{codeEvent}";
                        RemettreEvent.Height = 50;
                        RemettreEvent.Width = 60;
                        RemettreEvent.Margin = new Thickness(20, 0, 10, 0);
                        RemettreEvent.Style = buttonStyle;
                        RemettreEvent.VerticalAlignment = VerticalAlignment.Center;
                        //RemettreEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                        //RemettreEvent.Background = new SolidColorBrush(Colors.Transparent);
                        RemettreEvent.Background = imgBrushArchive;
                        //RemettreEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                        downloadEvent.Children.Add(RemettreEvent);

                        Button SupprimerEvent = new Button();
                        SupprimerEvent.Content = "";
                        SupprimerEvent.Click += supprimerArchive_Click;
                        SupprimerEvent.Name = $"SupprimerArchive_{codeEvent}";
                        SupprimerEvent.Height = 50;
                        SupprimerEvent.Width = 60;

                        SupprimerEvent.FontWeight = FontWeights.Bold;
                        SupprimerEvent.Margin = new Thickness(0, 0, 70, 0);
                        SupprimerEvent.Style = buttonStyle;
                        SupprimerEvent.Background = imgBrushDelete;
                        SupprimerEvent.VerticalAlignment = VerticalAlignment.Center;
                        //SupprimerEvent.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF4500")); //RED
                        //SupprimerEvent.Background = new SolidColorBrush(Colors.Transparent);
                        SupprimerEvent.BorderBrush = new SolidColorBrush(Colors.Transparent);

                        downloadEvent.Children.Add(SupprimerEvent);
                    }



                    downConfig.Children.Add(buttonEvent);
                    downConfig.Children.Add(downloadEvent);
                    //stackTemp.Children.Add(downConfig);
                    stackTemp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3d3b3c"));

                    stackTemp.Visibility = Visibility.Collapsed;

                    row2Container.Children.Add(stackTemp);
                    mainContainer.Children.Add(row2Container);

                    StackPanel row3Container = new StackPanel();
                    row3Container.Name = "stack1" + codeEvent;
                    row3Container.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#373737"));
                    row3Container.Width = detailLineStack.Width;
                    row3Container.Height = 90;
                    row3Container.Margin = new Thickness(20, 0, 20, 0);
                    row3Container.Visibility = Visibility.Collapsed;
                    row3Container.Children.Add(downConfig);
                    mainContainer.Children.Add(row3Container);
                    Separator sep = new Separator();
                    sep.Name = $"sep_{code}";
                    sep.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#282828"));
                    sep.Style = this.FindResource("separatorStyle") as Style;
                    sep.Margin = new Thickness(20, 0, 20, 0);
                    sep.Height = 20;
                    mainContainer.Children.Add(sep);
                }

            } //Fin foreach...
        }// Fin AddListToStackPanel...
        private void MiseAJourEventList(CancellationTokenSource cts, bool isarchive = false)
        {
            if (!isarchive) {
                IsStarted = true;

               
                foreach (var item in lstEventCode)
                {
                    MajEnLigneMethod(item);
                   
                }
            }
            else
            {
                
                foreach (var item in lstArchive)
                {
                    MajEnLigneMethod(item);
                    
                }
            }
        }

        private bool isStarted;
        public bool IsStarted
        {
            get
            {
                return isStarted;
            }
            set
            {
                isStarted = value;
            }
        }

        private void SynchroEventList(CancellationTokenSource cts, bool isarchive = false)
        {
            if (!isarchive)
            {
                IsStarted = true;

                cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;
                foreach (var item in lstEventCode)
                {
                    SynchroEvent(item);
                }
            }
            else
            {
                foreach (var item in lstEventCode)
                {
                    SynchroEvent(item);
                }
            }
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                btnActualiser.IsEnabled = true;
            }));
           
        }

        private bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public double IsSynchro(string code)
        {
            string FileUrl = "https://booth.selfizee.fr/transfert/files_" + code + ".txt";
            if (chk_con())
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.DownloadFile(FileUrl, Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt");

                }
                catch (Exception e)
                {

                }
            }
            string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            double progress =0;
            if (File.Exists(FilePath))
            {
                string TxtFileName = System.IO.Path.GetFileName(FilePath);
                char[] delimiters = { '_', '.' };
                string TxtEventName = TxtFileName.Split(delimiters)[1];
            }
            int nbContentRemoteFile = 0;
            int nbLocalContentFile = 0;
            string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            string MediaPhotosPath = Globals._MediaFolder + "\\" + code + "\\Photos";
            List<string> TxtLine = new List<string>();
            List<string> MediaLine = new List<string>();
            if (!File.Exists(MediaPath))
            {
                progress = 0;
            }
            else
            {
                string line;
                nbLocalContentFile = Directory.GetFiles(MediaPhotosPath).Count();
                if (File.Exists(FilePath))
                {
                    using (StreamReader TxtSR = new StreamReader(FilePath))
                    {
                        nbContentRemoteFile = File.ReadLines(FilePath).Count();
                    }
                  
                    try
                    {
                        if(nbContentRemoteFile == 0)
                        {
                            progress = 0;
                        }
                        else if (nbLocalContentFile <= nbContentRemoteFile)
                        {
                            progress = 100;// (double)nbContentRemoteFile / nbLocalContentFile * 100;
                        }
                        else if (nbLocalContentFile > nbContentRemoteFile)
                        {
                           
                            progress = ((double)nbContentRemoteFile / (double)nbLocalContentFile) * 100;
                        }

                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    progress = 0;
                }
                
            }
            return progress;
        }

     
    }

    public class Event
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string period { get; set; }
        public ImageBrush ImagePath { get; set; }
        public string synchro { get; set; }
        public int Progress { get; set; }
        public ImageBrush edit { get; set; }
        public ImageBrush run { get; set; }
    }
}
