﻿using Newtonsoft.Json;
using Selfizee.Managers;
using SimpleWifi;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for RunDownload.xaml
    /// </summary>
    public partial class RunDownload : UserControl
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);
        private const UInt32 WM_SYSCOMMAND = 0x112;
        private const UInt32 SC_RESTORE = 0xf120;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        private string OnScreenKeyboadApplication = "osk.exe";
        private string baseUri = "https://booth.selfizee.fr/evenements/getByCodeLogiciel/";
        private static Wifi wifi;

        public RunDownload()
        {
            wifi = new Wifi();
            InitializeComponent();

            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            //List_event.Foreground = System.Windows.Media.Brushes.White;
            //    Add_event.Foreground =  (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));
            Globals._eventToDownload = "";
            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            code.GotKeyboardFocus += TextBoxGetFocused;
            code.LostFocus += LostTextBoxKeyboardFocus;
            code.TextChanged += TextboxChangeValue;
            alphanumeriquekeyboard.ActiveContainer = code;
            
            ImageBrush btndownload = new ImageBrush();
            btndownload.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDownload, UriKind.Absolute));
            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Absolute));
            _back.Background = btnback;


            btn_download.Background = btndownload;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            close.Background = brushCroix;
            ImageBrush btnnext = new ImageBrush();
            btnnext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationFormNext, UriKind.Absolute));

            scrollNoEvent.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            scrollNoEvent.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

            int x = (int)SystemParameters.PrimaryScreenWidth;
            keybordDck.Width = x;
            keybordDck.Height = 400;
            alphanumeriquekeyboard.Width = (x * 90) / 100;
            alphanumeriquekeyboard.Height = 350;
        }

        private void gotoUserFromMendatory(object sender, RoutedEventArgs e)
        {
            
        }

        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }

        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(RunDownload));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion

        private void closeInfo(object sender, EventArgs e)
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollNoEvent.Visibility = Visibility.Collapsed;
        }

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (RunDownloadViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (RunDownloadViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (RunDownloadViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        public int checkEvent(string numEvent)
        {
            int result = 0;
            if (InternetConnectionManager.checkConnection("www.google.com"))
            {
                string finalUri = baseUri + numEvent + ".json";
                WebClient client = new WebClient();

                // Add a user agent header in case the 
                // requested URI contains a query.

                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                
                try
                {
                    Stream data = client.OpenRead(finalUri);
                    StreamReader reader = new StreamReader(data);
                    string jsonContent = reader.ReadToEnd();
                    dynamic stuff = JsonConvert.DeserializeObject(jsonContent);
                    data.Close();
                    reader.Close();

                    if (stuff.success == true)
                    {
                        Globals._nameEventToDownload = stuff.evenement.nom;
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return -1;
            }
          
            
            return result;

        }

        public void TextboxChangeValue(object sender, EventArgs e)
        {
            code.Text = ((TextBox)sender).Text.ToUpper();
        }

        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            if (string.IsNullOrEmpty(code.Text))
            {
                watermark.Visibility = Visibility.Visible;
            }
            else
            {
                watermark.Visibility = Visibility.Collapsed;
            }
            


        }

        public void TextBoxGetFocused(object sender, EventArgs e)
        {
            alphanumeriquekeyboard.Visibility = Visibility.Visible;
            watermark.Visibility = Visibility.Collapsed;
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (RunDownloadViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void closeScroll(object sender, RoutedEventArgs e)
        {
            scrollNoEvent.Visibility = Visibility.Collapsed;
        }
            

        private void RunDownload_Click(object sender, RoutedEventArgs e)
        {
            Globals._eventToDownload = code.Text;
            if (string.IsNullOrEmpty(code.Text))
            {
                string title = "Information";
                string message = "Veuillez saisir le code de l'événement à télécharger!!";
                bool result = CustomInformationBox.Prompt(title, message);
            }
            else
            {
                int result = 0;
                if (code.Text == "")
                {
                    result = 0;
                }
                else
                {
                    result = checkEvent(code.Text);
                }

                string currentDirectory = Globals._assetsFolder + "\\" + Globals._eventToDownload;

                if (result > 0)
                {
                    if (File.Exists(currentDirectory + "\\Config.ini"))
                    {
                        var viewModel = (RunDownloadViewModel)DataContext;
                        if (viewModel.GoToChoiceDownload.CanExecute(null))
                            viewModel.GoToChoiceDownload.Execute(null);
                    }
                    else
                    {
                        var viewModel = (RunDownloadViewModel)DataContext;
                        if (viewModel.GoToDownloadDetail.CanExecute(null))
                            viewModel.GoToDownloadDetail.Execute(null);
                    }

                }
                else
                {
                    if (result == 0)
                    {
                        string title = "Erreur";
                        string message = "L'événement N°: " + code.Text + " n'existe pas dans le serveur distant";
                        CustomInformationBox.Prompt(title, message);
                        //noEvent.Text = "L'événement N°: " + code.Text + " n'existe pas dans le serveur distant";
                        //scrollNoEvent.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        string title = "Erreur";
                        string message = "Impossible de se connecter à internet";
                        CustomInformationBox.Prompt(title, message);
                        //noEvent.Text = "Impossible de se connecter à internet";
                        //scrollNoEvent.Visibility = Visibility.Visible;
                    }

                }

            }

        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (RunDownloadViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter l'application, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
