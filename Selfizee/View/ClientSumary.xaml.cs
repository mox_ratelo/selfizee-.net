﻿using log4net;
using Selfizee.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ClientSumary.xaml
    /// </summary>
    public partial class ClientSumary : UserControl
    {
        public ClientSumary()
        {
            InitializeComponent();

            Bitmap imagePuce = new Bitmap(Globals._defaultBtnPuceGrise);
            Img_puce1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);
            Img_puce2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);

            Bitmap imageWifi = new Bitmap(Globals._defaultBtnIcoWifi);
            wifi_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imageWifi);

            Bitmap imageConnect = new Bitmap(Globals._defaultBtnIcoConnect);
            img_connect.Source = ImageUtility.convertBitmapToBitmapImage(imageConnect);

            Bitmap imagePhoto = new Bitmap(Globals._defaultBtnIcoPhoto);
            photo_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imagePhoto);

            Bitmap imageSetting = new Bitmap(Globals._defaultBtnIcoSetting);
            setting_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageSetting);

            Bitmap imageAssist = new Bitmap(Globals._defaultBtnIcoAssist);
            assist_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageAssist);

            Bitmap imagePuceBtn = new Bitmap(Globals._defaultBtnIcoPuceBtn);
            icone_pucebtn1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn4.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);

            Bitmap imagelogoNoir = new Bitmap(Globals._defaultLogoNoir);
            img_logo.Source = ImageUtility.convertBitmapToBitmapImage(imagelogoNoir);

            string bidon;



        }
        // public void OnLoaded(object sender, RoutedEventArgs e)
        //{
        //    var viewModel = (ClientSumaryViewModel)DataContext;
            
        //    if (viewModel.GoToClientSumary.CanExecute(null))
        //    {
        //        viewModel.GoToClientSumary.Execute(null);
        //    }
        //}
    }
}
