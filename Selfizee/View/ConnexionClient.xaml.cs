﻿using Newtonsoft.Json;
using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using static Selfizee.View.HistoUpdateWindow;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ConnexionClient.xaml
    /// </summary>
    /// 

    public partial class ConnexionClient : UserControl
    {
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        private int timeout { get; set; }
        private bool autoupdate = false;
        IniUtility iniWriting = new IniUtility(Globals._appConfigFile);
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        public ConnexionClient()
        {
            InitializeComponent();
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            alphanumeriquekeyboardForm1.Visibility = Visibility.Hidden;
            if (!Globals.getRemoteElements)
            {
                DownloadUpdateElements();
                Globals.getRemoteElements = true;
            }

            if (Globals.ScreenType.ToUpper() == "SPHERIK")
            {
                int x = (int)SystemParameters.PrimaryScreenWidth;
                keybordDck.Width = x;
                keybordDck.Height = 400;
                allDck.Width = x;
                alphanumeriquekeyboardForm1.Width = (x * 90) / 100;
                alphanumeriquekeyboardForm1.Height = 300;
                keybordDck1.Width = x;
                allGrid.Width = x;
                dockall.Width = x;
            }

            ImageBrush btnBrush = new ImageBrush();
            if (File.Exists(Globals._defaultbtnConnexion))
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConnexion, UriKind.Absolute));
            btnConnexion.Background = btnBrush;
            Bitmap imageBitmap = new Bitmap(Globals._defaultbtnselfizee_logo1);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            setTimeout();
            LaunchTimerAfter2();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingPanel.ClosePanel();
            }));

        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    if (timerCountdown != null) timerCountdown.Stop();
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void GotoAccueil()
        {
            var viewModel = (ConnexionClientViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void GotoAccueil(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (ConnexionClientViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown_minute.Stop();
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(2);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                timeout = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                timeout = 10;
            }


            if (timeout < 0)
            {
                timeout *= -1;
            }
        }

        private void DownloadUpdateElements()
        {
            if (InternetConnectionManager.checkConnection("booth.selfizee.fr"))//(InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                string jsonString = "";
                try
                {
                    string idborne = iniWriting.Read("idborne", "EVENTSCONFIG");
                    string json = new WebClient().DownloadString("https://booth.selfizee.fr/version-logiciels/getListVersionLogicielByBorne/" + idborne + ".json");
                    Write(json);
                    jsonString = Read();
                }
                catch (Exception e)
                {

                }
                var records = JsonConvert.DeserializeObject<List<RootObject>>(jsonString);
                if (records != null)
                {
                    RootObject root = records.First();
                    string url = root.url_photo_couverture;
                    string dir = "C:\\Events\\Assets\\Default\\Images\\img_couv.jpg";
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(new Uri(url), dir);
                    }
                }

            }
            else
            {

            }
        }
        public void Write(string input)
        {
            string path = @"C:\\Events\\JsonText.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            if (File.Exists(path))
            {
                File.Delete(path);
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(input);
                }
            }
        }

        public string Read()
        {
            string outputs = "";
            string path = @"C:\\Events\\JsonText.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    outputs = s;
                }
            }
            return outputs;
        }

        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if (timerCountdown != null) timerCountdown.Stop();
            setTimeout();
            LaunchTimerAfter2();
        }

        public void TxtGotFocus(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            alphanumeriquekeyboardForm1.Visibility = Visibility.Visible;
            alphanumeriquekeyboardForm1.ActiveContainer = textbox;
        }

        public void TxtLostFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    ShowButton();
                    break;
            }
        }

        private void ShowButton(object sender, RoutedEventArgs e)
        {
            var mdp = Mdp.Text.ToUpper();
            string code = iniWriting.Read("id", "EVENTSCONFIG");
            if (mdp.Equals(code))
            {
                GotoClient();
            }
            else
            {
                string Titre = "Connexion";
                string Message = "Mot de passe incorrect";
                CustomInformationBox.Prompt(Titre, Message);
            }
        }

        private void ShowButton()
        {
            var mdp = Mdp.Text.ToUpper();
            string code = iniWriting.Read("id", "EVENTSCONFIG");
            if (mdp.Equals(code))
            {
                GotoClient();
            }
            else
            {
                string Titre = "Connexion";
                string Message = "Mot de passe incorrect";
                CustomInformationBox.Prompt(Titre, Message);
            }
        }

        private void GotoClient()
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                }));

            }).ContinueWith(task =>
            {
                Dispatcher.BeginInvoke((Action)(() =>
                {
                    KillMe();
                    Globals._FlagConfig = "client";
                    var viewModel = (ConnexionClientViewModel)DataContext;
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                            viewModel.GoToClientSumarySpherik.Execute(null);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        if (viewModel.GoToClientSumary.CanExecute(null))
                            viewModel.GoToClientSumary.Execute(null);
                    }
                }));
            });
                
        }

        private void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();

            //  System.GC.Collect();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
