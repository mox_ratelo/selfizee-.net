﻿using Selfizee.Managers;
using Selfizee.Models;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour WifiList.xaml
    /// </summary>
    public partial class WifiList : UserControl
    {
        private static Wifi wifi;
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        public int timeout;
        public string errormsg = "Erreur connexion WIFI";
       // private DispatcherTimer dispatcherTimer;
        string push = "";

        List<DispatcherOperation> LastOperations;

        private void StopOperation()
        {
            foreach (var operation in LastOperations)
            {
                if (operation != null)
                {
                    if (operation.Status == DispatcherOperationStatus.Executing)
                    {
                        operation.Wait();
                    }
                    operation.Abort();
                }
            }
            LastOperations.Clear();
        }
        public WifiList()
        {
            wifi = new Wifi();
            //Globals._FlagConfig = "client";
            InitializeComponent();
            LastOperations = new List<DispatcherOperation>();

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Relative));

            Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);
            
            string code = Globals.codeEvent_toEdit;
            if (string.IsNullOrEmpty(code))
            {
                code = Globals.GetEventId();
            }
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));

            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            var brushTB = new ImageBrush();
            brushTB.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnTB, UriKind.Relative));
            if (Globals._FlagConfig == "client")
            {
                //btnTB.Background = brushTB;
                borderTB.Visibility = Visibility.Visible;
            }
            else if (Globals._FlagConfig == "admin")
            {
                btnTB.Width = 58;
                btnTB.Height = 44;
                btnTB.Background = brushCroix;
                btnTB.Visibility = Visibility.Visible;
            }
            if (Globals._FlagConfig == "client")
            {
                //var brushRetour = new ImageBrush();
                //brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAnim, UriKind.Relative));
                //btnRetour.Background = brushRetour;
                borderRetour.Visibility = Visibility.Visible;
                Title.FontSize = 20;
                stackTitle.Margin = new Thickness(0, -40, 0, 10);
            }
            else if (Globals._FlagConfig == "admin")
            {
                borderRetour.Visibility = Visibility.Hidden;
            }

            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);

            //Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            //ico_home.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);

            //Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            //lvWifi.ItemsSource = GetAccessPoints();
            
            imgCercle.Visibility = Visibility.Hidden;
           /* dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);*/
            setListWifi();
            LaunchTimerAfter2();
        }
        
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if (timerCountdown != null) timerCountdown.Stop();
            string code = Globals.codeEvent_toEdit;
            if (string.IsNullOrEmpty(code))
            {
                code = Globals.GetEventId();
            }
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            LaunchTimerAfter2();
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            //LoadingPanel.ClosePanel();
            //lblInfo.Visibility = Visibility.Hidden;
            //Disable the timer
          //  dispatcherTimer.IsEnabled = false;
        }
        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }
        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(WifiList));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        public void setListWifi()
        {
            LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingPanel.ShowPanel("Chargement en cours...");
            }))) ;
            LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
            {

                //Start the timer
              //  dispatcherTimer.Start();
                List<WifiModel> wifi = GetAccessPoints();
                wflist.Children.Clear();
                foreach (WifiModel wf in wifi)
                {
                    Border border = new Border();
                    border.CornerRadius = new CornerRadius(5);
                    border.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(63, 63, 63));
                    border.BorderBrush = new SolidColorBrush(Colors.Transparent);
                    border.BorderThickness = new Thickness(0);
                    border.Height = 100;
                    border.Margin = new Thickness(20, 5, 20, 5);

                    Grid grid = new Grid();
                    grid.HorizontalAlignment = HorizontalAlignment.Stretch;
                    DockPanel.SetDock(grid, Dock.Top);

                    ColumnDefinition col1 = new ColumnDefinition();
                    col1.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(col1);
                    ColumnDefinition col2 = new ColumnDefinition();
                    col2.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(col2);
                    ColumnDefinition col3 = new ColumnDefinition();
                    col3.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(col3);
                    ColumnDefinition col4 = new ColumnDefinition();
                    col4.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(col4);


                    Label lblWifiName = new Label();
                    lblWifiName.Content = wf.WifiName;
                    if (String.IsNullOrEmpty(wf.WifiName))
                    {
                        lblWifiName.Content = "Réseau masqué";
                    }


                    lblWifiName.Foreground = new SolidColorBrush(Colors.White);
                    lblWifiName.FontSize = 20;
                    lblWifiName.VerticalAlignment = VerticalAlignment.Center;
                    lblWifiName.Margin = new Thickness(20, 0, 0, 0);
                    Grid.SetColumn(lblWifiName, 0);

                    if (wf.IsConnected == "Oui")
                    {
                        System.Windows.Controls.Image imageWifiStatus = new System.Windows.Controls.Image();
                        Bitmap imageConnect = new Bitmap(Globals._defaultBtnIcoConnect);
                        imageWifiStatus.Source = ImageUtility.convertBitmapToBitmapImage(imageConnect);
                        imageWifiStatus.Width = 20;
                        imageWifiStatus.Height = 20;
                        imageWifiStatus.HorizontalAlignment = HorizontalAlignment.Left;
                        imageWifiStatus.VerticalAlignment = VerticalAlignment.Center;
                        imageWifiStatus.Margin = new Thickness(25, 0, 0, 0);
                        Grid.SetColumn(imageWifiStatus, 1);
                        grid.Children.Add(imageWifiStatus);
                    }
                    else
                    {
                        Label lblWifiStatus = new Label();
                        lblWifiStatus.Content = wf.IsConnected;
                        lblWifiStatus.Foreground = new SolidColorBrush(Colors.White);
                        lblWifiStatus.FontSize = 20;
                        lblWifiStatus.VerticalAlignment = VerticalAlignment.Center;
                        lblWifiStatus.Margin = new Thickness(20, 0, 0, 0);
                        Grid.SetColumn(lblWifiStatus, 1);
                        grid.Children.Add(lblWifiStatus);
                    }

                    Label lblWifiSignal = new Label();
                    lblWifiSignal.Content = wf.SignalStrength;
                    lblWifiSignal.Foreground = new SolidColorBrush(Colors.White);
                    lblWifiSignal.FontSize = 20;
                    lblWifiSignal.VerticalAlignment = VerticalAlignment.Center;
                    lblWifiSignal.Margin = new Thickness(20, 0, 0, 0);
                    Grid.SetColumn(lblWifiSignal, 2);

                    Button btnConnecter = new Button();
                    btnConnecter.Width = 200;
                    btnConnecter.Height = 50;
                    btnConnecter.Tag = wf.WifiName.ToString();
                    btnConnecter.BorderBrush = new SolidColorBrush(Colors.Transparent);
                    btnConnecter.Style = (Style)Application.Current.Resources["noHighlight"];
                    btnConnecter.Click += btnConnection_Click;
                    Grid.SetColumn(btnConnecter, 4);


                    if (wf.IsConnected == "Oui")
                    {
                        var brushDeConnect = new ImageBrush();
                        brushDeConnect.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDeconnecter, UriKind.Relative));//deconnection
                        btnConnecter.Background = brushDeConnect;
                    }
                    else
                    {
                        var brushconnect = new ImageBrush();
                        brushconnect.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConnecter, UriKind.Relative));//connexion
                        btnConnecter.Background = brushconnect;
                    }


                    grid.Children.Add(lblWifiName);
                    grid.Children.Add(lblWifiSignal);
                    grid.Children.Add(btnConnecter);
                    border.Child = grid;

                    wflist.Children.Add(border);
                    LoadingPanel.ClosePanel();

                }
            })));
        }

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            //Flag = (e.NewStatus == WifiStatus.Connected);
            setListWifi();
        }
        #endregion
        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        private void OnUcLoaded(object sender, RoutedEventArgs e)
        {
           /* dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);*/
            setListWifi();
        }
        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }
        private void Configuration_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToConfiguration.CanExecute(null))
                viewModel.GoToConfiguration.Execute(null);
        }
        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        }
        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter l'application, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
        private List<WifiModel> GetAccessPoints()
        {
            List<WifiModel> listWifi = new List<WifiModel>();
            try
            {
                IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints().OrderByDescending(ap => ap.SignalStrength);
                foreach (AccessPoint ap in accessPoints)
                {
                    listWifi.Add(new WifiModel() { WifiName = ap.Name, SignalStrength = string.Format("{0}%", ap.SignalStrength), IsConnected = string.Format("{0}", ap.IsConnected ? "Oui" : "Non"), ToConnect = !Status(), ToDisconnect = ap.IsConnected });
                }
            }
            catch (Exception)
            {
                return listWifi;
            }

            return listWifi;
        }
        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var wifiName = ((Button)sender).Tag.ToString();
                AccessPoint selectedAP = wifi.GetAccessPoints().FirstOrDefault(x => x.Name == wifiName);
                if (selectedAP.IsConnected)
                {
                    Task.Factory.StartNew(() =>
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            ((Button)sender).IsEnabled = false;
                        }));

                    }).ContinueWith(task =>
                    {
                       // dispatcherTimer.Start();
                        btnDeconnecter_Click(sender, e);
                    });
                }
                else
                {
                    Task.Factory.StartNew(() =>
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            ((Button)sender).IsEnabled = false;
                        }));
                    }).ContinueWith(task =>
                    {
                      //  dispatcherTimer.Start();
                        btnSeConnecter_Click(sender, e);
                        //LoadingPanel.ClosePanel();
                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        //private void btnSeConnecter_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        var wifiName = ((Button)sender).Tag.ToString();
        //        //AlertBox.ReportInfo(wifiName);
        //        //wifi.Disconnect();
        //        //TODO Connecter...
        //        AccessPoint selectedAP = wifi.GetAccessPoints().FirstOrDefault(x => x.Name == wifiName);
        //        // Auth
        //        AuthRequest authRequest = new AuthRequest(selectedAP);
        //        bool overwrite = true;

        //        if (authRequest.IsPasswordRequired)
        //        {
        //            if (selectedAP.HasProfile)
        //            // If there already is a stored profile for the network, we can either use it or overwrite it with a new password.
        //            {
        //                //Console.Write("\r\nA network profile already exist, do you want to use it (y/n)? ");
        //                //if (Console.ReadLine().ToLower() == "y")
        //                {
        //                    overwrite = false;
        //                }
        //            }

        //            if (overwrite)
        //            {
        //                if (authRequest.IsUsernameRequired)
        //                {
        //                    string username = PromptDialog.Prompt("Nom d'utilisateur", "Nom d'utilisateur", inputType: PromptDialog.InputType.Password);
        //                    authRequest.Username = username;
        //                }

        //                //string password = PromptDialog.Prompt("Mot de passe", "Mot de passe", inputType: PromptDialog.InputType.Password);
        //                string password = PromptDialog.Prompt("Mot de passe wifi", wifiName, inputType: PromptDialog.InputType.Password);
        //                authRequest.Password = password; //PasswordPrompt(selectedAP);

        //                if (authRequest.IsDomainSupported)
        //                {
        //                    string domaine = PromptDialog.Prompt("Domaine", "Domaine", inputType: PromptDialog.InputType.Password);
        //                    authRequest.Domain = domaine;
        //                }
        //            }
        //        }
        //        selectedAP.ConnectAsync(authRequest, overwrite, OnConnectedComplete);
        //        //errormsg = selectedAP.ErrorMsg;
        //        //lvWifi.ItemsSource = GetAccessPoints();
        //        setListWifi();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Erreur connection : {ex.Message}", "Erreur Wifi", MessageBoxButton.OK);//, MessageBoxImage.Error);
        //    }
        //}
        
        private void btnSeConnecter_Click(object sender, RoutedEventArgs e)
        {
            string wifiName = string.Empty;

            Task.Factory.StartNew(() =>
            {
                
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    wifiName = ((Button)sender).Tag.ToString();
                    LoadingPanel.ShowPanel("Connexion en cours ...");
                    /* lblInfo.Content = "Connexion en cours...";
                     lblInfo.Visibility = Visibility.Visible;*/
                   
                    ((Button)sender).IsEnabled = true;
                }));
            }).ContinueWith(task =>
            {
                

                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        AccessPoint selectedAP = wifi.GetAccessPoints().FirstOrDefault(x => x.Name == wifiName);
                        while (selectedAP == null)
                        {
                            selectedAP = wifi.GetAccessPoints().FirstOrDefault(x => x.Name == wifiName);
                        }
                        AuthRequest authRequest = new AuthRequest(selectedAP);
                        bool overwrite = true;
                        string password = getWifiPassword(wifiName);
                        if (selectedAP.HasProfile && !string.IsNullOrEmpty(password))
                        // If there already is a stored profile for the network, we can either use it or overwrite it with a new password.
                        {
                            overwrite = false;
                            authRequest.Password = password;
                        }

                        if (overwrite)
                        {
                            if (authRequest.IsPasswordRequired)
                            {

                                try
                                {
                                    this.Dispatcher.BeginInvoke((Action)(() =>
                                    {
                                        PromptDialog prompt = new PromptDialog(wifiName, "Mot de passe wifi", wifiName, "", inputType: PromptDialog.InputType.Password);
                                        setListWifi();
                                    }));


                                  //  dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show($"Erreur connection : {ex.Message}", "Erreur Wifi", MessageBoxButton.OK);//, MessageBoxImage.Error);
                                }
                            }
                        }
                        else
                        {
                            
                            Connect(selectedAP, authRequest, false);
                            errormsg = selectedAP.ErrorMsg;
                           // dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                        }
                        //LoadingPanel.ClosePanel();
                        /*lblInfo.Content = "";
                        lblInfo.Visibility = Visibility.Hidden;*/
                        LoadingPanel.ClosePanel();
                        ((Button)sender).IsEnabled = true;
                        setListWifi();


                    }));
                

            });

        }
        


        public static string Connect(AccessPoint ap, AuthRequest authrequest, bool overwrite)
        {
            string result = ""; 
            int increment = 0;
            while (result.ToLower() != "connecté" && increment < 3)
            {
                increment++;
                result = ap.ConnectAsync1(authrequest, overwrite);
            }


            return result;

        }

        private string getWifiPassword(string profileName)
        {
            try
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = "/c netsh wlan show profile " + profileName + " key =clear";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.Verb = "runas";
                p.Start();
                string s = p.StandardOutput.ReadToEnd();
                //s = s.Replace("\r\n", string.Empty);
                char[] tabChar = new char[1] { '\r' };
                char[] tabChar1 = new char[1] { ':' };
                
                string[] tabString = s.Split(tabChar);
                string passwordRow = tabString.Where(x => x.ToLower().Contains("contenu de la cl")).FirstOrDefault();
                if (string.IsNullOrEmpty(passwordRow))
                {
                    passwordRow = tabString.Where(x => x.ToLower().Contains("key content")).FirstOrDefault();
                    passwordRow = passwordRow.Replace("\n", string.Empty);
                    passwordRow = passwordRow.Replace("\r", string.Empty);

                    passwordRow = passwordRow.Split(tabChar1).Last().Trim();
                    passwordRow = passwordRow.Replace("\r\n", string.Empty);
                }
                else
                {
                    passwordRow = passwordRow.Replace("\n", string.Empty);
                    passwordRow = passwordRow.Replace("\r", string.Empty);

                    passwordRow = passwordRow.Split(tabChar1).Last();
                    passwordRow = passwordRow.Replace("\r\n", string.Empty);
                }
               
                if (!string.IsNullOrEmpty(passwordRow))
                {
                    return passwordRow;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch(Exception e)
            {
                return string.Empty;
            }
        }
        protected void WaitForThreads()
        {
            int maxThreads = 0;
            int placeHolder = 0;
            int availThreads = 0;
            int timeOutSeconds = 10;

            //Now wait until all threads from the Threadpool have returned
            while (timeOutSeconds > 0)
            {
                //figure out what the max worker thread count it
                System.Threading.ThreadPool.GetMaxThreads(out
                                     maxThreads, out placeHolder);
                System.Threading.ThreadPool.GetAvailableThreads(out availThreads,
                                                               out placeHolder);

                //if (availThreads == maxThreads) break;
                // Sleep
                System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(1000));
                --timeOutSeconds;
            }
            // You can add logic here to log timeouts
        }
        private void btnDeconnecter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        LoadingPanel.ShowPanel("Déconnexion ...");
                    }));
                }).ContinueWith(task =>
                {
                    LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        //wifi.Disconnect();
                        LoadingPanel.ClosePanel();
                        //setListWifi();
                        
                    })));
                    wifi.Disconnect();
                    setListWifi();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur déconnection : {ex.Message}", "Erreur Wifi", MessageBoxButton.OK);//, MessageBoxImage.Error);
            }
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
               // LoadingPanel.ClosePanel();
               // lblInfo.Visibility = Visibility.Hidden;
                ((Button)sender).IsEnabled = true;
            }));


        }
        private void OnConnectedComplete(bool success,string errormsg)
        {
            MessageBox.Show(string.Format("\n {0}", success ? " WIFI Connecté " : errormsg), "Information", MessageBoxButton.OK);//, MessageBoxImage.Information);

            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                //lvWifi.ItemsSource = GetAccessPoints();
                setListWifi();
            }));
        }
        public void Sumary_Click(object sender, RoutedEventArgs e)
        {
            
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                }));

            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (Globals._FlagConfig == "client")
                    {
                        var viewModel = (WifiViewModel)DataContext;
                        KillMe();
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                viewModel.GoToClientSumarySpherik.Execute(null);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            if (viewModel.GoToClientSumary.CanExecute(null))
                                viewModel.GoToClientSumary.Execute(null);
                        }
                    }
                    else
                    {
                        KillMe();
                        var viewModel = (WifiViewModel)DataContext;
                        if (viewModel.GoToListEvent.CanExecute(null))
                            viewModel.GoToListEvent.Execute(null);
                    }
                }));
                
            });
            
            
        }
        public void GotoAccueil()
        {
            KillMe();
            var viewModel = (WifiViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }
        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0)
            //{
            //    setTimeout();
            //}
            
            timeout--;
            if (timeout == 0)
            {
                Timer.Text = "";
                imgCercle.Visibility = Visibility.Hidden;
            }
            else
            {
                Timer.Text = timeout.ToString();
                imgCercle.Visibility = Visibility.Visible;
            }

            if (timeout == -1)
            {
                Timer.Text = "";
                if (timerCountdown != null) timerCountdown.Stop();
                if (timerCountdown_minute != null) timerCountdown_minute.Stop();
                GotoAccueil();
            }
        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();
        }
        void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            GotoAccueil();
        }
        private void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            StopOperation();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
