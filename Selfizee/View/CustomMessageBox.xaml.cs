﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for CustomMessageBox.xaml
    /// </summary>
    public partial class CustomMessageBox : Window
    {
        public CustomMessageBox(string message, string title)
        {
            InitializeComponent();
            txtMessage.Content = title;
            Title.Content = message;
            //Title = title;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            closeWindow.Background = brushCroix;
            ImageBrush btnYes = new ImageBrush();
            btnYes.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnyesPopup, UriKind.Absolute));
            btnOk.Background = btnYes;
           

            ImageBrush btnNo = new ImageBrush();
            btnNo.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnnoPopup, UriKind.Absolute));

            btnCancel.Background = btnNo;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        public static bool Prompt(string Message, string Title)
        {
            CustomMessageBox inst = new CustomMessageBox(Message, Title);
            inst.ShowDialog();
            if (inst.DialogResult == true)
                return inst.DialogResult == true;
            return false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void CloseWin(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
