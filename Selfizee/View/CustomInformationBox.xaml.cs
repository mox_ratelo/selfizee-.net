﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for CustomInformationBox.xaml
    /// </summary>
    public partial class CustomInformationBox : Window
    {
        public CustomInformationBox(string message, string title)
        {
            InitializeComponent();
            txtMessage.Content = title;
            Title.Content = message;
            //Title = title;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            closeWindow.Background = brushCroix;
            ImageBrush btnYes = new ImageBrush();
            btnYes.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnOkPopup, UriKind.Absolute));
            btnOk.Background = btnYes;

        }

       

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            //DialogResult = true;
            Close();
        }

        public static bool Prompt(string Message, string Title)
        {
            CustomInformationBox inst = new CustomInformationBox(Message, Title);
            inst.ShowDialog();
            if (inst.DialogResult == true)
                return inst.DialogResult == true;
            return false;
        }

        private void CloseWin(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
