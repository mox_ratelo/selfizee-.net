﻿using AForge.Video.DirectShow;
using Selfizee.Managers;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Printing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for Parametre.xaml
    /// </summary>
    public partial class Parametre : UserControl
    {
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        IniUtility iniWriting = new IniUtility(Globals._appConfigFile);
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        private static Wifi wifi;
        public Parametre()
        {
          //  wifi = new Wifi();
            InitializeComponent();
            setVideoDevices();
           // Flag = Status();
          //  wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;
            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Absolute));
            _back.Background = btnback;
            //Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;
            var brushSave = new ImageBrush();
            brushSave.ImageSource = new BitmapImage(new Uri(Globals._defaultGreenSave, UriKind.Relative));
            btn_save.Background = brushSave;
            int webcam = Convert.ToInt32(iniAppFile.GetSetting("WEBCAM", "activated"));
            idborne.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            idborne.LostFocus += LostTextBoxKeyboardFocus;
            if (webcam == 1)
            {
                WEBCAM.IsChecked = true;
            }else
            {
                DSLR.IsChecked = true;
            }
            int cursor = Convert.ToInt32(iniAppFile.GetSetting("CURSOR", "EnableCursor"));
            if (cursor == 1)
            {
                Curseur.IsChecked = true;
            }
            else
            {
                Curseur.IsChecked = false;
            }
            string borneName = iniAppFile.GetSetting("EVENTSCONFIG", "idborne");
            idborne.Text = borneName;
            getQueue();
        }

        private void getQueue()
        {
            var nb = LocalPrintServer.GetDefaultPrintQueue().NumberOfJobs;            
            if (nb > 0)
            {
                nbPrintInWait.Text = nb.ToString();
                deletePrintList.IsEnabled = true;
            }
            else
            {
                nbPrintInWait.Text = "0";
                deletePrintList.IsEnabled = false;
            }

        }

        

        private void setVideoDevices()
        {
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (VideoDevices.Any())
            {
                camera_txt.Visibility = Visibility.Visible;
                cbCamerasUsb.Visibility = Visibility.Visible;
                foreach (var _videoDevice in VideoDevices)
                    {
                       cbCamerasUsb.Items.Add(_videoDevice.Name);
                    }
                string cameraName = iniAppFile.GetSetting("WEBCAM", "Name");
                int index = 0;
                if (!string.IsNullOrEmpty(cameraName))
                {
                    for (int i = 0; i < cbCamerasUsb.Items.Count; i++)
                    {
                        string cameraselected = cbCamerasUsb.Items[i].ToString();
                        if (cameraselected.ToLower().Equals(cameraName.ToLower())) 
                        {
                            index = i;
                        }
                    }
                }

                cbCamerasUsb.SelectedIndex = index;

            }
            else
            {
                camera_txt.Visibility = Visibility.Collapsed;
                cbCamerasUsb.Visibility = Visibility.Collapsed;
            }
           
        }

        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }
/*
        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(Parametre));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion
        
        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion
        */
        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;
        }

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ParametreViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            string tag = textbox.Tag.ToString();

            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
                numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Visible;
                numeriqueKeyboard.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Visible;
                alphanumeriquekeyboard.ActiveContainer = textbox;
                alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Collapsed;
            }
        }

        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter l'application, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ParametreViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ParametreViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void DeletePrintList_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    LoadingPanel.ShowPanel("Suppression de la file d'attente ...");

                }));

            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    foreach (var pn in InstalledPrinters)
                    {
                        CleanPrinterQueue(pn);
                    }

                    nbPrintInWait.Text = LocalPrintServer.GetDefaultPrintQueue().NumberOfJobs.ToString();
                    deletePrintList.IsEnabled = false;

                    LoadingPanel.ClosePanel();
                    //lbl_loading.Visibility = Visibility.Hidden;
                }));
            });
            
        }

        public List<string> InstalledPrinters
        {
            get
            {
                return (from PrintQueue printer in new LocalPrintServer().GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local,
                EnumeratedPrintQueueTypes.Connections }).ToList()
                        select printer.Name).ToList();
            }
        }

        private void CleanPrinterQueue(string printerName)
        {
            using (var ps = new PrintServer())
            {
                using (var pq = new PrintQueue(ps, printerName, PrintSystemDesiredAccess.UsePrinter))
                {
                    //pq.Purge();
                    foreach (var job in pq.GetPrintJobInfoCollection())
                        job.Cancel();
                }
            }
        }

        private void save(object sender, RoutedEventArgs e)
        {
            if (Curseur.IsChecked == true)
            {
                iniWriting.Write("EnableCursor", "1", "CURSOR");
            }
            else
            {
                iniWriting.Write("EnableCursor", "0", "CURSOR");
            }
            if (WEBCAM.IsChecked == true)
            {
                iniWriting.Write("activated", "1", "WEBCAM");
            }
            else
            {
                iniWriting.Write("activated", "0", "WEBCAM");
            }

            if (!string.IsNullOrEmpty(idborne.Text))
            {
                iniWriting.Write("idborne", idborne.Text, "EVENTSCONFIG");
            }
            if(cbCamerasUsb.Visibility == Visibility.Visible)
            {
                int selectedIndex =cbCamerasUsb.SelectedIndex;
                string cameraselected = cbCamerasUsb.Items[selectedIndex].ToString();
                iniWriting.Write("Name", cameraselected, "WEBCAM");
            }
            MessageBox.Show("Modification effectuée avec succès");
        }

    }
}
