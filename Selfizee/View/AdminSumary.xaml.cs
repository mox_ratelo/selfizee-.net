﻿using log4net;
using Newtonsoft.Json;
using Selfizee.Managers;
using Selfizee.Models;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Printing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for AdminSumary.xaml
    /// </summary>
    public partial class AdminSumary : UserControl
    {

        private string[] imageFiles = new string[0];
        private int selected = 0;
        private int begin = 0;
        private int end = 0;
        private bool strip = false;
        private ImageCollection _photos;
        private string mediaDataPath = "";
        private string toPrint = "";
        string idborne = "";
        string MediaPhotosPath;
        private string photosDirectory = string.Empty;
        private List<ModifyEvent> lstCoordonate = new List<ModifyEvent>();
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        public string code { get; set; }
        private static Wifi wifi;
        public double progress; private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int timeout;
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        public AdminSumary()
        {
            try
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    Globals._FlagConfig = "admin";
                    InitializeComponent();
                    wifi = new Wifi();

                    InitializeComponent();

                    code = Globals.codeEvent_toEdit;//
                    if (string.IsNullOrEmpty(code))
                    {
                        code = Globals.GetEventId();
                    }
                    MediaPhotosPath = Globals._MediaFolder + "\\" + code + "\\Photos";
                    mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
                    INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
                    timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
                    Synchro();

                    idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
                    numBorne.Content = idborne;

                    Bitmap imagePuce = new Bitmap(Globals._defaultBtnPuceGrise);
                    Img_puce1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);
                    Img_puce2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);

                    Bitmap imageWifi = new Bitmap(Globals._defaultBtnIcoWifi);
                    //wifi_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imageWifi);

                    Bitmap imageConnect = new Bitmap(Globals._defaultBtnIcoConnect);
                    Bitmap imageNotConnect = new Bitmap(Globals._defaultbtnVisualisationCroix);

                    //ImageBrush btnAllPhotos = new ImageBrush();
                    //btnAllPhotos.ImageSource = new BitmapImage(new Uri(Globals._defaultallphotos, UriKind.Absolute));
                    //btnPhoto.Background = btnAllPhotos;

                    //ImageBrush btnTimeLine = new ImageBrush();
                    //btnTimeLine.ImageSource = new BitmapImage(new Uri(Globals._defaultalltimelines, UriKind.Absolute));
                    //btn_timelines.Background = btnTimeLine;

                    ImageBrush btndelete = new ImageBrush();
                    btndelete.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDeleteContacts, UriKind.Absolute));
                    delete_contacts.Background = btndelete;

                    idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
                    numBorne.Content = idborne;


                    var thisApp = Assembly.GetExecutingAssembly();
                    AssemblyName name = new AssemblyName(thisApp.FullName);
                    string localVersion = name.Version.ToString();
                    numVersion.Content = localVersion;

                    ImageBrush btnRazPrinted = new ImageBrush();
                    btnRazPrinted.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnResetPrinted, UriKind.Absolute));
                    RAZ_Printed.Background = btnRazPrinted;

                    //ImageBrush btnSetting = new ImageBrush();
                    //btnSetting.ImageSource = new BitmapImage(new Uri(Globals._defaultreglageConfig, UriKind.Absolute));
                    //btnConfig.Background = btnSetting;

                    //ImageBrush btnAssistance = new ImageBrush();
                    //btnAssistance.ImageSource = new BitmapImage(new Uri(Globals._defaultassistance, UriKind.Absolute));
                    //btnAssist.Background = btnAssistance;


                    Bitmap imagePhoto = new Bitmap(Globals._defaultBtnallphotos);
                    photos_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imagePhoto);

                    Bitmap imageSetting = new Bitmap(Globals._defaultBtnareglageConfig);
                    setting_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageSetting);

                    Bitmap imageAssist = new Bitmap(Globals._defaultBtnassistance);
                    assist_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageAssist);

                    Bitmap imagePuceBtn = new Bitmap(Globals._defaultBtnalltimelines);
                    timelines_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
                    //timelines_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
                    //icone_pucebtn3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
                    //icone_pucebtn4.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);

                    Bitmap imagePuceBtn1 = new Bitmap(Globals._defaultBtnIcoPuceBtn);
                    icone_pucebtn1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn1);
                    icone_pucebtn2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn1);
                    icone_pucebtn3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn1);
                    icone_pucebtn4.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn1);


                    Bitmap imagelogoNoir = new Bitmap(Globals._defaultLogoNoir);
                    img_logo.Source = ImageUtility.convertBitmapToBitmapImage(imagelogoNoir);

                    var brushCancel = new ImageBrush();
                    brushCancel.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix, UriKind.Relative));
                    cancelCopie.Background = brushCancel;

                    var brushPrint = new ImageBrush();
                    brushPrint.ImageSource = new BitmapImage(new Uri(Globals._defaultprintconfig, UriKind.Relative));
                    print.Background = brushPrint;

                    var brushNext = new ImageBrush();
                    brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhiteNext, UriKind.Relative));
                    Next.Background = brushNext;

                    var brushPrevious = new ImageBrush();
                    brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhitePrevious, UriKind.Relative));
                    Previous.Background = brushPrevious;

                    _photos = new ImageCollection();
                    //code = Globals.GetEventId();

                    codeEvent.Content = code;

                    string configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
                    IniUtility _iniUtility = new IniUtility(configIniPath);
                    string Name = _iniUtility.Read("Name", "EVENT");
                    EventName.Text = Name;
                    var typeEvent = _iniUtility.Read("typeEvent", "EVENT");
                    if (typeEvent != null)
                    {
                        Globals.typeEvent = typeEvent.ToLower();
                    }
                    string begin = _iniUtility.Read("Begin", "EVENT"); ///iniFile.GetSetting("EVENT", "Begin");
                    string end = _iniUtility.Read("End", "EVENT");
                    dateEventBegin.Content = begin;
                    dateEventEnd.Content = end;

                    int nbphoto = getNbPhotos();
                    if (nbphoto <= 1)
                    {
                        txtPhoto.Text = "PHOTO";
                    }
                    else
                    {
                        txtPhoto.Text = "PHOTOS";
                    }
                    NbPhoto.Text = nbphoto.ToString();

                    int nbprint = getNbPrinted();
                    if (nbprint <= 1)
                    {
                        txtImpression.Text = "IMPRESSION";
                    }
                    else
                    {
                        txtImpression.Text = "IMPRESSIONS";
                    }
                    Nbprint.Text = nbprint.ToString();

                    string pathJson = "C:\\Events\\Media\\" + code + "\\Data\\data.json";
                    if (File.Exists(pathJson))
                    {
                        List<boData> _lst = JsonConvert.DeserializeObject<List<boData>>(File.ReadAllText(pathJson));
                        int nb_coordonate = _lst.Where(c => c.champs != null).Count();
                        if (nb_coordonate == 0 || nb_coordonate == 1)
                        {
                            NbCoordonate.Text = nb_coordonate.ToString();
                            txtCoordonnee.Text = "COORDONNÉE";//not initialized
                        }
                        else if (nb_coordonate > 1)
                        {
                            NbCoordonate.Text = nb_coordonate.ToString();
                            txtCoordonnee.Text = "COORDONNÉES";//not initialized
                        }
                    }
                    else
                    {
                        NbCoordonate.Text = "0";
                        txtCoordonnee.Text = "COORDONNÉE";//not initialized
                    }

                    photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
                    Load3LstImage(photosDirectory);
                    setTimeLine();

                    imgCercle.Visibility = Visibility.Hidden;
                    LaunchTimerAfter2();
                }));
                
            }
            catch(Exception e)
            {
                
            }
            

        }

        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            LaunchTimerAfter2();
        }

        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();

        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }

        private void ReinitializationPrinted(object sender, RoutedEventArgs e)
        {
            //dockGrid.IsEnabled = false;
            //scrollReinit.Visibility = Visibility.Visible;

            MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment réinitialiser le nombre de photos imprimées ? \nEtes-vous sûr ? ", "Réinitialisation du nombre d'impressions", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                ReinitPrinted(sender, e);
            }
        }

        public int getNbPrinted()
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            return mgrBin.readIntData(binFileName);
        }
        public int getNbPhotos()
        {
            string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            if (Directory.Exists(photosDirectory))
            {
                return Directory.GetFiles(photosDirectory).Length;
            }
            return 0;
        }
        private void copieCancel(object sender, EventArgs e)
        {
            closeImageViewer();
        }
        private void closeImageViewer()
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollCopie.Visibility = Visibility.Collapsed;

            //Deselectionner l'image de la ListBox pour pouvoir re-cliquer
            lst3Image.SelectedIndex = -1;
        }
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

        }
        private void ShowPrevImage()
        {
            //ShowImage(this.imageFiles[(--this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(--this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ShowNextImage();
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }

        }
        private void ShowNextImage()
        {
            //ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(++this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private string GetDatePhoto(string pathPhoto)
        {
            DateTime creation = File.GetCreationTime(pathPhoto);
            string day = creation.Day.ToString();
            string month = creation.Month.ToString();
            string year = creation.Year.ToString();
            string hour = creation.Hour.ToString();
            string minutes = creation.Minute.ToString();
            if (day.Length == 1) day = "0" + day;
            if (month.Length == 1) month = "0" + month;
            if (hour.Length == 1) hour = "0" + hour;
            if (minutes.Length == 1) minutes = "0" + minutes;
            return "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
        }
        private void Imprimer(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                lbl_CurrentPrinting.Content = "Impression en cours....";
            }), DispatcherPriority.Send);
            Task.Factory.StartNew(() =>
            {
                //this.Dispatcher.BeginInvoke((Action)(() =>
                //{
                //    lbl_CurrentPrinting.Visibility = Visibility.Visible;
                //}));
                Thread.Sleep(100);

            }).ContinueWith(task =>
            {
                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";

                //lbl_CurrentPrinting.Content = "Impression en cours....";
                int toWrite = mgrBin.readIntData(binFileName);
                toWrite += Convert.ToInt32(1);
                writeTxtFileData(mediaDataPath, toWrite.ToString());
                mgrBin.writeData(toWrite, binFileName);
                Printing(false);
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_CurrentPrinting.Visibility = Visibility.Hidden;
                }));
            });

            //BinaryFileManager mgrBin = new BinaryFileManager();
            //string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            //lbl_CurrentPrinting.Content = "Impression en cours....";
            //int toWrite = mgrBin.readIntData(binFileName);
            //toWrite += Convert.ToInt32(1);
            //writeTxtFileData(mediaDataPath, toWrite.ToString());
            //mgrBin.writeData(toWrite, binFileName);
            //Printing(false);
        }
        private void Printing(bool sendMail)
        {
            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {


                }));

                // System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {

                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    string printerName;
                    //string binPath;

                    //var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    //printerName = inimanager.GetSetting("PRINTING", "printerName");
                    //printerName = printerName.Replace('"', ' ');
                    //printerName = printerName.Replace('\\', ' ');
                    //printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    //if (printerName != "DEFAULT")
                    //    printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            lbl_CurrentPrinting.Visibility = Visibility.Hidden;
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }

                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    closeImageViewer();
                    Thread.Sleep(4000);



                }));

            });


        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }
        private void writeTxtFileData(string filename, string toWrite)
        {
            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }
        private void deletePhoto_click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment supprimer ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";

                removeRowOnCSV(csvPath);
                removePhoto(toPrint);
                Load3LstImage(photosDirectory);

                //IsSynchro();

                /*****Close scrollCopie (ScrollViewer)****/
                copieCancel(sender, e);
            }
        }
        private void removeRowOnCSV(string csvpath)
        {
            if (File.Exists(csvpath))
            {
                string identity = System.IO.Path.GetFileNameWithoutExtension(toPrint);
                List<String> lines = new List<string>();
                string line;

                using (System.IO.StreamReader file = new System.IO.StreamReader(csvpath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }

                lines.RemoveAll(l => l.Contains(identity));
                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(csvpath))
                {
                    outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
                }
            }
        }
        public void removePhoto(string path)
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(path);
        }
        public void Config_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (AdminSumaryViewModel)DataContext;
            Globals.fromAdmin = true;
            KillMe();
            if (viewModel.GoToClientConfig.CanExecute(null))
                viewModel.GoToClientConfig.Execute(null);
        }

        public void timelines_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (AdminSumaryViewModel)DataContext;
            KillMe();
            if (viewModel.GoTotimeLine.CanExecute(null))
                viewModel.GoTotimeLine.Execute(null);
        }

        public void Load3LstImage(string pathDirectory)
        {
            int increment = 0;
            var result = new ObservableCollection<ImageItemViewModel>();
            if (!string.IsNullOrEmpty(pathDirectory))
            {
                getFiles(pathDirectory);
                DirectoryInfo dir = new DirectoryInfo(pathDirectory);
                FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();
                foreach (FileInfo myFile in files)
                {
                    System.Windows.Controls.Image myLocalImage = new System.Windows.Controls.Image();
                    increment++;
                    if(increment > 3)
                    {
                        break;
                    }
                    else
                    {
                        BitmapImage myImageSource = new BitmapImage();
                        myImageSource.BeginInit();
                        myImageSource.CacheOption = BitmapCacheOption.OnLoad;
                        myImageSource.UriSource = new Uri(@"file:///" + myFile.FullName); //new Uri(@"file:///" + myFile.FullName);
                        myImageSource.EndInit();
                        myLocalImage.Source = myImageSource;
                        
                        //int imgWidth = Convert.ToInt32(myImageSource.Width / 11);
                        //int imgHeight = Convert.ToInt32(myImageSource.Height / 11);

                        double x = 510;
                        double y = 190;
                        int imgWidth =0;
                        int imgHeight = 0;
                        int a = Convert.ToInt32(myImageSource.Width);
                        x = x / 3;
                        double rapport = Convert.ToInt32(a / x);
                        if (myImageSource.Width < myImageSource.Height)
                        {
                            a = Convert.ToInt32(myImageSource.Height);
                            rapport = Convert.ToInt32(a / y);
                        }
                        
                        
                        imgWidth = Convert.ToInt32(myImageSource.Width / rapport);
                        imgHeight = Convert.ToInt32(myImageSource.Height / rapport);

                        //result.Add(myLocalImage);
                        result.Add(new ImageItemViewModel { ImageItem = myImageSource, IsChecked = false, ImagePath = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath), ImgWidth=imgWidth,ImgHeight=imgHeight });
                    }
                   
                }
            }
            var newList = Enumerable.Reverse(result).Take(3).Reverse().ToList();
            lst3Image.ItemsSource = newList;
            if(increment == 2)
            {
                last3photos.Text = increment + " dernières photos : ";
            }
            else if (increment == 1)
            {
                last3photos.Text = "Dernière photo : ";
            }
            else if (increment == 0)
            {
                last3photos.Text = " Aucune photo";
            }
        }
        private void getFiles(string photosDirectory)
        {
            if (!Directory.Exists(photosDirectory))
            {
                Directory.CreateDirectory(photosDirectory);
            }
            int index = 0;
            var getFilesDirectory = Directory.GetFiles(photosDirectory);
            DirectoryInfo dir = new DirectoryInfo(photosDirectory);
            FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();

            foreach (FileInfo image in files)
            //foreach (var image in getFilesDirectory)
            {
                index++;
                System.Array.Resize(ref imageFiles, index);
                imageFiles[index - 1] = image.FullName;
            }
            this.selected = 0;
            this.begin = 0;
            this.end = imageFiles.Length;
        }
        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            strip = false;
            var selectedImage = (sender as ListBox).SelectedIndex;
            if (selectedImage >= 0)
            {
                this.Opacity = 1;
                var item = (ImageItemViewModel)lst3Image.Items[selectedImage];

                //var listeImages = (ObservableCollection<ImageItemViewModel>)lstImage.ItemsSource;
                //var item = listeImages.FindIndex(x => x.ImagePath == path);            
                //selected = Array.IndexOf(listeImages, item);
                //selected = lstImage.Select((c, i) => new { value = c, Index = i })
                //                    .Where(x => x.value.ImagePath == item.ImagePath)
                //                    .Select(x => x.Index).FirstOrDefault();
                selected = selectedImage;
                toPrint = item.ImagePath;
                ShowImage(toPrint);
                if (toPrint.ToUpper().Contains("STRIP"))
                {
                    strip = true;
                }
                dockGrid.IsEnabled = false;
                DateTime creation = File.GetCreationTime(toPrint);
                string day = creation.Day.ToString();
                string month = creation.Month.ToString();
                string year = creation.Year.ToString();
                string hour = creation.Hour.ToString();
                string minutes = creation.Minute.ToString();
                if (day.Length == 1) day = "0" + day;
                if (month.Length == 1) month = "0" + month;
                if (hour.Length == 1) hour = "0" + hour;
                if (minutes.Length == 1) minutes = "0" + minutes;
                txtDate.Text = "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
                scrollCopie.Visibility = Visibility.Visible;
            }
        }
        public void ShowImage(string path)
        {

            Bitmap imageBitmap = new Bitmap(path);
            img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
            var actual_Width = img_View.Width;
            var actual_Height = img_View.Height;
            var x = (int)SystemParameters.PrimaryScreenWidth * 50 / 100;
            var y = (int)SystemParameters.PrimaryScreenHeight * 50 / 100;

            if (actual_Width > actual_Height)
            {
                img_View.Height = img_View.Height * x / img_View.Width;
                img_View.Width = x;
            }
            else if (actual_Width < actual_Height)
            {
                img_View.Width = img_View.Width * y / img_View.Height;
                img_View.Height = y;
            }
            else
            {
                img_View.Width = x;
                img_View.Height = x;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
        }
        private void ShowPhotos_Click(object sender, RoutedEventArgs e)
        {
            
            var viewModel = (AdminSumaryViewModel)DataContext;
            KillMe();
            if (viewModel.GoToPhotos.CanExecute(null))
                viewModel.GoToPhotos.Execute(null);
        }

        private void GoToListEvent_Click(object sender, RoutedEventArgs e)
        {
            
            var viewModel = (AdminSumaryViewModel)DataContext;
            KillMe();
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            //code = code.Substring(1, code.Length - 2);
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (AdminSumaryViewModel)DataContext;
            KillMe();
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void ReinitPrinted(object sender, RoutedEventArgs e)
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            mgrBin.writeData(0, binFileName);
            writeTxtFileData(mediaDataPath, "0");
            Nbprint.Text = "0";
        }

        public async Task Synchro()
        {
            //string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            ////string FileUrl = "";
            ////if (chk_con())
            ////{
            ////    WebClient wc = new WebClient();
            ////    wc.DownloadFile(FileUrl, Globals._defaultUpdateDirectory + "\\files_"+code+".txt");
            ////}
            //string TxtFileName = System.IO.Path.GetFileName(FilePath);
            //char[] delimiters = { '_', '.' };
            //string TxtEventName = TxtFileName.Split(delimiters)[1];
            //string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            //List<string> TxtLine = new List<string>();
            //List<string> MediaLine = new List<string>();
            //if (!File.Exists(MediaPath))
            //{
            //    progress = 0;
            //}
            //else
            //{
            //    string line;
            //    using (StreamReader MediaSR = new StreamReader(MediaPath))
            //    {
            //        while ((line = MediaSR.ReadLine()) != null)
            //        {
            //            MediaLine.Add(line);
            //        }
            //    }
            //    //MediaLine.RemoveAt(0);
            //    int totalMedia = MediaLine.Count();
            //    using (StreamReader TxtSR = new StreamReader(FilePath))
            //    {
            //        while ((line = TxtSR.ReadLine()) != null)
            //        {
            //            TxtLine.Add(line);
            //        }
            //    }
            //    int totalTxt = TxtLine.Count();
            //    try
            //    {
            //        if (totalMedia >= totalTxt)
            //        {
            //            int totalTxtMatched = 0;
            //            foreach (var item in MediaLine)
            //            {
            //                string photo = item.Split(',')[1];
            //                if (TxtLine.Contains(photo))
            //                {
            //                    totalTxtMatched = totalTxtMatched + 1;
            //                }
            //                else
            //                {
            //                    break;
            //                }
            //            }
            //            progress = (double)totalTxtMatched / totalMedia * 100;
            //        }
            //        else
            //        {
            //            int totalMediaMatched = 0;
            //            foreach (var item in TxtLine)
            //            {
            //                foreach (var itm in MediaLine)
            //                {
            //                    string photo = itm.Split(',')[1];
            //                    if (item == photo)
            //                    {
            //                        totalMediaMatched = totalMediaMatched + 1;
            //                    }
            //                }
            //            }
            //            progress = (double)totalMediaMatched / totalTxt * 100;
            //        }

            //    }
            //    catch (Exception e)
            //    {

            //    }
            //}
            int valueProgress = (int)IsSynchro(code);
            libProgressBar.Content = (int)valueProgress + "%";
            progressBar1.Value = (int)valueProgress;
        }

        private bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public double IsSynchro(string code)
        {
            string FileUrl = "https://booth.selfizee.fr/transfert/files_" + code + ".txt";
            if (chk_con())
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.DownloadFile(FileUrl, Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt");

                }
                catch (Exception e)
                {

                }
            }
            string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            double progress = 0;
            if (File.Exists(FilePath))
            {
                string TxtFileName = System.IO.Path.GetFileName(FilePath);
                char[] delimiters = { '_', '.' };
                string TxtEventName = TxtFileName.Split(delimiters)[1];
            }
            int nbContentRemoteFile = 0;
            int nbLocalContentFile = 0;
            string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            string MediaPhotosPath = Globals._MediaFolder + "\\" + code + "\\Photos";
            List<string> TxtLine = new List<string>();
            List<string> MediaLine = new List<string>();
            if (!File.Exists(MediaPath))
            {
                progress = 0;
            }
            else
            {
                string line;
                nbLocalContentFile = Directory.GetFiles(MediaPhotosPath).Count();
                if (File.Exists(FilePath))
                {
                    using (StreamReader TxtSR = new StreamReader(FilePath))
                    {
                        nbContentRemoteFile = File.ReadLines(FilePath).Count();
                    }

                    try
                    {
                        if (nbContentRemoteFile == 0)
                        {
                            progress = 0;
                        }
                        else if (nbLocalContentFile <= nbContentRemoteFile)
                        {
                            progress = 100;// (double)nbContentRemoteFile / nbLocalContentFile * 100;
                        }
                        else if (nbLocalContentFile > nbContentRemoteFile)
                        {

                            progress = ((double)nbContentRemoteFile / (double)nbLocalContentFile) * 100;
                        }

                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    progress = 0;
                }

            }
            return progress;
        }

    
        public void GotoAccueil()
        {
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            var viewModel = (AdminSumaryViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        private void setTimeLine()
        {
            string path = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";
            DateTime histoDate = new DateTime();
            string curTime = "";
            if (File.Exists(path))
            {
                DateTime? lastDate = null;
                StreamReader sr = new StreamReader(path);
                String[] rows = Regex.Split(sr.ReadToEnd(), "\r\n");
                sr.Close();
                //for (int i = 1; i < rows.Length; i++)
                if(rows.Length > 0)
                {
                    for (int i = rows.Length - 1; i > 0; i--)
                    {
                        string[] tabText = rows[i].Split(',');

                        if (tabText == null || tabText.Count() < 8)
                            tabText = rows[i].Split(';');

                        if (tabText != null && tabText.Count() >= 8)
                        {
                            string dtPhoto = tabText[7];
                            string dtPrint = tabText[8];
                            string current = "";
                            string currentLib = "";
                            if (String.IsNullOrEmpty(dtPrint))
                            {
                                current = dtPhoto;
                                currentLib = "photo";
                                if (!string.IsNullOrEmpty(current))
                                {
                                    Regex regex = new Regex(@"\s");
                                    string[] splitted = regex.Split(current.ToLower());
                                    curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                    string curDate = splitted[0];

                                    //DateTime histoDate = Convert.ToDateTime(curDate);
                                    //DateTime histoDate = DateTime.Parse(curDate);
                                    if (curDate.Contains('/'))
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null); try
                                        {
                                            histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null);
                                        }
                                        catch (Exception e)
                                        {
                                            histoDate = DateTime.ParseExact(curDate, "dd/MM/yyyy", null);
                                        }
                                    }
                                    else
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "yyyy-MM-dd", null);
                                    }

                                }
                            }
                            else
                            {
                                current = dtPrint;
                                currentLib = "photo + impression";
                                if (!string.IsNullOrEmpty(current))
                                {
                                    Regex regex = new Regex(@"\s");
                                    string[] splitted = regex.Split(current.ToLower());
                                    curTime = splitted[1].Substring(0, 2) + "h" + splitted[1].Substring(3, 2);
                                    string curDate = splitted[0];

                                    //DateTime histoDate = Convert.ToDateTime(curDate);
                                    //DateTime histoDate = DateTime.Parse(curDate);
                                    if (curDate.Contains('/'))
                                    {
                                        try
                                        {
                                            histoDate = DateTime.ParseExact(curDate, "dd/MM/yy", null);
                                        }
                                        catch (Exception e)
                                        {
                                            histoDate = DateTime.ParseExact(curDate, "dd/MM/yyyy", null);
                                        }
                                    }
                                    else
                                    {
                                        histoDate = DateTime.ParseExact(curDate, "yyyy-MM-dd", null);
                                    }

                                }
                            }

                            if (!lastDate.HasValue || lastDate != histoDate)
                            {
                                TextBlock dateHisto = new TextBlock();
                                dateHisto.Text = histoDate.ToLongDateString().UppercaseFirstEach();
                                //dateHisto.Height = 15;
                                dateHisto.FontSize = 18;
                                dateHisto.Margin = new Thickness(35, 39, 0, 10);
                                dateHisto.FontWeight = FontWeights.Bold;
                                dateHisto.Foreground = System.Windows.Media.Brushes.White;
                                timelineStack.Children.Add(dateHisto);

                                lastDate = histoDate;
                            }
                            TextBlock infoHisto = new TextBlock();
                            infoHisto.Text = curTime + " : " + currentLib;
                            //infoHisto.Height = 15;
                            infoHisto.FontSize = 14;

                            infoHisto.Margin = new Thickness(35, 10, 0, 0);
                            infoHisto.FontWeight = FontWeights.Bold;
                            infoHisto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#B9B9B9"));
                            //rowHisto.Width = 250;
                            timelineStack.Children.Add(infoHisto);
                            Separator sep1 = new Separator();
                            sep1.Margin = new Thickness(35, 10, 0, 0);
                            sep1.Width = 400;
                            sep1.HorizontalAlignment = HorizontalAlignment.Left;
                            sep1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#B9B9B9"));
                            timelineStack.Children.Add(sep1);
                        }
                    }
                }
                else
                {
                    TextBlock dateHisto = new TextBlock();
                    dateHisto.Text = "Aucune action";
                    //dateHisto.Height = 15;
                    dateHisto.FontSize = 18;
                    dateHisto.Margin = new Thickness(35, 39, 0, 10);
                    dateHisto.FontWeight = FontWeights.Bold;
                    dateHisto.Foreground = System.Windows.Media.Brushes.White;
                    timelineStack.Children.Add(dateHisto);
                }
                
            }
            else
            {

                TextBlock dateHisto = new TextBlock();
                dateHisto.Text = "Aucune action";
                //dateHisto.Height = 15;
                dateHisto.FontSize = 14;
                dateHisto.Margin = new Thickness(35, 20, 0, 10);
                dateHisto.FontWeight = FontWeights.Bold;
                dateHisto.Foreground = System.Windows.Media.Brushes.White;
                timelineStack.Children.Add(dateHisto);
            }
        }

       
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }


        private void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();

            //  System.GC.Collect();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
