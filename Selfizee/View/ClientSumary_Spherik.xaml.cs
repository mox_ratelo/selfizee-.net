﻿using log4net;
using Newtonsoft.Json;
using Selfizee.Managers;
using Selfizee.Models;
using Selfizee.ViewModels;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Printing;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour CilentSumary_Spherik.xaml
    /// </summary>
    public partial class ClientSumary_Spherik : UserControl
    {
        private string[] imageFiles = new string[0];
        private int selected = 0;
        private int begin = 0;
        private int end = 0;
        private bool strip = false;
        string MediaPhotosPath;
        private ImageCollection _photos;
        private string mediaDataPath = "";
        private string toPrint = "";
        string idborne = "";
        private string photosDirectory = string.Empty;
        private List<ModifyEvent> lstCoordonate = new List<ModifyEvent>();
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        public string code { get; set; }
        private static Wifi wifi;
        public double progress = 0;
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        public int timeout;
        List<DispatcherOperation> LastOperations;
        private void StopOperation()
        {
            foreach (var operation in LastOperations)
            {
                if (operation != null)
                {
                    if (operation.Status == DispatcherOperationStatus.Executing)
                    {
                        operation.Wait();
                    }
                    operation.Abort();
                }
            }
            LastOperations.Clear();
        }

        public ClientSumary_Spherik()
        {
            
            wifi = new Wifi();
            Globals._FlagConfig = "client";
            InitializeComponent();

            LastOperations = new List<DispatcherOperation>();

            /* float lifePerc = System.Windows.Forms.SystemInformation.PowerStatus.BatteryLifePercent * 100;
             libProgressBattery.Content = lifePerc + " %";
             progressbattery.Value = (int)lifePerc;*/
            //CheckPortableValid("0630682013");

            code = Globals.GetEventId();
            MediaPhotosPath = Globals._MediaFolder + "\\" + code + "\\Photos";
            Globals.codeEvent_toEdit = code;
            mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            
            

            idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
            numBorne.Content = idborne;

            var typeEvent = _ini.GetSetting("EVENT", "typeEvent");
            if (typeEvent != null)
            {
                Globals.typeEvent = typeEvent.ToLower();
            }

            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            string localVersion = name.Version.ToString();
            numVersion.Content = localVersion;

            Bitmap imagePuce = new Bitmap(Globals._defaultBtnPuceGrise);
            Img_puce1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);
            Img_puce2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuce);

            Bitmap imageWifi = new Bitmap(Globals._defaultBtnIcoWifi);
            wifi_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imageWifi);

            Bitmap imageConnect = new Bitmap(Globals._defaultBtnIcoConnect);
            Bitmap imageNotConnect = new Bitmap(Globals._defaultbtnVisualisationCroix);

            

            if (wifi.ConnectionStatus == WifiStatus.Connected)
            {
                LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (chk_con())
                    {
                        Synchro();
                        img_connect.Source = ImageUtility.convertBitmapToBitmapImage(imageConnect);
                        txt_connect.Content = "Connecté";
                        setWifiDetails();
                        if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                        {
                            acces.Content = "oui";
                        }
                        else
                        {
                            acces.Content = "non";
                        }
                    }
                    else
                    {
                        img_connect.Source = ImageUtility.convertBitmapToBitmapImage(imageNotConnect);
                        txt_connect.Content = "Non connecté à internet";
                        connexion_details.Visibility = Visibility.Hidden;
                    }
                }), DispatcherPriority.Background));
                

            }
            else
            {
                img_connect.Source = ImageUtility.convertBitmapToBitmapImage(imageNotConnect);
                txt_connect.Content = "Non connecté à internet";
                connexion_details.Visibility = Visibility.Hidden;
            }

            Bitmap imagePhoto = new Bitmap(Globals._defaultBtnIcoPhoto);
            photo_Icone.Source = ImageUtility.convertBitmapToBitmapImage(imagePhoto);

            Bitmap imageSetting = new Bitmap(Globals._defaultBtnIcoSetting);
            setting_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageSetting);

            Bitmap imageAssist = new Bitmap(Globals._defaultBtnIcoAssist);
            assist_icone.Source = ImageUtility.convertBitmapToBitmapImage(imageAssist);

            //var brushWifi = new ImageBrush();
            //brushWifi.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWifiList, UriKind.Relative));
            //btnWifi.Background = brushWifi;

            //var brushAssist = new ImageBrush();
            //brushAssist.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAssist, UriKind.Relative));
            //btnAssist.Background = brushAssist;

            //var brushRetour = new ImageBrush();
            //brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAnim, UriKind.Relative));
            //btnRetour.Background = brushRetour;

            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);

            //var brushPhoto = new ImageBrush();
            //brushPhoto.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnPhoto, UriKind.Relative));
            //btnPhoto.Background = brushPhoto;

            Bitmap imagePuceBtn = new Bitmap(Globals._defaultBtnIcoPuceBtn);
            icone_pucebtn1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            icone_pucebtn4.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);


            //var brushConfig = new ImageBrush();
            //brushConfig.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConfig, UriKind.Relative));
            //btnConfig.Background = brushConfig;

            var brushAdmin = new ImageBrush();
            brushAdmin.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAdmin, UriKind.Relative));
            btnAdmin.Background = brushAdmin;

            Bitmap imagelogoNoir = new Bitmap(Globals._defaultLogoNoir);
            img_logo.Source = ImageUtility.convertBitmapToBitmapImage(imagelogoNoir);

            var brushCancel = new ImageBrush();
            brushCancel.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix, UriKind.Relative));
            cancelCopie.Background = brushCancel;

            var brushPrint = new ImageBrush();
            brushPrint.ImageSource = new BitmapImage(new Uri(Globals._defautlbtnprintrose, UriKind.Relative));
            print1.Background = brushPrint;

            var brushNext = new ImageBrush();
            brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhiteNext, UriKind.Relative));
            Next.Background = brushNext;

            var brushPrevious = new ImageBrush();
            brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhitePrevious, UriKind.Relative));
            Previous.Background = brushPrevious;

            _photos = new ImageCollection();
            //code = Globals.GetEventId();
            //codeEvent.Content = code;

            string configIniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            IniUtility _iniUtility = new IniUtility(configIniPath);
            string Name = _iniUtility.Read("Name", "EVENT");
            EventName.Text = Name;
            string CodeEvent = _iniUtility.Read("code", "EVENT");
            codeEvent.Content = CodeEvent;
            //iniFile.GetSetting("EVENT", "code");
            string begin = _iniUtility.Read("Begin", "EVENT"); ///iniFile.GetSetting("EVENT", "Begin");
            string end = _iniUtility.Read("End", "EVENT");
            dateEvent.Content = "du " + begin + " au " + end;


            int nbphoto = getNbPhotos();
            if (nbphoto <= 1)
            {
                txtPhoto.Text = "PHOTO";
            }
            else
            {
                txtPhoto.Text = "PHOTOS";
            }
            NbPhoto.Text = nbphoto.ToString();

            int nbprint = getNbPrinted();
            if (nbprint <= 1)
            {
                txtImpression.Text = "IMPRESSION";
            }
            else
            {
                txtImpression.Text = "IMPRESSIONS";
            }
            Nbprint.Text = nbprint.ToString();

            //NbCoordonate.Text = "0";
            //txtCoordonnee.Text = "COORDONNÉE";//not initialized
            string pathJson = "C:\\Events\\Media\\" + code + "\\Data\\data.json";
            if (File.Exists(pathJson))
            {
                List<boData> _lst = JsonConvert.DeserializeObject<List<boData>>(File.ReadAllText(pathJson));
                int nb_coordonate = _lst.Where(c => c.champs != null).Count();
                if (nb_coordonate == 0 || nb_coordonate == 1)
                {
                    NbCoordonate.Text = nb_coordonate.ToString();
                    txtCoordonnee.Text = "COORDONNÉE";//not initialized
                }
                else if (nb_coordonate > 1)
                {
                    NbCoordonate.Text = nb_coordonate.ToString();
                    txtCoordonnee.Text = "COORDONNÉES";//not initialized
                }
            }
            else
            {
                NbCoordonate.Text = "0";
                txtCoordonnee.Text = "COORDONNÉE";//not initialized
            }


            photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            ResizeGrid();
            Load3LstImage(photosDirectory);
            imgCercle.Visibility = Visibility.Hidden;
            LaunchTimerAfter2();

            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingPanel.ClosePanel();
            }));
        }

        public void ResizeGrid()
        {
            int x = ((int)SystemParameters.PrimaryScreenWidth - 90) /3;
            int y = (int)SystemParameters.PrimaryScreenHeight -260;
            
            firstGrid.Width = x;
            firstGrid.Height = y;
            secondGrid.Width = x;
            secondGrid.Height = y;
            ThirdGrid.Width = x;
            FourthGrid.Width = x ;
            FourthGrid.Height = y - 225;
            lst3Image.Width = x - 40;
        }
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if(timerCountdown_minute != null) timerCountdown_minute.Stop();
            if(timerCountdown != null) timerCountdown.Stop();
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            LaunchTimerAfter2();
        }
        public int getNbPrinted()
        {
            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + code + ".bin";
            return mgrBin.readIntData(binFileName);
        }
        public int getNbPhotos()
        {
            string photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            if (Directory.Exists(photosDirectory))
            {
                return Directory.GetFiles(photosDirectory).Length;
            }
            return 0;
        }
        private void copieCancel(object sender, EventArgs e)
        {
            closeImageViewer();
        }
        private void closeImageViewer()
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            scrollCopie.Visibility = Visibility.Collapsed;
            

            //Deselectionner l'image de la ListBox pour pouvoir re-cliquer
            lst3Image.SelectedIndex = -1;
        }
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

        }
        private void ShowPrevImage()
        {
            //ShowImage(this.imageFiles[(--this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(--this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ShowNextImage();
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }

        }
        private void ShowNextImage()
        {
            //ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(++this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private string GetDatePhoto(string pathPhoto)
        {
            DateTime creation = File.GetCreationTime(pathPhoto);
            string day = creation.Day.ToString();
            string month = creation.Month.ToString();
            string year = creation.Year.ToString();
            string hour = creation.Hour.ToString();
            string minutes = creation.Minute.ToString();
            if (day.Length == 1) day = "0" + day;
            if (month.Length == 1) month = "0" + month;
            if (hour.Length == 1) hour = "0" + hour;
            if (minutes.Length == 1) minutes = "0" + minutes;
            return "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
        }
        private void Imprimer(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_CurrentPrinting.Visibility = Visibility.Visible;
                }));

            }).ContinueWith(task =>
            {
                BinaryFileManager mgrBin = new BinaryFileManager();
                string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";

                //lbl_CurrentPrinting.Content = "Impression en cours....";
                int toWrite = mgrBin.readIntData(binFileName);
                toWrite += Convert.ToInt32(1);
                writeTxtFileData(mediaDataPath, toWrite.ToString());
                mgrBin.writeData(toWrite, binFileName);
                Printing(false);
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    lbl_CurrentPrinting.Visibility = Visibility.Hidden;
                }));
            });
            

            
        }
        private void Printing(bool sendMail)
        {

            LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
            {
                string printerName;
                //string binPath;

                //var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                //printerName = inimanager.GetSetting("PRINTING", "printerName");
                //printerName = printerName.Replace('"', ' ');
                //printerName = printerName.Replace('\\', ' ');
                //printerName = printerName.TrimEnd().TrimStart();

                IntPtr hDevMode = IntPtr.Zero;
                IntPtr pDevMode = IntPtr.Zero;

                PrintDocument pd = new PrintDocument();

                System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                //if (printerName != "DEFAULT")
                //    printerSettings.PrinterName = printerName;

                printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                printerSettings.Copies = 1;
                printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                pd.DefaultPageSettings.PaperSize.RawKind = 119;
                pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                pd.OriginAtMargins = false;
                pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                try
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        if (strip)
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrintPage += PrintPage;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                        }
                        else
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrintPage += PrintPage;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                        }

                    }));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                }
                closeImageViewer();
                Thread.Sleep(4000);



            })));


        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }
        private void writeTxtFileData(string filename, string toWrite)
        {
            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }
        private void deletePhoto_click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Voulez-vous vraiment supprimer ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                string csvPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\data.csv";

                removeRowOnCSV(csvPath);
                removePhoto(toPrint);
                Load3LstImage(photosDirectory);

                //IsSynchro();

                /*****Close scrollCopie (ScrollViewer)****/
                copieCancel(sender, e);
            }
        }
        private void removeRowOnCSV(string csvpath)
        {
            if (File.Exists(csvpath))
            {
                string identity = System.IO.Path.GetFileNameWithoutExtension(toPrint);
                List<String> lines = new List<string>();
                string line;

                using (System.IO.StreamReader file = new System.IO.StreamReader(csvpath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }

                lines.RemoveAll(l => l.Contains(identity));
                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(csvpath))
                {
                    outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
                }
            }
        }
        public void removePhoto(string path)
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(path);
        }
        public void Config_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;
            Globals.fromAdmin = false;
            if (viewModel.GoToClientConfig.CanExecute(null))
                viewModel.GoToClientConfig.Execute(null);
        }
        public void Wifi_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }
        public void Load3LstImage(string pathDirectory)
        {
            int increment = 0;
            var result = new ObservableCollection<ImageItemViewModel>();
            if (!string.IsNullOrEmpty(pathDirectory))
            {
                getFiles(pathDirectory);
                DirectoryInfo dir = new DirectoryInfo(pathDirectory);
                FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();
                foreach (FileInfo myFile in files)
                {
                    System.Windows.Controls.Image myLocalImage = new System.Windows.Controls.Image();
                    increment++;
                    if (increment > 3)
                    {
                        break;
                    }
                    else
                    {
                        BitmapImage myImageSource = new BitmapImage();
                        myImageSource.BeginInit();
                        myImageSource.CacheOption = BitmapCacheOption.OnLoad;
                        myImageSource.UriSource = new Uri(@"file:///" + myFile.FullName); //new Uri(@"file:///" + myFile.FullName);
                        myImageSource.EndInit();
                        myLocalImage.Source = myImageSource;

                        int xWidth = (int)SystemParameters.PrimaryScreenWidth - 90;
                        double x = (xWidth / 3) - 60;
                        double y = (int)SystemParameters.PrimaryScreenHeight - 600;
                        int imgWidth = 0;
                        int imgHeight = 0;
                        int a = Convert.ToInt32(myImageSource.Width);
                        x = x / 3;
                        double rapport = Convert.ToInt32(a / x);
                                            
                        imgWidth = Convert.ToInt32(myImageSource.Width / rapport);
                        imgHeight = Convert.ToInt32(myImageSource.Height / rapport);

                        if (y < imgHeight)
                        {
                            a = Convert.ToInt32(myImageSource.Height);
                            rapport = Convert.ToInt32(a / y);
                            imgWidth = Convert.ToInt32(myImageSource.Width / rapport);
                            imgHeight = Convert.ToInt32(myImageSource.Height / rapport);
                        }

                        //result.Add(myLocalImage);
                        result.Add(new ImageItemViewModel { ImageItem = myImageSource, IsChecked = false, ImagePath = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath), ImgWidth = imgWidth, ImgHeight = imgHeight });
                    }

                }
            }
            var newList = Enumerable.Reverse(result).Take(3).Reverse().ToList();
            lst3Image.ItemsSource = newList;
            if (increment == 2)
            {
                last3photos.Text = increment + " dernières photos : ";
            }
            else if (increment == 1)
            {
                last3photos.Text = "Dernière photo : ";
            }
            else if (increment == 0)
            {
                last3photos.Text = " Aucune photo";
            }
        }
        private void getFiles(string photosDirectory)
        {
            if (!Directory.Exists(photosDirectory))
            {
                Directory.CreateDirectory(photosDirectory);
            }
            int index = 0;
            var getFilesDirectory = Directory.GetFiles(photosDirectory);
            DirectoryInfo dir = new DirectoryInfo(photosDirectory);
            FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();

            foreach (FileInfo image in files)
            {
                index++;
                System.Array.Resize(ref imageFiles, index);
                imageFiles[index - 1] = image.FullName;
            }
            this.selected = 0;
            this.begin = 0;
            this.end = imageFiles.Length;
        }
        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            strip = false;
            var selectedImage = (sender as ListBox).SelectedIndex;
            //lbl_CurrentPrinting.Visibility = Visibility.Hidden;
            if (selectedImage >= 0)
            {
                
                this.Opacity = 1;
                var item = (ImageItemViewModel)lst3Image.Items[selectedImage];
                selected = selectedImage;
                toPrint = item.ImagePath;
                if (toPrint.ToUpper().Contains("STRIP"))
                {
                    strip = true;
                }
                ShowImage(toPrint);
                dockGrid.IsEnabled = false;
                DateTime creation = File.GetCreationTime(toPrint);
                string day = creation.Day.ToString();
                string month = creation.Month.ToString();
                string year = creation.Year.ToString();
                string hour = creation.Hour.ToString();
                string minutes = creation.Minute.ToString();
                if (day.Length == 1) day = "0" + day;
                if (month.Length == 1) month = "0" + month;
                if (hour.Length == 1) hour = "0" + hour;
                if (minutes.Length == 1) minutes = "0" + minutes;
                txtDate.Text = "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
                scrollCopie.Visibility = Visibility.Visible;
            }
        }
        public void ShowImage(string path)
        {

            Bitmap imageBitmap = new Bitmap(path);
            img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
            var actual_Width = img_View.Width;
            var actual_Height = img_View.Height;
            var x = (int)SystemParameters.PrimaryScreenWidth * 50 / 100;
            var y = (int)SystemParameters.PrimaryScreenHeight * 50 / 100;

            if (actual_Width > actual_Height)
            {
                img_View.Height = img_View.Height * x / img_View.Width;
                img_View.Width = x;
            }
            else if (actual_Width < actual_Height)
            {
                img_View.Width = img_View.Width * y / img_View.Height;
                img_View.Height = y;
            }
            else
            {
                img_View.Width = x;
                img_View.Height = x;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
        }
        private void ShowPhotos_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;
            if (viewModel.GoToClientPhotosList.CanExecute(null))
                viewModel.GoToClientPhotosList.Execute(null);
        }
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        private bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public double IsSynchro(string code)
        {
            string FileUrl = "https://booth.selfizee.fr/transfert/files_" + code + ".txt";
            if (chk_con())
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.DownloadFile(FileUrl, Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt");

                }
                catch (Exception e)
                {

                }
            }
            string FilePath = Globals._MediaFolder + "\\" + code + "\\Data\\files_" + code + ".txt";
            double progress = 0;
            if (File.Exists(FilePath))
            {
                string TxtFileName = System.IO.Path.GetFileName(FilePath);
                char[] delimiters = { '_', '.' };
                string TxtEventName = TxtFileName.Split(delimiters)[1];
            }
            int nbContentRemoteFile = 0;
            int nbLocalContentFile = 0;
            string MediaPath = Globals._MediaFolder + "\\" + code + "\\Data\\data.csv";
            string MediaPhotosPath = Globals._MediaFolder + "\\" + code + "\\Photos";
            List<string> TxtLine = new List<string>();
            List<string> MediaLine = new List<string>();
            if (!File.Exists(MediaPath))
            {
                progress = 0;
            }
            else
            {
                string line;
                nbLocalContentFile = Directory.GetFiles(MediaPhotosPath).Count();
                if (File.Exists(FilePath))
                {
                    using (StreamReader TxtSR = new StreamReader(FilePath))
                    {
                        nbContentRemoteFile = File.ReadLines(FilePath).Count();
                    }

                    try
                    {
                        if (nbContentRemoteFile == 0)
                        {
                            progress = 0;
                        }
                        else if (nbLocalContentFile <= nbContentRemoteFile)
                        {
                            progress = 100;// (double)nbContentRemoteFile / nbLocalContentFile * 100;
                        }
                        else if (nbLocalContentFile > nbContentRemoteFile)
                        {

                            progress = ((double)nbContentRemoteFile / (double)nbLocalContentFile) * 100;
                        }

                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    progress = 0;
                }

            }
            return progress;
        }


        public void Synchro()
        {
            LastOperations.Add(this.Dispatcher.BeginInvoke((Action)(() =>
             {
                 int valueProgress = (int)IsSynchro(code);
                 libProgressBar.Content = (int)valueProgress + "%";
                 progressBar1.Value = (int)valueProgress;
             })));
            
        }
        public void GotoAccueil()
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }
        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();

        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }
        public void Admin_Click(object sender, RoutedEventArgs e)
        {
            KillMe();
            var viewModel = (ClientSumarySpherikViewModel)DataContext;

            if (viewModel.GoToConnexionConfig.CanExecute(null))
                viewModel.GoToConnexionConfig.Execute(null);
        }
        public void setWifiDetails()
        {
            List<WifiModel> wifilst = GetAccessPoints();
            foreach (WifiModel wf in wifilst)
            {
                if (wf.IsConnected == "Oui")
                {
                    reseau.Content = wf.WifiName;
                    int wifietat = int.Parse(wf.SignalStrength.Split('%')[0]);
                    if (wifietat > 80)
                    {
                        etat.Content = "Fort";
                    }
                    else if (wifietat < 80 && wifietat > 50)
                    {
                        etat.Content = "Moyen";
                    }
                    else
                    {
                        etat.Content = "Faible";
                    }
                }
            }

            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                acces.Content = "Oui";
            }
        }
        private List<WifiModel> GetAccessPoints()
        {
            List<WifiModel> listWifi = new List<WifiModel>();
            try
            {
                IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints().OrderByDescending(ap => ap.SignalStrength);
                foreach (AccessPoint ap in accessPoints)
                {
                    listWifi.Add(new WifiModel() { WifiName = ap.Name, SignalStrength = string.Format("{0}%", ap.SignalStrength), IsConnected = string.Format("{0}", ap.IsConnected ? "Oui" : "Non"), ToConnect = !Status(), ToDisconnect = ap.IsConnected });
                }
            }
            catch (Exception)
            {
                return listWifi;
            }

            return listWifi;
        }


        private void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            StopOperation();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }
    }
}
