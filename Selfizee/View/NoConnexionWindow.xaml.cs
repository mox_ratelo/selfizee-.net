﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour NoConnexionWindow.xaml
    /// </summary>
    public partial class NoConnexionWindow : Window
    {
        public NoConnexionWindow()
        {
            InitializeComponent();

            Bitmap updateBitmap = new Bitmap(Globals._defaultImageNoConnex);
            Img_noconnex.Source = ImageUtility.convertBitmapToBitmapImage(updateBitmap);
            
        }
        public void BtnRetour_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
