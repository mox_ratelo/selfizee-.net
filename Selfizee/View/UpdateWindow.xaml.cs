﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        public string ExeLink { get; set; }
        public string RemoteVersion { get; set; }
        public UpdateWindow()
        {
            InitializeComponent();
            string jsonString = "";
            string path = "C:\\EVENTS\\Events\\AppConfig.ini";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);
            string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");
            //download and save json
            //if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            //{
            //    try
            //    {
            //        //string json = new WebClient().DownloadString("https://managerdev.selfizee.fr/api/get-list-version-logiciel.json");
            //        string json = new WebClient().DownloadString("https://booth.selfizee.fr/version-logiciels/getListVersionLogicielByBorne/" + idborne + ".json");
            //        Write(json);
            //        jsonString = Read();
            //    }
            //    catch (Exception e)
            //    {

            //    }
            //}
            //else
            //{
            //    jsonString = Read();
            //}
            jsonString = Read();
            //get json content
            var records = JsonConvert.DeserializeObject<List<RootObject>>(jsonString);
            RootObject root = records.First();

            ExeLink = root.url_file_exe;
            RemoteVersion = root.numero_version;

            Title.Content = root.titre.ToUpper();
            description.Text = root.message;

            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            //download, save, and get image
            string dir = "C:\\Events\\Assets\\Default\\Images\\img_couv.jpg";
            string url = root.url_photo_couverture;
            //if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            //{
            //    using (WebClient client = new WebClient())
            //    {
            //        client.DownloadFile(new Uri(url), dir);
            //    }
            //}
            Bitmap updateBitmap = new Bitmap(Globals._defaultImageCouv);
            CroppedBitmap cb = new CroppedBitmap(ToSource.ToBitmapSource(updateBitmap), new Int32Rect(0, 0, updateBitmap.Width, (updateBitmap.Height * 75) / 100));
            Img_update.Source = cb;// ImageUtility.convertBitmapToBitmapImage(updateBitmap);

            //version earlier->update
            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            string localVersion = name.Version.ToString();
            string version = localVersion + "->" + root.numero_version;
            Version.Content = version;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix2, UriKind.Relative));
            closeWindow.Background = brushCroix;
        }

        #region FlagDownload
        public bool FlagDownload { get; set; }
        #endregion

        public void Download_Click(object sender, RoutedEventArgs e)
        {
            FlagDownload = true;
            this.Close();
        }

        public void NotDownload_Click(object sender, RoutedEventArgs e)
        {
            FlagDownload = false;
            this.Close();
        }

        private void CloseWin(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public void BtnDetails_Click(object sender, RoutedEventArgs e)
        {
            HistoUpdateWindow histo = new HistoUpdateWindow();
            histo.ShowDialog();
        }

        public void Write(string input)
        {
            string path = @"C:\\Events\\JsonText.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            if (File.Exists(path))
            {
                File.Delete(path);
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(input);
                }
            }
        }
        public string Read()
        {
            string outputs = "";
            string path = @"C:\\Events\\JsonText.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    outputs = s;
                }
            }
            return outputs;
        }
        //public class RootObject
        //{
        //    public int id { get; set; }
        //    public string date_version { get; set; }
        //    public string numero_version { get; set; }
        //    public string description_courte { get; set; }
        //    public string file_exe { get; set; }
        //    public string etat { get; set; }
        //    public string titre { get; set; }
        //    public string message { get; set; }
        //    public string photo_couverture { get; set; }
        //    public string created { get; set; }
        //    public string modified { get; set; }
        //    public int type_maj_id { get; set; }
        //    public string url_photo_couverture { get; set; }
        //    public string url_file_exe { get; set; }
        //}
        public class BorneInfos
        {
            public int id { get; set; }
            public string numero_borne { get; set; }
            public string created { get; set; }
            public string modified { get; set; }
        }

        public class VersionLogicielHasBorneInfos
        {
            public int id { get; set; }
            public int version_logiciel_id { get; set; }
            public int borne_info_id { get; set; }
        }

        public class MatchingData
        {
            public BorneInfos BorneInfos { get; set; }
            public VersionLogicielHasBorneInfos VersionLogicielHasBorneInfos { get; set; }
        }

        public class RootObject
        {
            public int id { get; set; }
            public string date_version { get; set; }
            public string numero_version { get; set; }
            public string description_courte { get; set; }
            public string file_exe { get; set; }
            public string etat { get; set; }
            public string titre { get; set; }
            public string message { get; set; }
            public string photo_couverture { get; set; }
            public string created { get; set; }
            public string modified { get; set; }
            public int type_maj_id { get; set; }
            public bool is_limited { get; set; }
            public bool is_limited_client { get; set; }
            public MatchingData _matchingData { get; set; }
            public string url_photo_couverture { get; set; }
            public string url_file_exe { get; set; }
        }
    }
    public static class ToSource
    {
        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public static System.Windows.Media.Imaging.BitmapSource ToBitmapSource(this System.Drawing.Bitmap source)
        {
            var hBitmap = source.GetHbitmap();
            var result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(hBitmap);

            return result;
        }
    }

}
