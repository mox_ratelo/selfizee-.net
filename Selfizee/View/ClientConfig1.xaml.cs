﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ClientConfig.xaml
    /// </summary>
    public partial class ClientConfig : UserControl
    {
        public ClientConfig()
        {
            InitializeComponent();

            string bidon;

            Bitmap imageChoix = new Bitmap("C:\\EVENTS\\Media\\Photos\\Coast.jpg");
            choiximg1.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);
            choiximg2.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);
            choiximg3.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);


            Bitmap imagePuceBtn = new Bitmap(Globals._defaultBtnIcoPuceBtn);
            puce1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            puce2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            puce3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);

            Bitmap imageFrame = new Bitmap("C:\\EVENTS\\Media\\Photos\\Coast.jpg");
            Frame.Source = ImageUtility.convertBitmapToBitmapImage(imageFrame);

            Bitmap imagelogoNoir = new Bitmap(Globals._defaultLogoNoir);
            img_logo.Source = ImageUtility.convertBitmapToBitmapImage(imagelogoNoir);

        }
    }
}
