﻿using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour PromptDialog.xaml
    /// </summary>
    public partial class PromptDialog : Window
    {
        //public PromptDialog()
        //{
        //    InitializeComponent();
        //}

        public enum InputType
        {
            Text,
            Password
        }

        private InputType _inputType = InputType.Text;
        public static string response;
        public static AccessPoint apoint;
        public static AuthRequest authRequest;
        public static bool overWrite;
        private static Wifi wifi;


        public PromptDialog(string question, string title, string defaultValue = "", InputType inputType = InputType.Text)
        {
            InitializeComponent();


            //txtPasswordResponse.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            //txtPasswordResponse.LostFocus += LostTextBoxKeyboardFocus;

            this.Loaded += new RoutedEventHandler(PromptDialog_Loaded);
            txtQuestion.Content = question;
            Title.Content = title;
            //Title = title;
            txtResponse.Text = defaultValue;
            _inputType = inputType;
            if (_inputType == InputType.Password)
                txtResponse.Visibility = Visibility.Collapsed;
            else
                txtPasswordResponse.Visibility = Visibility.Collapsed;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            closeWindow.Background = brushCroix;

            //var brushSave = new ImageBrush();
            //brushSave.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnsave, UriKind.Relative));
            //btnOk.Background = brushSave;

            //var brushCancel = new ImageBrush();
            //brushCancel.ImageSource = new BitmapImage(new Uri(Globals._defaultcanceldelete, UriKind.Relative));
            //btnCancel.Background = brushCancel;
        }

        
        public PromptDialog(string wifiName, string question, string title, string defaultValue = "", InputType inputType = InputType.Text)
        {
            InitializeComponent();
            wifi = new Wifi();
            apoint = wifi.GetAccessPoints().FirstOrDefault(x => x.Name == wifiName);
            authRequest = new AuthRequest(apoint);
            
            bool overwrite = true;

            if (authRequest.IsPasswordRequired)
            {
                if (apoint.HasProfile)
                // If there already is a stored profile for the network, we can either use it or overwrite it with a new password.
                {
                    overwrite = false;
                }

                //if (overwrite)
                //{
                    PromptDialog inst = new PromptDialog(question, title, defaultValue, inputType);
                    inst.ShowDialog();

                    //if (authRequest.IsUsernameRequired)
                    //{
                    //    PromptDialog.Prompt(authRequest, apoint, overwrite, "Nom d'utilisateur", "Nom d'utilisateur", inputType: PromptDialog.InputType.Password);
                    //}

                    //PromptDialog.Prompt(authRequest, apoint, overwrite, "Mot de passe wifi", wifiName, inputType: PromptDialog.InputType.Password);

                    //if (authRequest.IsDomainSupported)
                    //{
                    //    PromptDialog.Prompt(authRequest, apoint, overwrite, "Domaine", "Domaine", inputType: PromptDialog.InputType.Password);
                    //}
                //}
                //else
                //{
                //    this.Dispatcher.BeginInvoke((Action)(() =>
                //    {
                //        lblConnectInfo.Content = "Connexion en cours...";
                //        lblConnectInfo.Visibility = Visibility.Visible;

                //    }));
                //    Connect(apoint, authRequest, false);
                //    this.Dispatcher.BeginInvoke((Action)(() =>
                //    {
                        
                //        lblConnectInfo.Visibility = Visibility.Hidden;

                //    }));
                //    Close();
                //}
            }
        }


        //public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        //{
        //    var passwordbox = sender as PasswordBox;
        //    string tag = passwordbox.Tag.ToString();

        //    if (tag == "NUMERIQUE")
        //    {
        //        alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
        //        numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
        //        numeriqueKeyboard.Visibility = Visibility.Visible;
        //        numeriqueKeyboard.ActiveContainer = passwordbox;
        //    }

        //    if (tag == "STRING")
        //    {
        //        alphanumeriquekeyboard.Visibility = Visibility.Visible;
        //        alphanumeriquekeyboard.ActiveContainer = passwordbox;
        //        alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
        //        numeriqueKeyboard.Visibility = Visibility.Collapsed;
        //    }
        //}

        //public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        //{
        //    alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
        //    numeriqueKeyboard.Visibility = Visibility.Collapsed;
        //}

        void PromptDialog_Loaded(object sender, RoutedEventArgs e)
        {
            if (_inputType == InputType.Password)
                txtPasswordResponse.Focus();
            else
                txtResponse.Focus();
        }

        //public static string Prompt(string question, string title, string defaultValue = "", InputType inputType = InputType.Text)
        //{
        //    PromptDialog inst = new PromptDialog(question, title, defaultValue, inputType);
        //    inst.ShowDialog();
        //    if (inst.DialogResult == true)
        //        return inst.ResponseText;
        //    return null;
        //}
        

        public static string Connect(AccessPoint ap, AuthRequest authrequest, bool overwrite)
        {
            string result = ""; ;
            int increment = 0;
            while (result.ToLower() != "connecté" && increment <3)
            {
                increment++;
                result = ap.ConnectAsync1(authrequest, overwrite);
            }
           
            
            return result;

        }
        public string ResponseText
        {
            get
            {
                if (_inputType == InputType.Password)
                    return txtPasswordResponse.Password;
                else
                    return txtResponse.Text;
            }
        }
       
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel("Connexion en cours...");
                    //lblConnectInfo.Content = "Connexion en cours...";
                    //lblConnectInfo.Visibility = Visibility.Visible;

                }));
            }).ContinueWith(task =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    authRequest.Password = txtPasswordResponse.Password;
                    string result = Connect(apoint, authRequest, overWrite);

                    LoadingPanel.ClosePanel();
                    if (result == "Connecté")
                    {
                        
                        DialogResult = true;
                        Close();

                    }
                    else
                    {
                        txtQuestion.Foreground = new SolidColorBrush(Colors.Red);
                        txtQuestion.Content = "* " + result;
                    }
                }));
                //}, null);

                //    prefix2.Visibility = Visibility.Hidden;
                //Title.Visibility = Visibility.Hidden;
                //lblInfo.Visibility = Visibility.Hidden;
                
                
               
            });


        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CloseWin(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Enter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                DialogResult = true;
                Close();
            }
        }
    }
}
