﻿using Selfizee.Managers;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Printing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ClientPhotoList.xaml
    /// </summary>
    public partial class ClientPhotoList : UserControl
    {

        private string mediaDataPath;
        private int selected = 0;
        private int begin = 0;
        private bool strip = false;
        private int end = 0;
        private string toPrint = "";
        private string[] imageFiles = new string[0];
        public string code { get; set; }
        private string photosDirectory = string.Empty;
        private DispatcherTimer timerCountdown;
        public int timeout;
        private double cumulativeDeltaX;
        private double cumulativeDeltaY;
        private double linearVelocity;

        public ClientPhotoList()
        {
            
            Globals._FlagConfig = "client";
            InitializeComponent();

            code = Globals.codeEvent_toEdit;
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            mediaDataPath = "C:\\EVENTS\\Media\\" + code + "\\Data\\print.txt";

            var brushCancel = new ImageBrush();
            brushCancel.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnVisualisationCroix, UriKind.Relative));
            cancelCopie.Background = brushCancel;

            var brushPrint = new ImageBrush();
            brushPrint.ImageSource = new BitmapImage(new Uri(Globals._defautlbtnprintrose, UriKind.Relative));
            print.Background = brushPrint;

            Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);

            //var brushTB = new ImageBrush();
            //brushTB.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnTB, UriKind.Relative));
            //btnTB.Background = brushTB;

            var brushRetour = new ImageBrush();
            brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnAnim, UriKind.Relative));
            //btnRetour.Background = brushRetour;

            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);


            var brushNext = new ImageBrush();
            brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhiteNext, UriKind.Relative));
            Next.Background = brushNext;

            var brushPrevious = new ImageBrush();
            brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnWhitePrevious, UriKind.Relative));
            Previous.Background = brushPrevious;

            //INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
            if (_print <= 0)
            {
                print.IsEnabled = false;
            }
            if (Globals.ScreenType == "SPHERIK")
            {
                txtDate.Margin = new Thickness(5, 50, 5, 5);
                img_View.Width = 900;
                img_View.Height = 500;
            }
            photosDirectory = Globals._MediaFolder + "\\" + code + "\\Photos";
            //LoadLstImages(photosDirectory);
            //imgCercle.Visibility = Visibility.Hidden;
            //LaunchTimerAfter2();
        }
        private void Imprimer(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing(false);


        }



        private void WindowLoaded(object sender, EventArgs e)
        {

            //string iniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            //INIFileManager iniFile = new INIFileManager(iniPath);
            //string Name = iniFile.GetSetting("EVENT", "Name");         
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;
                }));
                Thread.Sleep(100);

            }).ContinueWith(task =>
            {
                LoadLstImages(photosDirectory);
                LaunchTimerAfter2();
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    
                    LoadingPanel.ClosePanel();
                    //lbl_loading.Visibility = Visibility.Hidden;
                }));
            });

        }
        private void Printing(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;
                    //string binPath;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    //printerName = inimanager.GetSetting("PRINTING", "printerName");
                    //printerName = printerName.Replace('"', ' ');
                    //printerName = printerName.Replace('\\', ' ');
                    //printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    System.Drawing.Printing.PrinterSettings printerSettings = pd.PrinterSettings;

                    //if (printerName != "DEFAULT")
                    //    printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (strip)
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "Settings2InchCut.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                            else
                            {
                                if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                                {
                                    Globals.printed = true;
                                    pd.PrinterSettings = printerSettings;
                                    pd.PrintPage += PrintPage;
                                    pd.Print();
                                }
                                else
                                {
                                    MessageBox.Show("PrinterSettings is not updated!");
                                }
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }
                    lbl_CurrentPrinting.Content = "Impression en cours....";
                    closeImageViewer();
                    Thread.Sleep(5000);

                }));

            });

        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }
        private void writeTxtFileData(string filename, string toWrite)
        {
            //int i = 0;

            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }
        private void closeImageViewer()
        {
            dockGrid.IsEnabled = true;
            dockGrid.Opacity = 1;
            this.Opacity = 1;
            lstImages.Background = System.Windows.Media.Brushes.Transparent;
            scrollCopie.Visibility = Visibility.Collapsed;

            //Deselectionner l'image de la ListBox pour pouvoir re-cliquer
            lstImages.SelectedIndex = -1;
        }
        private void LoadLstImages(string pathDirectory)
        {

            //var result = new ObservableCollection<System.Windows.Controls.Image>();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                var result = new ObservableCollection<ImageItemViewModel>();
                if (!string.IsNullOrEmpty(pathDirectory))
                {

                    getFiles(pathDirectory);
                    foreach (string myFile in Directory.GetFiles(pathDirectory))
                    {
                        System.Windows.Controls.Image myLocalImage = new System.Windows.Controls.Image();

                        BitmapImage myImageSource = new BitmapImage();

                        myImageSource.BeginInit();
                        myImageSource.CacheOption = BitmapCacheOption.OnLoad;
                        myImageSource.UriSource = new Uri(@"file:///" + myFile);
                        myImageSource.DecodePixelHeight = 200;
                        myImageSource.EndInit();

                        myLocalImage.Source = myImageSource;
                        result.Add(new ImageItemViewModel { ImageItem = myImageSource, IsChecked = false, ImagePath = Uri.UnescapeDataString(myImageSource.UriSource.AbsolutePath) });
                    }
                }
                lstImages.ItemsSource = result;
            }));
        }

       

        private void getFiles(string photosDirectory)
        {
            int index = 0;
            var getFilesDirectory = Directory.GetFiles(photosDirectory);
            
            foreach (var image in getFilesDirectory)
            {
                index++;
                System.Array.Resize(ref imageFiles, index);
                imageFiles[index - 1] = image;
            }
            this.selected = 0;
            this.begin = 0;
            this.end = imageFiles.Length;

            ChangeLblPhotos(getFilesDirectory.Length);

        }
        private void ChangeLblPhotos(int nbPhotos)
        {
            if (nbPhotos > 1)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    nb_Photos.Content = $"{nbPhotos} PHOTOS";
                }));
                
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    nb_Photos.Content = $"{nbPhotos} PHOTO";
                }));
                
            }
        }
        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            strip = false;
            var selectedImage = (sender as ListBox).SelectedIndex;
            lbl_CurrentPrinting.Content = "";
            if (selectedImage >= 0)
            {
                this.Opacity = 1;
                var item = (ImageItemViewModel)lstImages.Items[selectedImage];

                //var listeImages = (ObservableCollection<ImageItemViewModel>)lstImage.ItemsSource;
                //var item = listeImages.FindIndex(x => x.ImagePath == path);            
                //selected = Array.IndexOf(listeImages, item);
                //selected = lstImage.Select((c, i) => new { value = c, Index = i })
                //                    .Where(x => x.value.ImagePath == item.ImagePath)
                //                    .Select(x => x.Index).FirstOrDefault();
                timerCountdown.Stop();
                selected = selectedImage;
                toPrint = item.ImagePath;
                ShowImage(toPrint);
                if (toPrint.ToUpper().Contains("STRIP"))
                {
                    strip = true;
                }
                dockGrid.IsEnabled = true;
                DateTime creation = File.GetCreationTime(toPrint);
                string day = creation.Day.ToString();
                string month = creation.Month.ToString();
                string year = creation.Year.ToString();
                string hour = creation.Hour.ToString();
                string minutes = creation.Minute.ToString();
                if (day.Length == 1) day = "0" + day;
                if (month.Length == 1) month = "0" + month;
                if (hour.Length == 1) hour = "0" + hour;
                if (minutes.Length == 1) minutes = "0" + minutes;
                txtDate.Text = "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
                lstImages.Background = System.Windows.Media.Brushes.Transparent;
                scrollCopie.Visibility = Visibility.Visible;
            }
        }
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            timerCountdown.Stop();
            imgCercle.Visibility = Visibility.Hidden;
            LaunchTimerAfter2();
        }
        private void copieCancel(object sender, EventArgs e)
        {
            closeImageViewer();
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ShowNextImage();
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }

        }
        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

        }
        private void ShowNextImage()
        {
            //ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);

            var pathPhoto = this.imageFiles[(++this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }
        private void ShowPrevImage()
        {
            var pathPhoto = this.imageFiles[(--this.selected) % this.imageFiles.Length];
            ShowImage(pathPhoto);
            txtDate.Text = GetDatePhoto(pathPhoto);
        }

        public void ShowImage(string path)
        {
            //selected = Array.IndexOf(imageFiles, path);
            Bitmap imageBitmap = new Bitmap(path);
            img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
            var actual_Width = img_View.Width;
            var actual_Height = img_View.Height;
            var x = (int)SystemParameters.PrimaryScreenWidth * 50 / 100;
            var y = (int)SystemParameters.PrimaryScreenHeight * 50 / 100;

            if (actual_Width > actual_Height)
            {
                img_View.Height = img_View.Height * x / img_View.Width;
                img_View.Width = x;
            }
            else if (actual_Width < actual_Height)
            {
                img_View.Width = img_View.Width * y / img_View.Height;
                img_View.Height = y;
            }
            else
            {
                img_View.Width = x;
                img_View.Height = x;
            }

            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }


        }
        private string GetDatePhoto(string pathPhoto)
        {
            DateTime creation = File.GetCreationTime(pathPhoto);
            string day = creation.Day.ToString();
            string month = creation.Month.ToString();
            string year = creation.Year.ToString();
            string hour = creation.Hour.ToString();
            string minutes = creation.Minute.ToString();
            if (day.Length == 1) day = "0" + day;
            if (month.Length == 1) month = "0" + month;
            if (hour.Length == 1) hour = "0" + hour;
            if (minutes.Length == 1) minutes = "0" + minutes;
            return "Photo prise le " + day + "/" + month + "/" + year + " à " + hour + "h" + minutes;
        }
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ClientPhotoListViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ClientPhotoListViewModel)DataContext;
            if (Globals.ScreenType == "SPHERIK")
            {
                if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                    viewModel.GoToClientSumarySpherik.Execute(null);
            }
            else if (Globals.ScreenType == "DEFAULT")
            {
                if (viewModel.GoToClientSumary.CanExecute(null))
                    viewModel.GoToClientSumary.Execute(null);
            }
        }
        public void GotoAccueil()
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ClientPhotoListViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }
        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0)
            //{
            //    setTimeout();
            //}
           
            timeout--;
            if (timeout == 0)
            {
                Timer.Text = "";
                imgCercle.Visibility = Visibility.Hidden;
            }
            else
            {
                Timer.Text = timeout.ToString();
                imgCercle.Visibility = Visibility.Visible;
            }

            if (timeout == -1)
            {
                Timer.Text = "";
                timerCountdown.Stop();
                GotoAccueil();
            }
        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }
        void LaunchTimerAfter2()
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromMinutes(2);
            timerCountdown.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown.Start();
        }
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }


        private void Next_Photo()
        {
            if (selected < imageFiles.Length)
            {
                ShowNextImage();
            }
            if (Previous.Visibility == Visibility.Hidden)
            {
                Previous.Visibility = Visibility.Visible;
            }
            if (selected == imageFiles.Length - 1)
            {
                Next.Visibility = Visibility.Hidden;
            }
            else
            {
                Next.Visibility = Visibility.Visible;
            }
        }

        private void Previous_Photo()
        {
            if (selected > 0)
            {
                ShowPrevImage();
            }
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }
        }


        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if(e.Delta > 0)
            {
                Next_Photo();
            }
            else
            {
                Previous_Photo();
            }
        }

        private void centerImageView_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = this;
            e.Handled = true;
        }

        private void centerImageView_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {

        //store values of horizontal & vertical cumulative translation

            cumulativeDeltaX = e.CumulativeManipulation.Translation.X;

            cumulativeDeltaY = e.CumulativeManipulation.Translation.Y;

            //store value of linear velocity into horizontal direction  

            linearVelocity = e.Velocities.LinearVelocity.X;
        }

        private void centerImageView_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
            e.ExpansionBehavior = new InertiaExpansionBehavior()
            {
                InitialVelocity = e.InitialVelocities.ExpansionVelocity,
                DesiredDeceleration = 10.0 * 96.0 / 1000000.0
            };
        }

        private void centerImageView_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            //check if this is swipe gesture
            if (cumulativeDeltaX < 0)
            {
                //show previous image
                Previous_Photo();
            }
            else
            {
                //show next image
                Next_Photo();
            }
        }
    }
}
