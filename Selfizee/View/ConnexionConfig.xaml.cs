﻿using Newtonsoft.Json;
using Selfizee.Managers;
using Selfizee.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using static Selfizee.View.HistoUpdateWindow;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for ConnexionConfig.xaml
    /// </summary>
    public partial class ConnexionConfig : UserControl
    {
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        private int timeout { get; set; }
        private bool autoupdate = false;
        IniUtility iniWriting = new IniUtility(Globals._appConfigFile);
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        Boolean haveAccount = true;
        public ConnexionConfig()
        {

            
            InitializeComponent();
            this.PreviewKeyDown += GameScreen_PreviewKeyDown;
            alphanumeriquekeyboardForm1.Visibility = Visibility.Hidden;
            if (!Globals.getRemoteElements)
            {
                DownloadUpdateElements();
                CheckAndSendVersion();
                Globals.getRemoteElements = true;
            }
            
            if (Globals.ScreenType.ToUpper() == "SPHERIK")
            {
                int x = (int)SystemParameters.PrimaryScreenWidth;
                keybordDck.Width = x;
                keybordDck.Height = 400;
                allDck.Width = x;
                alphanumeriquekeyboardForm1.Width = (x * 90) / 100;
                alphanumeriquekeyboardForm1.Height = 300;
                keybordDck1.Width = x;
                allGrid.Width = x;
                dockall.Width = x;
            }


            Globals.timerHomeLaunched = true;
            Globals.leaveHome = false;
            ImageBrush btnBrush = new ImageBrush();
            if(File.Exists(Globals._defaultbtnConnexion))
                btnBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConnexion, UriKind.Absolute));
            btnConnexion.Background = btnBrush;
            btnConfirm.Background = btnBrush;
            Bitmap imageBitmap = new Bitmap(Globals._defaultbtnselfizee_logo1);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
            if (File.Exists(Globals._defaultbtnHome))
            {
                Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
                icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);
            }
            
            setTimeout();

            if (Globals._FlagConfig == "client")
            {
                gotoDashBoard.Visibility = Visibility.Visible;
            }
            else if (Globals._FlagConfig == "admin")
            {
                gotoDashBoard.Visibility = Visibility.Hidden;
            }
            //LaunchTimerCountdown();
            var haveLogin = iniAppFile.GetSetting("CONNEXION", "login");
            var haveMdp = iniAppFile.GetSetting("CONNEXION", "password");

            if(String.IsNullOrEmpty(haveLogin) || String.IsNullOrEmpty(haveMdp))
            {
                ConnexionPanel.Visibility = Visibility.Collapsed;
                InitCompte.Visibility = Visibility.Visible;
                haveAccount = false;
            }
            else
            {
                ConnexionPanel.Visibility = Visibility.Visible;
                InitCompte.Visibility = Visibility.Collapsed;
                haveAccount = true;
            }
            LaunchTimerAfter2();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingPanel.ClosePanel();
            }));
        }

        public void CheckAndSendVersion()
        {
            try
            {
                string path = "C:\\EVENTS\\Events\\AppConfig.ini";
                INIFileManager _inimanager = new INIFileManager(path);
                string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");

                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string localVersion = name.Version.ToString();

                DateTime datenow = DateTime.Now;
                string sdatenow = datenow.ToString("yyyy-MM-dd HH:mm:ss");

                VersionData version = new VersionData();
                version.numero_borne = idborne;
                version.numero_version = localVersion;
                version.date_heure_installation = sdatenow;

                //string dir = "C:\\Events\\My ini\\Config.ini";
                //Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);

                if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                {
                    string isSent = _inimanager.GetSetting("SENDVERSION", "IsSent");
                    if (isSent.ToLower() == "false")
                    {
                        string response = SendVersionData(version);
                        string[] lresponnse = response.Split(',');
                        string successMsg = lresponnse[0].Split(':')[1];
                        string Msgsuccess = Regex.Replace(successMsg, "}", "");
                        if (Msgsuccess == "true")
                        {
                            //isSent = "true";
                            IniUtility _iniUtility = new IniUtility(path);
                            _iniUtility.Write("IsSent", "true", "SENDVERSION");
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }

        }

        public string SendVersionData(VersionData version)
        {
            string responseInString = "";
            try
            {
                string url = "https://booth.selfizee.fr/api/setVersionLogiciel";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["numero_borne"] = version.numero_borne;
                    data["numero_version"] = version.numero_version;
                    data["date_heure_installation"] = version.date_heure_installation;
                    var response = wb.UploadValues(url, "POST", data);

                    responseInString = Encoding.UTF8.GetString(response);
                }
            }
            catch (Exception e)
            {

            }
            return responseInString;

        }

        public void Write(string input)
        {
            string path = @"C:\\Events\\JsonText.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            if (File.Exists(path))
            {
                File.Delete(path);
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(input);
                }
            }
        }

        public string Read()
        {
            string outputs = "";
            string path = @"C:\\Events\\JsonText.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    outputs = s;
                }
            }
            return outputs;
        }

        private void DownloadUpdateElements()
        {
            if (InternetConnectionManager.checkConnection("booth.selfizee.fr"))//(InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                string jsonString = "";
                try
                {
                    string idborne = iniWriting.Read("idborne", "EVENTSCONFIG");
                    string json = new WebClient().DownloadString("https://booth.selfizee.fr/version-logiciels/getListVersionLogicielByBorne/" + idborne + ".json");
                    Write(json);
                    jsonString = Read();
                }
                catch (Exception e)
                {

                }
                var records = JsonConvert.DeserializeObject<List<RootObject>>(jsonString);
                if (records != null)
                {
                    RootObject root = records.First();
                    string url = root.url_photo_couverture;
                    string dir = "C:\\Events\\Assets\\Default\\Images\\img_couv.jpg";
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(new Uri(url), dir);
                    }
                }

            }
            else
            {

            }
        }
        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {            
            switch (e.Key)
            {
                case Key.Enter:
                    if (haveAccount)
                    {
                        ShowButton();
                    }
                    else
                    {
                        CreateAccount();
                    }
                    
                    break;

            }
        }
        public void TxtGotFocus(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            alphanumeriquekeyboardForm1.Visibility = Visibility.Visible;
            alphanumeriquekeyboardForm1.ActiveContainer = textbox;
        }

        public void TxtLostFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboardForm1.Visibility = Visibility.Collapsed;
        } 
        
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            imgCercle.Visibility = Visibility.Hidden;
            if(timerCountdown != null) timerCountdown.Stop();
            setTimeout();
            LaunchTimerAfter2();
        }
        public void LaunchTimerCountdown()
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            try
            {
                timeout--;
                if (timeout == 0)
                {
                    Timer.Text = "";
                    imgCercle.Visibility = Visibility.Hidden;
                }
                else
                {
                    Timer.Text = timeout.ToString();
                    imgCercle.Visibility = Visibility.Visible;
                }

                if (timeout == -1)
                {
                    Timer.Text = "";
                    if (timerCountdown != null) timerCountdown.Stop();
                    GotoAccueil();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void GotoAccueil()
        {
            var viewModel = (ConnexionConfigViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void GotoAccueil(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (ConnexionConfigViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }

        public void GotoDashBoard(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (ConnexionConfigViewModel)DataContext;
            KillMe();
            if (viewModel != null)
            {
                if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }
            }
        }

        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                timeout = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                timeout = 10;
            }


            if (timeout < 0)
            {
                timeout *= -1;
            }
        }

        private void ShowButton()
        {
            
            var login = CryptText(Login.Text).ToLower();
            var mdp = CryptText(Mdp.Text).ToLower();

            var haveLogin = iniAppFile.GetSetting("CONNEXION", "login").ToLower();
            var haveMdp = iniAppFile.GetSetting("CONNEXION", "password").ToLower();
            if (login.Equals(haveLogin) && mdp.Equals(haveMdp))
            {
                GoToListEvent();
            }
            else
            {
                string Titre = "Connexion";
                string Message = "Login ou mot de passe incorrect";
                CustomInformationBox.Prompt(Titre, Message);
            }
        }

        private bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void CreateAccount(object sender, RoutedEventArgs e)
        {
            var login = InitLogin.Text;
            var mdp = InitMdp.Text;
            var confirmMdp = InitMdpConfirm.Text;

            if (!String.IsNullOrEmpty(login) && !String.IsNullOrEmpty(mdp) &&  mdp.Equals(confirmMdp))
            {
                iniWriting.Write("login", CryptText(login).ToLower(), "CONNEXION");
                iniWriting.Write("password", CryptText(confirmMdp).ToLower(), "CONNEXION");

                GoToListEvent();
            }
            else
            {
                if (String.IsNullOrEmpty(login))
                {
                    string Titre = "Création compte";
                    string Message = "Login obligatoire";
                    CustomInformationBox.Prompt(Titre, Message);
                }
                else if(String.IsNullOrEmpty(mdp))
                {
                    string Titre = "Création compte";
                    string Message = "Mot de passe obligatoire";
                    CustomInformationBox.Prompt(Titre, Message);
                }
                else if (!mdp.Equals(confirmMdp))
                {
                    string Titre = "Création compte";
                    string Message = "Confirmation du mot de passe incorrect";
                    CustomInformationBox.Prompt(Titre, Message);
                }
            }           

        }

        private void CreateAccount()
        {
            var login = InitLogin.Text;
            var mdp = InitMdp.Text;
            var confirmMdp = InitMdpConfirm.Text;

            if (!String.IsNullOrEmpty(login) && !String.IsNullOrEmpty(mdp) && mdp.Equals(confirmMdp))
            {
                iniWriting.Write("login", CryptText(login).ToLower(), "CONNEXION");
                iniWriting.Write("password", CryptText(confirmMdp).ToLower(), "CONNEXION");
                GoToListEvent();
            }
            else
            {
                if (String.IsNullOrEmpty(login))
                {
                    string Titre = "Création compte";
                    string Message = "Login obligatoire";
                    CustomInformationBox.Prompt(Titre, Message);
                }
                else if (String.IsNullOrEmpty(mdp))
                {
                    string Titre= "Création compte";
                    string Message = "Mot de passe obligatoire";
                    CustomInformationBox.Prompt(Titre, Message);
                }
                else if (!mdp.Equals(confirmMdp))
                {
                    string Titre = "Création compte";
                    string Message = "Confirmation du mot de passe incorrect";
                    CustomInformationBox.Prompt(Titre, Message);
                }
            }

        }

        private void GoToListEvent()
        {
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    LoadingPanel.ShowPanel();
                    //lbl_loading.Visibility = Visibility.Visible;
                }));

            }).ContinueWith(task =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    Globals.showUpdateSoftware = true;
                    var viewModel = (ConnexionConfigViewModel)DataContext;
                    if (chk_con())
                    {
                        SendWs(Globals.ws_login, Globals.ws_pwd);
                    }
                    KillMe();

                    if (viewModel.GoToListEvent.CanExecute(null))
                    {
                        viewModel.GoToListEvent.Execute(null);
                    }

                }));
            });
        }

        private void ShowButton(object sender, RoutedEventArgs e)
        {
            var login = CryptText(Login.Text).ToLower();
            var mdp = CryptText(Mdp.Text).ToLower();

            var haveLogin = iniAppFile.GetSetting("CONNEXION", "login").ToLower();
            var haveMdp = iniAppFile.GetSetting("CONNEXION", "password").ToLower();
            if (login.Equals(haveLogin) && mdp.Equals(haveMdp))
            {
                GoToListEvent();
            }
            else
            {
                string Titre = "Connexion";
                string Message = "Login ou mot de passe incorrect";
                CustomInformationBox.Prompt(Titre, Message);
            }
            
        }

        private void SendWs(string login, string mdp)
        {
            try
            {
                var client = new WebClient();
                var method = "POST"; // If your endpoint expects a GET then do it.
                var parameters = new NameValueCollection();

                parameters.Add("username", login);
                parameters.Add("password", mdp);

                /* Always returns a byte[] array data as a response. */
                var response_data = client.UploadValues(Globals.ws_url_connexion, method, parameters);

                // Parse the returned data (if any) if needed.
                var responseString = Encoding.UTF8.GetString(response_data);
                ConnexionData obj = JsonConvert.DeserializeObject<ConnexionData>(responseString);
                if (obj.success)
                {
                    Globals.auth_token = "Bearer " + obj.data.token;
                }
            }
            catch(Exception ex)
            {
                string Titre = "Connexion";
                string Message = "Vérifiez votre connexion";
                CustomInformationBox.Prompt(Titre, Message);
            }
        }

        public void getConnected()
        {
           
            var viewModel = (ConnexionConfigViewModel)DataContext;
            KillMe();
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
            //if (viewModel.GoToClientSumary.CanExecute(null))
            //    viewModel.GoToClientSumary.Execute(null);
        }


        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown_minute.Stop();
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();

        }
        public void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(2);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }

        private void KillMe()
        {
            if (timerCountdown != null) timerCountdown.Stop();
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();

            //  System.GC.Collect();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private string CryptText(string texte)
        {
            string newText = "";
            foreach (char c in texte)
            {
                var t = Convert.ToInt32(c);
                t++;
                newText = newText + "" + char.ConvertFromUtf32(t);
            }
            return newText.Replace("=","") ;
        }


    }
}
