﻿using Selfizee.Managers;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ClientConfig.xaml
    /// </summary>
    public partial class ClientConfig : UserControl
    {
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        string idborne = "";
        private DispatcherTimer timerCountdown;
        private DispatcherTimer timerCountdown_minute;
        //public int timeout = Globals.timeOut;
        public int timeout;
        IniUtility iniWriting;
        bool green_BGPresent = false;
        INIFileManager _ini;
        private string code;
        private int cadre = 0;
        private int polaroid = 0;
        private int multishoot = 0;
        private bool blockExit = false;
        private int strip = 0;
        public ClientConfig()
        {
            InitializeComponent();

           // float lifePerc = System.Windows.Forms.SystemInformation.PowerStatus.BatteryLifePercent * 100;
            code = Globals.codeEvent_toEdit;
           /* int y = (int)SystemParameters.PrimaryScreenHeight;
            int h = y - 600;
            ClavierDock.Margin = new Thickness(0,10,0,0);*/
            //Bitmap imageChoix = new Bitmap("C:\\EVENTS\\Media\\Photos\\Coast.jpg");
            //choiximg1.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);
            //choiximg2.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);
            //choiximg3.Source = ImageUtility.convertBitmapToBitmapImage(imageChoix);

            //var brushTB = new ImageBrush();
            //brushTB.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnTB, UriKind.Relative));
            //btnTB.Background = brushTB;

            //var brushRetour = new ImageBrush();
            //brushRetour.ImageSource = new BitmapImage(new Uri(Globals._defaultLaunchAnim, UriKind.Relative));
            //btnRetour.Background = brushRetour;

            r1.Visibility = Visibility.Visible;
            r2.Visibility = Visibility.Hidden;
            r3.Visibility = Visibility.Hidden;
            r4.Visibility = Visibility.Hidden;

            Animation.Visibility = Visibility.Visible;
            Options.Visibility = Visibility.Hidden;
            Impression.Visibility = Visibility.Hidden;
            Global.Visibility = Visibility.Hidden;
            
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;

            Bitmap imagePuceBtn = new Bitmap(Globals._defaultBtnIcoPuceBtn);
            puce1.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            puce2.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);
            puce3.Source = ImageUtility.convertBitmapToBitmapImage(imagePuceBtn);

            Bitmap imageHome = new Bitmap(Globals._defaultbtnHome);
            icoHome.Source = ImageUtility.convertBitmapToBitmapImage(imageHome);
            
            Bitmap imageCercle = new Bitmap(Globals._defaultImgCercle);
            imgCercle.Source = ImageUtility.convertBitmapToBitmapImage(imageCercle);

            //var brushsave = new ImageBrush();
            //brushsave.ImageSource = new BitmapImage(new Uri(Globals._defaultGreenSave, UriKind.Relative));
            //btnSave.Background = brushsave;

            string PicLink = "";
            DirectoryInfo d;
            FileInfo[] Files;
            string str = "";
            string color = "";
            string sepia = "";
            string bw = "";

            _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            string greenDirectory = "C:\\EVENTS\\Assets\\" + code + "\\GreenScreen";
            cadre = Convert.ToInt32(_ini.GetSetting("CADRE", "activated"));
            polaroid= Convert.ToInt32(_ini.GetSetting("POLAROID", "activated"));
            multishoot= Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "activated"));
            strip= Convert.ToInt32(_ini.GetSetting("STRIP", "activated"));
            if (Directory.Exists(greenDirectory))
            {
                if(Directory.GetFiles(greenDirectory).ToList().Count > 0)
                {
                    green_BGPresent = true;
                }
            }
            if (cadre == 1)
            {
                typeForm.Content = "FRAME";
                d = new DirectoryInfo("C:\\EVENTS\\Assets\\" + code + "\\Frames\\Cadre");
                if (d.Exists)
                {
                    Files = d.GetFiles("*.png");
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpg");
                    }
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpeg");
                    }
                    if (Files != null)
                    {
                        foreach (FileInfo file in Files)
                        {
                            /*if (!file.Name.StartsWith("preview_"))
                            {
                                str = file.Name;
                            }*/
                            str = file.Name;

                        }
                        if (string.IsNullOrEmpty(str))
                        {
                            foreach (FileInfo file in Files)
                            {
                                if (!file.Name.StartsWith("preview_"))
                                {
                                    str = file.Name;
                                }

                            }
                        }
                        PicLink = "C:\\EVENTS\\Assets\\" + code + "\\Frames\\Cadre\\" + str;
                    }
                    else
                    {
                        PicLink = "";
                    }
                }
                color = _ini.GetSetting("CADRE", "COLOR");
                bw = _ini.GetSetting("CADRE", "BlackWhite");
                sepia = _ini.GetSetting("CADRE", "Sepia");
                countdown_cadre.Text = _ini.GetSetting("CADRE", "countdown");
            }
            else if (polaroid == 1)
            {
                typeForm.Content = "POLAROID";
                d = new DirectoryInfo("C:\\EVENTS\\Assets\\" + code + "\\Frames\\Polaroid");
                if (d.Exists)
                {
                    Files = d.GetFiles("*.png");
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpg");
                    }
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpeg");
                    }
                    if (Files != null)
                    {
                        foreach (FileInfo file in Files)
                        {
                            str = file.Name;

                        }
                        if (string.IsNullOrEmpty(str))
                        {
                            foreach (FileInfo file in Files)
                            {
                                if (!file.Name.StartsWith("preview_"))
                                {
                                    str = file.Name;
                                }

                            }
                        }
                        PicLink = "C:\\EVENTS\\Assets\\" + code + "\\Frames\\Polaroid\\" + str;
                    }
                    else
                    {
                        PicLink = "";
                    }
                }
                color = _ini.GetSetting("POLAROID", "COLOR");
                bw = _ini.GetSetting("POLAROID", "BlackWhite");
                sepia = _ini.GetSetting("POLAROID", "Sepia");
                countdown_cadre.Text = _ini.GetSetting("POLAROID", "countdown");
            }
            else if (multishoot == 1)
            {
                typeForm.Content = "MULTISHOOT";
                d = new DirectoryInfo("C:\\EVENTS\\Assets\\" + code + "\\Frames\\Multishoot");
                if (d.Exists)
                {
                    Files = d.GetFiles("*.png");
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpg");
                    }
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpeg");
                    }
                    if (Files != null)
                    {
                        foreach (FileInfo file in Files)
                        {
                            str = file.Name;

                        }
                        if (string.IsNullOrEmpty(str))
                        {
                            foreach (FileInfo file in Files)
                            {
                                if (!file.Name.StartsWith("preview_"))
                                {
                                    str = file.Name;
                                }

                            }
                        }
                        PicLink = "C:\\EVENTS\\Assets\\" + code + "\\Frames\\Multishoot\\" + str;
                    }
                    else
                    {
                        PicLink = "";
                    }
                }
                color = _ini.GetSetting("MULTISHOOT", "COLOR");
                bw = _ini.GetSetting("MULTISHOOT", "BlackWhite");
                sepia = _ini.GetSetting("MULTISHOOT", "Sepia");
                countdown_cadre.Text = _ini.GetSetting("MULTISHOOT", "countdown");
            }
            else if (strip == 1)
            {
                typeForm.Content = "STRIP";
                d = new DirectoryInfo("C:\\EVENTS\\Assets\\" + code + "\\Frames\\Strip");
                if (d.Exists)
                {
                    Files = d.GetFiles("*.png");
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpg");
                    }
                    if (Files.Count() <= 0)
                    {
                        Files = d.GetFiles("*.jpeg");
                    }
                    if (Files != null)
                    {
                        foreach (FileInfo file in Files)
                        {
                            str = file.Name;

                        }
                        if (string.IsNullOrEmpty(str))
                        {
                            foreach (FileInfo file in Files)
                            {
                                if (!file.Name.StartsWith("preview_"))
                                {
                                    str = file.Name;
                                }

                            }
                        }
                        PicLink = "C:\\EVENTS\\Assets\\" + code + "\\Frames\\Strip\\" + str;
                    }
                    else
                    {
                        PicLink = "";
                    }
                }
                color = _ini.GetSetting("STRIP", "COLOR");
                bw = _ini.GetSetting("STRIP", "BlackWhite");
                sepia = _ini.GetSetting("STRIP", "Sepia");
                countdown_cadre.Text = _ini.GetSetting("CADRE", "STRIP");
            }


            idborne = iniAppFile.GetSetting("EVENTSCONFIG", "idborne").ToLower();
           // numBorne.Content = idborne;
            
            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            string localVersion = name.Version.ToString();
          //  numVersion.Content = localVersion;

            if (PicLink != "")
            {
                Bitmap imageFrame = new Bitmap(PicLink);
                Frame.Source = ImageUtility.convertBitmapToBitmapImage(imageFrame);
            }
            
            //Bitmap imageFrame = new Bitmap("C:\\EVENTS\\Assets\\"+code+"\\Frames\\Cadre"+filename);
            //Frame.Source = ImageUtility.convertBitmapToBitmapImage(imageFrame);

            Bitmap imagelogoNoir = new Bitmap(Globals._defaultLogoNoir);
       //     img_logo.Source = ImageUtility.convertBitmapToBitmapImage(imagelogoNoir);

            iniWriting = new IniUtility("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            TOGlobal.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOGlobal.LostFocus += LostTextBoxKeyboardFocus;
            TOThanksGlobal.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOThanksGlobal.LostFocus += LostTextBoxKeyboardFocus;
            countdown_cadre.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            countdown_cadre.LostFocus += LostTextBoxKeyboardFocus;
            nb_authorizedmultiprinting.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            nb_authorizedmultiprinting.LostFocus += LostTextBoxKeyboardFocus;
            nb_authorizedprinting.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            nb_authorizedprinting.LostFocus += LostTextBoxKeyboardFocus;

            //INIFileManager iniEventFile = new INIFileManager("C:Events\\Assets\\" + code + "\\Config.ini");
            TOGlobal.Text = _ini.GetSetting("GENERAL", "timeout");
            var timeoutThanks = _ini.GetSetting("GENERAL", "timeoutThanks");
            if (string.IsNullOrEmpty(timeoutThanks))
            {
                TOThanksGlobal.Text =  "5";
                iniWriting.Write("timeoutThanks", "5", "GENERAL");
            }
            else
            {
                TOThanksGlobal.Text = timeoutThanks;
            }
            

            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            int mirrored = Convert.ToInt32(_ini.GetSetting("LIVEVIEW", "mirror"));
            if (mirrored == 1)
            {
                mirror.IsChecked = true;
            }
            else
            {
                mirror.IsChecked = false;
            }

            int reprised = Convert.ToInt32(_ini.GetSetting("BUTTONCONFIG", "enableButtonRetakePicture"));
            if (reprised == 1)
            {
                reprise.IsChecked = true;
            }
            else
            {
                reprise.IsChecked = false;
            }

            if (color == "1")
            {
                _color.IsChecked = true;
            }
            else
            {
                _color.IsChecked = false;
            }
            if (bw == "1")
            {
                _BlackWhite.IsChecked = true;
            }
            else
            {
                _BlackWhite.IsChecked = false;
            }
            if (sepia == "1")
            {
                _Sepia.IsChecked = true;
            }
            else
            {
                _Sepia.IsChecked = false;
            }
            int greenscreen = Convert.ToInt32(_ini.GetSetting("EVENT", "GreenScreen"));
            if (greenscreen == 1)
            {
                if (green_BGPresent)
                {
                    Param_green.Visibility = Visibility.Visible;
                }
                else
                {
                    Param_green.Visibility = Visibility.Hidden;
                }
                active_fond.IsChecked = true;
                choix.Visibility = Visibility.Visible;
                bool greenscreenImage = false;
                if (greenscreenImage) {
                    choix1.Visibility = Visibility.Visible;
                    choix2.Visibility = Visibility.Visible;
                    choix3.Visibility = Visibility.Visible;
                }
                else
                {
                    choix1.Visibility = Visibility.Visible;
                    choix2.Visibility = Visibility.Visible;
                    choix3.Visibility = Visibility.Visible;
                }
            }
            else
            {
                Param_green.Visibility = Visibility.Hidden;
                active_fond.IsChecked = false;
                choix.Visibility = Visibility.Hidden;
            }

            
            int Print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
            if (Print == 1)
            {
                printing.IsChecked = true;
            }
            else
            {
                printing.IsChecked = false;
            }

            int MultiPrint = Convert.ToInt32(_ini.GetSetting("PRINTING", "MultiPrinting"));
            if (Print == 1)
            {
                multiple_printing.IsChecked = true;
            }
            else
            {
                multiple_printing.IsChecked = false;
            }
            int printAuthorized = Convert.ToInt32(_ini.GetSetting("PRINTING", "eventauthorizedprint"));
            nb_authorizedprinting.Text = printAuthorized.ToString();
            string strActivated = _ini.GetSetting("FORM", "activated");
            int form = 0;
            if (!string.IsNullOrEmpty(strActivated))
            {
                form = Convert.ToInt32(strActivated);
            }
            if(Globals.typeEvent == "carmila")
            {
                blockFormulaire.Visibility = Visibility.Visible;
                NoForm.Visibility = Visibility.Collapsed;
                if (form == 1)
                {
                    formulaire.IsChecked = true;
                }
                else
                {
                    formulaire.IsChecked = false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Globals.typeEvent))
                {
                    if(File.Exists("C:\\EVENTS\\Assets\\" + code + "\\Form\\content.xml")){
                        blockFormulaire.Visibility = Visibility.Visible;
                        NoForm.Visibility = Visibility.Collapsed;
                        if (form == 1)
                        {
                            formulaire.IsChecked = true;
                        }
                        else
                        {
                            formulaire.IsChecked = false;
                        }

                    }
                    else
                    {
                        blockFormulaire.Visibility = Visibility.Collapsed;
                        NoForm.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    blockFormulaire.Visibility = Visibility.Collapsed;
                    NoForm.Visibility = Visibility.Visible;
                }
            }
            

            string nbprint = (_ini.GetSetting("PRINTING", "eventauthorizedmultiprinting"));
            nb_authorizedmultiprinting.Text = nbprint;

            if (Globals.fromAdmin)
            {
                blockNbMaxPrinting.Visibility = Visibility.Visible;
            }
            else
            {
                blockNbMaxPrinting.Visibility = Visibility.Collapsed;
            }
            if (Globals.ScreenType == "SPHERIK")
            {
                menu.Margin = new Thickness(-100, 0, 0, 0);
                Global.Height = 550;
                Impression.Height = 550;
                Options.Height = 550;
                Animation.Height = 550;
            }
            if (printing.IsChecked == true)
            {
                blockMultiPrint.Visibility = Visibility.Visible;
                blockNbPrint.Visibility = Visibility.Visible;
                if (Globals.fromAdmin)
                {
                    blockNbMaxPrinting.Visibility = Visibility.Visible;
                }
            }
            else
            {
                blockMultiPrint.Visibility = Visibility.Hidden;
                blockNbPrint.Visibility = Visibility.Hidden;
                blockNbMaxPrinting.Visibility = Visibility.Hidden;
            }
            imgCercle.Visibility = Visibility.Hidden;
            int y = (int)SystemParameters.PrimaryScreenHeight;
            int h = y - 360;
            ClavierDock.Margin = new Thickness(0, h, 0, 0);
            LaunchTimerAfter2();
            //Task.Run(()=>LaunchTimerAfter2());
        }
        public void GotoAccueil()
        {
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ClientConfigsViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToAccueil.CanExecute(null))
                    viewModel.GoToAccueil.Execute(null);
            }
        }
        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            if (blockExit)
            {
                string Titre = "Important";
                string Message = "Attention, veuillez activer au minimum un filtre de couleur sur votre cadre.";
                bool result = CustomInformationBox.Prompt(Titre, Message);
            }
            else
            {
                string code = Globals.codeEvent_toEdit;
                IniUtility iniFile = new IniUtility(Globals._appConfigFile);
                iniFile.Write("id", code, "EVENTSCONFIG");
                var viewModel = (ClientConfigsViewModel)DataContext;
                if (Globals._FlagConfig == "client")
                {
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                            viewModel.GoToClientSumarySpherik.Execute(null);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        if (viewModel.GoToClientSumary.CanExecute(null))
                            viewModel.GoToClientSumary.Execute(null);
                    }
                }
                else if (Globals._FlagConfig == "admin")
                {
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        if (viewModel.GoToAdminSumarySpherik.CanExecute(null))
                            viewModel.GoToAdminSumarySpherik.Execute(null);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        if (viewModel.GoToAdminSumary.CanExecute(null))
                            viewModel.GoToAdminSumary.Execute(null);
                    }
                    
                }
                
            }
            
        }

        private void GreenSettings(object sender, RoutedEventArgs e)
        {
            var vm = (ClientConfigsViewModel)DataContext;

            if (vm.GoToSettingGreen.CanExecute(null))
                vm.GoToSettingGreen.Execute(null);
        }
        public void GotoAccueil_Click(object sender, RoutedEventArgs e)
        {
            if (blockExit)
            {
                string Titre = "Important";
                string Message = "Attention, veuillez activer au minimum un filtre de couleur sur votre cadre.";
                bool result = CustomInformationBox.Prompt(Titre, Message);
            }
            else
            {
                GotoAccueil();
            }
                
        }
        //void timerCountdown_Tick(object sender, EventArgs e)
        //{
        //    //if (Globals.timeOut <= 0)
        //    //{
        //    //    setTimeout();
        //    //}
        //    timeout--;
        //    if (timeout == 0)
        //    {
        //        Timer.Text = "";
        //    }
        //    else
        //    {
        //        Timer.Text = timeout.ToString();
        //    }

        //    if (timeout == -1)
        //    {
        //        Timer.Text = "";
        //        timerCountdown.Stop();
        //        GotoAccueil();
        //    }
        //}
        //void LaunchTimerCountdown()
        //{
        //    timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
        //    timerCountdown.Interval = TimeSpan.FromSeconds(1);
        //    timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
        //    timerCountdown.Start();
        //}
        //public void setTimeout()
        //{
        //    IniUtility ini = new IniUtility(Globals._iniFile);
        //    if (ini.KeyExists("timeout", "GENERAL"))
        //    {
        //        Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
        //    }
        //    else
        //    {
        //        Globals.timeOut = 10;
        //    }


        //    if (Globals.timeOut < 0)
        //    {
        //        Globals.timeOut *= -1;
        //    }
        //}
        private void save(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TOGlobal.Text))
            {
                iniWriting.Write("timeout", TOGlobal.Text, "GENERAL");
            }

            if (!string.IsNullOrEmpty(TOThanksGlobal.Text))
            {
                iniWriting.Write("timeoutThanks", TOThanksGlobal.Text, "GENERAL");
            }

            _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            int cadre = Convert.ToInt32(_ini.GetSetting("CADRE", "activated"));
            int polaroid = Convert.ToInt32(_ini.GetSetting("POLAROID", "activated"));
            int multishoot = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "activated"));
            int strip = Convert.ToInt32(_ini.GetSetting("STRIP", "activated"));
            //int polaroidmultishoot = Convert.ToInt32(_ini.GetSetting("POLAROID MULTISHOOT", "activated"));

            if (cadre == 1)
            {
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "CADRE");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "CADRE");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "CADRE");
                }
                
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "CADRE");
                }
            }

           else if (polaroid == 1)
            {
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "POLAROID");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "POLAROID");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "POLAROID");
                }
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "POLAROID");
                }
            }
            else if (multishoot == 1)
            {
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "MULTISHOOT");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "MULTISHOOT");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "MULTISHOOT");
                }
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "MULTISHOOT");
                }
            }
            else if (strip == 1)
            {
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "STRIP");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "STRIP");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "STRIP");
                }
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "STRIP");
                }
                
            }
            /*else if(polaroidmultishoot == 1)
            {
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "STRIP");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "STRIP");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "STRIP");
                }
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "POLAROID MULTISHOOT");
                }
            }
            */
            if (multiple_printing.IsChecked == true)
            {
                iniWriting.Write("MultiPrinting", "1", "PRINTING");
            }
            else
            {
                iniWriting.Write("MultiPrinting", "0", "PRINTING");
            }
            if (Globals.fromAdmin)
            {
                if (!string.IsNullOrEmpty(nb_authorizedprinting.Text))
                {
                    iniWriting.Write("eventauthorizedprint", nb_authorizedprinting.Text, "PRINTING");
                }
            }

            if (reprise.IsChecked == true)
            {
                iniWriting.Write("enableButtonRetakePicture", "1", "BUTTONCONFIG");
            }
            else
            {
                iniWriting.Write("enableButtonRetakePicture", "0", "BUTTONCONFIG");
            }
                
            int mirrored = Convert.ToInt32(_ini.GetSetting("LIVEVIEW", "mirror"));
            if (mirror.IsChecked == true)
            {
                iniWriting.Write("mirror", "1", "LIVEVIEW");
            }
            else
            {
                iniWriting.Write("mirror", "0", "LIVEVIEW");
            }
            if (active_fond.IsChecked == true)
            {
                iniWriting.Write("GreenScreen", "1", "EVENT");
            }
            else
            {
                iniWriting.Write("GreenScreen", "0", "EVENT");
            }

            if (!string.IsNullOrEmpty(nb_authorizedmultiprinting.Text))
            {
                iniWriting.Write("eventauthorizedmultiprinting", nb_authorizedmultiprinting.Text, "PRINTING");
            }

            if (printing.IsChecked == true)
            {
                iniWriting.Write("print", "1", "PRINTING");
            }
            else
            {
                iniWriting.Write("print", "0", "PRINTING");
            }

            if (multiple_printing.IsChecked == true)
            {
                iniWriting.Write("MultiPrinting", "1", "PRINTING");
            }
            else
            {
                iniWriting.Write("MultiPrinting", "0", "PRINTING");
            }

            if (formulaire.IsChecked == true)
            {
                iniWriting.Write("activated", "1", "FORM");
            }
            else
            {
                iniWriting.Write("activated", "0", "FORM");
            }

            if (_color.IsChecked == false && _BlackWhite.IsChecked == false && _Sepia.IsChecked == false)
            {
                blockExit = true;
            }
            else
            {
                blockExit = false;
            }
            if (blockExit)
            {
                string Titre = "Important";
                string Message = "Attention, veuillez activer au minimum un filtre de couleur sur votre cadre.";
                bool result = CustomInformationBox.Prompt(Titre, Message);
            }
            else
            {
                string Titre = "Information";
                string Message = "Enregistrement effectué avec succès !";
                bool result = CustomInformationBox.Prompt(Titre, Message);
            }

            //MessageBox.Show("Enregistrement effectué avec succès");
            //MessageBox.Show("Enregistrement effectué avec succès !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            string tag = textbox.Tag.ToString();

            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
                numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Visible;
                numeriqueKeyboard.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Visible;
                alphanumeriquekeyboard.ActiveContainer = textbox;
                alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Collapsed;
            }
        }
        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;
        }

        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        private void BtnAnimation_Click(object sender, RoutedEventArgs e)
        {
            r1.Visibility = Visibility.Visible;
            r2.Visibility = Visibility.Hidden;
            r3.Visibility = Visibility.Hidden;
            r4.Visibility = Visibility.Hidden;

            Animation.Visibility = Visibility.Visible;
            Options.Visibility = Visibility.Hidden;
            Impression.Visibility = Visibility.Hidden;
            Global.Visibility = Visibility.Hidden;
        }
        private void BtnOption_Click(object sender, RoutedEventArgs e)
        {
            r1.Visibility = Visibility.Hidden;
            r2.Visibility = Visibility.Visible;
            r3.Visibility = Visibility.Hidden;
            r4.Visibility = Visibility.Hidden;

            Animation.Visibility = Visibility.Hidden;
            Options.Visibility = Visibility.Visible;
            Impression.Visibility = Visibility.Hidden;
            Global.Visibility = Visibility.Hidden;
        }
        private void BtnImpression_Click(object sender, RoutedEventArgs e)
        {
            r1.Visibility = Visibility.Hidden;
            r2.Visibility = Visibility.Hidden;
            r3.Visibility = Visibility.Visible;
            r4.Visibility = Visibility.Hidden;

            Animation.Visibility = Visibility.Hidden;
            Options.Visibility = Visibility.Hidden;
            Impression.Visibility = Visibility.Visible;
            Global.Visibility = Visibility.Hidden;
        }
        private void BtnGlobaln_Click(object sender, RoutedEventArgs e)
        {
            r1.Visibility = Visibility.Hidden;
            r2.Visibility = Visibility.Hidden;
            r3.Visibility = Visibility.Hidden;
            r4.Visibility = Visibility.Visible;

            Animation.Visibility = Visibility.Hidden;
            Options.Visibility = Visibility.Hidden;
            Impression.Visibility = Visibility.Hidden;
            Global.Visibility = Visibility.Visible;
        }
        
        public void setTimeout()
        {
            IniUtility ini = new IniUtility(Globals._iniFile);
            if (ini.KeyExists("timeout", "GENERAL"))
            {
                Globals.timeOut = Convert.ToInt32(ini.Read("timeout", "GENERAL")) + 1;
            }
            else
            {
                Globals.timeOut = 10;
            }


            if (Globals.timeOut < 0)
            {
                Globals.timeOut *= -1;
            }
        }
        public void txtColor_Click(object sender, RoutedEventArgs e)
        {
            if (_color.IsChecked==false)
            {
                _color.IsChecked = true;
            }
            else
            {
                _color.IsChecked = false;
            }
        }
        public void txtBlackWhite_Click(object sender, RoutedEventArgs e)
        {
            if (_BlackWhite.IsChecked == false)
            {
                _BlackWhite.IsChecked = true;
            }
            else
            {
                _BlackWhite.IsChecked = false;
            }
        }
        public void txtSepia_Click(object sender, RoutedEventArgs e)
        {
            if (_Sepia.IsChecked == false)
            {
                _Sepia.IsChecked = true;
            }
            else
            {
                _Sepia.IsChecked = false;
            }
        }
        public void Block_Click(object sender, RoutedEventArgs e)
        {
            DockPanel Block = sender as DockPanel;
            string nameBlock = Block.Name;
            switch (nameBlock)
            {
                case "blockMirror":
                    if (mirror.IsChecked == true)
                    {
                        mirror.IsChecked = false;
                    }
                    else
                    {
                        mirror.IsChecked = true;
                    }
                    break;
                case "blockReprise":
                    if (reprise.IsChecked == true)
                    {
                        reprise.IsChecked = false;
                    }
                    else
                    {
                        reprise.IsChecked = true;
                    }
                    break;
                case "blockFondVert":
                    if (active_fond.IsChecked == true)
                    {
                        active_fond.IsChecked = false;
                        Param_green.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        active_fond.IsChecked = true;
                        if (green_BGPresent)
                        {
                            Param_green.Visibility = Visibility.Visible;
                        }
                        
                    }
                    break;
                case "blockFormulaire":
                    if (formulaire.IsChecked == true)
                    {
                        formulaire.IsChecked = false;
                    }
                    else
                    {
                        formulaire.IsChecked = true;
                    }
                    break;
                case "blockPrint":
                    if (printing.IsChecked == true)
                    {
                        printing.IsChecked = false;
                    }
                    else
                    {
                        printing.IsChecked = true;
                    }
                    break;
                case "blockMultiPrint":
                    if (multiple_printing.IsChecked == true)
                    {
                        multiple_printing.IsChecked = false;
                    }
                    else
                    {
                        multiple_printing.IsChecked = true;
                    }
                    break;
            }
        }
        public void GreenEvent(object sender, RoutedEventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;
            if (btn.IsChecked == true)
            {
                if (green_BGPresent)
                {
                    Param_green.Visibility = Visibility.Visible;
                }
                else
                {
                    Param_green.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                Param_green.Visibility = Visibility.Hidden;
            }
        }
        public void PrintChecked(object sender, RoutedEventArgs e)
        {
            if (printing.IsChecked == true)
            {
                blockMultiPrint.Visibility = Visibility.Visible;
                blockNbPrint.Visibility = Visibility.Visible;
                if (Globals.fromAdmin)
                {
                    blockNbMaxPrinting.Visibility = Visibility.Visible;
                }
            }
            else
            {
                blockMultiPrint.Visibility = Visibility.Hidden;
                blockNbPrint.Visibility = Visibility.Hidden;
                blockNbMaxPrinting.Visibility = Visibility.Hidden;
            }
        }

        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //if (Globals.timeOut <= 0)
            //{
            //    setTimeout();
            //}
            
            timeout--;
            if (timeout == 0)
            {
                Timer.Text = "";
                imgCercle.Visibility = Visibility.Hidden;
            }
            else
            {
                Timer.Text = timeout.ToString();
                imgCercle.Visibility = Visibility.Visible;
            }

            if (timeout == -1)
            {
                Timer.Text = "";
                GotoAccueil();
            }
        }
        public void LaunchTimerCountdown(object sender, EventArgs e)
        {
            timerCountdown = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
            timerCountdown_minute.Stop();

        }
        void LaunchTimerAfter2()
        {
            timerCountdown_minute = new DispatcherTimer(); //DispatcherPriority.Send
            timerCountdown_minute.Interval = TimeSpan.FromMinutes(1);
            timerCountdown_minute.Tick += new EventHandler(LaunchTimerCountdown);
            timerCountdown_minute.Start();
        }
        public void Page_Click(object sender, RoutedEventArgs e)
        {
            Timer.Text = "";
            if (timerCountdown_minute != null) timerCountdown_minute.Stop();
            if (timerCountdown != null) timerCountdown.Stop();
            imgCercle.Visibility = Visibility.Hidden;
            INIFileManager _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            timeout = int.Parse(_ini.GetSetting("GENERAL", "timeout"));
            LaunchTimerAfter2();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {

            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);

        }

       
    }
}
