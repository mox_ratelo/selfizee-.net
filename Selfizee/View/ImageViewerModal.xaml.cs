﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for ImageViewerModal.xaml
    /// </summary>
    public partial class ImageViewerModal : UserControl
    {
        private string mediaDataPath;
        private int selected = 0;
        private int begin = 0;
        private string toPrint = "";
        private int end = 0;
        private string[] imageFiles = new string[0];
        public ImageViewerModal()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            cancelCopie.Background = brushCroix;

            var brushNext = new ImageBrush();
            brushNext.ImageSource = new BitmapImage(new Uri(Globals._defaultnext, UriKind.Relative));
            Next.Background = brushNext;

            var brushPrevious = new ImageBrush();
            brushPrevious.ImageSource = new BitmapImage(new Uri(Globals._defaultprevious, UriKind.Relative));
            Previous.Background = brushPrevious;

            var brushdelete = new ImageBrush();
            brushdelete.ImageSource = new BitmapImage(new Uri(Globals._defaultdeleteconfig, UriKind.Relative));
            delete.Background = brushdelete;
        }

        private void getFiles()
        {
            //int index = 0;
            //foreach (var image in _photos)
            //{
            //    index++;
            //    System.Array.Resize(ref imageFiles, index);
            //    imageFiles[index - 1] = image.Source;
            //}
            //this.selected = 0;
            //this.begin = 0;
            //this.end = imageFiles.Length;
            //nb_Photos.Text = imageFiles.Length + " PHOTOS";
        }

        private bool _hideRequest = false;
        private bool _result = false;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public string Url
        {
            get { return (string)GetValue(UrlProperty); }
            set { SetValue(UrlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UrlProperty =
            DependencyProperty.Register(
                "Url", typeof(string), typeof(ImageViewerModal), new UIPropertyMetadata(string.Empty));

        #endregion

        public bool ShowHandlerDialog(string url)
        {
            Url = url;
            Visibility = Visibility.Visible;

            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        private void ShowNextImage()
        {
            ShowImage(this.imageFiles[(++this.selected) % this.imageFiles.Length]);
        }

        private void copieCancel(object sender, EventArgs e)
        {
            //dockGrid.IsEnabled = true;
            ////dockGrid.Background = System.Windows.Media.Brushes.Black;
            //dockGrid.Opacity = 1;
            //this.Opacity = 1;
            //scrollCopie.Visibility = Visibility.Collapsed;
        }

        private void deletePhoto_click(object sender, RoutedEventArgs e)
        {
            //string csvPath = "C:\\EVENTS\\Media\\" + Globals.to + "\\Data\\data.csv";

            //removeRowOnCSV(csvPath);
            removePhoto(toPrint);
        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {

            BinaryFileManager mgrBin = new BinaryFileManager();
            string binFileName = Globals._dataFolder + "\\" + Globals.GetEventId() + ".bin";
            //lbl_CurrentPrinting.Content = "Impression en cours....";
            int toWrite = mgrBin.readIntData(binFileName);
            toWrite += Convert.ToInt32(1);
            writeTxtFileData(mediaDataPath, toWrite.ToString());
            mgrBin.writeData(toWrite, binFileName);
            Printing(false);
     }

        private void Printing(bool sendMail)
        {

            Task.Factory.StartNew(() =>
            {

                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    //savingData();
                }));

                System.Threading.Thread.Sleep(1);
            }).ContinueWith(task =>
            {
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("printing");
                }
                this.Dispatcher.BeginInvoke((Action)(() =>
                {

                    string printerName;

                    var inimanager = new Managers.INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
                    printerName = inimanager.GetSetting("PRINTING", "printerName");
                    printerName = printerName.Replace('"', ' ');
                    printerName = printerName.Replace('\\', ' ');
                    printerName = printerName.TrimEnd().TrimStart();

                    IntPtr hDevMode = IntPtr.Zero;
                    IntPtr pDevMode = IntPtr.Zero;

                    PrintDocument pd = new PrintDocument();

                    PrinterSettings printerSettings = pd.PrinterSettings;

                    if (printerName != "DEFAULT")
                        printerSettings.PrinterName = printerName;

                    printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                    printerName = LocalPrintServer.GetDefaultPrintQueue().FullName;

                    printerSettings.Copies = 1;
                    printerSettings.DefaultPageSettings.PaperSize = new PaperSize("Custom 6x4", 615, 413); //new PaperSize("210 x 297 mm", 210, 297); 
                    pd.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    pd.OriginAtMargins = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                    try
                    {
                        this.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (SettingsPrinter.UploadPrinterSettings(pd.PrinterSettings, "SettingsDefault.bin"))
                            {
                                Globals.printed = true;
                                pd.PrinterSettings = printerSettings;
                                pd.PrintPage += PrintPage;
                                pd.Print();
                            }
                            else
                            {
                                MessageBox.Show("PrinterSettings is not updated!");
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n " + "stacktrace:" + ex.StackTrace.ToString());
                    }

                    if (sendMail)
                    {
                        Globals.printed = true;
                        var viewModel = (VisualisationViewModel)DataContext;
                        if (viewModel.GoToFormStepOne.CanExecute(null))
                            viewModel.GoToFormStepOne.Execute(null);

                    }
                    else
                    {
                    }
                }));

            });

        }

        private void writeTxtFileData(string filename, string toWrite)
        {
            int i = 0;

            if (!File.Exists(filename))
            {
                var file = File.Create(filename);
                file.Close();
                file.Dispose();
            }

            File.WriteAllText(filename, toWrite);
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            var bmp = Bitmap.FromFile(toPrint);
            if (bmp.Height < bmp.Width)
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else
            {
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            //MessageBox.Show($"width : {m.Width} - height : {m.Height}");
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.DrawImage(bmp, e.PageBounds);
            bmp.Dispose();
        }

        public void removePhoto(string path)
        {

            //System.GC.Collect();
            //System.GC.WaitForPendingFinalizers();
            File.Delete(path);
        }

        private void removeRowOnCSV(string csvpath)
        {
            string identity = System.IO.Path.GetFileNameWithoutExtension(toPrint);
            List<String> lines = new List<string>();
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(csvpath);

            while ((line = file.ReadLine()) != null)
            {
                lines.Add(line);
            }

            lines.RemoveAll(l => l.Contains(identity));
            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(csvpath))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

        public void ShowImage(string path)
        {
            selected = Array.IndexOf(imageFiles, path);
            Bitmap imageBitmap = new Bitmap(path);
            img_View.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
        }

        private void ShowPrevImage()
        {
            ShowImage(this.imageFiles[(--this.selected) % this.imageFiles.Length]);
        }

       

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevImage();
            if (Next.Visibility == Visibility.Hidden)
            {
                Next.Visibility = Visibility.Visible;
            }
            if (selected == 0)
            {
                Previous.Visibility = Visibility.Hidden;
            }
            else
            {
                Previous.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Show the next image.
        /// </summary>

        

       
    }
}
