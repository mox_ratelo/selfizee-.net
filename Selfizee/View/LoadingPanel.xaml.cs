﻿using System.Windows;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour LoadingPanel.xaml
    /// </summary>
    public partial class LoadingPanel : Window
    {
        public static LoadingPanel panel ;
        public LoadingPanel()
        {
            InitializeComponent();
            lbl_chargement.Content = "Chargement en cours ...";
        }

        public LoadingPanel(string texte)
        {
            InitializeComponent();
            lbl_chargement.Content = texte;
        }

        public static void ShowPanel(string texte = "")
        {
            if (!string.IsNullOrEmpty(texte))
            {
                panel = new LoadingPanel(texte);
            }
            else
            {
                panel = new LoadingPanel();
            }
            panel.ShowDialog();
        }
        
        public static  void ClosePanel()
        {
            if (panel != null)
                panel.Close();

        }
    }
}
