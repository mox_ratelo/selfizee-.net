﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using Selfizee.Manager;
using System.Threading;
using Selfizee.Managers;
using System.Threading.Tasks;
using Selfizee.Models;
using log4net;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Controls;
using Aurigma.GraphicsMill.Transforms;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for TakeWebcamCamera.xaml
    /// </summary>
    public partial class TakeWebcamCamera : System.Windows.Controls.UserControl
    {

        #region Public properties

        public ObservableCollection<FilterInfo> VideoDevices { get; set; }

        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }
        #endregion
        private FilterInfo _currentDevice;

        #region Private fields

        private VideoCaptureDevice _videoSource;

        #endregion
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        private string path_ToSave = "";
        private string _imageFolder = @"View";
        private string _finalFolder = @"Final";
        private int currentwidth = 0;
        private int currentHeight = 0;
        private int nbShoot = 0;
        private Bitmap currentGSBackgroound = null;
        private int currentIdentification = 0;
        private string currentchromaKeyBG = "";
        private string currentBackground = "";
        private int focusInfo;
        private int sizeHeight = 0;
        private int sizeWidth = 0;
        static ManualResetEvent WaitEvent;
        static ManualResetEvent WaitChromaKey;
        int incrementCountDown = 0;
        int incrementimage = 0;
        String currDir = "";
        Bitmap imageBitmap;
        int xPosition = 0;
        bool portrait = false;
        bool pictureTaken = false;
        EOSDigital.API.Camera currentCamera;
        bool firstPhoto_Taken = false;
        bool IsInit = false;
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private  INIFileManager _inimanager = new INIFileManager(EventIni);
        public List<string> filtres = new List<string>();
        ImageBrush bgbrush = new ImageBrush();
        Action<BitmapImage> SetImageAction;
        int countdownManaged = 0;
        public delegate void ShowLoaderDelegate();
        int xImage = 0;
        int currentCanvasWidth = 0;
        int currentCanvasHeight = 0;
        ImageFilterManager imageManager = new ImageFilterManager();
        object ErrLock = new object();
        public delegate void ReadyToShowDelegate(object sender, EventArgs args);
        bool photoDownloaded = false;
        public event ReadyToShowDelegate ReadyToShow;
        private DispatcherTimer timer;
        //private List<BitmapImage> lstVideoFrame = new List<BitmapImage>();
        private DispatcherTimer timerCountdown;
        int left = 0;
        int widthportrait = 0;
        int top = 0;
        bool found = false;
        int width = 0;
        int _offer = 0;
        int height = 0;
        private delegate void DispatchHandler(string arg);
        private static readonly ILog Log =
             LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int minWidth = 0;
        int minHeight = 0;
        int previous_height = 0;
        int previous_width = 0;

        int iteration = 0;
        public TakeWebcamCamera()
        {
            try
            {
                InitializeComponent();
                EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
                _inimanager = new INIFileManager(EventIni);
                Globals._shootDynamic = null;
                Globals.nbShootCounter = 0;
                Globals.dtTakenPhoto = DateTime.MinValue;
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                incrementCountDown = Globals.countdown;
                RemoveFiles();
                setTimerToShowMain();
                
                //Setting background
                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("takephoto");

                if (backgroundAttributes.IsImage)
                {
                    addImageBrush();
                }

                var brushCroix = new ImageBrush();
                brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
                close.Background = brushCroix;
                close.Click += closeInfo;

                scrollNoCamera.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                scrollNoCamera.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }

                }

                int x = (int)SystemParameters.PrimaryScreenWidth;
                int y = (int)SystemParameters.PrimaryScreenHeight;

                this.Width = x;
                this.Height = y;
               

                getAllActivatedFilter();
                WaitEvent = new ManualResetEvent(false);
                WaitChromaKey = new ManualResetEvent(false);
                //IniUtility ini = new IniUtility(Globals._iniFile);
                nbShoot = getNbShoot();
                currentchromaKeyBG = "";
                currentchromaKeyBG = GetChromaKeyBG();
                currentBackground = "";
                Globals.incrementShoot = 0;
                if (nbShoot == 1)
                {
                    Globals.nbShootCounter = 1;
                }
                else if (Globals.nbShootCounter == 0)
                {
                    Globals.nbShootCounter = nbShoot;
                }
                else
                    Globals.nbShootCounter--;
                SetImageAction = (BitmapImage img) => { CanvasImage.Source = img; };
                currDir = Globals.currDir;
                path_ToSave = Globals.currDir + $"\\{_imageFolder}\\IMG_TEMP.JPG";
                _finalFolder = currDir + $"\\{_finalFolder}";
                IsInit = true;
                Globals.getDynamicPosition();


            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            LoadingPanel.ClosePanel();
        }

        private void closeInfo(object sender, EventArgs e)
        {
            
            mainGrid.IsEnabled = true;
            mainGrid.Opacity = 1;
            this.Opacity = 1;
            scrollNoCamera.Visibility = Visibility.Collapsed;
            var viewModel = (TakeWebcamCameraViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        private void GetVideoDevices()
        {
            string camera_usb = iniAppFile.GetSetting("WEBCAM", "Name");
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (!string.IsNullOrEmpty(camera_usb))
            {
                foreach (var _videoDevice in VideoDevices)
                {
                    if (_videoDevice.Name.ToLower().Equals(camera_usb.ToLower()))
                    {
                        found = true;
                        CurrentDevice = _videoDevice;
                    }
                }
            }
            else
            {
                
                if (VideoDevices.Any())
                {
                    if (VideoDevices.Count == 1)
                    {
                        found = true;
                        CurrentDevice = VideoDevices[0];
                    }
                    else
                    {
                        foreach (var _videoDevice in VideoDevices)
                        {
                            if (_videoDevice.Name.ToLower().Equals("lenovo easycamera") || _videoDevice.Name.ToLower().Equals("usb camera") || _videoDevice.Name.ToLower().Equals("microsoft camera front") || _videoDevice.Name.ToLower().Equals("logitech brio"))
                            //if (_videoDevice.Name == "HP HD Camera")
                            {
                                found = true;
                                CurrentDevice = _videoDevice;
                            }
                        }
                    }
                }
                else
                {
                    Log.Error("No video sources found");
                }
            }
            
            if (!found)
            {
                var viewModel = (TakeWebcamCameraViewModel)DataContext;
                if (viewModel.GoToNoCamera.CanExecute(null))
                    viewModel.GoToNoCamera.Execute(null);
                //scrollNoCamera.Visibility = Visibility.Visible;
            }
        }


        public void setOneBitmapImage(BitmapImage img)
        {
            if (img != null) bgbrush.ImageSource = img;
        }

        private void HandleCompletion(DispatcherOperation operation)
        {
            object result = operation.Result;
            // Use the result here
        }

        void showLoader()
        {
            GIFCtrl.Visibility = Visibility.Visible;
            GIFCtrl.StartAnimate();
        }

        void closeSplash()
        {
            foreach (var wndOtherWindow in System.Windows.Application.Current.Windows)
            {
                if (wndOtherWindow is SplashScreenWindow)
                {
                    (wndOtherWindow as Window).Hide();
                }
            }
        }

        public void RenderCanvas()
        {
            int index = 0;
            if (currentIdentification == 4 || currentIdentification == 3)
            {
                string[] imageFiles = Directory.GetFiles(Globals._tempFolder + "\\Color");
                foreach (string image in imageFiles)
                {
                    index++;
                    if (nbShoot == 3)
                    {
                        switch (index)
                        {
                            case 1:
                                CVShot1.Visibility = Visibility.Visible;
                                break;
                            case 2:
                                CVShot2.Visibility = Visibility.Visible;
                                break;
                            case 3:
                                CVShot3.Visibility = Visibility.Visible;
                                break;
                        }
                    }
                    else if (nbShoot == 2)
                    {
                        switch (Globals.nbShootCounter)
                        {
                            case 1:
                                CVShot1.Visibility = Visibility.Visible;
                                break;
                            case 2:
                                CVShot2.Visibility = Visibility.Visible;
                                break;
                        }
                    }
                }


            }
        }

        public void showMiniature()
        {

            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Render, new DispatcherOperationCallback(delegate (object parameter)
            {
                try
                {
                    int index = 0;
                    if (currentIdentification == 4 || currentIdentification == 2)
                    {
                        string[] imageFiles = Directory.GetFiles(Globals._tempFolder + "\\Color");
                        foreach (string image in imageFiles)
                        {
                            index++;
                            int x = (int)SystemParameters.PrimaryScreenWidth;
                            int y = (int)SystemParameters.PrimaryScreenHeight;
                            int widthMultishoot = Globals._shootDynamic[index - 1].width;
                            int heightMultishoot = Globals._shootDynamic[index - 1].height;

                            if (nbShoot == 4)
                            {

                                switch (index)
                                {
                                    case 1:
                                        CVShot1.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap = new Bitmap(image);
                                        imageBitmap = ImageUtility.ResizeImage(imageBitmap, minWidth, minHeight);
                                        bdr1.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot1.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot1.Width = 80;
                                        }
                                        */
                                        CanvasShot1.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);//new BitmapImage(new Uri(image, UriKind.RelativeOrAbsolute));
                                        if (imageBitmap != null)
                                        {
                                            imageBitmap.Dispose();
                                        }

                                        break;
                                    case 2:
                                        CVShot2.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap1 = new Bitmap(image);
                                        imageBitmap1 = ImageUtility.ResizeImage(imageBitmap1, minWidth, minHeight);
                                        bdr2.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot2.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot2.Width = 80;
                                        }*/
                                        CanvasShot2.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                                        if (imageBitmap1 != null)
                                        {
                                            imageBitmap1.Dispose();
                                        }
                                        break;
                                    case 3:
                                        CVShot3.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap2 = new Bitmap(image);
                                        imageBitmap2 = ImageUtility.ResizeImage(imageBitmap2, minWidth, minHeight);
                                        bdr3.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot3.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot3.Width = 80;
                                        }*/
                                        CanvasShot3.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap2);
                                        if (imageBitmap2 != null)
                                        {
                                            imageBitmap2.Dispose();
                                        }
                                        break;
                                    case 4:
                                        CVShot4.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap3 = new Bitmap(image);
                                        imageBitmap3 = ImageUtility.ResizeImage(imageBitmap3, minWidth, minHeight);
                                        bdr4.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot4.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot4.Width = 80;
                                        }*/
                                        CanvasShot4.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap3);
                                        if (imageBitmap3 != null)
                                        {
                                            imageBitmap3.Dispose();
                                        }
                                        break;
                                }
                            }
                            else if (nbShoot == 3)
                            {

                                switch (index)
                                {
                                    case 1:
                                        CVShot1.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap = new Bitmap(image);
                                        imageBitmap = ImageUtility.ResizeImage(imageBitmap, minWidth, minHeight);
                                        bdr1.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot1.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot1.Width = 80;
                                        }
                                        */
                                        CanvasShot1.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);//new BitmapImage(new Uri(image, UriKind.RelativeOrAbsolute));
                                        if (imageBitmap != null)
                                        {
                                            imageBitmap.Dispose();
                                        }

                                        break;
                                    case 2:
                                        CVShot2.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap1 = new Bitmap(image);
                                        imageBitmap1 = ImageUtility.ResizeImage(imageBitmap1, minWidth, minHeight);
                                        bdr2.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*
                                        if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot2.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot2.Width = 80;
                                        }*/
                                        CanvasShot2.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                                        if (imageBitmap1 != null)
                                        {
                                            imageBitmap1.Dispose();
                                        }
                                        break;
                                    case 3:
                                        CVShot3.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap2 = new Bitmap(image);
                                        imageBitmap2 = ImageUtility.ResizeImage(imageBitmap2, minWidth, minHeight);
                                        bdr3.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot3.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot3.Width = 80;
                                        }*/
                                        CanvasShot3.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap2);
                                        if (imageBitmap2 != null)
                                        {
                                            imageBitmap2.Dispose();
                                        }
                                        break;
                                }
                            }
                            else if (nbShoot == 2)
                            {
                                switch (index)
                                {
                                    case 1:
                                        CVShot1.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap1 = new Bitmap(image);
                                        imageBitmap1 = ImageUtility.ResizeImage(imageBitmap1, 250, 151);
                                        bdr1.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot1.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot1.Width = 80;
                                        }*/
                                        CanvasShot1.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                                        if (imageBitmap1 != null)
                                        {
                                            imageBitmap1.Dispose();
                                        }
                                        break;
                                    case 2:
                                        CVShot2.Visibility = Visibility.Visible;
                                        Bitmap imageBitmap = new Bitmap(image);
                                        imageBitmap = ImageUtility.ResizeImage(imageBitmap, 250, 151);
                                        bdr2.BorderBrush = System.Windows.Media.Brushes.White;
                                        /*if (widthMultishoot == heightMultishoot)
                                        {
                                            CVShot2.Width = 195;
                                        }
                                        else if (widthMultishoot < heightMultishoot)
                                        {
                                            CVShot2.Width = 80;
                                        }*/
                                        CanvasShot2.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                                        if (imageBitmap != null)
                                        {
                                            imageBitmap.Dispose();
                                        }
                                        break;
                                }
                            }
                        }


                    }
                }
                catch (OutOfMemoryException ex)
                {

                }

                frame.Continue = false;
                return null;
            }), null);
            Dispatcher.PushFrame(frame);


        }

        void timer_Tick(object sender, EventArgs e)
        {

            timer.Stop();

            if (ReadyToShow != null)
            {
                ReadyToShow(this, null);
            }
        }

        void setTimerToShowMain()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void LaunchTimerCountdown()
        {
            Globals.incrementShoot++;
            
            if (Globals.customFormat)
            {
                Globals.countdown = Globals.customCountDown + 1;
            }
            else
            {
                Globals.countdown = countdownManaged;
            }

            incrementCountDown = countdownManaged - 1;
            timerCountdown = new DispatcherTimer();
            timerCountdown.Interval = TimeSpan.FromSeconds(1);
            timerCountdown.Tick += new EventHandler(timerCountdown_Tick);
            timerCountdown.Start();
        }

        private void getImageTaken()
        {

            try
            {
                RenderTargetBitmap rtBmp = new RenderTargetBitmap((int)CanvasImage.ActualWidth, (int)CanvasImage.ActualHeight,
       96.0, 96.0, PixelFormats.Pbgra32);

                CanvasImage.Measure(new System.Windows.Size((int)CanvasImage.ActualWidth, (int)CanvasImage.ActualHeight));
                CanvasImage.Arrange(new Rect(new System.Windows.Size((int)CanvasImage.ActualWidth, (int)CanvasImage.ActualHeight)));

                rtBmp.Render(CanvasImage);

                PngBitmapEncoder encoder = new PngBitmapEncoder();
                MemoryStream stream = new MemoryStream();
                encoder.Frames.Add(BitmapFrame.Create(rtBmp));

                // Save to memory stream and create Bitamp from stream
                encoder.Save(stream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(stream);
                bitmap.Save(path_ToSave);
                Globals.nbShootCounter--;
                if (Globals.nbShootCounter == 0)
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        showLoader();
                    }));

                }
                string[] imageFile = Directory.GetFiles(Globals._tmpDir);
                if (imageFile.Length > 0)
                {
                    photoDownloaded = true;
                }
                string firstImage = imageFile[0];

                string oldName = imageFile[0];
                string directoryView = Path.GetDirectoryName(oldName);
                //CanonSDK.EdsDownloadComplete(Info.Reference);
                //CanonSDK.EdsRelease(Info.Reference);

                if (currentchromaKeyBG != "" || Globals.gswithoutframe)
                {
                    Bitmap input = null;
                    input = new Bitmap(imageFile[0]);
                    //if (!Globals.gswithoutframe)
                    //{
                    var result = imageManager.RemoveBackground(input, currentchromaKeyBG);
                    if (result != null)
                    {
                        File.Delete(imageFile[0]);
                        result.Dispose();
                    }
                    //}


                }
                else
                {
                    string currentName = "";
                    string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
                    foreach (string image in imageFiles)
                    {
                        currentName = image;
                        oldName = Path.GetFileNameWithoutExtension(currentName);
                        oldName = Path.GetDirectoryName(image) + "\\IMAGE_COLOR.jpg";
                        Bitmap imageBitmap = new Bitmap(image);
                        imageBitmap.Save(oldName, ImageFormat.Jpeg);
                        imageBitmap.Dispose();

                    }
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(currentName);
                }

                if (nbShoot > 1)
                {
                    imageFile = Directory.GetFiles(Globals._tmpDir);
                    if (currentIdentification == 2 || currentIdentification == 4)
                    {
                        int index = Globals.incrementShoot;
                        if (currentIdentification == 2)
                        {
                            if (index == 2)
                            {
                                index++;
                            }
                            else if (index == 3)
                            {
                                index += 2;
                            }
                        }

                        File.Copy(imageFile[0], Globals._tempFolder + "\\Color\\temp" + index + ".jpg");
                    }
                    else
                    {
                        File.Copy(imageFile[0], Globals._tempFolder + "\\temp" + Globals.incrementShoot + ".jpg");
                    }

                    var tempView = currDir + $"\\{_imageFolder}";
                    DirectoryInfo directory = new DirectoryInfo(tempView);
                    foreach (var item in directory.GetFiles())
                        File.Delete(item.FullName);
                }
            }
            catch (Exception ex) { }
            finally
            {
                if (firstPhoto_Taken == false)
                {
                    firstPhoto_Taken = true;
                }
                WaitEvent.Set();
            }


        }
        void timerCountdown_Tick(object sender, EventArgs e)
        {
            //incrementCountDown--;
            Globals.countdown--;
           
            if (Globals.countdown > 0)
            {
                xImage = Convert.ToInt32(CanvasImage.Margin.Left);
                lbl_CountDown.Content = Globals.countdown;
                WaitChromaKey.WaitOne();
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("countdown_" + Globals.countdown);
                }
            }
            else if (Globals.countdown == 0)
            {
                lbl_CountDown.Content = "";
                
            }
            else if (Globals.countdown < 0)
            {
                lbl_CountDown.Content = "1";
                //MainCamera.TakePhotoShutter();
                StopCamera();
                lbl_CountDown.Content = "";
                MainCamera_DownloadReady();
                Globals.dtTakenPhoto = DateTime.Now;
                photoDownloaded = true;
                //WaitEvent.WaitOne();
                if (photoDownloaded)
                {
                    if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                    {
                        ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                        comMgr.writeData("phototaken");
                    }

                    if (currentIdentification == 2 || currentIdentification == 4)
                    {
                        showMiniature();
                    }
                    if (currentIdentification == 1 && portrait == true)
                    {
                        cropBitmap();
                    }
                    else if (currentIdentification == 3 || _offer == 1)
                    {
                        //cropPolaroidBitmap();
                    }
                    CloseSession();
                    Thread.Sleep(2000);
                    
                    //System.Threading.Thread.Sleep(3000);

                    //Globals.nbShootCounter--;

                    if (Globals.nbShootCounter <= 0)
                    {
                        //DisposeCamera();
                        //pictureTaken = true;
                        ////showLoader();
                        

                        Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("currentIdentification");
                            switch (currentIdentification)
                            {
                                case 1:
                                   
                                    if(_offer == 1)
                                    {
                                        ImageUtility.createPhotoOfferOverlayed(currentBackground, xPosition, filtres);
                                    }
                                    else if (portrait)
                                    {
                                        ImageUtility.createPhotoOverlayedPortrait(currentBackground, xPosition, filtres);
                                    }
                                    else
                                    {
                                        ImageUtility.createPhotoOverlayed(currentBackground, xPosition, filtres);
                                    }
                                    
                                    break;
                                case 2:
                                    string name = Path.GetFileName(currentBackground);
                                    currentBackground = Globals.originalStripFrame + "\\" + name;


                                    ImageUtility.createPhotoStrip(currentBackground, filtres);

                                    break;
                                case 3:
                                    ImageUtility.createPhotoPolaroid(currentBackground, filtres);
                                    break;

                                case 4:
                                    ImageUtility.createPhotoMultiShoot(filtres);
                                    break;
                            }
                            System.Threading.Thread.Sleep(1);
                        }).ContinueWith(task =>
                        {

                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                Console.WriteLine("StopAnimate");
                                GIFCtrl.StopAnimate();
                                GIFCtrl.Visibility = Visibility.Hidden;
                                //CloseSession();
                                if (_videoSource != null)
                                {
                                    _videoSource.Stop();
                                    _videoSource = null;
                                }
                                GC.SuppressFinalize(this);
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                CallFilterPage();
                            }));

                        });



                    }
                    else
                    {
                        //CloseSession();
                        //OpenSession();
                        //startLiveView();
                        Thread.Sleep(200);
                        StartCamera();
                        LaunchTimerCountdown();
                    }
                }
                else
                {
                    Globals.nbShootCounter++;
                    //OpenSession();
                    //startLiveView();
                    StartCamera();
                    LaunchTimerCountdown();
                    //lbl_CountDown.Content = "Echec de la mise au point...";
                    Thread.Sleep(3000);
                }
                
            }
        }

        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }

        public void cropBitmap()
        {
            string currentName = "";
            string oldName = "";
            string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
            foreach (string image in imageFiles)
            {
                currentName = image;
                oldName = Path.GetFileNameWithoutExtension(currentName);
                oldName = Path.GetDirectoryName(image) + "\\" + oldName + "_.JPG";
                Bitmap imageBitmap = new Bitmap(image);
                int diff = Convert.ToInt32(imageBitmap.Width / 3.2);
                //int xpos = Convert.ToInt32(imageBitmap.Width / 3.5);
                int xpos = Convert.ToInt32(imageBitmap.Width / 19);
               
                int widthCanvas = Convert.ToInt32(imageBitmap.Width / 1.1);
                int heightCanvas = Convert.ToInt32(LVCanvas.Height);
                imageBitmap = ImageUtility.CropBitmap(imageBitmap, xpos, 0, widthCanvas, imageBitmap.Height);
                imageBitmap.Save(oldName, ImageFormat.Jpeg);
                imageBitmap.Dispose();

            }
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(currentName);
            File.Copy(oldName, currentName);
            File.Delete(oldName);

        }

        public void cropPolaroidBitmap()
        {
            string currentName = "";
            string oldName = "";
            string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
            foreach (string image in imageFiles)
            {
                currentName = image;
                oldName = Path.GetFileNameWithoutExtension(currentName);
                oldName = Path.GetDirectoryName(image) + "\\" + oldName + "_.JPG";
                Bitmap imageBitmap = new Bitmap(image);
                int diff = Convert.ToInt32(imageBitmap.Height / 10);
                int xpos = Convert.ToInt32(imageBitmap.Width / 37) - 40;
                int widthCanvas = Convert.ToInt32(imageBitmap.Height);
                int heightCanvas = Convert.ToInt32(imageBitmap.Height) - diff;
                imageBitmap = ImageUtility.CropBitmap(imageBitmap, xpos, 110, heightCanvas, heightCanvas);
                imageBitmap.Save(oldName, ImageFormat.Jpeg);
                imageBitmap.Dispose();

            }
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(currentName);
            File.Copy(oldName, currentName);
            File.Delete(oldName);

        }

        /*
        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
            Application curApp = Application.Current;
            curApp.Shutdown();
        }
        */

        public String CreateFileName(int index)
        {
            DateTime dt = DateTime.Now;
            String formattedDate = (String.Format("{0:u}", dt));
            formattedDate = formattedDate.Replace(":", "");
            formattedDate = formattedDate.Trim();
            return "IMG_" + index + "_" + formattedDate + ".jpg";
        }

        public string GetEvent()
        {
            var vm = (TakeWebcamCameraViewModel)DataContext;
            var uri = string.IsNullOrEmpty(vm.selectedEvent) ? "" : vm.selectedBGCromaKey;
            if (uri.IndexOf("file:") >= 0)
            {
                int strindex = "file:///".Length;
                int len = uri.Length - strindex;
                return uri.Substring(strindex, len);
            }
            return uri;
        }

        public string GetChromaKeyBG()
        {
            var vm = (TakeWebcamCameraViewModel)DataContext;
            //var uri = string.IsNullOrEmpty(vm.selectedBGCromaKey) ? Globals.greenScreen : vm.selectedBGCromaKey;
            var uri = Globals.greenScreen;
            if (string.IsNullOrEmpty(uri)) return "";
            if (uri.IndexOf("file:") >= 0)
            {
                int strindex = "file:///".Length;
                int len = uri.Length - strindex;
                return uri.Substring(strindex, len);
            }
            else return uri;
        }

        public string GetBackground()
        {
            //var vm = (TakeWebcamCameraViewModel)DataContext;
            //var uri = string.IsNullOrEmpty(vm.selectedBackground) ? "" : vm.selectedBackground;
            //if (string.IsNullOrEmpty(uri)) return "";
            //if (uri.IndexOf("file:") >= 0)
            //{
            //    int strindex = "file:///".Length;
            //    int len = uri.Length - strindex;
            //    return uri.Substring(strindex, len);
            //}
            //else return uri;
            return Globals._currentBackground;
        }

        private void CloseSession()
        {
            IsInit = false;
            timerCountdown.Stop();
        }

        private void setCanvasToPicture(Visual target, string filename)
        {
            if (target == null)
                return;
            Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
            RenderTargetBitmap rtb = new RenderTargetBitmap((Int32)bounds.Width, (Int32)bounds.Height, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(target);
                dc.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
            }
            rtb.Render(dv);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            using (Stream stm = File.Create(filename))
            {
                png.Save(stm);
            }
        }

        private async void MainCamera_LiveViewUpdated(EOSDigital.API.Camera sender, Stream img)
        {
            try
            {
                if (currentchromaKeyBG != "")
                {
                    Bitmap b = new Bitmap(img);
                    currentwidth = b.Width;
                    currentHeight = b.Height;
                    if (currentIdentification == 3)
                    {
                        b = ImageUtility.CropBitmap(b, left, 0, width, height);
                    }
                    else if (currentIdentification == 1)
                    {

                        if (portrait)
                        {
                            b = ImageUtility.CenterCrop(b, width, b.Height);
                        }
                    }

                    ImageFilterManager imageFilter = new ImageFilterManager();
                    if (!Globals.gswithoutframe)
                    {
                        b = imageFilter.RemoveBackgroundLiveView(b, currentchromaKeyBG);
                    }

                    b.SetResolution(640, 480);
                    var ms = new MemoryStream();
                    b.Save(ms, ImageFormat.Bmp);
                    using (WrapStream s = new WrapStream(ms))
                    {
                        img.Position = 0;
                        BitmapImage EvfImage = new BitmapImage();
                        EvfImage.BeginInit();
                        EvfImage.StreamSource = s;
                        EvfImage.CacheOption = BitmapCacheOption.None;
                        EvfImage.EndInit();
                        EvfImage.Freeze();
                        WaitChromaKey.Set();
                        await System.Windows.Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);

                    }

                }
                else
                {
                    if (currentIdentification == 3)
                    {
                        Bitmap b = new Bitmap(img);
                        currentwidth = b.Width;
                        currentHeight = b.Height;
                        b = ImageUtility.CropBitmap(b, left, 0, width, height);
                        var ms = new MemoryStream();
                        b.Save(ms, ImageFormat.Bmp);
                        using (WrapStream s = new WrapStream(ms))
                        {
                            img.Position = 0;
                            BitmapImage EvfImage = new BitmapImage();
                            EvfImage.BeginInit();
                            EvfImage.StreamSource = s;
                            EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                            EvfImage.EndInit();
                            EvfImage.Freeze();
                            WaitChromaKey.Set();
                            await System.Windows.Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);
                        }
                    }
                    else if (currentIdentification == 1)
                    {

                        if (portrait)
                        {
                            Bitmap b = new Bitmap(img);
                            currentwidth = b.Width;
                            currentHeight = b.Height;
                            b = ImageUtility.CropBitmap(b, left, 0, width, b.Height);
                            var ms = new MemoryStream();
                            b.Save(ms, ImageFormat.Bmp);
                            using (WrapStream s = new WrapStream(ms))
                            {
                                img.Position = 0;
                                BitmapImage EvfImage = new BitmapImage();
                                EvfImage.BeginInit();
                                EvfImage.StreamSource = s;
                                EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                EvfImage.EndInit();
                                EvfImage.Freeze();
                                WaitChromaKey.Set();
                                await System.Windows.Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);

                            }
                            b.Dispose();
                        }
                        else
                        {
                            using (WrapStream s = new WrapStream(img))
                            {
                                img.Position = 0;
                                BitmapImage EvfImage = new BitmapImage();
                                EvfImage.BeginInit();
                                EvfImage.StreamSource = s;
                                EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                EvfImage.EndInit();
                                EvfImage.Freeze();
                                WaitChromaKey.Set();
                                await System.Windows.Application.Current.Dispatcher.BeginInvoke(SetImageAction, DispatcherPriority.Normal, EvfImage);
                            }
                        }



                    }
                    else
                    {
                        using (WrapStream s = new WrapStream(img))
                        {
                            img.Position = 0;
                            BitmapImage EvfImage = new BitmapImage();
                            EvfImage.BeginInit();
                            EvfImage.StreamSource = s;
                            EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                            EvfImage.EndInit();
                            EvfImage.Freeze();
                            WaitChromaKey.Set();
                            await System.Windows.Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);

                        }
                    }

                }

            }
            catch (Exception ex) {  }
        }
        private void RemoveFiles()
        {

            String currDir = Globals.currDir;
            var tempView = currDir + $"\\{_imageFolder}";
            var tempFinal = currDir + $"\\{_finalFolder}";
            DirectoryInfo directory = new DirectoryInfo(tempView);
            foreach (var item in directory.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }


            DirectoryInfo directoryFinalOriginal = new DirectoryInfo(tempFinal + "\\Original");
            foreach (var item in directoryFinalOriginal.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryColor = new DirectoryInfo(Globals._tempFolder + "\\Color");
            foreach (var item in directoryTemporaryColor.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporarySepia = new DirectoryInfo(Globals._tempFolder + "\\Sepia");
            foreach (var item in directoryTemporarySepia.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryBW = new DirectoryInfo(Globals._tempFolder + "\\BLACKWHITE");
            foreach (var item in directoryTemporaryBW.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporary = new DirectoryInfo(Globals._tempFolder);
            foreach (var item in directoryTemporary.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryView = new DirectoryInfo(Globals._ViewTempFolder);
            foreach (var item in directoryTemporaryView.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryViewColor = new DirectoryInfo(Globals._ViewTempFolder + "\\Color");
            foreach (var item in directoryTemporaryViewColor.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryViewSepia = new DirectoryInfo(Globals._ViewTempFolder + "\\Sepia");
            foreach (var item in directoryTemporaryViewSepia.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryViewBW = new DirectoryInfo(Globals._ViewTempFolder + "\\BLACKWHITE");
            foreach (var item in directoryTemporaryViewBW.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            //DirectoryInfo directoryTemporaryViewBW = new DirectoryInfo(Globals._ViewTempFolder + "\\BW");
            //foreach (var item in directoryTemporaryViewBW.GetFiles())
            //{
            //    //System.GC.Collect();
            //    //System.GC.WaitForPendingFinalizers();
            //    File.Delete(item.FullName);
            //}

            DirectoryInfo directoryTemporaryViewOneColor = new DirectoryInfo(Globals._ViewTempFolder + "\\OneColor");
            foreach (var item in directoryTemporaryViewOneColor.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryViewPopart = new DirectoryInfo(Globals._ViewTempFolder + "\\Popart");
            foreach (var item in directoryTemporaryViewPopart.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryTempOneColor = new DirectoryInfo(Globals._tempFolder + "\\OneColor");
            foreach (var item in directoryTemporaryTempOneColor.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            DirectoryInfo directoryTemporaryTempPopart = new DirectoryInfo(Globals._tempFolder + "\\Popart");
            foreach (var item in directoryTemporaryTempPopart.GetFiles())
            {
                //System.GC.Collect();
                //System.GC.WaitForPendingFinalizers();
                File.Delete(item.FullName);
            }

            //DirectoryInfo directoryFinal = new DirectoryInfo(tempFinal);
            //foreach (var item in directoryFinal.GetFiles())
            //{

            //    System.GC.Collect();
            //    System.GC.WaitForPendingFinalizers();
            //    File.Delete(item.FullName);
            //}

        }

        private void CallFilterPage()
        {
            //DisposeCamera();
            var viewModel = (TakeWebcamCameraViewModel)DataContext;
            if (viewModel.GoToFiltre.CanExecute(null))
                viewModel.GoToFiltre.Execute(null);
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        
                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                               
                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                timerCountdown.Stop();
                                
                                StopCamera();
                                if (imageBitmap != null)
                                {
                                    imageBitmap.Dispose();
                                }

                                GC.SuppressFinalize(this);
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                var viewModel = (TakeWebcamCameraViewModel)DataContext;
                                /*if (viewModel.GoToConnexionClient.CanExecute(null))
                                    viewModel.GoToConnexionClient.Execute(null);*/
                                
                                Globals._FlagConfig = "client";
                                
                                if (Globals.ScreenType == "SPHERIK")
                                {
                                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                        viewModel.GoToClientSumarySpherik.Execute(null);
                                }
                                else if (Globals.ScreenType == "DEFAULT")
                                {
                                    if (viewModel.GoToClientSumary.CanExecute(null))
                                        viewModel.GoToClientSumary.Execute(null);
                                }

                                //LoadingPanel.ClosePanel();
                                //lbl_loading.Visibility = Visibility.Hidden;
                            }));
                        });
                        
                        
                        break;
                    case Key.F2:

                        Task.Factory.StartNew(() =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {

                                LoadingPanel.ShowPanel();
                                //lbl_loading.Visibility = Visibility.Visible;

                            }));
                            Thread.Sleep(100);

                        }).ContinueWith(task =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                timerCountdown.Stop();
                                StopCamera();
                                if (imageBitmap != null)
                                {
                                    imageBitmap.Dispose();
                                }

                                GC.SuppressFinalize(this);
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                Globals._FlagConfig = "admin";
                                var viewModel2 = (TakeWebcamCameraViewModel)DataContext;
                                if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                    viewModel2.GoToConnexionConfig.Execute(null);
                            }));
                        });
                        
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            try
            {
                SizeF f = getSizePortrait(1200,800,700);
                //GetEvent();
                //if(currentIdentification!=1)
                //{
                    //Globals.getDynamicPosition();
                //}
                
                GetVideoDevices();
            if (!found)
            {
                this.Focusable = false;
                scrollNoCamera.Focusable = true;
                Keyboard.Focus(scrollNoCamera);
            }
            else
            {
                this.Focusable = true;
                Keyboard.Focus(this);
            }
           
            //this.Focusable = true;
            //Keyboard.Focus(this);
            
            currentBackground = GetBackground();
            if (currentBackground != "")
            {
                Globals._currentBackground = currentBackground;
            }
            else if (Globals._currentBackground != "")
            {
                currentBackground = Globals._currentBackground;
            }
            if (currentchromaKeyBG != "")
            {
                Globals._currentChromakeyBG = currentchromaKeyBG;
            }
            else if (Globals._currentChromakeyBG != "")
            {
                currentchromaKeyBG = Globals._currentChromakeyBG;
            }
                //lbl_CountDown.VerticalAlignment = VerticalAlignment.Center;
                var workiningDir = Globals._tmpDir;
            var files = Directory.GetFiles(workiningDir);
            if (files.Length > 0)
                foreach (var file in files)
                    File.Delete(file);
            if (currentIdentification == 1)
            {
                    string offer = _inimanager.GetSetting("CADRE", "offer");
                   
                    if (!string.IsNullOrEmpty(offer))
                    {
                        _offer = Convert.ToInt32(offer);
                    }
                    using (System.Drawing.Image img = System.Drawing.Image.FromFile(currentBackground))
                    {
                        imageBitmap = new Bitmap(img);
                        widthportrait = imageBitmap.Width;
                    }
                    if(_offer > 0)
                    {
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            int x = (int)SystemParameters.PrimaryScreenWidth;
                            int y = (int)SystemParameters.PrimaryScreenHeight;
                            int taille = (y *98)/100;
                            int marginWidth = (x - taille) / 2;
                            int marginHeight = (y - taille) / 2;
                            
                            LVCanvas.Margin = new Thickness(marginWidth, marginHeight -10, marginWidth, marginHeight);

                            //WindowsFormsHost1.Margin = new Thickness(marginWidth, marginHeight-100, marginWidth, marginHeight);
                            int countHeight = (y - 220) / 2;
                            int gifHeight = (y - 100) / 2;
                            int gifWidth = (x - 110) / 2;
                            int countWidth = (x - 150) / 2;
                            lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                            GIFCtrl.Margin = new Thickness(gifWidth , gifHeight, gifWidth, gifHeight);
                            //lbl_CountDown.Margin = new Thickness(marginWidth, -10, marginWidth, 0);
                            //GIFCtrl.Margin = new Thickness(marginWidth, -10, marginWidth, 0);
                            CanvasImage.Height = taille; //930;
                            CanvasImage.Width = taille; //620
                            LVCanvas.Height = taille;
                            LVCanvas.Width = taille;



                            CanvasMaskImage.Height = taille; //930;
                            CanvasMaskImage.Width = taille; //620
                            CanvasMaskImage.Visibility = Visibility.Hidden;


                            //WindowsFormsHost1.Width = taille;
                            //WindowsFormsHost1.Height = taille;
                            centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight-10);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            int x = (int)SystemParameters.PrimaryScreenWidth;
                            int y = (int)SystemParameters.PrimaryScreenHeight;
                            int taille = (y * 98) / 100;
                            int marginWidth = (x - taille) / 2;
                            int marginHeight = (y - taille) / 2;
                            LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);

                            //WindowsFormsHost1.Margin = new Thickness(marginWidth, marginHeight - 100, marginWidth, marginHeight);
                            int countHeight = (y - 220) / 2;
                            int gifHeight = (y - 100) / 2;
                            int gifWidth = (x - 110) / 2;
                            int countWidth = (x - 150) / 2;
                            lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                            GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                            CanvasImage.Height = taille; //930;
                            CanvasImage.Width = taille; //620
                            LVCanvas.Height = taille;
                            LVCanvas.Width = taille;



                            CanvasMaskImage.Height = taille; //930;
                            CanvasMaskImage.Width = taille; //620
                            CanvasMaskImage.Visibility = Visibility.Hidden;

                            //WindowsFormsHost1.Width = taille;
                            //WindowsFormsHost1.Height = taille;

                            int xx = x - marginWidth;
                            int yy = y - marginHeight-10;
                            centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, xx, yy);
                        }
                    }
                    else
                    {
                        if (imageBitmap.Width < imageBitmap.Height)
                        {
                            Globals.portrait = true;
                            portrait = true;
                            if (Globals.ScreenType == "SPHERIK")
                            {
                                int x = (int)SystemParameters.PrimaryScreenWidth;
                                int y = (int)SystemParameters.PrimaryScreenHeight;
                                int currentwidth = 0;
                                int currentheight = 0;
                               
                                Bitmap bgImage = new Bitmap(currentBackground);
                                int widthMultishoot = bgImage.Width;
                                int heightMultishoot = bgImage.Height;
                                bgImage.Dispose();
                               
                                int width_ = y - 100;
                                int _width = Globals._shootDynamic[0].width;
                                int _height = Globals._shootDynamic[0].height;
                                SizeF theSize = getSizePaysageByWidth(_width, _height, width_);
                                currentheight = Convert.ToInt32(theSize.Height);
                                currentwidth = Convert.ToInt32(theSize.Width);
                                int marginWidth = (x - currentwidth) / 2;
                                int marginHeight = (y - currentheight) / 2;
                                LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);
                                int countHeight = (y - 220) / 2;
                                int gifHeight = (y - 100) / 2;
                                int gifWidth = (x - 110) / 2;
                                int countWidth = (x - 150) / 2;
                                lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                                GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);




                                CanvasImage.Height = currentheight;
                                CanvasImage.Width = currentwidth;
                                CanvasMaskImage.Height = currentheight;
                                CanvasMaskImage.Width = currentwidth;
                                LVCanvas.Height = currentheight;
                                LVCanvas.Width = currentwidth;
                                currentCanvasWidth = currentwidth;
                                currentCanvasHeight = currentheight;
                                
                              //  CanvasImage.Margin = new Thickness(0, 0, 0, 0);
                              //  CanvasMaskImage.Margin = new Thickness(-10, 0, 0, 0);
                                centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight - 10);

                            }
                            else if (Globals.ScreenType == "DEFAULT")
                            {
                                int x = (int)SystemParameters.PrimaryScreenWidth;
                                int y = (int)SystemParameters.PrimaryScreenHeight;
                                int currentwidth = 0;
                                int currentheight = 0;

                                Bitmap bgImage = new Bitmap(currentBackground);
                                int widthMultishoot = bgImage.Width;
                                int heightMultishoot = bgImage.Height;
                                bgImage.Dispose();

                                int width_ = y - 100;
                                int _width = Globals._shootDynamic[0].width;
                                int _height = Globals._shootDynamic[0].height;
                                SizeF theSize = getSizePaysageByWidth(_width, _height, width_);
                                currentheight = Convert.ToInt32(theSize.Height);
                                currentwidth = Convert.ToInt32(theSize.Width);
                                int marginWidth = (x - currentwidth) / 2;
                                int marginHeight = (y - currentheight) / 2;
                                LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);
                                int countHeight = (y - 220) / 2;
                                int gifHeight = (y - 100) / 2;
                                int gifWidth = (x - 110) / 2;
                                int countWidth = (x - 150) / 2;
                                lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                                GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);




                                CanvasImage.Height = currentheight;
                                CanvasImage.Width = currentwidth;
                                CanvasMaskImage.Height = currentheight;
                                CanvasMaskImage.Width = currentwidth;
                                LVCanvas.Height = currentheight;
                                LVCanvas.Width = currentwidth;
                                currentCanvasWidth = currentwidth;
                                currentCanvasHeight = currentheight;

                                //  CanvasImage.Margin = new Thickness(0, 0, 0, 0);
                                //  CanvasMaskImage.Margin = new Thickness(-10, 0, 0, 0);
                                centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight - 10);
                            }
                        }
                        else
                        {
                            if (Globals.ScreenType == "SPHERIK")
                            {
                                int x = (int)SystemParameters.PrimaryScreenWidth;
                                int y = (int)SystemParameters.PrimaryScreenHeight;
                                int currentwidth = 0;
                                int currentheight = 0;
                                //int widthMultishoot = 360;
                                //int heightMultishoot = 540;
                                //if (Globals._shootDynamic.Length > 0)
                                //{
                                //    widthMultishoot = Globals._shootDynamic[0].width;
                                //    heightMultishoot = Globals._shootDynamic[0].height;
                                //}
                                Bitmap bgImage = new Bitmap(currentBackground);
                                int widthMultishoot = bgImage.Width;
                                int heightMultishoot = bgImage.Height;
                                bgImage.Dispose();
                                //int height_ = y - 100;
                                //if (currentheight > 800)
                                //{
                                //    SizeF theSize = new SizeF(currentwidth * .90f, currentheight * .90f);
                                //    currentheight = Convert.ToInt32(theSize.Height);
                                //    currentwidth = Convert.ToInt32(theSize.Width);
                                //}
                                int width_ = y - 100;
                                //if (currentwidth > width_)
                                //{
                                SizeF theSize = getSizePaysageByWidth(widthMultishoot, heightMultishoot, width_);
                                currentheight = Convert.ToInt32(theSize.Height);
                                currentwidth = Convert.ToInt32(theSize.Width);
                                int marginWidth = (x - currentwidth) / 2;
                                int marginHeight = (y - currentheight) / 2;
                                LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);
                                int countHeight = (y - 220) / 2;
                                int gifHeight = (y - 100) / 2;
                                int gifWidth = (x - 110) / 2;
                                int countWidth = (x - 150) / 2;
                                lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                                GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);



                                currentCanvasWidth = currentwidth;
                                currentCanvasHeight = currentheight;
                                CanvasImage.Height = currentheight;
                                CanvasImage.Width = currentwidth;

                                LVCanvas.Height = currentheight;
                                LVCanvas.Width = currentwidth;

                                CanvasMaskImage.Height = currentheight;
                                CanvasMaskImage.Width = currentwidth;
                                //  CanvasImage.Margin = new Thickness(0, 0, 0, 0);
                                //  CanvasMaskImage.Margin = new Thickness(-10, 0, 0, 0);
                                centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight - 10);
                            }
                            else
                            if (Globals.ScreenType == "DEFAULT")
                            {
                                int x = (int)SystemParameters.PrimaryScreenWidth;
                                int y = (int)SystemParameters.PrimaryScreenHeight;
                                int currentwidth = 0;
                                int currentheight = 0;
                                //int widthMultishoot = 360;
                                //int heightMultishoot = 540;
                                //if (Globals._shootDynamic.Length > 0)
                                //{
                                //    widthMultishoot = Globals._shootDynamic[0].width;
                                //    heightMultishoot = Globals._shootDynamic[0].height;
                                //}
                                Bitmap bgImage = new Bitmap(currentBackground);
                                int widthMultishoot = bgImage.Width;
                                int heightMultishoot = bgImage.Height;
                                bgImage.Dispose();
                                //int height_ = y - 100;
                                //if (currentheight > 800)
                                //{
                                //    SizeF theSize = new SizeF(currentwidth * .90f, currentheight * .90f);
                                //    currentheight = Convert.ToInt32(theSize.Height);
                                //    currentwidth = Convert.ToInt32(theSize.Width);
                                //}
                                int width_ = y - 100;
                                //if (currentwidth > width_)
                                //{
                                SizeF theSize = getSizePaysageByWidth(widthMultishoot, heightMultishoot, width_);
                                currentheight = Convert.ToInt32(theSize.Height);
                                currentwidth = Convert.ToInt32(theSize.Width);
                                int marginWidth = (x - currentwidth) / 2;
                                int marginHeight = (y - currentheight) / 2;
                                LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);
                                int countHeight = (y - 220) / 2;
                                int gifHeight = (y - 100) / 2;
                                int gifWidth = (x - 110) / 2;
                                int countWidth = (x - 150) / 2;
                                lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                                GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                                currentCanvasWidth = currentwidth;
                                currentCanvasHeight = currentheight;



                                CanvasImage.Height = currentheight;
                                CanvasImage.Width = currentwidth;

                                LVCanvas.Height = currentheight;
                                LVCanvas.Width = currentwidth;

                                CanvasMaskImage.Height = currentheight;
                                CanvasMaskImage.Width = currentwidth;
                                //  CanvasImage.Margin = new Thickness(0, 0, 0, 0);
                                //  CanvasMaskImage.Margin = new Thickness(-10, 0, 0, 0);
                                centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight - 10);
                            }
                        }
                        Bitmap bmp = null;
                        using (System.Drawing.Image img = System.Drawing.Image.FromFile(currentBackground))
                        {
                            bmp = new Bitmap(img);
                            bmp = ImageUtility.ResizeImage(bmp, currentCanvasWidth, currentCanvasHeight);
                        }
                        
                        if(portrait == false)
                        {
                            CanvasMaskImage.Source = ImageUtility.convertBitmapToBitmapImage(bmp);//new BitmapImage(new Uri(currentBackground, UriKind.RelativeOrAbsolute));
                        }
                        
                    }
              
                }
            else if(currentIdentification == 4 || currentIdentification == 2)
                {
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;


                        int marginWidth = (x - 930) / 2;
                        int marginHeight = (y - 620) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);

                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = 620; //930;
                        CanvasImage.Width = 930; //620
                        LVCanvas.Height = 620;
                        LVCanvas.Width = 930;

                        

                        CanvasMaskImage.Height = 620; //930;
                        CanvasMaskImage.Width = 910; //620

                        
                        
                        //minWidth = marginWidth - marginWidth/4;
                        //minHeight = 620 * minWidth / 930;
                        //int xx = x - marginWidth ; 
                        //int yy = -(y - minHeight) + 2*(marginHeight - 20)  + minHeight/4;
                        //if(currentIdentification == 2)
                        //{
                        //      centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight - 10);
                        //}

                    }
                    else
                        if (Globals.ScreenType == "DEFAULT")
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int marginWidth = (x - 930) / 2;
                        int marginHeight = (y - 620) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);

                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = 620; //930;
                        CanvasImage.Width = 930; //620
                        LVCanvas.Height = 620;
                        LVCanvas.Width = 930;

                       

                        CanvasMaskImage.Height = 620; //930;
                        CanvasMaskImage.Width = 910; //620

                        

                      //  WindowsFormsHost1.Width = 930;
                      //  WindowsFormsHost1.Height = 620;

                        minWidth = marginWidth - marginWidth / 4;
                        minHeight = 620 * minWidth / 930;
                        int xx = x - marginWidth;
                        int yy = -(y - minHeight-10) + 2 * marginHeight;
                       // centerCanvas(minWidth, minHeight, xx, yy);
                    }
                }
            else if (currentIdentification == 3)
            {
                    if (Globals.ScreenType == "SPHERIK")
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;

                        int taille = (y * 98) / 100;
                        int marginWidth = (x - taille) / 2;
                        int marginHeight = (y - taille) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);

                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = taille; //930;
                        CanvasImage.Width = taille; //620
                        LVCanvas.Height = taille;
                        LVCanvas.Width = taille;

                       

                        CanvasMaskImage.Height = taille; //930;
                        CanvasMaskImage.Width = taille; //620
                        CanvasMaskImage.Visibility = Visibility.Hidden;
                        

                     //   WindowsFormsHost1.Width = taille;
                      //  WindowsFormsHost1.Height = taille;
                        centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight-10);
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int taille = (y * 98) / 100;
                        int marginWidth = (x - taille) / 2;
                        int marginHeight = (y - taille) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);

                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Width = taille; //620
                        LVCanvas.Height = taille;
                        LVCanvas.Width = taille;
                        
                        CanvasMaskImage.Height = taille; //930;
                        CanvasMaskImage.Width = taille; //620
                        CanvasMaskImage.Visibility = Visibility.Hidden;
                      //  
                      //  WindowsFormsHost1.Width = taille;
                      //  WindowsFormsHost1.Height = taille;

                        int xx = x - marginWidth;
                        int yy = y - marginHeight-10;
                        centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, xx, yy);
                    }
            }
            else if (currentIdentification == 0)
            {
                if (Globals.ScreenType == "SPHERIK")
                {

                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int taille = (y * 98) / 100;
                        int marginWidth = (x - taille) / 2;
                        int marginHeight = (y - taille) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);


                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Width = taille;
                        CanvasImage.Height = taille;
                        LVCanvas.Height = taille;
                        LVCanvas.Width = taille;
                        
                     //   WindowsFormsHost1.Height = taille;
                      //  WindowsFormsHost1.Width = taille;
                        centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight-10);
                        
                    }
                    else if (Globals.ScreenType == "DEFAULT")
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int marginWidth = (x - 950) / 2;
                        int marginHeight = (y - 620) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight-10, marginWidth, marginHeight);
                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = 620; //930;
                        CanvasImage.Width = 950; //620
                        LVCanvas.Height = 620;
                        LVCanvas.Width = 950;
                        
                        CanvasMaskImage.Height = 620; //930;
                        CanvasMaskImage.Width = 930; //620
                        
                     //   WindowsFormsHost1.Height = 620;
                      //  WindowsFormsHost1.Width = 950;
                        centerCanvas((int)LVCanvas.Width, (int)LVCanvas.Height, marginWidth, marginHeight-10);
                    }
                }
            // CVShot1.Margin = new Thickness { Left = CanvasImage.Margin.Left + 15, Top = CanvasImage.Margin.Top };

            if (CheckIfIsMirrored())
            {
                ScaleTransform scale = new ScaleTransform() { ScaleX = -1 };
                CanvasImage.LayoutTransform = scale;
            }
                StartCamera();
                
                Thread.Sleep(1000);
                LaunchTimerCountdown();
                left = Convert.ToInt32(CanvasImage.Margin.Left);
            top = Convert.ToInt32(LVCanvas.Margin.Top);
            width = Convert.ToInt32(LVCanvas.Width);
            height = Convert.ToInt32(LVCanvas.Height);
               // startLiveView();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                LoadingPanel.ClosePanel();
            }));


        }
        

        private void centerCanvas(int width, int height,int marginWidth, int marginHeight,int defaultWidth = 930)
        {
            
            int yBetweenMin = (marginWidth - defaultWidth - width) / 2;
            int y = (int)SystemParameters.PrimaryScreenHeight;
            int space_min = y - marginHeight;
            if(nbShoot > 1)
            {
                if (height == width)
                {
                    int total_minHeight = nbShoot * height + (nbShoot - 1) * yBetweenMin;

                    if (total_minHeight > space_min)
                    {
                        height = (space_min - (nbShoot - 1) * yBetweenMin - marginHeight) / nbShoot;
                        width = height;
                    }
                }
                else
                {
                    int total_minHeight = nbShoot * height + (nbShoot - 1) * yBetweenMin;

                    if (total_minHeight > space_min)
                    {
                        var tmp = height;
                        height = (space_min - (nbShoot - 1) * yBetweenMin - marginHeight) / nbShoot;
                        width = (width * height) / tmp;
                    }
                }
            }
            

            List<Canvas> list = new List<Canvas>();
            list.Add(CVShot1);
            list.Add(CVShot2);
            list.Add(CVShot3);
            list.Add(CVShot4);


            if (previous_height == 0)
            {
                list[0].Height = height;
                previous_height = (int)list[0].Height;
            }
            else
            {
                if (previous_height != height)
                {
                    list[iteration].Height = height;
                    previous_height = (int)list[iteration].Height;
                }
                else
                {
                    list[iteration].Height = height;
                }
            }
            if (previous_width == 0)
            {

                list[0].Width = width;
                previous_width = (int)list[0].Width;
            }
            else
            {
                if (previous_width != width)
                {
                    list[iteration].Width = width;
                    previous_width = (int)list[iteration].Width;
                }else
                {
                    list[iteration].Width = width;
                }
            }


            if (iteration > 0)
            {
                for(int i = 0; i <= iteration; i++)
                {
                    if (list[i].Height == list[i].Width)
                    {
                        list[i].Height = list[iteration].Height;
                        list[i].Width = list[i].Height;
                    }
                    else
                    {
                        list[i].Width = list[i].Width / list[i].Height * list[iteration].Height;
                        list[i].Height = list[iteration].Height;                        
                    }
                }               

            }


            CVShot1.Margin = new Thickness(marginWidth + yBetweenMin, marginHeight, 0, 0);
            CVShot2.Margin = new Thickness(marginWidth + yBetweenMin, marginHeight + previous_height + yBetweenMin, 0, 0);
            CVShot3.Margin = new Thickness(marginWidth + yBetweenMin, marginHeight + 2 * previous_height + (2 * yBetweenMin), 0, 0);
            CVShot4.Margin = new Thickness(marginWidth + yBetweenMin, marginHeight + 3 * previous_height + (3 * yBetweenMin), 0, 0);
            iteration++;

        }

        private void getAllActivatedFilter()
        {
            string sepia = "Sepia";
            string color = "COLOR";
            string bw = "BlackWhite";
            var inimanager = new INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            if (Globals.EventChosen == 0)
            {
                int _sepia = Convert.ToInt32(inimanager.GetSetting("CADRE", "Sepia"));
                int _bw = Convert.ToInt32(inimanager.GetSetting("CADRE", "BlackWhite"));
                int _color = Convert.ToInt32(inimanager.GetSetting("CADRE", "COLOR"));
                if (_sepia > 0)
                {
                    filtres.Add(sepia.ToUpper());
                }
                if (_bw > 0)
                {
                    filtres.Add(bw.ToUpper());
                }
                if (_color > 0)
                {
                    filtres.Add(color.ToUpper());
                }
            }
            else if (Globals.EventChosen==1)
            {
                int _sepia = Convert.ToInt32(inimanager.GetSetting("STRIP", "Sepia"));
                int _bw = Convert.ToInt32(inimanager.GetSetting("STRIP", "BlackWhite"));
                int _color = Convert.ToInt32(inimanager.GetSetting("STRIP", "COLOR"));
                if (_sepia > 0)
                {
                    filtres.Add(sepia);
                }
                if (_bw > 0)
                {
                    filtres.Add(bw);
                }
                if (_color > 0)
                {
                    filtres.Add(color);
                }
            }
            else if (Globals.EventChosen==2)
            {
                int _sepia = Convert.ToInt32(inimanager.GetSetting("POLAROID", "Sepia"));
                int _bw = Convert.ToInt32(inimanager.GetSetting("POLAROID", "BlackWhite"));
                int _color = Convert.ToInt32(inimanager.GetSetting("POLAROID", "COLOR"));
                if (_sepia > 0)
                {
                    filtres.Add(sepia);
                }
                if (_bw > 0)
                {
                    filtres.Add(bw);
                }
                if (_color > 0)
                {
                    filtres.Add(color);
                }
            }
            else if (Globals.EventChosen==3)
            {
                int _sepia = Convert.ToInt32(inimanager.GetSetting("MULTISHOOT", "Sepia"));
                int _bw = Convert.ToInt32(inimanager.GetSetting("MULTISHOOT", "BlackWhite"));
                int _color = Convert.ToInt32(inimanager.GetSetting("MULTISHOOT", "COLOR"));
                if (_sepia > 0)
                {
                    filtres.Add(sepia);
                }
                if (_bw > 0)
                {
                    filtres.Add(bw);
                }
                if (_color > 0)
                {
                    filtres.Add(color);
                }
            }
            //var inimanager = new INIFileManager($"{Globals.EventAssetFolder()}\\Config.ini");
            //filtres = inimanager.GetKeysByValue("FILTRE", "1");
        }

        private bool CheckIfIsMirrored()
        {
            var inimanager = new Managers.INIFileManager(Globals._iniFile);
            return inimanager.CheckIfIsMirrored();
        }

        private void createPhotoOverlayed()
        {
            try
            {
                string cadre = Globals._cadre;
                var temp = Globals.imageFolder;
                int index = 0;

                Bitmap imageOverlay = new Bitmap(cadre);
                ImageFilterManager imageManager = new ImageFilterManager();
                string[] imageFiles = Directory.GetFiles(temp);
                var originalImaePath = imageFiles[0];
                /*  chromakey */
                var chromaKeyBG = GetChromaKeyBG();
                if (chromaKeyBG != "")
                {
                    var input = new Bitmap(originalImaePath);
                    var result = imageManager.RemoveBackground(input, chromaKeyBG);
                    if (result != null)
                    {
                        File.Delete(originalImaePath);
                        result.Dispose();
                    }
                }
                /*  */
                imageManager.Applyfilter(imageFiles[0], filtres);
                imageFiles = Directory.GetFiles(temp);
                foreach (string image in imageFiles)
                {
                    index++;
                    Bitmap baseImage = new Bitmap(image);
                    string filename = _finalFolder + "\\" + CreateFileName(index);
                    List<Bitmap> images = new List<Bitmap>();
                    Bitmap finalImage = new Bitmap(imageOverlay.Width, imageOverlay.Height);
                    //imageOverlay = ImageUtility.ResizeImage(imageOverlay, baseImage.Width, baseImage.Height);
                    images.Add(baseImage);
                    images.Add(imageOverlay);
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        //set background color
                        g.Clear(System.Drawing.Color.Black);

                        //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                        int offset = 0;
                        foreach (Bitmap imageTemp in images)
                        {
                            g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, 0, imageOverlay.Width, imageOverlay.Height));
                        }
                    }
                    //Saving image...
                    finalImage.Save(filename, ImageFormat.Jpeg);
                }

            }
            catch (Exception ex) { }
            finally { WaitEvent.Set(); }
        }


        private void addImageBrush()
        {
            ImageBrush imgBrush = new ImageBrush();
            var fileName = Globals.LoadBG(Selfizee.Models.Enum.TypeFond.ForTakephoto);
            if (!File.Exists(fileName))
            {
                fileName = "C:\\EVENTS\\Assets\\Default\\Background\\background.jpg";
            }
            Bitmap bmp = null;

            using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
            {
                bmp = new Bitmap(img, new System.Drawing.Size(1920, 1080));
            }
            imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp); //new BitmapImage(new Uri(fileName, UriKind.Relative));
            imgBrush.Stretch = Stretch.Fill;
            dockGrid.Background = imgBrush;

        }

        private int getNbShoot()
        {
            IniUtility ini = new IniUtility(EventIni);
            if (Globals.customFormat)
            {
                switch (Globals.EventChosen)
                {
                    case 0:
                        currentIdentification = 1;
                        countdownManaged = 2;
                        break;
                    case 1:
                        currentIdentification = 2;
                        countdownManaged = 4;
                        break;
                    case 2:
                        currentIdentification = 3;
                        countdownManaged = 2;
                        break;
                    case 3:
                        currentIdentification = 4;
                        countdownManaged = Convert.ToInt32(ini.Read("countdown", "MULTISHOOT")) + 1;
                        break;
                }
                return Globals.customNbshoot;

            }
            else
            {

                var files = Directory.GetFiles(Globals._allEvents, "*.png");
                string text = System.IO.File.ReadAllText(Globals._iniFile);
                string[] sections = ini.GetSections(text);
                //if (sections.Length == 1)
                //{

                //string eventName = System.IO.Path.GetFileNameWithoutExtension(Globals.EventChosen).ToLower();
                switch (Globals.EventChosen)
                {
                    case 0:
                        countdownManaged = Convert.ToInt32(ini.Read("countdown", "CADRE")) + 1;
                        break;
                    case 1:
                        countdownManaged = Convert.ToInt32(ini.Read("countdown", "STRIP")) + 1;
                        break;
                    case 2:
                        countdownManaged = Convert.ToInt32(ini.Read("countdown", "POLAROID")) + 1;
                        break;
                    case 3:
                        countdownManaged = Convert.ToInt32(ini.Read("countdown", "MULTISHOOT")) + 1;
                        break;
                }
                //}
                switch (Globals.EventChosen)
                {
                    case 0:
                        currentIdentification = 1;
                        //countdownManaged = 6;
                        return 1;
                    case 1:
                        currentIdentification = 2;
                        //countdownManaged = 6;
                        return 3;
                    case 2:
                        currentIdentification = 3;
                        //countdownManaged = 4;
                        return 1;
                    case 3:
                        currentIdentification = 4;
                        String code = "";
                        int nbShoot = 0;
                        //IniUtility iniMultishoot = new IniUtility(Globals._iniMultishoot);

                        //if (ini.KeyExists("code", "MULTISHOOT"))
                        //{
                        //    code = ini.Read("code", "MULTISHOOT");
                        //}
                        //string section = "MULTISHOOT" + code.ToUpper();
                        //if (iniMultishoot.KeyExists("nbShoot", section))
                        //{
                        //    //string strNbShoot = iniInit.Read("nbShoot", section);
                        //    //countdownManaged = Convert.ToInt32(iniMultishoot.Read("countdown", eventName.ToUpper())) + 1;
                        //    int _nbShoot = 0;

                        //    nbShoot = Convert.ToInt32(iniMultishoot.Read("nbShoot", section));
                        //}
                        bool result = bool.Parse(_inimanager.GetSetting("SHOOT", "shoot1Activated"));
                        if (result)
                        {
                            nbShoot++;
                        }
                        result = bool.Parse(_inimanager.GetSetting("SHOOT", "shoot2Activated"));
                        if (result)
                        {
                            nbShoot++;
                        }
                        result = bool.Parse(_inimanager.GetSetting("SHOOT", "shoot3Activated"));
                        if (result)
                        {
                            nbShoot++;
                        }
                        result = bool.Parse(_inimanager.GetSetting("SHOOT", "shoot4Activated"));
                        if (result)
                        {
                            nbShoot++;
                        }
                        return nbShoot;
                }
            }

            return 0;
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private Bitmap resizeBackground(Bitmap param,int widthresized,int heightresized, int x, int y, int widthcrop, int heightcrop)
        {
            
            System.Drawing.Bitmap imageBitmap = param;
            imageBitmap = ImageUtility.ResizeImage(imageBitmap, widthresized, heightresized);
            Aurigma.GraphicsMill.Bitmap _background = new Aurigma.GraphicsMill.Bitmap(imageBitmap);
            if (x > 0)
            {
                 _background = (Aurigma.GraphicsMill.Bitmap)ImageUtility.CropBitmap(imageBitmap, x, y, widthcrop, heightcrop);
                _background.Transforms.Resize(widthcrop, heightcrop, ResizeInterpolationMode.High);
            }
            else
            {
                _background = (Aurigma.GraphicsMill.Bitmap)imageBitmap;
                _background.Transforms.Resize(widthcrop, heightcrop, ResizeInterpolationMode.High);
            }
            return (System.Drawing.Bitmap)_background;
        }

        private Bitmap resizeBackground(Bitmap param, int widthresized, int heightresized)
        {
            System.Drawing.Bitmap imageBitmap = param;
            imageBitmap = ImageUtility.ResizeImage(imageBitmap, widthresized, heightresized);
            Aurigma.GraphicsMill.Bitmap _background = new Aurigma.GraphicsMill.Bitmap(imageBitmap);
            return (System.Drawing.Bitmap)_background;
        }

        private void  video_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
            try
            {
                bool resized = false;
                BitmapImage bi;
                int xpos = 0;
                int ypos = 0;
                using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
                {
                    Bitmap b = bitmap;
                    currentwidth = b.Width;
                    currentHeight = b.Height;
                    try
                    {
                        if (currentchromaKeyBG != "")
                        {
                            //
                            if (currentIdentification == 3)
                            {
                                int widthCanvas = 0;
                                int heightCanvas = 0;
                                if (b.Height < 700)
                                {
                                    widthCanvas = b.Height;
                                    heightCanvas = b.Height;
                                }
                                else
                                {
                                    widthCanvas = Convert.ToInt32(700);
                                    heightCanvas = Convert.ToInt32(700);
                                }
                                xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                ypos = Convert.ToInt32(b.Height - heightCanvas) / 2;
                                if (currentGSBackgroound == null)
                                {
                                    Bitmap bg = new Bitmap(currentchromaKeyBG);
                                    int bhHeight = bg.Height;
                                    int bgWidth = bg.Width;
                                    int xpos_ = (bgWidth - bhHeight) / 2;
                                    bg = ImageUtility.CropBitmap(bg, xpos_, 0, bhHeight, bhHeight);
                                    currentGSBackgroound = ImageUtility.ResizeImage(bg, widthCanvas, heightCanvas);
                                    //currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, widthCanvas, heightCanvas);
                                    bg.Dispose();
                                }
                                b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);
                            }
                            else if (currentIdentification == 4)
                            {
                                int width = Globals._shootDynamic[incrementimage - 1].width;
                                int height = Globals._shootDynamic[incrementimage - 1].height;
                                if (width == height)
                                {
                                    int widthCanvas = 0;
                                    int heightCanvas = 0;
                                    if (b.Height < 700)
                                    {
                                        widthCanvas = b.Height;
                                        heightCanvas = b.Height;
                                    }
                                    else
                                    {
                                        widthCanvas = Convert.ToInt32(700);
                                        heightCanvas = Convert.ToInt32(700);
                                    }
                                    xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                    ypos = Convert.ToInt32(b.Height - heightCanvas) / 2; ;
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        int bhHeight = bg.Height;
                                        int bgWidth = bg.Width;
                                        int xpos_ = (bgWidth - bhHeight) / 2;
                                        bg = ImageUtility.CropBitmap(bg, xpos_, 0, bhHeight, bhHeight);
                                        currentGSBackgroound = ImageUtility.ResizeImage(bg, widthCanvas, heightCanvas);
                                        //currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, widthCanvas, heightCanvas);
                                        bg.Dispose();
                                    }
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);
                                }
                                else if (width < height)
                                {
                                    int currwidth = width;
                                    int currheight = height;
                                    if (b.Height > 800)
                                    {
                                        SizeF _ref = getSizePortrait(currwidth, currheight, 800);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    else
                                    {
                                        SizeF _ref = getSizePortrait(currwidth, currheight, b.Height);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }

                                    xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                    ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                    if (ypos < 0)
                                    {
                                        ypos = 0;
                                    }
                                    if (xpos < 0)
                                    {
                                        xpos = 0;
                                    }
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, currwidth, currheight);
                                        bg.Dispose();
                                    }
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                    var ms2 = new MemoryStream();
                                    b.Save(ms2, ImageFormat.Bmp);
                                    using (WrapStream s = new WrapStream(ms2))
                                    {

                                        BitmapImage EvfImage = new BitmapImage();
                                        EvfImage.BeginInit();
                                        EvfImage.StreamSource = s;
                                        EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                        EvfImage.EndInit();
                                        EvfImage.Freeze();
                                        WaitChromaKey.Set();

                                        Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    }
                                    b.Dispose();
                                }
                                else
                                {
                                    int currwidth = width;
                                    int currheight = height;
                                    int referenceWidth = width;
                                    int referenceHeight = height;
                                    if (referenceHeight > b.Height)
                                    {
                                        referenceHeight = b.Height;
                                    }
                                    if (referenceWidth > b.Width)
                                    {
                                        referenceWidth = b.Width;
                                    }
                                    if (b.Width > 930)
                                    {
                                        SizeF _ref = getSizePaysage(currwidth, currheight, 930, referenceHeight);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    else
                                    {
                                        SizeF _ref = getSizePaysage(currwidth, currheight, referenceWidth, referenceHeight);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                    ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                    if (ypos < 0)
                                    {
                                        ypos = 0;
                                    }
                                    if (xpos < 0)
                                    {
                                        xpos = 0;
                                    }
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, currwidth, currheight);
                                        bg.Dispose();
                                    }
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                    //var ms2 = new MemoryStream();
                                    //b.Save(ms2, ImageFormat.Bmp);
                                    //using (WrapStream s = new WrapStream(ms2))
                                    //{

                                    //    BitmapImage EvfImage = new BitmapImage();
                                    //    EvfImage.BeginInit();
                                    //    EvfImage.StreamSource = s;
                                    //    EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                    //    EvfImage.EndInit();
                                    //    EvfImage.Freeze();
                                    //    WaitChromaKey.Set();

                                    //    Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    //}
                                    //b.Dispose();
                                }

                            }
                            else if (currentIdentification == 1)
                            {

                                if (portrait)
                                {
                                    int currwidth = width;
                                    int currheight = height;
                                    if (b.Height > 800)
                                    {
                                        SizeF _ref = getSizePaysageByWidth(currwidth, currheight, 800);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    else
                                    {
                                        SizeF _ref = getSizePaysageByWidth(currwidth, currheight, b.Height);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }

                                    xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                    ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                    if (ypos < 0)
                                    {
                                        ypos = 0;
                                    }
                                    if (xpos < 0)
                                    {
                                        xpos = 0;
                                    }
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, currwidth, currheight);
                                        bg.Dispose();
                                    }
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                    //b = ImageUtility.CenterCrop(b, width, b.Height);
                                }
                                if(_offer == 1)
                                {
                                    int widthCanvas = 0;
                                    int heightCanvas = 0;
                                    if (b.Height < 700)
                                    {
                                        widthCanvas = b.Height;
                                        heightCanvas = b.Height;
                                    }
                                    else
                                    {
                                        widthCanvas = Convert.ToInt32(700);
                                        heightCanvas = Convert.ToInt32(700);
                                    }
                                    xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                    ypos = Convert.ToInt32(b.Height - heightCanvas) / 2; ;
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        int bhHeight = bg.Height;
                                        int bgWidth = bg.Width;
                                        int xpos_ = (bgWidth - bhHeight) / 2;
                                        bg = ImageUtility.CropBitmap(bg, xpos_, 0, bhHeight, bhHeight);
                                        currentGSBackgroound = ImageUtility.ResizeImage(bg, widthCanvas, heightCanvas);
                                        //currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight, xpos, ypos, widthCanvas, heightCanvas);
                                        bg.Dispose();
                                    }
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);
                                }
                                else
                                {
                                    if (b.Width > currentCanvasWidth)
                                    {
                                        xpos = (currentwidth - currentCanvasWidth) / 2;
                                        ypos = (currentHeight - currentCanvasHeight) / 2;
                                        if (xpos < 0)
                                        {
                                            xpos = 0;
                                        }
                                        if (ypos < 0)
                                        {
                                            ypos = 0;
                                        }
                                        if (currentCanvasHeight > b.Height)
                                        {
                                            currentCanvasHeight = b.Height;
                                        }
                                       
                                        currentwidth = currentCanvasWidth;
                                        currentHeight = currentCanvasHeight;
                                        b = ImageUtility.CropBitmap(b, xpos, ypos, currentCanvasWidth, currentCanvasHeight);
                                    }
                                    if (currentGSBackgroound == null)
                                    {
                                        Bitmap bg = new Bitmap(currentchromaKeyBG);
                                        currentGSBackgroound = resizeBackground(bg, currentwidth, currentHeight);
                                        bg.Dispose();
                                    }
                                    //b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                }
                            }
                            else
                            {
                                int currwidth = width;
                                int currheight = height;
                                int referenceWidth = width;
                                int referenceHeight = height;
                                if (referenceHeight > b.Height)
                                {
                                    referenceHeight = b.Height;
                                }
                                if (referenceWidth > b.Width)
                                {
                                    referenceWidth = b.Width;
                                }
                                if (b.Width > 930)
                                {
                                    SizeF _ref = getSizePaysage(currwidth, currheight, 930, referenceHeight);
                                    currheight = Convert.ToInt32(_ref.Height);
                                    currwidth = Convert.ToInt32(_ref.Width);
                                }
                                else
                                {
                                    SizeF _ref = getSizePaysage(currwidth, currheight, referenceWidth, referenceHeight);
                                    currheight = Convert.ToInt32(_ref.Height);
                                    currwidth = Convert.ToInt32(_ref.Width);
                                }
                                xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                if (ypos < 0)
                                {
                                    ypos = 0;
                                }
                                if (xpos < 0)
                                {
                                    xpos = 0;
                                }
                                if (currentGSBackgroound == null)
                                {
                                    Bitmap bg = new Bitmap(currentchromaKeyBG);
                                    currentGSBackgroound = resizeBackground(bg, currwidth, currheight);
                                    bg.Dispose();
                                }
                                b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                            }

                            ImageFilterManager imageFilter = new ImageFilterManager();
                            if (!Globals.gswithoutframe)
                            {
                                 b = imageFilter.removeGraphicsMillGreenAsync(b, currentchromaKeyBG, 0, 0, currentGSBackgroound, currentwidth, currentHeight);
                            }

                            //b.SetResolution(640, 480);
                            var ms = new MemoryStream();
                            b.Save(ms, ImageFormat.Bmp);
                            b.Dispose();
                            using (WrapStream s = new WrapStream(ms))
                            {
                               
                                BitmapImage EvfImage = new BitmapImage();
                                EvfImage.BeginInit();
                                EvfImage.StreamSource = s;
                                EvfImage.CacheOption = BitmapCacheOption.None;
                                EvfImage.EndInit();
                                EvfImage.Freeze();
                                WaitChromaKey.Set();
                                Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                //await Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);
                                //System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();
                            }

                        }
                        else
                        {
                            if (currentIdentification == 3)
                            {
                                int widthCanvas = 0;
                                int heightCanvas = 0;
                                //int diff = Convert.ToInt32(b.Width / 4);
                                if (b.Height < 700)
                                {
                                    widthCanvas = b.Height;
                                    heightCanvas = b.Height;
                                }
                                else
                                {
                                    widthCanvas = Convert.ToInt32(700);
                                    heightCanvas = Convert.ToInt32(700);
                                }
                                xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                ypos = Convert.ToInt32(b.Height - heightCanvas) / 2; ;
                                b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);

                                var ms1 = new MemoryStream();
                                b.Save(ms1, ImageFormat.Bmp);
                                using (WrapStream s = new WrapStream(ms1))
                                {

                                    BitmapImage EvfImage = new BitmapImage();
                                    EvfImage.BeginInit();
                                    EvfImage.StreamSource = s;
                                    EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                    EvfImage.EndInit();
                                    EvfImage.Freeze();
                                    WaitChromaKey.Set();
                                    Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                }
                                b.Dispose();
                            }
                            else if (currentIdentification == 4)
                            {
                                int _width = Globals._shootDynamic[incrementimage - 1].width;
                                int _height = Globals._shootDynamic[incrementimage - 1].height;
                                if (_width == _height)
                                {
                                    int widthCanvas = 0;
                                    int heightCanvas = 0;
                                    //int diff = Convert.ToInt32(b.Width / 4);
                                    if (b.Height < 700)
                                    {
                                        widthCanvas = b.Height;
                                        heightCanvas = b.Height;
                                    }
                                    else
                                    {
                                        widthCanvas = Convert.ToInt32(700);
                                        heightCanvas = Convert.ToInt32(700);
                                    }
                                    xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                    ypos = Convert.ToInt32(b.Height - heightCanvas) / 2; ;
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);

                                    var ms1 = new MemoryStream();
                                    b.Save(ms1, ImageFormat.Bmp);
                                    using (WrapStream s = new WrapStream(ms1))
                                    {

                                        BitmapImage EvfImage = new BitmapImage();
                                        EvfImage.BeginInit();
                                        EvfImage.StreamSource = s;
                                        EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                        EvfImage.EndInit();
                                        EvfImage.Freeze();
                                        WaitChromaKey.Set();
                                        Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    }
                                    b.Dispose();
                                }
                                else if (_width < _height)
                                {
                                    int currwidth = width;
                                    int currheight = height;
                                    if(b.Height > 800)
                                    {
                                        SizeF _ref = getSizePortrait(currwidth, currheight, 800);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    else
                                    {
                                        SizeF _ref = getSizePortrait(currwidth, currheight, b.Height);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    
                                    xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                    ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                    if(ypos < 0)
                                    {
                                        ypos = 0;
                                    }
                                    if(xpos < 0)
                                    {
                                        xpos = 0;
                                    }
                                    
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                    var ms2 = new MemoryStream();
                                    b.Save(ms2, ImageFormat.Bmp);
                                    using (WrapStream s = new WrapStream(ms2))
                                    {

                                        BitmapImage EvfImage = new BitmapImage();
                                        EvfImage.BeginInit();
                                        EvfImage.StreamSource = s;
                                        EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                        EvfImage.EndInit();
                                        EvfImage.Freeze();
                                        WaitChromaKey.Set();

                                        Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    }
                                    b.Dispose();
                                }
                                else
                                {
                                    int currwidth = b.Width; ;
                                    int currheight = b.Height;
                                    int referenceWidth = width;
                                    int referenceHeight = height;
                                    if (referenceHeight > b.Height)
                                    {
                                        referenceHeight = b.Height;
                                    }
                                    if (referenceWidth > b.Width)
                                    {
                                        referenceWidth = b.Width;
                                    }
                                    if (b.Width > 930)
                                    {
                                        SizeF _ref = getSizePaysage(currwidth, currheight, 930, referenceHeight);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    else
                                    {
                                        SizeF _ref = getSizePaysage(currwidth, currheight, referenceWidth, referenceHeight);
                                        currheight = Convert.ToInt32(_ref.Height);
                                        currwidth = Convert.ToInt32(_ref.Width);
                                    }
                                    xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                    ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                    if (ypos < 0)
                                    {
                                        ypos = 0;
                                    }
                                    if (xpos < 0)
                                    {
                                        xpos = 0;
                                    }

                                    b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                    var ms = new MemoryStream();
                                    b.Save(ms, ImageFormat.Bmp);
                                    using (WrapStream s = new WrapStream(ms))
                                    {
                                        //img.Position = 0;
                                        BitmapImage EvfImage = new BitmapImage();
                                        EvfImage.BeginInit();
                                        EvfImage.StreamSource = s;
                                        EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                        EvfImage.EndInit();
                                        EvfImage.Freeze();
                                        WaitChromaKey.Set();
                                        Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    }
                                    b.Dispose();
                                }
                                
                            }
                            else if (currentIdentification == 1)
                            {
                                if (_offer == 1)
                                {
                                    int widthCanvas = 0;
                                    int heightCanvas = 0;
                                    //int diff = Convert.ToInt32(b.Width / 4);
                                    if (b.Height < 700)
                                    {
                                        widthCanvas = b.Height;
                                        heightCanvas = b.Height;
                                    }
                                    else
                                    {
                                        widthCanvas = Convert.ToInt32(700);
                                        heightCanvas = Convert.ToInt32(700);
                                    }
                                    xpos = Convert.ToInt32(b.Width - widthCanvas) / 2;
                                    ypos = Convert.ToInt32(b.Height - heightCanvas) / 2; ;

                                    //int widthCanvas = Convert.ToInt32(b.Width / 2);
                                    //int heightCanvas = Convert.ToInt32(b.Height);
                                    b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);

                                    var ms1 = new MemoryStream();
                                    b.Save(ms1, ImageFormat.Bmp);
                                    using (WrapStream s = new WrapStream(ms1))
                                    {

                                        BitmapImage EvfImage = new BitmapImage();
                                        EvfImage.BeginInit();
                                        EvfImage.StreamSource = s;
                                        EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                        EvfImage.EndInit();
                                        EvfImage.Freeze();
                                        WaitChromaKey.Set();
                                        Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                    }
                                    b.Dispose();
                                }
                                else
                                {
                                    if (portrait)
                                    {
                                        int currwidth = width;
                                        int currheight = height;
                                        if (b.Height > 800)
                                        {
                                            SizeF _ref = getSizePaysageByWidth(currwidth, currheight, 800);

                                            currheight = Convert.ToInt32(_ref.Height);
                                            currwidth = Convert.ToInt32(_ref.Width);
                                        }
                                        else
                                        {
                                            SizeF _ref = getSizePaysageByWidth(currwidth, currheight, b.Height);

                                            currheight = Convert.ToInt32(_ref.Height);
                                            currwidth = Convert.ToInt32(_ref.Width);
                                        }

                                        xpos = Convert.ToInt32(b.Width - currwidth) / 2;
                                        ypos = Convert.ToInt32(b.Height - currheight) / 2; ;
                                        if (ypos < 0)
                                        {
                                            ypos = 0;
                                        }
                                        if (xpos < 0)
                                        {
                                            xpos = 0;
                                        }
                                        b = ImageUtility.CropBitmap(b, xpos, ypos, currwidth, currheight);
                                        var ms = new MemoryStream();
                                        b.Save(ms, ImageFormat.Bmp);
                                        using (WrapStream s = new WrapStream(ms))
                                        {

                                            BitmapImage EvfImage = new BitmapImage();
                                            EvfImage.BeginInit();
                                            EvfImage.StreamSource = s;
                                            EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                            EvfImage.EndInit();
                                            EvfImage.Freeze();
                                            WaitChromaKey.Set();
                                            Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                            //Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                            //Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);

                                        }
                                        b.Dispose();
                                    }
                                    else
                                    {
                                        //if(bSize.Width == 0)
                                        //{
                                        //    bSize = getSizePaysageByWidthAndHeight(b.Width, b.Height, currentCanvasHeight, currentCanvasWidth);

                                        //b = ImageUtility.ResizeImage(b, currentCanvasWidth, currentCanvasHeight);
                                        
                                        if (b.Width > currentCanvasWidth)
                                        {
                                            xpos = (currentwidth - currentCanvasWidth) / 2;
                                            ypos = (currentHeight - currentCanvasHeight) / 2;
                                            if(xpos < 0)
                                            {
                                                xpos = 0;
                                            }
                                            if(ypos < 0)
                                            {
                                                ypos = 0;
                                            }
                                            if(currentCanvasHeight > b.Height)
                                            {
                                                currentCanvasHeight = b.Height;
                                            }
                                            
                                            b = ImageUtility.CropBitmap(b, xpos, ypos, currentCanvasWidth, currentCanvasHeight);
                                        }
                                            
                                         
                                       
                                        //}

                                        var ms = new MemoryStream();
                                        b.Save(ms, ImageFormat.Bmp);
                                        using (WrapStream s = new WrapStream(ms))
                                        {

                                            BitmapImage EvfImage = new BitmapImage();
                                            EvfImage.BeginInit();
                                            EvfImage.StreamSource = s;
                                            EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                            EvfImage.EndInit();
                                            EvfImage.Freeze();
                                            WaitChromaKey.Set();
                                            Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));
                                            //Application.Current.Dispatcher.BeginInvoke(SetImageAction, EvfImage);
                                        }
                                    }
                                }




                            }
                            else
                            {
                                var ms = new MemoryStream();
                                b.Save(ms, ImageFormat.Bmp);
                                using (WrapStream s = new WrapStream(ms))
                                {

                                    BitmapImage EvfImage = new BitmapImage();
                                    EvfImage.BeginInit();
                                    EvfImage.StreamSource = s;
                                    EvfImage.CacheOption = BitmapCacheOption.OnLoad;
                                    EvfImage.EndInit();
                                    EvfImage.Freeze();
                                    WaitChromaKey.Set();

                                    Dispatcher.BeginInvoke(new ThreadStart(delegate { CanvasImage.Source = EvfImage; }));

                                }
                            }

                        }

                    }
                    catch (Exception ex) { }


                    bi = BitmapToImageSource(bitmap);
                }
                


                


            }
            catch (Exception exc)
            {
                Log.Error("Error on _videoSource_NewFrame:\n" + exc.Message);
                StopCamera();
            }
        }

        public void cropPolaroidBitmapMultiShoot(string url)
        {
            string currentName = url;
            string oldName = "";
            int widthMultishoot = Globals._shootDynamic[incrementimage - 1].width;
            int heightMultishoot = Globals._shootDynamic[incrementimage - 1].height;
            if (widthMultishoot == heightMultishoot)
            {
                oldName = Path.GetFileNameWithoutExtension(currentName);
                oldName = Path.GetDirectoryName(url) + "\\" + oldName + "_.JPG";
                FileStream s = new FileStream(url, FileMode.Open);
                Bitmap imageBitmap = new Bitmap(s);
                imageBitmap = ImageUtility.ResizeImage(imageBitmap, currentwidth, currentHeight);

                int widthCanvas = 0;
                int heightCanvas = 0;
                //int diff = Convert.ToInt32(b.Width / 4);
                if (imageBitmap.Height < 700)
                {
                    widthCanvas = imageBitmap.Height;
                    heightCanvas = imageBitmap.Height;
                }
                else
                {
                    widthCanvas = Convert.ToInt32(700);
                    heightCanvas = Convert.ToInt32(700);
                }
                int xpos = Convert.ToInt32(imageBitmap.Width - widthCanvas) / 2;
                int ypos = Convert.ToInt32(imageBitmap.Height - heightCanvas) / 2; ;

                //int widthCanvas = Convert.ToInt32(b.Width / 2);
                //int heightCanvas = Convert.ToInt32(b.Height);
                //b = ImageUtility.CropBitmap(b, xpos, ypos, widthCanvas, heightCanvas);
                Bitmap _imageBitmap = ImageUtility.CropBitmap(imageBitmap, xpos, ypos, widthCanvas, heightCanvas);
                _imageBitmap.Save(oldName, ImageFormat.Jpeg);
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                s.Close();
                s.Dispose();
                imageBitmap.Dispose();
                _imageBitmap.Dispose();
                File.Delete(url);
                File.Copy(oldName, currentName);
                File.Delete(oldName);




            }
        }

        private void MainCamera_DownloadReady()
        {
            try
            {
                
                Globals.nbShootCounter--;
                if (Globals.nbShootCounter == 0)
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        Console.WriteLine("MainCamera_DownloadReady");
                        showLoader();
                    }));

                }
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)CanvasImage.Source));
                //encoder.QualityLevel = 100;
                FileStream fstream = new FileStream(Globals._tmpDir + "\\ IMG_TEMP.JPG", FileMode.Create);
                encoder.Save(fstream);
                fstream.Close();
                string[] imageFile = Directory.GetFiles(Globals._tmpDir);
                if (imageFile.Length > 0)
                {
                    photoDownloaded = true;
                }
                string firstImage = imageFile[0];

                string oldName = imageFile[0];
                string directoryView = Path.GetDirectoryName(oldName);

                if (currentchromaKeyBG != "" || Globals.gswithoutframe)
                {
                    
                    
                    var input = new Bitmap(imageFile[0]);
                    oldName = Path.GetDirectoryName(imageFile[0]) + "\\IMAGE_COLOR.jpg";
                    //var result = imageManager.RemoveBackground(input, currentchromaKeyBG);
                    var result = imageManager.removeGraphicsMillGreenToSave(input, currentchromaKeyBG);
                    input.SetResolution(input.HorizontalResolution, input.VerticalResolution);
                    input.Save(oldName, ImageFormat.Jpeg);
                    if (result != null)
                    {
                        input.Dispose();
                        result.Dispose();
                        File.Delete(imageFile[0]);
                        
                        
                        
                    }
                }
                else
                {
                    string currentName = "";
                    string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
                    foreach (string image in imageFiles)
                    {
                        currentName = image;
                        oldName = Path.GetFileNameWithoutExtension(currentName);
                        oldName = Path.GetDirectoryName(image) + "\\IMAGE_COLOR.jpg";
                        Bitmap imageBitmap = new Bitmap(image);
                        imageBitmap.SetResolution(imageBitmap.HorizontalResolution, imageBitmap.VerticalResolution);

                        //use a graphics object to draw the resized image into the bitmap
                        using (Graphics graphics = Graphics.FromImage(imageBitmap))
                        {
                            //set the resize quality modes to high quality
                            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                            //draw the image into the target bitmap
                            graphics.DrawImage(imageBitmap, 0, 0, imageBitmap.Width, imageBitmap.Height);
                        }
                        imageBitmap.Save(oldName, ImageFormat.Jpeg);
                        imageBitmap.Dispose();

                    }
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(currentName);
                }

                if (nbShoot > 1)
                {
                    imageFile = Directory.GetFiles(Globals._tmpDir);
                    if (currentIdentification == 2 || currentIdentification == 4)
                    {
                        int index = Globals.incrementShoot;
                        if (currentIdentification == 2)
                        {
                            if (index == 2)
                            {
                                index++;
                            }
                            else if (index == 3)
                            {
                                index += 2;
                            }
                        }

                        File.Copy(imageFile[0], Globals._tempFolder + "\\Color\\temp" + index + ".jpg");
                        //if (currentIdentification == 4)
                        //{
                        //    cropPolaroidBitmapMultiShoot(Globals._tempFolder + "\\Color\\temp" + index + ".jpg");
                        //}
                    }
                    else
                    {
                        File.Copy(imageFile[0], Globals._tempFolder + "\\temp" + Globals.incrementShoot + ".jpg");
                    }

                    var tempView = currDir + $"\\{_imageFolder}";
                    DirectoryInfo directory = new DirectoryInfo(tempView);
                    foreach (var item in directory.GetFiles())
                        File.Delete(item.FullName);
                }
            }
            catch (Exception ex) {  }
            finally
            {
                if (firstPhoto_Taken == false)
                {
                    firstPhoto_Taken = true;
                }
               

            }
        }

        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            //a holder for the result
            Bitmap result = new Bitmap(width, height);
            //set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            //return the resulting bitmap
            return result;
        }

        private void StartCamera()
        {
            
            if (CurrentDevice != null)
            {
                _videoSource = new VideoCaptureDevice(CurrentDevice.MonikerString);
                VideoCapabilities _vid = selectResolution(_videoSource);
                if (_vid != null)
                {
                    _videoSource.VideoResolution = _vid;
                }
                _videoSource.NewFrame += video_NewFrame;
                _videoSource.Start();
            }
            //startLiveView();
            incrementimage++;
           
            if (currentIdentification == 4)
            {
                CanvasImage.Source = null;
                int width = Globals._shootDynamic[incrementimage - 1].width;
                int height = Globals._shootDynamic[incrementimage - 1].height;
                if (incrementimage >= 0)
                {
                   
                    if (width == height)
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;

                        int height_ = y - 100;

                        int marginWidth = (x - height_) / 2;
                        int marginHeight = (y - height_) / 2;

                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight -10, marginWidth, marginHeight);
                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = height_; //930;
                        CanvasImage.Width = height_; //620
                        LVCanvas.Height = height_;
                        LVCanvas.Width = height_;



                        CanvasMaskImage.Height = height_; //930;
                        CanvasMaskImage.Width = height_; //620
                        CanvasMaskImage.Visibility = Visibility.Hidden;
                        minWidth = marginWidth - marginWidth / 4;
                        minHeight = minWidth;
                        int xx = x - marginWidth;
                        int yy = -(y - minHeight);
                        centerCanvas(minWidth, minHeight, xx, marginHeight - 10, height_);

                    }
                    else if (width < height)
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int currentwidth = width;
                        int currentheight = height;
                        int height_ = y - 100;
                        //if (currentheight > 800)
                        //{
                        //    SizeF theSize = new SizeF(currentwidth * .90f, currentheight * .90f);
                        //    currentheight = Convert.ToInt32(theSize.Height);
                        //    currentwidth = Convert.ToInt32(theSize.Width);
                        //}
                        int width_ = y - 100;
                        //if (currentwidth > width_)
                        //{
                        SizeF theSize = getSizePaysageByWidth(currentwidth, currentheight, width_);
                        currentheight = Convert.ToInt32(theSize.Height);
                        currentwidth = Convert.ToInt32(theSize.Width);
                        int marginWidth = (x - currentwidth) / 2;
                        int marginHeight = (y - currentheight) / 2;

                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = currentheight;
                        CanvasImage.Width = currentwidth;
                        LVCanvas.Height = currentheight;
                        LVCanvas.Width = currentwidth;



                        CanvasMaskImage.Height = currentheight;
                        CanvasMaskImage.Width = currentwidth;
                        CanvasMaskImage.Visibility = Visibility.Hidden;

                        marginWidth = (x - currentwidth) / 2;
                        marginHeight = (y - currentheight) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight -10, marginWidth, marginHeight);
                        minWidth = marginWidth - marginWidth / 4;
                        minHeight = (int)LVCanvas.Height * minWidth / (int)LVCanvas.Width;
                        int xx = x - marginWidth;
                        int yy = -(y - minHeight);
                        centerCanvas(minWidth, minHeight, xx, marginHeight - 10, (int)LVCanvas.Width);
                    }
                    else
                    {
                        int x = (int)SystemParameters.PrimaryScreenWidth;
                        int y = (int)SystemParameters.PrimaryScreenHeight;
                        int currentwidth = width;
                        int currentheight = height;
                        int width_ = y - 300;
                        //if (currentwidth > width_)
                        //{
                            SizeF theSize = getSizePaysageByWidth(currentwidth, currentheight, width_);
                            currentheight = Convert.ToInt32(theSize.Height);
                            currentwidth = Convert.ToInt32(theSize.Width);
                        //}
                        int marginWidth = (x - currentwidth) / 2;
                        int marginHeight = (y - currentheight) / 2;
                        LVCanvas.Margin = new Thickness(marginWidth, marginHeight -10, marginWidth, marginHeight);
                        int countHeight = (y - 220) / 2;
                        int gifHeight = (y - 100) / 2;
                        int gifWidth = (x - 110) / 2;
                        int countWidth = (x - 150) / 2;
                        lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                        GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                        CanvasImage.Height = currentheight; //930;
                        CanvasImage.Width = currentwidth; //620
                        LVCanvas.Height = currentheight;
                        LVCanvas.Width = currentwidth;



                        CanvasMaskImage.Height = currentheight; //930;
                        CanvasMaskImage.Width = currentwidth; //620
                        CanvasMaskImage.Visibility = Visibility.Hidden;
                        minWidth = marginWidth - marginWidth / 4;
                        minHeight = currentheight * minWidth / currentwidth;
                        int xx = x - marginWidth;
                        int yy = -(y - minHeight);
                        centerCanvas(minWidth, minHeight, xx, marginHeight - 10, currentwidth);

                    }
                }
                
            }
            else if(currentIdentification == 2)
            {
                int x = (int)SystemParameters.PrimaryScreenWidth;
                int y = (int)SystemParameters.PrimaryScreenHeight;
                int currentwidth = 930;
                int currentheight = 620;
                int marginWidth = (x - currentwidth) / 2;
                int marginHeight = (y - currentheight) / 2;
                LVCanvas.Margin = new Thickness(marginWidth, marginHeight - 10, marginWidth, marginHeight);
                int countHeight = (y - 220) / 2;
                int gifHeight = (y - 100) / 2;
                int gifWidth = (x - 110) / 2;
                int countWidth = (x - 150) / 2;
                lbl_CountDown.Margin = new Thickness(countWidth, countHeight, countWidth, countHeight);
                GIFCtrl.Margin = new Thickness(gifWidth, gifHeight, gifWidth, gifHeight);
                CanvasImage.Height = currentheight; //930;
                CanvasImage.Width = currentwidth; //620
                LVCanvas.Height = currentheight;
                LVCanvas.Width = currentwidth;



                CanvasMaskImage.Height = 620; //930;
                CanvasMaskImage.Width = 910; //620
                CanvasMaskImage.Visibility = Visibility.Hidden;
                minWidth = marginWidth - marginWidth / 4;
                minHeight = 620 * minWidth / 930;
                int xx = x - marginWidth;
                int yy = -(y - minHeight);
                centerCanvas(minWidth, minHeight, xx, marginHeight - 10, 930);
            }
        }

        private static VideoCapabilities selectResolution(VideoCaptureDevice device)
        {
            if(device.VideoCapabilities.Length > 0)
            {
                VideoCapabilities capTemp = device.VideoCapabilities[0];
                foreach (var cap in device.VideoCapabilities)
                {
                    if (cap.FrameSize.Width > capTemp.FrameSize.Width)
                    {
                        capTemp = cap;
                    }
                }
                return capTemp;
            }
            return null;
        }

        private void StopCamera()
        {
            
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
                _videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame);
            }
        }

        private SizeF getSizePortrait(int width, int height,int reference)
        {
            for (float i = 100; i > 0; i--)
            {

                float index =  i/ 100f;
               
                SizeF theSize = new SizeF(width * index, height * index);
                if(reference > theSize.Height)
                {
                    return theSize;
                }
                
            }
            return new SizeF();
        }

        private SizeF getSizePaysage(int width, int height, int referenceWidth, int referenceHeight)
        {
            for (float i = 100; i > 0; i--)
            {

                float index = i / 100f;

                SizeF theSize = new SizeF(width * index, height * index);
                if (referenceWidth > theSize.Width && referenceHeight > theSize.Height)
                {
                    return theSize;
                }

            }
            return new SizeF();
        }

        private SizeF getSizePaysageByHeight(int width, int height, int reference)
        {
            for (float i = 100; i > 0; i--)
            {

                float index = i / 100f;
                if(index != 1)
                {
                    SizeF theSize = new SizeF(width * index, height * index);
                    if (reference < theSize.Height)
                    {
                        return theSize;
                    }
                }
               

            }
            return new SizeF();
        }

        private SizeF getSizePaysageByWidth(int width, int height, int reference)
        {
            if(height < reference)
            {
                for (float i = 0; i < 100;)
                {

                    float index = i;
                    
                    if (index != 0)
                    {
                        SizeF theSize = new SizeF(width * index, height * index);
                        if (reference < theSize.Height)
                        {
                            return theSize;
                        }
                    }

                    i = i + 0.1f;
                }
            }
            else
            {
                for (float i = 100; i > 0; i--)
                {

                    float index = i / 100f;
                    if (index != 1)
                    {
                        SizeF theSize = new SizeF(width * index, height * index);
                        if (reference > theSize.Height)
                        {
                            return theSize;
                        }
                    }


                }
            }
            
            return new SizeF();
        }

        private SizeF getSizePaysageByWidthAndHeight(int width, int height, int referenceHeight, int referenceWidth)
        {
            if (width < referenceWidth)
            {
                for (float i = 0; i < 100;)
                {

                    float index = i;

                    if (index != 0)
                    {
                        SizeF theSize = new SizeF(width * index, height * index);
                        if ( referenceWidth < theSize.Width)
                        {
                            return theSize;
                        }
                    }

                    i = i + 0.1f;
                }
            }
            else
            {
                for (float i = 100; i > 0; i--)
                {

                    float index = i / 100f;
                    if (index != 1)
                    {
                        SizeF theSize = new SizeF(width * index, height * index);
                        if (referenceWidth > theSize.Width)
                        {
                            return theSize;
                        }
                    }


                }
            }

            return new SizeF();
        }

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion

        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            timerCountdown.Stop();
            StopCamera();
            if (imageBitmap != null)
            {
                imageBitmap.Dispose();
            }

            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();
            GC.Collect();
            Globals._FlagConfig = "client";
            var viewModel = (TakeWebcamCameraViewModel)DataContext;
            if (viewModel != null)
            {
                if (viewModel.GoToConnexionClient.CanExecute(null))
                    viewModel.GoToConnexionClient.Execute(null);
                /*if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }*/
            }
        }
    }
}
