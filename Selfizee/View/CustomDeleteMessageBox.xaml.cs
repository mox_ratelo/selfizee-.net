﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for CustomDeleteMessageBox.xaml
    /// </summary>
    public partial class CustomDeleteMessageBox : Window
    {
        private static bool close = false;
        public CustomDeleteMessageBox(string message, string title)
        {
            InitializeComponent();
            txtMessage.Content = title;
            Title.Content = message;
            //Title = title;

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultclosescroll, UriKind.Relative));
            closeWindow.Background = brushCroix;
            ImageBrush btnYes = new ImageBrush();
            btnYes.ImageSource = new BitmapImage(new Uri(Globals._defaultDeleteAssets, UriKind.Absolute));
            btnDeleteAssets.Background = btnYes;


            ImageBrush btnNo = new ImageBrush();
            btnNo.ImageSource = new BitmapImage(new Uri(Globals._defaultDeleteAll, UriKind.Absolute));

            btnDeleteAll.Background = btnNo;
        }

        private void btnDeleteOnlyAssets_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        public static string Prompt(string Message, string Title)
        {
            CustomDeleteMessageBox inst = new CustomDeleteMessageBox(Message, Title);
            inst.ShowDialog();
            if(close == false)
            {
                if (inst.DialogResult == true)
                    return "assets";
                return "all";
            }
            return "";
        }

        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void CloseWin(object sender, RoutedEventArgs e)
        {
            close = true;
            Close();
        }
    }
}
