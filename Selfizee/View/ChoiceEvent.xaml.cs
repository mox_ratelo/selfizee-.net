﻿using Selfizee.Models;
using Selfizee.Models.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Selfizee.Managers;
using EOSDigital.API;
using log4net;
using Selfizee.Models.Form;
using System.Collections.ObjectModel;
using AForge.Video.DirectShow;
using System.Drawing;
using Selfizee.View;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for ChoiceEvent.xaml
    /// </summary>
    public partial class ChoiceEvent : UserControl
    {
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        private string _imageFolder = @"View"; //Globals.directory_Target; 
        private string _ChoiceEvent = @"Events";
        private string path_ToSave = "";
        private string _finalFolder = @"Final";
        private bool customFormat = false;
        private int webcam_Activated = 0;
        private string currDir = Globals.EventAssetFolder();
        private CanonAPI APIHandler;
        List<Format> lstFormat = new List<Format>();
        private static string EventIni = $"{Globals.EventAssetFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ImageCollection _photos;

        private List<string> ImageUri(string imageFolder)
        {
            List<string> images = new List<string>();
            DirectoryInfo directory = new DirectoryInfo(imageFolder);
            if (directory.GetFiles().Any())
                foreach (var item in directory.GetFiles())
                    images.Add(item.Name);
            return images;
        }

        public void GetCustomListFormat()
        {
           
            INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
            int cust = Convert.ToInt32(_inimanager.GetSetting("FORMAT", "CustomPosEnabled"));
            webcam_Activated = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
            if (cust == 1)
            {
                int nbFiles = Directory.GetFiles(Globals._customEventChoices).Length;
                if (nbFiles > 0)
                {
                    Globals.customFormat = true;
                   
                    customFormat = true;
                }
            }
           
            int i = 0;
            bool check = true;
            if (customFormat)
            {
                while (check)
                {
                    i++;
                    int posX = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomPos" + i + "X"));
                    int posY = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomPos" + i + "Y"));
                    int width = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomPos" + i + "Width"));
                    int height = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomPos" + i + "Height"));
                    int nbShoot = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomNbShoot" + i ));
                    int countDown = Convert.ToInt32(_ini.GetSetting("FORMAT", "CustomCountdown" + i ));
                    string format = _ini.GetSetting("FORMAT", "CustomFormat" + i);
                    if (posX > 0 || posY > 0 || width > 0 || height > 0 || !string.IsNullOrEmpty(format))
                    {

                        Format formatTemp = new Format();
                        formatTemp.posX = posX;
                        formatTemp.posY = posY;
                        formatTemp.countDown = countDown;
                        formatTemp.nbShoot = nbShoot;
                        formatTemp.width = width;
                        formatTemp.height = height;
                        formatTemp.format = format;
                        lstFormat.Add(formatTemp);
                        check = true;
                    }
                    else
                    {
                        check = false;
                    }
                }
            }
        }

        void GameScreen_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            GC.SuppressFinalize(this);
            try
                {
                    switch (e.Key)
                    {
                        case Key.F1:
                            var viewModel = (ChoiceEventViewModel)DataContext;
                            Globals._FlagConfig = "client";
                        if (Globals.ScreenType == "SPHERIK")
                        {
                            if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                                viewModel.GoToClientSumarySpherik.Execute(null);
                        }
                        else if (Globals.ScreenType == "DEFAULT")
                        {
                            if (viewModel.GoToClientSumary.CanExecute(null))
                                viewModel.GoToClientSumary.Execute(null);
                        }
                        break;
                        case Key.F2:
                            var viewModel2 = (ChoiceEventViewModel)DataContext;
                            if (viewModel2.GoToConnexionConfig.CanExecute(null))
                                viewModel2.GoToConnexionConfig.Execute(null);
                            Globals._FlagConfig = "admin";
                            break;

                    }
                }
            catch (Exception ex)
            {

            }
        }

        private bool RefreshCamera()
        {
            bool found = false;
            var vm = (ChoiceEventViewModel)DataContext;
            INIFileManager _ini = new INIFileManager(Globals._appConfigFile);
            int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
            if (webcam == 1)
            {
                VideoDevices = new ObservableCollection<FilterInfo>();
                string camera_usb = _ini.GetSetting("WEBCAM", "Name");
                foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
                {
                    VideoDevices.Add(filterInfo);
                }
                if (!string.IsNullOrEmpty(camera_usb))
                {
                    foreach (var _videoDevice in VideoDevices)
                    {
                        if (_videoDevice.Name.ToLower().Equals(camera_usb.ToLower()))
                        {
                            found = true;
                        }
                    }
                }
                else
                {
                    Log.Error("No video sources found");
                }
                if (!found)
                {
                    var viewModel = (ChoiceEventViewModel)DataContext;
                    if (viewModel.GoToNoCamera.CanExecute(null))
                        viewModel.GoToNoCamera.Execute(null);
                    //scrollNoCamera.Visibility = Visibility.Visible;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                List<Camera> cameraList = APIHandler.GetCameraList();
                if (cameraList.Count <= 0 && webcam_Activated != 1)
                {
                    Log.Warn("Caméra éteinte");

                    if (vm.GoToNoCamera.CanExecute(null))
                        vm.GoToNoCamera.Execute(null);
                }
                else
                {
                    return false;
                }
            }
            
            return true;

        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            try
            {
                this.Focusable = true;
            Keyboard.Focus(this);
            
            INIFileManager _iniApp = new INIFileManager(Globals._appConfigFile);
            string id = _iniApp.GetSetting("EVENTSCONFIG", "id");
            if (string.IsNullOrEmpty(id))
            {
                var vm = (ChoiceEventViewModel)DataContext;
                if (vm.GoToNoEvent.CanExecute(null))
                    vm.GoToNoEvent.Execute(null);
            }
            else
            {
                bool noCam = RefreshCamera();
                if (!noCam)
                {
                    Globals.EventChosen = 0;
                    // currDir = Directory.GetCurrentDirectory();
                    path_ToSave = currDir + $"\\{_imageFolder}";
                    _finalFolder = "C:\\Events\\Assets\\" + id + "\\Events";
                    GetImageFileInfo();
                    _photos = (ImageCollection)(Resources["MyEvents"] as ObjectDataProvider).Data;
                    if (!customFormat)
                    {
                        _photos.Path = _finalFolder;
                    }

                }
            }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnClick(object sender, MouseButtonEventArgs e)
        {
            string eventName = "";
            var vm = (ChoiceEventViewModel)DataContext;
            if (lstFormat.Count > 0)
            {
                foreach(Format _format in lstFormat)
                {
                    int xmax = _format.posX + _format.width;
                    int ymax = _format.posY + _format.height;
                    eventName = GetSelectedCustomChoices();
                    if (!string.IsNullOrEmpty(eventName))
                    {
                        switch (eventName.ToLower())
                        {
                            case "cadre":
                                //using (StreamWriter sw = File.AppendText(TmpCsv))
                                //{
                                //    sw.WriteLine($"event=cadre");
                                //}
                                Globals.EventChosen = Globals.GetNbShoot(TypeEvent.cadre);
                                vm.selectedEvent = eventName;
                                if (vm.GoToBackgroundWindow.CanExecute(null))
                                    vm.GoToBackgroundWindow.Execute(null);
                                break;
                            case "strip":
                                //using (StreamWriter sw = File.AppendText(TmpCsv))
                                //{
                                //    sw.WriteLine($"event=bandelette");
                                //}
                                Globals.EventChosen = Globals.GetNbShoot(TypeEvent.strip);
                                vm.selectedEvent = eventName;
                                if (vm.GoToBackgroundWindow.CanExecute(null))
                                    vm.GoToBackgroundWindow.Execute(null);
                                break;
                            case "polaroid":
                                //using (StreamWriter sw = File.AppendText(TmpCsv))
                                //{
                                //    sw.WriteLine($"event=polaroid");
                                //}
                                Globals.EventChosen = Globals.GetNbShoot(TypeEvent.polaroid);
                                vm.selectedEvent = eventName;
                                if (vm.GoToBackgroundWindow.CanExecute(null))
                                    vm.GoToBackgroundWindow.Execute(null);
                                break;
                            case "multishoot":
                                //using (StreamWriter sw = File.AppendText(TmpCsv))
                                //{
                                //    sw.WriteLine($"event=multishoot");
                                //}
                                Globals.EventChosen = Globals.GetNbShoot(TypeEvent.multishoot);
                                vm.selectedEvent = eventName;
                                if (vm.GoToTakePhoto.CanExecute(null))
                                    vm.GoToTakePhoto.Execute(null);
                                break;
                        }
                    }
                }
            }
            
        }

        public ChoiceEvent()
        {
            InitializeComponent();

            try
            {
                Globals.greenScreen = "";
                this.PreviewKeyDown += GameScreen_PreviewKeyDown;
                Globals.timerHomeLaunched = true;
                Globals.leaveHome = false;
                GetCustomListFormat();
                var vm = (ChoiceEventViewModel)DataContext;
                APIHandler = new CanonAPI();
               


                ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("ChoiceEvent");
                if (backgroundAttributes.IsImage || customFormat)
                {
                    ImageBrush imgBrush = new ImageBrush();
                    if (!customFormat)
                    {
                        var fileName = Globals.LoadBG(TypeFond.ForEvent);
                        if (!File.Exists(fileName))
                        {
                            fileName = Globals.EventFolder() + "\\Assets\\Default\\Background\\background.jpg";
                        }
                        Bitmap bmp = null;
                        using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileName))
                        {
                            bmp = new Bitmap(img);
                        }
                        imgBrush.ImageSource = ImageUtility.convertBitmapToBitmapImage(bmp);
                        imgBrush.Stretch = Stretch.Fill;
                        eventPage.Background = imgBrush;
                    }
                    else
                    {
                        string fileName = Directory.GetFiles(Globals._customEventChoices).FirstOrDefault();
                        imgBrush.ImageSource = new BitmapImage(new Uri(fileName));
                        imgBrush.Stretch = Stretch.Fill;
                        eventPage.Background = imgBrush;
                        backgroundAttributes.EnableTitle = false;
                    }

                }

                if (!backgroundAttributes.EnableTitle)
                    choiceeventTitle.Visibility = Visibility.Collapsed;

                if (!backgroundAttributes.IsImage)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                    {
                        var bgcolor = backgroundAttributes.BackgroundColor;
                        var bc = new BrushConverter();
                        this.Background = (System.Windows.Media.Brush)bc.ConvertFrom(bgcolor);
                    }



                    if (backgroundAttributes.EnableTitle)
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                            choiceeventTitle.Text = backgroundAttributes.Title;
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                            choiceeventTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                        {
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                                Canvas.SetLeft((TextBlock)choiceeventTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                            if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                                Canvas.SetTop((TextBlock)choiceeventTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                        }
                }
            }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show($"Many errors happened!\n{ex.Message.ToString()}\n\n{ex.InnerException}\n\n{ex.StackTrace}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowImage(object sender, SelectionChangedEventArgs args)
        {
            var list = ((ListBox)sender);
            var index = list?.SelectedIndex; //Save the selected index 
            if (index >= 0)
            {
                var selection = list.SelectedItem.ToString();
                if (!string.IsNullOrEmpty(selection))
                {
                    //Set currentImage to selected Image
                    var selLoc = new Uri(selection);
                    var id = new BitmapImage(selLoc);
                    var currFileInfo = new FileInfo(selection);
                }
            }
        }

        public bool GetCursorPos(int xmin, int xmax, int ymin, int ymax)
        {
            bool result = false;
            //get the mouse position and show on the TextBlock
            //System.Drawing.Point p = System.Windows.Forms.Cursor.Position;
            System.Windows.Point pointToWindow = Mouse.GetPosition(this);
            System.Windows.Point pointToScreen = PointToScreen(pointToWindow);
            if (pointToScreen.X >= xmin && pointToScreen.X <= xmax && pointToScreen.Y >= ymin && pointToScreen.Y <= ymax)
            {
                result = true;
            }
            else
                result = false;

            return result;
        }

        public string GetSelectedCustomChoices()
        {
            if(lstFormat.Count > 0)
            {
                foreach(Format format in lstFormat)
                {
                    int xmin = format.posX;
                    int xmax = format.posX + format.width;
                    int ymin = format.posY;
                    int ymax = format.posY + format.height;
                    bool result = GetCursorPos(xmin, xmax, ymin, ymax);
                    if (result)
                    {
                        Globals.customNbshoot = format.nbShoot;
                        Globals.customCountDown = format.countDown;
                        return format.format;
                    }
                }
            }
            return null;
        }



        private void GetImageFileInfo()
        {
            string temp = "";
            //var currDir = Globals.CurrentDirectory();
            if (customFormat)
            {
                temp = Globals._customEventChoices;
            }
            else
            {
                temp = currDir + $"\\{_ChoiceEvent}";
            }
            if (!Directory.Exists(temp))
            {
                Directory.CreateDirectory(temp);
            }
            //Delete all files in events
            if (!customFormat)
            {
                DirectoryInfo directoryTemporarySepia = new DirectoryInfo(temp);
                foreach (var item in directoryTemporarySepia.GetFiles())
                {
                    File.Delete(item.FullName);
                }
                var vm = (ChoiceEventViewModel)DataContext;
                var imageFiles = new ArrayList();


                var manager = new Manager.ImageFilterManager();
                IniUtility ini = new IniUtility();
                var files = Directory.GetFiles(Globals._allEvents, "*.png");
                IniUtility iniFile = new IniUtility(Globals._appConfigFile);
                string currentCode = iniFile.Read("id", "EVENTSCONFIG");
                string _iniFile = "C:\\EVENTS\\Assets\\" + currentCode + "\\" + "Config.ini";
                string frameDir = "C:\\EVENTS\\Assets\\" + currentCode + "\\Frames";
                INIFileManager _ini = new INIFileManager(_iniFile);
                string[] _sections = Directory.GetDirectories(frameDir);
                string[] sections = new string[0];
                int increment = 0;
                if (_sections.Length > 1)
                {
                    foreach(var item in _sections)
                    {
                        string dirEventName = item.Split(System.IO.Path.DirectorySeparatorChar).Last();
                        int result = Convert.ToInt32(_ini.GetSetting(dirEventName.ToUpper(), "activated"));
                        if(result == 1)
                        {
                            increment++;
                            Array.Resize<string>(ref sections, increment);
                            sections[increment - 1] = dirEventName.ToLower();
                        }
                    }
                }
                else
                {
                    string dirEventName = _sections[0].Split(System.IO.Path.DirectorySeparatorChar).Last();
                    Array.Resize<string>(ref sections, 1);
                    sections[0] = dirEventName.ToLower();
                }
                string text = System.IO.File.ReadAllText(_iniFile);
                //string[] sections = ini.GetSections(text);
                if (sections.Length == 1)
                {

                    string eventName = "";
                    string event_file = frameDir + "\\" + sections[0] + "\\preview_frame.png";
                    eventName = sections[0];
                    if (!File.Exists(event_file))
                    {
                        event_file = frameDir + "\\" + sections[0] + "\\preview_frame.jpg";
                        
                    }
                    if (!File.Exists(event_file))
                    {
                        event_file = frameDir + "\\" + sections[0] + "\\preview_frame.jpeg";
                    }

                    switch (eventName)
                    {
                        case "cadre":
                            string offer = _ini.GetSetting(sections[0].ToUpper(), "offer");
                            int _offer = 0;
                            if (!string.IsNullOrEmpty(offer))
                            {
                                _offer = Convert.ToInt32(offer);
                            }
                            Globals.EventChosen = Globals.GetNbShoot(TypeEvent.cadre);
                            vm.selectedEvent = sections[0];
                            if(_offer > 0)
                            {
                                if (vm.GoToBackgroundOffer.CanExecute(null))
                                    vm.GoToBackgroundOffer.Execute(null);
                            }
                            else
                            {
                                if (vm.GoToBackgroundWindow.CanExecute(null))
                                    vm.GoToBackgroundWindow.Execute(null);
                            }
                           
                            break;
                        case "strip":
                            Globals.EventChosen = Globals.GetNbShoot(TypeEvent.strip);
                            vm.selectedEvent = sections[0];
                            if (vm.GoToBackgroundWindow.CanExecute(null))
                                vm.GoToBackgroundWindow.Execute(null);
                            break;
                        case "polaroid":
                            Globals.EventChosen = Globals.GetNbShoot(TypeEvent.polaroid);
                            vm.selectedEvent = sections[0];
                            if (vm.GoToBackgroundWindow.CanExecute(null))
                                vm.GoToBackgroundWindow.Execute(null);
                            break;
                        case "multishoot":
                            string pathOfImage = Globals.EventAssetFolder() + "\\Frames\\Multishoot";
                            var _imageFiles = new ArrayList();
                            _imageFiles = GetImageFileInfo(pathOfImage);
                            if (_imageFiles.Count <= 0)
                            {
                                string title = "Erreur";
                                string message = "L'image du cadre est manquant!!";
                                bool result = CustomInformationBox.Prompt( title, message);
                                if (vm.GoToAccueil.CanExecute(null))
                                    vm.GoToAccueil.Execute(null);
                            }
                            else
                            {
                                Globals._currentBackground = _imageFiles[0].ToString();
                                Globals.EventChosen = Globals.GetNbShoot(TypeEvent.multishoot);
                                vm.selectedEvent = sections[0];
                                INIFileManager __ini = new INIFileManager(Globals._appConfigFile);
                                int webcam = Convert.ToInt32(__ini.GetSetting("WEBCAM", "activated"));
                                if (webcam == 1)
                                {
                                    if (vm.GoToTakePhotoWebcam.CanExecute(null))
                                        vm.GoToTakePhotoWebcam.Execute(null);
                                }
                                else
                                {
                                    if (vm.GoToTakePhoto.CanExecute(null))
                                        vm.GoToTakePhoto.Execute(null);
                                }
                            }
                            
                            break;
                    }
                    //if (vm.GoToTakePhoto.CanExecute(null))
                    //    vm.GoToTakePhoto.Execute(null);
                }
                else if (sections.Length > 1)
                {
                    foreach (var image in files)
                    {
                        
                        //int result = Convert.ToInt32(_ini.GetSetting(image.ToUpper(), "activated"));
                        string eventFile = System.IO.Path.GetFileNameWithoutExtension(image).ToLower();
                        int result = Convert.ToInt32(_ini.GetSetting(eventFile.ToUpper(), "activated"));
                        string eventFound = Array.Find(sections, element => element.ToLower().Contains(eventFile));

                        if (eventFound != null && result>0)
                        {
                            string event_file = frameDir + "\\" + eventFile + "\\preview_frame.png";
                            
                            if (!File.Exists(event_file))
                            {
                                event_file = frameDir + "\\" + eventFile + "\\preview_frame.jpg";

                            }
                            if (!File.Exists(event_file))
                            {
                                event_file = frameDir + "\\" + eventFile + "\\preview_frame.jpg";
                            }
                            File.Copy(event_file, temp + "\\" + System.IO.Path.GetFileName(image));
                            var info = new FileInfo(image);
                            imageFiles.Add(info);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Aucun événement associé à la connexion courante");
                    Log.Fatal("Aucun événement associé à la connexion courante");
                    Environment.Exit(0);
                }
            }
            else
            {

            }
            //return imageFiles;
        }

        private void SelectedEvents(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var vm = (ChoiceEventViewModel)DataContext;
            if (vm.GoToTakePhoto.CanExecute(null))
                vm.GoToTakePhoto.Execute(null);
        }

        private void LbSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            string eventName = "";
            var vm = (ChoiceEventViewModel)DataContext;
            Selfizee.Models.Images selectedImage = null; 
            //var  selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as ListBoxItem);
            if (customFormat)
            {
                eventName = GetSelectedCustomChoices();
            }
            else
            {
                selectedImage = (Selfizee.Models.Images)((sender as ListBox).SelectedItem as Images);
                
                eventName = System.IO.Path.GetFileNameWithoutExtension(selectedImage.Source.ToString()).ToLower();
            }
            
            string TmpCsv = $"{Globals.EventAssetFolder()}\\Tmp.csv";
            if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
            {
                ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                comMgr.writeData(eventName);
            }
            string offer = _inimanager.GetSetting("CADRE", "offer");
            int offer_activated = 0;
            if (string.IsNullOrEmpty(offer))
            {
                offer_activated = Convert.ToInt32(offer);
            }
            
            switch (eventName.ToLower())
            {
                case "cadre":
                    //using (StreamWriter sw = File.AppendText(TmpCsv))
                    //{
                    //    sw.WriteLine($"event=cadre");
                    //}
                    Globals.EventChosen = Globals.GetNbShoot(TypeEvent.cadre);
                    vm.selectedEvent = selectedImage.Source.ToString();
                    if (offer_activated == 1)
                    {
                        if (vm.GoToBackgroundOffer.CanExecute(null))
                            vm.GoToBackgroundOffer.Execute(null);
                    }
                    else
                    {
                        if (vm.GoToBackgroundWindow.CanExecute(null))
                            vm.GoToBackgroundWindow.Execute(null);
                    }
                    
                    break;
                case "strip":
                    //using (StreamWriter sw = File.AppendText(TmpCsv))
                    //{
                    //    sw.WriteLine($"event=bandelette");
                    //}
                    Globals.EventChosen = Globals.GetNbShoot(TypeEvent.strip);
                    vm.selectedEvent = selectedImage.Source.ToString();
                    if (vm.GoToBackgroundWindow.CanExecute(null))
                        vm.GoToBackgroundWindow.Execute(null);
                    break;
                case "polaroid":
                    //using (StreamWriter sw = File.AppendText(TmpCsv))
                    //{
                    //    sw.WriteLine($"event=polaroid");
                    //}
                    Globals.EventChosen = Globals.GetNbShoot(TypeEvent.polaroid);
                    vm.selectedEvent = selectedImage.Source.ToString();
                    if (vm.GoToBackgroundWindow.CanExecute(null))
                        vm.GoToBackgroundWindow.Execute(null);
                    break;
                case "multishoot":
                   
                   
                        INIFileManager _ini = new INIFileManager(Globals.EventAssetFolder() + "\\Config.ini");
                        int webcam = Convert.ToInt32(_ini.GetSetting("WEBCAM", "activated"));
                        Globals.EventChosen = Globals.GetNbShoot(TypeEvent.multishoot);
                        if (webcam == 1)
                        {
                            vm.selectedEvent = selectedImage.Source.ToString();
                            if (vm.GoToTakePhotoWebcam.CanExecute(null))
                                vm.GoToTakePhotoWebcam.Execute(null);
                        }
                        else
                        {
                            vm.selectedEvent = selectedImage.Source.ToString();
                            if (vm.GoToTakePhoto.CanExecute(null))
                                vm.GoToTakePhoto.Execute(null);
                        }
                    
                    
                    
                    break;
            }
           
            }

        private ArrayList GetImageFileInfo(string directory)
        {
            var imageFiles = new ArrayList();
            var temp = directory;
            var manager = new Manager.ImageFilterManager();
            var files = Directory.GetFiles(temp);

            foreach (var image in files)
            {
                string file_name = System.IO.Path.GetFileName(image);
                if (file_name == "file_frame.png" && imageFiles.Count <= 0)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }
                else if (file_name == "file_frame.jpg" && imageFiles.Count <= 0)
                {
                    var info = new FileInfo(image);
                    imageFiles.Add(info);
                }

            }

            return imageFiles;
        }

        public void GotoClientSumary_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ChoiceEventViewModel)DataContext;
            if (viewModel != null)
            {
                if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }
            }
        }

    }
}
