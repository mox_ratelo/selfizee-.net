﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for NoEvent.xaml
    /// </summary>
    public partial class NoEvent : UserControl
    {
        public NoEvent()
        {
            InitializeComponent();
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(Globals.noEventImage);
            bitmap.EndInit();
            imageCamera.Source = bitmap;
        }

        private void GoToConfig(object sender, RoutedEventArgs e)
        {
            var vm = (NoEventViewModel)DataContext;
            if (vm.GoToConnexionConfig.CanExecute(null))
                vm.GoToConnexionConfig.Execute(null);
        }

    }
}
