﻿using Selfizee.Managers;
using SimpleWifi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for Configuration.xaml
    /// </summary>
    public partial class Configuration : UserControl
    {
        INIFileManager iniAppFile = new INIFileManager(Globals._appConfigFile);
        INIFileManager _ini;
        IniUtility iniWriting;
        IniUtility iniAppWriting;
        private string code;
        private static Wifi wifi;

        public Configuration()
        {
            wifi = new Wifi();
            InitializeComponent();

            Flag = Status();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;
            code = Globals.codeEvent_toEdit;
            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnConfigBack, UriKind.Absolute));
            btn_Back.Background = btnback;

            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(Globals._defaultBtnLaunchAnimation, UriKind.Absolute));
            btn_lanchAnimation.Background = btnLaunchAnimBrush;

            var brushSave = new ImageBrush();
            brushSave.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnsave, UriKind.Relative));
            btn_save.Background = brushSave;

            iniWriting = new IniUtility("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            iniAppWriting = new IniUtility(Globals._appConfigFile);
            TOGlobal.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOGlobal.LostFocus += LostTextBoxKeyboardFocus;
            TOScreenSaver.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOScreenSaver.LostFocus += LostTextBoxKeyboardFocus;
            Intervalle_SC.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            Intervalle_SC.LostFocus += LostTextBoxKeyboardFocus;
            countdown_cadre.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            countdown_cadre.LostFocus += LostTextBoxKeyboardFocus;
            TOCadre.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOCadre.LostFocus += LostTextBoxKeyboardFocus;
            countdown_polaroid.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            countdown_polaroid.LostFocus += LostTextBoxKeyboardFocus;
            TOPloaroid.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOPloaroid.LostFocus += LostTextBoxKeyboardFocus;
            countdown_strip.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            countdown_strip.LostFocus += LostTextBoxKeyboardFocus;
            TOStrip.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOStrip.LostFocus += LostTextBoxKeyboardFocus;
            
            countdown_multishoot.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            countdown_multishoot.LostFocus += LostTextBoxKeyboardFocus;
            TOMultishoot.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            TOMultishoot.LostFocus += LostTextBoxKeyboardFocus;
            Code_Multishoot.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            Code_Multishoot.LostFocus += LostTextBoxKeyboardFocus;
            nb_auhtorizedPrinting.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            nb_auhtorizedPrinting.LostFocus += LostTextBoxKeyboardFocus;
            nb_authorizedmultiprinting.GotKeyboardFocus += CoordonneeTextBoxGetFocused;
            nb_authorizedmultiprinting.LostFocus += LostTextBoxKeyboardFocus;

            _ini = new INIFileManager("C:\\EVENTS\\Assets\\" + code + "\\Config.ini");
            //INIFileManager iniEventFile = new INIFileManager("C:Events\\Assets\\" + code + "\\Config.ini");
            TOGlobal.Text = _ini.GetSetting("GENERAL", "timeout");
            string _rebond = _ini.GetSetting("HOME", "Animation");
            if (_rebond.ToLower() == "rebound")
            {
                rebond.IsChecked = true;
                statique.IsChecked = false;
            }
            else
            {
                rebond.IsChecked = false;
                statique.IsChecked = true;
            }
            
            int sc_activated = Convert.ToInt32(_ini.GetSetting("SCREENSAVER", "activated"));
            string to_screen = _ini.GetSetting("SCREENSAVER", "timeout");
            string interval_screen = _ini.GetSetting("SCREENSAVER", "interval");
            if (sc_activated == 1)
            {
                screensaver_activated.IsChecked = true;
            }
            else
            {
                screensaver_activated.IsChecked = false;
            }
            int mirrored = Convert.ToInt32(_ini.GetSetting("LIVEVIEW", "mirror"));
            if (mirrored == 1)
            {
                mirror.IsChecked = true;
            }
            else
            {
                mirror.IsChecked = false;
            }
            TOScreenSaver.Text = to_screen;
            Intervalle_SC.Text = interval_screen;
            string color = _ini.GetSetting("FILTRE", "COLOR");
            string bw = _ini.GetSetting("FILTRE", "BlackWhite");
            string sepia = _ini.GetSetting("FILTRE", "Sepia");
            if (color == "1")
            {
                _color.IsChecked = true;
            }
            else
            {
                _color.IsChecked = false;
            }
            if (bw == "1")
            {
                _BlackWhite.IsChecked = true;
            }
            else
            {
                _BlackWhite.IsChecked = false;
            }
            if (sepia == "1")
            {
                _Sepia.IsChecked = true;
            }
            else
            {
                _Sepia.IsChecked = false;
            }
            string defaultDir = _ini.GetSetting("SCREENSAVER", "Directory");
            if (!string.IsNullOrEmpty(defaultDir) && defaultDir.ToLower() == "default")
            {
                _default.IsChecked = true;
            }
            else
            {
                dirEvent.IsChecked = true;
            }
            int greenscreen = Convert.ToInt32(_ini.GetSetting("EVENT", "GreenScreen"));
            if (greenscreen == 1)
            {
                active_fond.IsChecked = true;
            }
            else
            {
                desactive_fond.IsChecked = false;
            }
            string iniPath = Globals._assetsFolder + "\\" + code + "\\Config.ini";
            INIFileManager iniFile = new INIFileManager(iniPath);
            string Name = iniFile.GetSetting("EVENT", "Name");
            eventName.Text = Name;
            codeEvent.Text = $"({code})";
            getFormat();
            getPrintingInfo();
        }

        private bool Status()
        {
            return (wifi.ConnectionStatus == WifiStatus.Connected);
        }

        #region FlagConnexion
        public static readonly DependencyProperty IsConnected = DependencyProperty.Register("Flag", typeof(bool), typeof(Configuration));
        public bool Flag
        {
            get { return (bool)GetValue(IsConnected); }
            set
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    SetValue(IsConnected, value);
                }));
            }
        }
        #endregion

        #region ConnectionStatusChanged
        private void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
            Flag = (e.NewStatus == WifiStatus.Connected);
        }
        #endregion

        private void Wifi_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToWifiList.CanExecute(null))
                viewModel.GoToWifiList.Execute(null);
        }

        public void LostTextBoxKeyboardFocus(object sender, EventArgs e)
        {
            alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
            numeriqueKeyboard.Visibility = Visibility.Collapsed;
        }

        public void CoordonneeTextBoxGetFocused(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            string tag = textbox.Tag.ToString();

            if (tag == "NUMERIQUE")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Collapsed;
                numeriqueKeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Visible;
                numeriqueKeyboard.ActiveContainer = textbox;
            }

            if (tag == "STRING")
            {
                alphanumeriquekeyboard.Visibility = Visibility.Visible;
                alphanumeriquekeyboard.ActiveContainer = textbox;
                alphanumeriquekeyboard.HorizontalAlignment = HorizontalAlignment.Center;
                numeriqueKeyboard.Visibility = Visibility.Collapsed;
            }
        }

        private void Quitter_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Vous allez quitter le logiciel, êtes-vous sur ? ", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
        private void getPrintingInfo()
        {
            int multiple_print = Convert.ToInt32(_ini.GetSetting("PRINTING", "MultiPrinting"));
            int _print = Convert.ToInt32(_ini.GetSetting("PRINTING", "print"));
            if (multiple_print == 1)
            {
                multiple_printing.IsChecked = true;
            }
            else
            {
                multiple_printing.IsChecked = false;
            }
            if (_print == 1)
            {
                print.IsChecked = true;
            }
            else
            {
                print.IsChecked = false;
            }
            nb_auhtorizedPrinting.Text = _ini.GetSetting("PRINTING", "eventauthorizedprint");
            if (string.IsNullOrEmpty(nb_auhtorizedPrinting.Text))
            {
                nb_auhtorizedPrinting.Text = "0";
            }
            nb_authorizedmultiprinting.Text = _ini.GetSetting("PRINTING", "eventauthorizedmultiprinting");
        }

        private void getFormat()
        {
            var files = Directory.GetFiles(Globals._allEvents, "*.png");
            string frameDir = "C:\\EVENTS\\Assets\\" + code + "\\Frames";
            string[] sections = Directory.GetDirectories(frameDir);
            //string[] sections = ini.GetSections(text);
            
            if (sections.Length == 1)
            {
                string firstImage = Directory.GetFiles(sections[0]).First();
                string eventName = System.IO.Path.GetFileNameWithoutExtension(sections[0]).ToLower();

                switch (eventName)
                {
                    case "cadre":
                        cadre.Visibility = Visibility.Visible;
                        Bitmap imageBitmap = new Bitmap(firstImage);
                        Img_cadre.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                        int activated = Convert.ToInt32(_ini.GetSetting("CADRE", "activated"));
                        if (activated == 1)
                        {
                            Cadre_activated.IsChecked = true;
                        }
                        int sepiaCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "Sepia"));
                        int colorCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "COLOR"));
                        int bwCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "BlackWhite"));
                        if (sepiaCadre > 0)
                        {
                            _Sepia.IsChecked = true;
                        }
                        if (colorCadre > 0)
                        {
                            _color.IsChecked = true;
                        }
                        if (bwCadre > 0)
                        {
                            _BlackWhite.IsChecked = true;
                        }
                        countdown_cadre.Text = _ini.GetSetting("CADRE", "countdown");
                        TOCadre.Text = _ini.GetSetting("CADRE", "timeout");
                        polaroid.Visibility = Visibility.Collapsed;
                        strip.Visibility = Visibility.Collapsed;
                        multishoot.Visibility = Visibility.Collapsed;
                        break;
                    case "strip":
                        strip.Visibility = Visibility.Visible;
                        Bitmap imageBitmap1 = new Bitmap(firstImage);
                        Img_strip.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap1);
                        int activated1 = Convert.ToInt32(_ini.GetSetting("STRIP", "activated"));
                        if (activated1 == 1)
                        {
                            Strip_activated.IsChecked = true;
                        }
                        int sepiaCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "Sepia"));
                        int colorCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "COLOR"));
                        int bwCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "BlackWhite"));
                        if (sepiaCadre1 > 0)
                        {
                            _sepiaStrip.IsChecked = true;
                        }
                        if (colorCadre1 > 0)
                        {
                            _colorStrip.IsChecked = true;
                        }
                        if (bwCadre1 > 0)
                        {
                            _bwStrip.IsChecked = true;
                        }
                        countdown_strip.Text = _ini.GetSetting("STRIP", "countdown");
                        TOStrip.Text = _ini.GetSetting("STRIP", "timeout");
                        polaroid.Visibility = Visibility.Collapsed;
                        cadre.Visibility = Visibility.Collapsed;
                        multishoot.Visibility = Visibility.Collapsed;
                        break;
                    case "polaroid":
                        polaroid.Visibility = Visibility.Visible;
                        Bitmap imageBitmap2 = new Bitmap(firstImage);
                        Img_polaroid.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap2);
                        int activated2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "activated"));
                        if (activated2 == 1)
                        {
                            Polaroid_activated.IsChecked = true;
                        }
                        int sepiaCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "Sepia"));
                        int colorCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "COLOR"));
                        int bwCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "BlackWhite"));
                        if (sepiaCadre2 > 0)
                        {
                            _SepiaPolaroid.IsChecked = true;
                        }
                        if (colorCadre2 > 0)
                        {
                            _colorPolaroid.IsChecked = true;
                        }
                        if (bwCadre2 > 0)
                        {
                            _bwPolaroid.IsChecked = true;
                        }
                        countdown_polaroid.Text = _ini.GetSetting("POLAROID", "countdown");
                        TOPloaroid.Text = _ini.GetSetting("POLAROID", "timeout");
                        cadre.Visibility = Visibility.Collapsed;
                        strip.Visibility = Visibility.Collapsed;
                        multishoot.Visibility = Visibility.Collapsed;
                        break;
                    case "multishoot":
                        multishoot.Visibility = Visibility.Visible;
                        Bitmap imageBitmap3 = new Bitmap(firstImage);
                        Img_multishoot.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap3);
                        int activated3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "activated"));
                        if (activated3 == 1)
                        {
                            multishoot_activated.IsChecked = true;
                        }
                        int sepiaCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "Sepia"));
                        int colorCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "COLOR"));
                        int bwCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "BlackWhite"));
                        if (sepiaCadre3 > 0)
                        {
                            _SepiaMultishoot.IsChecked = true;
                        }
                        if (colorCadre3 > 0)
                        {
                            _colorMultishoot.IsChecked = true;
                        }
                        if (bwCadre3 > 0)
                        {
                            _bwMultishoot.IsChecked = true;
                        }
                        countdown_multishoot.Text = _ini.GetSetting("MULTISHOOT", "countdown");
                        TOMultishoot.Text = _ini.GetSetting("MULTISHOOT", "timeout");
                        Code_Multishoot.Text = _ini.GetSetting("MULTISHOOT", "code");
                        polaroid.Visibility = Visibility.Collapsed;
                        strip.Visibility = Visibility.Collapsed;
                        cadre.Visibility = Visibility.Collapsed;
                        break;
                }
                //if (vm.GoToTakePhoto.CanExecute(null))
                //    vm.GoToTakePhoto.Execute(null);
            }
            else if (sections.Length > 1)
            {
                foreach (var image in files)
                {
                    
                    //int result = Convert.ToInt32(_ini.GetSetting(image.ToUpper(), "activated"));
                    string eventFile = System.IO.Path.GetFileNameWithoutExtension(image).ToLower();
                    int result = Convert.ToInt32(_ini.GetSetting(eventFile.ToUpper(), "activated"));
                    string eventFound = Array.Find(sections, element => element.ToLower().Contains(eventFile));
                    

                    if (eventFound != null)
                    {
                        string firstImage = Directory.GetFiles(eventFound).First();
                        if (eventFile.ToLower() == cadre.Name.ToLower())
                        {
                            cadre.Visibility = Visibility.Visible;
                            Bitmap imageBitmap = new Bitmap(firstImage);
                            Img_cadre.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                            countdown_cadre.Text = _ini.GetSetting("CADRE", "countdown");
                            TOCadre.Text = _ini.GetSetting("CADRE", "timeout");
                            int sepiaCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "Sepia"));
                            int colorCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "COLOR"));
                            int bwCadre = Convert.ToInt32(_ini.GetSetting("CADRE", "BlackWhite"));
                            if (sepiaCadre > 1)
                            {
                                _Sepia.IsChecked = true;
                            }
                            if (colorCadre > 1)
                            {
                                _color.IsChecked = true;
                            }
                            if (bwCadre > 1)
                            {
                                _BlackWhite.IsChecked = true;
                            }
                        }
                        else if(eventFile.ToLower() == polaroid.Name.ToLower())
                        {
                            polaroid.Visibility = Visibility.Visible;
                            Bitmap imageBitmap = new Bitmap(firstImage);
                            Img_polaroid.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                            countdown_polaroid.Text = _ini.GetSetting("POLAROID", "countdown");
                            TOPloaroid.Text = _ini.GetSetting("POLAROID", "timeout");
                            int sepiaCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "Sepia"));
                            int colorCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "COLOR"));
                            int bwCadre2 = Convert.ToInt32(_ini.GetSetting("POLAROID", "BlackWhite"));
                            if (sepiaCadre2 > 1)
                            {
                                _SepiaPolaroid.IsChecked = true;
                            }
                            if (colorCadre2 > 1)
                            {
                                _colorPolaroid.IsChecked = true;
                            }
                            if (bwCadre2 > 1)
                            {
                                _bwPolaroid.IsChecked = true;
                            }
                        }
                        else if (eventFile.ToLower() == strip.Name.ToLower())
                        {
                            strip.Visibility = Visibility.Visible;
                            Bitmap imageBitmap = new Bitmap(firstImage);
                            Img_strip.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                            countdown_strip.Text = _ini.GetSetting("STRIP", "countdown");
                            TOStrip.Text = _ini.GetSetting("STRIP", "timeout");
                            int sepiaCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "Sepia"));
                            int colorCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "COLOR"));
                            int bwCadre1 = Convert.ToInt32(_ini.GetSetting("STRIP", "BlackWhite"));
                            if (sepiaCadre1 > 1)
                            {
                                _sepiaStrip.IsChecked = true;
                            }
                            if (colorCadre1 > 1)
                            {
                                _colorStrip.IsChecked = true;
                            }
                            if (bwCadre1 > 1)
                            {
                                _bwStrip.IsChecked = true;
                            }
                        }
                        else if (eventFile.ToLower() == multishoot.Name.ToLower())
                        {
                            multishoot.Visibility = Visibility.Visible;
                            Bitmap imageBitmap = new Bitmap(firstImage);
                            Img_multishoot.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);
                            countdown_multishoot.Text = _ini.GetSetting("MULTISHOOT", "countdown");
                            TOMultishoot.Text = _ini.GetSetting("MULTISHOOT", "timeout");
                            Code_Multishoot.Text = _ini.GetSetting("MULTISHOOT", "code");
                            int sepiaCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "Sepia"));
                            int colorCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "COLOR"));
                            int bwCadre3 = Convert.ToInt32(_ini.GetSetting("MULTISHOOT", "BlackWhite"));
                            if (sepiaCadre3 > 1)
                            {
                                _SepiaMultishoot.IsChecked = true;
                            }
                            if (colorCadre3 > 1)
                            {
                                _colorMultishoot.IsChecked = true;
                            }
                            if (bwCadre3 > 1)
                            {
                                _bwMultishoot.IsChecked = true;
                            }
                        }
                    }
                    else
                    {
                        if (eventFile.ToLower() == cadre.Name.ToLower())
                        {
                            cadre.Visibility = Visibility.Collapsed; 
                        }
                        else if (eventFile.ToLower() == polaroid.Name.ToLower())
                        {
                            polaroid.Visibility = Visibility.Collapsed;
                        }
                        else if (eventFile.ToLower() == strip.Name.ToLower())
                        {
                            strip.Visibility = Visibility.Collapsed;
                        }
                        else if (eventFile.ToLower() == multishoot.Name.ToLower())
                        {
                            multishoot.Visibility = Visibility.Collapsed;
                        }
                    }
                    if(result > 0 && eventFound!=null)
                    {
                        if (eventFile.ToLower() == cadre.Name.ToLower())
                        {
                            Cadre_activated.IsChecked = true;
                        }
                        else if (eventFile.ToLower() == polaroid.Name.ToLower())
                        {
                            Polaroid_activated.IsChecked = true;
                        }
                        else if (eventFile.ToLower() == strip.Name.ToLower())
                        {
                            Strip_activated.IsChecked = true;
                        }
                        else if (eventFile.ToLower() == multishoot.Name.ToLower())
                        {
                            multishoot_activated.IsChecked = true;
                        }
                    }
                }
            }
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void Parametre_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToParametre.CanExecute(null))
                viewModel.GoToParametre.Execute(null);
        
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToEditEvent.CanExecute(null))
                viewModel.GoToEditEvent.Execute(null);
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void save(object sender, RoutedEventArgs e)
        {
            
            if (!string.IsNullOrEmpty(TOGlobal.Text))
            {
                iniWriting.Write("timeout", TOGlobal.Text, "GENERAL");
            }
            if (rebond.IsChecked==true)
            {
                iniWriting.Write("Animation", "rebound", "HOME");
            }
            else
            {
                iniWriting.Write("Animation", "static", "HOME");
            }
            if(screensaver_activated.IsChecked == true)
            {
                iniWriting.Write("activated", "1", "SCREENSAVER");
            }
            else
            {
                iniWriting.Write("activated", "0", "SCREENSAVER");
            }
            if (!string.IsNullOrEmpty(TOScreenSaver.Text))
            {
                iniWriting.Write("timeout", TOScreenSaver.Text, "SCREENSAVER");
            }
            if (!string.IsNullOrEmpty(Intervalle_SC.Text))
            {
                iniWriting.Write("interval", Intervalle_SC.Text, "SCREENSAVER");
            }
            ////////////////////
            saveFormat();
            ////////////////////
            if(_color.IsChecked == true)
            {
                iniWriting.Write("COLOR", "1", "FILTRE");
            }
            else
            {
                iniWriting.Write("COLOR", "0", "FILTRE");
            }
            if (_BlackWhite.IsChecked == true)
            {
                iniWriting.Write("BlackWhite", "1", "FILTRE");
            }
            else
            {
                iniWriting.Write("BlackWhite", "0", "FILTRE");
            }
            if (_Sepia.IsChecked == true)
            {
                iniWriting.Write("Sepia", "1", "FILTRE");
            }
            else
            {
                iniWriting.Write("Sepia", "0", "FILTRE");
            }

            if(multiple_printing.IsChecked == true)
            {
                iniWriting.Write("MultiPrinting", "1", "PRINTING");
            }
            else
            {
                iniWriting.Write("MultiPrinting", "0", "PRINTING");
            }
            if (!string.IsNullOrEmpty(nb_auhtorizedPrinting.Text))
            {
                iniWriting.Write("eventauthorizedprint", nb_auhtorizedPrinting.Text, "PRINTING");
            }
            if (!string.IsNullOrEmpty(nb_authorizedmultiprinting.Text))
            {
                iniWriting.Write("eventauthorizedmultiprinting", nb_authorizedmultiprinting.Text, "PRINTING");
            }
            

            if (_default.IsChecked == true)
            {
                iniWriting.Write("Directory", "default", "SCREENSAVER");
            }
            else
            {
                iniWriting.Write("Directory", "event", "SCREENSAVER");
            }
            int mirrored = Convert.ToInt32(_ini.GetSetting("LIVEVIEW", "mirror"));
            if (mirror.IsChecked == true)
            {
                iniWriting.Write("mirror", "1", "LIVEVIEW");
            }
            else
            {
                iniWriting.Write("mirror", "0", "LIVEVIEW");
            }
         
            if (print.IsChecked == true)
            {
                iniWriting.Write("print", "1", "PRINTING");
            }
            else
            {
                iniWriting.Write("print", "0", "PRINTING");
            }
            
            if (active_fond.IsChecked==true)
            {
                iniWriting.Write("GreenScreen", "1", "EVENT");
            }
            else
            {
                iniWriting.Write("GreenScreen", "0", "EVENT");
            }
            //MessageBox.Show("Enregistrement effectué avec succès");
            MessageBox.Show("Enregistrement effectué avec succès !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var viewModel = (EditEventViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void saveFormat()
        {
            if(cadre.Visibility == Visibility.Visible)
            {
                if (Cadre_activated.IsChecked == true)
                {
                    iniWriting.Write("activated", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("activated", "0", "CADRE");
                }
                if (!string.IsNullOrEmpty(TOCadre.Text))
                {
                    iniWriting.Write("timeout", TOCadre.Text, "CADRE");
                }
                if (!string.IsNullOrEmpty(countdown_cadre.Text))
                {
                    iniWriting.Write("countdown", countdown_cadre.Text, "CADRE");
                }
                if (_color.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "CADRE");
                }
                if (_Sepia.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "CADRE");
                }
                if (_BlackWhite.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "CADRE");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "CADRE");
                }
            }
            ///////////////////
            if(polaroid.Visibility == Visibility.Visible)
            {
                if (Polaroid_activated.IsChecked == true)
                {
                    iniWriting.Write("activated", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("activated", "0", "POLAROID");
                }
                if (!string.IsNullOrEmpty(TOPloaroid.Text))
                {
                    iniWriting.Write("timeout", TOPloaroid.Text, "POLAROID");
                }
                if (!string.IsNullOrEmpty(countdown_polaroid.Text))
                {
                    iniWriting.Write("countdown", countdown_polaroid.Text, "POLAROID");
                }
                if (_colorPolaroid.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "POLAROID");
                }
                if (_SepiaPolaroid.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "POLAROID");
                }
                if (_bwPolaroid.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "POLAROID");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "POLAROID");
                }
            }

            ///////////////////////////
            if (strip.Visibility == Visibility.Visible)
            {
                if (Strip_activated.IsChecked == true)
                {
                    iniWriting.Write("activated", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("activated", "0", "STRIP");
                }
                if (!string.IsNullOrEmpty(TOStrip.Text))
                {
                    iniWriting.Write("timeout", TOStrip.Text, "STRIP");
                }
                if (!string.IsNullOrEmpty(countdown_strip.Text))
                {
                    iniWriting.Write("countdown", countdown_strip.Text, "STRIP");
                }
                if (_colorStrip.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "STRIP");
                }
                if (_sepiaStrip.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "STRIP");
                }
                if (_bwStrip.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "STRIP");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "STRIP");
                }
            }
           
            /////////////////////////////
            if(multishoot.Visibility == Visibility.Visible)
            {
                if (multishoot_activated.IsChecked == true)
                {
                    iniWriting.Write("activated", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("activated", "0", "MULTISHOOT");
                }
                if (!string.IsNullOrEmpty(TOMultishoot.Text))
                {
                    iniWriting.Write("timeout", TOMultishoot.Text, "MULTISHOOT");
                }
                if (!string.IsNullOrEmpty(countdown_multishoot.Text))
                {
                    iniWriting.Write("countdown", countdown_multishoot.Text, "MULTISHOOT");
                }

                if (!string.IsNullOrEmpty(Code_Multishoot.Text))
                {
                    iniWriting.Write("code", Code_Multishoot.Text, "MULTISHOOT");
                }
                if (_colorMultishoot.IsChecked == true)
                {
                    iniWriting.Write("COLOR", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("COLOR", "0", "MULTISHOOT");
                }
                if (_SepiaMultishoot.IsChecked == true)
                {
                    iniWriting.Write("Sepia", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("Sepia", "0", "MULTISHOOT");
                }
                if (_bwMultishoot.IsChecked == true)
                {
                    iniWriting.Write("BlackWhite", "1", "MULTISHOOT");
                }
                else
                {
                    iniWriting.Write("BlackWhite", "0", "STRIP");
                }
            }
           
        }

        private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        {
            string code = Globals.codeEvent_toEdit;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (ConfigurationViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }
        
    }

   
}
