﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO.Compression;
using System.ComponentModel;
using System.Net.Http;
using System.Collections.Specialized;
using System.Reflection;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour ProgressUpdateWindow.xaml
    /// </summary>
    public partial class ProgressUpdateWindow : Window
    {
        //public long filesize;
        //public string ftpFileNamePath;
        //public string username;
        //public string password;
        string url;
        string dir;
        HttpWebRequest httpRequest;

        public ProgressUpdateWindow(string url, string dir)
        {
            InitializeComponent();
            this.url=url;
            this.dir = dir;
            

            Task.Run(() => DownloadFile(url,dir));
        }
        //public ProgressUpdateWindow(long filesize, string ftpFileNamePath, string username, string password)
        //{
        //    string path = "C:\\EVENTS\\Events\\first.txt";
        //    File.Create(path);
        //    InitializeComponent();
        //    long filesize = this.filesize;
        //    string ftpFileNamePath = this.ftpFileNamePath;
        //    string username = this.username;
        //    string password = this.password;
        //    Task.Run(() => DownloadFile(filesize, ftpFileNamePath, username, password));



        //}
        //public void DownloadFile(long filesize,string ftpFilenamePath, string username, string password)
        //{
        //    long fileSize = filesize;
        //    try
        //    {
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpFilenamePath);
        //        request.Credentials = new NetworkCredential(username, password);
        //        request.Method = WebRequestMethods.Ftp.DownloadFile;


        //      if (!Directory.Exists(Globals._defaultUpdateDirectory))
        //        {
        //            Directory.CreateDirectory(Globals._defaultUpdateDirectory);
        //        }
        //        using (Stream ftpStream = request.GetResponse().GetResponseStream())
        //        using (Stream fileStream = File.Create(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe"))
        //        {
        //            byte[] buffer = new byte[10240];
        //            int read;
        //            int totalReadBytesCount = 0;
        //            while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
        //            {
        //                fileStream.Write(buffer, 0, read);
        //                int position = (int)fileStream.Position;
        //                totalReadBytesCount += read;
        //                var progress = (int)((float)totalReadBytesCount / (float)fileSize * 100);

        //                Application.Current.Dispatcher.Invoke((Action)delegate {
        //                    prg.Value = (int)progress;
        //                    percentage.Text = progress + " %";
        //                });
        //            }

        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    AssemblyManager assmManager = new AssemblyManager();
        //    //long remoteFileSize = assmManager.GetLocalFileSize();
        //    //if (remoteFileSize == filesize)
        //    //{
        //    Application.Current.Dispatcher.Invoke((Action)delegate {
        //    assmManager.InstallUpdate();
        //    System.Windows.Forms.Application.Restart();
        //    Environment.Exit(0);
        //        //var endUpdateW = new EndUpdateWindow();
        //        //endUpdateW.ShowDialog();
        //        //this.Close();
        //    });

        //}

        public void DownloadFile(string link,string filesname)
        {
            string url = "";
            if (System.IO.Path.GetExtension(link) == ".zip")
            {
                url = Globals._defaultUpdateDirectory + "\\Selfizee_setup.zip";
            }
            else if (System.IO.Path.GetExtension(link) == ".exe")
            {
                url = Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe";
            }

            if (!Directory.Exists(Globals._defaultUpdateDirectory))
            {
                Directory.CreateDirectory(Globals._defaultUpdateDirectory);
            }
            if(File.Exists(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe"))
            {
                File.Delete(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe");
            }
            if (File.Exists(url))
            {
                File.Delete(url);
            }

            try
            {
                httpRequest = (HttpWebRequest)WebRequest.Create(link);
                httpRequest.Credentials = CredentialCache.DefaultCredentials;

                // if the URI doesn't exist, an exception will be thrown here...
                using (var httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = httpResponse.GetResponseStream())
                    {
                        //    using (FileStream localFileStream = File.Create(Globals._defaultUpdateDirectory + "\\Selfizee_setup.exe"))
                        using (FileStream localFileStream = File.Create(url))
                        {
                            long fileSize = httpRequest.GetResponse().ContentLength;
                            var buffer = new byte[4096];
                            long totalBytesRead = 0;
                            int bytesRead;
                            while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                totalBytesRead += bytesRead;
                                localFileStream.Write(buffer, 0, bytesRead);

                                int position = (int)localFileStream.Position;
                                var progress = (int)((float)totalBytesRead / (float)fileSize * 100);
                                //MessageBox.Show(totalBytesRead.ToString()+"/"+fileSize.ToString()+"*100= "+progress.ToString());
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    prg.Value = (int)progress;
                                    percentage.Text = progress + " %";
                                });
                                
                            }
                        }
                        
                    }
                }
                if (System.IO.Path.GetExtension(url) == ".zip")
                {
                    string zippath = Globals._defaultUpdateDirectory + "\\Selfizee_setup.zip";
                    string extractfile = Globals._defaultUpdateDirectory;
                    unzipFile(zippath, extractfile);
                }
               
            }
            catch (Exception e)
            {
               // MessageBox.Show("Téléchargement intérompu!");
            }
            try
            {
                AssemblyManager assmManager = new AssemblyManager();
                //Application.Current.Dispatcher.Invoke((Action)delegate
                //{
                
                assmManager.InstallUpdate();
                //});
            }
            catch (Exception e)
            {
                MessageBox.Show("Installation annulée!");
            }


        }

        

        public void BtnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            if (httpRequest != null)
            {
                 httpRequest.Abort();
                 this.Close();
            }
           
        }

        public void unzipFile(string zipPath, string extractPath)
        {
            extractPath = System.IO.Path.GetFullPath(extractPath);
            if (!extractPath.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
                extractPath += System.IO.Path.DirectorySeparatorChar;
            using (ZipArchive archive = ZipFile.OpenRead(zipPath))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    if (entry.FullName.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
                    {
                        // Gets the full path to ensure that relative segments are removed.
                        string destinationPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(extractPath, entry.FullName));

                        // Ordinal match is safest, case-sensitive volumes can be mounted within volumes that
                        // are case-insensitive.
                        if (destinationPath.StartsWith(extractPath, StringComparison.Ordinal))
                            entry.ExtractToFile(destinationPath);
                    }
                }
            }
        }
        public void btnRetour_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
