﻿using Newtonsoft.Json;
using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour HistoUpdateWindow.xaml
    /// </summary>
    public partial class HistoUpdateWindow : Window
    {
        public HistoUpdateWindow()
        {
            InitializeComponent();
            string json;
            string path = "C:\\EVENTS\\Events\\AppConfig.ini";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);
            string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");

            var brushCroix = new ImageBrush();
            brushCroix.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Relative));
            closeWindow.Background = brushCroix;

            /*if url path*/
            if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                //json = new WebClient().DownloadString("https://managerdev.selfizee.fr/api/get-list-version-logiciel.json");
                //string jsonfile = new WebClient().DownloadString("https://managerdev.selfizee.fr/historique-versions/getByBorne/" +idborne+ ".json");
                string jsonfile = new WebClient().DownloadString("https://booth.selfizee.fr/version-logiciels/getListVersionLogicielByBorne/" + idborne + ".json");// "DQ7H" + ".json");
                Write(jsonfile);
                json = Read();
            }

            /* if physical path*/
            else
            {
                json = Read();
            }
            var records= JsonConvert.DeserializeObject<List<RootObject>>(json);

            foreach (var itm in records)
            {
                //List<HistoriqueVersion> histoVersion = itm.historique_versions;
                //foreach (var item in histoVersion)
                //{
                    Grid grid = new Grid();
                    grid.Background = new SolidColorBrush(Color.FromRgb(68, 68, 68));
                    grid.Margin = new Thickness(20, 10, 20, 10);
                    grid.VerticalAlignment = VerticalAlignment.Stretch;
                    DockPanel.SetDock(grid, Dock.Top);

                    StackPanel stack = new StackPanel();
                    stack.Orientation = Orientation.Vertical;
                    stack.Margin = new Thickness(20, 10, 20, 10);

                    Grid topgrid = new Grid();

                    ColumnDefinition col1 = new ColumnDefinition();
                    col1.Width = GridLength.Auto;
                    topgrid.ColumnDefinitions.Add(col1);
                    ColumnDefinition col2 = new ColumnDefinition();
                    col2.Width = new GridLength(1, GridUnitType.Star);
                    topgrid.ColumnDefinitions.Add(col2);
                    ColumnDefinition col3 = new ColumnDefinition();
                    col3.Width = GridLength.Auto;
                    topgrid.ColumnDefinitions.Add(col3);

                    Label lblnumVers = new Label();
                    lblnumVers.Content = itm.numero_version;//item.version_logiciel.numero_version;
                    lblnumVers.Foreground = new SolidColorBrush(Color.FromRgb(221, 221, 221));
                    lblnumVers.FontWeight = FontWeights.Bold;
                    lblnumVers.FontSize = 25;
                    Grid.SetColumn(lblnumVers, 0);
                    topgrid.Children.Add(lblnumVers);


                    DateTime datevers = DateTime.Parse(itm.date_version);
                    string datev = datevers.ToString("dd/MM/yyyy");//datevers.Day + "/" + datevers.Month + "/" + datevers.Year;
                    Label lbldateVers = new Label();
                    lbldateVers.Content = datev;//item.date_heure_installation;
                    lbldateVers.Foreground = new SolidColorBrush(Color.FromRgb(221, 221, 221));
                    lbldateVers.FontWeight = FontWeights.Bold;
                    lbldateVers.FontSize = 14;
                    Grid.SetColumn(lbldateVers, 2);
                    topgrid.Children.Add(lbldateVers);

                    stack.Children.Add(topgrid);

                    Label lbltitleVers = new Label();
                    lbltitleVers.Content = itm.description_courte;// item.version_logiciel.titre;
                    lbltitleVers.Foreground = new SolidColorBrush(Color.FromRgb(221, 221, 221));
                    stack.Children.Add(lbltitleVers);

                    //Label lblmsgVers = new Label();
                    //lblmsgVers.Content = item.version_logiciel.description_courte;
                    //lblmsgVers.Foreground = new SolidColorBrush(Color.FromRgb(169, 169, 169));
                    //stack.Children.Add(lblmsgVers);

                    grid.Children.Add(stack);
                    Update.Children.Add(grid);
                //}
                

                
            }

        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TS_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            TS.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            ScrollViewer scrollviewer = sender as ScrollViewer;
            if (e.Delta > 0)
            {
                scrollviewer.LineUp();
            }
            else
            {
                scrollviewer.LineDown();
            }
            e.Handled = true;
        }        //public class RootObject
        //{
        //    public int id { get; set; }
        //    public string date_version { get; set; }
        //    public string numero_version { get; set; }
        //    public string description_courte { get; set; }
        //    public string file_exe { get; set; }
        //    public string etat { get; set; }
        //    public string titre { get; set; }
        //    public string message { get; set; }
        //    public string photo_couverture { get; set; }
        //    public string created { get; set; }
        //    public string modified { get; set; }
        //    public int type_maj_id { get; set; }
        //    public string url_photo_couverture { get; set; }
        //    public string url_file_exe { get; set; }
        //}

        //public class VersionLogiciel
        //{
        //    public int id { get; set; }
        //    public DateTime date_version { get; set; }
        //    public string numero_version { get; set; }
        //    public string description_courte { get; set; }
        //    public string file_exe { get; set; }
        //    public string etat { get; set; }
        //    public string titre { get; set; }
        //    public string message { get; set; }
        //    public object photo_couverture { get; set; }
        //    public DateTime created { get; set; }
        //    public DateTime modified { get; set; }
        //    public int type_maj_id { get; set; }
        //    public string url_photo_couverture { get; set; }
        //    public string url_file_exe { get; set; }
        //}

        //public class HistoriqueVersion
        //{
        //    public int id { get; set; }
        //    public int borne_info_id { get; set; }
        //    public int version_logiciel_id { get; set; }
        //    public DateTime date_heure_installation { get; set; }
        //    public DateTime created { get; set; }
        //    public DateTime modified { get; set; }
        //    public VersionLogiciel version_logiciel { get; set; }
        //}

        //public class RootObject
        //{
        //    public int id { get; set; }
        //    public string numero_borne { get; set; }
        //    public DateTime created { get; set; }
        //    public DateTime modified { get; set; }
        //    public List<HistoriqueVersion> historique_versions { get; set; }
        //}

        public class BorneInfos
        {
            public int id { get; set; }
            public string numero_borne { get; set; }
            public string created { get; set; }
            public string modified { get; set; }
        }

        public class VersionLogicielHasBorneInfos
        {
            public int id { get; set; }
            public int version_logiciel_id { get; set; }
            public int borne_info_id { get; set; }
        }

        public class MatchingData
        {
            public BorneInfos BorneInfos { get; set; }
            public VersionLogicielHasBorneInfos VersionLogicielHasBorneInfos { get; set; }
        }

        public class RootObject
        {
            public int id { get; set; }
            public string date_version { get; set; }
            public string numero_version { get; set; }
            public string description_courte { get; set; }
            public string file_exe { get; set; }
            public string etat { get; set; }
            public string titre { get; set; }
            public string message { get; set; }
            public string photo_couverture { get; set; }
            public string created { get; set; }
            public string modified { get; set; }
            public int type_maj_id { get; set; }
            public bool is_limited { get; set; }
            public bool is_limited_client { get; set; }
            public MatchingData _matchingData { get; set; }
            public string url_photo_couverture { get; set; }
            public string url_file_exe { get; set; }
        }

        public string Read()
        {
            string output = "";
            string path = @"C:\\Events\\JsonText.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    output = s;
                }
            }
            return output;
        }
        public void Write(string input)
        {
            string path = @"C:\\Events\\JsonText.txt";
            if (File.Exists(path))
            {
                File.Delete(path);
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(input);
                }
            }
        }
    }
}
