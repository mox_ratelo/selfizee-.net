﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for SplashScreenWindow.xaml
    /// </summary>
    public partial class SplashScreenWindow : Window
    {
        private Selfizee.MainWindow m_mainWindow;

        private DispatcherTimer splashAnimationTimer;
        public SplashScreenWindow()
        {
            InitializeComponent();
            //splashAnimationTimer = new DispatcherTimer();
            //splashAnimationTimer.Interval = TimeSpan.FromMilliseconds(1000);
            //splashAnimationTimer.Start();
            VideoControl.Source = new Uri(Globals._defaultLoaderVideo);
            VideoControl.Play();

            //Thread.Sleep(2000);

            m_mainWindow = new Selfizee.MainWindow();

            m_mainWindow.ReadyToShow += new MainWindow.ReadyToShowDelegate(m_mainWindow_ReadyToShow);

            m_mainWindow.Closed += new EventHandler(m_mainWindow_Closed);
        }

        void m_mainWindow_ReadyToShow(object sender, EventArgs args)
        {

            #region Animate the splash screen fading

            Storyboard sb = new Storyboard();
            //
            DoubleAnimation da = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = new Duration(TimeSpan.FromSeconds(3))
            };
            //
            Storyboard.SetTarget(da, this);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Children.Add(da);
            //
            sb.Completed += new EventHandler(sb_Completed);
            //
            sb.Begin();

            #endregion //  splash screen fading With Animation.
        }

        void sb_Completed(object sender, EventArgs e)
        {
            // When the splash screen fades out, we can show the main window.....
            m_mainWindow.Visibility = Visibility.Visible;
            MainWindowViewModel context = new MainWindowViewModel();
            m_mainWindow.DataContext = context;
            m_mainWindow.Show();
            this.Close();
        }

        void m_mainWindow_Closed(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
