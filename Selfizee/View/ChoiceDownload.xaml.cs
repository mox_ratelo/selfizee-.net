﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee.View
{
    /// <summary>
    /// Interaction logic for ChoiceDownload.xaml
    /// </summary>
    public partial class ChoiceDownload : UserControl
    {
        public ChoiceDownload()
        {
            InitializeComponent();

            //var brushBack = new ImageBrush();
            //brushBack.ImageSource = new BitmapImage(new Uri(Globals._defaultbackButton, UriKind.Relative));
            //btn_return.Background = brushBack;

            ImageBrush btnback = new ImageBrush();
            btnback.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnFlecheRetour, UriKind.Absolute));
            _back.Background = btnback;

            //List_event.Foreground = System.Windows.Media.Brushes.White;
            //Add_event.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ec1d60"));

            Bitmap imageBitmap4 = new Bitmap(Globals._defaultImageSelfizee);
            //Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap4);

            all.IsChecked = true;

            ImageBrush btndownload = new ImageBrush();
            btndownload.ImageSource = new BitmapImage(new Uri(Globals._defaultbtnDownload, UriKind.Absolute));
            btn_download.Background = btndownload;
        }

        private void ListEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ChoiceDownloadViewModel)DataContext;
            if (viewModel.GoToListEvent.CanExecute(null))
                viewModel.GoToListEvent.Execute(null);
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ChoiceDownloadViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (ChoiceDownloadViewModel)DataContext;
            if (viewModel.GoToDownload.CanExecute(null))
                viewModel.GoToDownload.Execute(null);
        }

        private void Download_Click(object sender, RoutedEventArgs e)
        {
            if (images.IsChecked==true)
            {
                Globals.paramDownload = "images";
            }
            else if (config.IsChecked == true)
            {
                Globals.paramDownload = "config";
            }
            else
            {
                Globals.paramDownload = "all";
            }
            
                var viewModel = (ChoiceDownloadViewModel)DataContext;
                if (viewModel.GoToDownloadDetail.CanExecute(null))
                    viewModel.GoToDownloadDetail.Execute(null);
        }
    }
}
