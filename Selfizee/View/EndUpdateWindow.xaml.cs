﻿using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.Specialized;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Selfizee.View
{
    /// <summary>
    /// Logique d'interaction pour EndUpdateWindow.xaml
    /// </summary>
    public partial class EndUpdateWindow : Window
    {
        public EndUpdateWindow()
        {
            InitializeComponent();

            Bitmap imageBitmap = new Bitmap(Globals._defaultImageSelfizee);
            Img_selfizee.Source = ImageUtility.convertBitmapToBitmapImage(imageBitmap);

            //Bitmap updateBitmap = new Bitmap(Globals._defaultImageCouv);
            //Img_update.Source = ImageUtility.convertBitmapToBitmapImage(updateBitmap);
            Bitmap updateBitmap = new Bitmap(Globals._defaultImageCouv);
            CroppedBitmap cb = new CroppedBitmap(ToSource.ToBitmapSource(updateBitmap), new Int32Rect(0, 0, updateBitmap.Width, (updateBitmap.Height * 75) / 100));
            Img_update.Source = cb;// ImageUtility.convertBitmapToBitmapImage(updateBitmap);
            CheckAndSendVersion();

        }
        public void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            //System.Windows.Forms.Application.Restart();
            //Environment.Exit(0);
            this.Close();
        }

        public string SendVersionData(VersionData version)
        {
            string responseInString = "";
            try
            {
                string url = "https://booth.selfizee.fr/api/setVersionLogiciel";
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["numero_borne"] = version.numero_borne;
                    data["numero_version"] = version.numero_version;
                    data["date_heure_installation"] = version.date_heure_installation;
                    var response = wb.UploadValues(url, "POST", data);

                    responseInString = Encoding.UTF8.GetString(response);
                }
            }
            catch (Exception e)
            {

            }
            return responseInString;

        }

        public void CheckAndSendVersion()
        {
            try
            {
                string path = "C:\\EVENTS\\Events\\AppConfig.ini";
                Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);
                string idborne = _inimanager.GetSetting("EVENTSCONFIG", "idborne");

                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string localVersion = name.Version.ToString();

                DateTime datenow = DateTime.Now;
                string sdatenow = datenow.ToString("yyyy-MM-dd HH:mm:ss");

                VersionData version = new VersionData();
                version.numero_borne = idborne;
                version.numero_version = localVersion;
                version.date_heure_installation = sdatenow;

                //string dir = "C:\\Events\\My ini\\Config.ini";
                //Managers.INIFileManager _inimanager = new Managers.INIFileManager(path);

                if (InternetConnectionManager.checkConnection("upload.selfizee.fr"))
                {
                    string isSent = _inimanager.GetSetting("SENDVERSION", "IsSent");
                    if (isSent.ToLower() == "false")
                    {
                        string response = SendVersionData(version);
                        string[] lresponnse = response.Split(',');
                        string successMsg = lresponnse[0].Split(':')[1];
                        string Msgsuccess = Regex.Replace(successMsg, "}", "");
                        if (Msgsuccess == "true")
                        {
                            //isSent = "true";
                            IniUtility _iniUtility = new IniUtility(path);
                            _iniUtility.Write("IsSent", "true", "SENDVERSION");
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }

        }
    }
}

