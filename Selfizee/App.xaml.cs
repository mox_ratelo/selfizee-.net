﻿using Aurigma.GraphicsMill.Licensing;
using Newtonsoft.Json;
using Selfizee.Managers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Media;
using static Selfizee.View.HistoUpdateWindow;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public string seletedFiltre { get; set; }
        IniUtility iniWriting = new IniUtility(Globals._appConfigFile);
        ResourceDictionary rd = new ResourceDictionary();
        ImageBrush ib = new ImageBrush(); 
        //ib.I 
        
        protected override void OnStartup(StartupEventArgs e)
        {

            try
            {
                var key = "gm.std.dev.max:1.maint:2021-02-24_QDDgJdhdxc6+0dfAKElmMVQ3petbDFvAaBXRGtUWGEfQgKMdzIDSqgylFp2JiIiOysRov3CoTljpNbycfIZ4GvHwY3LyjBVCcOL008QAX0Z0DmAsUm0drnLwnPlS+MU9MQ==";
                Aurigma.GraphicsMill.Licensing.License.Set(key);
                checkSelfizeeInstance();
                Globals.createFolder();
                base.OnStartup(e);
                CreateDirectoryApp();
                View.SplashScreenWindow app = new View.SplashScreenWindow();
                app.Show();
                if (!String.IsNullOrEmpty(Globals.portName) && Globals.portNumber > 0)
                {
                    ComManager comMgr = new ComManager(Globals.portName, Globals.portNumber);
                    comMgr.writeData("EVENT_" + Globals.GetEventId());
                }

                int width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                if (width <= 1800)

                {
                    iniWriting.Write("Type", "SPHERIK", "EVENTSCONFIG");
                }
                else
                {
                    iniWriting.Write("Type", "DEFAULT", "EVENTSCONFIG");
                }
                //DownloadUpdateElements();
                
            }
            catch (Exception ex)
            {
                
            }
           
        }

        public void Write(string input)
        {
            string path = @"C:\\Events\\JsonText.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            if (File.Exists(path))
            {
                File.Delete(path);
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(input);
                }
            }
        }

        public string Read()
        {
            string outputs = "";
            string path = @"C:\\Events\\JsonText.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    outputs = s;
                }
            }
            return outputs;
        }

        private void DownloadUpdateElements()
        {
            if (InternetConnectionManager.checkConnection("booth.selfizee.fr"))//(InternetConnectionManager.checkConnection("upload.selfizee.fr"))
            {
                string jsonString = "";
                try
                {
                    string idborne = iniWriting.Read("idborne", "EVENTSCONFIG");
                    string json = new WebClient().DownloadString("https://booth.selfizee.fr/version-logiciels/getListVersionLogicielByBorne/" + idborne + ".json");
                    Write(json);
                    jsonString = Read();
                }
                catch (Exception e)
                {

                }
                var records = JsonConvert.DeserializeObject<List<RootObject>>(jsonString);
                if (records != null)
                {
                    RootObject root = records.First();
                    string url = root.url_photo_couverture;
                    string dir = "C:\\Events\\Assets\\Default\\Images\\img_couv.jpg";
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(new Uri(url), dir);
                    }
                }
               
            }
            else
            {
                
            }
        }

        private void CreateDirectoryApp()
        {
            if (!Directory.Exists("C:\\Events"))
            {
                Directory.CreateDirectory("C:\\Events");
            }
            if (!Directory.Exists("C:\\Events\\Assets"))
            {
                Directory.CreateDirectory("C:\\Events\\Assets");
            }
            if (!Directory.Exists("C:\\Events\\Assets"))
            {
                Directory.CreateDirectory("C:\\Events\\Assets\\Archives");
            }
            if (!Directory.Exists(Globals._rotateFolder))
            {
                Directory.CreateDirectory(Globals._rotateFolder);
            }
            if (!Directory.Exists("C:\\Events\\Assets\\Default")) {
                Directory.CreateDirectory("C:\\Events\\Assets\\Default");
            }
                //24/01/2020 modif
            if (Directory.Exists("C:\\Events\\Assets\\Default"))//(!Directory.Exists("C:\\Events\\Assets\\Default"))
            {
                string[] paths = Directory.GetDirectories(Globals.exerDir + "\\Images\\Default", "*", SearchOption.AllDirectories);
                foreach (string dirPath in paths)
                {
                    string dirAssets = dirPath.Replace(Globals.exerDir + "\\Images\\Default", "C:\\Events\\Assets\\Default");
                    if (!Directory.Exists(dirAssets))
                    {
                        Directory.CreateDirectory(dirAssets);
                    }
                }


                //Copy all the files & Replaces any files with the same name
                paths = Directory.GetFiles(Globals.exerDir + "\\Images\\Default", "*.*", SearchOption.AllDirectories);
                foreach (string newPath in paths)
                {
                    string fileAssets = newPath.Replace(Globals.exerDir + "\\Images\\Default", "C:\\Events\\Assets\\Default");
                    if (!File.Exists(fileAssets))
                    {
                        File.Copy(newPath,fileAssets , true);
                    }
                    string emailFileName = Path.GetFileNameWithoutExtension(newPath);
                    if (emailFileName.Equals("email"))
                    {
                        if (File.Exists("C:\\Events\\Assets\\Default\\Buttons\\email.png"))
                        {
                            File.Delete("C:\\Events\\Assets\\Default\\Buttons\\email.png");
                        }
                        File.Copy(Globals.exerDir + "\\Images\\Default\\Buttons\\email.png", "C:\\Events\\Assets\\Default\\Buttons\\email.png");
                    }
                   
                }
            }
            if (!Directory.Exists("C:\\Events\\Events"))
            {
                Directory.CreateDirectory("C:\\Events\\Events");
            }
            //DateTime now = DateTime.Now;
            //string datenow = now.Day + "-" + now.Month + "-" + now.Year;
            //string backup = "C:\\EVENTS\\backup\\"+datenow;
            //if (!Directory.Exists(backup))
            //{
            //    Directory.CreateDirectory(backup);
            //}

            if (!File.Exists("C:\\Events\\Events\\AppConfig.ini"))
            {
                File.Copy(Globals.exerDir + "\\AppConfig.ini", "C:\\Events\\Events\\AppConfig.ini");
            }

            else
            {
                //IniUtility _ini=new IniUtility("C:\\Events\\Events\\AppConfig.ini");
                //INIFileManager _iniFile=new INIFileManager(Globals.exerDir + "\\AppConfig.ini");
                //string updatevalue = _iniFile.GetSetting("UPDATE", "IsUpdate");
                //_ini.Write("IsUpdate", updatevalue.ToLower(), "UPDATE");

                //string sendvalue = _iniFile.GetSetting("SENDVERSION","IsSent");
                //_ini.Write("IsSent", sendvalue.ToLower(), "SENDVERSION");

                //string formvalue = _iniFile.GetSetting("FORM","type");
                //_ini.Write("type", updatevalue.ToLower(), "FORM");
                //File.Move("C:\\Events\\Events\\AppConfig.ini", backup + "\\AppConfig.ini");
                //File.Copy(Globals.exerDir + "\\AppConfig.ini", "C:\\Events\\Events\\AppConfig.ini");
            }

            if (!Directory.Exists("C:\\PRINTERSETTINGS")) { 
                Directory.CreateDirectory("C:\\PRINTERSETTINGS");
            }

            if (!File.Exists("C:\\PRINTERSETTINGS\\SettingsDefault.bin")) { 
                if(File.Exists(Globals.exerDir + "\\SettingsDefault.bin"))
                    File.Copy(Globals.exerDir + "\\SettingsDefault.bin", "C:\\PRINTERSETTINGS\\SettingsDefault.bin",true);
            }

            if (!File.Exists("C:\\PRINTERSETTINGS\\Settings2InchCut.bin")) {
                if (File.Exists(Globals.exerDir + "\\Settings2InchCut.bin"))
                    File.Copy(Globals.exerDir + "\\Settings2InchCut.bin", "C:\\PRINTERSETTINGS\\Settings2InchCut.bin",true);
            }
        }

        public static void ReplaceFile(string fileToMoveAndDelete, string fileToReplace, string backupOfFileToReplace)
        {
            // Create a new FileInfo object.    
            FileInfo fInfo = new FileInfo(fileToMoveAndDelete);

            // replace the file.    
            fInfo.Replace(fileToReplace, backupOfFileToReplace, false);
        }
        private void checkSelfizeeInstance()
        {
            Process Proc_EnCours = Process.GetCurrentProcess();
            //Collection des processus actuellement lancés
            Process[] Les_Proc = Process.GetProcesses();
            //Pour chaque processus lancé
            foreach (Process Processus in Les_Proc)
                /*Il ne faut pas comparer par rapport à cette instance
                    du programme mais une autre (grâce à l'ID)*/
                if (Proc_EnCours.Id != Processus.Id)
                    //Si les ID sont différents mais de même nom ==> 2 fois le même programme
                    if (Proc_EnCours.ProcessName == Processus.ProcessName)
                    {
                        Processus.Kill();// = Processus;
                        //MessageBox.Show("Selfizee ne peut pas être lancé 2 fois!");
                        //Application.Current.Run();
                        //Environment.Exit(0);
                    }
        }

        void App_Exit(object sender, ExitEventArgs e)
        {
            string exitTime = DateTime.Now.ToString("HH:mm");
            DateTime connexion = DateTime.Parse(Globals.heureConnexion);
            DateTime deconnexion = DateTime.Parse(exitTime);
            var diff = deconnexion.Subtract(connexion);
            var res = String.Format("{0}:{1}:{2}", diff.Hours, diff.Minutes, diff.Seconds);
            System.Windows.Application.Current.Shutdown(); //or Environment.Exit(0);

        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
            e.Handled = true;
        }
    }
}
