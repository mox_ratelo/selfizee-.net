﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;
using Selfizee.Manager;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using AForge.Imaging.Filters;
using AForge.Imaging;
using AForge;
using Selfizee.Managers;
using System.Windows;
using System.Windows.Interop;
using log4net;
using System.Text;
using ImageMagick;
using Aurigma.GraphicsMill;
using Aurigma.GraphicsMill.Transforms;
using Aurigma.GraphicsMill.Codecs;

namespace Selfizee
{
    public static class ImageUtility
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            try
            {
                var destRect = new Rectangle(0, 0, width, height);
                var destImage = new System.Drawing.Bitmap(width, height);

                destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                }

                return destImage;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static System.Drawing.Bitmap removeChromaKey(System.Drawing.Bitmap toClean)
        {
            toClean.Save("C:\\Users\\Tojo\\Desktop\\tmp.png", ImageFormat.Png);
            var image = new MagickImage("C:\\Users\\Tojo\\Desktop\\tmp.png");
            image.ColorFuzz = new Percentage(20);
            image.TransparentChroma(new MagickColor(System.Drawing.Color.LawnGreen), new MagickColor(System.Drawing.Color.PaleGreen));
            return image.ToBitmap();
        }

        public static System.Drawing.Bitmap CropBitmap(System.Drawing.Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            //try
            //{
            //    using (var _bitmap = new Aurigma.GraphicsMill.Bitmap(bitmap))
            //    {
            //        _bitmap.Transforms.Crop(cropX, cropY, cropWidth, cropHeight);
            //        System.Drawing.Bitmap toReturn = (System.Drawing.Bitmap)_bitmap;
            //        _bitmap.Dispose();
            //        return toReturn;
            //    }
            //}
            //catch(Exception ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            //}
            //return null;
            try
            {
                Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
                System.Drawing.Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
                return cropped;
            }
            catch (Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static System.Drawing.Bitmap croppedBitmap(System.Drawing.Bitmap imageFile, int canvasX, int canvasy, int width, int height)
        {
            try
            {
                var rect1 = new Rect()
                {
                    X = canvasX,
                    Y = canvasy,
                    Width = width,
                    Height = height
                };

                // calc scale in PIXEls for CroppedBitmap...
                BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(imageFile.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                var scaleWidth = (bitmapSource.PixelWidth) / (imageFile.Width);
                var scaleHeight = (bitmapSource.PixelHeight) / (imageFile.Height);

                var rcFrom = new Int32Rect()
                {
                    X = (int)(rect1.X * scaleWidth),
                    Y = (int)(rect1.Y * scaleHeight),
                    Width = (int)(rect1.Width * scaleWidth),
                    Height = (int)(rect1.Height * scaleHeight)
                };
                var cropped = new CroppedBitmap(bitmapSource, rcFrom);
                return BitmapFromSource(cropped);
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static System.Drawing.Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            try
            {
                System.Drawing.Bitmap bitmap;
                using (MemoryStream outStream = new MemoryStream())
                {
                    BitmapEncoder enc = new BmpBitmapEncoder();
                    enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                    enc.Save(outStream);
                    bitmap = new System.Drawing.Bitmap(outStream);
                }
                return bitmap;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static System.Drawing.Bitmap CenterCrop(System.Drawing.Bitmap srcImage, int newWidth, int newHeight)
        {
            try
            {
                System.Drawing.Bitmap ret = null;

                int w = srcImage.Width;
                int h = srcImage.Height;

                if (w < newWidth || h < newHeight)
                {
                    MessageBox.Show("Out of boundary");
                    return ret;
                }
                int tierWidth = w - newWidth;
                int posX_for_centerd_crop = tierWidth;
                int posY_for_centerd_crop = (h - newHeight);

                var CenteredRect = new Rectangle(posX_for_centerd_crop,
                                        posY_for_centerd_crop, newWidth, newHeight);

                ret = srcImage.Clone(CenteredRect, srcImage.PixelFormat);

                return ret;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static void CreateoverlayedFrame(string image, string overlayedImage)
        {
            System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(image);
            List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
            int width = 0;
            int height = 0;
            System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(overlayedImage);
            width = baseImage.Width;
            height = baseImage.Height;
            //}
            System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(width, height);
            images.Add(baseImage);
            images.Add(imageOverlay);
            using (Graphics g = Graphics.FromImage(finalImage))
            {
                //set background color
                g.Clear(System.Drawing.Color.Black);

                //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                int offset = 0;
                foreach (System.Drawing.Bitmap imageTemp in images)
                {
                    g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, 0, width, height));
                }
            }
            ImageCodecInfo myImageCodecInfo;
            System.Drawing.Imaging.Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            // Get an ImageCodecInfo object that represents the JPEG codec.
            myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

            // Create an Encoder object based on the GUID
            // for the Quality parameter category.
            myEncoder = System.Drawing.Imaging.Encoder.Quality;

            // EncoderParameter object in the array.
            using (myEncoderParameters = new EncoderParameters(1))
            {
                // Save the bitmap as a JPEG file with quality level.            
                myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                myEncoderParameters.Param[0] = myEncoderParameter;
                string directoryTemp = Path.GetDirectoryName(image) + "\\temp_Image.jpg"; 
                finalImage.Save(directoryTemp, myImageCodecInfo, myEncoderParameters);
                finalImage.Dispose();
                baseImage.Dispose();
                if (File.Exists(image))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(image);
                }
                File.Copy(directoryTemp, image);
                File.Delete(directoryTemp);
            }
            
        }

        public static void createPhotoOverlayed(string imageFile, int position, List<string> filtres)
        {
            try
            {
                string finalFileName = "";
                CleanFinalFolder();
                string cadre = imageFile;
                var temp = Globals._tmpDir;
                int index = 0;

                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                {
                    throw new ArgumentNullException();
                }
                System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(cadre);
                ImageFilterManager imageManager = new ImageFilterManager();
                string[] imageFiles = Directory.GetFiles(temp);
                imageManager.Applyfilter(imageFiles[0], filtres);
                imageFiles = Directory.GetFiles(temp);
                foreach (string image in imageFiles)
                {
                    String filter = Path.GetFileNameWithoutExtension(image);
                    filter = filter.Substring(filter.IndexOf(",") + 2);
                    index++;
                   
                    string filename = Globals._finalFolder + "\\" + CreateFileName(index, filter);
                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    int width = 0;
                    int height = 0;
                    width = imageOverlay.Width;
                    height = imageOverlay.Height;
                    //}
                    System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(image);
                    if (width < height)
                    {
                        baseImage = ImageUtility.ResizeImage(baseImage, width, height);
                    }
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(width, height);
                    images.Add(baseImage);
                    images.Add(imageOverlay);
                    
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        //set background color
                        g.Clear(System.Drawing.Color.Black);
                        g.CompositingQuality = CompositingQuality.HighQuality;
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                        int offset = 0;
                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, 0, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                        finalFileName = filename;
                    }
                    string image_overlay = Path.GetFileNameWithoutExtension(cadre) + "_overlay.png";
                    image_overlay = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                    if (File.Exists(image_overlay))
                    {
                        //string fileOverlayed = Path.GetDirectoryName(cadre) + "\\" + image_overlay;
                        CreateoverlayedFrame(finalFileName, image_overlay);
                    }
                    //finalImage.Save(filename, ImageFormat.Jpeg);
                }
                bool used = filtres.Contains("COLOR");
                if (!used)
                {
                    imageFiles = Directory.GetFiles(Globals._finalFolder);
                    int keyIndex = Array.FindIndex(imageFiles, w => w.Contains("_COLOR"));
                    File.Delete(imageFiles[keyIndex]);
                }
               
            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            //finally { WaitEvent.Set(); }
        }

        public static void createPhotoOverlayedPortrait(string imageFile, int position, List<string> filtres)
        {
            try
            {
                string finalFileName = "";
                CleanFinalFolder();
                string cadre = imageFile;
                var temp = Globals._tmpDir;
                int index = 0;

                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                {
                    throw new ArgumentNullException();
                }
                System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(cadre);
                ImageFilterManager imageManager = new ImageFilterManager();
                string[] imageFiles = Directory.GetFiles(temp);
                imageManager.Applyfilter(imageFiles[0], filtres);
                imageFiles = Directory.GetFiles(temp);
                foreach (string image in imageFiles)
                {
                    String filter = Path.GetFileNameWithoutExtension(image);
                    filter = filter.Substring(filter.IndexOf(",") + 2);
                    index++;

                    string filename = Globals._finalFolder + "\\" + CreateFileName(index, filter);
                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    int width = 0;
                    int height = 0;
                    width = imageOverlay.Width;
                    height = imageOverlay.Height;
                    int _width = Globals._shootDynamic[0].width;
                    int _height = Globals._shootDynamic[0].height;
                    int xpos = Globals._shootDynamic[0].x;
                    int ypos = Globals._shootDynamic[0].y;
                    //}
                    System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(image);
                    if (width < height)
                    {
                        baseImage = ImageUtility.ResizeImage(baseImage, _width, _height);
                    }
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(width, height);
                    images.Add(imageOverlay);
                    images.Add(baseImage);

                    int increment = 0;
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                       
                        //set background color
                        g.Clear(System.Drawing.Color.Black);
                        g.CompositingQuality = CompositingQuality.HighQuality;
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                       
                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            increment++;
                            int offset = 0;
                            int _y = 0;
                            if (increment == 2)
                            {
                                offset = xpos;
                                _y = ypos;
                                width = _width;
                                height = _height;
                            }
                            g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, _y, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                        finalFileName = filename;
                    }
                    string image_overlay = Path.GetFileNameWithoutExtension(cadre) + "_overlay.png";
                    image_overlay = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                    if (File.Exists(image_overlay))
                    {
                        //string fileOverlayed = Path.GetDirectoryName(cadre) + "\\" + image_overlay;
                        CreateoverlayedFrame(finalFileName, image_overlay);
                    }
                    //finalImage.Save(filename, ImageFormat.Jpeg);
                }
                bool used = filtres.Contains("COLOR");
                if (!used)
                {
                    imageFiles = Directory.GetFiles(Globals._finalFolder);
                    int keyIndex = Array.FindIndex(imageFiles, w => w.Contains("_COLOR"));
                    File.Delete(imageFiles[keyIndex]);
                }

            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            //finally { WaitEvent.Set(); }
        }

        public static void createPhotoOfferOverlayed(string imageFile, int position, List<string> filtres)
        {
            try
            {
                CleanFinalFolder();
                string cadre = imageFile;
                string finalFileName = "";
                var temp = Globals._tempFolder;
                int index = 0;
                string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
                String tempFile = imageFiles[0];

                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                    throw new ArgumentNullException();
                //var inimanager = new INIFileManager($"{Globals.EventConfigFolder()}\\Config.ini");
                //var filtres = inimanager.GetKeysByValue("FILTRE", "1");

                System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(cadre);
                ImageFilterManager imageManager = new ImageFilterManager();

                imageManager.Applyfilter(imageFiles[0], filtres);
                imageFiles = Directory.GetFiles(Globals._tmpDir);
                foreach (String file in imageFiles)
                {
                    String dirDestination = Globals._tempFolder + "\\" + Path.GetFileName(file);
                    File.Copy(file, dirDestination);
                }

                string[] imageTempFiles = Directory.GetFiles(temp);
                foreach (string image in imageTempFiles)
                {
                    String filter = Path.GetFileNameWithoutExtension(image);
                    filter = filter.Substring(filter.IndexOf(",") + 2);
                    index++;
                    System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(image);
                    string filename = Globals._finalFolder + "\\" + CreateFileName(index, filter);
                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(baseImage.Width, baseImage.Height);
                    int heightPolaroid = baseImage.Height - ((baseImage.Height / 3));
                    heightPolaroid = heightPolaroid - 100;
                    imageOverlay = ImageUtility.ResizeImage(imageOverlay, baseImage.Width - 95, heightPolaroid);
                    //heightPolaroid = heightPolaroid - 10;
                    //imageOverlay = ImageUtility.ResizeImage(imageOverlay, baseImage.Width - 5, heightPolaroid);
                    images.Add(imageOverlay);
                    images.Add(baseImage);

                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        int offset = 0;
                        int ySet = 0;
                        int width = 0;
                        int height = 0;
                        int increment = 0;
                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            increment++;
                            System.Drawing.Bitmap BmpTemp = imageTemp;
                            switch (increment)
                            {
                                case 1:
                                    if (Globals._shootDynamic.Length > 0)
                                    {
                                        offset = Globals._shootDynamic[0].x;
                                        ySet = Globals._shootDynamic[0].y;
                                        width = Globals._shootDynamic[0].width;
                                        height = Globals._shootDynamic[0].height;
                                        BmpTemp = ImageUtility.ResizeImage(imageTemp, width, height);
                                    }
                                    else
                                    {
                                        offset = 45;
                                        //ySet = 45;
                                        ySet = 110;
                                        width = imageOverlay.Width;
                                        height = imageOverlay.Height;
                                    }



                                    break;
                                case 2:
                                    offset = 0;
                                    ySet = 0;
                                    width = baseImage.Width;
                                    height = baseImage.Height;
                                    break;
                            }
                            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.CompositingQuality = CompositingQuality.HighQuality;
                            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            g.DrawImage(BmpTemp, new System.Drawing.Rectangle(offset, ySet, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalFileName = filename;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                        string image_overlay = Path.GetFileNameWithoutExtension(cadre) + "_overlay.png";
                        image_overlay = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                        if (File.Exists(image_overlay))
                        {
                            //string fileOverlayed = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                            CreateoverlayedFrame(finalFileName, image_overlay);
                        }
                    }
                    //finalImage.Save(filename, ImageFormat.Jpeg);

                }


                bool used = filtres.Contains("COLOR");
                if (!used)
                {
                    imageFiles = Directory.GetFiles(Globals._finalFolder);
                    int keyIndex = Array.FindIndex(imageFiles, w => w.Contains("_COLOR"));
                    File.Delete(imageFiles[keyIndex]);
                }
            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            //finally { WaitEvent.Set(); }
        }

        public static void writeDebug(string path, string value)
        {
            //using (System.IO.StreamWriter file =
            //new System.IO.StreamWriter(path))
            //{
            //    file.WriteLine(value);
            //}
            File.AppendAllText(path,
                   value + Environment.NewLine);
        }

        public static string CreateOverlayedStrip(string imageFile)
        {
            string fileName = "";
            try
            {
                List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                string[] strImages = Directory.GetFiles(Globals._finalFolder + "\\Temporaire");
                System.Drawing.Bitmap baseImage;
                System.Drawing.Bitmap imageOverlay;
                int width = 0;
                int height = 0;
                int i = 0;
                foreach (string image in strImages)
                {
                    i++;
                    if (i == 2)
                    {
                        baseImage = new System.Drawing.Bitmap(image);
                        images.Add(baseImage);
                        width = baseImage.Width;
                        height = baseImage.Height;
                    }
                    else
                    {
                        imageOverlay = new System.Drawing.Bitmap(image);
                        images.Add(imageOverlay);
                    }

                }


                int finalwidth = width + (width / 2);
                System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(finalwidth, height);


                using (Graphics g = Graphics.FromImage(finalImage))
                {
                    //set background color
                    g.Clear(System.Drawing.Color.YellowGreen);

                    //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                    int offset = 0;
                    int k = 0;
                    foreach (System.Drawing.Bitmap imageTemp in images)
                    {
                        k++;
                        if (k == 1)
                        {
                            offset = width / 3;
                        }
                        else
                            offset = 0;
                        g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, 0, width, height));
                    }
                }
                fileName = Path.GetFileName(imageFile);
                fileName = Globals._finalFolder + "\\Affichage\\" + fileName;
                finalImage.MakeTransparent(System.Drawing.Color.YellowGreen);
                string filenamewithoutext = Path.GetFileNameWithoutExtension(fileName);
                filenamewithoutext += ".png";
                string directoryName = Path.GetDirectoryName(fileName);
                fileName = directoryName + "\\" + filenamewithoutext;
                finalImage.Save(fileName, ImageFormat.Png);
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            
            return fileName;
        }

        public static void createPhotoMultiShoot(List<string> filtres)
        {
            try
            {
                CleanFinalFolder();
                //Getting list of filter
                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                    throw new ArgumentNullException();
                //var inimanager = new INIFileManager($"{Globals.EventConfigFolder()}\\Config.ini");
                //var filtres = inimanager.GetKeysByValue("FILTRE", "1");

                var temp = Globals._tempFolder + "\\Color";
                string[] imageFiles = Directory.GetFiles(temp);
                int incrementFile = 0;
                foreach (string image in imageFiles)
                {
                    incrementFile += 1;

                    foreach (var filtre in filtres)
                    {
                        switch (filtre.ToUpper())
                        {
                            case "GREYSCALE":
                                break;
                            case "SEPIA":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\Sepia\\" + Path.GetFileName(image));
                                break;
                            case "BLACKWHITE":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\BLACKWHITE\\" + Path.GetFileName(image));
                                break;
                            case "ONECOLOR":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\OneColor\\" + Path.GetFileName(image));
                                break;
                            case "POPART":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\Popart\\" + Path.GetFileName(image));
                                break;
                        }
                    }

                }

                string[] directories = Directory.GetDirectories(Globals._tempFolder);
                foreach (string directory in directories)
                {
                    string FolderName = directory.Split(Path.DirectorySeparatorChar).Last();
                    var updatedItems = filtres.Select(p => p.ToUpper()
                    ).ToList();

                    bool used = updatedItems.Contains(FolderName.ToUpper());
                    if (used)
                    {
                        createImageMultiShootByFilter(directory, FolderName);
                    }

                }
            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
        }
        public static void CopyImageToAnotherDirectory(string path, string destination)
        {
            try
            {
                File.Copy(path, destination);
            }
            catch(Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            
        }

        public static System.Drawing.Bitmap MirrorImage(System.Drawing.Bitmap source)
        {
            try
            {
                System.Drawing.Bitmap mirrored = new System.Drawing.Bitmap(source.Width, source.Height);
                for (int i = 0; i < source.Height; i++)
                    for (int j = 0; j < source.Width; j++)
                        mirrored.SetPixel(i, j, source.GetPixel(source.Width - j - 1, i));
                return mirrored;
            }
            catch(Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            return null;
        }


        public static void createPhotoStrip(string imageFile, List<string> filtres)
        {
            try
            {
                CleanFinalFolder();
                var temp = Globals._tempFolder + "\\Color";
                //Getting list of filter
                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                    throw new ArgumentNullException();
                //var inimanager = new INIFileManager($"{Globals.EventConfigFolder()}\\Config.ini");
                //var filtres = inimanager.GetKeysByValue("FILTRE", "1");

                string[] imageFiles = Directory.GetFiles(temp);
                int incrementFile = 0;
                foreach (string image in imageFiles)
                {
                    incrementFile += 2;
                    CopyImageToAnotherDirectory(image, temp + "\\temp" + incrementFile + ".jpg");

                    foreach (var filtre in filtres)
                    {
                        switch (filtre.ToUpper())
                        {
                            case "SEPIA":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\Sepia\\" + Path.GetFileName(image));
                                //CopyImageToAnotherDirectory(temp + "\\temp" + incrementFile + ".jpg", Globals._tempFolder + "\\Sepia\\temp" + incrementFile + ".jpg");
                                break;
                            case "BLACKWHITE":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\BLACKWHITE\\" + Path.GetFileName(image));
                                //CopyImageToAnotherDirectory(temp + "\\temp" + incrementFile + ".jpg", Globals._tempFolder + "\\BLACKWHITE\\temp" + incrementFile + ".jpg");
                                break;
                            case "ONECOLOR":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\OneColor\\" + Path.GetFileName(image));
                                //CopyImageToAnotherDirectory(temp + "\\temp" + incrementFile + ".jpg", Globals._tempFolder + "\\OneColor\\temp" + incrementFile + ".jpg");
                                break;
                            case "POPART":
                                CopyImageToAnotherDirectory(image, Globals._tempFolder + "\\Popart\\" + Path.GetFileName(image));
                                //CopyImageToAnotherDirectory(temp + "\\temp" + incrementFile + ".jpg", Globals._tempFolder + "\\Popart\\temp" + incrementFile + ".jpg");
                                break;
                        }

                    }


                }
                string[] directories = Directory.GetDirectories(Globals._tempFolder);
                bool duplicated = false;
                foreach (string directory in directories)
                {
                    string FolderName = directory.Split(Path.DirectorySeparatorChar).Last();
                    var used = filtres.Where(x=>x.ToUpper().Contains(FolderName.ToUpper())).FirstOrDefault();
                    
                    if (used!=null)
                    {
                        createImageByFilter(directory, FolderName, imageFile, duplicated);
                        duplicated = true;
                    }

                }
            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
        }

        public static System.Drawing.Bitmap ColorSubstitution(this System.Drawing.Bitmap sourceBitmap)
        {
            try
            {
                // create filter
                HSLFiltering filter = new HSLFiltering();
                //Blue
                filter.Hue = new IntRange(180, 240);
                filter.Saturation = new Range(0.1f, 1.0f);
                filter.Luminance = new Range(0.1f, 0.7f);
                //Green
                //filter.Hue = new IntRange(90, 115);
                //filter.Saturation = new Range(0.3f, 1.0f);
                //filter.Luminance = new Range(0.15f, 0.9f);
                //Red
                //filter.Hue = new IntRange(345, 15);
                //filter.Saturation = new Range(0.4f, 1.0f);
                //filter.Luminance = new Range(0.15f, 1.0f);
                //Yellow
                //filter.Hue = new IntRange(25, 80);
                //filter.Saturation = new Range(0.2f, 0.9f);
                //filter.Luminance = new Range(0.25f, 0.8f);
                filter.UpdateLuminance = false;
                filter.UpdateHue = false;
                // apply the filter
                filter.ApplyInPlace(sourceBitmap);
                return sourceBitmap;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static System.Drawing.Bitmap ApplyFilterSharpen(System.Drawing.Bitmap bmp)
        {
            try
            {
                AForge.Imaging.Filters.Edges filter = new Edges();
                filter.Divisor = 10;
                filter.ProcessAlpha = false;
                filter.Threshold = 15;
                //Sharpen filter = new Sharpen();
                // apply filter
                filter.ApplyInPlace(bmp);
                // Smooth smoothFilter = new SmoothingMode();
                return bmp;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        
            public static string GetLast(this string source, int tail_length)
            {
                if (tail_length >= source.Length)
                    return source;
                return source.Substring(source.Length - tail_length);
            }
       


        public static void createImageByFilter(string currentDirectory, string filter, string imageFile, bool duplicated)
        {
            try
            {
                ImageFilterManager imageManager = new ImageFilterManager();
                //Apply filter on images
                string[] imagesNonFiltered = Directory.GetFiles(currentDirectory);
                int i = 0;
                foreach (string image in imagesNonFiltered)
                {
                    i++;
                    imageManager.ApplyOneFilter(image, filter, i);
                }

                string[] directories = Directory.GetDirectories(Globals._tempFolder);;
                foreach (string directory in directories)
                {

                    int indexChar = 0;
                    string FolderName = directory.Split(Path.DirectorySeparatorChar).Last();
                    if (FolderName.ToLower() != "color")
                    {
                       
                        string[] imageDirectoryFiles = Directory.GetFiles(directory);
                        if(imageDirectoryFiles.Length <= 3)
                        {
                            foreach (string image in imageDirectoryFiles)
                            {
                                string imageTemp = Path.GetFileNameWithoutExtension(image);
                                indexChar = Convert.ToInt32(GetLast(imageTemp, 1));
                                indexChar++;
                                CopyImageToAnotherDirectory(image, directory + "\\temp" + indexChar + ".jpg");
                            }
                        }
                    }
                }
                    int index = 0;
                int incrementFile = 0;
                string currentImage = "";
                string cadre = imageFile;
                string[] imageFiles = Directory.GetFiles(Globals._tempFolder + "\\" + filter);
                foreach (string image in imageFiles)
                {
                    incrementFile++;
                    string filename = Globals.tempDir + "\\Temp\\" + filter + "\\IMG_" + incrementFile + ".jpg";
                    if (currentImage == "")
                    {
                        currentImage = cadre;
                    }
                    System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(currentImage);
                    int offset = 0;
                    index++;
                    int yset = 0;
                    int width = 0;
                    int height = 0;

                    System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(image);

                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(baseImage.Width, baseImage.Height);
                    imageOverlay = ImageUtility.ResizeImage(imageOverlay, (baseImage.Width / 2) - 30, (baseImage.Height / 4) - 40);

                    images.Add(baseImage);
                    images.Add(imageOverlay);
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        //set background color
                        //g.Clear(System.Drawing.Color.Black);
                        g.CompositingQuality = CompositingQuality.HighQuality;
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                        int increment = 0;
                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            increment++;
                            if (increment == 1)
                            {
                                offset = 0;
                                yset = 0;
                                width = baseImage.Width;
                                height = baseImage.Height;
                            }
                            else
                            {
                                switch (index)
                                {
                                    case 1:
                                        if(Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[0].x;
                                            yset = Globals._shootDynamic[0].y;
                                            width = Globals._shootDynamic[0].width;
                                            height = Globals._shootDynamic[0].height;
                                        }
                                        else
                                        {
                                            offset = 20;
                                            yset = 10;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                       
                                        break;
                                    case 2:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[0].width + Globals._shootDynamic[0].x + 50;
                                            yset = Globals._shootDynamic[0].y;
                                            width = Globals._shootDynamic[0].width;
                                            height = Globals._shootDynamic[0].height;
                                        }
                                        else
                                        {
                                            offset = imageOverlay.Width + 30;
                                            yset = 10;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                        break;
                                    case 3:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[1].x;
                                            yset = Globals._shootDynamic[1].y;
                                            width = Globals._shootDynamic[1].width;
                                            height = Globals._shootDynamic[1].height;
                                        }
                                        else
                                        {
                                            offset = 20;
                                            yset = imageOverlay.Height + 20;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                        break;
                                    case 4:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[1].width + Globals._shootDynamic[1].x + 50;
                                            yset = Globals._shootDynamic[1].y;
                                            width = Globals._shootDynamic[1].width;
                                            height = Globals._shootDynamic[1].height;
                                        }
                                        else
                                        {
                                            offset = imageOverlay.Width + 30;
                                            yset = imageOverlay.Height + 20;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                        break;
                                    case 5:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[2].x;
                                            yset = Globals._shootDynamic[2].y;
                                            width = Globals._shootDynamic[2].width;
                                            height = Globals._shootDynamic[2].height;
                                        }
                                        else
                                        {
                                            offset = 25;
                                            yset = (imageOverlay.Height * (2)) + 75;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                        break;
                                    case 6:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[2].width + Globals._shootDynamic[2].x + 50;
                                            yset = Globals._shootDynamic[2].y;
                                            width = Globals._shootDynamic[2].width;
                                            height = Globals._shootDynamic[2].height;
                                        }
                                        else
                                        {
                                            offset = imageOverlay.Width + 35;
                                            yset = (imageOverlay.Height * (2)) + 30;
                                            width = imageOverlay.Width;
                                            height = imageOverlay.Height;
                                        }
                                        break;
                                }
                            }


                            g.DrawImage(imageTemp, new System.Drawing.Rectangle(offset, yset, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                        string image_overlay = Path.GetFileNameWithoutExtension(cadre) + "_overlay.png";
                        image_overlay = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                        if (File.Exists(image_overlay))
                        {
                            string fileOverlayed = Path.GetDirectoryName(cadre) + "\\" + image_overlay;
                            CreateoverlayedFrame(filename, fileOverlayed);
                        }
                    }
                    //finalImage.Save(filename, ImageFormat.Jpeg);

                    finalImage.Dispose();
                    currentImage = filename;
                }
                File.Copy(currentImage, Globals._finalFolder + "\\Original\\IMG_" + filter + ".jpg");
                divideBitmap(currentImage, Globals._finalFolder + "\\IMG_" + filter + ".jpg");
            }
            catch(Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
           
        }

        public static void createImageMultiShootByFilter(string currentDirectory, string filter)
        {
            try
            {
                ImageFilterManager imageManager = new ImageFilterManager();
                //Apply filter on images
                string[] imagesNonFiltered = Directory.GetFiles(currentDirectory);
                int i = 0;
                foreach (string image in imagesNonFiltered)
                {
                    i++;
                    imageManager.ApplyOneFilter(image, filter, i);
                }
                IniUtility ini = new IniUtility(Globals._iniFile);
                string codeMultishoot = "";
                if (ini.KeyExists("code", "MULTISHOOT"))
                {
                    codeMultishoot = ini.Read("code", "MULTISHOOT");
                }

                int index = 0;
                int incrementFile = 0;
                string currentImage = "";
                string imageBase = "";
                string originalBaseImage = "";
                string finalFileImage = "";
                //string cadre = Globals._Multishoot;

                string[] imageFiles = Directory.GetFiles(Globals._tempFolder + "\\" + filter);
                foreach (string image in imageFiles)
                {
                    //originalBaseImage = cadre + "Multishoot" + codeMultishoot + ".png";
                    originalBaseImage = Globals._currentBackground;//cadre + "file_frame.png";
                    //if (!File.Exists(originalBaseImage))
                    //{
                    //    originalBaseImage = cadre + "file_frame.jpg";
                    //}
                    IniUtility iniMultishoot = new IniUtility(Globals._iniMultishoot);
                    string section = "MULTISHOOT" + codeMultishoot.ToUpper();
                    incrementFile++;
                    string filename = Globals._tmpDir + "\\Temp\\" + filter + "\\IMG_" + incrementFile + ".jpg";
                    if (currentImage == "")
                    {
                        currentImage = originalBaseImage;
                        //currentImage = cadre + "Multishoot" + codeMultishoot + ".png";
                        imageBase = originalBaseImage;
                        //imageBase = "Multishoot" + codeMultishoot + ".png";
                    }
                    System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(currentImage);
                    int offset = 0;
                    index++;
                    int yset = 0;
                    int width = 0;
                    int height = 0;

                    System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(image);

                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(baseImage.Width, baseImage.Height);
                    string widthTemp = "width" + incrementFile;
                    string heightTemp = "height" + incrementFile;
                    //int widthIni = Convert.ToInt32(iniMultishoot.Read(widthTemp, section));
                    //int heightini = Convert.ToInt32(iniMultishoot.Read(heightTemp, section));
                    //imageOverlay = ImageUtility.ResizeImage(imageOverlay, widthIni, heightini);

                    images.Add(baseImage);
                    images.Add(imageOverlay);
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        //go through each image and draw it on the final image (Notice the offset; since I want to overlay the images i won't have any offset between the images in the finalImage)
                        int increment = 0;

                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            increment++;
                            System.Drawing.Bitmap bmpTemp = imageTemp;
                            if (increment == 1)
                            {
                                offset = 0;
                                yset = 0;
                                width = baseImage.Width;
                                height = baseImage.Height;
                            }
                            else
                            {
                                switch (index)
                                {
                                    case 1:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[0].x;
                                            yset = Globals._shootDynamic[0].y; ;
                                            width = Globals._shootDynamic[0].width;
                                            height = Globals._shootDynamic[0].height;
                                            int _width = 0;
                                            int _height = 0;
                                            _width = Globals._shootDynamic[0].width;
                                            _height = Globals._shootDynamic[0].height;
                                            bmpTemp = ImageUtility.ResizeImage(imageTemp, _width, _height);
                                            if (Globals._shootDynamic[0].rotation > 0)
                                            {
                                                bmpTemp = ImageUtility.RotateBitmap(Globals._shootDynamic[0].rotation, bmpTemp);

                                                yset = yset - (Globals.heightRotate - height);
                                                width = bmpTemp.Width;
                                                height = bmpTemp.Height;
                                            }

                                        }
                                        else
                                        {
                                            if (iniMultishoot.KeyExists("x1", section))
                                            {
                                                offset = Convert.ToInt32(iniMultishoot.Read("x1", section));
                                                yset = Convert.ToInt32(iniMultishoot.Read("y1", section));
                                                width = Convert.ToInt32(iniMultishoot.Read("width1", section));
                                                height = Convert.ToInt32(iniMultishoot.Read("height1", section));
                                            }
                                        }


                                        break;
                                    case 2:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[1].x;
                                            yset = Globals._shootDynamic[1].y; ;
                                            width = Globals._shootDynamic[1].width;
                                            height = Globals._shootDynamic[1].height;
                                           
                                            int _width = 0;
                                            int _height = 0;
                                          
                                            _width = Globals._shootDynamic[1].width;
                                            _height = Globals._shootDynamic[1].height;
                                            bmpTemp = ImageUtility.ResizeImage(imageTemp, _width, _height);
                                            if (Globals._shootDynamic[1].rotation > 0)
                                            {
                                                bmpTemp = ImageUtility.RotateBitmap(Globals._shootDynamic[1].rotation, bmpTemp);
                                               
                                                yset = yset - (Globals.heightRotate - height);
                                                width = bmpTemp.Width;
                                                height = bmpTemp.Height;
                                            }
                                            
                                        }
                                        else
                                        {
                                            if (iniMultishoot.KeyExists("x2", section))
                                            {
                                                offset = Convert.ToInt32(iniMultishoot.Read("x2", section));
                                                yset = Convert.ToInt32(iniMultishoot.Read("y2", section));
                                                width = Convert.ToInt32(iniMultishoot.Read("width2", section));
                                                height = Convert.ToInt32(iniMultishoot.Read("height2", section));
                                            }
                                        }
                                        break;
                                    case 3:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[2].x;
                                            yset = Globals._shootDynamic[2].y; ;
                                            width = Globals._shootDynamic[2].width;
                                            height = Globals._shootDynamic[2].height;
                                            
                                            int _width = 0;
                                            int _height = 0;
                                            _width = Globals._shootDynamic[2].width;
                                            _height = Globals._shootDynamic[2].height;
                                            bmpTemp = ImageUtility.ResizeImage(imageTemp, _width, _height);
                                            if (Globals._shootDynamic[2].rotation > 0)
                                            {
                                                bmpTemp = ImageUtility.RotateBitmap(Globals._shootDynamic[2].rotation, bmpTemp);

                                                yset = yset - (Globals.heightRotate - height);
                                                width = bmpTemp.Width;
                                                height = bmpTemp.Height;
                                            }
                                        }
                                        else
                                        {
                                            if (iniMultishoot.KeyExists("x3", section))
                                            {
                                                offset = Convert.ToInt32(iniMultishoot.Read("x3", section));
                                                yset = Convert.ToInt32(iniMultishoot.Read("y3", section));
                                                width = Convert.ToInt32(iniMultishoot.Read("width3", section));
                                                height = Convert.ToInt32(iniMultishoot.Read("height3", section));
                                            }
                                        }
                                        break;
                                    case 4:
                                        if (Globals._shootDynamic.Length > 0)
                                        {
                                            offset = Globals._shootDynamic[3].x;
                                            yset = Globals._shootDynamic[3].y; ;
                                            width = Globals._shootDynamic[3].width;
                                            height = Globals._shootDynamic[3].height;
                                          
                                            int _width = 0;
                                            int _height = 0;
                                            _width = Globals._shootDynamic[3].width;
                                            _height = Globals._shootDynamic[3].height;
                                            bmpTemp = ImageUtility.ResizeImage(imageTemp, _width, _height);
                                            if (Globals._shootDynamic[3].rotation > 0)
                                            {
                                                bmpTemp = ImageUtility.RotateBitmap(Globals._shootDynamic[3].rotation, bmpTemp);

                                                yset = yset - (Globals.heightRotate - height);
                                                width = bmpTemp.Width;
                                                height = bmpTemp.Height;
                                            }
                                        }
                                        else
                                        {
                                            if (iniMultishoot.KeyExists("x4", section))
                                            {
                                                offset = Convert.ToInt32(iniMultishoot.Read("x4", section));
                                                yset = Convert.ToInt32(iniMultishoot.Read("y4", section));
                                                width = Convert.ToInt32(iniMultishoot.Read("width4", section));
                                                height = Convert.ToInt32(iniMultishoot.Read("height4", section));
                                            }
                                        }
                                        break;
                                }
                            }
                            g.DrawImage(bmpTemp, new System.Drawing.Rectangle(offset, yset, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                    }

                    //finalImage.Save(filename, ImageFormat.Jpeg);

                    finalImage.Dispose();
                    currentImage = filename;
                    finalFileImage = filename;
                }
                string image_overlay = Path.GetFileNameWithoutExtension(originalBaseImage) + "_overlay.png";
                image_overlay = Path.GetDirectoryName(originalBaseImage) + "\\Overlay\\" + image_overlay;
                if (File.Exists(image_overlay))
                {
                    //string fileOverlayed = Path.GetDirectoryName(imageBase) + "\\" + image_overlay;
                    CreateoverlayedFrame(finalFileImage, image_overlay);
                }
                File.Copy(finalFileImage, Globals._finalFolder + "\\IMG_" + filter + ".jpg");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }

        }//

        private static System.Drawing.Bitmap RotateBitmap(int orientation, System.Drawing.Bitmap bmp)
        {
            Globals.widthRotate = 0;
            Globals.heightRotate = 0;
            string tempFile = Globals._rotateFolder + "\\Tem_rotate.jpg";
            if (File.Exists(tempFile))
            {
                File.Delete(tempFile);
            }

            var stream = new System.IO.MemoryStream();
            bmp.Save(stream, ImageFormat.Jpeg);
            using (var reader = ImageReader.Create(stream))
            using (var rotate = new Rotate(orientation, System.Drawing.Color.YellowGreen, Aurigma.GraphicsMill.Transforms.InterpolationMode.High))
            using (var writer = ImageWriter.Create(tempFile))
            {
                Pipeline.Run(reader + rotate + writer);

            }

            Aurigma.GraphicsMill.Bitmap toReturn = new Aurigma.GraphicsMill.Bitmap(tempFile);
            toReturn.Channels.SetAlpha(1.0f);
            toReturn.Channels.Transparentize(System.Drawing.Color.YellowGreen, 0.2f);
            Globals.widthRotate = toReturn.Width;
            Globals.heightRotate = toReturn.Height;
            System.Drawing.Bitmap drawToReturn = (System.Drawing.Bitmap)toReturn;
            //drawToReturn = clearBackGround(drawToReturn);
            //drawToReturn.MakeTransparent(System.Drawing.Color.DarkGray);
            return drawToReturn;

            //ImageManipulator img = new ImageManipulator(bmp);
            //bmp = img.RotateBitmap(orientation, bmp);
            //bmp.Save(tempFile, ImageFormat.Jpeg);
            //return bmp;
        }

        //public static System.Drawing.Bitmap RotateBitmap(int orientation, System.Drawing.Bitmap bmp)
        //{
        //    var transformBitmap = (TransformedBitmap);
        //    RotateTransform rotateTransform = (RotateTransform)(transformBitmap.Transform);
        //    rotateTransform.Angle += 90;
        //    image1.Source = transformBitmap.Clone();
        //}

        public static string createImageFileMultishoot(string code)
        {
            try
            {
                return Globals._Multishoot + "Multishoot" + code + ".jpg";
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static void CleanFinalFolder()
        {
            try
            {
                DirectoryInfo directoryFinal = new DirectoryInfo(Globals._finalFolder);
                //writeDebug(Globals._debugText, directoryFinal.GetFiles().Count().ToString() + " Files found");
                foreach (var item in directoryFinal.GetFiles())
                {
                    System.GC.Collect();
                    System.GC.WaitForFullGCComplete();
                    File.Delete(item.FullName);
                }
            }
            catch(Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
           

        }

        public static void createPhotoPolaroid(string imageFile, List<string> filtres)
        {
            try
            {
                CleanFinalFolder();
                string cadre = imageFile;
                string finalFileName = "";
                var temp = Globals._tempFolder;
                int index = 0;
                string[] imageFiles = Directory.GetFiles(Globals._tmpDir);
                String tempFile = imageFiles[0];

                var exepath = Globals.ExeDir();
                if (!File.Exists($"{Globals.EventAssetFolder()}\\Config.ini"))
                    throw new ArgumentNullException();
                //var inimanager = new INIFileManager($"{Globals.EventConfigFolder()}\\Config.ini");
                //var filtres = inimanager.GetKeysByValue("FILTRE", "1");

                System.Drawing.Bitmap baseImage = new System.Drawing.Bitmap(cadre);
                ImageFilterManager imageManager = new ImageFilterManager();

                imageManager.Applyfilter(imageFiles[0], filtres);
                imageFiles = Directory.GetFiles(Globals._tmpDir);
                foreach (String file in imageFiles)
                {
                    String dirDestination = Globals._tempFolder + "\\" + Path.GetFileName(file);
                    File.Copy(file, dirDestination);
                }

                string[] imageTempFiles = Directory.GetFiles(temp);
                foreach (string image in imageTempFiles)
                {
                    String filter = Path.GetFileNameWithoutExtension(image);
                    filter = filter.Substring(filter.IndexOf(",") + 2);
                    index++;
                    System.Drawing.Bitmap imageOverlay = new System.Drawing.Bitmap(image);
                    string filename = Globals._finalFolder + "\\" + CreateFileName(index, filter);
                    List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
                    System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(baseImage.Width, baseImage.Height);
                    int heightPolaroid = baseImage.Height - ((baseImage.Height / 3));
                    heightPolaroid = heightPolaroid - 100 ;
                    imageOverlay = ImageUtility.ResizeImage(imageOverlay, baseImage.Width - 95, heightPolaroid);
                    //heightPolaroid = heightPolaroid - 10;
                    //imageOverlay = ImageUtility.ResizeImage(imageOverlay, baseImage.Width - 5, heightPolaroid);
                    images.Add(baseImage);
                    images.Add(imageOverlay);
                    using (Graphics g = Graphics.FromImage(finalImage))
                    {
                        int offset = 0;
                        int ySet = 0;
                        int width = 0;
                        int height = 0;
                        int increment = 0;
                        foreach (System.Drawing.Bitmap imageTemp in images)
                        {
                            System.Drawing.Bitmap BmpTemp = imageTemp;
                            increment++;
                            switch (increment)
                            {
                                case 1:
                                    offset = 0;
                                    ySet = 0;
                                    width = baseImage.Width;
                                    height = baseImage.Height;
                                    break;
                                case 2:
                                    if(Globals._shootDynamic.Length > 0)
                                    {
                                        offset = Globals._shootDynamic[0].x;
                                        ySet = Globals._shootDynamic[0].y;
                                        width = Globals._shootDynamic[0].width;
                                        height = Globals._shootDynamic[0].height;
                                        BmpTemp = ImageUtility.ResizeImage(imageTemp, width, height);
                                    }
                                    else
                                    {
                                        offset = 45;
                                        ySet = 110;
                                        width = imageOverlay.Width;
                                        height = imageOverlay.Height;
                                    }
                                    
                                    break;
                            }
                            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.CompositingQuality = CompositingQuality.HighQuality;
                            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            g.DrawImage(BmpTemp, new System.Drawing.Rectangle(offset, ySet, width, height));
                        }
                    }
                    //Saving image...
                    ImageCodecInfo myImageCodecInfo;
                    System.Drawing.Imaging.Encoder myEncoder;
                    EncoderParameter myEncoderParameter;
                    EncoderParameters myEncoderParameters;

                    // Get an ImageCodecInfo object that represents the JPEG codec.
                    myImageCodecInfo = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // EncoderParameter object in the array.
                    using (myEncoderParameters = new EncoderParameters(1))
                    {
                        // Save the bitmap as a JPEG file with quality level.            
                        myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        finalFileName = filename;
                        finalImage.Save(filename, myImageCodecInfo, myEncoderParameters);
                        string image_overlay = Path.GetFileNameWithoutExtension(cadre) + "_overlay.png";
                        image_overlay = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                        if (File.Exists(image_overlay))
                        {
                            //string fileOverlayed = Path.GetDirectoryName(cadre) + "\\Overlay\\" + image_overlay;
                            CreateoverlayedFrame(finalFileName, image_overlay);
                        }
                    }
                    //finalImage.Save(filename, ImageFormat.Jpeg);
                    
                }

                
                bool used = filtres.Contains("COLOR");
                if (!used)
                {
                    imageFiles = Directory.GetFiles(Globals._finalFolder);
                    int keyIndex = Array.FindIndex(imageFiles, w => w.Contains("_COLOR"));
                    File.Delete(imageFiles[keyIndex]);
                }
            }
            catch (Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            //finally { WaitEvent.Set(); }
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }



        //public static void SaveJpeg(string path, System.Drawing.Image image, int quality)
        //{
        //    //ensure the quality is within the correct range
        //    if ((quality < 0) || (quality > 100))
        //    {
        //        //create the error message
        //        string error = string.Format("Jpeg image quality must be between 0 and 100, with 100 being the highest quality.  A value of {0} was specified.", quality);
        //        //throw a helpful exception
        //        throw new ArgumentOutOfRangeException(error);
        //    }

        //    //create an encoder parameter for the image quality
        //    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
        //    //get the jpeg codec
        //    ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

        //    //create a collection of all parameters that we will pass to the encoder
        //    EncoderParameters encoderParams = new EncoderParameters(1);
        //    //set the quality parameter for the codec
        //    encoderParams.Param[0] = qualityParam;
        //    //save the image using the codec and the parameters
        //    image.Save(path, jpegCodec, encoderParams);
        //}

        //public static ImageCodecInfo GetEncoderInfo(string mimeType)
        //{
        //    //do a case insensitive search for the mime type
        //    string lookupKey = mimeType.ToLower();

        //    //the codec to return, default to null
        //    ImageCodecInfo foundCodec = null;

        //    //if we have the encoder, get it to return
        //    if (Encoder.ContainsKey(lookupKey))
        //    {
        //        //pull the codec from the lookup
        //        foundCodec = Encoders[lookupKey];
        //    }

        //    return foundCodec;
        //}

        public static System.Drawing.Bitmap Crop(System.Drawing.Bitmap source)
        {
            try
            {
                if (source.Width == source.Height) return source;
                int size = Math.Min(source.Width, source.Height);
                var sourceRect = new Rectangle((source.Width - size) / 2, (source.Height - size) / 2, size, size);
                var cropped = new System.Drawing.Bitmap(size, size);
                using (var g = Graphics.FromImage(cropped))
                    g.DrawImage(source, 0, 0, sourceRect, GraphicsUnit.Pixel);
                return cropped;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static String CreateFileName(int index, string filter)
        {
            DateTime dt = DateTime.Now;
            String formattedDate = (String.Format("{0:u}", dt));
            formattedDate = formattedDate.Replace(":", "");
            formattedDate = formattedDate.Trim();
            return "IMG_" + filter + "_" + index + "_" + formattedDate + ".jpg";
        }

        public static void divideBitmap(string path, string newPath)
        {
            try
            {
                System.Drawing.Bitmap originalImage = new System.Drawing.Bitmap(System.Drawing.Image.FromFile(path));
                Rectangle rect = new Rectangle(0, 0, originalImage.Width / 2, originalImage.Height);
                System.Drawing.Bitmap firstHalf = originalImage.Clone(rect, originalImage.PixelFormat);
                firstHalf.Save(newPath);

                //by dina
                originalImage.Dispose();
                firstHalf.Dispose();
            }
            catch(Exception ex) { Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]"); }
            
        }

        public static System.Drawing.Bitmap divideBitmapToBitmap(string path)
        {
            try
            {
                System.Drawing.Bitmap originalImage = new System.Drawing.Bitmap(System.Drawing.Image.FromFile(path));
                Rectangle rect = new Rectangle(0, 0, originalImage.Width / 2, originalImage.Height);
                System.Drawing.Bitmap firstHalf = originalImage.Clone(rect, originalImage.PixelFormat);
                return firstHalf;
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }
            return null;
        }

        public static BitmapImage convertBitmapToBitmapImage(System.Drawing.Bitmap image)
        {
            try
            {
                using (var memory = new MemoryStream())
                {
                    image.Save(memory, ImageFormat.Png);
                    memory.Position = 0;

                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();

                    return bitmapImage;
                }
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }

            return null;
        }

        public static System.Drawing.Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            // BitmapImage bitmapImage = new BitmapImage(new Uri("../Images/test.png", UriKind.Relative));
            try
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    BitmapEncoder enc = new BmpBitmapEncoder();
                    enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                    enc.Save(outStream);
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                    return new System.Drawing.Bitmap(bitmap);
                }
            }
            catch(Exception ex)
            {
                Log.Error("L'erreur suivante a été détectée: [[" + ex.Message + "]]");
            }

            return null;
            
        }

        public static System.Drawing.Bitmap GradientBasedEdgeDetectionFilter(
                                this System.Drawing.Bitmap sourceBitmap,
                                EdgeFilterType filterType,
                                DerivativeLevel derivativeLevel,
                                float redFactor = 1.0f,
                                float greenFactor = 1.0f,
                                float blueFactor = 1.0f,
                                byte threshold = 0)
        {
            BitmapData sourceData =
                       sourceBitmap.LockBits(new Rectangle(0, 0,
                       sourceBitmap.Width, sourceBitmap.Height),
                       ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format32bppArgb);


            byte[] pixelBuffer = new byte[sourceData.Stride *
                                          sourceData.Height];


            byte[] resultBuffer = new byte[sourceData.Stride *
                                           sourceData.Height];


            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0,
                                       pixelBuffer.Length);


            sourceBitmap.UnlockBits(sourceData);


            int derivative = (int)derivativeLevel;
            int byteOffset = 0;
            int blueGradient, greenGradient, redGradient = 0;
            double blue = 0, green = 0, red = 0;


            bool exceedsThreshold = false;


            for (int offsetY = 1; offsetY < sourceBitmap.Height - 1; offsetY++)
            {
                for (int offsetX = 1; offsetX <
                    sourceBitmap.Width - 1; offsetX++)
                {
                    byteOffset = offsetY * sourceData.Stride +
                                 offsetX * 4;


                    blueGradient =
                    Math.Abs(pixelBuffer[byteOffset - 4] -
                    pixelBuffer[byteOffset + 4]) / derivative;


                    blueGradient +=
                    Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                    pixelBuffer[byteOffset + sourceData.Stride]) / derivative;


                    byteOffset++;


                    greenGradient =
                    Math.Abs(pixelBuffer[byteOffset - 4] -
                    pixelBuffer[byteOffset + 4]) / derivative;


                    greenGradient +=
                    Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                    pixelBuffer[byteOffset + sourceData.Stride]) / derivative;


                    byteOffset++;


                    redGradient =
                    Math.Abs(pixelBuffer[byteOffset - 4] -
                    pixelBuffer[byteOffset + 4]) / derivative;


                    redGradient +=
                    Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                    pixelBuffer[byteOffset + sourceData.Stride]) / derivative;


                    if (blueGradient + greenGradient + redGradient > threshold)
                    { exceedsThreshold = true; }
                    else
                    {
                        byteOffset -= 2;


                        blueGradient = Math.Abs(pixelBuffer[byteOffset - 4] -
                                                pixelBuffer[byteOffset + 4]);
                        byteOffset++;


                        greenGradient = Math.Abs(pixelBuffer[byteOffset - 4] -
                                                 pixelBuffer[byteOffset + 4]);
                        byteOffset++;


                        redGradient = Math.Abs(pixelBuffer[byteOffset - 4] -
                                               pixelBuffer[byteOffset + 4]);


                        if (blueGradient + greenGradient + redGradient > threshold)
                        { exceedsThreshold = true; }
                        else
                        {
                            byteOffset -= 2;


                            blueGradient =
                            Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                            pixelBuffer[byteOffset + sourceData.Stride]);


                            byteOffset++;


                            greenGradient =
                            Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                            pixelBuffer[byteOffset + sourceData.Stride]);


                            byteOffset++;


                            redGradient =
                            Math.Abs(pixelBuffer[byteOffset - sourceData.Stride] -
                            pixelBuffer[byteOffset + sourceData.Stride]);


                            if (blueGradient + greenGradient +
                                      redGradient > threshold)
                            { exceedsThreshold = true; }
                            else
                            {
                                byteOffset -= 2;


                                blueGradient =
                                Math.Abs(pixelBuffer[byteOffset - 4 - sourceData.Stride] -
                                pixelBuffer[byteOffset + 4 + sourceData.Stride]) / derivative;


                                blueGradient +=
                                Math.Abs(pixelBuffer[byteOffset - sourceData.Stride + 4] -
                                pixelBuffer[byteOffset + sourceData.Stride - 4]) / derivative;


                                byteOffset++;


                                greenGradient =
                                Math.Abs(pixelBuffer[byteOffset - 4 - sourceData.Stride] -
                                pixelBuffer[byteOffset + 4 + sourceData.Stride]) / derivative;


                                greenGradient +=
                                Math.Abs(pixelBuffer[byteOffset - sourceData.Stride + 4] -
                                pixelBuffer[byteOffset + sourceData.Stride - 4]) / derivative;


                                byteOffset++;


                                redGradient =
                                Math.Abs(pixelBuffer[byteOffset - 4 - sourceData.Stride] -
                                pixelBuffer[byteOffset + 4 + sourceData.Stride]) / derivative;


                                redGradient +=
                                Math.Abs(pixelBuffer[byteOffset - sourceData.Stride + 4] -
                                pixelBuffer[byteOffset + sourceData.Stride - 4]) / derivative;


                                if (blueGradient + greenGradient + redGradient > threshold)
                                { exceedsThreshold = true; }
                                else
                                { exceedsThreshold = false; }
                            }
                        }
                    }


                    byteOffset -= 2;


                    if (exceedsThreshold)
                    {
                        if (filterType == EdgeFilterType.EdgeDetectMono)
                        {
                            blue = green = red = 255;
                        }
                        else if (filterType == EdgeFilterType.EdgeDetectGradient)
                        {
                            blue = blueGradient * blueFactor;
                            green = greenGradient * greenFactor;
                            red = redGradient * redFactor;
                        }
                        else if (filterType == EdgeFilterType.Sharpen)
                        {
                            blue = pixelBuffer[byteOffset] * blueFactor;
                            green = pixelBuffer[byteOffset + 1] * greenFactor;
                            red = pixelBuffer[byteOffset + 2] * redFactor;
                        }
                        else if (filterType == EdgeFilterType.SharpenGradient)
                        {
                            blue = pixelBuffer[byteOffset] + blueGradient * blueFactor;
                            green = pixelBuffer[byteOffset + 1] + greenGradient * greenFactor;
                            red = pixelBuffer[byteOffset + 2] + redGradient * redFactor;
                        }
                    }
                    else
                    {
                        if (filterType == EdgeFilterType.EdgeDetectMono ||
                            filterType == EdgeFilterType.EdgeDetectGradient)
                        {
                            blue = green = red = 0;
                        }
                        else if (filterType == EdgeFilterType.Sharpen ||
                                 filterType == EdgeFilterType.SharpenGradient)
                        {
                            blue = pixelBuffer[byteOffset];
                            green = pixelBuffer[byteOffset + 1];
                            red = pixelBuffer[byteOffset + 2];
                        }
                    }


                    blue = (blue > 255 ? 255 :
                           (blue < 0 ? 0 :
                            blue));


                    green = (green > 255 ? 255 :
                            (green < 0 ? 0 :
                             green));


                    red = (red > 255 ? 255 :
                          (red < 0 ? 0 :
                           red));


                    resultBuffer[byteOffset] = (byte)blue;
                    resultBuffer[byteOffset + 1] = (byte)green;
                    resultBuffer[byteOffset + 2] = (byte)red;
                    resultBuffer[byteOffset + 3] = 255;
                }
            }

            System.Drawing.Bitmap resultBitmap = new System.Drawing.Bitmap(sourceBitmap.Width,
                                             sourceBitmap.Height);


            BitmapData resultData =
                       resultBitmap.LockBits(new Rectangle(0, 0,
                       resultBitmap.Width, resultBitmap.Height),
                       ImageLockMode.WriteOnly,
                       System.Drawing.Imaging.PixelFormat.Format32bppArgb);


            Marshal.Copy(resultBuffer, 0, resultData.Scan0,
                                       resultBuffer.Length);


            resultBitmap.UnlockBits(resultData);


            return resultBitmap;
        }


        public enum EdgeFilterType
        {
            None,
            EdgeDetectMono,
            EdgeDetectGradient,
            Sharpen,
            SharpenGradient,
        }

        public enum DerivativeLevel
        {
            First = 1,
            Second = 2
        }

    }

    public class ColorSubstitutionFilter
    {
        private int thresholdValue = 10;
        public int ThresholdValue
        {
            get { return thresholdValue; }
            set { thresholdValue = value; }
        }

        private System.Drawing.Color sourceColor = System.Drawing.Color.White;
        public System.Drawing.Color SourceColor
        {
            get { return sourceColor; }
            set { sourceColor = value; }
        }

        private System.Drawing.Color newColor = System.Drawing.Color.White;
        public System.Drawing.Color NewColor
        {
            get { return newColor; }
            set { newColor = value; }
        }
    }
}
