﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Selfizee.Models.Enum;
using log4net;
using Selfizee.Models;
using System.Linq;

namespace Selfizee
{
    public class Globals
    {
        private static string appconfig = @"\AppConfig.ini";
        public static string ScreenType = "";
        public static String exerDir = Directory.GetCurrentDirectory();
        public static bool connected = false;
        public static bool add_event = false;
        
        public static String currDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"Selfizee");
        public static String _rotateFolder = currDir + "\\Rotate";
        public static int countdown = 7;
        public static int timeOut = 10;
        public static string gift = "";
        public static String directory_Target = "D:\\Sauvegarde_fichier\\";
        public static String imageFolder = @"View";
        public static String customizedFolder = @"Customized";
        public static String _imageFrame = @"Frames";
        public static String _Event = @"Events";
        public static String _Assets = @"Events";
        public static int widthRotate = 0;
        public static int heightRotate = 0;
        public static String imageSavedFolder = @"Saved";
        public static String downloadDirectory = "C:\\EVENTS\\Download";
        public static String TempFolder = @"Temp";
        public static String _finalDir = @"Final";
        public static String _assetsFolder = "C:\\EVENTS\\Assets";
        public static String _finalFolder = currDir + $"\\{_finalDir}";
        public static String _customizedFolder = currDir + $"\\{customizedFolder}";
        public static String _dataFolder = currDir + "\\Data";
        public static String _dividedStrip = _customizedFolder + "\\Strip\\Divided";
        public static String _OriginalStrip = _customizedFolder + "\\Strip\\Original";
        public static String tempDir = currDir + $"\\{imageFolder}";
        public static String _tempFolder = currDir + $"\\{TempFolder}";
        public static String _waitingFolder = currDir + "\\Waiting";
        public static String _ViewTempFolder = currDir + $"\\{imageFolder}\\{TempFolder}";
        public static String archiveDir = currDir + $"\\..\\..\\Archives";
        public static String _tmpDir = currDir + $"\\{imageFolder}";
        public static String _saved = currDir + $"\\{imageSavedFolder}";
        public static String _MediaFolder = "C:\\EVENTS\\Media";
        public static String _Frame = EventAssetFolder() + $"\\{_imageFrame}\\Cadre\\" + "cadre.png";
        public static String _Strip = EventAssetFolder() + $"\\{_imageFrame}\\Strip\\" + "strip.jpg";
        public static string _cadre = EventAssetFolder() + $"\\Frames\\Cadre\\cadre.png";
        public static string _greenfolder = EventAssetFolder() + $"\\GreenScreen";
        public static String _Multishoot = EventAssetFolder() + $"\\{_imageFrame}\\Multishoot\\";
        public static String _Polaroid = EventAssetFolder() + $"\\{_imageFrame}\\Polaroid\\" + "polaroid.jpg";
        public static String _bgFond = AbsoluteDefaultAssetFolder() + $"\\Backgrounds\\background.jpg";
        public static String _bgAccueil = EventAssetFolder() + $"\\Backgrounds\\BG_ACCUEIL.jpg";
        public static String _bgFiltre = EventAssetFolder() + $"\\Backgrounds\\BG_FILTRE.jpg";
        public static String _bgMerci = EventAssetFolder() + $"\\Backgrounds\\BG_MERCI.jpg";
        public static String _accueilBouton = EventAssetFolder() + $"\\Buttons\\bouton.png";
        public static String _eventChoices = EventAssetFolder() + $"\\{_Event}";
        public static String _customEventChoices = EventAssetFolder() + $"\\{_Event}\\Custom";
        public static String _iniFile = EventAssetFolder() + "\\Config.ini";
        public static String _iniMultishoot = exerDir + "\\Multishoot.ini";
        public static String _arrowLeft = $"{EventAssetFolder()}\\Buttons\\arrowLeft.png";
        public static String _arrowRight = $"{EventAssetFolder()}\\Buttons\\arrowRight.png";
        public static String originalStripFrame = $"{EventAssetFolder()}\\Frames\\Strip";
        public static String stripFrame = currDir + "\\Temporary";
        public static String _allEvents = exerDir + $"\\Images\\{_Event}";
        public static String _currentChromakeyBG = "";
        public static string _originalString = "";
        public static bool _DataFirstPage = false;
        public static bool getRemoteElements = false;
        public static List<MandatoryControl> obligatoryField {get;set;}
        public static String _gifLoader = currDir + $"\\Images\\Gifs\\Loader.gif";
        public static string _flashFile = $"{EventAssetFolder()}\\Flash";
        public static string _debugText = currDir + $"\\Debug.txt";
        public static String _currentBackground = "";
        public static int nbShootCounter = 0;
        public static int incrementShoot = 0;
        public const string directoryEvent = "C:\\EVENTS";
        public static string codeEvent_toEdit = "";
        public static string typeEvent = "";
        public static string _archivesFolder = "C:\\Events\\Assets\\Archives";
        public static int EventChosen = 0; // 0 cadre, 1 polaroid, 2 multishoot
        public static string _butonAccueil = $"{EventAssetFolder()}\\Buttons\\rebound.png";
        public static string _defaultbutonAccueil = $"{EventAssetFolder()}\\Default\\rebound.png";
        public static string _btnFiltreGoToTakePicture = $"{EventAssetFolder()}\\Buttons\\takepicture.jpg";
        public static string _defaultBtnPuceGrise = "C:\\Events\\Assets\\Default\\Buttons\\puce-grise.png";
        public static string _defaultBtnIcoWifi = "C:\\Events\\Assets\\Default\\Buttons\\ico-wifi.png";
        public static string _defaultBtnIcoWifiOff = "C:\\Events\\Assets\\Default\\Buttons\\ico-wifi-off.png";
        public static string _defaultBtnIcoConnect = "C:\\Events\\Assets\\Default\\Buttons\\ico-valid.png";
        public static string _defaultBtnIcoPhoto = "C:\\Events\\Assets\\Default\\Buttons\\ico-photos.png";
        public static string _defaultBtnIcoSetting = "C:\\Events\\Assets\\Default\\Buttons\\ico-data.png";
        public static string _defaultBtnIcoAssist = "C:\\Events\\Assets\\Default\\Buttons\\ico-assistance.png";

        public static string _defaultBtnOkPopup = "C:\\Events\\Assets\\Default\\Buttons\\ok.png";
        public static string _defaultBtnyesPopup = "C:\\Events\\Assets\\Default\\Buttons\\yes.png";
        public static string _defaultBtnnoPopup = "C:\\Events\\Assets\\Default\\Buttons\\no.png";

        public static string _defaultBtnallphotos = "C:\\Events\\Assets\\Default\\Buttons\\ico-photos.png";
        public static string _defaultBtnalltimelines = "C:\\Events\\Assets\\Default\\Buttons\\ico-timeline.png";
        public static string _defaultBtnareglageConfig = "C:\\Events\\Assets\\Default\\Buttons\\ico-data.png";
        public static string _defaultBtnassistance = "C:\\Events\\Assets\\Default\\Buttons\\ico-assistance.png";

        public static string _defaultallphotos = "C:\\Events\\Assets\\Default\\Buttons\\btn-touteslesphotos.png";
        public static string _defaultalltimelines = "C:\\Events\\Assets\\Default\\Buttons\\btn-voir-timeline.png";
        public static string _defaultreglageConfig = "C:\\Events\\Assets\\Default\\Buttons\\btn-reglages.png";
        public static string _defaultassistance = "C:\\Events\\Assets\\Default\\Buttons\\btn-assistance.png";

        public static string _defaultBtnIcoPuceBtn = "C:\\Events\\Assets\\Default\\Buttons\\puce-bouton.png";
        public static string _defaultLogoNoir =  exerDir + "\\Images\\Default\\Buttons\\logo-selfizee-noir.png";
        public static string _defaultBtnDetailsClosed = "C:\\Events\\Assets\\Default\\Buttons\\btn-details.png";
        public static string _defaultBtnEditEvent = "C:\\Events\\Assets\\Default\\Buttons\\btn_edit_event.png";
        public static string _defaultBtnDetailsOpen = "C:\\Events\\Assets\\Default\\Buttons\\btn-details-open.png";
        public static string _defaultBtnLaunchAnimation = "C:\\Events\\Assets\\Default\\Buttons\\btn_lancer_animation.png";
        public static string _defaultbtnFiltreGoToTakePicture = "C:\\Events\\Assets\\Default\\Buttons\\takepicture.jpg";
        public static string _defaultImageCamera = "C:\\Events\\Assets\\Default\\Images\\camera.jpg";
        public static string _defaultImageLoader = "C:\\Events\\Assets\\Default\\Images\\img_loader.png";
        public static string _defaultNoneImage = "C:\\Events\\Assets\\Default\\Images\\none.jpg";
        public static string _defaultImageHotLine = "C:\\Events\\Assets\\Default\\Images\\hotline.jpg";
        public static string _defaultImageSelfizee = exerDir + "\\Images\\Default\\Images\\logo_selfizee.png";
        public static string _defaultImageNoConnex = "C:\\Events\\Assets\\Default\\Images\\point-exclamation.png";
        public static string _defaultImageUpdate = "C:\\Events\\Assets\\Default\\Images\\img_update.jpg";
        public static string _defaultImageCouv = "C:\\Events\\Assets\\Default\\Images\\img_couv.jpg";
        public static string _defaultImagePrinter = "C:\\Events\\Assets\\Default\\Images\\printer.jpg";
        public static string _btnVisualisationBackToTakePicture = $"{EventAssetFolder()}\\Buttons\\printandsend.png";
        public static string _btnVisualisationBackToFiltreChoice = $"{EventAssetFolder()}\\Buttons\\printandsend.png";
        public static string _btnVisualisationPrinting = $"{EventAssetFolder()}\\Buttons\\print.png";
        public static string _btnVisualisationHelp = $"{EventAssetFolder()}\\Buttons\\help.png";
        public static string _screesaverAssetFolder = $"{EventAssetFolder()}\\Screensaver";
        public static string _btnVisualisationEnd = $"{EventAssetFolder()}\\Buttons\\Visualisation\\quit.png";
        public static string _btnVisualisationEmail = "C:\\Events\\Assets\\Default\\Buttons\\email.png";
        public static string _btnVisualisationPrintEmail = $"{EventAssetFolder()}\\Buttons\\print-mail.png";
        public static string _xmlFormContent = $"{EventAssetFolder()}\\Form\\Content.xml";
        public static string _btnVisualisationHome = $"{EventAssetFolder()}\\Buttons\\home.png";
        public static string _defaultbtnVisualisationPrinting = "C:\\Events\\Assets\\Default\\Buttons\\print.png";
        public static string _defaultbtnHome = "C:\\Events\\Assets\\Default\\Buttons\\home.png";
        public static string _defaultbtnReglages = "C:\\Events\\Assets\\Default\\Buttons\\bt-reglages.png";
        public static string _defaultbtnExit = "C:\\Events\\Assets\\Default\\Buttons\\btn-quitter.png";
        public static string _defaultbtnVisualisationCroix = "C:\\Events\\Assets\\Default\\Buttons\\croix.png";
        public static string _defaultbtnConfig = "C:\\Events\\Assets\\Default\\Buttons\\btn-reglages.png";
        public static string _defaultbtnPhoto = "C:\\Events\\Assets\\Default\\Buttons\\btn-touteslesphotos.png";
        public static string _defaultbtnAssist = "C:\\Events\\Assets\\Default\\Buttons\\btn-assistance.png";
        public static string _defaultbtnAdmin = "C:\\Events\\Assets\\Default\\Buttons\\ico-admin.jpg";
        public static string _defaultbtnWhiteNext = "C:\\Events\\Assets\\Default\\Buttons\\btn-next.png";
        public static string _defaultbtnWhitePrevious = "C:\\Events\\Assets\\Default\\Buttons\\btn-retour.png";
        public static string _defaultbtnFlecheRetour = "C:\\Events\\Assets\\Default\\Buttons\\back.png";
        public static string _defaultbtnVisualisationCroix2 = "C:\\Events\\Assets\\Default\\Buttons\\croix-grise.png";
        public static string _defaultbtnVisualisationEnd = "C:\\Events\\Assets\\Default\\Buttons\\quit.png";
        public static string _defaultbtnVisualisationEmail = "C:\\Events\\Assets\\Default\\Buttons\\email.png";
        public static string _defaultbtnVisualisationPrintEmail = "C:\\Events\\Assets\\Default\\Buttons\\print-mail.png";

        public static string _defaultbtnarchiveEvent = "C:\\Events\\Assets\\Default\\Buttons\\btn_archive_event.png";
        public static string _defaultbtndeleteEvent = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_event.png";

        public static string _defaultbtnVisualisationHelp = "C:\\Events\\Assets\\Default\\Buttons\\help.png";
        public static string _defaultbtnVisualisationFormPrevious = "C:\\Events\\Assets\\Default\\Buttons\\btn_previous.png";
        public static string _defaultbtnCancelForm = "C:\\Events\\Assets\\Default\\Buttons\\btn_cancelform.png";
        public static string _defaultbtnCalendar = "C:\\Events\\Assets\\Default\\Buttons\\btn_calendar.png";

        public static string _defaultbtnYesSms = "C:\\Events\\Assets\\Default\\Buttons\\btn_valide_2.png";
        public static string _defaultbtnNoSms = "C:\\Events\\Assets\\Default\\Buttons\\btn_refuse_2.png";
        public static string _defaultbtnYesEmail = "C:\\Events\\Assets\\Default\\Buttons\\btn_valide_1.png";
        public static string _defaultbtnNoEmail = "C:\\Events\\Assets\\Default\\Buttons\\btn_refuse_1.png";

        public static string _defaultbtnNoChecked = "C:\\Events\\Assets\\Default\\Buttons\\btn_no.png";
        public static string _defaultbtnNoUnChecked = "C:\\Events\\Assets\\Default\\Buttons\\btn_no_1.png";
        public static string _defaultbtnYesChecked = "C:\\Events\\Assets\\Default\\Buttons\\btn_yes.png";
        public static string _defaultbtnYesUnChecked = "C:\\Events\\Assets\\Default\\Buttons\\btn_yes_1.png";
        


        public static string _defaultbtnVisualisationFormNext = "C:\\Events\\Assets\\Default\\Buttons\\btn_formnext.png";
        public static string _defaultbtnConfigBack = "C:\\Events\\Assets\\Default\\Buttons\\btn_Back1.png";
        public static string _defaultbtnConnexion = "C:\\Events\\Assets\\Default\\Buttons\\btn_connexion.png";
        public static string _defaultbtnDeleteContacts = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_contact.png";
        public static string _defaultbtnDownload = "C:\\Events\\Assets\\Default\\Buttons\\btn_download.png";
        public static string _defaultbtnDownloadOther = "C:\\Events\\Assets\\Default\\Buttons\\btn_download_animation.png";
        public static string _defaultbtnBackToEvent = "C:\\Events\\Assets\\Default\\Buttons\\btn_back_event.png";
        public static string _defaultbtnResetPrinted = "C:\\Events\\Assets\\Default\\Buttons\\btn_reset_zero.png";
        public static string _defaultbtndeletecoordonate = "C:\\Events\\Assets\\Default\\Buttons\\ico_delete.png";
        public static string _defaultbtnsave = "C:\\Events\\Assets\\Default\\Buttons\\btn_save.png";
        public static string _defaultbtneditcoordonate = "C:\\Events\\Assets\\Default\\Buttons\\ico_edit.png";
        public static string _defaultclosescroll = "C:\\Events\\Assets\\Default\\Buttons\\btn_close_2.png";
        public static string _defaultValidate = "C:\\Events\\Assets\\Default\\Buttons\\btn_valider.png";
        public static string _defaultnext = "C:\\Events\\Assets\\Default\\Buttons\\btn_left.png";
        public static string _defaultprevious = "C:\\Events\\Assets\\Default\\Buttons\\btn_right.png";
        public static string _defaultbackButton = "C:\\Events\\Assets\\Default\\Buttons\\btn_back_3.png";
        public static string _defaultdeleteImage = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_1.png";
        public static string _defaultdeleteconfig = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_2.png";
        public static string _defaultcanceldelete = "C:\\Events\\Assets\\Default\\Buttons\\btn_cancel_2.png";
        public static string _defaultdeletedataconfig = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_data.png";
        public static string _defaultokconfig = "C:\\Events\\Assets\\Default\\Buttons\\image_check.png";
        public static string _defaultexportdataconfig = "C:\\Events\\Assets\\Default\\Buttons\\btn_exporter_2.png";
        public static string _defaultdeletephotosconfig = "C:\\Events\\Assets\\Default\\Buttons\\btn_delete_photo_2.png";
        public static string _defaultprintconfig = "C:\\Events\\Assets\\Default\\Buttons\\btn_imprimer_2.png";
        public static string _defaultbtnselfizee_logo1 = exerDir + "\\Images\\Default\\Buttons\\selfizee_logo_1.png";
        public static string _defaultUpdateDirectory = "C:\\Events\\Assets\\Default\\Update";
        public static string _defaultbtnVisualisationHome = "C:\\Events\\Assets\\Default\\Buttons\\home.png";
        public static string _defaultbtnPuce = "C:\\Events\\Assets\\Default\\Buttons\\puce-bouton.png";
        public static string _defaultbtnPuceLeft = "C:\\Events\\Assets\\Default\\Buttons\\puce-bouton-left.png";
        public static string _defaultbtnConnecter = "C:\\Events\\Assets\\Default\\Buttons\\bt-seconnecter.png";
        public static string _defaultbtnDeconnecter = "C:\\Events\\Assets\\Default\\Buttons\\bt-deconnecter.png";
        public static string _defaultbtnWifiList = "C:\\Events\\Assets\\Default\\Buttons\\boutons-reseauxwifi.png";
        public static string _formResultPath = EventAssetFolder() + "\\FormResult.csv";
        public static string _defaultimageArrow = "C:\\Events\\Assets\\Default\\Buttons\\arrow.png";
        public static string csvHoraires = $"{EventAssetFolder()}\\InstantGagnant\\horaires.csv";
        public static string _defautlbtnprintrose = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt-imprimer-rose.png";
        public static string _defaultbtnTB = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt-tableau.png";
        public static string _defaultbtnAnim = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt-retour-rose.png";
        public static string _defaultImgCercle = "C:\\EVENTS\\Assets\\Default\\Buttons\\rond-decompte.png";
        public static bool fromAdmin = false;

        public static string _defaultDeleteAssets = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt-supprimer-et-conserver.jpg";
        public static string _defaultDeleteAll = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt-supprimer-et-effacer.jpg";
        public static string _defaultLoaderVideo = exerDir + "\\Images\\Default\\Video\\introNoirR.mp4";

        public static string _defaultGreenSave = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt_enregistrer_vert.png";
        public static string _defaultLaunchAnim = "C:\\EVENTS\\Assets\\Default\\Buttons\\bt_lancer_vide.png";
        public static string _PortableConnected = "";
        public static string _BirthConnected = "";
        public static int gift_index = -1;
        public static bool showUpdateSoftware = false;
        public static int nbField = 0;
        public static string point_relais = "";
        public static string printedImage = "";
        public static string imageChosen = "";
        public static String greenScreen = "";
        public static string noEventImage = exerDir + "\\Images\\no_event.jpg";
        public static bool mailsent = false;
        public static bool gswithoutframe = false;
        public static string paramDownload = "";
        public static DateTime dtTakenPhoto = DateTime.MinValue;
        public static DateTime dtPrint = DateTime.MinValue;
        public static bool printed = false;
        public static bool timerHomeLaunched = false;
        public static int radio_increment = 0;
        public static int check_increment = 0;
        public static int combo_increment = 0;
        public static string heureConnexion;
        public static string portName = "";
        public static int portNumber = 0;
        public static string binFileName = "";
        public static string nextcloudPath = "";
        public static bool customFormat = false;
        public static bool portrait = false;
        public static int customNbshoot = 0;
        public static int customCountDown = 0;
        public static string filterChosen = "";
        public static bool leaveHome = false;
        public static string _eventToDownload = "";
        public static string _nameEventToDownload = "";
        public static string _appConfigFile = directoryEvent + "\\Events\\AppConfig.ini";
        public static DynamicShoot[] _shootDynamic = null;
        public static boData dataToSave = null;
        public static List<boData> lstDataToSave = null;
        public static string auth_token = "";
        public static string ws_login = "admin";
        public static string ws_pwd = "123456";
        public static string ws_url_connexion = "https://booth.selfizee.fr/api/users/getToken.json";
        private static readonly ILog Log =
             LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string _FlagConfig = "client";

        public static List<InstantGagnant> lstHoraires = new List<InstantGagnant>();

        public static string ExeDir()
        {
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string directoryName = Path.GetDirectoryName(exePath);
            return directoryName;
        }

        public static void getHoraires()
        {
            string[] tokens;
            char[] separators = { ';' };
            string str = "";
            //List<InstantGagnant> lstHoraires = new List<InstantGagnant>();
            FileStream fs = new FileStream(csvHoraires,
                                           FileMode.Open);
            StreamReader sr = new StreamReader(fs, Encoding.Default);
            int index = 0;

            while ((str = sr.ReadLine()) != null)
            {
                index++;
                if (index > 1)
                {
                    tokens = str.Split(separators);
                    DateTime date = Convert.ToDateTime(tokens[0]);
                    DateTime heure = Convert.ToDateTime(tokens[1]);
                    DateTime defineDate = new DateTime(date.Year, date.Month, date.Day, heure.Hour, heure.Minute, 0);
                    string cadeau = tokens[2];
                    int type = Convert.ToInt32(tokens[3]);

                    InstantGagnant instGagnant = new InstantGagnant();
                    instGagnant.startDate = defineDate;
                    instGagnant.gift = cadeau;
                    instGagnant.originalString = str;
                    instGagnant.index = index;
                    string winCsv = tokens[4].ToLower();
                    if (!string.IsNullOrEmpty(winCsv) && winCsv != "non")
                    {
                        instGagnant.win = true;
                    }
                    else
                    {
                        instGagnant.win = false;
                    }
                    instGagnant.TypeCadeau = type;
                    lstHoraires.Add(instGagnant);
                }
            }
            fs.Close();
            fs.Dispose();
        }

        public static void createFolder()
        {
            createSingleFolder(tempDir);
            createSingleFolder(tempDir + "\\Temp");
            createSingleFolder(tempDir + "\\Temp\\Blackwhite");
            createSingleFolder(tempDir + "\\Temp\\Color");
            createSingleFolder(tempDir + "\\Temp\\OneColor");
            createSingleFolder(tempDir + "\\Temp\\Popart");
            createSingleFolder(tempDir + "\\Temp\\Sepia");
            createSingleFolder(_finalFolder);
            createSingleFolder(_finalFolder + "\\Original");
            createSingleFolder(_finalFolder + "\\Affichage");
            createSingleFolder(_finalFolder + "\\Temporaire");
            createSingleFolder(_tempFolder);
            createSingleFolder(_tempFolder + "\\Blackwhite");
            createSingleFolder(_tempFolder + "\\Color");
            createSingleFolder(_tempFolder + "\\OneColor");
            createSingleFolder(_tempFolder + "\\Popart");
            createSingleFolder(_tempFolder + "\\Sepia");
            createSingleFolder(stripFrame);
            createSingleFolder(_waitingFolder);
            createSingleFolder(_dataFolder);
            createSingleFolder(ExeDir() + "\\Logs");
            createSingleFolder(directoryEvent + "\\Media");
            string pathMediaPhoto = EventMediaFolder() + "\\Photos";
            string pathMedia = EventMediaFolder();
            createSingleFolder(pathMediaPhoto);
            createSingleFolder(pathMediaPhoto + "\\Original");
            createSingleFolder(EventMediaFolder() + "\\Data");
        }

        public static void createSingleFolder(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                if (!Directory.Exists(folderPath))
                {
                    Log.Error("Impossible de créer le répertoire " + folderPath);
                }
            }

        }
        public static String LoadBG(TypeFond type)
        {
            var currDir = Globals.currDir;
            //var temp = currDir + "\\..\\..\\Assets\\Backgrounds";
            //var _temp = System.IO.Path.GetDirectoryName(Application.StartupPath) + "\\..\\..\\Assets\\Backgrounds";
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            var exeDir = Path.GetDirectoryName(exePath);
            var _temp = EventAssetFolder() + "\\Backgrounds";
            var _defaultBackground = EventFolder() + "\\Assets\\Default\\Background";

            switch (type)
            {
                case TypeFond.BgGolbal:
                    return Directory.GetFiles(_defaultBackground, "background.jpg")[0];
                case TypeFond.ForAccueil:
                    return EventAssetFolder() +  "\\Backgrounds\\background_home.jpg";
                case TypeFond.ForTakephoto:
                    return EventAssetFolder() + "\\Backgrounds\\background_takepicture.jpg";
                case TypeFond.ForThanks:
                    return EventAssetFolder() + "\\Backgrounds\\background_thanks.jpg";
                case TypeFond.ForFiltre:
                    return EventAssetFolder() + "\\Backgrounds\\background_filter.jpg";
                case TypeFond.ForEvent:
                    return EventAssetFolder() + "\\Backgrounds\\background_format.jpg";
                case TypeFond.ForChromaKeyChoice:
                    return EventAssetFolder() + "\\Backgrounds\\background_greenscreen.jpg";
                case TypeFond.ForNoCamera:
                    return EventAssetFolder() + "\\Backgrounds\\background_nocamera.jpg";
                case TypeFond.ForVisualisation:
                    return EventAssetFolder() + "\\Backgrounds\\background_visualisation.jpg";
            }
            return Directory.GetFiles(_defaultBackground, "background.jpg")[0];
        }

        public static int GetNbShoot(TypeEvent type)
        {
            switch (type)
            {
                case TypeEvent.cadre:
                    return 0;
                case TypeEvent.strip:
                    return 1;
                case TypeEvent.polaroid:
                    return 2;
                case TypeEvent.multishoot:
                    return 3;

            }
            return 0;
        }

        public static String CurrentDirectory()
        {
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            var exeDir = Path.GetDirectoryName(exePath);
            return exeDir;
        }

        public static String LoadBouton()
        {
            var currDir = CurrentDirectory();
            var temp = EventAssetFolder() + "\\Buttons";
            return Directory.GetFiles(temp, "bouton.png")[0];
        }

        public static String GetFiltred(string path)
        {
            int strindex = "file:///".Length;
            int len = path.Length - strindex;
            return path.Substring(strindex, len);
        }

        public static String EventFolder()
        {
            var path = "";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(directoryEvent + "\\Events\\AppConfig.ini");//{appconfig}");
            //var eventPath = _inimanager.GetSetting("EVENTSCONFIG", "assetpath");
            //path = Path.Combine(eventPath, id);
            if (!System.IO.Directory.Exists(directoryEvent))
                return "";
            return directoryEvent;
        }

        public static String EventAssetFolder()
        {
            var path = "";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(directoryEvent + "\\Events\\AppConfig.ini");//{appconfig}");
            //var eventPath = _inimanager.GetSetting("EVENTSCONFIG", "assetpath");
            var id = _inimanager.GetSetting("EVENTSCONFIG", "id");
            path = directoryEvent + "\\Assets\\" + id;
            if (!System.IO.Directory.Exists(path))
                return "";
            return path;
        }

        public static String AbsoluteDefaultAssetFolder()
        {
            var path = "";
            //Managers.INIFileManager _inimanager = new Managers.INIFileManager(directoryEvent + "\\Events\\AppConfig.ini");//{appconfig}");
            //var eventPath = _inimanager.GetSetting("EVENTSCONFIG", "assetpath");

            path = directoryEvent + "\\Assets\\Default";
            if (!System.IO.Directory.Exists(path))
                return "";
            return path;
        }

        public static String EventMediaFolder()
        {
            var path = "";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(directoryEvent + "\\Events\\AppConfig.ini");//{appconfig}");
            //var eventPath = _inimanager.GetSetting("EVENTSCONFIG", "assetpath");
            var id = _inimanager.GetSetting("EVENTSCONFIG", "id");
            path = directoryEvent + "\\Media\\" + id;
            //if (!System.IO.Directory.Exists(path))
            //    return "";
            return path;
        }

        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static void  getDynamicPosition()
        {
            _shootDynamic = new DynamicShoot[0];
            bool indexshootreal = false;
            string iniPath = EventAssetFolder() + "\\Config.ini";
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(iniPath);
            int index = 0;
            for(int i = 0; i < 4; i++)
            {
                bool tosave = false;
                index++;
                DynamicShoot _shoot = new DynamicShoot();
                string currentshoot1PosX = "shoot" + index + "PosX";
                string currentshoot1PosY = "shoot" + index + "PosY";
                string currentshoot1Width = "shoot" + index + "Width";
                string currentshoot1Height = "shoot" + index + "Height";
                string currentshoot1Rotation = "shoot" + index + "Rotation";
                string currentindex1shoot = "shoot" + index + "Zindex ";
                string x = _inimanager.GetSetting("SHOOT", currentshoot1PosX);
                string y = _inimanager.GetSetting("SHOOT", currentshoot1PosY);
                string width = _inimanager.GetSetting("SHOOT", currentshoot1Width);
                string height = _inimanager.GetSetting("SHOOT", currentshoot1Height);
                string rotation = _inimanager.GetSetting("SHOOT", currentshoot1Rotation);
                string index_shoot = _inimanager.GetSetting("SHOOT", currentindex1shoot);
                if (IsNumeric(x))
                {
                    x = x.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(x);
                    _shoot.x = (int)f;
                }
                else
                {
                    tosave = false;
                }
                if (IsNumeric(y))
                {
                    y = y.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(y);
                    _shoot.y = Convert.ToInt32(f);
                }
                else
                {
                    tosave = false;
                }
                if (IsNumeric(width))
                {
                    width = width.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(width);
                    _shoot.width = Convert.ToInt32(f);
                }
                else
                {
                    tosave = false;
                }
                if (IsNumeric(height))
                {
                    height = height.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(height);
                    _shoot.height = Convert.ToInt32(f);
                }
                else
                {
                    tosave = false;
                }
                if (IsNumeric(rotation))
                {
                    rotation = rotation.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(rotation);
                    _shoot.rotation = Convert.ToInt32(f);
                }
                else
                {
                    tosave = false;
                }
                if (IsNumeric(index_shoot))
                {
                    indexshootreal = true;
                    index_shoot = index_shoot.Replace('.', ',');
                    tosave = true;
                    float f = float.Parse(index_shoot);
                    _shoot.z_index_shoot = Convert.ToInt32(f);
                }
                if (tosave)
                {
                    Array.Resize(ref _shootDynamic, index);
                    _shootDynamic[i] = _shoot;
                }
                else
                {
                    index--;
                }
               
            }
            if (indexshootreal)
            {
                _shootDynamic = _shootDynamic.OrderBy(o => o.z_index_shoot).ToArray();
            }
        }

        public static String GetEventId()
        {
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(directoryEvent + "\\Events\\AppConfig.ini");
            var id = _inimanager.GetSetting("EVENTSCONFIG", "id");
            return !String.IsNullOrEmpty(id)?id:"";
        }


    }
}
