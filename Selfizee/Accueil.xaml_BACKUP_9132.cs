﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Selfizee.Models.Enum;
using Selfizee.Managers;
using Selfizee.Models;
using System.Windows.Forms.Integration;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for Accueil.xaml
    /// </summary>
    public partial class Accueil : UserControl
    {
        private static string EventIni = $"{Globals.EventConfigFolder()}\\Config.ini";
        private INIFileManager _inimanager = new INIFileManager(EventIni);

        public Accueil()
        {
            InitializeComponent();
            ViewBackgroundAttributeModel backgroundAttributes = _inimanager.GetBackGroundAttributes("Accueil");
  
            if(backgroundAttributes.IsImage)
            {
                ImageBrush imgBrush = new ImageBrush();
                imgBrush.ImageSource = new BitmapImage(new Uri(Globals.LoadBG(TypeFond.ForAccueil), UriKind.Absolute));
                imgBrush.Stretch = Stretch.Fill;
                Globals._currentChromakeyBG = "";
                accueilBG.Background = imgBrush;
            }

            //if (!backgroundAttributes.EnableTitle)
            //    accueilTitle.Visibility = Visibility.Collapsed;
            
            if (!backgroundAttributes.IsImage)
            {
                if(!string.IsNullOrEmpty(backgroundAttributes.BackgroundColor))
                {
                    var bgcolor = backgroundAttributes.BackgroundColor;
                    var bc = new BrushConverter();
                    this.Background = (Brush)bc.ConvertFrom(bgcolor);
                }

                

<<<<<<< HEAD
                //if(backgroundAttributes.EnableTitle)
                //{
                //    if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                //        accueilTitle.Text = backgroundAttributes.Title;
                //    if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                //        accueilTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                //    if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                //    {
                //        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                //            Canvas.SetLeft((TextBlock)accueilTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                //        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                //            Canvas.SetTop((TextBlock)accueilTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                //    }
                //}
              
                //if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontFamily))
                //    accueilTitle.FontStyle = backgroundAttributes.TitleFontFamily; 
                        
=======
                if(backgroundAttributes.EnableTitle)
                {
                    if (!string.IsNullOrEmpty(backgroundAttributes.Title))
                        accueilTitle.Text = backgroundAttributes.Title;
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitleFontSize))
                        accueilTitle.FontSize = Convert.ToInt32(backgroundAttributes.TitleFontSize);
                    if (!string.IsNullOrEmpty(backgroundAttributes.TitlePosition))
                    {
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionX))
                            Canvas.SetLeft((TextBlock)accueilTitle, Convert.ToInt32(backgroundAttributes.TitlePositionX));
                        if (!string.IsNullOrEmpty(backgroundAttributes.TitlePositionY))
                            Canvas.SetTop((TextBlock)accueilTitle, Convert.ToInt32(backgroundAttributes.TitlePositionY));
                    }
                }                  
>>>>>>> 5fd4b026cee98b9ff1fdc26fe9ba5892053f27f9
            }          
        }

        public void PlayFlashFile()
        {
            string swfFileName = Directory.GetFiles(Globals._flashFile).First();
            SWFPlayer.FlashPlayer player = new SWFPlayer.FlashPlayer(swfFileName);
            player.Click += new EventHandler(Player_Click);
            //player.Anchor = System.Windows.Forms.AnchorStyles.Top;
            Host.Child = player;
           
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // do some stuff here.
            MessageBox.Show("User control clicked");
        }

        private void Player_Click(object sender, EventArgs e)
        {
            MessageBox.Show("User control clicked");
        }

        private void WindowLoaded(object sender, EventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers(); 
            var path1 = $"{Globals._tmpDir}\\1.png";
            var path2 = $"{Globals._tmpDir}\\2.png";
            if (File.Exists(path1))File.Delete(path1);               
            if (File.Exists(path2)) File.Delete(path2);
            Managers.INIFileManager _inimanager = new Managers.INIFileManager(Globals.EventConfigFolder() + "\\Config.ini");
            var flashActivated = _inimanager.GetSetting("HOME", "Animation").ToLower();
            if (flashActivated == "flash")
            {
                PlayFlashFile();
            }
            else if(flashActivated == "rebond")
            {
                if (System.IO.File.Exists(Globals._butonAccueil))
                    image.DataContext = Globals._butonAccueil;
            }
            else
            {
                imageStatique.DataContext = Globals._butonAccueil; 
            }


        }

        private string LoadBG()
        {
            var currDir = Directory.GetCurrentDirectory();
            var temp = currDir + "\\..\\..\\Assets\\Backgrounds";
            return Directory.GetFiles(temp, "BG_ACCUEIL.jpg")[0];
        }
    }
}
