﻿using log4net;
using Selfizee.Managers;
using Selfizee.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Selfizee
{
    public class Compare {
        private static readonly ILog Log =
                                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string ftpServerIP = "37.187.132.132";
        private static string ftpUserName = "dev@upload.selfizee.fr";
        private static string ftpUserNameEvent = "dev@sync.selfizee.fr"; //"upload@syncdev.selfizee.fr"; //"syncdev"; //"sync@uploadv2.selfizee.fr";
        private static string ftpPassword = "qB06JE0K#PvVB4^j7Ahxq";
        private static string ftpPasswordEvent = "[2WLR1FHxCZmXxUKr7>dc[=4";//"^G_yvAY@n~$A}}op<B(*JLN6"; //"T2=-rB1yn}MJ?~D?Yb51n$4M"; //"S99e)%VxI?q*5*q_$6zK/8k/";

        //Console.WriteLine(FilesContentsAreEqualAsync(fi1, fi2).GetAwaiter().GetResult());    
        public static async Task<bool> FilesContentsAreEqualAsync(FileInfo fileInfo1, FileInfo fileInfo2)
        {
            if (fileInfo1 == null)
            {
                throw new ArgumentNullException(nameof(fileInfo1));
            }

            if (fileInfo2 == null)
            {
                throw new ArgumentNullException(nameof(fileInfo2));
            }

            //if (string.Equals(fileInfo1.FullName, fileInfo2.FullName, StringComparison.OrdinalIgnoreCase))
            //{
            //    return true;
            //}

            if (fileInfo1.Length != fileInfo2.Length)
            {
                return false;
            }
            else
            {
                using (var file1 = fileInfo1.OpenRead())
                {
                    using (var file2 = fileInfo2.OpenRead())
                    {
                        return await StreamsContentsAreEqualAsync(file1, file2).ConfigureAwait(false);
                    }
                }
            }
        }

        private static async Task<int> ReadFullBufferAsync(Stream stream, byte[] buffer)
        {
            int bytesRead = 0;
            while (bytesRead < buffer.Length)
            {
                int read = await stream.ReadAsync(buffer, bytesRead, buffer.Length - bytesRead).ConfigureAwait(false);
                if (read == 0)
                {
                    // Reached end of stream.
                    return bytesRead;
                }

                bytesRead += read;
            }

            return bytesRead;
        }

        private static async Task<bool> StreamsContentsAreEqualAsync(Stream stream1, Stream stream2)
        {
            const int bufferSize = 1024 * sizeof(Int64);
            var buffer1 = new byte[bufferSize];
            var buffer2 = new byte[bufferSize];

            while (true)
            {
                int count1 = await ReadFullBufferAsync(stream1, buffer1).ConfigureAwait(false);
                int count2 = await ReadFullBufferAsync(stream2, buffer2).ConfigureAwait(false);

                if (count1 != count2)
                {
                    return false;
                }

                if (count1 == 0)
                {
                    return true;
                }

                int iterations = (int)Math.Ceiling((double)count1 / sizeof(Int64));
                for (int i = 0; i < iterations; i++)
                {
                    if (BitConverter.ToInt64(buffer1, i * sizeof(Int64)) != BitConverter.ToInt64(buffer2, i * sizeof(Int64)))
                    {
                        return false;
                    }
                }
            }
        }

        public static bool FoldersContentsAreEqual(string pathFolderA, string pathFolderB)
        {
            // Create two identical or different temporary folders   
            // on a local drive and change these file paths. 
             
            //string pathA = @"C:\TestDir";
            //string pathB = @"C:\TestDir2";

            DirectoryInfo dir1 = new DirectoryInfo(pathFolderA);
            DirectoryInfo dir2 = new DirectoryInfo(pathFolderB);

            // Take a snapshot of the file system.  
            IEnumerable<FileInfo> list1 = dir1.GetFiles("*.*", SearchOption.AllDirectories);
            IEnumerable<FileInfo> list2 = dir2.GetFiles("*.*", SearchOption.AllDirectories);

            //A custom file comparer defined below  
            FileCompare myFileCompare = new FileCompare();

            // This query determines whether the two folders contain  
            // identical file lists, based on the custom file comparer  
            // that is defined in the FileCompare class.  
            // The query executes immediately because it returns a bool. 
             
            //bool areIdentical = list1.SequenceEqual(list2, myFileCompare);
            return list1.SequenceEqual(list2, myFileCompare);
        }

        public static List<string> CommonFilesInFolderAFolderB(string pathFolderA, string pathFolderB)
        {
            // Create two identical or different temporary folders   
            // on a local drive and change these file paths. 

            //string pathA = @"C:\TestDir";
            //string pathB = @"C:\TestDir2";

            DirectoryInfo dir1 = new DirectoryInfo(pathFolderA);
            DirectoryInfo dir2 = new DirectoryInfo(pathFolderB);

            // Take a snapshot of the file system.  
            IEnumerable<FileInfo> list1 = dir1.GetFiles("*.*", SearchOption.AllDirectories);
            IEnumerable<FileInfo> list2 = dir2.GetFiles("*.*", SearchOption.AllDirectories);

            //A custom file comparer defined below  
            FileCompare myFileCompare = new FileCompare();
            var result = new List<string>();

            // Find the common files. It produces a sequence and doesn't   
            // execute until the foreach statement.  
            var queryCommonFiles = list1.Intersect(list2, myFileCompare);

            if (queryCommonFiles.Count() > 0)
            {
                //The following files are in both folders
                foreach (var v in queryCommonFiles)
                {
                    result.Add(v.FullName); //shows which items end up in result list  
                }
            }
            return result;
        }

       

        public static List<string> FilesInFolderANotInFolderB(string pathFolderA, string pathFolderB)
        {
            // Create two identical or different temporary folders   
            // on a local drive and change these file paths. 

            //string pathA = @"C:\TestDir";
            //string pathB = @"C:\TestDir2";

            DirectoryInfo dir1 = new DirectoryInfo(pathFolderA);
            DirectoryInfo dir2 = new DirectoryInfo(pathFolderB);

            // Take a snapshot of the file system.  
            IEnumerable<FileInfo> list1 = dir1.GetFiles("*.*", SearchOption.AllDirectories);
            IEnumerable<FileInfo> list2 = dir2.GetFiles("*.*", SearchOption.AllDirectories);

            //A custom file comparer defined below  
            FileCompare myFileCompare = new FileCompare();
            var result = new List<string>();

            // Find the set difference between the two folders.  
            // For this example we only check one way.  
            var queryList1Only = (from file in list1 select file).Except(list2, myFileCompare);

            if (queryList1Only.Count() > 0)
            {
                //The following files are in both folders
                foreach (var v in queryList1Only)
                {
                    result.Add(v.FullName); //shows which items end up in result list  
                }
            }
            return result;
        }

        private static void DownloadFile(string localFilePath, string remoteFileUri, NetworkCredential credentials)
        {
            try
            {
                var folderPath = Path.GetDirectoryName(localFilePath);
                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                System.Net.FtpWebRequest downloadRequest = (System.Net.FtpWebRequest)WebRequest.Create(remoteFileUri);
                downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                downloadRequest.Credentials = credentials;

                //using (Stream targetStream = downloadRequest.GetRequestStream())
                using (System.Net.FtpWebResponse responseFileDownload = (System.Net.FtpWebResponse)downloadRequest.GetResponse())
                using (Stream responseStream = responseFileDownload.GetResponseStream())
                using (FileStream writeStream = new FileStream(localFilePath, FileMode.Create))
                {

                    int Length = 2048;
                    Byte[] buffer = new Byte[Length];
                    int bytesRead = responseStream.Read(buffer, 0, Length);
                    int bytes = 0;

                    while (bytesRead > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                        bytesRead = responseStream.Read(buffer, 0, Length);
                        bytes += bytesRead;// don't forget to increment bytesRead !
                                           //int totalSize = (int)(fileSize) / 1000; // Kbytes
                                           //worker.ReportProgress((bytes / 1000) * 100 / totalSize, totalSize);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Compare.cs - DownloadFile Error : {ex}");
            }
        }

    public static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath)
        {
            //Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
            //{
                try
                {
                System.Net.FtpWebRequest listRequest = (System.Net.FtpWebRequest)WebRequest.Create(url);
                    listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    listRequest.Credentials = credentials;

                    List<string> lines = new List<string>();

                    using (System.Net.FtpWebResponse listResponse = (System.Net.FtpWebResponse)listRequest.GetResponse())
                    using (Stream listStream = listResponse.GetResponseStream())
                    using (StreamReader listReader = new StreamReader(listStream))
                    {
                        while (!listReader.EndOfStream)
                        {
                            lines.Add(listReader.ReadLine());
                        }
                    }

                    foreach (string line in lines)
                    {
                        string[] tokens =
                            line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                        string name = tokens[8];
                        string permissions = tokens[0];

                        string localFilePath = Path.Combine(localPath, name);
                        string fileUrl = url + name;

                        if (permissions[0] == 'd')
                        {
                            if (!Directory.Exists(localFilePath))
                            {
                                Directory.CreateDirectory(localFilePath);
                            }

                            DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath);
                        }
                        else
                        {
                        System.Net.FtpWebRequest downloadRequest = (System.Net.FtpWebRequest)WebRequest.Create(fileUrl);
                            downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            downloadRequest.Credentials = credentials;

                            using (System.Net.FtpWebResponse downloadResponse =
                                      (System.Net.FtpWebResponse)downloadRequest.GetResponse())
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"Compare.cs - DownloadFtpDirectory Error : {ex}");
                }

            //});//);


        }
    public static void DeleteDirectory(string path)
    {
            string[] subdir = Directory.GetDirectories(path);
            string[] subfiles = Directory.GetFiles(path);
            string[] AllContent = new string[subfiles.Length + subdir.Length];
            subdir.CopyTo(AllContent, 0);
            Array.ConstrainedCopy(subfiles, 0, AllContent, subdir.Length,subfiles.Length);
            //subdir=subdir.Concat(subfiles).ToArray();
            foreach (var item in AllContent)
            {
                if (Directory.Exists(item))
                {
                    string[] files = Directory.GetFiles(item);
                    string[] dir = Directory.GetDirectories(item);
                    if (dir.Count() != 0)
                    {
                        DeleteDirectory(item);
                    }
                    foreach (var item2 in files)
                    {
                        File.Delete(item2);
                    }
                    if (files.Count() == 0)
                    {
                        Directory.Delete(item);
                    }
                }
                else
                {
                    File.Delete(item);
                }
            }
            //if (subdir.Count() == 0)
            //{
                Directory.Delete(path);
            //}
        }
    public static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
    {
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                if (dir.Name.ToLower() != "files.xml")
                {
                    CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
                }
            }
                
            foreach (FileInfo file in source.GetFiles())
            {
                if (Globals.paramDownload.Equals("config"))
                {
                    if (source.Name.ToLower().Equals("configuration"))
                    {
                        string filename = target.FullName + "\\" + file.Name;
                        if ((!System.IO.File.Exists(filename)))
                        {
                            file.CopyTo(filename);
                        }
                        else
                        {
                            System.IO.File.Delete(filename);
                            file.CopyTo(filename);
                        }
                    }
                }
                else
                {
                    string filename = target.FullName + "\\" + file.Name;
                    if ((!System.IO.File.Exists(filename)))
                    {
                        file.CopyTo(filename);
                    }
                    else
                    {
                        List<string> ImageExtensions = new List<string> { ".JPG", ".JPE", ".BMP", ".GIF", ".PNG" };
                        if (ImageExtensions.Contains(Path.GetExtension(filename).ToUpperInvariant()))
                        {
                            Image image2 = Image.FromFile(filename);
                            image2.Dispose();
                        } 
                        System.IO.File.Delete(filename);
                        file.CopyTo(filename);
                    }
                }


            }

            

        }

        public static void deleteUnnecesaryDirectory(string sourcepath, string destPath, string code)
        {
            string[] existingTargetFiles = Directory.GetFiles(sourcepath, "*.*", SearchOption.AllDirectories);

            // Get existing directories in destination
            string[] existingTargetDirectories = Directory.GetDirectories(sourcepath, "*", SearchOption.AllDirectories);

            // Compare and delete files that exist in destination but not source
            foreach (string existingFiles in existingTargetFiles)
            {
                string remoteToReplace = $"C:/Events/Download/" + code + "/Remote/";
                string localToReplace = $"C:/Events/Assets/" + code + "/";
                string pathRemote = $"Download/" + code + "Remote";
                string pathLocal = $"Assets/" + code + "/" ;
                string[] splitted = existingFiles.Split(new string[] { pathLocal }, StringSplitOptions.None);
                remoteToReplace = remoteToReplace + splitted[1];
                localToReplace = localToReplace + splitted[1];
                if (!File.Exists(remoteToReplace) && !remoteToReplace.ToLower().Contains("config.ini"))
                {

                    File.Delete(localToReplace);
                }
            }
            // Compare and delete directories that exist in destination but not source
            foreach (string existingDirectory in existingTargetDirectories)
            {
                if (Directory.GetFiles(existingDirectory).Count() <= 0 && Directory.GetDirectories(existingDirectory).Count() <= 0)
                {
                    Directory.Delete(existingDirectory);
                }
            }
            

        }
        public static bool SynchronizeFromFtpToLocalDirectory(string eventCode, List<string> listeEventImages)//async Task<bool>
        {
        try
        {

            if (listeEventImages != null && listeEventImages.Count() > 0)
            {
                string localPath = $"C:/EVENTS/Assets/{eventCode}/";
                string remotePath = $"ftp://{ftpServerIP}/{eventCode}/";
                string sourcePath = $"C:/EVENTS/Download/{eventCode}/Remote/";
                
                if (!Directory.Exists(sourcePath))
                {
                    Directory.CreateDirectory(sourcePath);
                }
                    GC.Collect();
                    //Directory.Delete(localPath, true);
                    if (!Directory.Exists(localPath))
                    {
                        Directory.CreateDirectory(localPath);
                    }
                    string destPath = $"C:/EVENTS/Download/{eventCode}/Remote";
                    string[] subdir = Directory.GetDirectories(sourcePath);
                   
                    var diSource = new DirectoryInfo(destPath);
                    var diTarget = new DirectoryInfo(localPath);
                    CopyFilesRecursively(diSource, diTarget);
                    deleteUnnecesaryDirectory(localPath, destPath, eventCode);
                    File.Delete(localPath + "//Config.ini");
                    File.Copy(localPath + "//Configuration//config.ini", localPath + "//Config.ini");
                    File.Delete(localPath + "//Configuration//config.ini");
                    Directory.Delete(localPath + "//Configuration");
                }

            return true;
        }
        catch (Exception ex)
        {
            Log.Error($"Compare.cs - SynchronizeFromFtpToLocalDirectory Error : {ex}");
            return false;
        }
    }
        //averina mintsy: parcourir c:/event => nouvelle liste vec prefix c:/Event/codeevent/item(listeEventImages) => comparer fichier avec local => si existe pas delete
        public static void DeleteLocalFileNotSynchro(string eventCode, List<string> listeEventImages)
        {
            try
            {
                string EventAssetPath = $"C:/EVENTS/Assets/{eventCode}/";
                string[] AllFolderContent = Directory.GetFiles(EventAssetPath, "*.*", SearchOption.AllDirectories);//Directory.GetDirectories(EventAssetPath);
                foreach (var item in AllFolderContent)
                {
                    string folderpathOld = item.Replace(EventAssetPath, "");
                    string folderpath = folderpathOld.Replace("\\", "/");
                    if (!listeEventImages.Contains(folderpath))
                    {
                        if(Path.GetFileName(folderpath) != "Config.ini")
                        {
                            File.Delete(item);
                        }
                    }
                    string[] dossier = Directory.GetFiles(Path.GetDirectoryName(item));
                    if (dossier.Count() == 0)
                    {
                        Directory.Delete(item);
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public static void CompareAndDeleteFtpLocalFolder(string eventCode, string[] directory, List<string> listeEventImages, List<string> LstFtpEventFolderName)
        {
            try
            {
                foreach (var file in directory)
                {
                    if (Directory.Exists(file))
                    {
                        string dirsubfolder = Path.GetFileName(file);
                        if (LstFtpEventFolderName.Contains(dirsubfolder))
                        {
                            var subfolder = Directory.GetDirectories(file);
                            if (subfolder.Count() != 0)
                            {
                                CompareAndDeleteFtpLocalFolder(eventCode, subfolder, listeEventImages, LstFtpEventFolderName);
                            }
                            else
                            {
                                string[] files = Directory.GetFiles(file);
                                if (files.Count() != 0)
                                {
                                    CompareAndDeleteFtpLocalFolder(eventCode, files, listeEventImages, LstFtpEventFolderName);
                                }
                                else
                                {
                                    Directory.Delete(file);
                                }
                            }

                        }
                        else
                        {
                            string[] filesinfolder = Directory.GetFiles(file);
                            if (filesinfolder.Count() != 0)
                            {
                                foreach (var item in filesinfolder)
                                {
                                    File.Delete(item);
                                }
                            }
                            Directory.Delete(file);
                        }
                    }
                    else
                    {
                        string directoryname = Path.GetFileName(Path.GetDirectoryName(file));
                        string parent = Path.GetDirectoryName(Path.GetDirectoryName(file));
                        string filename = Path.GetFileName(file);
                        string path = "";
                        if (Path.GetFileName(parent) == eventCode)
                        {
                            path = $"{directoryname}/{filename}";
                        }
                        else
                        {
                            path = $"{parent}/{directoryname}/{filename}";
                        }
                        if (!listeEventImages.Contains(path))
                        {
                            string filepath = Path.GetFullPath(file);
                            File.Delete(filepath);
                        }
                    }

                }
            }
            catch(Exception e)
            {

            }
            
        }
    public static async Task<IEnumerable<string>> CountRemotePhotos(string remoteUri, NetworkCredential credentials)
        {
            return await Task.Run(() =>
            {
                string[] list = null;
                try
                {
                    System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(remoteUri);
                    request.Credentials = credentials;
                    request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                    using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        }
                    }
                    var ftpFiles = list.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();

                    return ftpFiles;
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - CountRemotePhotos Error : {wex}");
                    return new List<string>();
                }
            });
        }

        public static async Task<IEnumerable<string>> CountRemoteImagesEvent(string remoteUri, NetworkCredential credentials)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var filesInFolders = GetAllFolderContent(credentials, remoteUri).ToList();

                    return filesInFolders.Select(x => x.Filename);
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - CountRemotePhotos Error : {wex}");
                    return new List<string>();
                }
            });
        }

        //GetMissingFilesInFtpNotInLocalOutNumber(@"C:\local\mods", "ftp://ftp.example.com/remote/mods/",new NetworkCredential("username", "password"), out nb);
        public static async Task<IEnumerable<string>> GetMissingFilesInFtpNotInLocalAndCount(string localPath, string remoteUri, NetworkCredential credentials) //out int filesNumber
        {
            return await Task.Run(() =>
            {
                List<string> remoteFiles = new List<string>();
                List<DirectoryItem> returnValue = new List<DirectoryItem>();
                string[] list = null;
                try
                {
                    System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(remoteUri);
                    request.Credentials = credentials;
                    request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                    using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        }
                    }

                    var fpttFiles = list.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();

                    IEnumerable<string> localFiles = Directory.GetFiles(localPath).Select(path => Path.GetFileName(path));

                    return fpttFiles.Except(localFiles);
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - GetMissingFilesInFtpNotInLocalAndCount Error : {wex}");
                    return new List<string>();
                }
            });
        }

        public static IEnumerable<FileDetails> GetAllFolderContent(NetworkCredential credentials, string remotePath)
        {
            //return  Task.Run(() =>
            //{
                try
                {
                    var result = new List<FileDetails>();
                    string[] lstFolders;
                    System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(remotePath);
                    request.Credentials = credentials;
                    request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            lstFolders = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        }
                        var folderExists = lstFolders.Where(x => x.Substring(0, 5).Contains("drwxr")).ToList();
                        if (folderExists != null && folderExists.Count > 0)
                        {
                            var filesExists = lstFolders.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();
                            if (filesExists != null && filesExists.Count() > 0)
                            {
                                foreach (var item in filesExists)
                                {
                                    result.Add(new FileDetails() {
                                        LastModified = GetFileDateLastModified(credentials, $"{remotePath}/{item}"),
                                        Filename = item
                                    });
                                }
                            }

                            var folders = lstFolders.Where(x => x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();
                            foreach (var fld in folders)
                            {
                            var sous_result = GetAllFolderContent(credentials, $"{remotePath}/{fld}").ToList();
                                if (sous_result != null && sous_result.Count() > 0)
                                {
                                    foreach (var item in sous_result.Select(x=>x.Filename))
                                    {
                                        //result.Add($"{fld}/{item}");
                                        result.Add(new FileDetails()
                                        {
                                            LastModified = GetFileDateLastModified(credentials, $"{remotePath}/{fld}/{item}"),
                                            Filename = $"{fld}/{item}"
                                            });
                                    }
                                }
                            }
                        }
                        else
                        {
                            var filesExists = lstFolders.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();
                            if (filesExists != null && filesExists.Count() > 0)
                            {
                                foreach (var item in filesExists)
                                {
                                    result.Add(new FileDetails()
                                    {
                                        LastModified = GetFileDateLastModified(credentials, $"{remotePath}/{item}"),
                                        Filename = item
                                    });
                                }
                            }
                        }

                        return result;
                    }
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - GetAllFolderContent Error : {wex}");
                    return new List<FileDetails>();
                }
            //});
        }
        
        private static DateTime GetFileDateLastModified(NetworkCredential credentials, string fullname)
        {
            System.Net.FtpWebRequest reqFTP;
            DateTime result = DateTime.MinValue;
            try
            {
                reqFTP = (System.Net.FtpWebRequest)System.Net.FtpWebRequest.Create(new Uri(fullname));
                reqFTP.UseBinary = true;
                reqFTP.KeepAlive = true;
                reqFTP.Credentials = credentials;
                reqFTP.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)reqFTP.GetResponse();
                result = response.LastModified;
                response.Close();

            }
            catch (Exception e)
            {

            }
            return result;
        }

        private static bool chk_con()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        //public static async Task<string> GetLatestFileDateInFolderContent(NetworkCredential credentials, string remotePath)
        public static DateTime GetLatestFileDateInFolderContent(NetworkCredential credentials, string remotePath)
        {
            DateTime datevers = DateTime.MinValue;
            //return await Task.Run(() =>
            //{
                try
                {
                    if (chk_con()) {

                        var lastFileModif = GetAllFolderContent(credentials, remotePath)
                                                .OrderByDescending(f => (f.LastModified == null ? DateTime.MinValue : f.LastModified))
                                                .FirstOrDefault();
                        //DateTime lastFileModif = getLastDateModif(credentials, remotePath);
                        if (lastFileModif != null)
                        {
                            var lastModif = lastFileModif.LastModified;
                            datevers = DateTime.Parse(lastModif.ToString());
                            //string dateModif = datevers.ToString("dd/MM/yyyy");
                            //string heureModif = datevers.ToString("HH");
                            //string minModif = datevers.ToString("mm");


                            //string code = remotePath.Split('/')[3];
                            //DateTime datelastmodif = Directory.GetLastWriteTimeUtc(Globals._assetsFolder + "\\" + code);
                            ////return $"({lastModif.Day}/{lastModif.Month}/{lastModif.Year} à {lastModif.Hour}h{lastModif.Minute})";
                            //if (datelastmodif < datevers)
                            //    return $"({dateModif} à {heureModif}h{minModif})";
                        }                        
                    }
                return datevers;
                    //return string.Empty;
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - GetLatestFileDateInFolderContent Error : {wex}");
                //return string.Empty;
                return datevers;
                }
            //});
        }

        public static async Task<DateTime> GetLatestFileDateInFolderContentAsync(NetworkCredential credentials, string remotePath)
        {
            DateTime datevers = DateTime.MinValue;
            return await Task.Run(() =>
            {
                try
                {
                    if (chk_con())
                    {

                        var lastFileModif = GetAllFolderContent(credentials, remotePath)
                                                .OrderByDescending(f => (f.LastModified == null ? DateTime.MinValue : f.LastModified))
                                                .FirstOrDefault();
                        //DateTime lastFileModif = getLastDateModif(credentials, remotePath);
                        if (lastFileModif != null)
                        {
                            var lastModif = lastFileModif.LastModified;
                            datevers = DateTime.Parse(lastModif.ToString());
                            //string dateModif = datevers.ToString("dd/MM/yyyy");
                            //string heureModif = datevers.ToString("HH");
                            //string minModif = datevers.ToString("mm");


                            //string code = remotePath.Split('/')[3];
                            //DateTime datelastmodif = Directory.GetLastWriteTimeUtc(Globals._assetsFolder + "\\" + code);
                            ////return $"({lastModif.Day}/{lastModif.Month}/{lastModif.Year} à {lastModif.Hour}h{lastModif.Minute})";
                            //if (datelastmodif < datevers)
                            //    return $"({dateModif} à {heureModif}h{minModif})";
                        }
                    }
                    return datevers;
                    //return string.Empty;
                }
                catch (WebException wex)
                {
                    Log.Error($"Compare.cs - GetLatestFileDateInFolderContent Error : {wex}");
                    //return string.Empty;
                    return datevers;
                }
            });
        }
        public static bool GetFileModifiedFtpVsLocal(string ftpFile, string localFile, NetworkCredential credentials)
        {
            bool result = false;
            try
            {
                System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(ftpFile);
                request.Credentials = credentials;
                request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse();
                var fileInfo = new FileInfo(localFile);
                var localFileInfo = File.GetLastWriteTimeUtc(localFile);
                if (localFileInfo != response.LastModified)
                    result = true;
            }
            catch (Exception ex)
            {
                Log.Error($"Compare.cs - GetFileModifiedFtpVsLocal Error : {ex}");
            }
            return result;
        }

        public static  IEnumerable<string> CompareFtpVsLocalAndCountMissingFiles(string code, bool runDownload)//async Task<IEnumerable<string>>
        {
            ////return await Task.Run(() =>
            ////{
                List<string> filesToDownload = new List<string>();
                //filesNumber = 0;
                NetworkCredential credentialsEvent = new NetworkCredential(ftpUserNameEvent, ftpPasswordEvent);
                string ftpDirectoryPathCode = $"ftp://{ftpServerIP}/{code}";
                string locDirectoryPathCode = $"C:/EVENTS/Assets/{code}";
                string remoteFolderToLocal = $"C:/EVENTS/Download/{code}/Remote";
                Directory.Delete(remoteFolderToLocal, true);
                if (!Directory.Exists(remoteFolderToLocal))
                    Directory.CreateDirectory(remoteFolderToLocal);

                //PHOTOS
                try
                {
                    
                    if(runDownload)
                        DownloadFtpDirectory($"{ftpDirectoryPathCode}/", credentialsEvent, remoteFolderToLocal);

                    //Get all files from remote event folder
                    var filesInFolders = GetAllFolderContent(credentialsEvent, ftpDirectoryPathCode).ToList();
                    foreach (var fi in filesInFolders.Select(x => x.Filename))
                    {
                        //if (!fi.Contains("Config.ini")) { 
                        //if (!File.Exists($"{locDirectoryPathCode}/{fi}"))
                        //{ //Remote file is not existe in local
                        //    filesToDownload.Add(fi);
                        //}
                        //else
                        //{ //Comparer les images
                        //var isImgEquals = CompareBitmapsFast((Bitmap)Bitmap.FromFile($"{remoteFolderToLocal}/{fi}"), (Bitmap)Bitmap.FromFile($"{locDirectoryPathCode}/{fi}"));
                        //if (fi.EndsWith(".png") || fi.EndsWith(".jpg"))
                        //{
                        //    var isImgEquals = CompareTwoBitmaps((Bitmap)Bitmap.FromFile($"{remoteFolderToLocal}/{fi}"), (Bitmap)Bitmap.FromFile($"{locDirectoryPathCode}/{fi}"));
                        //    if (isImgEquals != CompareResult.ciCompareOk)
                        //        filesToDownload.Add(fi);
                        //}
                        //if (fi.Contains("Config.ini"))
                        //    {
                        //        FileInfo remotefi = new FileInfo(remoteFolderToLocal + "/" + fi);
                        //        FileInfo localfi = new FileInfo(locDirectoryPathCode + "/" + fi);
                        //        if (!Compare.FilesAreEqual(remotefi, localfi))
                        //        {
                        //            filesToDownload.Add(fi);
                        //        }

                        //    }
                        //    else
                        //    {
                        //        FileInfo remotefi = new FileInfo(remoteFolderToLocal + "/" + fi);
                        //        FileInfo localfi = new FileInfo(locDirectoryPathCode + "/" + fi);
                        //        if (!Compare.FilesAreEqual(remotefi, localfi))
                        //        {
                        //            filesToDownload.Add(fi);
                        //        }
                        //    }
                        //}
                        //}
                        filesToDownload.Add(fi);
                    }


                    //Traitenement de config.ini
                    //var currentConfig = $"{locDirectoryPathCode}/Config.ini";
                    //var remoteConfig = $"{remoteFolderToLocal}/Configuration/Config.ini";
                    //bool isConfigChanged = false;

                    //if (File.Exists(remoteConfig) && File.Exists(currentConfig)) { 
                    //    isConfigChanged = !FilesContentsAreEqualAsync(new FileInfo(remoteConfig), new FileInfo(currentConfig)).GetAwaiter().GetResult();
                    //}

                    //if (isConfigChanged)
                    //filesToDownload.Add("Configuration/Config.ini");


            }
                catch (Exception ex)
                {
                    Log.Error($"Compare.cs - CompareFtpVsLocalAndCountMissingFiles Error : {ex}");
                    return new List<string>();
                }

                return filesToDownload;
            //});
        }

        public static FileInfo GetNewestFile(DirectoryInfo directory)
        {
            try
            {
                return directory.GetFiles()
                .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
                .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
                .FirstOrDefault();
            }
            catch(Exception e)
            {
                return null;
            }
        }


        //GetMissingFilesInFtpNotInLocal(@"C:\local\mods", "ftp://ftp.example.com/remote/mods/",new NetworkCredential("username", "password"));
        public static IEnumerable<string> GetMissingFilesInFtpNotInLocal(string localPath, string remoteUri, NetworkCredential credentials)
        {
            List<string> remoteFiles = new List<string>();
            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;
            try
            {
                System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(remoteUri);
                request.Credentials = credentials;
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                }

                var ftpFiles = list.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();
                IEnumerable<string> localFiles = Directory.GetFiles(localPath).Select(path => Path.GetFileName(path));
                
                return ftpFiles.Except(localFiles);
            }
            catch (WebException wex)
            {
                Log.Error($"Compare.cs - GetMissingFilesInFtpNotInLocal Error : {wex}");
                //ftp Directory is not exist
                return new List<string>();
            }
        }

        //GetMissingFilesInLocalNotInFtp(@"C:\local\mods", "ftp://ftp.example.com/remote/mods/",new NetworkCredential("username", "password"));
        public static IEnumerable<string> GetMissingFilesInLocalNotInFtp(string localPath, string remoteUri, NetworkCredential credentials)
        {
            List<string> remoteFiles = new List<string>();
            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;
            try
            {
                System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)WebRequest.Create(remoteUri);
                request.Credentials = credentials;
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                }

                var ftpFiles = list.Where(x => x.Split(' ').Last() != "data.csv" && !x.Substring(0, 5).Contains("drwxr")).Select(x => x.Split(' ').Last()).ToList();
                IEnumerable<string> localFiles = Directory.GetFiles(localPath).Select(path => Path.GetFileName(path));

                return localFiles.Except(ftpFiles);
            }
            catch (WebException wex)
            {
                //ftp Directory is not exist

                return new List<string>();
            }
        }

        public static bool CompareBitmapsFast(Bitmap bmp1, Bitmap bmp2)
        {
            bool result = true;
            try
            {
                if (bmp1 == null || bmp2 == null)
                    return false;
                if (object.Equals(bmp1, bmp2))
                    return true;
                if (!bmp1.Size.Equals(bmp2.Size) || !bmp1.PixelFormat.Equals(bmp2.PixelFormat))
                    return false;

                int bytes = bmp1.Width * bmp1.Height * (Image.GetPixelFormatSize(bmp1.PixelFormat) / 8);


                byte[] b1bytes = new byte[bytes];
                byte[] b2bytes = new byte[bytes];

                BitmapData bitmapData1 = bmp1.LockBits(new Rectangle(0, 0, bmp1.Width, bmp1.Height), ImageLockMode.ReadOnly, bmp1.PixelFormat);
                BitmapData bitmapData2 = bmp2.LockBits(new Rectangle(0, 0, bmp2.Width, bmp2.Height), ImageLockMode.ReadOnly, bmp2.PixelFormat);

                Marshal.Copy(bitmapData1.Scan0, b1bytes, 0, bytes);
                Marshal.Copy(bitmapData2.Scan0, b2bytes, 0, bytes);

                for (int n = 0; n <= bytes - 1; n++)
                {
                    if (b1bytes[n] != b2bytes[n])
                    {
                        result = false;
                        break;
                    }
                }

                bmp1.UnlockBits(bitmapData1);
                bmp2.UnlockBits(bitmapData2);
            }
            catch (Exception ex)
            {
                Log.Error($"Compare.cs - CompareBitmapsFast Error : {ex}");
            }
            finally
            {
                bmp1.Dispose();
                bmp2.Dispose();                
            }
            
            return result;
        }
    

        public enum CompareResult
        {
            ciCompareOk,
            ciPixelMismatch,
            ciSizeMismatch
        };

        public static CompareResult CompareTwoBitmaps(Bitmap bmp1, Bitmap bmp2)
        {
            CompareResult cr = CompareResult.ciCompareOk;
            byte[] btImage1 = new byte[1];
            byte[] btImage2 = new byte[1];
            try
            {
                //Test to see if we have the same size of image
                if (bmp1.Size != bmp2.Size)
                {
                    cr = CompareResult.ciSizeMismatch;
                }
                else
                {
                    //Convert each image to a byte array
                    System.Drawing.ImageConverter ic =
                           new System.Drawing.ImageConverter();
                    //byte[] btImage1 = new byte[1];
                    btImage1 = (byte[])ic.ConvertTo(bmp1, btImage1.GetType());
                    //byte[] btImage2 = new byte[1];
                    btImage2 = (byte[])ic.ConvertTo(bmp2, btImage2.GetType());

                    //Compute a hash for each image
                    SHA256Managed shaM = new SHA256Managed();
                    byte[] hash1 = shaM.ComputeHash(btImage1);
                    byte[] hash2 = shaM.ComputeHash(btImage2);

                    //Compare the hash values
                    for (int i = 0; i < hash1.Length && i < hash2.Length
                                      && cr == CompareResult.ciCompareOk; i++)
                    {
                        if (hash1[i] != hash2[i])
                            cr = CompareResult.ciPixelMismatch;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Compare.cs - CompareTwoBitmaps Error : {ex}");
            }
            finally
            {
                bmp1.Dispose();
                bmp2.Dispose();
                bmp1 = null;
                bmp2 = null;
            }
            
            return cr;
        }


        const int BYTES_TO_READ = sizeof(Int64);
        static bool FilesAreEqual(FileInfo first, FileInfo second)
        {
            if (first.Length != second.Length)
                return false;

            if (string.Equals(first.FullName, second.FullName, StringComparison.OrdinalIgnoreCase))
                return true;

            int iterations = (int)Math.Ceiling((double)first.Length / BYTES_TO_READ);

            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                byte[] one = new byte[BYTES_TO_READ];
                byte[] two = new byte[BYTES_TO_READ];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }
    }
    public class FileCompare : IEqualityComparer<FileInfo>
    {
        public FileCompare() { }

        public bool Equals(FileInfo f1, FileInfo f2)
        {
            //return (f1.Name == f2.Name &&
            //        f1.Length == f2.Length);
            return Compare.FilesContentsAreEqualAsync(f1, f2).GetAwaiter().GetResult();
        }

        // Return a hash that reflects the comparison criteria. According to the   
        // rules for IEqualityComparer<T>, if Equals is true, then the hash codes must  
        // also be equal. Because equality as defined here is a simple value equality, not  
        // reference identity, it is possible that two or more objects will produce the same  
        // hash code.  
        public int GetHashCode(FileInfo fi)
        {
            string s = $"{fi.Name}{fi.Length}";
            return s.GetHashCode();
        }
    }

    public class FileSimpleCompare : IEqualityComparer<FileInfo>
    {
        public FileSimpleCompare() { }

        public bool Equals(FileInfo f1, FileInfo f2)
        {
            return (f1.Name == f2.Name &&
                    f1.Length == f2.Length);
            //return Compare.FilesContentsAreEqualAsync(f1, f2).GetAwaiter().GetResult();
        }

        // Return a hash that reflects the comparison criteria. According to the   
        // rules for IEqualityComparer<T>, if Equals is true, then the hash codes must  
        // also be equal. Because equality as defined here is a simple value equality, not  
        // reference identity, it is possible that two or more objects will produce the same  
        // hash code.  
        public int GetHashCode(FileInfo fi)
        {
            string s = $"{fi.Name}{fi.Length}";
            return s.GetHashCode();
        }
       
    }

    public static class StringExtension
    {
        public static string CapitalizeFirst(this string s)
        {
            bool IsNewSentense = true;
            var result = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (IsNewSentense && char.IsLetter(s[i]))
                {
                    result.Append(char.ToUpper(s[i]));
                    IsNewSentense = false;
                }
                else
                    result.Append(s[i]);

                if (s[i] == '!' || s[i] == '?' || s[i] == '.')
                {
                    IsNewSentense = true;
                }
            }

            return result.ToString();
        }

        public static string UppercaseFirstEach(this string s)
        {
            char[] a = s.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }

            return new string(a);
        }
    }
}
