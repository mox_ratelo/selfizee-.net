﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Selfizee
{
    /// <summary>
    /// Interaction logic for PostInstall.xaml
    /// </summary>
    public partial class PostInstall : UserControl
    {
        public PostInstall()
        {
            InitializeComponent();
        }

        public void GotoAccueil(object sender, MouseButtonEventArgs e)
        {
            string code = Globals._eventToDownload;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (PostInstallViewModel)DataContext;
            if (viewModel.GoToAccueil.CanExecute(null))
                viewModel.GoToAccueil.Execute(null);
        }

        //private void LaunchAnimation_Click(object sender, RoutedEventArgs e)
        //{
        //    string code = Globals._eventToDownload;
        //    IniUtility iniFile = new IniUtility(Globals._appConfigFile);
        //    iniFile.Write("id", code, "EVENTSCONFIG");
        //    var viewModel = (PostInstallViewModel)DataContext;
        //    if (viewModel.GoToAccueil.CanExecute(null))
        //        viewModel.GoToAccueil.Execute(null);
        //}

        public void GotoDashBoard(object sender, MouseButtonEventArgs e)
        {
            string code = Globals._eventToDownload;
            IniUtility iniFile = new IniUtility(Globals._appConfigFile);
            iniFile.Write("id", code, "EVENTSCONFIG");
            var viewModel = (PostInstallViewModel)DataContext;
            if (viewModel != null)
            {
                if (Globals.ScreenType == "SPHERIK")
                {
                    if (viewModel.GoToClientSumarySpherik.CanExecute(null))
                        viewModel.GoToClientSumarySpherik.Execute(null);
                }
                else if (Globals.ScreenType == "DEFAULT")
                {
                    if (viewModel.GoToClientSumary.CanExecute(null))
                        viewModel.GoToClientSumary.Execute(null);
                }
            }
        }
    }
}
