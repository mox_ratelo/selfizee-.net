﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Selfizee
{
    public class VisualisationViewModel : BaseViewModel, IPageViewModel
    {
        private ICommand _goToTanks;
        private ICommand _goToFiltre;
        private ICommand _gotoTakePhoto;
        private ICommand _gotoFormStepOne;
        private ICommand _gotoAccueil;
        private ICommand _gotoHotline;
        private ICommand _goToConnexionConfig;

        public ICommand GoToFiltre
        {
            get
            {
                return _goToFiltre ?? (_goToFiltre = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToFiltre", "FromVisualisation");
                }));
            }
        }

        private ICommand _goToClientSumary;
        public ICommand GoToClientSumary
        {
            get
            {
                return _goToClientSumary ?? (_goToClientSumary = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumary", "");
                }));
            }
        }
        private ICommand _goToClientSumarySpherik;
        public ICommand GoToClientSumarySpherik
        {
            get
            {
                return _goToClientSumarySpherik ?? (_goToClientSumarySpherik = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToClientSumarySpherik", "");
                }));
            }
        }

        private ICommand _goToConnexionClient;

        public ICommand GoToConnexionClient
        {
            get
            {
                return _goToConnexionClient ?? (_goToConnexionClient = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToConnexionClient", "");
                }));
            }
        }

        public ICommand GoToConnexionConfig
        {
            get
            {
                return _goToConnexionConfig ?? (_goToConnexionConfig = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToConnexionConfig", "");
                }));
            }
        }
        public ICommand GoToThanks
        {
            get
            {
                return _goToTanks ?? (_goToTanks = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToThanks", "");
                }));
            }
        }

        public ICommand GoToHotline
        {
            get
            {
                return _gotoHotline ?? (_gotoHotline = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToHotline", "");
                }));
            }
        }

        public ICommand GoToAcceuil
        {
            get
            {
                return _gotoAccueil ?? (_gotoAccueil = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToAcceuil", "");
                }));
            }
        }

        public ICommand GoToTakePhoto
        {
            get
            {
                return _gotoTakePhoto ?? (_gotoTakePhoto = new RelayCommand(x =>
                {
                    //Mediator.Notify("GoToTakePhoto", "");
                    Mediator.Notify("GoToAccueil", "FromVisualisation");
                }));
            }
        }

        public ICommand GoToFormStepOne
        {
            get
            {
                return _gotoFormStepOne?? (_gotoFormStepOne = new RelayCommand(x=>{
                    Mediator.Notify("GoToFormStepOne", ""); 
                })); 
            }
        }

        public string selectedFilter { get; set; }
    }
}
