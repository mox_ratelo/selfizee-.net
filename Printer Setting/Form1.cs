﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace Printer_Settings
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitialseCurrentSetup();
            FindPrinters();
        }

       
        PrinterSettings CurrentPrintSettings = new PrinterSettings();
        PrintDocument pd = new PrintDocument();
        PrintSettings objPrinterSetup = new PrintSettings();
        PrinterSetup CurrentSetup = new PrinterSetup();
        PrinterSetup DefaultSetup = new PrinterSetup();
        BinaryFormatter formatter = new BinaryFormatter();
        bool nuInhibit = false;
        //byte[] Comparator1;
        //byte[] Comparator2;
        byte[] DevModeArray;

        //-------------------------------Button event handlers------------------------------------
        //private void btnStore1_Click(object sender, EventArgs e)
        //{
        //    string Status = "In Use";
        //    CurrentSetup = new PrinterSetup();
        //    int Index = 0;
        //    Button btn = (Button)sender;
        //    if (ModifierKeys == Keys.Control)
        //    {
        //        Status = "Free";
        //    }
           
        //}
        private void btnRestore1_Click(object sender, EventArgs e)
        {
            CurrentSetup = new PrinterSetup();
            int Index = 0;
            Button btn = (Button)sender;
            switch (btn.Name)
            {
                case "btnRecall1":
                    Index = 0;
                    goto case "Set";
                case "btnRecall2":
                    Index = 1;
                    goto case "Set";
                case "btnRecall3":
                    Index = 2;
                    goto case "Set";
                case "btnRecall4":
                    Index = 3;
                    goto case "Set";
                case "btnRecall5":
                    Index = 4;
                    goto case "Set";
                case "btnRecall6":
                    Index = 5;
                    goto case "Set";
                case "btnRecall7":
                    Index = 6;
                    goto case "Set";
                case "btnRecall8":
                    Index = 7;
                    goto case "Set";
                case "btnRecall9":
                    Index = 8;
                    goto case "Set";
                case "btnRecall10":
                    Index = 9;
                    goto case "Set";
                case "Set":
                    CurrentSetup = (PrinterSetup)objPrinterSetup.alPrinterSetup[Index];
                    if (CurrentSetup == null)
                    {
                        MessageBox.Show("To recall this location SET IT FIRST!");
                    }
                    else
                    {
                        Recall();

                        btn.ForeColor = Color.Red;
                    }
                    break;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Reset();
            saveFileDialog1.FileName = "";
            saveFileDialog1.Title = "Sauvegarder le fichier de param�trage";
            saveFileDialog1.Filter = "Fichier de param�tre de l'imprimante|*.Psf";
            saveFileDialog1.InitialDirectory = @"C:\Users\Kermit\Desktop\Devmode";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Object obj = new PrintSettings();
                obj = objPrinterSetup;
                Stream stream = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, obj);
                stream.Close();
            }
        }
        private void btnSaveDevmode_Click(object sender, EventArgs e)//Save devmode structure
        {
            {
                saveFileDialog1.Reset();
                saveFileDialog1.FileName = "";
                saveFileDialog1.Title = "Sauvegarder les configs";
                saveFileDialog1.Filter = "fichier |*.Bin";
                saveFileDialog1.InitialDirectory = @"C:\Users\Kermit\Desktop\Devmode";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    GetDevmode(CurrentPrintSettings, 1, saveFileDialog1.FileName);
                }
            }
        }
        //private void btnLoad_Click(object sender, EventArgs e)
        //{
        //    {
        //        string Filename = "";
        //        string extension = "";
        //        openFileDialog1.Reset();
        //        openFileDialog1.Title = "Load Printer Settings File";
        //        openFileDialog1.Filter = "Devmode Structure File|*.Bin|Printer Settings File|*.Psf";
        //        openFileDialog1.InitialDirectory = @"C:\Users\Kermit\Desktop\Devmode";
        //        if (openFileDialog1.ShowDialog() == DialogResult.OK)
        //        {
        //            Filename = openFileDialog1.FileName;
        //            extension = Path.GetExtension(Filename).ToUpper();
        //            if (File.Exists(openFileDialog1.FileName))
        //            {
        //                if (extension == ".PSF")
        //                {                            
        //                    Loadfile(Filename);
        //                }
        //                if (extension == ".BIN")
        //                {
                                       
        //                }
        //                UpdatePrintForm(CurrentPrintSettings, CurrentSetup);
        //            }
        //        }
        //    }
        //}
        private void btnProperties_Click(object sender, EventArgs e)
        {
            IntPtr ipDevMode = CurrentPrintSettings.GetHdevmode();
            CurrentPrintSettings.DefaultPageSettings.CopyToHdevmode(ipDevMode);
            CurrentPrintSettings = OpenPrinterPropertiesDialog(CurrentPrintSettings);
            UpdatePrintForm(CurrentPrintSettings, CurrentSetup);
        }
        //private void btnLoad1_Click(object sender, EventArgs e)
        //{
            
        //}
        //private void btnLoad2_Click(object sender, EventArgs e)
        //{
           
        //}
        
        private void cbxInstalledPrinters_SelectedIndexChanged(object sender, EventArgs e)
        {
            pd = new PrintDocument();
            if (cbxInstalledPrinters.SelectedIndex != -1)
            {
                pd.PrinterSettings.PrinterName = cbxInstalledPrinters.Text;
                LoadPrinterSettings();
            }
        }
   
        [DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter,
        [MarshalAs(UnmanagedType.LPWStr)] string pDeviceNameg,
        IntPtr pDevModeOutput, IntPtr pDevModeInput, int fMode);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalFree(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalLock(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalUnlock(IntPtr handle);

       
        private void GetDevmode(PrinterSettings printerSettings, int mode, String Filename)
        {
          
            IntPtr hDevMode = IntPtr.Zero;                        
            IntPtr pDevMode = IntPtr.Zero;                          
            IntPtr hwnd = this.Handle;
            try
            {
                
                hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
               
                pDevMode = GlobalLock(hDevMode);
                int sizeNeeded = DocumentProperties(hwnd, IntPtr.Zero, printerSettings.PrinterName, IntPtr.Zero, pDevMode, 0);
                if (sizeNeeded <= 0)
                {
                    MessageBox.Show("Devmode Bummer, Cant get size of devmode structure");
                    GlobalUnlock(hDevMode);
                    GlobalFree(hDevMode);
                    return;
                }
                DevModeArray = new byte[sizeNeeded];
                if (mode == 1)
                {
                    FileStream fs = new FileStream(Filename, FileMode.Create);
                    for (int i = 0; i < sizeNeeded; ++i)
                    {
                        fs.WriteByte(Marshal.ReadByte(pDevMode, i));
                    }
                    fs.Close();
                    fs.Dispose();
                }
                if (mode == 2)
                {
                    for (int i = 0; i < sizeNeeded; ++i)
                    {
                        DevModeArray[i] = (byte)(Marshal.ReadByte(pDevMode, i));   
                    }
                }
               
               
                GlobalUnlock(hDevMode);
                
                GlobalFree(hDevMode);
                hDevMode = IntPtr.Zero;
            }
            catch (Exception ex)
            {
                if (hDevMode != IntPtr.Zero)
                {
                    MessageBox.Show("BUGGER");
                    GlobalUnlock(hDevMode);
                    
                    GlobalFree(hDevMode);
                    hDevMode = IntPtr.Zero;
                }
            }
        }
        private void SetDevmode(PrinterSettings printerSettings, int mode, String Filename)
        {
            
            IntPtr hDevMode = IntPtr.Zero;                        
            IntPtr pDevMode = IntPtr.Zero;                          
            Byte[] Temparray;
            try
            {
                DevModeArray = CurrentSetup.Devmodearray;
                
                hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);                
               
                pDevMode = GlobalLock(hDevMode);                             
               
                    for (int i = 0; i < DevModeArray.Length; ++i)
                    {
                        Marshal.WriteByte(pDevMode, i, DevModeArray[i]);
                    }
               
                GlobalUnlock(hDevMode);
              
                printerSettings.SetHdevmode(hDevMode);
                printerSettings.DefaultPageSettings.SetHdevmode(hDevMode);
                
                GlobalFree(hDevMode);
            }
            catch (Exception ex)
            {
                if (hDevMode != IntPtr.Zero)
                {
                    MessageBox.Show("BUGGER");
                    GlobalUnlock(hDevMode);
                    
                    GlobalFree(hDevMode);
                    hDevMode = IntPtr.Zero;
                }
            }

        }
        private void ChangeSetup()
        {
            CurrentPrintSettings.DefaultPageSettings.Landscape = CurrentSetup.Landscape;
            CurrentPrintSettings.DefaultPageSettings.PaperSize = CurrentSetup.PaperSize;
            
            CurrentPrintSettings.DefaultPageSettings.PaperSource = CurrentSetup.PaperSource;
            
            CurrentPrintSettings.Duplex= CurrentSetup.DSided;
        }      
        private PrinterSettings OpenPrinterPropertiesDialog(PrinterSettings printerSettings)
        {            
            IntPtr hDevMode = IntPtr.Zero;
            IntPtr devModeData = IntPtr.Zero;
            IntPtr hPrinter = IntPtr.Zero;
            String pName = printerSettings.PrinterName;
            try
            {
                hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
                IntPtr pDevMode = GlobalLock(hDevMode);
                int sizeNeeded = DocumentProperties(this.Handle, IntPtr.Zero, pName, pDevMode, pDevMode, 0);

                if (sizeNeeded < 0)
                {
                    MessageBox.Show("Bummer, Cant get size of devmode structure");
                    Marshal.FreeHGlobal(devModeData);
                    Marshal.FreeHGlobal(hDevMode);
                    devModeData = IntPtr.Zero;
                    hDevMode = IntPtr.Zero;
                    return printerSettings;
                }
                devModeData = Marshal.AllocHGlobal(sizeNeeded);
                
                int returncode = DocumentProperties(this.Handle, IntPtr.Zero, pName, devModeData, pDevMode, 14);
                if (returncode < 0) 
                {
                    MessageBox.Show("Dialogue Bummer, Got me a devmode, but the dialogue got stuck");
                    Marshal.FreeHGlobal(devModeData);
                    Marshal.FreeHGlobal(hDevMode);
                    devModeData = IntPtr.Zero;
                    hDevMode = IntPtr.Zero;
                    return printerSettings;
                }
                if (returncode ==2) 
                {
                    GlobalUnlock(hDevMode);
                    if (hDevMode != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(hDevMode); 
                        hDevMode = IntPtr.Zero;
                    }
                    if (devModeData != IntPtr.Zero)
                    {
                        GlobalFree(devModeData);
                        devModeData = IntPtr.Zero;
                    }
                }
                GlobalUnlock(hDevMode);

                if (hDevMode != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(hDevMode); 
                    hDevMode = IntPtr.Zero;
                }
                if (devModeData != IntPtr.Zero)
                {
                    printerSettings.SetHdevmode(devModeData);
                    printerSettings.DefaultPageSettings.SetHdevmode(devModeData);
                    GlobalFree(devModeData);
                    devModeData = IntPtr.Zero;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred, caught and chucked back\n" + ex.Message);
            }
            finally
            {
                if (hDevMode != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(hDevMode);
                }
                if (devModeData != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(devModeData);
                }
            }
            return printerSettings;
        }
        private void UpdatePrintForm(PrinterSettings TempSettings, PrinterSetup TempSetup)
        {
            int i = 0;  

            cbxInstalledPrinters.Text = TempSettings.PrinterName;
            if (TempSettings.IsValid == false)
            {
                MessageBox.Show("No such Printer on your system");
                return;
            }
                                     
        }
        private PaperSize SetCustomPaper(int CustomWidth, int CustomHeight, int RawKind, string Name)
        {
            if (CustomWidth <= 0)
            {
                CustomWidth = 100;
            }
            if (CustomHeight <= 0)
            {
                CustomHeight = 100;
            }
            PaperSize myPapersize = new PaperSize(Name, CustomWidth, CustomHeight);
            myPapersize.RawKind = RawKind;
            return myPapersize;
        }
        private void InitialseCurrentSetup()
        {
            objPrinterSetup.alPrinterSetup.Clear();
            for (int Iterator = 0; Iterator <= 9; Iterator++)
            {
                PrinterSetup temp = new PrinterSetup();
                temp = null;
                objPrinterSetup.alPrinterSetup.Add(temp);
            }
        }
        private void Store()
        {
            GetDevmode(CurrentPrintSettings, 2, "");
            CurrentSetup.NameOfPrinter = CurrentPrintSettings.PrinterName;
            CurrentSetup.PaperSize = CurrentPrintSettings.DefaultPageSettings.PaperSize;
            CurrentSetup.PrintQuality = CurrentPrintSettings.DefaultPageSettings.PrinterResolution;
            CurrentSetup.PaperSource = CurrentPrintSettings.DefaultPageSettings.PaperSource;
            CurrentSetup.CanDuplex = CurrentPrintSettings.CanDuplex;
            CurrentSetup.DSided = CurrentPrintSettings.Duplex;
            CurrentSetup.Devmodearray = DevModeArray;
        }
        private void Recall()
        {
            CurrentPrintSettings.PrinterName = CurrentSetup.NameOfPrinter;
            UpdatePrintForm(CurrentPrintSettings, CurrentSetup);        
            UpdatePrintForm(CurrentPrintSettings, CurrentSetup);
        }
        public void Loadfile(String Filename)          
        {
            if (File.Exists(Filename))
            {
                PrintSettings Fileobject = new PrintSettings(); 
                PrintSettings Tempobject = new PrintSettings();   
                Stream stream = new FileStream(Filename, FileMode.Open, FileAccess.Read, FileShare.None);
                Fileobject = (PrintSettings)formatter.Deserialize(stream);
                objPrinterSetup = (PrintSettings)Fileobject;
               
                stream.Close();
            }
        }
        private void FindPrinters()
        {
            cbxInstalledPrinters.Items.Clear();
            foreach (String printer in PrinterSettings.InstalledPrinters)
            {
                cbxInstalledPrinters.Items.Add(printer.ToString());
            }
            if (cbxInstalledPrinters.Items.Count > 0)
            {
                cbxInstalledPrinters.SelectedIndex = 0;
            }
            LoadPrinterSettings();
        }
        private void LoadPrinterSettings() 
        {
            try
            {
                nuInhibit = true;
                CurrentPrintSettings = pd.PrinterSettings;
                tbxPrinterName.Text = CurrentPrintSettings.PrinterName;
               
                foreach (PaperSize PS in CurrentPrintSettings.PaperSizes)  
                {
                    
                }
                
               
                foreach (PrinterResolution PR in CurrentPrintSettings.PrinterResolutions)  
                {
                    
                }
               
                nuInhibit = false;
                UpdatePrintForm(CurrentPrintSettings, CurrentSetup);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                nuInhibit = false;
            }
        }    



        /// <summary>
        /// This class is the printer settings item. contains notes, printername, source, copies etc
        /// </summary>
        [Serializable]
        public class PrinterSetup
        {
            public string PrinterNotes = "";
            public PaperSize PaperSize;
            public PaperSource PaperSource;
            public PrinterResolution PrintQuality;
            public string NameOfPrinter = "";
            public bool Landscape = false;
            public bool CanDuplex = false;
            public Duplex DSided;
            public byte[] Devmodearray;
        }
        /// <summary>
        /// This class is the printer settings arraylist that can be saved to a file
        /// </summary>
        [Serializable]
        public class PrintSettings
        {
            public String strVersion = "";
            public ArrayList alPrinterSetup = new ArrayList();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Loadfile("D:\\Pojet selfizee\\multishoot.bin");
        }
    }
}