﻿namespace Printer_Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.cbxInstalledPrinters = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnProperties = new System.Windows.Forms.Button();
            this.tbxPrinterName = new System.Windows.Forms.TextBox();
            this.btnSaveDevmode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // cbxInstalledPrinters
            // 
            this.cbxInstalledPrinters.FormattingEnabled = true;
            this.cbxInstalledPrinters.Location = new System.Drawing.Point(189, 26);
            this.cbxInstalledPrinters.Name = "cbxInstalledPrinters";
            this.cbxInstalledPrinters.Size = new System.Drawing.Size(270, 21);
            this.cbxInstalledPrinters.TabIndex = 2;
            this.cbxInstalledPrinters.SelectedIndexChanged += new System.EventHandler(this.cbxInstalledPrinters_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "S�l�ctionner une  imprimante";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(31, 68);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(131, 20);
            this.btnProperties.TabIndex = 4;
            this.btnProperties.Text = "Configurer";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // tbxPrinterName
            // 
            this.tbxPrinterName.Location = new System.Drawing.Point(189, 68);
            this.tbxPrinterName.Name = "tbxPrinterName";
            this.tbxPrinterName.ReadOnly = true;
            this.tbxPrinterName.Size = new System.Drawing.Size(270, 20);
            this.tbxPrinterName.TabIndex = 1;
            // 
            // btnSaveDevmode
            // 
            this.btnSaveDevmode.Location = new System.Drawing.Point(31, 105);
            this.btnSaveDevmode.Name = "btnSaveDevmode";
            this.btnSaveDevmode.Size = new System.Drawing.Size(131, 41);
            this.btnSaveDevmode.TabIndex = 2;
            this.btnSaveDevmode.Text = "Sauvegarder en fichier bin";
            this.btnSaveDevmode.UseVisualStyleBackColor = true;
            this.btnSaveDevmode.Click += new System.EventHandler(this.btnSaveDevmode_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 181);
            this.Controls.Add(this.btnSaveDevmode);
            this.Controls.Add(this.btnProperties);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxInstalledPrinters);
            this.Controls.Add(this.tbxPrinterName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ComboBox cbxInstalledPrinters;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnProperties;
        private System.Windows.Forms.TextBox tbxPrinterName;
        private System.Windows.Forms.Button btnSaveDevmode;
    }
}

