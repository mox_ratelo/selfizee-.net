﻿namespace SWFPlayer
{
    partial class FlashPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlashPlayer));
            this.SWFPlayerObject = new AxShockwaveFlashObjects.AxShockwaveFlash();
            ((System.ComponentModel.ISupportInitialize)(this.SWFPlayerObject)).BeginInit();
            this.SuspendLayout();
            // 
            // SWFPlayerObject
            // 
            this.SWFPlayerObject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SWFPlayerObject.Enabled = true;
            this.SWFPlayerObject.Location = new System.Drawing.Point(1, 0);
            this.SWFPlayerObject.Name = "SWFPlayerObject";
            this.SWFPlayerObject.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("SWFPlayerObject.OcxState")));
            this.SWFPlayerObject.Size = new System.Drawing.Size(1920, 1080);
            this.SWFPlayerObject.TabIndex = 0;
            this.SWFPlayerObject.UseWaitCursor = true;
            // 
            // FlashPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.SWFPlayerObject);
            this.Enabled = false;
            this.Name = "FlashPlayer";
            this.Size = new System.Drawing.Size(2305, 1117);
            this.UseWaitCursor = true;
            ((System.ComponentModel.ISupportInitialize)(this.SWFPlayerObject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxShockwaveFlashObjects.AxShockwaveFlash SWFPlayerObject;
    }
}
