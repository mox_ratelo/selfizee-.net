﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWFPlayer
{
    public partial class FlashPlayer: UserControl
    {
        public FlashPlayer(string uri)
        {
            InitializeComponent();
            this.SWFPlayerObject.LoadMovie(0, uri);
            this.SWFPlayerObject.Play();
            this.MaximumSize = MaximumSize;
            this.AutoSize = true;
            this.Dock = DockStyle.Fill;
            SWFPlayerObject.Margin = Padding.Empty;
            //SWFPlayerObject.Dock = DockStyle.Fill;
        }

        public void LoadMovie(int i, string uri)
        {
            this.SWFPlayerObject.LoadMovie(i, uri);
        }

        public void Play()
        {
            this.SWFPlayerObject.Play();
        }
    }
}
