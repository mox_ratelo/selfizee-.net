﻿using System.Windows;

namespace KeyBoardControl.Keyboard.Keys
{
    public class OnScreenKeySpecial : OnScreenKey
    {
        internal OnScreenKeySpecial(int row, int column, string label, ExecuteDelegate executeDelegate,string path)
             : base(row, column, new[] { label }, null, executeDelegate,path) { }

        internal OnScreenKeySpecial(int row, int column, string label, string value)
        : base(row, column, new[] { label })
        {
            Value = value;
        }

        internal override void KeyPressEventHandler(object sender, RoutedEventArgs routedEventArgs)
        {
            IsChecked = false;
            OnClick(new OnScreenKeyPressEventArgs(Execute));
        }

        protected override void Update() { }

        public override void CanType(bool canPress) { }
    }
}
