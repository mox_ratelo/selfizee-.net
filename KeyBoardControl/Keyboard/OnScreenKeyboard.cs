﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Drawing;
using KeyBoardControl.Keyboard.Keys;
using System.IO;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using System.Windows.Data;
using System.Text;
using System.Windows.Markup;
using System.Diagnostics;

namespace KeyBoardControl.Keyboard
{
    public class OnScreenKeyboard : Grid
    {
        Style grayStyle = new Style(typeof(ToggleButton));
        Style normalStyle = new Style(typeof(ToggleButton));
        Style whiteStyle = new Style(typeof(ToggleButton));
        #region dependency properties

        public static readonly DependencyProperty ToggleButtonStyleProperty = DependencyProperty.Register("ToggleButtonStyle",
            typeof(Style), typeof(OnScreenKeyboard), new PropertyMetadata(null));

        public Style ToggleButtonStyle
        {
            get { return (Style)GetValue(ToggleButtonStyleProperty); }
            set { SetValue(ToggleButtonStyleProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register("SaveCommand",
            typeof(ICommand), typeof(OnScreenKeyboard), new PropertyMetadata(null));

        public ICommand SaveCommand
        {
            get { return (ICommand)GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand",
            typeof(ICommand), typeof(OnScreenKeyboard), new PropertyMetadata(null));

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        #endregion

        #region ActiveControl region

        public FrameworkElement ActiveContainer
        {
            get { return (FrameworkElement)GetValue(ActiveContainerProperty); }
            set { SetValue(ActiveContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveContainer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveContainerProperty =
            DependencyProperty.Register("ActiveContainer", typeof(FrameworkElement), typeof(OnScreenKeyboard),
                new PropertyMetadata(null, OnActiveContainerChanged));

        private object ActiveControl
        {
            get { return GetValue(ActiveControlProperty); }
            set { SetValue(ActiveControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveControl.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty ActiveControlProperty =
            DependencyProperty.Register("ActiveControl", typeof(object), typeof(OnScreenKeyboard), new PropertyMetadata(null));


        private object KeyboardControl
        {
            get { return GetValue(KeyboardControlProperty); }
            set { SetValue(KeyboardControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for KeyboardControl.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty KeyboardControlProperty =
            DependencyProperty.Register("KeyboardControl", typeof(OnScreenKeyboard), typeof(OnScreenKeyboard),
                new PropertyMetadata(null));

        private static void OnActiveContainerChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is FrameworkElement)
            {
                var frameworkElement = (FrameworkElement)e.NewValue;
                var keyboardControl = (OnScreenKeyboard)dependencyObject;
                //keyboardControl.CanType = true;
                frameworkElement.GotKeyboardFocus += FrameworkElement_GotKeyboardFocus;
                frameworkElement.SetValue(KeyboardControlProperty, dependencyObject);
            }
            else if (e.OldValue is Control)
            {
                var frameworkElement = (FrameworkElement)e.OldValue;
                frameworkElement.GotKeyboardFocus -= FrameworkElement_GotKeyboardFocus;
            }
        }

        private static void FrameworkElement_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var dependencyObject = (DependencyObject)sender;
            var keyboardControl = (OnScreenKeyboard)dependencyObject.GetValue(KeyboardControlProperty);
            keyboardControl._activeControl = (FrameworkElement)e.NewFocus;
            //keyboardControl.CanType = e.NewFocus is TextBoxBase || e.NewFocus is PasswordBox;
        }

        private FrameworkElement _activeControl;

        #endregion

        private bool? CanType
        {
            get { return _canType; }
            set
            {
                if (_canType != value)
                {
                    _canType = value;
                    _allOnScreenKeys.ForEach(i => i.CanType(value == true));
                }
            }
        }

        private bool? _canType;

        private readonly List<OnScreenKeyboardSection> _sections = new List<OnScreenKeyboardSection>();
        private readonly List<OnScreenKey> _allOnScreenKeys = new List<OnScreenKey>();
        private readonly List<OnScreenKeyStateModifier> _activeKeyModifiers = new List<OnScreenKeyStateModifier>();

        public override void BeginInit()
        {
            SetValue(FocusManager.IsFocusScopeProperty, true);
            var mainSection = new OnScreenKeyboardSection();
            
            var mainKeys = new ObservableCollection<OnScreenKey>
            {
				//new OnScreenKeyNormal(0, 00, new[] {"`", "~", "∞", "≈"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 01, new[] {"1", "!", "§", "¡"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 02, new[] {"2", "@", "☼"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 03, new[] {"3", "#", "†", "‡"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 04, new[] {"4", "$", "£", "¢" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 05, new[] {"5", "%", "§", "¶"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 06, new[] {"6", "^", "∈", "°" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 07, new[] {"7", "&", "∩", "⊄"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 08, new[] {"8", "*", "∪"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 09, new[] {"9", "(", "⊂", "⊆" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 10, new[] {"0", ")", "⊃", "⊇" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 11, new[] {"-", ";", "×", "Ø"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeyNormal(0, 12, new[] {"=", "+", "≠", "±" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeySpecial(0, 13, "Bksp", ExecuteDelegateFunctions.BackspaceExecuteDelegate) {GridWidth = new GridLength(2, GridUnitType.Star)},

				//new OnScreenKeySpecial(1, 00, "Tab", ExecuteDelegateFunctions.MoveNextExecuteDelegate) {GridWidth = new GridLength(1.5, GridUnitType.Star)},
				new OnScreenKeyNormal(0, 00, new[] {"a", "a", "α", "Θ", "q"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 01, new[] {"z", "z","ω","Ω","w"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 02, new[] {"e", "e", "ε","e"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 03, new[] {"r", "r","ρ","r"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 04, new[] {"t", "t","t"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 05, new[] {"y", "y", "ψ", "Ψ","y"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 06, new[] {"u", "u","u"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 07, new[] {"i", "i","i"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 08, new[] {"o", "o","o"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 09, new[] {"p", "p", "π", "Π","p"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeySpecial(0, 10, "<---", ExecuteDelegateFunctions.BackspaceExecuteDelegate,"test") {GridWidth = new GridLength(2, GridUnitType.Star)},
                new OnScreenKeySpecial( 0,  11, "    ", ExecuteDelegateFunctions.MoveToNextPositionDelegate,""){GridWidth = new GridLength(0.2, GridUnitType.Star)},
                new OnScreenKeyNormal(0, 12, new[] {"1", "{", "‹", "«","1"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial) ,
                new OnScreenKeyNormal(0, 13, new[] {"2", "}", "›", "»","2"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 14, new[] {"3", "|","3"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                

				//new OnScreenKeyToggle(2, 00, new[] {"Caps"}, OnScreenKeyModifierType.Shift)
				//{
				//	GridWidth = new GridLength(1.7, GridUnitType.Star)
				//},
				new OnScreenKeyNormal(1, 00, new[] {"q", "q", "θ","a"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 01, new[] {"s", "s", "σ", "Σ","s"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 02, new[] {"d", "d", "δ", "Δ","d"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 03, new[] {"f", "f","f"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 04, new[] {"g", "g", "γ", "Γ","g"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 05, new[] {"h", "h", "φ", "Φ","h"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 06, new[] {"j", "j","j"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 07, new[] {"k", "k","k"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 08, new[] {"l", "l", "λ", "Λ","l"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 09, new[] {"m", "m", "♠", "♣","\\"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                //new OnScreenKeyNormal(1, 10, new[] {"<", "'", "♥", "♦","<"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeySpecial( 1,  10, "<", ExecuteDelegateFunctions.MoveToPreviousPositionDelegate,""){GridWidth = new GridLength(1.0, GridUnitType.Star)},
                //new OnScreenKeyNormal(1, 11, new[] {">", "'", "♥", "♦",">"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeySpecial( 1,  11, ">", ExecuteDelegateFunctions.MoveToNextPositionDelegate,""){GridWidth = new GridLength(1.0, GridUnitType.Star)},


                new OnScreenKeySpecial( 1,  12, "    ", ExecuteDelegateFunctions.MoveToNextPositionDelegate,""){GridWidth = new GridLength(0.2, GridUnitType.Star)},
                new OnScreenKeyNormal(1, 13, new[] {"4", "4", "♥", "♦","4"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 14, new[] {"5", "5", "♥", "♦","5"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 15, new[] {"6", "6", "♥", "♦","6" }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
				//new OnScreenKeySpecial(1, 11, "Enter", ExecuteDelegateFunctions.MoveNextExecuteDelegate) {GridWidth = new GridLength(1.8, GridUnitType.Star)},

				//new OnScreenKeyModifier(3, 00, new[] {"Shift"}, OnScreenKeyModifierType.Shift){GridWidth = new GridLength(2.4, GridUnitType.Star)},
                new OnScreenKeyNormal(2, 00, new[] {"w", "w", "ζ","z"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 01, new[] {"x", "x", "ξ", "Ξ","x"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 02, new[] {"c", "c", "χ", "Χ","c"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 03, new[] {"v", "v","v"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 04, new[] {"b", "b", "β","b"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 05, new[] {"n", "n","h"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 06, new[] {"_", ":", "μ","m"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 07, new[] {"-", "<", "≤"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 09, new[] {"@", ">", "≥"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(2.0, GridUnitType.Star)},
                new OnScreenKeyNormal(2, 08, new[] {".", "?","."}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(2.0, GridUnitType.Star)},
                new OnScreenKeySpecial( 2,  10, "    ", ExecuteDelegateFunctions.MoveToNextPositionDelegate,""){GridWidth = new GridLength(0.2, GridUnitType.Star)},
                new OnScreenKeyNormal(2, 11, new[] {"7", "7","7"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 12, new[] {"8", "8","8"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 13, new[] {"9", "9","9"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),



                //new OnScreenKeyModifier(3, 00, new[] {"&?!"}, OnScreenKeyModifierType.Shift),
                //new OnScreenKeyModifier(3, 01, new[] {"QWERTY"}, OnScreenKeyModifierType.qwerty){GridWidth = new GridLength(2.6, GridUnitType.Star)},
                new OnScreenKeyNormal(3, 00, new[] {" "}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(9.4, GridUnitType.Star)},
                new OnScreenKeyNormal(3, 01, new[] {".fr"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(1.1, GridUnitType.Star)},
                new OnScreenKeyNormal(3, 02, new[] {".com"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(1.1, GridUnitType.Star)},
                new OnScreenKeySpecial( 3,  03, "    ", ExecuteDelegateFunctions.MoveToNextPositionDelegate,""){GridWidth = new GridLength(0.2, GridUnitType.Star)},
                new OnScreenKeyNormal(3, 04, new[] {""}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){Opacity=0},
                new OnScreenKeyNormal(3, 05, new[] {"0"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){GridWidth = new GridLength(0.9, GridUnitType.Star)},
                new OnScreenKeyNormal(3, 06, new[] {""}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){Opacity=0},
				//new OnScreenKeySpecial(4, 00,  "Clear", ExecuteDelegateFunctions.ClearExecuteDelegate),
				//new OnScreenKeyModifier(4, 01, new [] { "Special"}, OnScreenKeyModifierType.Special),
				//new OnScreenKeySpecial(4, 02,  string.Empty, " "){GridWidth = new GridLength(5, GridUnitType.Star)},
				//new OnScreenKeySpecial(4, 03,  "Save",  nameof(SaveCommand)) {ClickCommand = nameof(SaveCommand)},
				//new OnScreenKeySpecial(4, 04, "Cancel",  nameof(CancelCommand)) {ClickCommand = nameof(CancelCommand)},

                new OnScreenKeyNormal(4,00,new[] { "@gmail.com" },CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){
                    GridWidth = new GridLength(3.0, GridUnitType.Star)
                },
                new OnScreenKeyNormal(4,01,new[] { "@hotmail.com" },CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){
                    GridWidth = new GridLength(2.66, GridUnitType.Star)
                },
                new OnScreenKeyNormal(4,02,new[] { "@yahoo.com" },CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){
                    GridWidth = new GridLength(2.66, GridUnitType.Star)},
                new OnScreenKeyNormal(4,03,new[] { "@orange.fr" },CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial){
                    GridWidth = new GridLength(2.66, GridUnitType.Star)}
            };

            mainSection.Keys = mainKeys;
            mainSection.SetValue(ColumnProperty, 0);
            _sections.Add(mainSection);
            ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
            Children.Add(mainSection);
            _allOnScreenKeys.AddRange(mainSection.Keys);

            //var specialSection = new OnScreenKeyboardSection();
            //var specialKeys = new ObservableCollection<OnScreenKey>
            //                      {
            //                          new OnScreenKeyNormal ( 0,  0, Key = new ChordKey("Select All", VirtualKeyCode.CONTROL, VirtualKeyCode.VK__A), GridWidth = new GridLength(2, GridUnitType.Star)},
            //                          new OnScreenKeyNormal ( 0,  1, Key = new ChordKey("Undo", VirtualKeyCode.CONTROL, VirtualKeyCode.VK__Z) },
            //                          new OnScreenKeyNormal ( 1,  0, Key = new ChordKey("Copy", VirtualKeyCode.CONTROL, VirtualKeyCode.VK__C) },
            //                          new OnScreenKeyNormal ( 1,  1, Key = new ChordKey("Cut", VirtualKeyCode.CONTROL, VirtualKeyCode.VK__X) },
            //                          new OnScreenKeyNormal ( 1,  2, Key = new ChordKey("Paste", VirtualKeyCode.CONTROL, VirtualKeyCode.VK__V) },
            //                          new OnScreenKeyNormal ( 2,  0, Key = new VirtualKey(VirtualKeyCode.DELETE, "Del") },
            //                          new OnScreenKeyNormal ( 2,  1, Key = new VirtualKey(VirtualKeyCode.HOME, "Home") },
            //                          new OnScreenKeyNormal ( 2,  2, Key = new VirtualKey(VirtualKeyCode.END, "End") },
            //                          new OnScreenKeyNormal ( 3,  0, Key = new VirtualKey(VirtualKeyCode.PRIOR, "PgUp") },
            //                          new OnScreenKeyNormal ( 3,  1, Key = new VirtualKey(VirtualKeyCode.UP, "Up") },
            //                          new OnScreenKeyNormal ( 3,  2, Key = new VirtualKey(VirtualKeyCode.NEXT, "PgDn") },
            //                          new OnScreenKeyNormal ( 4,  0, Key = new VirtualKey(VirtualKeyCode.LEFT, "Left") },
            //                          new OnScreenKeyNormal ( 4,  1, Key = new VirtualKey(VirtualKeyCode.DOWN, "Down") },
            //                          new OnScreenKeyNormal ( 4,  2, Key = new VirtualKey(VirtualKeyCode.RIGHT, "Right") },
            //                      };

            //specialSection.Keys = specialKeys;
            //specialSection.SetValue(ColumnProperty, 1);
            //_sections.Add(specialSection);
            //ColumnDefinitions.Add(new ColumnDefinition());
            //Children.Add(specialSection);
            //_allOnScreenKeys.AddRange(specialSection.Keys);
            
            Loaded += OnScreenKeyboard_Loaded;

            base.BeginInit();
        }

        private Style createWhiteStyle()
        {
            Style whiteStyle = new Style(typeof(ToggleButton));
            Setter BackgroundSetter = new Setter();
            BackgroundSetter.Property = BackgroundProperty;
            var startColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#ffffff");
            var endColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#ffffff");
            LinearGradientBrush gradient = new LinearGradientBrush(startColour.Color, endColour.Color, new System.Windows.Point(0, 0), new System.Windows.Point(0, 1));
            GradientStop gdStop = new GradientStop(startColour.Color, 0.2);
            GradientStop gdStop1 = new GradientStop(startColour.Color, 0.85);
            GradientStop gdStop2 = new GradientStop(startColour.Color, 1);
            gradient.GradientStops.Add(gdStop);
            gradient.GradientStops.Add(gdStop1);
            gradient.GradientStops.Add(gdStop2);
            BackgroundSetter.Value = startColour;
            whiteStyle.Setters.Add(BackgroundSetter);
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty, FontWeights.Normal);
            whiteStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 25.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter foreground = new Setter(ToggleButton.ForegroundProperty, startColour);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            //Setter background = new Setter(ToggleButton.BackgroundProperty, System.Drawing.Color.White);
            whiteStyle.Setters.Add(fontWeight);
            whiteStyle.Setters.Add(fontSize);
            whiteStyle.Setters.Add(margin);
            whiteStyle.Setters.Add(isTabStop);
            whiteStyle.Setters.Add(overrideDefaultStyle);
            whiteStyle.Setters.Add(foreground);
            //whiteStyle.Setters.Add(background);
            ControlTemplate template = XamlRead(getWhiteControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = template;
            whiteStyle.Setters.Add(templateStyle);
            return whiteStyle;
        }

        public static object XamlRead(string xamlString, bool insertNamespacesInFirstElement)
        {
            string copy = xamlString;
            if (insertNamespacesInFirstElement)
            {
                int indexOfSelfClosingTag = xamlString.IndexOf("/>");
                int indexOfClosingTag = xamlString.IndexOf(">");
                int actualIndex = -1;
                if (indexOfSelfClosingTag == -1)
                    actualIndex = indexOfClosingTag;
                else if (indexOfClosingTag == -1)
                    actualIndex = indexOfSelfClosingTag;
                else
                    actualIndex = Math.Min(indexOfSelfClosingTag, indexOfClosingTag);
                if (actualIndex > -1)
                    copy = copy.Insert(actualIndex, " xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"");
            }
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(copy);
            MemoryStream stream = new MemoryStream(bytes);
            return XamlReader.Load(stream);
        }

        private string getControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"ToggleButton\">");
            sb.Append("<Border Name=\"border\" BorderBrush=\"#f1f1f1\" Background=\"#f1f1f1\" BorderThickness=\"0\">");
            sb.Append("<Grid>");
            sb.Append("<ContentPresenter Name=\"contentShadow\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("<ContentPresenter Name=\"content\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("</Grid>");
            sb.Append("</Border>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property=\"Background\" Value =\"#DCDCDC\"/> ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#FF4788c8\" />");
            sb.Append("<Setter Property=\"Foreground\" Value =\"#FF4788c8\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" Value =\"#DCDCDC\"/> ");

            sb.Append("<Setter TargetName=\"content\" Property =\"RenderTransform\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<TranslateTransform Y=\"1.0\" />");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsChecked\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" Value =\"#DCDCDC\"/> ");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsFocused\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#FF282828\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value =\"False\" > ");
            sb.Append("<Setter Property=\"Foreground\" Value =\"Gray\" />");
            sb.Append("<Setter TargetName=\"border\" Property =\"Opacity\" Value =\"0.7\" />");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private string getDarkgrayControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"ToggleButton\">");
            sb.Append("<Border Name=\"border\" BorderBrush=\"#DCDCDC\" Background=\"#DCDCDC\" BorderThickness=\"0\">");
            sb.Append("<Grid>");
            sb.Append("<ContentPresenter Name=\"contentShadow\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("<ContentPresenter Name=\"content\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("</Grid>");
            sb.Append("</Border>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#DCDCDC\" />");
            sb.Append("<Setter Property=\"Foreground\" Value =\"#FF4788c8\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<LinearGradientBrush StartPoint=\"0, 0\" EndPoint =\"0, 1\" > ");
            sb.Append("<GradientStop Offset=\"0.35\" Color =\"#DCDCDC\" />");
            sb.Append("<GradientStop Offset=\"0.95\" Color =\"#DCDCDC\" />");
            sb.Append("<GradientStop Offset=\"1\" Color =\"#DCDCDC\" />");
            sb.Append("</LinearGradientBrush>");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");

            sb.Append("<Setter TargetName=\"content\" Property =\"RenderTransform\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<TranslateTransform Y=\"1.0\" />");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsChecked\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<LinearGradientBrush StartPoint=\"0,0\" EndPoint =\"0,1\" > ");
            sb.Append("<GradientStop Offset=\"0.2\" Color =\"#DCDCDC\" />");
            sb.Append("<GradientStop Offset=\"0.85\" Color =\"DarkGray\" />");
            sb.Append("<GradientStop Offset=\"1\" Color =\"#DCDCDC\" />");
            sb.Append("</LinearGradientBrush>");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsFocused\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#FF282828\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value =\"False\" > ");
            sb.Append("<Setter Property=\"Foreground\" Value =\"Gray\" />");
            sb.Append("<Setter TargetName=\"border\" Property =\"Opacity\" Value =\"0.7\" />");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private string getDarkgrayImageControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"{ x:Type ToggleButton}\">");
            sb.Append("<StackPanel Orientation=\"Horizontal\" >");
            sb.Append("<Image Name=\"PART_Image\" Source=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png\" />");
            sb.Append("</StackPanel>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value=\"True\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value=\"True\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value=\"False\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private string getWhiteControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"ToggleButton\">");
            sb.Append("<Border Name=\"border\" BorderBrush=\"Transparent\" Background=\"Transparent\" BorderThickness=\"0\">");
            sb.Append("<Grid>");
            sb.Append("<ContentPresenter Name=\"contentShadow\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("<ContentPresenter Name=\"content\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("</Grid>");
            sb.Append("</Border>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"Transparent\" />");
            sb.Append("<Setter Property=\"Foreground\" Value =\"Transparent\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<LinearGradientBrush StartPoint=\"0, 0\" EndPoint =\"0, 1\" > ");
            sb.Append("<GradientStop Offset=\"0.35\" Color =\"Transparent\" />");
            sb.Append("<GradientStop Offset=\"0.95\" Color =\"Transparent\" />");
            sb.Append("<GradientStop Offset=\"1\" Color =\"Transparent\" />");
            sb.Append("</LinearGradientBrush>");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");

            sb.Append("<Setter TargetName=\"content\" Property =\"RenderTransform\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<TranslateTransform Y=\"1.0\" />");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsChecked\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<LinearGradientBrush StartPoint=\"0,0\" EndPoint =\"0,1\" > ");
            sb.Append("<GradientStop Offset=\"0.2\" Color =\"Transparent\" />");
            sb.Append("<GradientStop Offset=\"0.85\" Color =\"Transparent\" />");
            sb.Append("<GradientStop Offset=\"1\" Color =\"Transparent\" />");
            sb.Append("</LinearGradientBrush>");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsFocused\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#ffffff\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value =\"False\" > ");
            sb.Append("<Setter Property=\"Foreground\" Value =\"Gray\" />");
            sb.Append("<Setter TargetName=\"border\" Property =\"Opacity\" Value =\"0.7\" />");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private Style createGrayStyle()
        {
            Style grayStyle = new Style(typeof(ToggleButton));
            Setter BackgroundSetter = new Setter();
            BackgroundSetter.Property = BackgroundProperty;
            var startColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#f1f1f1");
            var endColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#f1f1f1");
            LinearGradientBrush gradient = new LinearGradientBrush(startColour.Color, endColour.Color, new System.Windows.Point(0, 0), new System.Windows.Point(0, 1));
            GradientStop gdStop = new GradientStop(startColour.Color, 0.2);
            GradientStop gdStop1 = new GradientStop(startColour.Color, 0.85);
            GradientStop gdStop2 = new GradientStop(startColour.Color, 1);
            gradient.GradientStops.Add(gdStop);
            gradient.GradientStops.Add(gdStop1);
            gradient.GradientStops.Add(gdStop2);
            BackgroundSetter.Value = gradient;
            grayStyle.Setters.Add(BackgroundSetter);
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty,FontWeights.Normal);
            grayStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 30.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            grayStyle.Setters.Add(fontSize);
            grayStyle.Setters.Add(fontWeight);
            grayStyle.Setters.Add(margin);
            grayStyle.Setters.Add(isTabStop);
            grayStyle.Setters.Add(overrideDefaultStyle);
            ControlTemplate template = XamlRead(getControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = template;
            grayStyle.Setters.Add(templateStyle);
            return grayStyle;
        }

        private Style createDarkGrayStyle()
        {
            Style whiteStyle = new Style(typeof(ToggleButton));
            Setter BackgroundSetter = new Setter();
            BackgroundSetter.Property = BackgroundProperty;
            var startColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#DCDCDC");
            var endColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#DCDCDC");
            LinearGradientBrush gradient = new LinearGradientBrush(startColour.Color, endColour.Color, new System.Windows.Point(0.0, 0.0), new System.Windows.Point(0, 1));
            GradientStop gdStop = new GradientStop(startColour.Color, 0.2);
            GradientStop gdStop1 = new GradientStop(startColour.Color, 0.85);
            GradientStop gdStop2 = new GradientStop(startColour.Color, 1);
            gradient.GradientStops.Add(gdStop);
            gradient.GradientStops.Add(gdStop1);
            gradient.GradientStops.Add(gdStop2);
            BackgroundSetter.Value = gradient;
            whiteStyle.Setters.Add(BackgroundSetter);
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty, FontWeights.Normal);
            whiteStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 20.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            whiteStyle.Setters.Add(fontWeight);
            whiteStyle.Setters.Add(margin);
            whiteStyle.Setters.Add(fontSize);
            whiteStyle.Setters.Add(isTabStop);
            whiteStyle.Setters.Add(overrideDefaultStyle);
            ControlTemplate _template = XamlRead(getDarkgrayControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = _template;
            whiteStyle.Setters.Add(templateStyle);
            return whiteStyle;
        }

        private Style createDarkGrayImageStyle()
        {
            Style whiteStyle = new Style(typeof(ToggleButton));
            //Setter BackgroundSetter = new Setter();
            //BackgroundSetter.Property = BackgroundProperty;
            //var startColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#DCDCDC");
            //var endColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#DCDCDC");
            //LinearGradientBrush gradient = new LinearGradientBrush(startColour.Color, endColour.Color, new System.Windows.Point(0.0, 0.0), new System.Windows.Point(0, 1));
            //GradientStop gdStop = new GradientStop(startColour.Color, 0.2);
            //GradientStop gdStop1 = new GradientStop(startColour.Color, 0.85);
            //GradientStop gdStop2 = new GradientStop(startColour.Color, 1);
            //gradient.GradientStops.Add(gdStop);
            //gradient.GradientStops.Add(gdStop1);
            //gradient.GradientStops.Add(gdStop2);
            //BackgroundSetter.Value = gradient;
            //whiteStyle.Setters.Add(BackgroundSetter);
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty, FontWeights.Normal);
            whiteStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 20.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            string image_btn = "C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png";
            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(image_btn, UriKind.Absolute));
            Setter background = new Setter(ToggleButton.BackgroundProperty, btnLaunchAnimBrush);
            whiteStyle.Setters.Add(fontWeight);
            whiteStyle.Setters.Add(margin);
            whiteStyle.Setters.Add(fontSize);
            whiteStyle.Setters.Add(isTabStop);
            whiteStyle.Setters.Add(overrideDefaultStyle);
            whiteStyle.Setters.Add(background);
            ControlTemplate _template = XamlRead(getDarkgrayImageControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = _template;
            whiteStyle.Setters.Add(templateStyle);
            return whiteStyle;
        }

        private void OnScreenKeyboard_Loaded(object sender, RoutedEventArgs e)
        {
            TouchEnabledTextBox_GotTouchCapture();
            _allOnScreenKeys.ForEach(x =>
            {
                Style currentStyle = new Style();
                
                //x.Style = ToggleButtonStyle;
                if (x.Content != null)
                {
                    if (x.Content.ToString() == "<---")
                    {
                        currentStyle = createDarkGrayImageStyle();
                        x.Style = currentStyle;
                        string image_btn = "C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_.png";
                        ImageBrush btnLaunchAnimBrush = new ImageBrush();
                        btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(image_btn, UriKind.Absolute));
                        x.Background = btnLaunchAnimBrush;
                    }
                    else if (x.Content.ToString() == "    ")
                    {
                        currentStyle = createWhiteStyle();
                        x.Style = currentStyle;
                    }
                    else if(x.Content.ToString() == "@gmail.com" || x.Content.ToString() == "@hotmail.com" || x.Content.ToString() == "@orange.fr" || x.Content.ToString() == "@yahoo.com")
                    {
                        //x.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#DCDCDC"));
                        currentStyle = createDarkGrayStyle();
                        x.Style = currentStyle;
                    }
                    else
                    {
                        //x.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#f1f1f1"));
                        currentStyle = createGrayStyle();
                        x.Style = currentStyle;
                    }
                }
               
                //x.FontSize = 30;
                //x.BorderThickness = new Thickness(2);
                //x.BorderBrush = System.Windows.Media.Brushes.White;
                x.OnScreenKeyPressEvent -= OnScreenKeyPressEventHandler;
                x.OnScreenKeyPressEvent += OnScreenKeyPressEventHandler;
            });
        }

        void OnScreenKeyPressEventHandler(object sender, OnScreenKeyPressEventArgs e)
        {
            TouchEnabledTextBox_GotTouchCapture();
            if (e.StateModifier == null)
            {
                e.Execute(_activeControl);
                var singleInstance = _activeKeyModifiers.Where(i => i.SingleInstance).Select(k => k).ToList();
                singleInstance.ForEach(j => _activeKeyModifiers.Remove(j));
            }
            else
            {
                var dups = _activeKeyModifiers.Where(i => i.ModifierType == e.StateModifier.ModifierType).Select(k => k).ToList();
                dups.ForEach(j => _activeKeyModifiers.Remove(j));
                if (e.StateModifier.Clear == false)
                    _activeKeyModifiers.Add(e.StateModifier);
            }
            _allOnScreenKeys.ForEach(i => i.Update(_activeKeyModifiers));
            ToggleButton tgn = (ToggleButton)sender;
            tgn.IsChecked = false;
            TextBox txtActive = (TextBox)_activeControl;
            int pos = txtActive.SelectionStart;
            int textLength = tgn.Content.ToString().Length - 1;
            txtActive.SelectionStart = textLength + pos;
        }

        private void TouchEnabledTextBox_GotTouchCapture()
        {
            Process[] oskProcessArray = Process.GetProcessesByName("TabTip");
            foreach (Process onscreenProcess in oskProcessArray)
            {
                onscreenProcess.Kill();
                onscreenProcess.Dispose();
                onscreenProcess.Close();
            }
        }

    }
}