﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using KeyBoardControl.Keyboard.Keys;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Markup;
using System.IO;

namespace KeyBoardControl.Keyboard
{
    public class OnScreenNumericKeyboard : Grid
    {
        #region dependency properties

        public static readonly DependencyProperty ToggleButtonStyleProperty = DependencyProperty.Register("NumericToggleButtonStyle",
            typeof(Style), typeof(OnScreenNumericKeyboard), new PropertyMetadata(null));

        public Style NumericToggleButtonStyle
        {
            get { return (Style)GetValue(ToggleButtonStyleProperty); }
            set { SetValue(ToggleButtonStyleProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register("SaveCommand",
            typeof(ICommand), typeof(OnScreenNumericKeyboard), new PropertyMetadata(null));

        public ICommand SaveCommand
        {
            get { return (ICommand)GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand",
            typeof(ICommand), typeof(OnScreenNumericKeyboard), new PropertyMetadata(null));

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        #endregion

        #region ActiveControl region

        public FrameworkElement ActiveContainer
        {
            get { return (FrameworkElement)GetValue(ActiveContainerProperty); }
            set { SetValue(ActiveContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveContainer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveContainerProperty =
            DependencyProperty.Register("ActiveContainer", typeof(FrameworkElement), typeof(OnScreenNumericKeyboard),
                new PropertyMetadata(null, OnActiveContainerChanged));

        private object ActiveControl
        {
            get { return GetValue(ActiveControlProperty); }
            set { SetValue(ActiveControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveControl.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty ActiveControlProperty =
            DependencyProperty.Register("ActiveControl", typeof(object), typeof(OnScreenNumericKeyboard), new PropertyMetadata(null));


        private object KeyboardControl
        {
            get { return GetValue(KeyboardControlProperty); }
            set { SetValue(KeyboardControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for KeyboardControl.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty KeyboardControlProperty =
            DependencyProperty.Register("KeyboardControl", typeof(OnScreenNumericKeyboard), typeof(OnScreenNumericKeyboard),
                new PropertyMetadata(null));

        private static void OnActiveContainerChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is FrameworkElement)
            {
                var frameworkElement = (FrameworkElement)e.NewValue;
                var keyboardControl = (OnScreenNumericKeyboard)dependencyObject;
                //keyboardControl.CanType = true;
                frameworkElement.GotKeyboardFocus += FrameworkElement_GotKeyboardFocus;
                frameworkElement.SetValue(KeyboardControlProperty, dependencyObject);
                //TextBox tt = (TextBox)frameworkElement;
                //if(tt.Text == "__/__/____")
                //{
                //    tt.SelectionStart = 0;
                //}
               
            }
            else if (e.OldValue is Control)
            {
                var frameworkElement = (FrameworkElement)e.OldValue;
                frameworkElement.GotKeyboardFocus -= FrameworkElement_GotKeyboardFocus;
            }
        }

        private static void FrameworkElement_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var dependencyObject = (DependencyObject)sender;
            var keyboardControl = (OnScreenNumericKeyboard)dependencyObject.GetValue(KeyboardControlProperty);
            keyboardControl._activeControl = (FrameworkElement)e.NewFocus;
            TextBox tt = (TextBox)dependencyObject;
            if (tt.Text == "__/__/____")
            {
                tt.SelectionStart = 0;
            }
           
            //keyboardControl.CanType = e.NewFocus is TextBoxBase || e.NewFocus is PasswordBox;
        }

        private FrameworkElement _activeControl;

        #endregion

        private bool? CanType
        {
            get { return _canType; }
            set
            {
                if (_canType != value)
                {
                    _canType = value;
                    _allOnScreenKeys.ForEach(i => i.CanType(value == true));
                }
            }
        }

        private bool? _canType;

        private readonly List<OnScreenKeyboardSection> _sections = new List<OnScreenKeyboardSection>();
        private readonly List<OnScreenKey> _allOnScreenKeys = new List<OnScreenKey>();
        private readonly List<OnScreenKeyStateModifier> _activeKeyModifiers = new List<OnScreenKeyStateModifier>();

        public override void BeginInit()
        {
            SetValue(FocusManager.IsFocusScopeProperty, true);

            var mainSection = new OnScreenKeyboardSection();
            var mainKeys = new ObservableCollection<OnScreenKey>
            {
				new OnScreenKeyNormal(2, 00, new[] {"7", "A", "α", "Θ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 01, new[] {"8", "Z","ω","Ω"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(2, 02, new[] {"9", "E", "ε"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
         

				//new OnScreenKeyToggle(2, 00, new[] {"Caps"}, OnScreenKeyModifierType.Shift)
				//{
				//	GridWidth = new GridLength(1.7, GridUnitType.Star)
				//},
				new OnScreenKeyNormal(1, 00, new[] {"4", "Q", "θ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 01, new[] {"5", "S", "σ", "Σ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(1, 02, new[] {"6", "D", "δ", "Δ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
         

				//new OnScreenKeyModifier(3, 00, new[] {"Shift"}, OnScreenKeyModifierType.Shift){GridWidth = new GridLength(2.4, GridUnitType.Star)},
                new OnScreenKeyNormal(0, 00, new[] {"1", "W", "ζ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 01, new[] {"2", "X", "ξ", "Ξ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(0, 02, new[] {"3", "C", "χ", "Χ"}, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),

                new OnScreenKeyNormal(3, 00, new[] {" " }, CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeyNormal(3, 01, new[] {"0"},CaptionUpdateDelegateDelegateFunction.ShiftAndSpecial),
                new OnScreenKeySpecial(3, 02,   "Bksp" , ExecuteDelegateFunctions.BackspaceExecuteDelegate,"")

            };

            mainSection.Keys = mainKeys;
            mainSection.SetValue(ColumnProperty, 0);
            _sections.Add(mainSection);
            ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
            Children.Add(mainSection);
            _allOnScreenKeys.AddRange(mainSection.Keys);

            Loaded += OnScreenKeyboard_Loaded;

            base.BeginInit();
        }

        private string getDarkgrayImageControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"{ x:Type ToggleButton}\">");
            sb.Append("<StackPanel Orientation=\"Horizontal\" >");
            sb.Append("<Image Name=\"PART_Image\" Source=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png\" />");
            sb.Append("</StackPanel>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value=\"True\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value=\"True\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value=\"False\">");
            sb.Append("<Setter Property=\"Source\" Value=\"C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png\"  TargetName=\"PART_Image\"/>");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private Style createDarkGrayImageStyle()
        {
            Style whiteStyle = new Style(typeof(ToggleButton));
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty, FontWeights.Normal);
            whiteStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 20.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            string image_btn = "C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png";
            ImageBrush btnLaunchAnimBrush = new ImageBrush();
            btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(image_btn, UriKind.Absolute));
            Setter background = new Setter(ToggleButton.BackgroundProperty, btnLaunchAnimBrush);
            whiteStyle.Setters.Add(fontWeight);
            whiteStyle.Setters.Add(margin);
            whiteStyle.Setters.Add(fontSize);
            whiteStyle.Setters.Add(isTabStop);
            whiteStyle.Setters.Add(overrideDefaultStyle);
            whiteStyle.Setters.Add(background);
            ControlTemplate _template = XamlRead(getDarkgrayImageControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = _template;
            whiteStyle.Setters.Add(templateStyle);
            return whiteStyle;
        }

        public static object XamlRead(string xamlString, bool insertNamespacesInFirstElement)
        {
            string copy = xamlString;
            if (insertNamespacesInFirstElement)
            {
                int indexOfSelfClosingTag = xamlString.IndexOf("/>");
                int indexOfClosingTag = xamlString.IndexOf(">");
                int actualIndex = -1;
                if (indexOfSelfClosingTag == -1)
                    actualIndex = indexOfClosingTag;
                else if (indexOfClosingTag == -1)
                    actualIndex = indexOfSelfClosingTag;
                else
                    actualIndex = Math.Min(indexOfSelfClosingTag, indexOfClosingTag);
                if (actualIndex > -1)
                    copy = copy.Insert(actualIndex, " xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"");
            }
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(copy);
            MemoryStream stream = new MemoryStream(bytes);
            return XamlReader.Load(stream);
        }

        private Style createGrayStyle()
        {
            Style grayStyle = new Style(typeof(ToggleButton));
            Setter BackgroundSetter = new Setter();
            BackgroundSetter.Property = BackgroundProperty;
            var startColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#f1f1f1");
            var endColour = (SolidColorBrush)new BrushConverter().ConvertFrom("#f1f1f1");
            LinearGradientBrush gradient = new LinearGradientBrush(startColour.Color, endColour.Color, new System.Windows.Point(0, 0), new System.Windows.Point(0, 1));
            GradientStop gdStop = new GradientStop(startColour.Color, 0.2);
            GradientStop gdStop1 = new GradientStop(startColour.Color, 0.85);
            GradientStop gdStop2 = new GradientStop(startColour.Color, 1);
            gradient.GradientStops.Add(gdStop);
            gradient.GradientStops.Add(gdStop1);
            gradient.GradientStops.Add(gdStop2);
            BackgroundSetter.Value = gradient;
            grayStyle.Setters.Add(BackgroundSetter);
            Setter fontWeight = new Setter(ToggleButton.FontWeightProperty, FontWeights.Normal);
            grayStyle.Setters.Add(fontWeight);
            Setter fontSize = new Setter(ToggleButton.FontSizeProperty, 30.0);
            Setter margin = new Setter(ToggleButton.MarginProperty, new Thickness(2));
            Setter isTabStop = new Setter(ToggleButton.IsTabStopProperty, false);
            Setter overrideDefaultStyle = new Setter(ToggleButton.OverridesDefaultStyleProperty, true);
            grayStyle.Setters.Add(fontSize);
            grayStyle.Setters.Add(fontWeight);
            grayStyle.Setters.Add(margin);
            grayStyle.Setters.Add(isTabStop);
            grayStyle.Setters.Add(overrideDefaultStyle);
            ControlTemplate template = XamlRead(getControlTemplate(), true) as ControlTemplate;

            Setter templateStyle = new Setter();
            templateStyle.Property = ToggleButton.TemplateProperty;
            templateStyle.Value = template;
            grayStyle.Setters.Add(templateStyle);
            return grayStyle;
        }

        private string getControlTemplate()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ControlTemplate TargetType=\"ToggleButton\">");
            sb.Append("<Border Name=\"border\" BorderBrush=\"#f1f1f1\" Background=\"#f1f1f1\" BorderThickness=\"0\">");
            sb.Append("<Grid>");
            sb.Append("<ContentPresenter Name=\"contentShadow\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("<ContentPresenter Name=\"content\" HorizontalAlignment =\"Center\" VerticalAlignment =\"Center\" />");
            sb.Append("</Grid>");
            sb.Append("</Border>");
            sb.Append("<ControlTemplate.Triggers>");
            sb.Append("<Trigger Property=\"IsMouseOver\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property=\"Background\" Value =\"#DCDCDC\"/> ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#FF4788c8\" />");
            sb.Append("<Setter Property=\"Foreground\" Value =\"#FF4788c8\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsPressed\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" Value =\"#DCDCDC\"/> ");

            sb.Append("<Setter TargetName=\"content\" Property =\"RenderTransform\" > ");
            sb.Append("<Setter.Value>");
            sb.Append("<TranslateTransform Y=\"1.0\" />");
            sb.Append("</Setter.Value>");
            sb.Append("</Setter>");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsChecked\" Value =\"True\" > ");
            sb.Append("<Setter Property=\"Background\" Value =\"#DCDCDC\"/> ");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsFocused\" Value =\"True\" > ");
            sb.Append("<Setter TargetName=\"border\" Property =\"BorderBrush\" Value =\"#FF282828\" />");
            sb.Append("</Trigger>");
            sb.Append("<Trigger Property=\"IsEnabled\" Value =\"False\" > ");
            sb.Append("<Setter Property=\"Foreground\" Value =\"Gray\" />");
            sb.Append("<Setter TargetName=\"border\" Property =\"Opacity\" Value =\"0.7\" />");
            sb.Append("</Trigger>");
            sb.Append("</ControlTemplate.Triggers>");
            sb.Append("</ControlTemplate>");
            return sb.ToString();
        }

        private void OnScreenKeyboard_Loaded(object sender, RoutedEventArgs e)
        {
            _allOnScreenKeys.ForEach(x =>
            {
                Style currentStyle = new Style();
                if (x.Content != null)
                {
                    if(x.Content.ToString() == "Bksp")
                    {
                        currentStyle = createDarkGrayImageStyle();
                        x.Style = currentStyle;
                        //string image_btn = "C:\\EVENTS\\Assets\\Default\\Buttons\\delete_back_1.png";
                        //ImageBrush btnLaunchAnimBrush = new ImageBrush();
                        //btnLaunchAnimBrush.ImageSource = new BitmapImage(new Uri(image_btn, UriKind.Absolute));
                        //x.Background = btnLaunchAnimBrush;
                    }
                    else
                    {
                        currentStyle = createGrayStyle();
                        x.Style = currentStyle;
                    }
                }
                //x.Style = NumericToggleButtonStyle;
                x.OnScreenKeyPressEvent -= OnScreenKeyPressEventHandler;
                x.OnScreenKeyPressEvent += OnScreenKeyPressEventHandler;
            });
        }

        void OnScreenKeyPressEventHandler(object sender, OnScreenKeyPressEventArgs e)
        {
            if (e.StateModifier == null)
            {
                e.Execute(_activeControl);
                var singleInstance = _activeKeyModifiers.Where(i => i.SingleInstance).Select(k => k).ToList();
                singleInstance.ForEach(j => _activeKeyModifiers.Remove(j));
            }
            else
            {
                var dups = _activeKeyModifiers.Where(i => i.ModifierType == e.StateModifier.ModifierType).Select(k => k).ToList();
                dups.ForEach(j => _activeKeyModifiers.Remove(j));
                if (e.StateModifier.Clear == false)
                    _activeKeyModifiers.Add(e.StateModifier);
            }
            _allOnScreenKeys.ForEach(i => i.Update(_activeKeyModifiers));
        }
    }
}

