using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using KeyBoardControl.Keyboard.Keys;
using System.Text.RegularExpressions;
using System;

namespace KeyBoardControl.Keyboard
{
	public delegate void ExecuteDelegate(OnScreenKey button, FrameworkElement frameworkElement);
	internal static class ExecuteDelegateFunctions
	{

		public static ExecuteDelegate BackspaceExecuteDelegate = (key, frameworkElement) =>
		{
			if (frameworkElement is TextBoxBase)
			{
                var textBoxBase = (TextBox)frameworkElement;
                string txt = textBoxBase.Text;
                

                if (txt.Contains("/"))
                {
                    char[] tabEnd = textBoxBase.Text.ToArray();
                    int incr = textBoxBase.SelectionStart -1;
                    if(incr >= 0 && incr <= tabEnd.Length - 1)
                    {
                        if (IsNumeric(tabEnd[incr]) || tabEnd[incr] == '_' || tabEnd[incr] == '/')
                        {
                            if (tabEnd[textBoxBase.SelectionStart - 1] == '/')
                            {
                                incr--;
                            }
                            tabEnd[incr] = '_';
                            string charsStr = new string(tabEnd);
                            textBoxBase.Text = charsStr;
                            if (tabEnd[incr] == '/')
                            {
                                textBoxBase.SelectionStart = incr - 1;
                            }
                            else
                            {
                                textBoxBase.SelectionStart = incr;
                            }
                        }
                    }
                    
                }
                else
                {
                    var start = textBoxBase.Text.Substring(0, textBoxBase.SelectionStart);
                    var end = textBoxBase.Text.Substring(textBoxBase.SelectionStart + textBoxBase.SelectionLength);
                    if (textBoxBase.SelectionLength > 0)
                    {
                        textBoxBase.Text = start + end;
                        textBoxBase.SelectionLength = 0;
                    }
                    else if (start.Length > 0)
                    {
                        textBoxBase.Text = start.Substring(0, start.Length - 1) + end;
                        textBoxBase.SelectionStart = start.Length - 1;
                        textBoxBase.SelectionLength = 0;
                    }
                }
				
			}
			else if (frameworkElement is PasswordBox)
			{
				var passwordBoxBase = (PasswordBox)frameworkElement;
				passwordBoxBase.Password = passwordBoxBase.Password.Substring(0, passwordBoxBase.Password.Length - 1);
			}
		};

        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static ExecuteDelegate DefaultExecuteDelegate = (key, frameworkElement) =>
		{
			if (frameworkElement is TextBoxBase)
			{
				var textBoxBase = (TextBox)frameworkElement;
				var start = textBoxBase.Text.Substring(0, textBoxBase.SelectionStart);
				var end = textBoxBase.Text.Substring(textBoxBase.SelectionStart + textBoxBase.SelectionLength);
                char[] tabEnd = textBoxBase.Text.ToArray();
                int incr = textBoxBase.SelectionStart;
                
                if(tabEnd.Length > 0 && textBoxBase.Text.Contains("/"))
                {
                    if (tabEnd[incr] == '_' || tabEnd[incr] == '/')
                    {
                        if (tabEnd[textBoxBase.SelectionStart] == '/')
                        {
                            incr++;
                        }
                        tabEnd[incr] = key.Value.ToCharArray()[0];
                        //var regex = new Regex(Regex.Escape("_"));
                        //var newText = regex.Replace(end, key.Value, 1);
                        //textBoxBase.Text = start + newText;
                        string charsStr = new string(tabEnd);
                        textBoxBase.Text = charsStr;
                        if (tabEnd[incr] == '/')
                        {
                            textBoxBase.SelectionStart = incr + 2;
                        }
                        else
                        {
                            textBoxBase.SelectionStart = incr + 1;
                        }

                        textBoxBase.SelectionLength = 0;
                    }
                }
                else
                {
                    textBoxBase.Text = start + key.Value + end;
                    textBoxBase.SelectionStart = start.Length + 1;
                    textBoxBase.SelectionLength = 0;
                }
				
			}
			else if (frameworkElement is PasswordBox)
			{
				var passwordBoxBase = (PasswordBox)frameworkElement;
				passwordBoxBase.Password = passwordBoxBase.Password + key.Value;
			}
		};

		public static ExecuteDelegate ClearExecuteDelegate = (key, frameworkElement) =>
		{
			if (frameworkElement is TextBoxBase)
			{
				var textBoxBase = (TextBox)frameworkElement;
				textBoxBase.Text = string.Empty;
			}
			else if (frameworkElement is PasswordBox)
			{
				var passwordBoxBase = (PasswordBox)frameworkElement;
				passwordBoxBase.Password = string.Empty;
			}
		};

        public static ExecuteDelegate MoveToNextPositionDelegate = (key, frameworkElement) =>
        {
            if (frameworkElement is TextBoxBase)
            {
                var textBoxBase = (TextBox)frameworkElement;
                int start = textBoxBase.SelectionStart + 1;
                if(start > textBoxBase.Text.Length)
                {
                    start = textBoxBase.Text.Length;
                }
                textBoxBase.SelectionStart = start;
            }
        };

        public static ExecuteDelegate MoveToPreviousPositionDelegate = (key, frameworkElement) =>
        {
            if (frameworkElement is TextBoxBase)
            {
                var textBoxBase = (TextBox)frameworkElement;
                int start = textBoxBase.SelectionStart - 1;
                if(start <= 0)
                {
                    start = 0;
                }
                textBoxBase.SelectionStart = start;
            }
        };

        public static ExecuteDelegate MoveNextExecuteDelegate = (key, frameworkElement) =>
		{
			frameworkElement.MoveFocus(key.Modifiers.Any(i => i.ModifierType == OnScreenKeyModifierType.Shift)
				? new TraversalRequest(FocusNavigationDirection.Right)
				: new TraversalRequest(FocusNavigationDirection.Left));
		};
        public static ExecuteDelegate MovePreviousExecuteDelegate = (key, frameworkElement) =>
        {
            frameworkElement.MoveFocus(key.Modifiers.Any(i => i.ModifierType == OnScreenKeyModifierType.Shift)
                ? new TraversalRequest(FocusNavigationDirection.Left)
                : new TraversalRequest(FocusNavigationDirection.Right));
        };
    }
}